/*
  $Id: demo3.cpp,v 0.0.0.1 1999/6/6 23:40:46 mbn Exp $

  ------------------------------------------------------------------------
  ClanLib, the platform independent game SDK.

  This library is distributed under the GNU LIBRARY GENERAL PUBLIC LICENSE
  version 2. See COPYING for details.

  For a total list of contributers see CREDITS.

  ------------------------------------------------------------------------

  File purpose:
    Shows the usage of the ResourceManager

*/

#include <clanlib.h>
#include <clanlayer2.h>

class TDemo3 : public CL_ClanApplication
{
public:
  virtual char *get_title()
  {
    return "Demo3: The ResourceManager";
  }

  virtual int main(int, char**)
  {
    cout << "Demo3: part of the tutorial by Arjan Lamers." << endl;
    
    cout << "Initializing display" << endl;
    CL_System::init_display();
    CL_Display::set_videomode(320,200,16);
   
    cout << "Loading resources" << endl;
    CL_ResourceManager *resources = new CL_ResourceManager( "demo3.dat");
    
    assert( resources!=NULL );

    CL_Surface *image1 = CL_Res_Surface::load("Demo3/image1", *resources);
    CL_Surface *image2 = CL_Res_Surface::load("Demo3/image2", *resources);

    int n_loops = CL_Res_Integer::load("Demo3/n_loops", *resources, -1 );

    if( n_loops==-1 )
      cout << "Looping eternally" << endl;
     else
      cout << "Looping " << n_loops << " times" << endl;

    cout << "Creating LayerManager" << endl;
    CL_LayerManager *layerm = new CL_LayerManager( 2 );    
    layerm->put_screen( image1, 0, 0, 0, 0 );
    layerm->put_screen( image2, 100, 100, 0, 1 );

    cout << "Showing layers" << endl;

    layerm->show_layers();
    layerm->select_layer( 1 );

    int d=1, count=1;
    int layer_a=0, layer_b=1;
    while (CL_Keyboard::get_keycode(CL_Keyboard::KEY_ESCAPE) == 0 && n_loops>=0 )
    {
      // clear the display
      CL_Display::clear_display( 0, 0, 0, 1 );

      // move the layers
      layerm->move_layer( layer_a, d, d );
      layerm->move_layer( layer_b, -d, -d );

      // show the layers
      layerm->show_layers();
      CL_Display::flip_display();      
      CL_System::keep_alive();

      // calculate next movement
      if( count==1 ) d--; else d++;
      if( d==100 || d==0 ) 
      {
        count = -count;
        layerm->swap_layers( layer_a, layer_b );
        if (layer_a==0) 
          { layer_a=1; layer_b=0; }
        else
          { layer_a=0; layer_b=1; }

      }

    if( n_loops!=-1 ) // if -1 loop eternally 
      n_loops--;
    }

    cout << "Cleaning up..." << endl;    
    delete image1;

    return 1;
  }
} app;
