#include <iostream.h>

#include <clanlib.h>
#include <clanlayer2.h>

class TDemo1 : public CL_ClanApplication
{
public:

  virtual char *get_title()
  { return "Demo1: image viewing"; }

  virtual int main(int, char**)
  {
    cout << "Demo1: part of the tutorial by Arjan Lamers." << endl;

    cout << "Initializing display" << endl;
    CL_System::init_display();
    CL_Display::set_videomode(320, 200, 16);

    cout << "Loading image" << endl;
    CL_Surface *image = CL_TargaProvider::create("image1.tga",NULL);

    cout << "Displaying the image" << endl;
    image->put_screen(0,0);
    CL_Display::flip_display();

    cout << "Waiting for a key..." << endl;
    while (CL_Keyboard::get_keycode(CL_Keyboard::KEY_ESCAPE) == 0)  
      CL_System::keep_alive();   

    cout << "Cleaning up..." << endl;
    delete image;

    return 1;
  }
} app;
