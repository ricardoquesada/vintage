/***************************************************************************
                          canvasview.cpp  -  description
                             -------------------
    begin                : Sat Jun  9 16:20:37 ART 2001
    copyright            : (C) 2001 by Ricardo Quesada
    email                : riq@core-sdi.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// include files for Qt
#include <qprinter.h>
#include <qpainter.h>

// application specific includes
#include "canvasview.h"
#include "canvasdoc.h"
#include "canvas.h"

CanvasView::CanvasView(QWidget *parent, const char *name) : QWidget(parent, name)
{
  setBackgroundMode(PaletteBase);
}

CanvasView::~CanvasView()
{
}

CanvasDoc *CanvasView::getDocument() const
{
  CanvasApp *theApp=(CanvasApp *) parentWidget();

  return theApp->getDocument();
}

void CanvasView::print(QPrinter *pPrinter)
{
  QPainter printpainter;
  printpainter.begin(pPrinter);
	
  // TODO: add your printing code here

  printpainter.end();
}
