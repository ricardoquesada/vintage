/***************************************************************************
                          canvasdoc.cpp  -  description
                             -------------------
    begin                : Sat Jun  9 16:20:37 ART 2001
    copyright            : (C) 2001 by Ricardo Quesada
    email                : riq@core-sdi.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// include files for Qt
#include <qdir.h>
#include <qwidget.h>

// include files for KDE
#include <klocale.h>
#include <kmessagebox.h>
#include <kio/job.h>
#include <kio/netaccess.h>

// application specific includes
#include "canvasdoc.h"
#include "canvas.h"
#include "canvasview.h"

QList<CanvasView> *CanvasDoc::pViewList = 0L;

CanvasDoc::CanvasDoc(QWidget *parent, const char *name) : QObject(parent, name)
{
  if(!pViewList)
  {
    pViewList = new QList<CanvasView>();
  }

  pViewList->setAutoDelete(true);
}

CanvasDoc::~CanvasDoc()
{
}

void CanvasDoc::addView(CanvasView *view)
{
  pViewList->append(view);
}

void CanvasDoc::removeView(CanvasView *view)
{
  pViewList->remove(view);
}
void CanvasDoc::setURL(const KURL &url)
{
  doc_url=url;
}

const KURL& CanvasDoc::URL() const
{
  return doc_url;
}

void CanvasDoc::slotUpdateAllViews(CanvasView *sender)
{
  CanvasView *w;
  if(pViewList)
  {
    for(w=pViewList->first(); w!=0; w=pViewList->next())
    {
      if(w!=sender)
        w->repaint();
    }
  }

}

bool CanvasDoc::saveModified()
{
  bool completed=true;

  if(modified)
  {
    CanvasApp *win=(CanvasApp *) parent();
    int want_save = KMessageBox::warningYesNoCancel(win, i18n("Warning"),
                                         i18n("The current file has been modified.\n"
                                              "Do you want to save it?"));
    switch(want_save)
    {
      case 1:
           if (doc_url.fileName() == i18n("Untitled"))
           {
             win->slotFileSaveAs();
           }
           else
           {
             saveDocument(URL());
       	   };

       	   deleteContents();
           completed=true;
           break;

      case 2:
           setModified(false);
           deleteContents();
           completed=true;
           break;	

      case 3:
           completed=false;
           break;

      default:
           completed=false;
           break;
    }
  }

  return completed;
}

void CanvasDoc::closeDocument()
{
  deleteContents();
}

bool CanvasDoc::newDocument()
{
  /////////////////////////////////////////////////
  // TODO: Add your document initialization code here
  /////////////////////////////////////////////////
  modified=false;
  doc_url.setFileName(i18n("Untitled"));

  return true;
}

bool CanvasDoc::openDocument(const KURL& url, const char *format /*=0*/)
{
  QString tmpfile;
  KIO::NetAccess::download( url, tmpfile );
  /////////////////////////////////////////////////
  // TODO: Add your document opening code here
  /////////////////////////////////////////////////

  KIO::NetAccess::removeTempFile( tmpfile );

  modified=false;
  return true;
}

bool CanvasDoc::saveDocument(const KURL& url, const char *format /*=0*/)
{
  /////////////////////////////////////////////////
  // TODO: Add your document saving code here
  /////////////////////////////////////////////////

  modified=false;
  return true;
}

void CanvasDoc::deleteContents()
{
  /////////////////////////////////////////////////
  // TODO: Add implementation to delete the document contents
  /////////////////////////////////////////////////

}
