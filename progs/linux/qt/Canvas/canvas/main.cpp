/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sat Jun  9 16:20:37 ART 2001
    copyright            : (C) 2001 by Ricardo Quesada
    email                : riq@core-sdi.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>

#include "canvas.h"

static const char *description =
	I18N_NOOP("Canvas");
// INSERT A DESCRIPTION FOR YOUR APPLICATION HERE
	
	
static KCmdLineOptions options[] =
{
  { "+[File]", I18N_NOOP("file to open"), 0 },
  { 0, 0, 0 }
  // INSERT YOUR COMMANDLINE OPTIONS HERE
};

int main(int argc, char *argv[])
{

	KAboutData aboutData( "canvas", I18N_NOOP("Canvas"),
		VERSION, description, KAboutData::License_GPL,
		"(c) 2001, Ricardo Quesada", 0, 0, "riq@core-sdi.com");
	aboutData.addAuthor("Ricardo Quesada",0, "riq@core-sdi.com");
	KCmdLineArgs::init( argc, argv, &aboutData );
	KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication app;
 
  if (app.isRestored())
  {
    RESTORE(CanvasApp);
  }
  else 
  {
    CanvasApp *canvas = new CanvasApp();
    canvas->show();

    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
		
		if (args->count())
		{
        canvas->openDocumentFile(args->arg(0));
		}
		else
		{
		  canvas->openDocumentFile();
		}
		args->clear();
  }

  return app.exec();
}  
