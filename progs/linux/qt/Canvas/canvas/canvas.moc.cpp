/****************************************************************************
** CanvasApp meta object code from reading C++ file 'canvas.h'
**
** Created: Sat Jun 9 16:27:45 2001
**      by: The Qt MOC ($Id: qt/src/moc/moc.y   2.3.0   edited 2001-02-20 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#if !defined(Q_MOC_OUTPUT_REVISION)
#define Q_MOC_OUTPUT_REVISION 9
#elif Q_MOC_OUTPUT_REVISION != 9
#error "Moc format conflict - please regenerate all moc files"
#endif

#include "canvas.h"
#include <qmetaobject.h>
#include <qapplication.h>



const char *CanvasApp::className() const
{
    return "CanvasApp";
}

QMetaObject *CanvasApp::metaObj = 0;

void CanvasApp::initMetaObject()
{
    if ( metaObj )
	return;
    if ( qstrcmp(KMainWindow::className(), "KMainWindow") != 0 )
	badSuperclassWarning("CanvasApp","KMainWindow");
    (void) staticMetaObject();
}

#ifndef QT_NO_TRANSLATION

QString CanvasApp::tr(const char* s)
{
    return qApp->translate( "CanvasApp", s, 0 );
}

QString CanvasApp::tr(const char* s, const char * c)
{
    return qApp->translate( "CanvasApp", s, c );
}

#endif // QT_NO_TRANSLATION

QMetaObject* CanvasApp::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    (void) KMainWindow::staticMetaObject();
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    typedef void (CanvasApp::*m1_t0)();
    typedef void (QObject::*om1_t0)();
    typedef void (CanvasApp::*m1_t1)();
    typedef void (QObject::*om1_t1)();
    typedef void (CanvasApp::*m1_t2)();
    typedef void (QObject::*om1_t2)();
    typedef void (CanvasApp::*m1_t3)(const KURL&);
    typedef void (QObject::*om1_t3)(const KURL&);
    typedef void (CanvasApp::*m1_t4)();
    typedef void (QObject::*om1_t4)();
    typedef void (CanvasApp::*m1_t5)();
    typedef void (QObject::*om1_t5)();
    typedef void (CanvasApp::*m1_t6)();
    typedef void (QObject::*om1_t6)();
    typedef void (CanvasApp::*m1_t7)();
    typedef void (QObject::*om1_t7)();
    typedef void (CanvasApp::*m1_t8)();
    typedef void (QObject::*om1_t8)();
    typedef void (CanvasApp::*m1_t9)();
    typedef void (QObject::*om1_t9)();
    typedef void (CanvasApp::*m1_t10)();
    typedef void (QObject::*om1_t10)();
    typedef void (CanvasApp::*m1_t11)();
    typedef void (QObject::*om1_t11)();
    typedef void (CanvasApp::*m1_t12)();
    typedef void (QObject::*om1_t12)();
    typedef void (CanvasApp::*m1_t13)();
    typedef void (QObject::*om1_t13)();
    typedef void (CanvasApp::*m1_t14)(const QString&);
    typedef void (QObject::*om1_t14)(const QString&);
    m1_t0 v1_0 = &CanvasApp::slotFileNewWindow;
    om1_t0 ov1_0 = (om1_t0)v1_0;
    m1_t1 v1_1 = &CanvasApp::slotFileNew;
    om1_t1 ov1_1 = (om1_t1)v1_1;
    m1_t2 v1_2 = &CanvasApp::slotFileOpen;
    om1_t2 ov1_2 = (om1_t2)v1_2;
    m1_t3 v1_3 = &CanvasApp::slotFileOpenRecent;
    om1_t3 ov1_3 = (om1_t3)v1_3;
    m1_t4 v1_4 = &CanvasApp::slotFileSave;
    om1_t4 ov1_4 = (om1_t4)v1_4;
    m1_t5 v1_5 = &CanvasApp::slotFileSaveAs;
    om1_t5 ov1_5 = (om1_t5)v1_5;
    m1_t6 v1_6 = &CanvasApp::slotFileClose;
    om1_t6 ov1_6 = (om1_t6)v1_6;
    m1_t7 v1_7 = &CanvasApp::slotFilePrint;
    om1_t7 ov1_7 = (om1_t7)v1_7;
    m1_t8 v1_8 = &CanvasApp::slotFileQuit;
    om1_t8 ov1_8 = (om1_t8)v1_8;
    m1_t9 v1_9 = &CanvasApp::slotEditCut;
    om1_t9 ov1_9 = (om1_t9)v1_9;
    m1_t10 v1_10 = &CanvasApp::slotEditCopy;
    om1_t10 ov1_10 = (om1_t10)v1_10;
    m1_t11 v1_11 = &CanvasApp::slotEditPaste;
    om1_t11 ov1_11 = (om1_t11)v1_11;
    m1_t12 v1_12 = &CanvasApp::slotViewToolBar;
    om1_t12 ov1_12 = (om1_t12)v1_12;
    m1_t13 v1_13 = &CanvasApp::slotViewStatusBar;
    om1_t13 ov1_13 = (om1_t13)v1_13;
    m1_t14 v1_14 = &CanvasApp::slotStatusMsg;
    om1_t14 ov1_14 = (om1_t14)v1_14;
    QMetaData *slot_tbl = QMetaObject::new_metadata(15);
    QMetaData::Access *slot_tbl_access = QMetaObject::new_metaaccess(15);
    slot_tbl[0].name = "slotFileNewWindow()";
    slot_tbl[0].ptr = (QMember)ov1_0;
    slot_tbl_access[0] = QMetaData::Public;
    slot_tbl[1].name = "slotFileNew()";
    slot_tbl[1].ptr = (QMember)ov1_1;
    slot_tbl_access[1] = QMetaData::Public;
    slot_tbl[2].name = "slotFileOpen()";
    slot_tbl[2].ptr = (QMember)ov1_2;
    slot_tbl_access[2] = QMetaData::Public;
    slot_tbl[3].name = "slotFileOpenRecent(const KURL&)";
    slot_tbl[3].ptr = (QMember)ov1_3;
    slot_tbl_access[3] = QMetaData::Public;
    slot_tbl[4].name = "slotFileSave()";
    slot_tbl[4].ptr = (QMember)ov1_4;
    slot_tbl_access[4] = QMetaData::Public;
    slot_tbl[5].name = "slotFileSaveAs()";
    slot_tbl[5].ptr = (QMember)ov1_5;
    slot_tbl_access[5] = QMetaData::Public;
    slot_tbl[6].name = "slotFileClose()";
    slot_tbl[6].ptr = (QMember)ov1_6;
    slot_tbl_access[6] = QMetaData::Public;
    slot_tbl[7].name = "slotFilePrint()";
    slot_tbl[7].ptr = (QMember)ov1_7;
    slot_tbl_access[7] = QMetaData::Public;
    slot_tbl[8].name = "slotFileQuit()";
    slot_tbl[8].ptr = (QMember)ov1_8;
    slot_tbl_access[8] = QMetaData::Public;
    slot_tbl[9].name = "slotEditCut()";
    slot_tbl[9].ptr = (QMember)ov1_9;
    slot_tbl_access[9] = QMetaData::Public;
    slot_tbl[10].name = "slotEditCopy()";
    slot_tbl[10].ptr = (QMember)ov1_10;
    slot_tbl_access[10] = QMetaData::Public;
    slot_tbl[11].name = "slotEditPaste()";
    slot_tbl[11].ptr = (QMember)ov1_11;
    slot_tbl_access[11] = QMetaData::Public;
    slot_tbl[12].name = "slotViewToolBar()";
    slot_tbl[12].ptr = (QMember)ov1_12;
    slot_tbl_access[12] = QMetaData::Public;
    slot_tbl[13].name = "slotViewStatusBar()";
    slot_tbl[13].ptr = (QMember)ov1_13;
    slot_tbl_access[13] = QMetaData::Public;
    slot_tbl[14].name = "slotStatusMsg(const QString&)";
    slot_tbl[14].ptr = (QMember)ov1_14;
    slot_tbl_access[14] = QMetaData::Public;
    metaObj = QMetaObject::new_metaobject(
	"CanvasApp", "KMainWindow",
	slot_tbl, 15,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    metaObj->set_slot_access( slot_tbl_access );
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    return metaObj;
}
