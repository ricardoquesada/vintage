/****************************************************************************
** CanvasView meta object code from reading C++ file 'canvasview.h'
**
** Created: Sat Jun 9 16:27:55 2001
**      by: The Qt MOC ($Id: qt/src/moc/moc.y   2.3.0   edited 2001-02-20 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#if !defined(Q_MOC_OUTPUT_REVISION)
#define Q_MOC_OUTPUT_REVISION 9
#elif Q_MOC_OUTPUT_REVISION != 9
#error "Moc format conflict - please regenerate all moc files"
#endif

#include "canvasview.h"
#include <qmetaobject.h>
#include <qapplication.h>



const char *CanvasView::className() const
{
    return "CanvasView";
}

QMetaObject *CanvasView::metaObj = 0;

void CanvasView::initMetaObject()
{
    if ( metaObj )
	return;
    if ( qstrcmp(QWidget::className(), "QWidget") != 0 )
	badSuperclassWarning("CanvasView","QWidget");
    (void) staticMetaObject();
}

#ifndef QT_NO_TRANSLATION

QString CanvasView::tr(const char* s)
{
    return qApp->translate( "CanvasView", s, 0 );
}

QString CanvasView::tr(const char* s, const char * c)
{
    return qApp->translate( "CanvasView", s, c );
}

#endif // QT_NO_TRANSLATION

QMetaObject* CanvasView::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    (void) QWidget::staticMetaObject();
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    QMetaData::Access *slot_tbl_access = 0;
    metaObj = QMetaObject::new_metaobject(
	"CanvasView", "QWidget",
	0, 0,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    metaObj->set_slot_access( slot_tbl_access );
#ifndef QT_NO_PROPERTIES
#endif // QT_NO_PROPERTIES
    return metaObj;
}
