/*	$Id: options.c,v 1.5 2001/04/10 05:46:50 gera Exp $	*/
/*
 *	WaveAccess 3500 driver
 *
 *	ricardo quesada
 *	gerardo richarte
 */
/**
 * @file options.c
 * Lee del archivo de configuracion los parametros
 */

#include <stdio.h>
#include <assert.h>
#include <time.h>

#include "wiasd.h"
#include "wias.h"

static WIAS_STATUS read_until_eol( FILE *fp )
{
	int c;
	do {
		c = getc( fp );
		if( c == EOF )
			return WIAS_STATUS_FILEERROR;
	} while( c !='\n');

	return WIAS_STATUS_SUCCESS;
}

static WIAS_STATUS get_word( FILE *fp, char *buf, int buflen )
{
	int c;
	int i;
	assert( fp );

	memset(buf,0,buflen);
	/* saca posibles comentarios y whitespaces */
again:
	c = getc( fp );
	if( c == EOF )
		return WIAS_STATUS_FILEERROR;

	if( c == '\n' || c==' ' || c == '\t')
		goto again;

	if( c == '#' ) {
		if( read_until_eol( fp ) != WIAS_STATUS_SUCCESS )
			return WIAS_STATUS_FILEERROR;
		goto again;
	}

	/* empieza el word */
	i=0;
	buf[i++] = (char) c;

	do {
		c = getc( fp );
		buf[i++] = (char) c;
		if(i >= buflen)
			break;
	} while ( c!=EOF && c!='\n' && c!=' ' && c!='\t' );

	buf[i-1]=0;

	PDEBUG("Word leido:%s\n",buf);
	return WIAS_STATUS_SUCCESS;
}

static WIAS_STATUS get_special_word( FILE *fp, char *buf, int buflen )
{
	int c;
	int i;
	assert( fp );

	memset(buf,0,buflen);

	i=0;
	do {
		c = getc( fp );
		buf[i++] = (char) c;
		if(i >= buflen)
			break;
	} while ( c!=EOF && c!='\n' && c!=' ' && c!='\t' );

	buf[i-1]=0;

	return WIAS_STATUS_SUCCESS;
}


static WIAS_STATUS get_special_value( FILE *fp, char *req, char *buf, int buflen )
{
	int c;
	char b[MAX_REGID];

	assert( fp );

	memset(buf,0,buflen);
	/* saca posibles comentarios y whitespaces */
again:
	c = getc( fp );
	if( c == EOF ) 
		return WIAS_STATUS_FILEERROR;

	if( c == '\n' || c==' ' || c == '\t')
		goto again;

	if( c == '#' ) {

		if( get_special_word(fp,b,sizeof(b)) == WIAS_STATUS_SUCCESS ) {
			if( strncasecmp(b,req,strlen(req)) == 0)
				return get_special_word(fp,buf,buflen);
		}

		if( read_until_eol( fp ) != WIAS_STATUS_SUCCESS )
			return WIAS_STATUS_FILEERROR;
		goto again;
	}
	fseek(fp,-1,SEEK_CUR);
	return WIAS_STATUS_ERROR;
}

int options_from_file(struct session *ses, char *path )
{
	FILE *fp;
	char username[MAX_USERNAME];
	char userpasswd[MAX_USERPASSWD];
	char uservpn[MAX_USERVPN];
	char ifn[IFNAMSIZ];
	char ma[MAX_MACADDR];
	char regid[MAX_REGID];

	assert(ses);
	assert(path);


	fp = fopen(path,"r");
	if( fp == NULL  )
		return WIAS_STATUS_FILEERROR;

	if( get_special_value( fp, "regid:", regid, sizeof(regid)) != WIAS_STATUS_SUCCESS ) {
		/* le pido que se registre */
		struct tm *t;
		time_t tt;
		time(&tt);
		t = localtime( &tt );
		if( (t->tm_year+1900) > 2001 || (t->tm_mon+1) >= 5) {
			fprintf(stderr,"\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			fprintf(stderr,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			fprintf(stderr,"\nREGISTRACION:\n");
			fprintf(stderr,"Usted no esta registrado como usuario Linux de Velocom.\n");
			fprintf(stderr,"Use el comando `wiasreg' para registrase. Es gratis, sencillo y rapido!\n");
			fprintf(stderr,"\nRecuerde: `wiasreg' luego de conectarse.\n\n");
			fprintf(stderr,"\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			fprintf(stderr,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			sleep(3);
		}
	} else {
		/* todo bien */
	}

	if (get_word( fp, username, sizeof(username)) != WIAS_STATUS_SUCCESS)
		goto error;
	if (get_word( fp, userpasswd, sizeof(userpasswd)) != WIAS_STATUS_SUCCESS)
		goto error;
	if (get_word( fp, uservpn, sizeof(uservpn)) != WIAS_STATUS_SUCCESS)
		goto error;
	if (get_word( fp, ifn, sizeof(ifn)) != WIAS_STATUS_SUCCESS)
		goto error;

	strncpy (ses->dev, ifn, sizeof(ses->dev));
	strncpy (ses->username, username, sizeof(ses->username));
	strncpy (ses->userpasswd, userpasswd, sizeof(ses->userpasswd));
	strncpy (ses->uservpn, uservpn, sizeof(ses->uservpn) );

	/* ahora intenta leer la mac address que es opcional */
	if( get_word( fp, ma, sizeof(ma)) == WIAS_STATUS_SUCCESS ) {
		int i;
		char *p;
		unsigned char mac[ETH_ALEN];

		p = ma;
		for(i=0;i<ETH_ALEN;i++) {
			mac[i] = strtol( p, NULL, 16 );
			p = strchr( p, ':' );
			p++;
		}
#ifdef DEBUG
		for(i=0;i<ETH_ALEN;i++) {
			printf("%.2x",mac[i]);
		}
		printf("\n");
#endif	/* DEBUG */
		memcpy( ses->dmac, mac, ETH_ALEN );
		ses->forcedmac = 1;
	}

	fclose( fp );
	return WIAS_STATUS_SUCCESS;

error:
	fclose( fp );
	return WIAS_STATUS_FILEERROR;
}
