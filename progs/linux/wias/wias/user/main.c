/*	$Id: main.c,v 1.1.1.1 2001/04/10 04:00:04 riq Exp $	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 */
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <asm/ioctl.h>
#include <unistd.h>
#include <assert.h>

#include "wiasd.h"
#include "wias.h"

struct session ses_table[N_SES];

/**
 * @fn int create_raw_socket (unsigned short type)
 */
int create_raw_socket (unsigned short type)
{

	int optval = 1, rv;

	if ((rv = socket (PF_PACKET, SOCK_RAW, htons (type))) < 0) {
		fprintf(stderr,"socket: %m");
		return -1;
	}

	if (setsockopt (rv, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) < 0) {
		fprintf(stderr,"setsockopt: %m");
		return -1;
	}

	return rv;
}

/**
 * @fn int get_hw_addr (struct session *ses) 
 */
int get_hw_addr (struct session *ses) 
{
	struct ifreq ifr;

	assert(ses);

	strncpy (ifr.ifr_name, ses->dev, sizeof (ifr.ifr_name));

	if (ioctl (ses->sock, SIOCGIFHWADDR, &ifr) < 0) {
		fprintf(stderr,"ioctl(SIOCGIFHWADDR): %m\n");
		return -1;
	}

	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
		fprintf(stderr,"interface %s is not Ethernet!\n", ses->dev);
		return -1;
	}

	memcpy (ses->smac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

	PDEBUG("we have hware address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n"
			,(unsigned char)ses->smac[0]
			,(unsigned char)ses->smac[1]
			,(unsigned char)ses->smac[2]
			,(unsigned char)ses->smac[3]
			,(unsigned char)ses->smac[4]
			,(unsigned char)ses->smac[5] );

	if (ioctl (ses->sock, SIOCGIFINDEX, &ifr) < 0) {
		fprintf(stderr,"ioctl(SIOCGIFINDEX): %m\n");
		return -1;
	}
	ses->ifindex = ifr.ifr_ifindex;
	PDEBUG("inindex: %d\n",ses->ifindex);
	return 0;
}

/**
 * @fn void ses_cleanup (struct session *ses)
 */
void ses_cleanup (struct session *ses)
{
	assert(ses);
	close (ses->sock);
}

/**
 * @fn int send_packet (struct session *ses, void *packet, int len )
 */
int send_packet (struct session *ses, void *packet, int len )
{
	struct sockaddr_ll addr;
	int c;

	assert(ses);
	assert(packet);

	memset (&addr, 0, sizeof (addr));
	/*
	 * it seems that the only useful variable is sll_ifindex.
	 * but, I fill all of them. see packet(7)
	 */
	addr.sll_family = AF_PACKET;
	addr.sll_protocol = htons( ETH_P_WIAS_CTRL);
	addr.sll_ifindex = ses->ifindex;
	addr.sll_pkttype = PACKET_HOST;
	addr.sll_halen = 0;
	addr.sll_hatype = ARPHRD_ETHER;

	if ((c = sendto (ses->sock, packet, len, 0, (struct sockaddr*)&addr, sizeof (addr))) < 0) {
		PDEBUG("sendto (send_packet): %m");
	}

	return c;
}

/**
 * @fn int ses_init (struct session *ses)
 */
WIAS_STATUS ses_init (struct session *ses)
{
	int fd;

	if ((ses->sock = create_raw_socket (ETH_P_WIAS_CTRL)) < 0) {
		fprintf(stderr,"unable to create raw socket\n");
		return WIAS_STATUS_NOPERM;
	}

	if (get_hw_addr (ses) != 0) {
		fprintf(stderr,"No pude encontrar la mac address :-(\n");
		return WIAS_STATUS_ERROR;
	}

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error in ses_init\n");
		return WIAS_STATUS_NODEV;
	}
	ioctl(fd, WIAS_SETDEV, ses->dev);
	ioctl(fd, WIAS_DELTIMER);
	close(fd);

	fd = open( "/dev/random", O_RDONLY );
	if ( fd == -1 ) {
		ses->tag = 0x5248;
	} else {
		if( read( fd, &ses->tag, sizeof(ses->tag)) != sizeof(ses->tag)) {
			ses->tag = 0x5248;
		}
		close(fd);
	}

	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn void ses_setmac( struct session *ses, char *mac )
 */
void ses_setmac( struct session *ses, char *mac )
{
	static int listo=0;
	int fd;

	if( listo )
		return;

	/* solo, si no hay que forzarla */
	if( !ses->forcedmac )
		memcpy( ses->dmac, mac, ETH_ALEN );

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error al setdesmac\n");
		return;
	}
	ioctl(fd, WIAS_ADDMACDST, ses->dmac);
	ioctl(fd, WIAS_ADDMACSRC, ses->smac);
	close(fd);
	listo=1;
}

/**
 * @fn ses_setmiscioctl( struct session *ses )
 */
void ses_setmiscioctl( struct session *ses )
{
	int fd;
	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error al setmiscioctl\n");
		return;
	}
	ioctl(fd, WIAS_ADDTIMER);
	ioctl(fd, WIAS_INITSEQ);
	close(fd);
}

/**
 * @fn void ses_default( struct session *ses )
 */
WIAS_STATUS ses_default( struct session *ses )
{
	assert(ses);

	ses->sock = -1;
	ses->opt_debug = 0;
	ses->opt_daemonize = 1;
	ses->forcedmac = 0;
	ses->log_to_fd = fileno (stdout);
	memcpy( ses->dmac, MAC_BCAST_ADDR, ETH_ALEN );
	memcpy( ses->smac, MAC_BCAST_ADDR, ETH_ALEN );

	if (options_from_file(ses,WIAS_OPTIONS_FILE) != WIAS_STATUS_SUCCESS ) {
		fprintf(stderr,"Error while trying to read file %s\n",WIAS_OPTIONS_FILE);
		return WIAS_STATUS_FILEERROR;
	}
	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn int OpenSap()
 */
WIAS_STATUS OpenSap( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	fprintf(stderr,"\nOpenning SAP...");
	wias_send_open_sap_request( ses ,"\x00\x00\x00\x00\xae\xe0\xa3\x91");
	s = wias_get_open_sap_confirm( ses );
	if (s!= WIAS_STATUS_SUCCESS)
		return WIAS_STATUS_ERROR;
	fprintf(stderr,"OK (SAP=%d)\n",ses->SAP);
	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn WIAS_STATUS Disonnect( struct session *ses )
 */
WIAS_STATUS Disconnect( struct session *ses )
{
	char buf[MAX_PACKET];
	int len=sizeof(buf);
	WIAS_STATUS s=WIAS_STATUS_ERROR;
	fd_set rfds;
	struct timeval timeout;
	uchar reason;

	assert(ses);

	fprintf(stderr,"\nDisconnecting...");
	wias_send_disconnect_indication( ses, 0 );
	wias_send_disconnect_response( ses, 0 );
	wias_send_force_disconnect( ses );

	FD_ZERO(&rfds);
	FD_SET(ses->sock,&rfds);
	timeout.tv_sec=1;
	timeout.tv_usec=0;

	switch (select(ses->sock+1,&rfds,NULL,NULL,&timeout)) {
		case	0:	// time out
			wias_send_close_sap_request(ses);
			wias_get_packet( ses, buf, &len );
			usleep(5000);
			return WIAS_STATUS_TIMEOUT;
			break;
		case	-1:	// error
			break;
		default:	// something set
			s = wias_get_disconnect_indication( ses, &reason );
			fprintf(stderr," (%x)",s);
			switch (s) {
				case	WIAS_STATUS_SUCCESS:
					wias_send_disconnect_response( ses, reason );
					wias_send_close_sap_request(ses);
					wias_get_packet( ses, buf, &len );
					usleep(5000);
					break;
				default:		// evitar warnings en la compilacion
					/* mmm */
					break;
			}
	}
	return s;
}

/**
 * @fn WIAS_STATUS Connect( struct session *ses )
 */
WIAS_STATUS Connect( struct session *ses )
{
	WIAS_STATUS s=WIAS_STATUS_ERROR;
	fd_set rfds;
	struct timeval timeout;

	assert(ses);

	fprintf(stderr,"\nConnecting...");
	wias_send_connect_request( ses );

	FD_ZERO(&rfds);
	FD_SET(ses->sock,&rfds);
	timeout.tv_sec=1;
	timeout.tv_usec=0;

	switch (select(ses->sock+1,&rfds,NULL,NULL,&timeout)) {
		case	0:	// time out
			Disconnect(ses);
			return WIAS_STATUS_TIMEOUT;
			break;
		case	-1:	// error
			break;
		default:	// something set
			s = wias_get_connect_confirm( ses );
			fprintf(stderr," (%x)",s);
			switch (s) {
				case	WIAS_STATUS_BADLOGIN:
					fprintf(stderr,"\nInvalid username or passwrd\n");
					exit(1);
				case	WIAS_STATUS_BADVPN:
					fprintf(stderr,"\nInvalid VPN number\n");
					exit(2);
				case	WIAS_STATUS_BADTYPE:
					fprintf(stderr," packet out of sequence");
					break;
				case	WIAS_STATUS_TIMEOUT:
					fprintf(stderr,"\n timeout received. Forcing a full retry\n");
					break;
				default:
					break;
			}
	}
	if( s != WIAS_STATUS_SUCCESS )
	Disconnect(ses);
	return s;
}




int main (int argc, char **argv)
{
	char buf[MAX_PACKET];
	int len=sizeof(buf);

	struct session *ses = &ses_table[0];

	fprintf(stderr,"Wias driver v"WIAS_VERSION" - Version DEMO para Velocom\n");

	memset(ses,0,sizeof(struct session));

	if( ses_default( ses ) != WIAS_STATUS_SUCCESS )
		return -1;

	if( ses_init( ses ) != WIAS_STATUS_SUCCESS )
		return -2;

	ses->SAP=1;
	do {
		int i;
		usleep(5000);
	
		for(i=0;i<3;i++) {
			wias_send_discover_request( ses );
			if (wias_get_packet( ses, buf, &len ) != WIAS_STATUS_SUCCESS)
				continue;
			ses_setmac( ses, &buf[6] );
			usleep(5000);

		}

		wias_send_link_status_request( ses );
		if (wias_get_packet( ses, buf, &len ) != WIAS_STATUS_SUCCESS)
			continue;
		usleep(5000);

#ifdef PARANOIC
		wias_send_close_sap_request(ses);
		if (wias_get_packet( ses, buf, &len ) != WIAS_STATUS_SUCCESS)
			continue;
		usleep(5000);
#endif /* PARANOIC */

		if (OpenSap(ses) != WIAS_STATUS_SUCCESS)
			continue;
		usleep(5000);

	} while (Connect(ses) != WIAS_STATUS_SUCCESS);

	ses_setmiscioctl( ses );

	exit(0);
}
