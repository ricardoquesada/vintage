/*	$Id: wreset.c,v 1.1.1.1 2001/04/10 04:00:05 riq Exp $	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <asm/ioctl.h>
#include <unistd.h>
#include <assert.h>

#include "wiasd.h"
#include "wias.h"

struct session ses_table[N_SES];

/**
 * @fn int create_raw_socket (unsigned short type)
 */
int create_raw_socket (unsigned short type)
{
	int optval = 1, rv;

	if ((rv = socket (PF_INET, SOCK_PACKET, htons (type))) < 0) {
		printf("socket: %m");
		return -1;
	}

	if (setsockopt (rv, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) < 0) {
		printf("setsockopt: %m");
		return -1;
	}

	return rv;
}

/**
 * @fn int get_hw_addr (struct session *ses) 
 */
int get_hw_addr (struct session *ses) 
{
	struct ifreq ifr;

	assert(ses);

	strncpy (ifr.ifr_name, ses->dev, sizeof (ifr.ifr_name));

	if (ioctl (ses->sock, SIOCGIFHWADDR, &ifr) < 0) {
		printf("ioctl(SIOCGIFHWADDR): %m\n");
		return -1;
	}

	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
		printf("interface %s is not Ethernet!\n", ses->dev);
		return -1;
	}

	memcpy (ses->smac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

	PDEBUG("we have hware address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n"
			,(unsigned char)ses->smac[0]
			,(unsigned char)ses->smac[1]
			,(unsigned char)ses->smac[2]
			,(unsigned char)ses->smac[3]
			,(unsigned char)ses->smac[4]
			,(unsigned char)ses->smac[5] );

	return 0;
}

/**
 * @fn void ses_cleanup (struct session *ses)
 */
void ses_cleanup (struct session *ses)
{
	assert(ses);
	close (ses->sock);
}

/**
 * @fn int send_packet (struct session *ses, void *packet, int len )
 */
int send_packet (struct session *ses, void *packet, int len )
{
	struct sockaddr addr;
	int c;

	assert(ses);
	assert(packet);

	memset (&addr, 0, sizeof (addr));
	strcpy (addr.sa_data, ses->dev);

	if ((c = sendto (ses->sock, packet, len, 0, &addr, sizeof (addr))) < 0) {
		PDEBUG("sendto (send_packet): %m");
	}

	return c;
}

/**
 * @fn int ses_init (struct session *ses)
 */
WIAS_STATUS ses_init (struct session *ses)
{
	int fd;

	if ((ses->sock = create_raw_socket (ETH_P_WIAS_CTRL)) < 0) {
		printf("unable to create raw socket\n");
		return WIAS_STATUS_NOPERM;
	}

	if (get_hw_addr (ses) != 0) {
		printf("No pude encontrar la mac address :-(\n");
		return WIAS_STATUS_ERROR;
	}

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error in ses_init\n");
		return WIAS_STATUS_NODEV;
	}
	ioctl(fd, WIAS_SETDEV, ses->dev);
	ioctl(fd, WIAS_DELTIMER);
	close(fd);

	fd = open( "/dev/random", O_RDONLY );
	if ( fd == -1 ) {
		ses->tag = 0x5248;
	} else {
		if( read( fd, &ses->tag, sizeof(ses->tag)) != sizeof(ses->tag)) {
			ses->tag = 0x5248;
		}
		close(fd);
	}

	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn void ses_default( struct session *ses )
 */
WIAS_STATUS ses_default( struct session *ses )
{
	assert(ses);

	ses->sock = -1;
	ses->opt_debug = 0;
	ses->opt_daemonize = 1;
	ses->log_to_fd = fileno (stdout);
	memcpy( ses->dmac, MAC_BCAST_ADDR, ETH_ALEN );
	memcpy( ses->smac, MAC_BCAST_ADDR, ETH_ALEN );

	if (options_from_file(ses,WIAS_OPTIONS_FILE) != WIAS_STATUS_SUCCESS ) {
		printf("Error while trying to read file %s\n",WIAS_OPTIONS_FILE);
		return WIAS_STATUS_FILEERROR;
	}
	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn int OpenSap()
 */
int OpenSap( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		printf("\nOpenning SAP...");
		wias_send_open_sap_request( ses ,"\x00\x00\x00\x00\xae\xe0\xa3\x91");
		s = wias_get_open_sap_confirm( ses );
	} while(s!= WIAS_STATUS_SUCCESS);
	printf("OK (SAP=%d)\n",ses->SAP);
	return 0;
}

/**
 * @fn int Connect( struct session *ses )
 */
int Connect( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		printf("\nConnecting...");
		wias_send_connect_request( ses );
		s = wias_get_connect_confirm( ses );
		if( s == WIAS_STATUS_BADLOGIN ) {
			printf("\nInvalid username or passwrd\n");
			exit(1);
		} else if ( s == WIAS_STATUS_BADVPN ) {
			printf("\nInvalid VPN number\n");
			exit(2);
		}
	} while (s!=WIAS_STATUS_SUCCESS);
	printf("OK\n");

	return 0;
}


int main (int argc, char **argv)
{
	struct session *ses = &ses_table[0];

	printf("wias statistics v"WIAS_VERSION"\n\n");

	memset(ses,0,sizeof(struct session));

	if( ses_default( ses ) != WIAS_STATUS_SUCCESS )
		return -1;

	if( ses_init( ses ) != WIAS_STATUS_SUCCESS )
		return -2;

	wias_send_reset_request( ses );
	return 0;
}
