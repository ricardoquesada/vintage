/*	$Id: wias.h,v 1.1.1.1 2001/04/10 04:00:03 riq Exp $	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 *
 */
/* WIAS protocol structures, defines and flow */
/* Flow:
 * 
 * 	Discover Request 	Host --> Modem
 * 	Discover Confirm 	Host <-- Modem
 * 	Open SAP Request	Host --> Modem
 * 	Open SAP Confirm	Host <-- Modem
 * 	Connect Request		Host --> Modem
 * 	Connect Confirm		Host <-- Modem
 *
 * 	Link Status Request	Host --> Modem
 * 	Link Status Confirm	Host <-- Modem
 *
 * 	Close SAP Request	Host --> Modem
 * 	Disconnect Indication	Host <-- Modem
 * 	Close SAP Confirm	Host <-- Modem
 *
 */

#ifndef __WIAS_H
#define __WIAS_H

#pragma pack(1)

#define WIAS_VERSION	"0.5.1"

#define ETH_P_WIAS_CTRL	(0x8866)		/**< wias control channel */
#define ETH_P_WIAS_DATA	(0x8867)		/**< wias data channel */

/* #define WIAS_TAG	(0x5247) */		/**< RG */

#define N_SES 1					/**< number of sessions */

// types are ushort in network byte ordering
#define WIAS_TYPE_DISCOVER_REQUEST	0x00
#define WIAS_TYPE_DISCOVER_CONFIRM	0x01

#define WIAS_TYPE_OPEN_SAP_REQUEST	0x02
#define WIAS_TYPE_OPEN_SAP_CONFIRM	0x03

#define WIAS_TYPE_CLOSE_SAP_REQUEST	0x04
#define WIAS_TYPE_CLOSE_SAP_CONFIRM	0x05

#define WIAS_TYPE_ECHO_SAP_REQUEST	0x06		// modem to host
#define WIAS_TYPE_ECHO_SAP_CONFIRM	0x07

#define WIAS_TYPE_CONNECT_REQUEST	0x08
#define WIAS_TYPE_CONNECT_INDICATION	0x09
#define WIAS_TYPE_CONNECT_RESPONSE	0x0A
#define WIAS_TYPE_CONNECT_CONFIRM	0x0B

#define WIAS_TYPE_DISCONNECT_REQUEST	0x0C
#define WIAS_TYPE_DISCONNECT_CONFIRM	0x0D

#define WIAS_TYPE_DISCONNECT_INDICATION	0x0E
#define WIAS_TYPE_DISCONNECT_RESPONSE	0x0F

#define WIAS_TYPE_PROVIDER_DISCONNECT_INDICATION	0x10

#define WIAS_TYPE_CONNECT_STATUS_REQUEST	0x11
#define WIAS_TYPE_CONNECT_STATUS_CONFIRM	0x12

#define WIAS_TYPE_FLOW_CONTROL_REQUEST	0x13
#define WIAS_TYPE_FLOW_CONTROL_INDICATION	0x14

#define WIAS_TYPE_LINK_STATUS_REQUEST	0x15
#define WIAS_TYPE_LINK_STATUS_CONFIRM	0x16

#define WIAS_TYPE_LINK_STATUS_INDICATION	0x17

#define WIAS_TYPE_CONNECTION_UPDATE_REQUEST	0x18
#define WIAS_TYPE_CONNECTION_UPDATE_CONFIRM	0x19
#define WIAS_TYPE_CONNECTION_UPDATE_INDICATION	0x1A
#define WIAS_TYPE_CONNECTION_UPDATE_RESPONSE	0x1B

#define WIAS_TYPE_RESET_REQUEST		0x1C
#define WIAS_TYPE_RESET_INDICATION	0x1D

#define WIAS_TYPE_DISCONNECT_GROUP_INDICATION	0x1E
#define WIAS_TYPE_DISCONNECT_GROUP_RESPONSE	0x1F

#define WIAS_TYPE_PING_WIRELESS_MODEM_REQUEST	0x20
#define WIAS_TYPE_PING_WIRELESS_MODEM_CONFIRM	0x21

enum {
	WIAS_CONNECTION_STATUS_OK = 0x00,
	WIAS_CONNECTION_STATUS_BADLOGIN = 0xd7,
	WIAS_CONNECTION_STATUS_BADVPN = 0xd8,
	WIAS_CONNECTION_STATUS_TIMEOUT1 = 0xf4,
	WIAS_CONNECTION_STATUS_TIMEOUT2 = 0xf5,
};

#define uchar	unsigned char
#define ushort	unsigned short
#define uint	unsigned int

typedef enum {
	WIAS_STATUS_SUCCESS,
	WIAS_STATUS_BADTYPE,
	WIAS_STATUS_NOMEM,
	WIAS_STATUS_BADIOCTL,
	WIAS_STATUS_BADPARAM,
	WIAS_STATUS_BADLEN,
	WIAS_STATUS_BADSAP,
	WIAS_STATUS_BADLOGIN,
	WIAS_STATUS_BADVPN,
	WIAS_STATUS_UNEXPECTED,
	WIAS_STATUS_FILEERROR,
	WIAS_STATUS_TIMEOUT,
	WIAS_STATUS_ERROR,
	WIAS_STATUS_NOPERM,
	WIAS_STATUS_NODEV,
} WIAS_STATUS, *PWIAS_STATUS;

struct wias_hdr {
	struct ethhdr ethhdr;
	ushort type;
};

struct wias_discover_request {
	struct wias_hdr hdr;
	ushort request_tag;
	char zero[136];
};

struct version {
	ushort mayor;	// Network byte ordering
	ushort minor;
};

struct wias_version_element {
	uchar MAC_provider_type;
	uchar reserved[3];
	struct version current_v;	
	char current_bd[16];		// Current Version Build Date
	struct version configuration_v;
	struct version SNMP_agent_v;
	struct version version_0_v;
	char version_0_bd[16];		// Version Zero Build Date
	struct version flash_B_v;
	char flash_B_bd[16];		// Flash B Build Date
	struct version XWD_interface_v;
	struct version MAC_wireles_v;
	struct version MAC_hwd_brd_v;
	uint MAC_hwd_serial;
	struct version physical_hwd_brd_v;
	uint physical_hwd_serial;
};

struct wias_discover_confirm {
	struct wias_hdr hdr;
	ushort request_tag;
	struct wias_version_element version;
	uint TOS;			// Type Of Service
	ushort open_SAPs;		// Number of open SAPs
	ushort max_open_SAPs;		// Maximum
	ushort open_connections;	// Number of open connections
	ushort max_open_connections;	// its maximum
	uchar state;
	uchar too_many_connections;	// Boolean
	uchar access_pri_threshold;
	ushort access_pri_classes;	// Number of Access Priority Classes
	char padding[3];
};

struct wias_open_SAP_request {
	struct wias_hdr hdr;
	char padding[2];
	struct version MAC_iface_v;
	char MAC_user_id[8];
	char padding_2[16];
};

struct wias_open_SAP_confirm {
	struct wias_hdr hdr;
	char padding[2];
	struct version MAC_iface_v;
	char MAC_user_id[8];
	struct version PHY_v;
	struct version MAC_v;
	uchar response;			// Response code
	uchar SAP;
	uchar too_many_coonections;	// boolean
	ushort access_pri_classes;	// Number of Access Priority Classes
	uchar access_pri_threshold;
	char padding_2[2];
};

struct wias_connection_cookie {
	uchar modem_port;
	ushort hub_port;
};
	
struct wias_QOS {			// Quality of Service
	uint TTL;			// Time to Live (mS)
	uchar service_access_pri;
	char names_and_names[45];
};

struct wias_connection_element {
	uchar end_system_channel_id;
	struct wias_connection_cookie connection_cookie;
	uchar status;
	uchar reserved;
	struct wias_QOS qos;
};

struct wias_connect_request {
	struct wias_hdr hdr;
	uchar SAP;
	char reserverd[3];
	uchar group_id;
	uchar elements_in_conn_group;	// Number of Elements in Connection Group
	struct wias_connection_element connection_elements[1];
	ushort user_data_len;
	char user_data[0];
};

struct wias_connect_confirm {
	struct wias_hdr hdr;
	uchar SAP;
	char reserverd[3];
	uchar group_id;
	uchar elements_in_conn_group;	// Number of Elements in Connection Group
	struct wias_connection_element connection_elements[1];
	ushort user_data_len;
	char user_data[0];
};

struct wias_txrx_counters {
	ulong success_tx_QAM;
	ulong fail_tx_QAM;
	ulong success_tx_QPSK;
	ulong fail_tx_QPSK;
	unsigned long long success_rx_QAM;
	unsigned long long success_rx_QPSK;
//	ulong wire_tx;
//	ulong wire_rx;
};

struct wias_link_capa_element  {
	uchar max_frag_size_QAM;
	uchar max_frag_size_QPSK;
	uchar user_data[6];
};

struct wias_link_element {
	ushort ESSID;
	ushort BSSID;
	char challenge_seq_id;
	char reserved0[3];

	struct wias_txrx_counters txrx_c;

//	char drop_retry;
//	char drop_aging;
	ulong tx_contentions;
	ulong success_tx_contentions;
	ulong associate_requests;
	ulong associate_responses;
	ulong beacons;
	ulong beacons_missed;

	ulong wire_tx;				// out of order
	ulong wire_rx;				// out of order

	ulong system_uptime;			// centisegs (ms /10)
	ulong link_uptime;			// centisegs (ms /10)

	struct wias_link_capa_element lce;
	uchar link_status_flags;		// bitmaped
	uchar delay;
	uchar signal_strength_AP;		// dB
	uchar signal_strength_WM;		// dB
	uchar quality_AP;
	uchar quality_WM;
	uchar power_set;			// dBm
	uchar power_measure;			// dBm
};

struct wias_ismrf_link_element_2_4 {
	uchar country;
	uchar antenna_type;
	uchar cable_attenuation;			// dB
	uchar reserved;
};

struct wias_link_status_request {
	struct wias_hdr hdr;
	uchar req_option;			// request option
	char reserved0;
	char reserved1[0x60];
	ushort req_tag;				// request tag
	char reserved2[2];
};

struct wias_link_status_confirm {
	struct wias_hdr hdr;
	char reserved0[2];
	struct wias_link_element le;
	ushort req_tag;				// request tag
	ushort reserved1[2];
	struct wias_ismrf_link_element_2_4 ism;	// 2.4 Ghz ruggedized ISC radio only
	char hidden_data[10];
};

struct wias_close_SAP_request {
	struct wias_hdr hdr;
	uchar SAP;
	char padding;
	uchar zero[0x2A];
};

struct wias_disconnect_indication {
	struct wias_hdr hdr;
	uchar SAP;
	uchar reason;			// Reason for disconnecting
	uchar zero[0x2A];		// TODO Complete
};

struct wias_close_SAP_confirm {
	struct wias_hdr hdr;
	uchar response_code;
	uchar SAP;
	uchar zero[0x2A];
};

struct wias_reset_request {
	struct wias_hdr hdr;
	uchar hidden_data[0x2C];
};

/* XXX this is a session struct (not discovery) */
struct wias_echo_request {
	struct wias_hdr hdr;
	uchar data[0x2C];
};

struct wias_echo_reply {
	struct wias_hdr hdr;
	uchar data[0x2C];
};

struct wias_force_disconnect {
	struct wias_hdr hdr;
	uchar data[0x2C];
};

#ifndef __KERNEL__
void wias_send_reset_request(struct session *ses);
void wias_send_discover_request(struct session *ses);
WIAS_STATUS wias_get_packet( struct session *ses, char *ptr, int *ptrlen );
void wias_send_open_sap_request( struct session *ses,char *user_id);
int wias_send_close_sap_request( struct session *ses );
int wias_send_connect_request( struct session *ses );
void wias_send_link_status_request( struct session *ses );
void wias_send_echo_request( struct session *ses );
WIAS_STATUS wias_get_open_sap_confirm( struct session *ses );
WIAS_STATUS wias_get_connect_confirm( struct session *ses );
WIAS_STATUS wias_get_echo_replay( struct session *ses );
WIAS_STATUS wias_get_link_status_confirm( struct session *ses, void *buf, int *len );
WIAS_STATUS wias_get_disconnect_indication( struct session *ses, uchar *reason );
void wias_send_force_disconnect( struct session *ses );
void wias_send_disconnect_response( struct session *ses, uchar reason );
void wias_send_disconnect_indication( struct session *ses, uchar reason );

void wias_send_X( struct session *ses, ushort type );
#endif


/* ioctls */
#define  WIAS_ADDMACDST _IOW('t',128,char *)
#define  WIAS_ADDMACSRC _IOW('t',129,char *)
#define  WIAS_SETDEV 	_IOW('t',130,char *)
#define  WIAS_DELDEV 	_IOW('t',131,char *)
#define  WIAS_ADDTIMER	_IOW('t',132,char *)
#define  WIAS_INITSEQ	_IOW('t',133,char *)
#define  WIAS_DELTIMER	_IOW('t',134,char *)


/* kernel mode */
#ifdef __KERNEL__
#undef DEBUG_PDEBUG
#ifdef DEBUG_PDEBUG
#define PDEBUG(a...) printk(a)
#else
#define PDEBUG(a...)
#endif

#define WIASOE_TYPE 1

#define WIASOE_HDR_LEN  sizeof(struct wiasoe_hdr)

#define CLOSED 0
#define OPENED 1

#define WIAS_COMMAND_MASK	(0xF000)
#define WIAS_MSG_MODEM_REQUEST	(0x8000)
#define WIAS_MSG_MODEM_REPLY	(0x4000)
#define WIAS_MSG_ECHO_REQUEST	(0x5000)
#define WIAS_MSG_ECHO_REPLY	(0x9000)
#define WIAS_LEN_MASK		(~WIAS_COMMAND_MASK)
struct wiasoe_hdr {
	__u16	msg_len;
	__u16	seq;
}__attribute__ ((packed));

struct wiasoe_ether {
	struct ethhdr ether;
	struct wiasoe_hdr wiasoe;
} __attribute__ ((packed));

struct wiasoe_struct {
        int magic;
        struct tty_struct *tty;
	struct device *dev;
	struct wiasoe_ether wiasoe_hdr;
	int state;
	int counter;
	int seq;
};

#endif /* __KERNEL__ */

#endif  /* __WIAS_H */
