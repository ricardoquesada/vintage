# MODULE client description file example
#
[ModuleInfo]
Game = Tenes Empanadas Graciela
Author = Ricardo Quesada
# CommandLine path is figured relative to GameDir unless starting with /
CommandLine = /usr/local/bin/tegclient --ggz
Frontend = gtk
Homepage = http://teg.sourceforge.net
Name = teg
Protocol = 0.0.6
Version = 0.0.6
