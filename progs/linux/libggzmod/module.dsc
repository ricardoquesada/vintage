# MODULE server description file example
#
Name = teg 
Version = 0.0.6
Description = A clone of a RISK clone.
Author = Ricardo Quesada
Homepage = http://teg.sourceforge.net
# PlayersAllowed and BotsAllowed should be repeated as many times as
# necessary to specify the valid distinct options which can appear
PlayersAllowed = 2
PlayersAllowed = 3
PlayersAllowed = 4
PlayersAllowed = 5
PlayersAllowed = 6
BotsAllowed = 0
BotsAllowed = 1
BotsAllowed = 2
BotsAllowed = 3
BotsAllowed = 4
BotsAllowed = 5
# Set AllowLeave to 1 if game supports players leaving during gameplay
AllowLeave = 1
# ExecutablePath is figured relative to GameDir unless starting with /
ExecutablePath = /usr/local/bin/tegserver --ggz
# GameDisabled is a quick way to turn off the game if necessary
#GameDisabled
