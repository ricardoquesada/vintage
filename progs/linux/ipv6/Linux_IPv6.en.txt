Linux, IPv6 and a little bit of confusion :-)
riq (rquesada@pjn.gov.ar)
v0.3.2 March of 1999


   1st PART - THE BEGINNING
   ------------------------

0. Preface, disclaimer, etc, etc.
---------------------------------
This document is thought for those people who want to put IPv6 in his 
glibc2.1/Linux machine. I assume that those people, know how to compile the 
kernel, read a Makefile, patch a program, has some knowlegde of networkning,
and etc, etc.
This document is very specific for glibc2.1, although people with a non-glibc2.1
system can get some ideas.
Anyway, I do recommed to read the Bieringer's IPv6-Howto. (See appendix B)


All my tests were done with:
	Debian 2.0r3 + kernel 2.2.3 + glibc2.1
	RedHat 5.2 + kernel 2.2.3 + glibc2.1

NOTE: There are lot of differences between some pre-glibc2.1 and the real
glibc-2.1... so use glibc2.1 :-)
Eg: glibc-2.0.109 is a pre-glibc-2.1 that differs a lot from glibc2.1.0

NOTE II: All of the mentioned files, patchs & documents can be donwloaded
from the sites that appears in the appendix B.


1. Kernel configuration 
-----------------------
Get the kernel v2.2, config it with IPv6 support & optionally, with
tunnel support.

In  "Code maturity level options" put:
	[*] Prompt for development and/or incomplete code/drivers

In "Networking options" put:
	<*> The IPv6 protocol (EXPERIMENTAL)
	[*] IPv6: enable EUI-64 token format

	<*> IP: tunneling		<- I think this one too.

("IPv6: disable provider based addresses" is optional, but I think you
should enable it.)


2. glibc 2.1.0
--------------
Get the the glibc-2.1:
	If you are brave and "macho" compile it by yourself...
	or if you are a coward (like me :-)) download the binaries & the
	headers. (See appendix B)


3. Net-tools
------------
This package includes:
	ifconfig, route, netstat and those kind of things.
Well... if you download a recent version you could have IPv6 with them.

It's very easy to compile them:
make config ; make ; make install

Be sure to answer "yes" for the IPv6 support :-)


4. iproute2
-----------
This package has the same functionality of the net-tools, but with other things
very interesting. It's worth a look.

It has no problem at compilation time:
	make
but there is no "make install"... so copy the "ip" command to you favourite
directory (eg: /sbin ).


5. IPv6 numbers
---------------
The IPv6 numbers are 128-bits long, so they are very difficult to remember :-(.
They are split in 8 chunks of 16 bits each, written in hexadecimal, separated by":".

If you have done the previous steps, you should have some IPv6 assigned numbers.
Just type:
	ifconfig
	or
	ip addr
[If you compile the IPv6 support as a module, type "modprobe ipv6"]

Outputs:
[root@homero ~]# ifconfig
lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host

eth0      Link encap:Ethernet  HWaddr 00:00:21:A7:21:21  
          inet addr:200.9.243.11  Bcast:200.9.243.15  Mask:255.255.255.240
          inet6 addr: fe80::200:21ff:fea7:2121/10 Scope:Link

[root@homero ~]# ip addr
1: lo: <LOOPBACK,UP> mtu 3924 qdisc noqueue
    link/LOOPBACK 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 brd 127.255.255.255 scope host lo
    inet6 ::1/128 scope host 

2: eth0: <BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast
    link/ETHER 00:00:21:a7:21:21 brd ff:ff:ff:ff:ff:ff
    inet 200.9.243.11/28 brd 200.9.243.15 scope global eth0
    inet6 fe80::200:21ff:fea7:2121/10 scope link 

As you can see, the outputs are very similar, and my IPv6-loopback number is:
	::1
The /128 is the mask (remeber that the number are 128-bits long).
Note that the scope is "host" (pay attention to the mask!). 

My other IPv6 number is (from my eth0):
	fe80::200:21ff:fea7:2121
and has "link" as scope.


These numbers are the same:
	my loopback		my eth0 number
	(scope "host")		(scope "link")
	-------------------   ---------------------------------- 
	| ::1             |   | fe80::200:21ff:fea7:2121      |
	| 0:0:0:0:0:0:0:1 |   | fe80:0:0:0:200:21ff:fea7:2121 | 
	| 0::1            |   | fe80:0::0:200:21ff:fea7:2121  |
	| 0:0::0:1        |   | fe80::0:0:200:21ff:fea7:2121  |
	-------------------   ----------------------------------
Do you get the idea?

The IPv6 numbers has certain scope according to their first bits.
See appendix A.


6. inet6-apps
-------------
The inet6-apps is a package that contains basic applications like:
ping, inetd, ftp, ftpd, finger, fingerd and some more.

Versions prior to 0.36 are intended to be used with glibc2.0 or prior.
And versions from 0.36 are intended to be used with glibc2.1... Notice that
you can compile v0.35 with glibc2.1, but it is not easy.


Well, the inet6-apps includes an IPv6 library, but I had some problems with
that library (specially with the resolver). So, the idea is to use the
functions from the glibc2.1.
Apply my patch (this patch also modifies the source code of some apps): 
	cd /usr/src
	tar xzvf inet6-apps-0.36.tar.gz
	gzip -cd inet6-apps-0.36.patch.0.2.gz | patch -p0

and then:
	cd inet6-apps-0.36
	make ; make install

The "make install" will replace the /etc/hosts & /etc/protocols files.
The binaries should be in /usr/inet6/bin.

Well, if all went OK, you can ping yourself ("::1" is your loopback):
[root@homero ~]# /usr/inet6/bin/ping ::1
PING ::1 (::1): 56 data bytes
64 bytes from ::1: icmp_seq=0 ttl=64 time=0.282 ms
64 bytes from ::1: icmp_seq=1 ttl=64 time=0.166 ms
64 bytes from ::1: icmp_seq=2 ttl=64 time=0.14 ms
64 bytes from ::1: icmp_seq=3 ttl=64 time=0.161 ms


7. telnet
---------
No patch is needed to compile this package.
make ; make install
and you'll have telned & telnetd in /usr/inet6/bin

Note: In my debian 2.0r3 I had to do this:
cd /lib; ln -s libtermcap.so.2 libtermcap.so
I should have had something wrong there.

If you can't telnet or ftp to ::1, update your /etc/inetd.conf. Hint: Don't
use wrappers, and remember to start your new inetd daemon. (kill -9 the old
one & then start the new one (/usr/inet6/inetd) ).


8. traceroute, tcpdump, libpcap
-------------------------------
1) The lastest version is "traceroute-1.4a5+ipv6-1.tar.gz", and I think it is
not prepared for the inet6-apps-0.36.
Note: This package compiles well with inet6-apps-0.35.

So, if you are using inet6-apps-0.36 apply my patch:
	gzip -dc traceroute.patch.gz | patch -p0
	cd traceroute
	make ; make install

2) The libpcap libraries compiles OK... but be careful with SO_ATTACH_FILTER.
The SO_ATTACH_FILTER is a socket option that enables you to attach a certain
filter to a socket.
If you want this, you should enable "CONFIG_FILTER" in the kernel.
Anyway, I had some erros with "CONFIG_FILTER" { FIXME: Find out why }.
So, if you have the same problem, apply my patch.
Notice that this is a library, and you'll notice this error when you run a
program that is linked against this library (eg: tcpdump).

3) tcpdump: In this program, you should also apply my patchs. As always...
	cd /usr/src
	gzip -dc tcpdump.patch.gz | patch -p0
	cd tcpdump
	make ; make install


Note: The patchs of traceroute & tcpdump asumes that the SOURCES of the 
inet6-apps-0.36 are in /usr/src. If they are not there you'll get an ERROR.

Note II: These patchs, also asume that you have installed the patched version
of the inet6-apps-0.36. Why? Because they need the patched version of 
GNUmakefile.config & GNUmakefile.inc.



   2nd PART - BACKGROUND, CONFIGURATION, ETC. 
   ------------------------------------------

9. Configuration
----------------
Well, if you have 2 machines in your network you could start playing with
your automatically asigned IPv6 numbers (the "scope link" numbers - see 
appendix B ).
But if you want another number type:
	ip addr add 3ffe:a000::1/64 dev eth0
	or	
	ifconfig eth0 add 3ffe:a000::1/64

and in the other machine put something like this:
	
	ip addr add 3ffe:a000::2/64 dev eth0

Now you could try the compiled packages (telnet, ftp, ping, etc).
Use tcpdump like this:
	tcpdump ip proto IPv6
to watch the IPv6 packets.


10. A brief description of the 6bone
------------------------------------
The 6bone is a network of IPv6 machines that connects to each other using
internet, experimenting this new protocol. I think this network is running
since 1996, and since then, more and more machines are joining the 6bone.

Most of the routers that are running today, only understand IPv4, so you need
to build tunnels between the IPv6 machines. This is done, by encapsulating the
IPv6 packets in IPv4.

Argentina is connected to the 6bone since march'99 thanks to Horacio Pe�a.
If you are in Argentina, and want to join the 6bone e-mail him:
	horape@compendium.com.ar


11. Tunnels
-----------
Once you have your machines running with IPv6, it's time to build tunnels...

1)	ip tunnel add my_tunnel remote 10.2.2.4 mode sit ttl 64
2)	ip link set my_tunnel up
3)	ip addr add 3ffe:a001::1 dev my_tunnel
4)	ip route add 3ffe::/16 dev my_tunnel

The first line tells me which is the IPv4 number of the other side of the tunnel
, and also I "create" a new interface: my_tunnel.
In the next line I set the new interface up.
Then, in the 3rd line, I assign an IPv6 number to that interface...
...and finally a default route is created.

[That is an example of a 6bone-script]

12. Router advertisement 
------------------------
The protocol IPv6 has some new features, and one is the "router advertisement".
In Linux, the program that uses this feature is called "radvd". 
Radvd has more or less the same functionality of DHCPD (dinamic host
configuration protocol daemon) but with more options.

The radvd runs in a machine, and publish in its network the prefix.
If we continue with our example, our prefix would be:
		3ffe:a000::/64
and all the machines in our IPv6 network would get that message, and their
first 64 bits would be: 3ffe:a000::/64. The lastest 64 bits would be a mix
of ther 48-bit MAC address. (Newer MAC address (I believe) has 64 bits).

So, to configure all the machines in our IPv6 network, we only need to config
our radvd... Cool :-) Those machines would get their IPv6 numbers, routes,
TTL of their number, automatically.
Note: You don't need an "radvd" client, since this is part of the IPv6 protocol.
All the IPv6 stacks should support this feature (Linux has it).

Eg:
Machine A with radvd                  Machine B. MAC: 00:00:21:a7:21:21
1. Publish the prefix      -->        2. Get the prefix.
    3ffe:a000::/64         RA         3. The lastest 64 bits are filled
					 with your 48-bit MAC address.
                                      4. Routes and other things are created.
                                                 Prefix + MAC=
                                         3ffe:a000::200:21ff:fea7:2121/64

This programs can run in your IPv6 router, but remember that you must tell
the kernel to forward your IPv6 packets (like in IPv4).
Eg:
	echo "1" > /proc/sys/net/ipv6/conf/all/forwarding

Notice: If you are using the 6bone-setup script modify:
	/proc/sys/net/ipv6/conf/default with
	/proc/sys/net/ipv6/conf/all

Well, to compile radvd (at least version 0.4.2) you'll need to do some changes.
So, donwload the source, my patch, apply it, etc, etc, etc.
The only difference is the use of configure. So once the patch is applyed:
	./configure --prefix=/usr/inet6

Then config it (copy radvd.conf.example to /usr/inet6/etc/radvd.conf ) and run
radvd. You'll find new IPv6 numbers in all your IPv6 machines of your network!

[root@homero ~]# ip addr

2: eth0: <BROADCAST,MULTICAST,UP> mtu 1500 qdisc pfifo_fast
  link/ETHER 00:00:21:a7:21:21 brd ff:ff:ff:ff:ff:ff            <-- MAC address
  inet 200.9.243.11/28 brd 200.9.243.15 scope global eth0
  inet6 fe80::200:21ff:fea7:2121/10 scope link 
  inet6 3ffe:a000::200:21ff:fea7:2121/64 scope global dynamic   <-- global scope
     valid_lft forever prefered_lft 604732sec

Note the "global dynamic scope" and the lastest 64 bits. Compare it with the
MAC address.

For more info read appendix B.



APPENDIX
---------

Appendix A.
-----------
Scopes & meanings.

The first bits of each number defines the scope.
Look at this table:

	-------------------------------------------------------
	| bits        | Scope                                 |
	-------------------------------------------------------
	|001          | aggregatable Global Unicast Addresses |
	|             | (numbers that will replace classes    |
	|             | A,B,C of IPv4 )                       |
	|1111 1110 10 | scope link .should not be forwarded.  |
	|1111 1110 11 | scope site .should not leave your site|
	|1111 1111    | multicast                             |
	|0000 0000    | scope host .should not leave your host|
	-------------------------------------------------------
The others combinations of bits are reserved for future use.

My ethernet has this number:
	fe80::200:21ff:fea7:2121     <- scope link
The lastest 64 bits are mapped with my 48-bit MAC address.

An scope link example:
        -------------------     -------------------------------
        |   First bits    |     |         lastest 64 bits     |
hexa    |  f  |  e  |  8  | ... |        0200:21ff:fea7:2121  | 
binario |1111 |1110 |1000 | ... |       ????????????????????? | 
        -------------------     -------------------------------


Appendix B.
-----------
Address that you may find useful:

6bone and IPv6 in general:
	http://www.6bone.net
	http://www.ipv6.org

IPv6 applications (tcpdump, inet6-apps, traceroute, libpcap and a lot more):
	ftp://ftp.inner.net/pub/ipv6

Net-tools:
	ftp://ftp.cs-ipv6.lancs.ac.uk/pub/Code/Linux/Net_Tools/

iproute2:
	ftp://ftp.inr.ac.ru/ip-routing

radvd:
	http://www.terra.net/ipv6/radvd.html (info)
	ftp://ftp.cityline.net/pub/systems/linux/network/ipv6/radvd/ (source)

kernel:
	http://www.kernel.org

glibc:
	ftp://alpha.gnu.org (source)
	ftp://ftp.gnu.org   (source)
	ftp://rawhide.redhat.com  (binary)
	
RFCs that you might find useful/interesting:
	rfc-2460, rfc-2463, rfc-2373, rfc-2401, rfc-2402, rfc-2406.

Bieringer's HOWTO:
	http://www.bieringer.de/linux/IPv6/IPv6-HOWTO

iproute2-micro-howto:
	http://www.compendium.com.ar/policy-routing.txt
	http://www.compendium.com.ar/policy-routing.es.txt (spanish)

Patchs that I've made. Useful to compile the apps with a glibc2.1.0:
	http://www.pjn.gov.ar/~rquesada/IPv6/patches

All of the sources mentioned in this document are in:
	http://www.pjn.gov.ar/~rquesada/IPv6/src

This document could be found in:
	http://www.pjn.gov.ar/~rquesada/IPv6/Linux_IPv6.en.txt


Greetings, etc, etc:
--------------------
	A Bombi, mi hechicera,
	a Edu, feliz casamiento,
	a Javi, feliz cumple,
	a HoraPe, por organizar el 6bone en Argentina y por su ayuda,
	al B.A.L.U.G (Balug Argentina Linux Users Group) y
	a M�, P� y Viole.
	
	
riq
March'99
