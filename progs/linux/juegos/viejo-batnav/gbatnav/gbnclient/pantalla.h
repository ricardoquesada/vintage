/*	$Id: pantalla.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/

/* 
	Includes de pantalla
	Funcion que chequea el port y esas cosas
*/


#ifndef  __BN_PANTALLA__
# define __BN_PANTALLA__

# ifdef __cplusplus
extern "C" {
# endif __cplusplus



void 
textfill(gint, gchar *, ... );

void 
pmicell(int x,int y,int color);

void 
ptucell(int x,int y,int color);

void 
foot_right( char* text );

void 
foot_left( char* text );


   
# ifdef __cplusplus
}
# endif __cplusplus

# endif __BN_PANTALLA__
