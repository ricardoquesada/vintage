/*	$Id: g_interface.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/
/*
	Includes de g_interface
*/


#ifndef  __BN_G_INTERFACE__
# define __BN_G_INTERFACE__

gboolean g_do_exit(GtkWidget *widget, gpointer data);

gboolean g_box_lost();

gboolean g_box_win();

void about( GtkWidget *widget, gpointer data );

void init_X();

# endif __BN_G_INTERFACE__
