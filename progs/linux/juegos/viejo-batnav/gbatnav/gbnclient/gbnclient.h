/*	$Id: gbnclient.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/

/* 
	Includes de gbnclient
	Funcion que chequea el port y esas cosas
*/


#ifndef  __BN_GBNCLIENT__
# define __BN_GBNCLIENT__

# ifdef __cplusplus
extern "C" {
# endif __cplusplus

gint sacar_usrpage( gint jugador );

void remove_page( gint );

gint buscar_usr( gint usr );

gint expose_event( GtkWidget *widget, GdkEventExpose *event );

gint button_press_event (GtkWidget *widget, GdkEventButton *event);

gint expose_event_about (GtkWidget *widget, GdkEventExpose *event);

gint page_switch( GtkWidget *widget, GtkNotebookPage *page, gint page_num );

gint expose_event_right( GtkWidget *widget, GdkEventExpose *event );

gint button_press_event_right( GtkWidget *widget, GdkEventButton *event );

void iwtable( char *dest);

void filtermiboard();

void putintemp( char *table );

void showboard( GdkPixmap *pixmap );

void fillboard( char *filltable, int a );

char* inteliclient( char *table);

int play( void );

void status( );

void init_datos( void );

void init_cliente( void );

int init_robot( void );

void bn_help( void );
   
# ifdef __cplusplus
}
# endif __cplusplus

# endif __BN_GBNCLIENT__
