/*	$Id: configure.c,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/

#include <gnome.h> 
#include "cliente.h"
#include "pantalla.h"
#include "gbnclient.h"

/* Boton Config */
void
c_destroy_window( GtkWidget * widget, GtkWidget **window )
{
	*window=NULL;
}

gint
configure_apply( GtkWidget *widget, GtkWidget *window )
{
      
	if( strncmp( usuario.nombre, gtk_entry_get_text( GTK_ENTRY( conf_entry_name)), MAXNAMELEN) != 0)
	{
		if( usuario.play>=CONNEC )
			textfill(0,_("You must reconnect to use your new name"));
	
		strncpy( usuario.nombre, gtk_entry_get_text( GTK_ENTRY( conf_entry_name )) ,MAXNAMELEN);
	}
   
	strncpy( usuario.server_name, gtk_entry_get_text( GTK_ENTRY( conf_entry_server )) ,50);
	usuario.port = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(conf_spinner_port));
/*   	usuario.autorobot = GTK_TOGGLE_BUTTON(conf_check_button)->active ; */
	usuario.debug_level = GTK_TOGGLE_BUTTON(conf_cb_ttyfill)->active ;
   	
	gnome_config_set_int   ("/gbnclient/data/port",  usuario.port);
/*	gnome_config_set_int   ("/gbnclient/data/autorobot",  usuario.autorobot); */
	gnome_config_set_int   ("/gbnclient/data/debuglevel",  usuario.debug_level);
	gnome_config_set_string("/gbnclient/data/servername",usuario.server_name);
	gnome_config_set_string("/gbnclient/data/playername",usuario.nombre);
	gnome_config_sync();
   
	return TRUE;
}

gint
configure_ok( GtkWidget *widget, GtkWidget *window )
{
	configure_apply( widget, window );
	gtk_widget_destroy(window);
	return TRUE;
}

void
configure( void )
{
	static GtkWidget *win_configure=NULL;
	GtkWidget *main_vbox;
	GtkWidget *vbox;
	GtkWidget *hbox1;
	GtkWidget *hbox2;
	GtkWidget *hbox3;
	GtkWidget *hbox4;
	GtkWidget *frame;
	GtkWidget *label1;
	GtkWidget *label2;
	GtkWidget *label3;
	GtkWidget *separator;
	GtkWidget *button_ok;
	GtkWidget *button_apply;
	GtkWidget *button_cancel;
	GtkAdjustment *adj;

	if(!win_configure)
	{
		win_configure = gtk_widget_new (gtk_window_get_type (),
			"GtkWindow::type", GTK_WINDOW_TOPLEVEL,
			"GtkWindow::title", "Gnome Batnav - Preferences",
			"GtkContainer::border_width",0,
			NULL);
		gtk_signal_connect ( GTK_OBJECT( win_configure), "destroy",
			GTK_SIGNAL_FUNC( c_destroy_window ),
			&win_configure );
		gtk_signal_connect ( GTK_OBJECT( win_configure), "delete_event",
			GTK_SIGNAL_FUNC( c_destroy_window),
			&win_configure );
	
		main_vbox = gtk_vbox_new( FALSE, 0);
		gtk_container_add ( GTK_CONTAINER(win_configure), main_vbox );
	
		frame = gtk_frame_new (_("Preferences"));
		gtk_box_pack_start( GTK_BOX( main_vbox ), frame, TRUE, TRUE, 0);
	
		vbox = gtk_vbox_new( FALSE, 0);
		gtk_container_add ( GTK_CONTAINER(frame), vbox );
	
		hbox1 = gtk_hbox_new( FALSE, 0);
		gtk_container_add( GTK_CONTAINER( vbox ), hbox1 );
		label1 = gtk_label_new(_("Server port:"));
		gtk_box_pack_start( GTK_BOX( hbox1), label1, TRUE, TRUE, 0);
		adj = (GtkAdjustment * ) gtk_adjustment_new( usuario.port, 1.0, 65536.0, 1.0, 5.0, 1.0 );
		conf_spinner_port = gtk_spin_button_new( adj, 0, 0);
		gtk_box_pack_start( GTK_BOX( hbox1), conf_spinner_port, TRUE, TRUE, 0);
	
		hbox2 = gtk_hbox_new( FALSE, 0);
		gtk_container_add( GTK_CONTAINER( vbox ), hbox2 );
		label2 = gtk_label_new(_("Server name:"));
		gtk_box_pack_start( GTK_BOX( hbox2), label2, TRUE, TRUE, 0);
		conf_entry_server = gtk_entry_new( );
		gtk_entry_set_text( GTK_ENTRY( conf_entry_server ), usuario.server_name);
		gtk_box_pack_start( GTK_BOX( hbox2), conf_entry_server, TRUE, TRUE, 0);
	
		hbox3 = gtk_hbox_new( FALSE, 0);
		gtk_container_add( GTK_CONTAINER( vbox ), hbox3 );
		label3 = gtk_label_new(_("User name:"));
		gtk_box_pack_start( GTK_BOX( hbox3), label3, TRUE, TRUE, 0);
		conf_entry_name = gtk_entry_new( );
		gtk_entry_set_text( GTK_ENTRY( conf_entry_name ), usuario.nombre);
		gtk_box_pack_start( GTK_BOX( hbox3), conf_entry_name, TRUE, TRUE, 0);

/*
		conf_check_button = gtk_check_button_new_with_label(_("Auto launch robot"));
		GTK_TOGGLE_BUTTON(conf_check_button)->active=usuario.autorobot;
		gtk_box_pack_start( GTK_BOX( vbox ), conf_check_button, TRUE, TRUE, 0);
*/
		
		conf_cb_ttyfill = gtk_check_button_new_with_label(_("Lot of messages"));
		GTK_TOGGLE_BUTTON(conf_cb_ttyfill)->active=usuario.debug_level;
		gtk_box_pack_start( GTK_BOX( vbox ), conf_cb_ttyfill, TRUE, TRUE, 0);

		separator = gtk_hseparator_new ();
		gtk_box_pack_start (GTK_BOX( main_vbox), separator, FALSE, TRUE, 0);
	
		hbox4 = gtk_hbox_new (FALSE, 10);
		gtk_box_pack_start (GTK_BOX (main_vbox), hbox4, FALSE, TRUE, 0);
	
		button_ok = gnome_stock_button(GNOME_STOCK_BUTTON_OK);
		gtk_signal_connect_object (GTK_OBJECT (button_ok), "clicked",
			GTK_SIGNAL_FUNC(configure_ok),
			GTK_OBJECT(win_configure) );
		gtk_box_pack_start (GTK_BOX (hbox4), button_ok, TRUE, TRUE, 0);
	
		button_apply = gnome_stock_button(GNOME_STOCK_BUTTON_APPLY);
		gtk_signal_connect_object (GTK_OBJECT (button_apply), "clicked",
			GTK_SIGNAL_FUNC(configure_apply),
			GTK_OBJECT( win_configure ));
		gtk_box_pack_start (GTK_BOX (hbox4), button_apply, TRUE, TRUE, 0);

		button_cancel = gnome_stock_button( GNOME_STOCK_BUTTON_CANCEL);
		gtk_signal_connect_object (GTK_OBJECT (button_cancel), "clicked",
			GTK_SIGNAL_FUNC(gtk_widget_destroy),
			GTK_OBJECT(win_configure) );
		gtk_box_pack_start (GTK_BOX (hbox4), button_cancel, TRUE, TRUE, 0);
	
	}

	if (!GTK_WIDGET_VISIBLE (win_configure))
		gtk_widget_show_all (win_configure);
	else
		gtk_widget_destroy (win_configure);
}
