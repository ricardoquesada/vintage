/*	$Id: bnwrite.c,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/

#include <stdio.h>
#include <stdarg.h>
#include "cliente.h"
#include "../gbnserver/riq-net.h"

int bnwrite(char *fmt, ...)
{
	int a;
	char t[1000];
	va_list args;
	va_start( args, fmt );

	vsprintf(t,fmt,args);
	va_end(args);
	
	a = riq_net_printf(usuario.sock,"%s\n",t);
	if(a < 1 )
		printf("bnwrite error\n");
	return (a);
}
