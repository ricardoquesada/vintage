/*	$Id: random_board.c,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/
/* 
 * Archivo que contiene los algoritmos para generar
 * una tabla aleatoria valida
 */

/* 'Users use same prandom in localhost' fix by Radovan Garabik <garabik@atlas03.dnp.fmph.uniba.sk> */
 

#include <stdlib.h>
#include <config.h>
#include <gnome.h>

#include "../gbnserver/protocol.h"
#include "cliente.h"
#include "random_board.h"
#include "pantalla.h"

void
generar( void )
{
	gint x,y,d,t,i,j;

	if( usuario.play==PLAY || usuario.play==TURN ) { 
		/* permito que un PERDIO modifique su board */
		textfill(0,_("You can't modify your board while playing."));
		return ;

	} else if( usuario.play==BOARD ) {
		textfill(0,_("You've already sent your board to the server. You can't modify it."));
		return ;

	} else {
		usuario.random = gnome_config_get_int_with_default("/gbnclient/data/random=1995",NULL);
		srandom( usuario.random );
		usuario.random = random();		
		gnome_config_set_int   ("/gbnclient/data/random",  usuario.random);
		gnome_config_sync();

		for(i=0;i<10;i++) {
			/* clean tabla */
			for(j=0;j<10;j++)
				usuario.mitabla[i][j]=NOBARCO;
		}

		for(i=1;i<=10;i++) {	
			x = 1+(int) (10.0*rand()/(RAND_MAX+1.0));
			y = 1+(int) (10.0*rand()/(RAND_MAX+1.0));
			d = (x + y) & 1;	/* Direccion */
			t = tamano( i );	/* Tama#o */
	
			buscar_poscicion(x,y,d,t);
		}

		for(i=0;i<10;i++) {
			for(j=0;j<10;j++)
				pmicell(i,j,usuario.mitabla[i][j]);
		}
	}
}

gint
posible( gint x, gint y, gint d, gint t)
{
	gint i;

	/* Por tamano, entra ? */	
	if( !entra( x,y,d,t) )
		return FALSE;
	
	/* Por colision, vale ? */
	if(d==0) {
		/* Horizontal */
		for( i=0; i<=t+1; i++ ) {
			if(!( vacio(x-1+i,y) && vacio( x-1+i,y-1) && vacio( x-1+i,y+1))	)
				return FALSE;
		}
		return TRUE;
	}

	else{
		/* Vertical */ 
		for( i=0; i<=t+1; i++ ) {
			if(! (vacio( x, y-1+i ) && vacio( x-1, y-1+i ) && vacio( x+1, y-1+i ))	)
				return FALSE;
		}
		return TRUE;
	}
}

/*
 * Algoritmo recursivo
 */
void 
buscar_poscicion( gint x, gint y, gint d, gint t)
{
	/* Pregunta si es posible en la otra direccion */
	if( posible( x,y, ((d^1) & 1),t )) {
		poner_barco( x,y,  ((d^1) &1), t);
		return;
	}

	/* Incrementa Y en uno y asi sucesivamente */
	/* Digamos que recorre todo - Nada de Backtracking o algo mejor */	
	y++;
	if(y>10) {
		y=1;
		x++;
		if(x>10)
			x=1;
	}

	if( posible( x,y,d,t) ) {
		poner_barco( x,y,d,t);
		return ;
	}
	else
		return buscar_poscicion( x,y,d,t);
}


gint
tamano( gint i )
{
	gint a;

	switch(i) {	
	case 1:
	case 3:
	case 7:
	case 8:
		a = 1;
		break;
	case 2:
	case 9:
	case 5:
		a = 2;
		break;
	case 4:
	case 10:
		a = 3;
		break;
	case 6:
		a = 4;
		break;
	}
	return a;
}

/* Pregunta si un barco entra por tamano */
gint
entra( gint x, gint y, gint d, gint t)
{
	if(d==0) {
		/* Horizontal */
		if(x+t-1<=10)
			return TRUE;
		else
			return FALSE;
	}
	
	else {
		/* Vertical */
		if(y+t-1<=10)
			return TRUE;
		else
			return FALSE;
	}
}

gint
vacio( gint x, gint y)
{
	if(x<1 || x>10 || y<1 || y>10)
		return TRUE;

	if( usuario.mitabla[x-1][y-1]==NOBARCO || usuario.mitabla[x-1][y-1]==AGUA )
		return TRUE;
	else
		return FALSE;
}

void
poner_barco( gint x, gint y, gint d, gint t)
{
	gint i;

	if(d==0) {
		/* Horizontal */
		for(i=1;i<=t;i++)
			usuario.mitabla[x-2+i][y-1]=BARCO;
	}
	else {
		/* Vertical */
		for(i=1;i<=t;i++)
			usuario.mitabla[x-1][y-2+i]=BARCO;
	}
}
