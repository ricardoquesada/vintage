/*	$Id: proceso.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/
/* 
	Includes de proceso
	Funcion que chequea el port y esas cosas
*/


#ifndef __BN_PROCESO__
#define __BN_PROCESO__

#include <gnome.h>

void aux_disconnect();
gint aux_connect();
gint proceso( gpointer data, gint sock, GdkInputCondition GDK_INPUT_READ );

# endif __BN_PROCESO__
