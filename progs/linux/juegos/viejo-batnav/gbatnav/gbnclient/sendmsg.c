/*	$Id: sendmsg.c,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/
#include <gnome.h>

#include "cliente.h"
#include "pantalla.h"
#include "gbnclient.h"
#include "bnwrite.h"

/* Boton SendMsg */

void
sm_destroy_window( GtkWidget * widget, GtkWidget **window )
{
	*window=NULL;
}

gint
sendmsg_apply( GtkWidget *widget, GtkWidget *window )
{
	strncpy( usuario.tomsg, gtk_entry_get_text( GTK_ENTRY( send_entry_message )), PROT_MAX_LEN);

	if(usuario.play>=CONNEC)
		bnwrite(BN_MESSAGE"=%s",usuario.tomsg);
	return TRUE;
}

gint
sendmsg_ok( GtkWidget *widget, GtkWidget *window )
{
	sendmsg_apply( widget, window );
	gtk_widget_destroy( window );
	return TRUE;
}

void
bnsendmsg( void )
{
	static GtkWidget *win_sendmsg=NULL;
	GtkWidget *main_vbox;
	GtkWidget *vbox;
	GtkWidget *hbox2;
	GtkWidget *hbox4;
	GtkWidget *frame;
	GtkWidget *label2;
	GtkWidget *separator;
	GtkWidget *button_ok;
	GtkWidget *button_apply;
	GtkWidget *button_cancel;
   
	if(!win_sendmsg)
	{
		win_sendmsg = gtk_widget_new (gtk_window_get_type (),
			"GtkWindow::type", GTK_WINDOW_TOPLEVEL,
			"GtkWindow::title", "Batalla Naval - Send Message",
			"GtkContainer::border_width",0,
			NULL);
		gtk_signal_connect ( GTK_OBJECT( win_sendmsg ), "destroy",
			GTK_SIGNAL_FUNC( sm_destroy_window ),
			&win_sendmsg );
		gtk_signal_connect ( GTK_OBJECT( win_sendmsg ), "delete_event",
			GTK_SIGNAL_FUNC( sm_destroy_window),
			&win_sendmsg );
	
		main_vbox = gtk_vbox_new( FALSE, 0);
		gtk_container_add ( GTK_CONTAINER(win_sendmsg), main_vbox );
	
		frame = gtk_frame_new (_("Send Message"));
		gtk_box_pack_start( GTK_BOX( main_vbox ), frame, TRUE, TRUE, 0);
	
		vbox = gtk_vbox_new( FALSE, 0);
		gtk_container_add ( GTK_CONTAINER(frame), vbox );
	
		hbox2 = gtk_hbox_new( FALSE, 0);
		gtk_container_add( GTK_CONTAINER( vbox ), hbox2 );
		label2 = gtk_label_new(_("Message:"));
		gtk_box_pack_start( GTK_BOX( hbox2), label2, TRUE, TRUE, 0);
		send_entry_message = gtk_entry_new( );
		gtk_entry_set_text( GTK_ENTRY( send_entry_message ), "");
		gtk_box_pack_start( GTK_BOX( hbox2), send_entry_message, TRUE, TRUE, 0);
	
		separator = gtk_hseparator_new ();
		gtk_box_pack_start (GTK_BOX( main_vbox), separator, FALSE, TRUE, 0);
	
		hbox4 = gtk_hbox_new (FALSE, 10);
		gtk_box_pack_start (GTK_BOX (main_vbox), hbox4, FALSE, TRUE, 0);
	
		button_ok = gnome_stock_button(GNOME_STOCK_BUTTON_OK);
		gtk_signal_connect_object (GTK_OBJECT (button_ok), "clicked",
			GTK_SIGNAL_FUNC(sendmsg_ok),
			GTK_OBJECT(win_sendmsg) );
		gtk_box_pack_start (GTK_BOX (hbox4), button_ok, TRUE, TRUE, 0);

		button_apply = gnome_stock_button(GNOME_STOCK_BUTTON_APPLY);
		gtk_signal_connect_object (GTK_OBJECT (button_apply), "clicked",
			GTK_SIGNAL_FUNC(sendmsg_apply),
			GTK_OBJECT( win_sendmsg ));
		gtk_box_pack_start (GTK_BOX (hbox4), button_apply, TRUE, TRUE, 0);

		button_cancel = gnome_stock_button(GNOME_STOCK_BUTTON_CANCEL);
		gtk_signal_connect_object (GTK_OBJECT (button_cancel), "clicked",
			GTK_SIGNAL_FUNC(gtk_widget_destroy),
			GTK_OBJECT(win_sendmsg) );
		gtk_box_pack_start (GTK_BOX (hbox4), button_cancel, TRUE, TRUE, 0);
	
	}

	if(!GTK_WIDGET_VISIBLE( win_sendmsg))
		gtk_widget_show_all( win_sendmsg );
	else
		gtk_widget_destroy( win_sendmsg );
}

