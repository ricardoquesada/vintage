/*	$Id: g_interface.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/

#ifndef __BN_G_INTERFACE_H__
#define __BN_G_INTERFACE_H__

#include <stdarg.h>
#include <gnome.h>
#include "protocol.h"

void init_screen();

void say_in_clist( gint, gint, gchar *);

#endif /* __BN_G_INTERFACE_H__ */
