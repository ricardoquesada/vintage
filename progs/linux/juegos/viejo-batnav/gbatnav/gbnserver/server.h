/*	$Id: server.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/
#ifndef __BN_SERVER_H__
#define __BN_SERVER_H__

#include <gnome.h>
#include "protocol.h"


typedef struct tabla_typ {
	int  x,y;
	char p[10][10];
} tabla;



struct st_datos {
	char	server_name[50];		/* server name */
	int	port;				/* donde va el port */
	int	sock;				/* socket ppal del server */
	int	usock;				/* Unix Domain Socket (pa' los robots) */
	int	nro_tot[MAXPLAYER];		/* estado del jugador */
	int	nro_fd[MAXPLAYER];		/* fd o socket */
	int	hits[MAXPLAYER];		/* how many hits has a player received */
	tabla	table[MAXPLAYER];		/* tablas de los jugadores */
	char	names[MAXPLAYER][MAXNAMELEN];	/* other players's name */
	int	tag[MAXPLAYER];			/* gdk que lee */
	int	robot[MAXPLAYER];		/* para saber si el que se esta conectando es robot */
};

enum {
	C_NUMBER,
	C_NAME,
	C_HOSTNAME,
	C_CLIVER,
	C_STATUS,
	C_LASTTOKEN
};

struct st_datos usuario;

#define BATNAV_UDSOCKET "/tmp/batnav-socket"

#endif /* __BN_SERVER_H__ */
