/*	$Id: riq-net.h,v 1.1.1.1 2000/02/06 23:42:17 riq Exp $	*/
#ifndef __RIQ_NET_H__
#define __RIQ_NET_H__

int riq_net_connect_unix(char *path );
int riq_net_connect_tcp(char *host, int port );
ssize_t riq_net_readline(int sock, void *gs,size_t maxlen );
int riq_net_printf(int sock, char *format, ...);

#endif /* __RIQ_NET_H__ */
