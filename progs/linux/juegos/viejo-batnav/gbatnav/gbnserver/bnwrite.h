/*	$Id: bnwrite.h,v 1.1.1.1 2000/02/06 23:42:18 riq Exp $	*/
/*
 * Funcion bnwrite
 */

#ifndef __BNWRITE_H__
#define __BNWRITE_H__

void broadcast( char *fmt, ...);
int bnwrite(int, gchar *fmt, ...);

#endif /* __BNWRITE_H__ */
