/*	$Id: robot_proceso.h,v 1.1.1.1 2000/02/06 23:42:18 riq Exp $	*/
/* 
	Includes de proceso
	Funcion que chequea el port y esas cosas
*/


#ifndef  __BN_PROCESO__
#define __BN_PROCESO__
#include <gnome.h>

gint buscar_jugador_vacio( void );
gint robot_proceso( gpointer data, gint sock, GdkInputCondition GDK_INPUT_READ );
void robot_init();

# endif __BN_PROCESO__
