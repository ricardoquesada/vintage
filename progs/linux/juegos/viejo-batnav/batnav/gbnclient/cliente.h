/*
 * Batalla Naval (c) 1995,96,97,98 Ricardo Quesada
 * (rquesada@dc.uba.ar)
 * 
 * Funciones del Batalla Naval.
 * 
 */


#ifndef  __BN_CLIENTE__
# define __BN_CLIENTE__

# define GBNVER "Cliente GTK v0.60-pre31"
# define GBNVERH 0
# define GBNVERL 60		

# define LARGO 20
# define ANCHO 20
# define MAXPLAYER 5

# include "../share/protocol.h"

/*
 * Estructuras y Funciones Principales
 */
struct usuario 
{
   char server_name[50];             /* server name */
   int port;                         /* server port */
   char nombre[MAXNAMELEN];          /* user name */
   char names[MAXPLAYER][MAXNAMELEN]; /* other players's name */
   
   int firsthit;                     /* para borrar BN new on 0.30 */
   int play;                         /* already playing ? */
   int numjug;                       /* number of player */
   int usrfrom;                      /* player who is owner of enemy window */
   
   char tomsg[MSGMAXLEN];            /* usuadas para mandar mensajes */
   int  towho;
   int  lecho;                       /* local echo */
   
   char status[5][16];               /* new on v0.43 */
   int  hide;                        /* hide my board new on v0.44 */
   
   int tag;                          /* propio de GDK_INPUT_ADD */
   char tempclit[MSGMAXLEN];         /* Variable usada por el cliente */

   int pages[MAXPLAYER];             /* Variable de las paginas . only GTK ? */
   int gano;                         /* un detalle usado para los pixmaps */
} usuario;

/* variables que tienen que ver al juego propiamente dicho */
gint sock;
struct sockaddr_in server;
struct hostent *host_info;
char mitabla[10][10];          /* mi tabla de barcos */
char barquitos[4];             /* tama�os de los barcos 1 de 4, 2 de 3, etc. */
char bnsup;                    /* usado por bnwrite */
char temp[10];                 /* Un temporal que se puede borrar ? */


/* ---- Variables Globales ---- */
/* Variables - GTK */
/* contenedores */
GtkWidget *window;
GtkWidget *hbox;
GtkWidget *vbox;
/* left */
GtkWidget *notebook_left;
GtkWidget *label_left; 
GtkWidget *drawing_left;
GtkWidget *hbox_text_help;
GtkWidget *text_help;
GtkWidget *vscrollbar_help;

/* center */
GtkWidget *vbox_buttons;
GtkWidget *button_connect;
GtkWidget *button_disconnect;
GtkWidget *button_sendboard;
GtkWidget *button_start;
GtkWidget *button_sendmsg;
GtkWidget *button_status;
GtkWidget *button_config;
GtkWidget *button_quit;

/* right */
GtkWidget *notebook_right;
GtkWidget *label_right_about;
GtkWidget *drawing_right_about;
GtkWidget *label_right[MAXPLAYER];
GtkWidget *label_right2[MAXPLAYER];
GtkWidget *drawing_right[MAXPLAYER];

static void remove_page( gint page_num );

/* otros */
GtkWidget *hbox_text;
GtkWidget *vscrollbar;
GtkWidget *separator;
GtkWidget *text;


/* Pixmaps */
GdkPixmap *fondo ;
GdkPixmap *rojo ;
GdkPixmap *negro ;
GdkPixmap *azul ;
GdkPixmap *verde ;
GdkPixmap *about ;
GdkPixmap *helpxpm ;
GdkPixmap *winner ;
GdkPixmap *gameover ;
GdkPixmap *icono ;
GdkBitmap *mask ;

GtkWidget *hbox_status;
GtkWidget *statusbar_left;
GtkWidget *statusbar_right;

/* Usadas por Configure */
GtkWidget *spinner_port;
GtkWidget *entry_server;
GtkWidget *entry_name;

/* Usadas por Sendmsg */
GtkWidget *spinner_towho;
GtkWidget *entry_message;
GtkWidget *toggle_button;

/* Manejo de pantalla */
static void init_X();
static void ttyfill(char msg[]);
static void pmicell(int x,int y,int color);
static void ptucell(int x,int y,int color);


/* Funciones del juego */
static size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo );

/* Funciones  boton */
int  init_cliente( void );
void status( );
int  play ( void );
void do_exit( );
void configure( void );
void bnsendmsg( void );
void fillboard( char *, int );
void showboard( GdkPixmap * );
void inteliclient( char* );
int  buscar_usr( int );

#endif __BN_CLIENTE__
