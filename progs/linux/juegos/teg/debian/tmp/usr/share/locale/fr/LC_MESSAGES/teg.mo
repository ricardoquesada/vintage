��    �     �    <  :   �!  >   �!     *"  /   H"  -   x"  #   �"  >   �"  .   	#  "   8#  &   [#  2   �#  *   �#      �#     $     $     *$     J$     e$  "   {$     �$     �$     �$  %   �$  
   %  +   %     D%  +   P%     |%     �%     �%     �%     �%     �%  .   �%     &  	   &     "&     )&     8&  :   =&  	   x&     �&     �&  
   �&     �&     �&     �&     �&     �&  B   �&     '     '     '     ='     E'  o   W'     �'  F   �'  -   #(  ;   Q(  E   �(  A   �(  A   )  E   W)  >   �)     �)  /   �)  	   *     (*     6*  D   Q*  $   �*  0   �*     �*     +      +      =+     ^+     d+     u+     �+     �+     �+     �+     �+     ,     ,     5,     O,     e,     |,     �,     �,     �,     �,     �,     -     --     H-     `-     y-     �-     �-     �-     �-  -   �-     !.  +   =.  7   i.  4   �.  #   �.  &   �.  #   !/  !   E/  (   g/  )   �/     �/  /   �/  -   0  &   30  '   Z0  '   �0  4   �0     �0  >   �0  =   =1      {1  (   �1     �1  '   �1     2     )2     H2     e2     m2     t2  5   }2     �2     �2     �2     �2  5   �2     
3     3  	   ,3  H   63  @   3  2   �3  1   �3     %4     -4     @4     I4  
   V4  	   a4     k4  	   y4     �4     �4     �4     �4     �4     �4     �4     �4     �4     �4      5  !   5     :5     @5     N5     f5     �5     �5     �5     �5     �5     �5     �5      �5     �5     �5     6     6     6     6  2   16     d6     k6     �6     �6     �6     �6     �6     �6     �6     �6  "   �6     "7     /7     I7     c7     �7  !   �7     �7  '   �7     �7     8      88  -   Y8     �8  %   �8  "   �8  /   �8  /   9     L9     h9     �9     �9  *   �9     �9     �9     �9     :     :     .:  7   B:  
   z:     �:     �:     �:      �:     �:  $   �:  %   ;  .   ;;  &   j;     �;     �;     �;  \   �;     <     )<     ;<     U<     a<     �<     �<     �<     �<  )   �<     �<  !   �<     =     3=     K=     e=  !   {=  -   �=     �=     �=  G   �=  8   1>     j>     w>     �>     �>     �>  G   �>     �>     ?     ?  	   ?     "?     )?     7?     G?     [?     `?     h?  :   o?  	   �?     �?  O   �?  5   @  C   A@  "   �@     �@  "   �@     �@     �@     �@     A  *   /A     ZA     kA     wA     �A     �A     �A     �A     �A     �A     �A     B     :B     KB     [B  !   gB     �B  .   �B     �B     �B     �B     C     C  7   6C     nC     {C     �C     �C     �C  g   �C  �   D     �D     �D     �D     �D     �D  	   �D     �D     E     E  	   E     !E     'E     ,E     @E  	   OE     YE     fE     nE  $   zE     �E     �E  	   �E     �E     �E     �E     �E     �E     F     %F     3F     7F     @F     FF     KF     ZF     kF  A   |F  
   �F     �F     �F     �F     �F  
   �F     G     G  "   #G     FG     UG  
   bG     mG     �G     �G     �G  &   �G     �G     �G     H  !   H     =H     NH     bH  7   |H  .   �H     �H     I     !I     9I     QI     hI     zI  %   �I     �I     �I     �I     �I     �I     
J  C   J  =   UJ      �J  .   �J      �J  '   K  ?   ,K  6   lK  )   �K  =   �K  0   L  1   <L  ?   nL     �L     �L  #   �L     �L     M  -   *M     XM     nM     �M  #   �M     �M  +   �M     �M  +   N     4N     DN     ZN     jN     �N     �N  5   �N     �N  	   �N     �N     �N     
O     O  	   "O     ,O     3O  
   :O     EO     LO     RO  	   XO     bO  E   pO     �O     �O     �O     �O     �O  k   P     tP  B   �P  ,   �P  >   �P  ?   5Q  F   uQ  E   �Q  C   R  B   FR     �R  :   �R     �R     �R  !   �R  [   S  %   aS  8   �S     �S  &   �S  '   �S  (   &T     OT     VT     jT     �T     �T     �T     �T     �T     U     +U     HU     eU     ~U     �U     �U     �U     �U     
V     $V     @V     ^V     |V     �V     �V     �V     �V     W     #W  2   <W  '   oW  )   �W  7   �W  E   �W  )   ?X  +   iX  +   �X  1   �X  1   �X  6   %Y      \Y  4   }Y  3   �Y  3   �Y  )   Z  )   DZ  F   nZ  )   �Z  K   �Z  8   +[     d[  6   �[  8   �[  3   �[  6   (\  .   _\  %   �\     �\     �\     �\  Q   �\     ]     ']     3]     J]  I   O]     �]     �]     �]  P   �]  J   ^  >   f^  8   �^  	   �^     �^      _     _      _     -_     9_  	   I_  
   S_     ^_     q_     _     �_     �_     �_  "   �_     �_     �_     �_  %   `     ,`     3`     D`     ``     z`     �`     �`     �`     �`     �`     �`  $   �`  	   �`     �`     a     a     a     +a  G   Ba     �a     �a     �a     �a     �a  "   �a     �a     �a     b     b     b     >b     Ob      lb  "   �b     �b  '   �b  !   �b  3   c  #   Nc  $   rc  &   �c  /   �c  #   �c  +   d      >d  6   _d  6   �d  !   �d  "   �d  #   e     6e  5   Re     �e     �e     �e     �e  	   �e     �e  9   �e     f     ,f     Df     Zf  *   rf     �f  '   �f  (   �f  .   �f  )   .g     Xg     _g     og  [   �g     �g     �g     h     .h  #   Ah     eh     xh     �h     �h  .   �h  ,   �h  +   i     4i      Si  !   ti  !   �i  3   �i  5   �i     "j     9j  Q   Aj  B   �j     �j     �j     �j     �j  	   k  P   !k     rk     �k     �k     �k     �k     �k     �k     �k     �k     �k     �k  :   �k     7l     Cl  W   Il  <   �l  9   �l  )   m     Bm  &   Xm     m     �m     �m     �m  1   �m     n     $n     4n     Bn     Wn  ,   in     �n     �n     �n     �n     �n     o     o     4o  &   Co     jo  :   �o      �o     �o     �o     
p     (p  <   Fp     �p     �p     �p  
   �p     �p  �   �p  �   Pq  $   �q  !   �q     !r  
   )r     4r     Br     Qr     _r     lr     �r     �r     �r     �r     �r     �r  
   �r     �r     �r      �r     s     "s     3s     Cs     Rs     Ws     [s     zs     �s     �s     �s     �s     �s     �s     �s     �s     t  I   "t     lt     xt     �t     �t     �t     �t     �t     �t  $   �t     u     $u     0u     ?u     Zu  	   uu     u  )   �u     �u     �u     �u  !   	v     +v     Bv     Xv  L   mv  '   �v  "   �v  "   w  '   (w     Pw     lw     �w  *   �w  '   �w  !   �w     x     2x     Kx     dx     tx  �   �   {  �   ]   4   1       D       3  �             $      �  \   �             #   �         �   O  e      �       �   8   �   �   +  T   |  �   �   >          s   R      j   Q         �           C   g  n   
     2  F       o  �          %   �   �   ^   t   @   �   �  �   *                       p  �   (   A          9          �       .      �   N               �   �   
   �           t  V  M   j  �   -      �       w  �       �   �   !                  �   @  2   �  �   B        >   ~       �   b       �   c                       ?   �   J       �           �   �   �      y   P  K   �   �   �   %  I   x       q   �       �   �   �      �   �   &   �      �   �   ^  �   G   �   �           �   �   P      �   �   d          �        �   ;      �   X   r  �   �  �   �   �       �   �        s  �   p   �   }   �       O         ?      �   �          �   8  `     ]  w           {   �   :      \  I  f  �       G  Q   z   6  }  ~   "      !  �   �           �   �           �       �   H   �       R         )   �  N  �   Y  �      �       �           �       �       	   S   #        �   D  '  _   [   [  u             �       �     T        M  �   )  E  	      �      v      C        <   �   &     �  �      c      g   A  z             7   +   <  (  v       E   1     L  �   =          �         ,          �   �   n  Y         5     �  �   i   �   :   �     4  �       k       *          W   �   ,  �   �   �   �   �     �   5  e   �   �           X  d   J      F  �  .        l   i  �   _  h     f         o      K  �    �       q     �     m   �         -   /       6           $   ;           �   �   '   U      Z       �         �   h      b  �       �          �   �       u      �   0   �               �   `       k  �   �   S  7  /  W    =       �       �   |   a    Z  �   L       r   9       x      �  H  a         0  l  m  �             �           B       �   3   V   U                                "   y      �           �  
If you want to EXCHANGE your cards for armies,
DO IT NOW!   -c, --console BOOLEAN	Enable the console or not (default 1)
   -g, --ggz		Enable GGZ mode
   -g, --ggz		Enable the GGZ mode (default OFF)
   -h, --help		Print a summary of the options
   -n, --name NAME	Use NAME as name
   -p, --port PORT	Bind the server to port PORT (default 2000)
   -p, --port PORT	Connect to server port PORT
   -q, --quiet		dont show messages
   -s, --seed SEED	New seed for random
   -s, --server HOST	Connect to the server at HOST
   -v, --version		Print the version number
 %s Better luck for the next time %s armies: %d %s variable is set to: %s
 %s variable is set to: (%d,%d)
 %s(%s) is attacking %s(%s) %s, Are you a robot ? %s, Do you have Lewinsky's phone ? %s, Do you have a sister ? %s, I cant believe it! %s, You play very well %s, You really dont know how to play! 1. Regroup 1st part:
Place %d armies in your countries 2. Get card 2nd part:
Place %d armies in your countries 3. End turn A country was conquered A new country A player lost the game A player won the game Africa And place another %d armies in your countries
 Arabia Argentina Armies Armies to move Asia Attack your enemy, again using same source and destination Australia Borneo Brazil California Cards Chile China Chupete Cleared output window.
 Client with incompatible protocol version (server:%d , client:%d)
 Colombia Color Command '%s' not recongnized
 Connect Connect to server Conquer 2 countries of Oceania,
2 of Africa, 2 of South America,
3 of Europe, 4 of North America,
and 3 of Asia Conquer 30 countries Conquer Africa,
5 countries of North America and
4 countries of Europe Conquer Asia and
2 countries of South America Conquer Europe,
4 countries of Asia and,
2 of South America Conquer North America,
2 countries of Oceania and
4 countries of Asia Conquer Oceania,
conquer Africa and,
5 countries of North America Conquer Oceania,
conquer North America and,
2 countries of Europe Conquer South America,
7 countries of Europe and
3 frontier countries Conquer South America,
conquer Africa and,
4 countries of Asia Conquer the world Conquer the world, or play with secret missions Countries Country Cards Decrease zoom factor by 5% Destination country: '%s'. Now select the quantity of armies to move Dices: %s: %d %d %d vs. %s: %d %d %d Different protocols version. Server:%d Client:%d Disconnect from server Display the map at 1:1 scale Does anyone speak Speranto ? Dont play, just observe the game Egypt Enables GGZ mode Error creating canvas
 Error creating image
 Error in clitok_ataque() Error in clitok_dados() Error in clitok_error() Error in clitok_exit() Error in clitok_fichas() Error in clitok_fichas2() Error in clitok_fichasc() Error in clitok_ggz() Error in clitok_lost() Error in clitok_message() Error in clitok_newplayer() Error in clitok_objetivo() Error in clitok_ok() Error in clitok_pais() Error in clitok_paises() Error in clitok_playerid() Error in clitok_pversion() Error in clitok_start() Error in clitok_status() Error in clitok_tarjeta() Error in clitok_tropas() Error in clitok_turno() Error in clitok_winner() Error in out_fichas() Error in start. Are there at least 2 players? Error setting %s variable.
 Error while trying to connect to GGZ client Error while trying to connect to server '%s' at port %d Error, '%s' doesnt have any avalaible armies to move Error, '%s' isnt frontier with '%s' Error, '%s' isnt one of your countries Error, It's not the time to regroup Error, cant initilize a new turn
 Error, it's not the moment to get a card Error, it's not the time to put 2 armies. Error, it's not your turn. Error, its not the moment to exchange the cards Error, its not the moment to send your armies Error, its not the time to send armies Error, put the correct number of armies Error, the 2 armies where placed before Error, the cards must be all equal, or all different Error, the game is not started Error, the game is started so you cant change the type of game Error, unexpected error in reagrupe_click(). Report this bug! Error, you are already connected Error, you can't regroup your armies now Error, you cant add armies now Error, you cant put more than %d armies Error, you cant regroup now. Error, you cant sub armies now Error, you must be connected Etiopia Europe Exchange Exchanged approved. Now you can place %d more armies! Exit Exit TEG Extra armies FALSE Fatal error: You can't run this program as superuser
 GGZ mode activated.
 Game Options Game Over Game over, Player %s(%s) is the winner

The accomplished mission was:
%s Game over, you are the winner!

The accomplished mission was:
%s Game with one player. Player %s(%d) is the winner
 Game without players. Initializing another game.
 Germany Get a country card Get card Good luck %s Goodbye %s Goodbye.
 Great Britain Greenland Hi %s How are you %s ? I love this game I'm a coward I'm player number:%d Iceland In Increase zoom factor by 5% India Israel It makes you more happy It shows info about the countries Italy Its your turn Its your turn to attack Its your turn to attack!! Japan Jirafa Launch a robot Malaysia Message Options Mexico Mongolia Moving armies from '%s' to '%s'
 Mr.President My color is: %s Name Name: North America Not yet implemented
 Now you can add %d extra armies in your countries! Number Observe the game, dont play it Oceania Oregon Out Parameter is missing for "%s".
 Pass the turn to another player Perla Peru Place %d armies in %s
 Place %d armies in your countries
 Place armies Play to conquer the world Play with secret missions Player %d abandoned the game Player %d exit the game Player %s(%d) abandoned the game
 Player %s(%d) has color %s
 Player %s(%d) is the winner! Game Over
 Player %s(%d) lost the game
 Player %s(%d) quit the game
 Player %s(%s) abandoned the game Player %s(%s) exchanged 3 cards for %d armies Player %s(%s) exit the game Player %s(%s) has the turn to attack! Player %s(%s) is placing %d armies Player %s(%s) is placing %d armies for 1st time Player %s(%s) is placing %d armies for 2nd time Player %s(%s) is the WINNER Player %s(%s) lost the game Player %s(%s) lost the game
 Player lost the game Player[%d] '%s' is connected with color %s Players status window Poland Put 2 armies Re_group armies Really surrender ? Regroup your armies Regroup your armies, moving from one country to another Regrouping Regrouping armies Requesting a mission... Reset the attack Returning a not so random number Robot name: %s Robot: Abnormal error in ai_fichas() Robot: Abnormal error in ai_fichasc() Robot: Error while trying to connect to server Robot: Unexpected error in canje_out() Russia Secret mission Select this card Select two countries:
1st: click on the source country
2nd: click on the destination country Select type of game Select your color Select your desired color Send armies Send the armies recently placed Send your armies Server Options Server name: Server port: Set the initial number of armies to place Set the seed for random Setting %s variable to: %s -> %s
 Show Error Messages Show Important Messages Show Informative Messages Show Players Messages Show Players Messages with colors Show the armies that others player had placed Shows the set options Siberia Source country is the same as the destination. Resetting the regroup... Source country: '%s'. Now select the destination country South Africa South America Spain Standalone server.
 Start Start again selecting the source and destination country for the attack Start server locally Start the game Status Surrender Sweden TEG Home Page TEG Preferences TEG server version  TRUE Tartary Taymir Tenes Empanadas Graciela - Robot v%s - by Ricardo Quesada
 Terranova Thanks The attack was reset. Please, select the source country to continue your attack The game has already started. Connect as an observer. The server is full :( Try connecting as an observer. Go to settings The server report an error in '%s' Token '%s' not found Try to run it as a different user
 Turkey Type '%s' for help
 Type '%s' for more help
 Unrecognized option: "%s"
 Usage: %s [option ...]
Valid options are:
 Using seed: %u

 View _cards View _dices View _mission View _players View all the card that you won View players View players status View the result of the dices View the status of the players View your mission of this game Welcome back %s! Welcome to TEG! Who started Yo tambien quiero un mundo mejor! You are the winner You conquered '%s' in your attack from '%s'!!
 You dont have any cards yet You lost You lost the game :( You need to be connected You received card: '%s' You received card: '%s' and 2 armies where placed there Your mission Your mission is: %s Zaire Zoom In Zoom Out [Note: The server had move one army to '%s' for you.
Now choose, how many more armies you want to move] [Note: You can regroup as many times as you want,
as long as you dont regroup an armie that were
regrouped before in this turn.] [This is the common mission.] [This is your secret mission.] _Actions _Connect _Disconnect _End turn _Launch robot _Reattck _Update Map attacking black blue cerrando coneccion
 client version connected disconnected enabled ending turn error %d (compiled without strerror) exchanging cards exits the game game over gettting card green idle internal use. Dont use it internal use. Dont use it. making a pact moving armies n/a observer other pink placing armies placing armies 2 placing armies 3 players:%d, connections:%d, game number:%d, mission:%s, rules:%s
 postarmies postarmies 2 postarmies 3 protocol version red regrouping request a mission save the game sends a message to all the players server version sets options shows help shows status of players shows the status of the players started starting turn tegserver: Abnormal error in select()
 to ask for help to attack a country to comment a command to exchange your cards for armies to exit the game to finish your turn to pick up a country-card to place 2 armies in a country. You must have that card to place the armies after a turn have finished to place the initials 3 armies to place the initials 5 armies to register as a player to remind me what to do to reorder your armies to select a color to send a message to send armies to a conquered country to set an async callback to set options to start playing to start the game to surrender yellow 
Si vous voulez CHANGER des cartes en arm�es,
FAITES-LE MAINTENANT!   -c, --console BOOL�EN	Activer ou non la console (d�faut 1)
   -g, --ggz		Active le mode GGZ
   -g, --ggz		Activer le mode GGZ (d�faut NON)
   -h, --help		Liste les options
   -n, --name NOM	Utilise NOM comme nom
   -p, --port PORT	Assigner le serveur au port PORT (d�f: 2000)
   -p, --port PORT	Se connecte au port PORT du serveur
   -q, --quiet		n'affiche pas de messages
   -s, --seed GRAINE		Nouvelle graine de g�n�rateur al�atoire
   -s, --server HOST	Se connecte au serveur HOST
   -v, --version		Affiche la version du programme
 %s, j'esp�re que tu aura une meilleure chance la prochaine fois %s arm�es: %d Variable %s positionn�e a: %s
 Variable %s positionn�e a: (%d,%d)
 %s(%s) attaque %s(%s) %s, Est-tu un robot? %s, As-tu le num�ro de t�l�phone de Lewinsky? %s, As-tu une soeur ? %s, je n'y crois pas ! %s, tu joues bien %s, tu ne sais vraiment pas jouer ! 1.Regroupement 1�re partie:
Placez %d arm�es dans vos pays 2.Tirer une carte 2�me partie:
Placez %d arm�es dans vos pays 3.Finir le tour Un pays a �t� conquis Un nouveau pays Un joueur a perdu la partie Un joueur � gagn� la partie Afrique Et pour finir, placez %d autres arm�es dans vos pays
 Arabie Argentine Arm�es Arm�es � d�placer Asie Attaquer � nouveau Australie Born�o Br�sil Californie Cartes Chili Chine Choupette Logs effac�s
 Client avec version de protocole incompatible (serveur:%d client:%d)
 Colombie Couleur Commande '%s' non reconnue
 Se connecter Se connecter au serveur Conqu�rir 2 pays d'Oc�anie,
2 d'Afrique, 2 d'Am�rique du Sud,
3 d'Europe, 4 d'Am�rique du Nord,
et 3 d'Asie Conqu�rir 30 pays Conqu�rir l'Afrique,
5 pays d'Am�rique du Nord et 
4 pays d'Europe Conqu�rir l'Asie et
2 pays d'Am�rique du Sud Conqu�rir l'Europe,
4 pays d'Asie, et
2 pays d'Am�rique du Sud Conqu�rir l'Am�rique du Nord,
2 pays d'Oc�anie et
4 pays d'Asie Conqu�rir l'Oc�anie,
conqu�rir l'Afrique et,
5 pays d'Am�rique du Nord Conqu�rir l'Oc�anie,
conqu�rir l'Am�rique du Nord, et
2 pays d'Europe Conqu�rir l'Am�rique du Sud,
7 pays d'Europe et 
3 pays limitrophes Conqu�rir l'Am�rique du Sud,
conqu�rir l'Afrique, et
4 pays d'Asie Conqu�rir le monde Jouer � la conqu�te du monde ou avec des objectifs secrets Pays Cartes Diminuer le facteur de zoom de 5% Pays de destination: '%s'. Maintenant vous devez s�l�ctionner le nombre d'arm�es � d�placer D�s: %s: %d %d %d contre %s: %d %d %d Protocoles de versions diff�rentes. Serveur:%d Client:%d D�connexion du serveur Montrer la planisph�re � l'�chelle 1:1 Est-ce que quelqu'un parle l'Esperanto? Ne joue pas, observe seulement la partie �gypte Activer le mode GGZ Erreur en cr�ant un canevas
 Erreur en cr�ant une image
 Erreur dans clitok_ataque() Erreur dans clitok_dados() Erreur dans clitok_error() Erreur dans clitok_exit() Erreur dans clitok_fichas() Erreur dans clitok_fichas2() Erreur dans clitok_fichasc() Erreur dans clitok_ggz() Erreur dans clitok_lost() Erreur dans clitok_message() Erreur dans clitok_newplayer() Erreur dans clitok_objetivo() Erreur dans clitok_ok() Erreur dans clitok_pais() Erreur dans clitok_paises() Erreur dans clitok_playerid() Erreur dans clitok_pversion() Erreur dans clitok_start() Erreur dans clitok_status() Erreur dans clitok_tarjeta() Erreur dans clitok_tropas() Erreur dans clitok_turno() Erreur dans clitok_winner() Erreur dans out_fichas() Erreur au d�marrage. Y'a-t-il au moins 2 joueurs ? Error d'affectation de la variable %s.
 Erreur lors de la connexion au client GGZ Erreur lors de la connection au serveur '%s' au port %d Erreur, le pays '%s' n'a pas d'arm�es disponibles pour un d�placement Erreur, '%s' n'est pas limitrophe de '%s' Erreur, le pays '%s' ne vous appartient pas Erreur, ce n'est pas le moment de regrouper Erreur, impossible d'initialiser un nouveau tour
 Erreur, ce n'est pas le moment de tirer une carte Erreur, ce n'est pas le moment de placer les 2 arm�es. Erreur, ce n'est pas votre tour. Erreur, ce n'est pas le moment de changer les cartes Erreur, ce n'est pas le moment d'envoyer les arm�es Erreur, ce n'est pas le moment d'envoyer les arm�es Erreur, placez un nombre correct d'arm�es Erreur, les 2 arm�es ont d�ja �t� plac�es Erreur, les cartes doivent �tre ou toutes �gales ou toutes diff�rentes Erreur, la partie n'a pas encore commenc� Erreur, impossible de changer de type de partie, car la partie est en cours Erreur innatendue dans reagrupe_click(). Signalez ce bug Erreur, vous �tes d�j� connect� Erreur, ce n'est pas le moment de regrouper vos arm�es Erreur, vous ne pouvez pas ajouter des arm�es maintenant Erreur, vous ne pouver pas mettre plus de %d arm�es Erreur, ce n'est pas le moment de regrouper vos arm�es Erreur, vous ne pouver pas d�sarmer maintenant Erreur, vous �tes devez �tre connect� �thiopie Europe Echanger L'�change a �t� valid�. Vous pouver placer maintenant %d arm�es suppl�mentaires ! Quitter Quitter TEG Arm�es suppl�mentaires FAUX Erreur fatale: N'executer pas ce programme en temps que superutilisateur
 Mode GGZ activ�.
 Options du jeu Partie termin�e Partie termin�e. Le joueur %s(%s) est le vainqueur

L'objectif atteint �tait:
%s Partie termin�e, Vous �tes le vainqueur!

Votre objectif atteint �tait:
%s Partie avec un seul joueur. Le joueur %s(%d) est le vainqueur
 Partie sans joueurs. Initialisation d'une autre partie.
 Allemagne Tirer une carte de pays Tirer une carte Bonne chance %s Au revoir %s Au revoir.
 Grande Bretagne Gro�nland Bonjour %s Comment vas-tu %s? J'aime ce jeu Je suis un l�che Je suis le joueur num�ro: %d Islande Avant Augmenter le facteur de zoom de 5% Inde Isra�l Cela vous rend plus heureux Affiche les informations sur les pays Italie C'est votre tour C'est votre tour d'attaquer C'est � vous d'attaquer ! Japon Girafe Lancer un robot Malaisie Configuration des messages M�xique Mongolie D�placement d'arm�es de '%s' a '%s'
 Pr�sident Ma couleur est le: %s Nom Nom: Am�rique du Nord Pas encore impl�ment�
 Maintenant vous pouvez ajouter %d arm�es suppl�mentaires dans vos pays! Num�ro Observe la partie, sans jouer Oc�anie Or�gon Arri�re Il manque un param�tre pour "%s".
 Passer votre tour Perle Perou Place %d arm�es en %s
 Placez %d arm�es dans vos pays
 Place des arm�es Jouer � la conqu�te du monde Jouer avec des objectifs secrets Le joueur %d a abandonn� la partie Le joueur %d a quitt� la partie Le joueur %s(%d) a abandonn� la partie
 Le joueur %s(%d) a la couleur %s
 Le joueur %s(%d) est le vainqueur. Partie termin�e
 Le joueur %s(%d) a perdu la partie
 Le joueur %s(%d) a quitt� la partie
 Le joueur %s(%s) a abandonn� la partie Le joueur %s(%s) A chang� 3 cartes en %d arm�es Le joueur %s(%s) a quitt� la partie C'est au tour du joueur %s(%s) d'attaquer ! Le joueur %s(%s) place %d arm�es Le joueur %s(%s) place %d arm�es pour la premi�re fois Le joueur %s(%s) place %d arm�es pour la deuxi�me fois Le joueur %s(%s) est le VAINQUEUR Le joueur %s(%s) a perdu la partie Le joueur %s(%s) a perdu la partie
 Le joueur a perdu la partie Le joueur [%d] '%s' s'est connect� avec la couleur %s �tat des joueurs Pologne Place 2 arm�es Re_grouper les arm�es Abandon ? Regrouper vos arm�es Regrouper vos arm�es, en les bougeant de pays limitrophes Regroupement Regroupement des arm�es Demande d'objectif... R�initialiser l'attaque Retourne un nombre pas si al�atoire que �a Nom du robot: %s Robot: Erreur anormale dans ai_fichas() Robot: Erreur anormale dans ai_fichasc() Robot: Erreur lors de la connection au serveur Robot: Erreur inattendue dans canej_out() Russie Objectif secret Choisir cette carte S�l�ctionnez 2 pays:
1er: Cliquez sur le pays de d�part
2�me: Cliquez sur le pays d'arriv�e Choisissez le type de partie Choissisez votre couleur Choissisez votre couleur Envoyer des arm�es Envoyer les arm�es r�cement plac�es Envoyer vos arm�es Options du serveur Nom du serveur: Port du serveur: Param�trer le nombre initial d'arm�es � placer Param�trer la graine de g�n�rateur al�atoire Affectation de la variable %s  a: %s -> %s
 Afficher les messages d'erreur Afficher les messages importants Afficher les messages informatifs Afficher les messages des joueurs Afficher les messages des joueurs avec les couleurs Montrer les arm�es que les autres joueurs ont plac�es Affiche le param�trage Sib�rie Les pays de d�part et d'arriv�e sont le m�me. R�initialisation du regroupement... Pays de d�part: '%s'. Maintenant choisissez le pays de destination Afrique du Sud Am�rique du Sud Espagne Serveur en mode autonome
 Commencer Commencez une nouvelle attaque, en s�l�ctionnant les pays de d�part et d'arriv�e Lancer un serveur local Commencer la partie �tat Abandon Su�de Site de TEG Pr�f�rences de TEG Version du serveur TEG  VRAI Tartarie Tamir Tenes Empanadas Graciala - Robot v%s - de Ricardo Quesada
 Terre-neuve Merci L'attaque a �t� r�initialis�e. Veuillez choisi le pays d'o� doitcontinuer votre attaque La partie a d�ja commenc�. Connectez-vous comme observateur. Le serveur est plein :( Connexion en tant qu'observateur. Le serveur a signal� une erreur dans '%s' Token '%s' non trouv� Essayez avec un utilisateur diff�rent
 Turquie Tapez '%s' pour plus d'aide
 Tapez '%s' pour plus d'aide
 Option non reconnue: "%s"
 Usage: %s [options...]
Les options valides sont:
 Graine utilis�e : %u
 Voir vos cartes Voir les _d�s Voir votre _objectif Voir les _joueurs Voir toutes les cartes que vous avez gagn�es Voir les joueurs Voir l'�tat des joueurs Voir le r�sultat des d�s Voir l'�tat des autres joueurs Voir votre objectif secret Rebonjour %s Bienvenue dans TEG ! Qui a commenc� Je souhaites aussi un monde meilleur ! Vous �tes le vainqueur Vous avez conquis '%s' lors de votre attaque depuis '%s'!
 Vous n'avez pas encore de cartes Vous avez perdu Vous avez perdu la partie :( Vous �tes devez �tre connect� Vous avez re�u la carte: '%s' Vous avez re�u la carte: '%s'. Vous pouvez y placer 2 arm�es Votre objectif Votre objectif est: %s Za�re Zoom avant Zoom arri�re [Nota: Le serveur a d�plac� 1 arm�e vers '%s' pour vous.
Maintenant choisissez combien d'arm�es suppl�mentaires vous souhaitez rajouter] [Nota: Vous pouvez regrouper autant de fois que vous le
voulez, � condition de ne pas regrouper des arm�es qui
ont d�ja boug� ce tour ci. [Ceci est l'objectif commun � tous.] [Ceci est votre objectif secret.] Actions _Connexion _D�connection _Finir le tour _Lancer robot _R�-attaquer Act_ualiser la planisph�re attaque noir bleu fermeture de la connexion
 version du client connect� d�connect� autoris� fin du tour erreur %d (compil� sans sterror) �change de cartes quitte la partie partie termin�e tire une carte vert Zzz usage interne. Ne pas utiliser usage interne. Ne pas utiliser. �laboration d'un pacte d�placement d'arm�es ? observateur autre rose placement des arm�es placement des arm�es 2 placement des arm�es 3 joueurs :%d, connexions :%d, joueur num�ro :%d, objectifs :%s, r�gles:%s
 post arm�es post arm�es 2 post arm�es 3 version du protocole rouge regroupement pour demander un objectif sauvegarder la partie envoie un message � tout les joueurs version du serveur Param�trage affiche l'aide affiche l'�tat des joueurs affiche l'�tat des joueurs commenc�e commencement du tour tegserver: Erreur anormale dans select()
 pour demander de l'aide pour attaquer un pays pour commenter quelque chose pour changer des cartes en arm�es pour quitter la partie pour finir votre tour pour tirer une carte pour placer 2 arm�es dans le pays vous appartenant correspondant � la carte. pour placer les arm�es � la fin du tour pour placer les 3 arm�es initiales pour placer les 5 arm�es initiales pour s'inscrire comme un nouveau joueur pour me rappeler quoi faire pour r�organiser les arm�es pour choisir une couleur pour envoyer un message aux autres joueurs pour envoyer des arm�es en pays conquis pour cr�er un callback asynchrone pour param�trer les options pour commencer la partie pour commencer la partie pour abandonner jaune 