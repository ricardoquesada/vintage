��    �     �  w  |  :   X'  >   �'     �'  /   �'  -    (  #   N(  >   r(  .   �(  "   �(  &   )  2   *)  *   ])      �)     �)     �)     �)     �)     *     (*  %   >*     d*  "   *     �*     �*     �*     �*     �*     +     (+     ?+     ^+     v+  	   �+     �+  %   �+  	   �+     �+     �+     ,  
   ,  +   #,     O,  +   [,     �,  !   �,     �,     �,     �,     �,  !   -  $   *-  $   O-     t-     |-  .   �-     �-     �-     �-     �-     �-  :   �-  @   2.     s.     �.     �.     �.     �.  B   �.     /     /     %/     -/  o   ?/     �/  F   �/  -   0  ;   90  E   u0  A   �0  A   �0  E   ?1  >   �1     �1  /   �1     2     2     52     M2     h2  	   �2     �2     �2     �2     �2     �2     �2     3     !3  D   <3  $   �3     �3     �3     �3     �3  0   �3     .4     E4     b4      4     �4     �4  $   �4     �4     �4     5     5     25     J5     b5     y5     �5     �5     �5     �5     �5     6     )6     D6     Y6     p6     �6     �6     �6     �6     �6     
7     #7     ;7     T7  -   j7     �7  +   �7  7   �7  4   8  #   M8  &   q8  #   �8  !   �8     �8  '   �8     $9  (   A9  )   j9     �9  /   �9  -   �9  &   :  '   4:  '   \:  4   �:     �:  >   �:  =   ;      U;  (   v;     �;  '   �;     �;     <     "<     ?<     F<  5   O<     �<     �<     �<     �<  5   �<     �<     �<     �<  	   =  B   =  H   R=  @   �=  2   �=  1   >     A>     I>     \>     e>  
   r>  	   }>     �>  	   �>     �>     �>     �>     �>     �>     �>     �>  
   �>     �>     ?     ?     ?     8?     =?  !   U?     w?     }?     �?     �?     �?     �?     �?     �?     �?      �?     @      @     0@     5@     ;@     D@     _@     m@  2   �@     �@     �@     �@     �@     �@     �@     �@     A     =A     BA  "   YA     |A     �A     �A     �A     �A  "   �A  #   B     9B  !   RB     tB  #   �B  '   �B     �B     �B      C  -   7C     eC  %   �C  "   �C  /   �C  /   �C     *D     FD     bD     D  *   �D     �D     �D     �D     �D     �D     E     E  7   (E  
   `E     kE     }E     �E      �E     �E     �E  $   �E  %   F     :F  .   WF  &   �F     �F     �F     �F     �F     �F  \   �F     FG     ZG     lG     �G     �G     �G     �G     �G     �G  )   �G     H  !   .H     PH     dH     |H     �H  !   �H  -   �H     �H  G   I  8   ZI     �I     �I     �I     �I     �I  G   �I     J     +J  '   :J     bJ  	   iJ     sJ     zJ     �J     �J     �J     �J  @   �J  :   �J     5K  O   <K  M   �K  5   �K  C   L  "   TL     wL     �L     �L     �L     �L     �L  )   �L     M  "   #M     FM     MM     aM     zM  *   �M     �M  *   �M     N     N     5N     AN     MN     [N     iN     �N     �N     �N     �N     �N     O     O     %O     +O     7O  &   OO  9   vO  &   �O  #   �O  %   �O     !P  .   4P     cP  -   P     �P     �P     �P     �P  7   �P     4Q     AQ     UQ     [Q     cQ  g   lQ  �   �Q     UR     sR     �R     �R     �R  	   �R     �R     �R     �R  	   �R     �R     �R     �R  	   S     S     S      S  $   ,S     QS     bS  	   qS     {S     �S     �S     �S     �S     �S     �S     �S  L   �S     KT     TT     ZT     _T     nT     T  A   �T  
   �T     �T     �T     �T     U  
   U     U     )U  "   7U     ZU     iU  
   vU     �U     �U     �U     �U  &   �U     �U     V     V  !   /V     QV     bV     vV  7   �V  .   �V     �V     W     5W     MW     eW     |W     �W  %   �W     �W     �W     �W     �W     X     X     +X  9   2X  >   lX      �X  .   �X  #   �X  ,   Y  E   LY  4   �Y  #   �Y  /   �Y  .   Z  1   JZ  0   |Z     �Z     �Z     �Z     �Z     [     0[  *   C[     n[  #   �[     �[     �[     �[     �[     \     \     (\     ?\     \\     t\     �\     �\     �\     �\  
   �\     �\     ]     ]  )    ]     J]  )   Z]     �]  &   �]     �]     �]     �]     �]     ^  %   5^  %   [^  	   �^     �^  3   �^     �^  	   �^     �^     �^     �^     _  ;   %_     a_     x_     �_     �_     �_  E   �_      `     `     "`     +`  k   B`     �`  B   �`  )   a  >   0a  @   oa  B   �a  B   �a  J   6b  =   �b     �b  4   �b     c     #c     =c     Wc     tc     �c     �c     �c     �c     �c     �c     d     d     6d  J   Qd  $   �d     �d     �d     �d     e  7    e     Xe  !   se      �e  $   �e     �e     �e  .   �e     "f     5f     Kf     af     zf     �f     �f     �f     �f     �f     g     $g     ;g     Ug     qg     �g     �g     �g     �g     �g     h     h     8h     Rh     kh     �h     �h  -   �h     �h  0   �h  4   .i  =   ci  $   �i  #   �i  #   �i  .   j  $   =j  4   bj  $   �j  (   �j  0   �j     k  .   ,k  .   [k  +   �k  -   �k  &   �k  1   l     =l  K   ]l  8   �l     �l  .   �l  &   +m  *   Rm      }m  '   �m      �m     �m     �m  <   �m     3n     9n     Fn     Vn  @   \n     �n     �n     �n     �n  I   �n  O   &o  D   vo  ;   �o  /   �o     'p     0p     Jp     Xp     hp     wp     �p     �p     �p     �p     �p     �p     �p     �p     �p     q     q     (q     1q     9q     Tq     Yq  !   kq     �q     �q     �q     �q     �q     �q     �q     �q     r     r     (r     7r     Gr     Nr  
   Vr     ar     wr     �r  4   �r     �r     �r     �r     s     
s     s     s     :s     Xs     ]s     us     �s     �s     �s      �s     �s  #   t  $   ?t     dt  $   �t  $   �t  '   �t  1   �t  "   &u     Iu  #   iu  4   �u  !   �u  ,   �u  ,   v  6   >v  8   uv     �v  !   �v  "   �v     w  3   .w     bw     vw     ~w     �w     �w     �w     �w  8   �w     
x     x     ,x     Ax     Tx     rx     �x  #   �x  $   �x     �x  4   
y  &   ?y     fy     ly  
   }y     �y     �y  Y   �y      z     z     -z     Az     Rz     rz     �z     �z     �z  -   �z     �z  "   {     2{     N{     k{     �{  (   �{  (   �{     �{  A   |  3   Y|  
   �|     �|     �|     �|     �|  <   �|     }     )}  +   ;}     g}  	   n}     x}     }     �}     �}  	   �}     �}  B   �}  ;   ~     N~  N   V~  ?   �~  ?   �~  =   %  $   c     �     �     �     �     �     �  %   �     5�      ;�     \�     d�     ��     ��  2   ��     �  8   �     F�     Z�     y�  
   ��     ��     ��     ��     Ё     ށ     ��     �     :�     R�     `�     t�     y�     ��  (   ��  <   ˂  +   �  (   4�  (   ]�     ��  /   ��     Ƀ  .   �     �     �     3�     M�  ;   k�     ��     ��     Ƅ     ̄     ل  e   �  y   K�     Ņ     ߅     ��     ��     �     �     +�  
   9�     D�     U�     ^�     d�     i�  	   }�     ��  
   ��     ��      ��     ҆     �     �     �     �     �     /�     E�     a�     p�  
   ��  O   ��  
   އ     �     �     �     �     �  C   0�     t�     ��     ��     ��     ��     ��     ̈     �  &   �     �     /�     A�     O�  "   o�     ��     ��  %   ��     щ     �     ��  #   	�     -�     B�     Z�  B   m�  :   ��  !   �  !   �     /�     O�     i�     ��  &   ��  +   ǋ  !   �     �     *�     @�     V�     e�     r�  E   x       6             p         �  �  .  �   �  �  �      �   �   �  �  �        �   �  C      �   �   |      ,  �  �   c          N  �  �   Z   R   �           (   8  �   v   �       �  �     �  �   m   n   �   �      �       �  w          �   �   _   s      �   :   �  <  k  E  O          #   �   �  h  �   d  -   �  F             L     �   �   f  !  �  B      u  m    �   V  ?          �  A   �           "   y      h   &  �   �   �   3  �  �   �    �   �     A  �  �   �  �   �       �  �  �         ;    �  �       V   �       i       �  �  �      �      �   �     4  *  W       �   �   {   �   a               B   �  x  l  0           �           �   Y  `   �   �   �      �       r      �   �  �   %      �             �  �   �  �          \   �   S   /   '       �  .   �   >   O   �  �       �  �  ]   Q  S      �    �               �  u   G              t  �   Q   +   o   7   5           c   {     )   �           P  �   \     �      *   �  	       C           1   i      2       �  �      I     �   �       �   �       H   +  �       �   �   �   �    �    �   �          �                            �  �             |   	  �       Z  �  �           �  �  �       �   r          #     �   @   j   �  5            "  $         
         g                 �   �  �   �  �   �     J  L   t   �          �   n  R  (  �  
   �   �       �   ^  �  _           �   z  o  ?   ~  �  >      �      b       3   b      �       �   �   �      W  �     �   �         D         �   `  X   '  �   ,   �  6  �                 �   G   U   4   K  �   �               T   �  p   �             �   �             d   �   w       q  e     �        7      �   k      �   /              �     q           �  �              [  $      s   �     �         J                         �  �      !   1  �   �   �         H      �   =        8   M      �     Y          :  }   N                   �   �      &           �   �      �  �   ~   �           �   �   �       l   <   2     �       �   �  �   �   T          �            ;   f   [   ]  �  �  F  0  P   �  �   y       g   j  �  U  M   �  }  �  z   �      �  �       @  �   X  -  �      e   I           9       a      9          =       �  K   )  �  �   %               �  v  D  ^       �   �  �     �   �   
If you want to EXCHANGE your cards for armies,
DO IT NOW!   -c, --console BOOLEAN	Enable the console or not (default 1)
   -g, --ggz		Enable GGZ mode
   -g, --ggz		Enable the GGZ mode (default OFF)
   -h, --help		Print a summary of the options
   -n, --name NAME	Use NAME as name
   -p, --port PORT	Bind the server to port PORT (default 2000)
   -p, --port PORT	Connect to server port PORT
   -q, --quiet		dont show messages
   -s, --seed SEED	New seed for random
   -s, --server HOST	Connect to the server at HOST
   -v, --version		Print the version number
 %s Better luck for the next time %s armies: %d %s variable is set to: %s
 %s variable is set to: %u
 %s variable is set to: (%d,%d)
 %s(%s) is attacking %s(%s) %s, Are you a robot ? %s, Are you sure what you are saying? %s, Are you talking to me? %s, Do you have Lewinsky's phone ? %s, Do you have a sister ? %s, Do you think so? %s, I cant believe it! %s, I dont agree %s, I dont think so %s, I dont understand you %s, I'll be the winner %s, Were you here last night ? %s, What are you saying %s, What can I say ? %s, What? %s, You play very well %s, You really dont know how to play! %s, funny %s, sure %s, try to improve your game %s, you make me laugh 1. Regroup 1st part:
Place %d armies in your countries 2. Get card 2nd part:
Place %d armies in your countries 3. End turn A clone of T.E.G. (a Risk clone). A country was conquered A new country A player lost the game A player won the game Abnormal error in token_exit(%d)
 Abnormal error in turno_2nextplayer
 Abnormal error in turno_2prevplayer
 Address Africa And place another %d armies in your countries
 Aral Armies Armies to move Armies_pos has no x
 Armies_pos has no y
 Attack your enemy, again using same source and destination BUG: The server beleives that player `%s' does not have the turn Board has no name
 Bound to port: %d
 Brazil Cards Cleared output window.
 Client with incompatible protocol version (server:%d , client:%d)
 Color Command '%s' not recongnized
 Connect Connect to server Conquer 2 countries of Oceania,
2 of Africa, 2 of South America,
3 of Europe, 4 of North America,
and 3 of Asia Conquer 30 countries Conquer Africa,
5 countries of North America and
4 countries of Europe Conquer Asia and
2 countries of South America Conquer Europe,
4 countries of Asia and,
2 of South America Conquer North America,
2 countries of Oceania and
4 countries of Asia Conquer Oceania,
conquer Africa and,
5 countries of North America Conquer Oceania,
conquer North America and,
2 countries of Europe Conquer South America,
7 countries of Europe and
3 frontier countries Conquer South America,
conquer Africa and,
4 countries of Asia Conquer the world Conquer the world, or play with secret missions Continent has no name
 Continent has no pos_x
 Continent has no pos_y
 Could not find the %s file Couldn't open '/dev/random'
 Countries Country Cards Country has no army_x
 Country has no army_y
 Country has no file
 Country has no name
 Country has no pos_x
 Country has no pos_y
 Decrease zoom factor by 5% Destination country: '%s'. Now select the quantity of armies to move Dices: %s: %d %d %d vs. %s: %d %d %d Dices_file has no %d
 Dices_pos has no x
 Dices_pos has no y
 Dices_text has no color
 Different protocols version. Server:%d Client:%d Disconnect from server Display the map at 1:1 scale Does anyone speak Speranto ? Dont play, just observe the game Egypt Empty document
 Enable/Disable common secret mission Enables GGZ mode Error creating canvas
 Error creating image
 Error in clitok_ataque() Error in clitok_dados() Error in clitok_error() Error in clitok_exit() Error in clitok_fichas() Error in clitok_fichas2() Error in clitok_fichasc() Error in clitok_ggz() Error in clitok_lost() Error in clitok_message() Error in clitok_newplayer() Error in clitok_objetivo() Error in clitok_ok() Error in clitok_pais() Error in clitok_paises() Error in clitok_playerid() Error in clitok_pversion() Error in clitok_start() Error in clitok_status() Error in clitok_tarjeta() Error in clitok_tropas() Error in clitok_turno() Error in clitok_winner() Error in out_fichas() Error in start. Are there at least 2 players? Error setting %s variable.
 Error while trying to connect to GGZ client Error while trying to connect to server '%s' at port %d Error, '%s' doesnt have any avalaible armies to move Error, '%s' isnt frontier with '%s' Error, '%s' isnt one of your countries Error, It's not the time to regroup Error, cant initilize a new turn
 Error, couldn't find file: %s Error, couldn't find file: objetivo.png Error, couldn't find file:%s Error, it's not the moment to get a card Error, it's not the time to put 2 armies. Error, it's not your turn. Error, its not the moment to exchange the cards Error, its not the moment to send your armies Error, its not the time to send armies Error, put the correct number of armies Error, the 2 armies where placed before Error, the cards must be all equal, or all different Error, the game is not started Error, the game is started so you cant change the type of game Error, unexpected error in reagrupe_click(). Report this bug! Error, you are already connected Error, you can't regroup your armies now Error, you cant add armies now Error, you cant put more than %d armies Error, you cant regroup now. Error, you cant sub armies now Error, you must be connected Europe Exchange Exchanged approved. Now you can place %d more armies! Exit Exit TEG Extra armies FALSE Fatal error: You can't run this program as superuser
 France GGZ mode activated.
 Game Options Game Over Game over, A player won the game

The accomplished mission was:
%s Game over, Player %s(%s) is the winner

The accomplished mission was:
%s Game over, you are the winner!

The accomplished mission was:
%s Game with one player. Player %s(%d) is the winner
 Game without players. Initializing another game.
 Germany Get a country card Get card Good luck %s Goodbye %s Goodbye.
 Great Britain Greenland Hey %s! Hi %s How are you %s ? Human I love this game I'm a bit tired I'm a coward I'm hungry I'm player number:%d Iceland In Increase zoom factor by 5% Iran It makes you more happy It shows info about the countries Italy Its your turn Its your turn to attack Its your turn to attack!! Japan Launch a robot Malaysia Message Options Mexico Moving armies from '%s' to '%s'
 Mr.President My color is: %s Name Name: New York No %s, I dont believe that North America Not yet implemented
 Now you can add %d extra armies in your countries! Number Observe the game, dont play it Oceania Oregon Out Out of memory
 Parameter is missing for "%s".
 Pass the turn to another player Peru Place %d armies in %s
 Place %d armies in your countries
 Place armies Play to conquer the world Play with secret missions Player %d abandoned the game Player %d exit the game Player %s was kicked from the game Player %s was kicked from the game
 Player %s was not found
 Player %s(%d) abandoned the game
 Player %s(%d) has color %s
 Player %s(%d) is connected from %s
 Player %s(%d) is the winner! Game Over
 Player %s(%d) lost the game
 Player %s(%d) quit the game
 Player %s(%s) abandoned the game Player %s(%s) exchanged 3 cards for %d armies Player %s(%s) exit the game Player %s(%s) has the turn to attack! Player %s(%s) is placing %d armies Player %s(%s) is placing %d armies for 1st time Player %s(%s) is placing %d armies for 2nd time Player %s(%s) is the WINNER Player %s(%s) lost the game Player %s(%s) lost the game
 Player lost the game Player[%d] '%s' is connected with color %s Players status window Poland Preview Put 2 armies Re_group armies Really surrender ? Regroup your armies Regroup your armies, moving from one country to another Regrouping Regrouping armies Requesting a mission... Reset the attack Returning a not so random number Robot intelligence: %d%% Robot name: %s Robot: Abnormal error in ai_fichas() Robot: Abnormal error in ai_fichasc() Robot: Error in gui_fichas() Robot: Error while trying to connect to server Robot: Unexpected error in canje_out() Russia Secret mission Select Select theme: Select this card Select two countries:
1st: click on the source country
2nd: click on the destination country Select type of game Select your color Select your desired color Send armies Send the armies recently placed Send your armies Server Options Server name: Server port: Set the initial number of armies to place Set the seed for random Setting %s variable to: %s -> %s
 Show Error Messages Show Important Messages Show Informative Messages Show Players Messages Show Players Messages with colors Show the armies that others player had placed Shows the set options Source country is the same as the destination. Resetting the regroup... Source country: '%s'. Now select the destination country South Africa South America Spain Standalone server.
 Start Start again selecting the source and destination country for the attack Start server locally Start the game Starting game number: %d with seed: %u
 Status Surrender Sweden TEG Home Page TEG Preferences TEG server version  TRUE Tartary Tenes Empanadas Graciela - Gnome client v%s - by Ricardo Quesada Tenes Empanadas Graciela - Robot v%s - by Ricardo Quesada
 Thanks The attack was reset. Please, select the source country to continue your attack The game has already started :( Try connecting as an observer. Go to settings The game has already started. Connect as an observer. The server is full :( Try connecting as an observer. Go to settings The server report an error in '%s' Theme:Author is empty
 Theme:Email is empty
 Theme:Screenshot is empty
 Theme:version is empty
 Themes Token '%s' not found Tomorrow, not today. %s dont get me wrong Total Try to run it as a different user
 Turkey Type '%s' for help
 Type '%s' for more help
 Unable to load theme `%s'
 Unknow option. Try using `%s %s' for help
 Unrecognized option: "%s"
 Usage: %s [option ...]
Valid options are:
 Using seed: %u

 Using theme `%s - v%s' by %s
 View _cards View _dices View _mission View _players View all the card that you won View players View players status View the result of the dices View the status of the players View your mission of this game Welcome back %s! Welcome to TEG! What? Who started Who wants an Empanada ? Wrong type (%s). Nothing was expected
 Wrong type (%s). continent, board and dices was expected
 Wrong type (%s). country was expected
 Wrong type. root node != teg_theme
 Yes, why not ? %s, but are you sure ? You are the winner You conquered '%s' in your attack from '%s'!!
 You dont have any cards yet You have to restart TEG to use the new theme. You lost You lost the game :( You need to be connected You received card: '%s' You received card: '%s' and 2 armies where placed there Your mission Your mission is: %s Yukon Zoom In Zoom Out [Note: The server had move one army to '%s' for you.
Now choose, how many more armies you want to move] [Note: You can regroup as many times as you want,
as long as you dont regroup an armie that were
regrouped before in this turn.] [This is the common mission.] [This is your secret mission.] _Actions _Connect _Disconnect _End turn _Launch robot _Reattck _Update Map attacking black blue client version connected disconnected enabled ending turn error %d (compiled without strerror) exchanging cards exits the game game over gettting card green internal use. Dont use it internal use. Dont use it. kick player from the game making a pact moving armies nod number, countries, armies, cards, exch, name, human, color, status, address
 observer other pink placing armies placing armies 2 placing armies 3 players:%d, connections:%d, game number:%d, mission:%s, rules:%s
 postarmies postarmies 2 postarmies 3 protocol version red regrouping request a mission save the game sends a message to all the players server version sets options shows help shows status of players shows the status of the players started starting turn tegserver: Abnormal error in select()
 to ask for help to attack a country to comment a command to exchange your cards for armies to exit the game to finish your turn to pick up a country-card to place 2 armies in a country. You must have that card to place the armies after a turn have finished to place the initials 3 armies to place the initials 5 armies to register as a player to remind me what to do to reorder your armies to select a color to send a message to send armies to a conquered country to set an async callback to set options to start playing to start the game to surrender view options yellow 
Si quieres CANJEAR tarjetas por ej�rcitos,
�HAZLO AHORA!   -c, --console BOOLEAN	Habilitar la consola o no (defecto 1)
   -g, --ggz		Habilitar modo GGZ
   -g, --ggz		Habilitar modo GGZ (defecto OFF)
   -h, --help		Muestra las opciones
   -n, --name NOMBRE	Usar NOMBRE como nombre
   -p, --port PUERTO	Bindear el servidor al puerto PUERTO (def: 2000)
   -p, --port PUERTO	Conectar al PUERTO del servidor
   -q, --quiet		no mostrar mensajes
   -s, --seed SEMILLA		Usar SEMILLA para random
   -s, --server HOST	Conectar al servidor HOST
   -v, --version		Muestra al versi�n del programa
 %s, ojala que tengas mejor suerte la pr�xima vez %s ej�rcitos: %d Variable %s seteada a: %s
 Variable %s seteada a: %u
 Variable %s seteada a: (%d,%d)
 %s(%s) esta atacando %s(%s) %s, �Sos un robot? %s, Estas seguro de lo que estas diciendo? %s, �Me estas hablando a mi? %s, �Tenes el telefono de Lewinsky? %s, �Tenes hermanas? %s, �Estas seguro? %s, No lo puedo creer %s, no estoy de acuerdo %s, no creo eso %s, No te entiendo %s, yo ser� el ganador %s, �Donde estuviste anoche? %s, Que estas diciendo? %s, �Que te puedo decir? %s, Que? %s, �Qu� bien que jugas! %s, vos si que no sabes jugar %s, gracioso %s, seguro %s trata de mejorar tu juego %s, me haces reir 1.Reagrupar 1ra parte:
Pon %d ej�rcitos en tus paises 2.Sacar tarjeta 2da parte:
Pon %d ej�rcitos en tus paises 3.Finalizar turno Un clone de T.E.G. (un clone de Risk). Un pa�s fue conquistado Un nuevo pa�s Un jugador perdi� el juego Un jugador gan� el juego Error anormal en select (%d)
 Error anormal en turno_2nextplayer()
 Error anormal en turno_2prevplayer()
 Direccion Africa Y finalmente, pon otros %d ej�rcitos en tus paises
 Aral Ej�rcitos Ej�rcitos a mover Armies_pos no tiene x
 Armies_pos no tiene y
 Atacar otra vez BUG: El servidor cree que el jugador '%s' no tiene el turno Board no tiene nombre
 Bindeado al puerto: %d
 Brasil Tarjetas Ventana de salida limpia
 Cliente con versi�n de protocolo incompatible (server:%d cliente:%d)
 Color Comando '%s' no reconocido
 Conectar Conectarse al servidor Conquistar 2 paises de Ocean�a,
2 de Africa, 2 de Sud Am�rica,
3 de Europa, 4 de Norte Am�rica,
y 3 de Asia Conquistar 30 paises Conquistar Africa,
5 paises de Norte Am�rica y 
4 paises de Europa Conquistar Asia y
2 paises de Sud Am�rica Conquistar Europa,
4 paises de Asia, y
2 paises de Sud Am�rica Conquistar Norte Am�rica,
2 paises de Ocean�a y
4 paises de Asia Conquistar Ocean�a,
conquistar Africa y,
5 paises de Norte Am�rica Conquistar Ocean�a,
conquistar Norte Am�rica, y
2 paises de Europa Conquistar Sud Am�rica,
7 paises de Europa y 
3 paises lim�trofes entre si Conquistar Sud Am�rica,
conquistar Africa, y
4 paises de Asia Conquistar el mundo Jugar a conquistar el mundo � con objetivos secretos Continent no tiene nombre
 Continent no tiene pos_x
 Continent no tiene pos_y
 No se encontr� el archivo %s No se pudo abrir '/dev/random'
 Paises Tarjetas Country no tiene army_x
 Country no tiene army_y
 Country no tiene file
 Country no tiene nombre
 Country no tiene pos_x
 Country no tiene pos_y
 Decrementar el zoom por 5% Pa�s destino: '%s'. Ahora seleccione la cantidad de paises que desea mover Dados: %s: %d %d %d vs. %s: %d %d %d Dices_file no tiene %d
 Dices_pos no tiene x
 Dices_pos no tiene y
 Dices_text no tiene color
 Protocolos con distinta versi�n. Servidor:%d Cliente:%d Desconectarse del servidor Mostar el mapa con una escala 1:1 �Alguno de Uds. habla Esperando? No jugar, tan solo observar el juego Egipto Documento vacio
 Habilita/Deshabilita el objetivo secreto com�n Habilitar modo GGZ Error creando canvas
 Error creando imagen
 Error en clitok_ataque() Error en clitok_dados() Error en clitok_error() Error en clitok_exit() Error en clitok_fichas() Error en clitok_fichas2() Error en clitok_fichasc() Error en clitok_ggz() Error en clitok_lost() Error en clitok_message() Error en clitok_newplayer() Error en clitok_objetivo() Error en clitok_ok() Error en clitok_pais() Error en clitok_paises() Error en clitok_playerid() Error en clitok_pversion() Error en clitok_start() Error en clitok_status() Error en clitok_tarjeta() Error en clitok_tropas() Error en clitok_turno() Error en clitok_winner() Error en out_fichas() Error al comenzar. �Hay al menos 2 jugadores? Error seteando variable %s.
 Error al tratar de conectarse con el cliente GGZ Error al conectarse al servidor '%s' en el puerto %d Error, el pa�s '%s' no tiene ej�rcitos disponibles para mover Error, '%s' no es lim�trofe con '%s' Error, El pa�s '%s' no te pertenece Error, no es el momento de reagrupa Error, no se puede inicializar un nuevo turno
 Error, no se encontro el archivo: %s Error, no se pudo encontrar el archivo: objetivo.png Error, no se encontro el archivo: %s Error, no es el momento de sacar tarjeta Error, no es el momento de poner los 2 ej�rcitos Error, no es tu turno Error, este no es el momento de hacer un canje Error, no es el momento de enviar de ej�rcitos Error, no es el momento de enviar ej�rcitos Error, pon el n�mero de correcto de ej�rcitos Error, los ej�rcitos ya fueron puestos Error, las tarjetas deben ser iguales o distintas Error, el juego no ha comenzado Error, no se puede cambiar el tipo de juego porque el juego ya ha comenzado Error no esperado en reagrupe_click(). Reportar este bug Error, ya estas conectado Error, no puedes reagrupar tus ej�rcitos ahora Error, no puedes poner ej�rcitos ahora Error, no puedes poner m�s de %d ej�rcitos Error, no puedes reagrupar ahora Error, no puedes quitar ej�rcitos ahora Error, Uds. debe estar conectado Europa Canjear El canje fue aprovado. �Ahora puedes poner %d ej�rcitos m�s! Salir Salir de TEG Ej�rcitos extra FALSO Error fatal: No puedes ejecutar este programa como superusuario
 Francia Modo GGZ activado.
 Opciones del juego Juego terminado Juego terminado, un jugador es el ganador!

El objetivo alcanzado fue:
%s Juego termiando. El jugador %s(%s) es el ganador

El objetivo alcanzado fue:
%s Juego terminado, �Uds. es el ganador!

El objetivo alcanzado fue:
%s Juego con un solo jugador. El jugador %s(%d) es el ganador
 Juego sin jugadores. Inicializando otro juego.
 Alemania Sacar una tarjeta de pa�s Sacar tarjeta Buena suerte %s Hasta luego %s Hasta luego.
 Gran Breta�a Groenlandia �Uy, %s! Hola %s �C�mo estas %s? Humano Me encanta este juego Estoy un poco cansado Soy un cobarde Tengo hambre Soy el jugador n�mero: %d Islandia Adentro Incrementar el zoom por 5% Ir�n Te hace m�s feliz Muestra informaci�n de los paises Italia Es tu turno Es tu turno de atacar �Es tu turno de atacar! Jap�n Lanzar un robot Malasia Opciones de mensajes M�jico Mover ej�rcitos de '%s' a '%s'
 Sr. Presidente Mi color es: %s Nombre Nombre: Nueva York %s, No lo puedo creer Norte Am�rica Todav�a no esta implementado
 Ahora puedes agregar %d extra ej�rcitos a tus paises N�mero Observar el juego, no jugar Ocean�a Oreg�n Afuera Sin memoria
 Falta par�metro para "%s".
 Pasar el turno a otro jugador Per� Pon %d ej�rcitos en %s
 Pon %d ej�rcitos en tus paises
 Pon ej�rcitos Jugar a conquistar el mundo Jugar con objetivos secretos El jugador %d abandon� del juego El jugador %d sali� del juego El jugador %s fue hechado del juego El jugador %s fue hecacho del juego
 Jugador %s no fue encontrado
 El jugador %s(%d) abandon� el juego
 El jugador %s(%d) tiene el color %s
 Jugador %s(%d) esta conectado desde %s
 El jugador %s(%d) es el ganador. Juego terminado
 El jugador %s(%d) perdi� el juego
 Jugador %s(%d) sali� del juego
 El jugador %s(%s) abandon� el juego El jugador %s(%s) canje� 3 tarjetas por %d ej�rcitos El jugador %s(%s) sali� del juego �El jugador %s(%s) tiene el turno de atacar! El jugador %s(%s) esta poniendo %d ej�rcitos El jugador %s(%s) es poniendo %d ej�rcitos por 1ra vez El jugador %s(%s) esta poniendo %d ej�rcitos por 2da vez El jugador %s(%s) es el GANADOR El jugador %s(%s) perdi� el juego El jugador %s(%s) perdi� el juego
 Un jugador perdi� el juego El jugador [%d] '%s' esta conectado con el color %s Estado de jugadores Polonia Muestra Pon 2 ejercitos Rea_grupar los ej�rcitos � Abandonar ? Reagrupar ej�rcitos Reagrupar los ej�rcitos, moviendolos a paises lim�trofes Reagrupando Reagrupando ejercitos Pidiendo objetivo... Resetear el ataque Devolviendo un nro. no random Inteligencia del robot: %d%% Nombre del robot: %s Robot: Error anormal en ai_fichas() Robot: Error anormal en ai_fichasc() Robot: Error en gui_fichas() Robot: Error al tratar de conectarse con el servidor Robot: Error inesperado en canej_out() Rusia Objetivo secreto Seleccione Seleccione tema: Seleccioname Seleccione 2 paises:
1ro: Haga click en el pa�s origen
2do: Haga click en el pa�s destino Seleccione tipo de juego Seleccione su color Seleccione su color Enviar ej�rcitos Enviar ej�rcitos recien puestos Enviar ej�rcitos Opciones del servidor Nombre del servidor: Puerto del servidor: Setear el n�mero inicial de ej�rcitos a poner Setear la semilla para random Seteando variable %s  a: %s -> %s
 Mostrar mensajes de errores Mostrar mensajes importantes Mostrar mensajes informativos Mostrar mensajes de jugadores Mostrar mensajes de jugadores en colores Mostrar ej�rcitos que otros hayan puesto Muestra las opciones para set Pa�s origen es el mismo que el destino. Reseteando el reagrupe... Pa�s origen: '%s'. Ahora seleccione el pa�s destino Sud Africa Sud Am�rica Espa�a Server en modo 'standalone'
 Comenzar Empezar el ataque otra vez, selecionando de nuevo los paises Lanzar servidor localmente Comenzar el juego Comenzando juego n�mero: %d con semilla %u
 Estado Abandonar Suecia Pagina de TEG Preferencias del TEG Versi�n del servidor TEG  VERDADERO Tartaria Tenes Empanadas Graciela - cliente Gnome v%s - por Ricardo Quesada Tenes Empanadas Graciala - Robot v%s - por Ricardo Quesada
 Gracias El ataque fue reseteado. Por favor seleccione un pais para seguir con suataque El juego ya ha comenzado. Intenta conectarte como un observador El juego ya ha comenzado. Intenta conectarte como un observador El servidor esta lleno. Intenta conectarte como un observador El servidor report� un error en '%s' Theme:Author esta vacio
 Theme:Email esta vacio
 Theme:Screenshot esta vacio
 Theme:version esta vacio
 Temas Token '%s' no encontrado Maniana, hoy no. %s no lo tomes a mal Total Intente con un usuario distinto
 Turqu�a Escriba '%s' para m�s ayuda
 Escriba '%s' para m�s ayuda
 No se pudo cargar el tema `%s'
 Opcion desconocida. Intente con`%s %s' para ayuda
 Opci�n no reconocida: "%s"
 Modo de uso: %s [opciones...]
Las opciones v�lidas son:
 Usando semilla: %u
 Usando tema `%s - v%s' por %s
 Ver tarjetas Ver _dados Ver tu _objetivo Ver _jugadores Ver todas las tarjetas ganadas Ver jugadores Ver el estado de los jugadores Ver el resultado de los dados Ver el estado de los jugadores Ver tu objetivo secreto Bienvenido %s �Bienvenido al TEG! Que? Quien empez� �Qui�n quiere una Empanada? Tipo incorrecto (%s). Nada era esperado
 Tipo incorrecto (%s). continent, board y dices era esperado
 Tipo incorrecto (%s). country era esperado
 Tipo incorrecto. nodo raiz != teg_theme
 Si, Por que no ? %s, pero estas seguro ? Uds. es el ganador �Uds. a conquistado '%s' es tu ataque de '%s'!
 No tiene tarjetas todavia Tienes que recomenzar el TEG para usar el tema Perdiste Uds. perdi� el juego :( Necesitas estar conectado Has recivido la tarjeta: '%s' Has recivido la tarjeta: '%s'. Puedes poner 2 ej�rcitos ah� Tu objetivo Tu objetivo es: %s Yuk�n Zoom adentro Zoom afuera [Nota: El servidor ha movido 1 ej�rcito a '%s'por Uds.
Ahora elija cuantos m�s ej�rcitos desea mover] [Nota: Puedes reagrupar cuantas veces quieras,
siempre y cuando no muevas un ej�rcito que haya sido
movido en este turno. [Objetivo com�n a todos.] [Tu objetivo secreto.] Acciones _Conectarse _Desconectarse _Finalizar turno _Lanzar robot _Re-atacar Act_ualizar mapa atacando negro azul versi�n del cliente conectado desconectado habilitado finalizando turno error %d (compilado sin sterror) canjeando tarjetas salir de juego juego terminado sacando tarjeta verde uso interno. No usar uso interno. No usar. hechar un jugador del juego haciendo pacto moviendo ej�rcitos de acuerdo n�mero, pa�ses, ej�rci., tarjetas, canjes, nom., humano, color, estado, direc.
 observador otro rosa poniendo ej�rcitos poniendo ej�rcitos 2 poniendo ej�rcitos 3 jugadores:%d, conecciones:%d, juego nro:%d,objetivos:%s, reglas:%s
 post ej�rcitos post ej�rcitos 2 post ej�rcitos 3 versi�n del protocolo rojo reagrupando para pedir un objetivo salvar el juego envia un mensaje a todos los jugadores versi�n del servidor opciones de seteo muestra ayuda muestra estado de los jugadores muestra el estado de los jugadores empezado empezando turno tegserver: Error anormal en select()
 para pedir ayuda para atacar un pa�s para comentar algo para canjear tarjetas por ej�rcitos para salir del juego para finalizar el turno para sacar tarjeta para poner 2 ej�rcitos en el pa�s conquistado si tienes la tarjeta para poner ej�rcitos luego de que haya finalizado el turno para poner los 3 paises iniciales para poner los 5 paises iniciales para registrar un nuevo jugador para recordarme que hacer para reagrupar los ej�rcitos para seleccionar un color para enviar un mensaje a los jugadores para enviar ej�rcitos a un pa�s conquistado para setear un callback asincrono para setear opciones para comenzar a jugar para comenzar a jugar para abandonar ver opciones amarillo 