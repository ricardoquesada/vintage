��    �     <  �  \      x(  :   y(  >   �(     �(  /   )  -   A)  #   o)  >   �)  .   �)  "   *  &   $*  2   K*  *   ~*      �*     �*     �*     �*     +     .+     I+  %   _+     �+  "   �+     �+     �+     �+     
,     ,     /,     I,     `,     ,     �,  	   �,     �,  %   �,  	   �,     �,     -     #-  
   9-     D-  +   H-     t-  +   �-     �-  !   �-     �-     �-      .     .     -.     5.     <.  .   C.     r.     y.  	   ~.     �.     �.     �.  :   �.  	   �.  	   �.     �.     �.      /     /     /  
   /     */     1/     7/     =/     C/  	   K/     U/  B   m/     �/  
   �/     �/     �/     �/     �/     �/     0  o   0     �0  F   �0  -   �0  ;   1  E   I1  A   �1  A   �1  E   2  >   Y2     �2  /   �2     �2     �2     	3  (   !3     J3     e3  	   �3     �3     �3     �3     �3     �3     �3     4     4     <4  D   W4  $   �4  0   �4     �4     	5     &5      C5     d5     j5     z5     �5     �5  -   �5     �5  +   6  7   .6  4   f6  #   �6  &   �6  #   �6  !   
7     ,7  '   J7     r7  (   �7  )   �7     �7  /   �7  -   -8  &   [8  '   �8  '   �8  4   �8     9  >   &9  =   e9      �9  (   �9     �9  '   :     4:     Q:     n:     v:     }:  5   �:     �:     �:     �:     �:  5   �:     ;     ;     2;  	   ?;  B   I;  H   �;  @   �;  2   <  1   I<     {<     �<     �<     �<     �<     �<     �<     �<  
   �<  	   �<     �<  	   �<  	   �<      =     =     =     =     0=     @=  
   M=     X=     m=     u=     x=     �=     �=     �=     �=  !   �=     �=     �=     �=     >     %>     +>     0>  	   7>     A>     I>     R>     a>  
   j>     u>     ~>     �>     �>     �>  
   �>      �>     �>     �>     �>     �>     �>     ?     ?     -?  2   B?     u?     |?     �?     �?     �?     �?     �?     �?     �?     �?     �?     @     @     @     "@     +@  "   B@     e@     r@     �@     �@     �@  "   �@  #   �@     "A  !   ;A     ]A  #   yA  '   �A     �A     �A      �A  -    B     NB  %   jB  "   �B  /   �B  /   �B     C     /C     KC     hC  *   }C     �C     �C     �C     �C     �C     �C     �C     	D  7   D  
   UD     `D     rD     �D      �D     �D     �D     �D  .   �D  &   E     ?E     FE     ME     \E     cE     qE  \   �E     �E     �E     F     F     +F     KF     \F     kF     xF  )   �F     �F  !   �F     �F     �F     G     G     7G  !   MG  -   oG     �G     �G  G   �G  8   H     <H     IH     WH     ]H     iH  G   oH     �H     �H  '   �H     I     
I  	   I     I     #I     1I     AI     UI     [I     cI  	   jI     tI  @   �I  	   �I     �I  O   �I  M   /J  5   }J  C   �J  "   �J     K     !K     )K  "   /K     RK     YK     mK     �K     �K     �K     �K  *   �K     L     L     4L     @L     LL     ZL     hL     �L     �L     �L     �L     �L     M     M     $M     *M     6M  &   NM  9   uM  &   �M     �M  .   �M     N  -   4N     bN     kN     �N     �N  7   �N     �N     �N     
O     O     O     O  	   'O  g   1O  �   �O     P     8P     WP     `P     iP  	   uP     P     �P     �P  	   �P     �P     �P     �P  	   �P     �P     �P     �P  $   �P     Q     'Q  	   6Q     @Q     NQ     TQ     YQ     sQ     �Q     �Q     �Q     �Q     �Q     �Q  L   �Q     R     (R     .R     3R     BR     SR  A   dR  
   �R     �R     �R     �R     �R  
   �R     �R     �R  "   S     .S     =S  
   JS     US     mS     �S     �S  &   �S     �S     �S     �S  !   T     %T     6T     JT  7   dT  .   �T     �T     �T     	U     !U     9U     PU     bU  %   tU     �U     �U     �U     �U     �U     �U     �U    V  <   W  G   YW      �W  ?   �W  /   X  )   2X  G   \X  (   �X  '   �X  >   �X  0   4Y  ,   eY     �Y     �Y     �Y      �Y     �Y     Z     #Z  -   <Z     jZ     �Z     �Z     �Z     �Z     �Z     [     '[     <[  +   S[     [     �[     �[  $   �[  +   �[     \     \     &\     E\  
   U\     `\  0   d\     �\  2   �\  	   �\  /   �\     ]     (]     7]     S]     o]     r]     y]  0   �]     �]     �]     �]     �]     �]     �]  6   �]  
   %^     0^     <^     E^     L^  	   d^     n^     ^     �^     �^     �^     �^     �^     �^     �^  A   �^     _     _  	    _     *_     0_     <_  	   W_     a_  k   u_     �_  >   �_  (   2`  5   [`  ?   �`  4   �`  4   a  8   ;a  0   ta     �a  2   �a     �a     b     b  (   9b     bb  "   b     �b     �b     �b     �b     �b     �b     c     (c  "   >c     ac  ?   �c  &   �c  7   �c     d     7d     Vd     qd     �d     �d     �d      �d      �d  9   �d  $   (e  3   Me  ?   �e  0   �e  *   �e  %   f  9   Cf  &   }f  %   �f  0   �f  %   �f  ,   !g  <   Ng  "   �g  0   �g  -   �g  +   h  ,   9h  ,   fh  D   �h  *   �h  N   i  H   Ri  !   �i  8   �i  /   �i  1   &j  8   Xj     �j     �j     �j     �j  @   �j     k  
   k     !k     4k  M   ;k  
   �k     �k     �k     �k  F   �k  L   l  @   _l  9   �l  ,   �l     m     m     m     m     $m     :m  
   Pm     [m     im  
   qm     |m     �m  
   �m     �m     �m     �m     �m     �m     �m     �m     n     +n     2n     7n     Un     \n     an     hn  *   �n     �n     �n  "   �n  "   �n     o     o     o     (o     4o     =o     Fo     [o  
   go  	   ro     |o     �o     �o     �o     �o  !   �o     �o     �o     �o     �o     �o  #   p     'p     3p  E   Lp     �p  "   �p     �p     �p     �p     �p     �p     �p     q     q  %   5q     [q     dq     mq     rq     ~q  %   �q     �q     �q     �q  "   r  "   0r  '   Sr  (   {r      �r  '   �r      �r  (   s  3   7s     ks  '   �s  &   �s  5   �s  &   t  $   4t     Yt  -   xt  .   �t     �t  1   �t     'u     Du  /   `u     �u     �u     �u     �u     �u     �u     �u     �u  @   v     Rv     _v     sv     �v  %   �v     �v     �v     �v  8   �v  +   1w     ]w     fw     mw     {w     �w     �w  K   �w     �w     �w     x  	   -x  !   7x  	   Yx     cx     xx     �x  0   �x  0   �x     �x     y     y     0y     Iy     gy  $   �y  1   �y     �y     �y  5   �y  -   5z  	   cz  
   mz     xz     �z     �z  4   �z     �z     �z  ,   �z     {     {     '{     0{     9{     F{     X{  
   l{     w{     �{     �{     �{  A   �{  
   �{     |  Y   |  o   b|  B   �|  _   }  '   u}     �}  
   �}     �}      �}     �}     �}     �}  #   ~     2~     O~     h~  3   p~     �~      �~     �~     �~     �~     �~  ,        <     M  $   b     �  *   �     �     �     �     �     �  -   +�  ?   Y�  +   ��     ŀ  6   ؀     �  9   )�     c�     t�     ��  !   ��  C   ́     �     �     4�     :�     @�     L�  	   Y�  u   c�  k   ق      E�     f�  	   ��  	   ��     ��     ��     ��     ��     փ  	   �     �     ��     �  	   �     �     '�     -�  #   9�     ]�     l�     ~�     ��     ��     ��     ��  &   ń  $   �     �     #�     ,�     :�     ?�  N   D�  
   ��     ��     ��     ��     ��     ̅  E   ޅ     $�     7�     L�     a�     s�     w�     ��     ��  %   ��     ׆     �     ��     �     .�  	   K�     U�  +   c�     ��     ��     ��  (   ܇     �     �     8�  A   X�  #   ��  )   ��  )   �     �  '   /�      W�     x�     ��  +   ��  !   ؉     ��     �     "�     :�     H�     ]�         �   1   �      �  �   D  �  �   �     �   �   k       2   �  }  �         �   �      y  f  �   A  �   l   z   �   �  d       �  �   m           	                2  �  �   w     K   �   �   8  �      a  g                    �       �   O          !  �   n   /    �   @          �  �         �       E  �       -           q                    j      n  ,  F  |  ]  `  �  �  i   �     4   W   �   �  B  �  �   �  �  �   �  �   �              \  �   �  p           �   �   �  �  �          �       9  �      *  o   �       j   �   �  {  v  Y  .  �  �       }   a   �   �   J        �   �  ^      �       �   �  P    e  �  �  �   �   �             �  �  �      &          �   �      �   r  �   �  �      A   C   �        Z        '       7   �   �   �   Q   �   Q             �      �   �              U       3   p  �      L  )           7     t      �  y   ;   �   �   �  �      J   �   T   x     �   �   @  /   �  �  %      N   �  
   �   H   f   {   ^   r       #   �   i  �  �   _   b   `   ]       G          �            5       �         �      �  5  )                      �   �      T  �  >  u   &   P   ,       �  �          �   M       |   �   $   �   �      (         M  0  �         �  ~                   �  Y   _          �   V  <      b  c  =  �   �  q  �           L   �  �  �   [    �  �  ;  �  >           �   �    \   �   4  �   �      �  �  �       �  t  c   1      �  �          Z   �   u  +      m  �   �  S   s  �  �   �   �       ~          �  �   �       �              h      6  �          �   �   �   �   o  �   %        "  �  �  �  �   !   U          �   �  �       �       V      E   -      8   F      .   d      C      '  �    �   �      �   6               	   "   w              =           <      �                    9       (     �  �   �  �           �   �   �             �  v   :  $          B       �  #  �  :       �   e               �   �       �  
  �    h   �      �  �  X  W  �       �           g       �      �  3  K              �  �       N  G       �      �       z  �       �   H  S     x       O   D   *   �  �       [       �   I   R  �   �  �       ?      �  �   �   ?       �   �   �           �  R           �       �  �   �  �      �   0       k      �      �  X   s   �          �   �       �  l     +  �   �  �      I  �       �        
If you want to EXCHANGE your cards for armies,
DO IT NOW!   -c, --console BOOLEAN	Enable the console or not (default 1)
   -g, --ggz		Enable GGZ mode
   -g, --ggz		Enable the GGZ mode (default OFF)
   -h, --help		Print a summary of the options
   -n, --name NAME	Use NAME as name
   -p, --port PORT	Bind the server to port PORT (default 2000)
   -p, --port PORT	Connect to server port PORT
   -q, --quiet		dont show messages
   -s, --seed SEED	New seed for random
   -s, --server HOST	Connect to the server at HOST
   -v, --version		Print the version number
 %s Better luck for the next time %s armies: %d %s variable is set to: %s
 %s variable is set to: %u
 %s variable is set to: (%d,%d)
 %s(%s) is attacking %s(%s) %s, Are you a robot ? %s, Are you sure what you are saying? %s, Are you talking to me? %s, Do you have Lewinsky's phone ? %s, Do you have a sister ? %s, Do you think so? %s, I cant believe it! %s, I dont agree %s, I dont think so %s, I dont understand you %s, I'll be the winner %s, Were you here last night ? %s, What are you saying %s, What can I say ? %s, What? %s, You play very well %s, You really dont know how to play! %s, funny %s, sure %s, try to improve your game %s, you make me laugh 1. Regroup 1:1 1st part:
Place %d armies in your countries 2. Get card 2nd part:
Place %d armies in your countries 3. End turn A clone of T.E.G. (a Risk clone). A country was conquered A new country A player lost the game A player won the game Address Africa Alaska And place another %d armies in your countries
 Arabia Aral Argentina Armies Armies to move Asia Attack your enemy, again using same source and destination Australia Batistuta Batman Borneo Bound to port: %d
 Brazil Bush California Canada Cards Chile China Chupete Ciccolina Cleared output window.
 Client with incompatible protocol version (server:%d , client:%d)
 Clinton Coca Sarli Colombia Color Comanche Command '%s' not recongnized
 Connect Connect to server Conquer 2 countries of Oceania,
2 of Africa, 2 of South America,
3 of Europe, 4 of North America,
and 3 of Asia Conquer 30 countries Conquer Africa,
5 countries of North America and
4 countries of Europe Conquer Asia and
2 countries of South America Conquer Europe,
4 countries of Asia and,
2 of South America Conquer North America,
2 countries of Oceania and
4 countries of Asia Conquer Oceania,
conquer Africa and,
5 countries of North America Conquer Oceania,
conquer North America and,
2 countries of Europe Conquer South America,
7 countries of Europe and
3 frontier countries Conquer South America,
conquer Africa and,
4 countries of Asia Conquer the world Conquer the world, or play with secret missions Continent has no name
 Continent has no pos_x
 Continent has no pos_y
 Copyright (C) 2000, 2001 Ricardo Quesada Could not find the %s file Couldn't open '/dev/random'
 Countries Country Cards Country has no army_x
 Country has no army_y
 Country has no file
 Country has no name
 Country has no pos_x
 Country has no pos_y
 Coward %s(%s) has surrendered Decrease zoom factor by 5% Destination country: '%s'. Now select the quantity of armies to move Dices: %s: %d %d %d vs. %s: %d %d %d Different protocols version. Server:%d Client:%d Disconnect from server Display the map at 1:1 scale Does anyone speak Speranto ? Dont play, just observe the game Egypt Empty document
 Enables GGZ mode Error creating canvas
 Error creating image
 Error in start. Are there at least 2 players? Error setting %s variable.
 Error while trying to connect to GGZ client Error while trying to connect to server '%s' at port %d Error, '%s' doesnt have any avalaible armies to move Error, '%s' isnt frontier with '%s' Error, '%s' isnt one of your countries Error, It's not the time to regroup Error, cant initilize a new turn
 Error, couldn't find file: %s Error, couldn't find file: objetivo.png Error, couldn't find file:%s Error, it's not the moment to get a card Error, it's not the time to put 2 armies. Error, it's not your turn. Error, its not the moment to exchange the cards Error, its not the moment to send your armies Error, its not the time to send armies Error, put the correct number of armies Error, the 2 armies where placed before Error, the cards must be all equal, or all different Error, the game is not started Error, the game is started so you cant change the type of game Error, unexpected error in reagrupe_click(). Report this bug! Error, you are already connected Error, you can't regroup your armies now Error, you cant add armies now Error, you cant put more than %d armies Error, you cant regroup now. Error, you must be connected Etiopia Europe Exchange Exchanged approved. Now you can place %d more armies! Exit Exit TEG Extra armies Fantoche Fatal error: You can't run this program as superuser
 France GGZ mode activated.
 Game Options Game Over Game over, A player won the game

The accomplished mission was:
%s Game over, Player %s(%s) is the winner

The accomplished mission was:
%s Game over, you are the winner!

The accomplished mission was:
%s Game with one player. Player %s(%d) is the winner
 Game without players. Initializing another game.
 Gatubela Gaturro Geniol Germany Get a country card Get card Gobi Good luck %s Goodbye %s Goodbye.
 Great Britain Greenland Guaymayen Havanna How are you %s ? Human I love this game I'm a bit tired I'm a coward I'm hungry I'm player number:%d Iceland In Increase zoom factor by 5% India Iran Israel It makes you more happy It shows info about the countries Italy Its your turn Its your turn to attack Its your turn to attack!! Japan Java Jirafa Katchatka La cuca Labrador Launch a robot Lewinsky Madagascar Malaysia Maradona Message Options Mexico Mongolia Mono Mario Moving armies from '%s' to '%s'
 Mr.President My color is: %s Name Name: New York No %s, I dont believe that North America Not yet implemented
 Now you can add %d extra armies in your countries! Number Observe the game, dont play it Oceania Oregon Out Out of memory
 Pamela Pampa Pampita Parameter is missing for "%s".
 Pass the turn to another player Pele Perla Peru Pitufina Place %d armies in %s
 Place %d armies in your countries
 Place armies Play to conquer the world Play with secret missions Player %d abandoned the game Player %d exit the game Player %s was kicked from the game Player %s was kicked from the game
 Player %s was not found
 Player %s(%d) abandoned the game
 Player %s(%d) has color %s
 Player %s(%d) is connected from %s
 Player %s(%d) is the winner! Game Over
 Player %s(%d) lost the game
 Player %s(%d) quit the game
 Player %s(%s) abandoned the game Player %s(%s) exchanged 3 cards for %d armies Player %s(%s) exit the game Player %s(%s) has the turn to attack! Player %s(%s) is placing %d armies Player %s(%s) is placing %d armies for 1st time Player %s(%s) is placing %d armies for 2nd time Player %s(%s) is the WINNER Player %s(%s) lost the game Player %s(%s) lost the game
 Player lost the game Player[%d] '%s' is connected with color %s Players status window Poland Preview Put 2 armies Raton Perez Re_group armies Really surrender ? Regroup your armies Regroup your armies, moving from one country to another Regrouping Regrouping armies Requesting a mission... Reset the attack Returning a not so random number Risk Robot intelligence: %d%% Robot name: %s Robot: Error while trying to connect to server Robot: Unexpected error in canje_out() Russia Sahara Secret mission Select Select theme: Select this card Select two countries:
1st: click on the source country
2nd: click on the destination country Select type of game Select your color Select your desired color Send armies Send the armies recently placed Send your armies Server Options Server name: Server port: Set the initial number of armies to place Set the seed for random Setting %s variable to: %s -> %s
 Shakira Show Error Messages Show Important Messages Show Informative Messages Show Players Messages Show Players Messages with colors Show the armies that others player had placed Shows the set options Siberia Source country is the same as the destination. Resetting the regroup... Source country: '%s'. Now select the destination country South Africa South America Spain Spice Girls Start Start again selecting the source and destination country for the attack Start server locally Start the game Starting game number: %d with seed: %u
 Status Sumatra Surrender Sweden TEG Home Page TEG Preferences TEG server version  Talia Tartary Taymir Tehuelche Tenes Empanadas Graciela Tenes Empanadas Graciela - Gnome client v%s - by Ricardo Quesada Terranova Thanks The attack was reset. Please, select the source country to continue your attack The game has already started :( Try connecting as an observer. Go to settings The game has already started. Connect as an observer. The server is full :( Try connecting as an observer. Go to settings The server report an error in '%s' Themes Topacio Total Try to run it as a different user
 Turkey Type '%s' for help
 Type '%s' for more help
 Unable to create map! Unable to load theme `%s'
 Unrecognized option: "%s"
 Uruguay Usage: %s [option ...]
Valid options are:
 Using seed: %u

 Using theme `%s - v%s' by %s
 View _cards View _dices View _mission View _players View all the card that you won View players View players status View the result of the dices View the status of the players View your mission of this game Welcome back %s! Welcome to TEG! What? Who started Who wants an Empanada ? Wrong type (%s). Nothing was expected
 Wrong type (%s). continent, board and dices was expected
 Wrong type (%s). country was expected
 You are the winner You conquered '%s' in your attack from '%s'!!
 You dont have any cards yet You have to restart TEG to use the new theme. You lost You lost the game :( You need to be connected You received card: '%s' You received card: '%s' and 2 armies where placed there Your mission Your mission is: %s Yukon Zaire Zoom In Zoom Out Zoom _1:1 [Note: The server had move one army to '%s' for you.
Now choose, how many more armies you want to move] [Note: You can regroup as many times as you want,
as long as you dont regroup an armie that were
regrouped before in this turn.] [This is the common mission.] [This is your secret mission.] _Actions _Connect _Disconnect _End turn _Launch robot _Reattck _Update Map attacking black blue client version connected disconnected enabled ending turn error %d (compiled without strerror) exchanging cards exits the game game over gettting card green idle internal use. Dont use it internal use. Dont use it. kick player from the game making a pact mmm... moving armies no nod number, countries, armies, cards, exch, name, human, color, status, address
 observer other pink placing armies placing armies 2 placing armies 3 players:%d, connections:%d, game number:%d, mission:%s, rules:%s
 postarmies postarmies 2 postarmies 3 protocol version red regrouping request a mission save the game sends a message to all the players server version sets options shows help shows status of players shows the status of the players started starting turn tegserver: Abnormal error in select()
 to ask for help to attack a country to comment a command to exchange your cards for armies to exit the game to finish your turn to pick up a country-card to place 2 armies in a country. You must have that card to place the armies after a turn have finished to place the initials 3 armies to place the initials 5 armies to register as a player to remind me what to do to reorder your armies to select a color to send a message to send armies to a conquered country to set an async callback to set options to start playing to start the game to surrender view options yellow Project-Id-Version: teg 0.7.1
POT-Creation-Date: 2001-09-02 22:08-0300
PO-Revision-Date: 2001-08-29 20:36+0200
Last-Translator: Thomas R. Koll <tomk32@tomk32.de>
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 
Wenn du deine Armee Karten AUSTAUSCHEN willst,
TU ES JETZT!  -c, --console BOOLEAN Erm�glicht die Konsole, oder nicht (Standard 1)
  -g, --ggz		Aktiviere GGZ Modus
  -g, --ggz             Erm�glicht den GGZ Modus (Standard OFF)
  -h, --help		Zeigt eine �bersicht der Optionen
  -n, --name NAME	Verwende NAME als Namen
  -p, --port PORT       Kn�pft den Server and Port PORT (Standard 2000)
  -p, --port PORT	Verbinde zum Port PORT
  -q, --quiet		keine Meldungen anzeigen
  -, -seed SEED         Neue Ausgangszahl f�r Zufallsgenerator
  -s, --server HOST	Verbinde zum Server auf HOST
  -v, --version		Zeige die Versionsnummer an
 %s Vielleicht n�chstes Mal %s Armeen: %d %s Variable ist: %s
 Variable %s ist gesetzt auf: %u
 Variable %s ist: (%d,%d)
 %s(%s) greift %s(%s) an %s, bist du ein Roboter? %s, kann's sein dass du Schei� daher laberst? %s, redest du mit mir? %s, hast du was an den Ohren? %s, hast du 'ne Schwester? %s, glaubst doch selber nicht? %s, ich kann es nicht glauben! %s, da liegst fett daneben %s, glaub ich kaum? %s, kapier ich nicht %s, Ich werde gewinnen %s, warst du nicht schon letzte Nacht hier? %s, WIE BITTE? %s, was soll ich dazu sagen? %s, was? %s, du spielst gar nicht so schlecht %s, du wei�t wirklich nicht wie man spielt! %s, sehr lustig.. %s, schon klar %s, du solltest besser spielen %s, sehr lustig 1. Bewegen 1:1 Erster Teil:
Plaziere %d Armeen in deinen L�nder 2. L�nderkarte Zweiter Teil:
Plaziere %d Armeen in deinen L�ndern 3. Fertig Eine Neufassung von T.E.G. (eine Risiko Kopie). Ein Land wurde erobert Ein neues Land Ein Mitspieler hat verloren Ein Mitspieler hat gewonnen IP Afrika Alaska Und plaziere weiter %d Armeen in deinen L�ndern
 Arabien Aralsee Argentinien Armeen Zu bewegende Armeen Asien Feind nochmal mit gleicher Ursprung und Ziel angreifen Australien Steffi Graf Godzilla Borneo Verbunden mit Port: %d
 Brasilien Gerhard Schr�der Kalifornien Kanada Karten Chile China Pinky Franz Ausgabefenster gel�scht
 Client mit imkompatibler Protokollversion (Server:%d, Client:%d)
 Homer Simpson Heinz Kolumbien Farbe Darth Vader Befehl '%s' ist unbekannt
 Verbinden Verbinde zum Server Erobere 2 L�nder in Ozeanien,
2 in Afrika, 2 ind S�damerika,
3 in Europa, 4 in Nord America,
und 3 in Asien Erobere 30 L�nder Erobere Afrika,
5 L�nder in Nordamerika und
4 L�nder in Europa Erobere Asien und
3 L�nder in S�damerika Erobere Europa,
4 L�nder in Asien und
2 in S�damerika Erobere Nordamerika,
2 L�nder in Ozeanien und
4 L�nder in Asien Erobere Ozeanien,
Afrika und
5 L�nder in Nordamerika Erobere Ozeanien,
Nordamerika und
2 L�nder in Europa Erobere S�damerica,
7 Lander in Europa und
3 Grenzl�nder Erobere S�damerika,
Afrika und
4 L�nder in Asien Erobere die Welt 		Erobere die Welt oder spiele mit Geheimmissionen Kontinent hat keinen Namen
 Kontinent hat kein pos_x
 Kontinent hat kein pos_y
 Copyright (C) 2000, 2001 Ricardo Quesada Konnte Datei %s nicht finden Konnte '/dev/random' nicht �ffnen
 L�nder L�nder Karte Land hat kein army_x
 Land hat kein army_y
 Land hat keine Datei
 Land hat keinen Namen...
 Land hat keine pos_x
 Land hat keine pos_y
 Der Feigling %s(%s) hat aufgegeben Spielansicht um 5% verkleinern Zielland: '%s'. W�hle jetzt die Anzahl der zu Bewegenden Armeen W�rfel: %s: %d %d %d vs. %s: %d %d %d" Unterschiedliche Protokolversionen. Server:%d Client:%d Vom Server unterbrochen Zeige die Karte im Ma�stab 1:1 Spricht irgendwer Deutsch? Beobachtermodus �gypten Leere Datei
 Schalte GGZ Modus ein Fehler beim Erzeugen der Karten
 Fehler beim Erzeugen der Bilder
 Fehler beim Starten. Es m�ssen mindestens 2 Spieler sein! Fehler beim setzen von Variable %s.
 Fehler w�hrend des Verbindungsaufbau zum GGZ Client Fehler w�hrend des Verbindungsaufbau zum Server '%s' am Port %d Fehler, '%s' hat keine freien Armeen zum Bewegen Fehler, '%s' ist kein Nachbarland von '%s' Fehler, '%s' ist keines deiner L�nder Fehler, du kannst deine Truppen jetzt nicht neu anordnen. Fehler, kann keine neue Runde starten
 Fehler, konnte Datei nicht finden: %s Fehler, konnte Datei nicht finden: objectivo.png Fehler, konnte Datei nicht finden: %s Fehler, du kannst jetzt keine Karte erhalten Fehler, es ist nicht der Zeitpunkt um die 2 Armeen zu setzen Fehler, du bist nicht an der Reihe Fehler, es ist nicht die Zeit zum Kartentauschen Fehler, du kannst jetzt keine Armeen schicken Fehler, du kannst jetzt keine Armeen senden Fehler, setze die richtige Anzahl von Armeen Fehler, die 2 Armeen wurden bereits plaziert Fehler, die Karten m�ssen alle entweder gleich oder verschieden sein Fehler, das Spiel hat noch nicht begonnen. Fehler, das Spiel hat schon angefangen und du kannst die Spielart nicht �ndern Fehler, unerwarteter Fehler in reagrupe_click(). Bitte melde diesen Bug! Fehler, du bist bereits verbunden Fehler, du kannst deine Armeen jetzt nicht neu verteilen Fehler, du kannst jetzt keine Armeen hinzuf�gen Fehler, du kannst nicht mehr als %d Armeen setzen Fehler, du kannst deine Armeen jetzt nicht neu verteilen Fehler, du must verbunden sein. �thopien Europa Austauschen Austausch erfolgt. Du kannst jetzt %d zus�tzliche Armeen setzen! Beenden Beende TEG Zus�tzliche Armeen Tomboy Schwerer Fehler: Die Programm kann nicht als Superuser (root) benutzt werden
 Frankreich GGZ Modus aktiviert.
 Spiel Einstellungen Spiel vorbei Spiel zu Ende, ein Spieler hat gewonnen!

Die erf�llte Mission war:
%s Spiel beendet, Spieler %s(%s) ist der Gewinner

Die erf�llte Mission war:
%s Spiel zu Ende, du bist der Sieger!

Die erf�llte Mission war:
%s Spiel mit einem Spieler. Spieler %s(%d) ist der Gewinner
 Spiel ohne Spieler. Starte ein neues Spiel.
 Rosi Brain Wolfi Deutschland Nimm eine L�nderkarte Nimm eine L�nderkarte W�ste Gobi Viel Gl�ck %s Ciao %s Bis bald.
 Gro� Brittanien Gr�nland Der R�uber Mustafa Wie geht's dir %s ? Mensch Ich liebe dieses Spiel Ich bin m�de Ich bin ein Feigling Ich bin hungrig Ich bin der Spieler Nummer: %d Island Rein Spielansicht um 5% vergr��ern Indien Iran Israel Es macht dich gl�cklicher Es zeigt die Informationen �ber die L�nder Italien Du bist an der Reihe Du bist mit Angreifen an der Reihe Du bist mit Angreifen an der Reihe Japan Java Der Polizist Kamtschatka Lie�chen Labrador Starte einen Roboter Theo Waigel Madagaskar Malaysien Caesar Nachrichten Optionen Mexiko Mongolei Superman Bewege Armeen von '%s' nach '%s'
 Bobbele Meine Farbe ist: %s Name Name: New York Nein %s, ich kann es nicht glauben! Nordamerika Noch nicht impementiert
 Du kannst jetzt %d zus�tzliche Armeen in deinen L�ndern positioniern! Nummer Spiel beobachten, nicht mitspielen Ozeanien Oregon Raus Speichermangel :-(
 Claudia Schiffer Erich Honnecker K�ptain Blaub�r Ein Parameter for '%s' fehlt.
 Zug an den n�chsten Spieler �bergeben Hannibal Napoleon Peru DJ Tomcraft Plaziere %d Armeen in %s
 Plaziere %d Armeen in deinen L�ndern
 Plaziere deine Armeen Spiele um die Weltherrschaft Spiele um geheime Missionen Spieler %d hat das Spiel verlassen Spieler %d hat das Spiel verlassen Spieler %s wurde aus dem Spiel geworfen Spieler %s wurde aus dem Spiel geworfen
 Spieler %s wurde nicht gefunden
 Spieler %s(%d) hat das Spiel verlassen
 Spieler %s(%d) hat die Farbe %s
 Spieler %s(%d) ist verbunden von %s aus
 Spieler %s(%d) ist der Gewinner! Das Spiel ist aus
 Spieler %s(%d) hat verloren
 Spieler %s(%d) hat das Spiel verlassen
 Spieler %s(%s) hat das Spiel verlassen Spieler %s(%s) hat 3 Karten gegen %d Armeen getauscht Spieler %s(%s) hat das Spiel verlassen Spieler %s(%s) darf jetzt angreifen! Spieler %s(%s) setzt %d Armeen Spieler %s(%s) setzt %d Armeen zum ersten Mal Spieler %s(%s) setzt %d Armeen zum zweiten Mal Spieler %s(%s) ist der GEWINNER Spieler %s(%s) hat verloren und ist ausgeschieden Spieler %s(%s) hat verloren
 Ein Mitspieler hat verloren Spieler[%d] '%s' mit der Farbe %s ist verbunden Spielerstatus Fenster Polen Vorschau Setzte 2 Armeen Sepperl Armeen neu _gruppieren Wirklich aufgeben? Ordne deine Armeen neu an Ordne deine Armeen neu an, bewege sie von einem Land ins n�chste Verteile neu Ordne Armeen neu an Fordere eine Mission an... Angriff abbrechen Verwende eine nicht so zuf�llige Zahl Risiko Roboter Intelligence: %d%% Robotername: %s Roboter: Fehler w�hrend des Verbindungsaufbau zum Server Roboter: Unerwarteter Fehler in canje_out() Russland Sahara Geheimmission W�hle W�hle Karte W�hle diese Karte W�hle zwei L�nder:
1: klick auf das Ursprungsland
2: klick auf das Zielland W�hle die Spielart W�hle deine Farbe W�hle deine gew�nschte Farbe Angreifen Sende die gerade gesetzten Armeen Angreifen Server Einstellungen Server Name: Server Port: 		Setze die Zahl der Armeen zu Beginn des Spiels 		Setzt die Anfangszahl f�r den Zufallsgenerator Setzt %s Variable: %s -> %s
 HerrBert Zeige Fehlermeldungen Zeige Wichtige Meldungen Zeige Informative Nachrichten Zeige Spieler Nachrichten Zeige Spieler Nachrichten mit Farben Zeige die Armeen die andere Spieler gesetzt haben 		Zeigt die gesetzen Optionen Sibirien Ursprungsland und Zielland sind gleich. Starte neu... Ursprungsland: '%s'. Jetzt w�hle das Zielland S�dafrika S�damerika Spanien Schiller Start W�hle den Ursprung und das Ziel der Attacke nochmals Starte lokalen Server Starte das Spiel Starte Spiel Nummer: %d mit Zufallszahl: %u
 Status Sumatra Aufgeben Schweden TEG Homepage TEG Einstellungen TEG Server Version  von Goethe Tartarus Westsibirien Luke Skywalker Tenes Empanadas Graciela Tenes Empanadas Graciela - Gnome Client v%s - von Ricardo Quesada Neuengland Danke Der Angriff wurde abgebrochen. Bitte w�hle ein Ursprungsland um deine Attack
fortzusetzen Das Spiel hat schon l�ngst begonnen :( Versuch dich als Beobachter einzuloggen. Schau dazu in die Einstellungen Das Spiel hat schon l�ngst begonnen. Log dich also Beobachter ein. Der Server ist voll :( Versuch dich als Beobachter einzuloggen. Schau dazu in die Einstellungen Der Server erhielt einen Fehler in '%s' Themes Michi Beck Total Starte es als normaler Benutzer
 T�rkei Tippe '%s' f�r Hilfe
 Tippe '%s' f�r mehr Hilfe
 Erstellen der Karte fehlgeschlagen! Kann Karte nicht laden `%s'
 Unbekannte Option: '%s'
 Uruguay Verwendung: %s [option ...]
G�ltige Optionen sind:
 Verwende Seed: %u
 Verwende Karte `%s - v%s' by %s
 Zeige _Karten Zeige _W�rfel _Mission Spieler_�bersicht Schau die die Karten die du gewonnen hast an Spieler�bersicht Zeige Spieler Status Schau die das Ergebnis der W�rfel an Zeige den Status der Spieler an Schau dir deine Aufgabe in diesem Spiel an Willkommen zur�ck %s! Willkommen bei TEG! H��h? Wer hat gestartet Wer will ein Empanada? Falscher Typ (%s). Wir haben nichts erwartet
 Falscher Typ '%s'. Kontinent, Brett und W�rfel wurden erwartet
 Falscher Typ (%s). Ein Land wurde erwartet
 Du bist der Sieger Du hast in deinem Angriff '%s' von '%s' aus erobert!!
 Du hast noch keine Karten Du musst TEG neu starten um das neue Aussehen zu benutzen Du hast verloren Du hast das Spiel verloren :( Du musst verbunden sein Du hast eine Karte erhalten: '%s' Du hast eine Karte erhalten: '%s' und 2 Armeen wurden dort plaziert Deine Mission Deine Mission ist: %s Yukon Zaire Ver_gr��ern Ver_kleinern Zoom _1:1 [Achtung: Der Server hat f�r dich eine Armee nach '%s' bewegt.
Nun w�hle wieviel Armeen du zus�tzlich bewegen willst] [ACHTUNG: Du kannst so oft bewegen wie du willst,
so lange jede Armee nur einmal in einem Zug bewegt wird.] [Das ist deine normale Mission.] [Das ist deine Geheimmission.] A_ktionen _Verbinde _Unterbreche Zug be_enden Lade _Roboter Attacke _wiederholen _Aktualisiere Karte greift an schwarz blau Client Version verbunden unterbrochen aktiv beendet Zug Fehler %d (kompliert ohne strerror) tauscht Karten beendet das Spiel Spiel vorbei erh�lt Karte gr�n unt�tig wird intern benutzt, Pfoten weg wird intern verwendet, lass es in Ruh. wirft einen Mitspieler aus dem Spiel schlie�t B�ndinis hmmm.... bewegt Armeen nein jaja Nummern, Karten, L�nder, Armeen, Tausch, Name, Mensch, Farbe, Status, Adresse
 Beobachter andere pink plaziert Armeen plaziert Armeen 2 plaziert Armeen 3 Spieler:%d, Verbindungen:%d, Spielnummer:%d, missionen:%s, Regeln:%s
 Armeen aufgestellt Armeen aufgestellt 2 Armeen aufgestellt 3 Protokoll Version rot ordnen neu an fordere eine Mission an speichert das Spiel sendet eine Nachricht an alle Spieler Server Version setzt einige Optionen zeigt die Hilfeseite zeigt den Status der Spieler zeigt den Status der Spieler gestartet startet Runde tegserver: Unerwarteter Fehler in select()
 um Hilfe zu bekommen um ein Land anzugreifen um ein Kommando zu kommentieren um deine Karten gegen Armeen zu tauschen um das Spiel zu verlassen um deinen Zug zu beenden um eine L�nderkarte zu bekommen um 2 Armeen in einem Land zu plazieren musst du diese Karte haben um Armeen nach Zugende zu plazieren um die anf�nglichen 3 Armeen zu plazieren um die anf�nglichen 5 Armeen zu plazieren um also Spieler einzusteigen um mich dran zu erinnern was zu tun ist um deine Armeen neu zu verteilen um eine Farbe zu w�hlen um eine Nachricht zu senden um Armeen in ein erobertes Land zu schicken um einen async callback zu setzen um Optionen zu setzen startet das Spiel um das Spiel zu starten um aufzugeben Einstellungen zeigen gelb 