/*	$Id: globals.h,v 1.41 2001/11/12 04:29:26 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file globals.h
 */
#ifndef __TEGC_GLOBALS_H
#define __TEGC_GLOBALS_H

#include "client.h"
#include "themes.h"


/*
 * Estas son las funciones que deben exportar los modulos (gtk,win,etc)
 * (para hacer un nuevo port, ver gui-null/null.c)
 *
 * NO CAMBIAR EL ORDEN. DO NOT CHANGE THE ORDER
 *
 */

#if 0
enum {
	GUI_INIT,		/**< inicializacion de variables                                 */
	GUI_MAIN,		/**< main loop */
	GUI_DISCONNECT,		/**< me desconecte callback */
	GUI_CONNECTED,		/**< conectado */
	GUI_HABILITADO,		/**< habilitado */
	GUI_START,		/**< empece a jugar callback */
	GUI_STATUS,		/**< status callback */
	GUI_EXIT,		/**< quit callback */
	GUI_TEXTMSG,		/**< mensaje a la pantalla ppal */
	GUI_FICHAS,		/**< poner las 1ra fichas */
	GUI_FICHAS2,		/**< poner las 2das fichas */
	GUI_TURNO,		/**< es mi turno */
	GUI_ATAQUE,		/**< me estan atacando */
	GUI_PAIS,		/**< pais, due�o y cant. de ejercitos */
	GUI_PAIS_SELECT,	/**< selecciona un pais */
	GUI_TROPAS,		/**< tropas */
	GUI_TARJETA,		/**< tarjeta que gane */
	GUI_FICHASC,		/**< fichas de contienente y paises */
	GUI_DADOS,		/**< mostrar dados */
	GUI_REAGRUPE,		/**< mostrar ventana de reagrupe */
	GUI_SENSI,		/**< usada para mostrar sensitiviness en los botones */
	GUI_CANJE,		/**< avisa que hiciste bien el canje */
	GUI_LOST,		/**< un jugador perdio */
	GUI_WINNER,		/**< un jugador gano */
	GUI_OBJETIVO,		/**< muestra tu objetivo */
	GUI_TEXTPLAYERMSG,	/**< mensaje de un jugador */

	GUI_LAST		/**< este no  existe */
};
#endif

extern TEG_STATUS gui_init(int argc, char **argv);
extern TEG_STATUS gui_main(void);
extern TEG_STATUS gui_exit(char *str);
extern TEG_STATUS gui_textmsg(char *aString );
extern TEG_STATUS gui_disconnect(void);
extern TEG_STATUS gui_habilitado(int numjug);
extern TEG_STATUS gui_connected( char *c);
extern TEG_STATUS gui_reconnected();
extern TEG_STATUS gui_status(void);
extern TEG_STATUS gui_start(void);
extern TEG_STATUS gui_fichas(int cant, int conts);
extern TEG_STATUS gui_turno(void);
extern TEG_STATUS gui_pais(int p);
extern TEG_STATUS gui_pais_select( int pais );
extern TEG_STATUS gui_tropas(int src, int dst, int cant);
extern TEG_STATUS gui_tarjeta(int pais );
extern TEG_STATUS gui_dados();
extern TEG_STATUS gui_reagrupe( int src, int dst, int cant );
extern TEG_STATUS gui_sensi();
extern TEG_STATUS gui_canje();
extern TEG_STATUS gui_ataque( int src, int dst );
extern TEG_STATUS gui_canje();
extern TEG_STATUS gui_lost(int numjug);
extern TEG_STATUS gui_surrender(int numjug);
extern TEG_STATUS gui_winner(int numjug, int objetivo);
extern TEG_STATUS gui_objetivo();
extern TEG_STATUS gui_textplayermsg(char *n,int nj, char *m );
extern TEG_STATUS gui_scores(void);

typedef struct _cjuego {
	int fd;				/**< mi fd */
	JUG_ESTADO estado;		/**< estado del juego */
	int numjug;			/**< numero de jugador (yo) */
	int human;			/**< Am I a human ? */
	int observer;			/**< si soy observer o jugador */
	int jugadores;			/**< cantidad de jugadores */
	char myname[PLAYERNAME_MAX_LEN];
	int mycolor;			/**< color preferido */
	char sername[SERVER_NAMELEN];	/**< server name */
	int serport;			/**< server port */
	LIST_ENTRY tarjetas_list;	/**< lista de tarjetas */
	int tarjetas_cant;		/**< cantidad de tarjetas */
	int objetivo;			/**< objetivo a cumplir */
	int reglas;			/**< reglas del juego */
	int dados_srcpais;		/**< utimo pais que ataco */
	int dados_src[3];		/**< ultimos dados del atacante */
	int dados_dstpais;		/**< ultimo pais que se defendio */
	int dados_dst[3];		/**< ultimos dados del defensor */
	int msg_show;			/**< mascara de los mensajes que hay que mostrar */
	int with_ggz;			/**< si tiene soporte para GGZ */
	char theme[THEME_MAX_NAME];	/**< theme que se esta usando */
} CJUEGO, *PCJUEGO;

/* funciones y variables exportadas */
extern CJUEGO g_juego;

#endif /* __TEGC_GLOBALS_H */
