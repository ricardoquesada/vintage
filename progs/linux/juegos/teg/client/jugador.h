/*	$Id: jugador.h,v 1.13 2001/11/26 05:45:55 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file jugador.h
 */

#ifndef __TEGC_JUGADOR_H
#define __TEGC_JUGADOR_H

typedef struct _jugador {
	LIST_ENTRY next;
	char nombre[PLAYERNAME_MAX_LEN];
	char addr[PLAYERADDR_MAX_LEN];
	int color;
	int score;
	int numjug;
	JUG_ESTADO estado;
	int tot_paises;
	int tot_ejercitos;
	int tot_tarjetas;
	int empezo_turno;		/* dice si empezo el turno */
	int human;
} JUGADOR, *PJUGADOR;


/* funciones exportadas */
extern LIST_ENTRY g_list_jugador;

TEG_STATUS jugador_whois( int numjug, PJUGADOR *j);
TEG_STATUS jugador_update( PJUGADOR j );
PJUGADOR jugador_ins( PJUGADOR j);
TEG_STATUS jugador_del( PJUGADOR j );
TEG_STATUS jugador_flush();
TEG_STATUS jugador_init();

#endif /* __TEGC_JUGADOR_H */
