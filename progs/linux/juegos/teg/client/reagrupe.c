/*	$Id: reagrupe.c,v 1.13 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file reagrue.c
 * Contiene algunas funciones auxiliares para el manejo del estado 'ESTADO_REAGRUPE'
 */

#include "client.h"

static int pais_origen = -1;
static int pais_destino = -1;

/* valores que guardan lo ultimo enviado, y en caso de error se borra el ejer_reagrupe */
static int last_origen = -1;
static int last_destino = -1;
static int last_cant = -1;

TEG_STATUS reagrupe_check( void )
{
	JUG_ESTADO e = ESTADO_GET();

	if( e>=JUG_ESTADO_ATAQUE && e<=JUG_ESTADO_REAGRUPE ) {
		ESTADO_SET(JUG_ESTADO_REAGRUPE);
		return TEG_STATUS_SUCCESS;
	} else
		return TEG_STATUS_ERROR;
}

TEG_STATUS reagrupe_click( PPAIS p )
{
	if( reagrupe_check() != TEG_STATUS_SUCCESS ) {
		textmsg(M_ERR,_("Error, It's not the time to regroup"));
		return TEG_STATUS_UNEXPECTED;
	}

	if( pais_origen == -1 ) {
		if(p->numjug == WHOAMI() ) {
			if( p->ejercitos - p->ejer_reagrupe > 1 ) {
				p->selected &= ~PAIS_SELECT_REGROUP_ENTER;
				p->selected |= PAIS_SELECT_REGROUP;
				gui_pais_select(p->id);
				pais_origen = p->id;
				textmsg(M_INF,_("Source country: '%s'. Now select the destination country"),p->nombre);
			} else {
				textmsg(M_ERR,_("Error, '%s' doesnt have any avalaible armies to move"),p->nombre);
				return TEG_STATUS_UNEXPECTED;
			}
		} else { 
			textmsg(M_ERR,_("Error, '%s' isnt one of your countries"),p->nombre);
			return TEG_STATUS_UNEXPECTED;
		}
	} else if( pais_destino == -1 ) {
		if( pais_origen == p->id ) {
			textmsg(M_INF,_("Source country is the same as the destination. Resetting the regroup..."));
			reagrupe_reset();
			return TEG_STATUS_SUCCESS;
		}

		if(p->numjug == WHOAMI()  ) {
			if( paises_eslimitrofe( pais_origen, p->id ) ) {
				p->selected &= ~PAIS_SELECT_REGROUP_ENTER;
				p->selected |= PAIS_SELECT_REGROUP;
				gui_pais_select(p->id);
				pais_destino = p->id;
				textmsg(M_INF,_("Destination country: '%s'. Now select the quantity of armies to move"),p->nombre);
				gui_reagrupe(pais_origen, pais_destino, g_paises[pais_origen].ejercitos - g_paises[pais_origen].ejer_reagrupe - 1);
			} else {
				textmsg(M_ERR,_("Error, '%s' isnt frontier with '%s'"),p->nombre,g_paises[pais_origen].nombre);
				return TEG_STATUS_UNEXPECTED;
			}
		} else {
			textmsg(M_ERR,_("Error, '%s' isnt one of your countries"),p->nombre);
			reagrupe_reset();
			return TEG_STATUS_UNEXPECTED;
		}
	} else {
		textmsg(M_ERR,_("Error, unexpected error in reagrupe_click(). Report this bug!"));
		reagrupe_reset();
		return TEG_STATUS_UNEXPECTED;
	}

	return TEG_STATUS_SUCCESS;
}

void reagrupe_reset( void )
{
	if( pais_origen != -1 ) {
		g_paises[pais_origen].selected &= ~PAIS_SELECT_REGROUP;
		gui_pais_select( pais_origen );
		pais_origen = -1;
	}

	if( pais_destino != -1 ) {
		g_paises[pais_destino].selected &= ~PAIS_SELECT_REGROUP;
		gui_pais_select( pais_origen );
		pais_destino = -1;
	}
}

void reagrupe_bigreset( void )
{
	int i;

	reagrupe_reset();

	for(i=0;i<PAISES_CANT;i++)
		g_paises[i].ejer_reagrupe = 0;
	last_origen = -1;
	last_destino = -1;
	last_cant = 0;
}

TEG_STATUS reagrupe_init( void )
{
	ataque_reset();

	if( reagrupe_check() != TEG_STATUS_SUCCESS ) {
		textmsg( M_ERR,_("Error, you can't regroup your armies now"));
		return TEG_STATUS_ERROR;
	}
	reagrupe_reset();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS reagrupe_set_and_save( int src, int dst, int cant )
 * Funcion usada para setear el ejer_reagrupe y si hay algun error
 * en el envio luego restaura
 */
TEG_STATUS reagrupe_set_and_save( int src, int dst, int cant )
{
	last_origen = src;
	last_destino = dst;
	last_cant = cant;
	g_paises[ dst ].ejer_reagrupe += cant;

	return TEG_STATUS_SUCCESS;
}

/**
 * @ TEG_STATUS reagrupe_restore_from_error( void )
 * Si hubo algun error en el envio del reagrupe, esto se encarga
 * de restorear el ejer_reagrupe
 */
TEG_STATUS reagrupe_restore_from_error( void )
{
	if( last_origen != -1 && last_destino != -1 && last_cant >= 0) {
		g_paises[ last_destino ].ejer_reagrupe -= last_cant;
		return TEG_STATUS_SUCCESS;
	} else {
		return TEG_STATUS_ERROR;
	}

}

/**
 * @fn TEG_STATUS reagrupe_out()
 * Envia ejercitos que se estan reagrupando
 */
TEG_STATUS reagrupe_out( int src, int dst, int cant)
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if(e==JUG_ESTADO_REAGRUPE) {
		reagrupe_reset();
		reagrupe_set_and_save( src, dst, cant );
		net_printf(g_juego.fd,TOKEN_REAGRUPE"=%d,%d,%d\n",src,dst,cant);
	} else {
		textmsg(M_ERR,_("Error, you cant regroup now."));
		return TEG_STATUS_ERROR;
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS reagrupe_enter( PPAIS p )
 * Cuando se esta reagrupando resalta los paises que se pueden reagrupar
 * @param p Pais a resaltar
 */
TEG_STATUS reagrupe_enter( PPAIS p )
{
	if( reagrupe_check() != TEG_STATUS_SUCCESS ) {
		return TEG_STATUS_UNEXPECTED;
	}

	if( pais_origen == -1 ) {
		if(p->numjug == WHOAMI() ) {
			if( p->ejercitos - p->ejer_reagrupe > 1 ) {
				p->selected |= PAIS_SELECT_REGROUP_ENTER;
				gui_pais_select(p->id);
			}
		}
	} else if( pais_destino == -1 ) {
		if(p->numjug == WHOAMI()  ) {
			if( paises_eslimitrofe( pais_origen, p->id ) ) {
				p->selected |= PAIS_SELECT_REGROUP_ENTER;
				gui_pais_select(p->id);
			}
		}
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS reagrupe_leave( PPAIS p )
{
	if( reagrupe_check() != TEG_STATUS_SUCCESS ) {
		return TEG_STATUS_UNEXPECTED;
	}
	if( p->selected & PAIS_SELECT_REGROUP_ENTER ) {
		p->selected &= ~PAIS_SELECT_REGROUP_ENTER;
		gui_pais_select(p->id);
	}
	return TEG_STATUS_SUCCESS;
}
