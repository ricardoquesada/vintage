/*	$Id: main.c,v 1.20 2001/11/10 18:56:35 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file main.c
 * archivo principal del cliente
 */

#include <stdio.h>

#include "client.h"

#ifdef WITH_GGZ
#include "ggz_client.h"
#endif /* WITH_GGZ */


TEG_STATUS main_init()
{
	juego_init();
	g_juego.human = 1;
	jugador_init();
	scores_init();
	return TEG_STATUS_SUCCESS;
}

int main( int argc, char **argv)
{
	int i;

	init_nls();
	dont_run_as_root();

	i = 1;

	main_init();

	if( gui_init(argc, argv) != TEG_STATUS_SUCCESS ) {
		printf("Aborting...\n");
		exit(-1);
	}

#ifdef WITH_GGZ
	if( g_juego.with_ggz) ggz_client_init("teg");
#endif /* WITH_GGZ */

	gui_main();

#ifdef WITH_GGZ
	if( g_juego.with_ggz) ggz_client_quit();
#endif /* WITH_GGZ */

	return 1;
}
