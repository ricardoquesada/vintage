/*	$Id: ataque.c,v 1.17 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file ataque.c
 * Contiene algunas funciones auxiliares para el manejo del estado 'ESTADO_TURNO'
 */

#include "client.h"


static int pais_origen = -1;
static int pais_destino = -1;
static int pais_origen_bak = -1;
static int pais_destino_bak = -1;

static int show_src = -1;
static int show_dst = -1;

/**
 * @fn static TEG_STATUS ataque_check()
 * Dice si se esta en condiciones de hacer algo con el ataque
 */
static TEG_STATUS ataque_check()
{
	JUG_ESTADO e;

	e = ESTADO_GET();

	if( e==JUG_ESTADO_ATAQUE || e==JUG_ESTADO_TROPAS) {
		ESTADO_SET(JUG_ESTADO_ATAQUE);
		return TEG_STATUS_SUCCESS;
	} else
		return TEG_STATUS_ERROR;
}

/**
 * @fn static void ataque_reset()
 * resetea el ataque
 */
void ataque_reset()
{
	if( pais_origen != -1 ) {
		g_paises[pais_origen].selected &= ~PAIS_SELECT_ATTACK;
		gui_pais_select(pais_origen);
		pais_origen = -1;
	}

	if( pais_destino != -1 ) {
		g_paises[pais_destino].selected &= ~PAIS_SELECT_ATTACK;
		gui_pais_select(pais_destino);
		pais_destino = -1;
	}
}

/**
 * @fn void ataque_restore()
 */
void ataque_restore()
{
	pais_origen = pais_origen_bak;
	pais_destino = pais_destino_bak;
}

/**
 * @fn void ataque_backup()
 */
void ataque_backup()
{
	pais_origen_bak = pais_origen;
	pais_destino_bak = pais_destino;
}

/**
 * @fn TEG_STATUS ataque_init()
 */
TEG_STATUS ataque_init()
{
	if( ataque_check() != TEG_STATUS_SUCCESS )
		return TEG_STATUS_UNEXPECTED;

	ataque_backup();
	ataque_reset();

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS ataque_click( PPAIS p )
 */
TEG_STATUS ataque_click( PPAIS p )
{
	if( ataque_check() != TEG_STATUS_SUCCESS ) {
		textmsg(M_ERR,_("Error, It's not the time to attack"));
		return TEG_STATUS_UNEXPECTED;
	}

	if( pais_origen == -1 ) {
		if(p->numjug == WHOAMI()) {
			if( p->ejercitos >1 ) {
				p->selected &= ~PAIS_SELECT_ATTACK_ENTER;
				p->selected |= PAIS_SELECT_ATTACK;
				gui_pais_select(p->id);
				pais_origen = p->id;
				textmsg(M_INF,_("Source country: '%s'. Now select the destination country"),p->nombre);
			} else {
				textmsg(M_ERR,_("Error, '%s' must have at least 2 armies"),p->nombre);
				return TEG_STATUS_UNEXPECTED;
			}
		} else {
			textmsg(M_ERR,_("Error, '%s' isnt one of your countries"),p->nombre);
			return TEG_STATUS_UNEXPECTED;
		}
	} else if( pais_destino == -1 ) {
		if( pais_origen == p->id ) {
			textmsg(M_INF,_("Source country is the same as the destination. Resetting the attack..."));
			ataque_reset();
			return TEG_STATUS_SUCCESS;
		}

		if(p->numjug != WHOAMI() ) {
			if( paises_eslimitrofe(pais_origen, p->id) ) {
				p->selected &= ~PAIS_SELECT_ATTACK_ENTER;
				p->selected |= PAIS_SELECT_ATTACK;
				gui_pais_select(p->id);
				pais_destino = p->id;
				textmsg(M_INF,_("Destination country: '%s'. Attacking..."),p->nombre);
				ataque_out();
			} else {
				textmsg(M_ERR,_("Error, '%s' isnt frontier with '%s'"),p->nombre,g_paises[pais_origen].nombre);
				ataque_reset();
				return TEG_STATUS_UNEXPECTED;
			}
		} else {
			textmsg(M_ERR,_("Error, you cant attack your own countries ('%s')"),p->nombre);
			ataque_reset();
			return TEG_STATUS_UNEXPECTED;
		}
	} else {
		ataque_reset();
		textmsg(M_ERR,_("Error, unexpected error in ataque_click(). Report this bug!"));
		return TEG_STATUS_UNEXPECTED;
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS ataque_finish( int *ori, int *dst )
 */
TEG_STATUS ataque_finish( int *ori, int *dst )
{
	if( ataque_check() != TEG_STATUS_SUCCESS )
		return TEG_STATUS_UNEXPECTED;

	if( pais_destino == -1 || pais_origen == -1 )
		return TEG_STATUS_ERROR;

	*ori = pais_origen;
	*dst = pais_destino;

	return TEG_STATUS_SUCCESS;
}


TEG_STATUS ataque_pre_reset()
{
	if( ataque_check() != TEG_STATUS_SUCCESS )
		return TEG_STATUS_ERROR;
	else {
		ataque_reset();
		return TEG_STATUS_SUCCESS;
	}
}


/**
 * @fn TEG_STATUS ataque_out()
 * Sends to server the attack message
 */
TEG_STATUS ataque_out()
{
	int src;
	int dst;

	if( ataque_finish( &src, &dst ) != TEG_STATUS_SUCCESS )  {
		textmsg( M_ERR,_("Error, make sure to select the countries first."));
		ataque_init();
		return TEG_STATUS_ERROR;
	}

	net_printf(g_juego.fd,TOKEN_ATAQUE"=%d,%d\n",src,dst);
	ataque_backup();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS ataquere_out()
 * Sends to server the attack message
 */
TEG_STATUS ataquere_out()
{
	ataque_restore();
	return ataque_out();
}

/**
 * @fn TEG_STATUS ataque_enter( PPAIS p )
 * Es para cuando el mouse se pone arriba del pais, lo resalta
 * @param p Pais a resaltar
 */
TEG_STATUS ataque_enter( PPAIS p )
{
	if( ataque_check() != TEG_STATUS_SUCCESS ) {
		return TEG_STATUS_UNEXPECTED;
	}

	if( pais_origen == -1 ) {
		if(p->numjug == WHOAMI()) {
			if( p->ejercitos >1 ) {
				if( !(p->selected & PAIS_SELECT_ATTACK_ENTER)) {
					p->selected |= PAIS_SELECT_ATTACK_ENTER;
					gui_pais_select(p->id);
				}
			}
		}
	} else if( pais_destino == -1 ) {
		if(p->numjug != WHOAMI() ) {
			if( paises_eslimitrofe(pais_origen, p->id) ) {
				if( !(p->selected & PAIS_SELECT_ATTACK_ENTER)) {
					p->selected |= PAIS_SELECT_ATTACK_ENTER;
					gui_pais_select(p->id);
				}
			}
		}
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS ataque_leave( PPAIS p )
{
	if( ataque_check() != TEG_STATUS_SUCCESS ) {
		return TEG_STATUS_UNEXPECTED;
	}
	if( p->selected & PAIS_SELECT_ATTACK_ENTER ) {
		p->selected &= ~PAIS_SELECT_ATTACK_ENTER;
		gui_pais_select(p->id);
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS ataque_unshow()
 * Desmuestra el ataque de un pais a otro. No usar esto para mostrar mis ataques
 */
TEG_STATUS ataque_unshow()
{

	if( show_src != -1 ) {
		g_paises[show_src].selected &= ~PAIS_SELECT_ATTACK_SRC;
		gui_pais_select(show_src);
		show_src = -1;
	}

	if( show_dst != -1 ) {
		g_paises[show_dst].selected &= ~PAIS_SELECT_ATTACK_DST;
		gui_pais_select(show_dst);
		show_dst = -1;
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS ataque_show( int src, int dst )
 * Muestra el ataque de un pais a otro. No usar esto para mostrar mis ataques
 */
TEG_STATUS ataque_show( int src, int dst )
{
	ataque_unshow();

	show_src = src;
	show_dst = dst;

	g_paises[src].selected |= PAIS_SELECT_ATTACK_SRC;
	g_paises[dst].selected |= PAIS_SELECT_ATTACK_DST;
	gui_pais_select(src);
	gui_pais_select(dst);

	return TEG_STATUS_SUCCESS;
}
