/*	$Id: misc.h,v 1.16 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file misc.h
 */
#ifndef __TEGC_MISC_H
#define __TEGC_MISC_H

TEG_STATUS juego_init();
TEG_STATUS juego_finalize();
TEG_STATUS conectar();
TEG_STATUS desconectar();
TEG_STATUS playerid_restore_from_error( void );
TEG_STATUS launch_server( int port );
TEG_STATUS launch_robot( void );
TEG_STATUS textmsg( int level, char *format, ...);
TEG_STATUS dirs_create();

#define ESTADO_ES(a) (g_juego.estado==(a))
#define ESTADO_MENOR(a) (g_juego.estado<(a))
#define ESTADO_MAYOR(a) (g_juego.estado>(a))
#define ESTADO_MENOR_IGUAL(a) (g_juego.estado<=(a))
#define ESTADO_MAYOR_IGUAL(a) (g_juego.estado>=(a))

#define WHOAMI() g_juego.numjug
#define ESTADO_GET() g_juego.estado
#define ESTADO_SET(a) (g_juego.estado=(a))

#endif /* __TEGC_MISC_H */
