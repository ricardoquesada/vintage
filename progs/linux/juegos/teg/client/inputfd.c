/*	$Id: inputfd.c,v 1.80 2001/12/21 22:10:01 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file inputfd.c
 * Maneja los 'tokens' de entrada
 */
#include <string.h>
#include <stdio.h>

#include "client.h"

TEG_STATUS clitok_rem(char *str);
TEG_STATUS clitok_status(char *str);
TEG_STATUS clitok_start(char *str);
TEG_STATUS clitok_playerid(char *str);
TEG_STATUS clitok_newplayer(char *str);
TEG_STATUS clitok_message(char *str);
TEG_STATUS clitok_paises(char *str);
TEG_STATUS clitok_fichas(char *str);
TEG_STATUS clitok_fichas2(char *str);
TEG_STATUS clitok_fichasc(char *str);
TEG_STATUS clitok_turno(char *str);
TEG_STATUS clitok_ataque(char *str);
TEG_STATUS clitok_dados(char *str);
TEG_STATUS clitok_pais(char *str);
TEG_STATUS clitok_error(char *str);
TEG_STATUS clitok_ok(char *str);
TEG_STATUS clitok_tropas(char *str);
TEG_STATUS clitok_tarjeta(char *str);
TEG_STATUS clitok_canje(char *str);
TEG_STATUS clitok_modalidad(char *str);
TEG_STATUS clitok_objetivo(char *str);
TEG_STATUS clitok_winner( char *str );
TEG_STATUS clitok_lost( char *str );
TEG_STATUS clitok_exit( char *str );
TEG_STATUS clitok_pversion( char *str );
TEG_STATUS clitok_sversion( char *str );
TEG_STATUS clitok_ggz( char *str );
TEG_STATUS clitok_serverfull(void);
TEG_STATUS clitok_surrender(char *str);
TEG_STATUS clitok_empezado(void);
TEG_STATUS clitok_kick(char *str);
TEG_STATUS clitok_scores(char *str);
TEG_STATUS clitok_reconnect(char *str);
TEG_STATUS clitok_enum_cards( char *str );
TEG_STATUS clitok_playersscores( char *str );

struct {
	char *label;
	TEG_STATUS (*func) ();
} tokens[] = {
	{ TOKEN_REM,		clitok_rem 	},
	{ TOKEN_STATUS,		clitok_status 	},
	{ TOKEN_START,		clitok_start 	},
	{ TOKEN_PLAYERID,	clitok_playerid	},
	{ TOKEN_NEWPLAYER,	clitok_newplayer},
	{ TOKEN_MESSAGE,	clitok_message	},
	{ TOKEN_PAISES,		clitok_paises	},
	{ TOKEN_FICHAS,		clitok_fichas	},
	{ TOKEN_FICHAS2,	clitok_fichas2	},
	{ TOKEN_FICHASC,	clitok_fichasc	},
	{ TOKEN_TURNO,		clitok_turno	},
	{ TOKEN_ATAQUE,		clitok_ataque	},
	{ TOKEN_DADOS,		clitok_dados	},
	{ TOKEN_PAIS,		clitok_pais	},
	{ TOKEN_ERROR,		clitok_error	},
	{ TOKEN_OK,		clitok_ok	},
	{ TOKEN_TROPAS,		clitok_tropas	},
	{ TOKEN_TARJETA,	clitok_tarjeta	},
	{ TOKEN_CANJE,		clitok_canje	},
	{ TOKEN_MODALIDAD,	clitok_modalidad},
	{ TOKEN_OBJETIVO,	clitok_objetivo	},
	{ TOKEN_WINNER,		clitok_winner	},
	{ TOKEN_LOST,		clitok_lost	},
	{ TOKEN_EXIT,		clitok_exit	},
	{ TOKEN_PVERSION,	clitok_pversion	},
	{ TOKEN_SVERSION,	clitok_sversion	},
	{ TOKEN_SURRENDER,	clitok_surrender},
#ifdef WITH_GGZ
	{ TOKEN_GGZ,		clitok_ggz	},
#endif /* WITH_GGZ */
	{ TOKEN_SERVERFULL,	clitok_serverfull},
	{ TOKEN_GAMEINPROGRESS,	clitok_empezado	},
	{ TOKEN_KICK,		clitok_kick	},
	{ TOKEN_SCORES,		clitok_scores	},
	{ TOKEN_RECONNECT,	clitok_reconnect},
	{ TOKEN_ENUM_CARDS,	clitok_enum_cards},

};
#define	NRCLITOKENS  (sizeof(tokens)/sizeof(tokens[0]))

struct {
	char *label;
	TEG_STATUS (*func) ();
} err_tokens[] = {
	{ TOKEN_FICHAS,		fichas_restore_from_error },
	{ TOKEN_FICHAS2,	fichas2_restore_from_error },
	{ TOKEN_FICHASC,	fichasc_restore_from_error },
	{ TOKEN_REAGRUPE,	reagrupe_restore_from_error },
	{ TOKEN_EJER2,		ejer2_restore_from_error },
	{ TOKEN_PLAYERID,	playerid_restore_from_error },
	{ TOKEN_START,		aux_start_error },
};
#define	NRERRCLITOKENS  (sizeof(err_tokens)/sizeof(err_tokens[0]))

/**
 * @fn TEG_STATUS clitok_serverfull( void )
 */
TEG_STATUS clitok_serverfull( void )
{
	textmsg( M_ERR, _("The server is full. Try connecting as an observer"));
	desconectar();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS clitok_empezado( void )
 */
TEG_STATUS clitok_empezado( void )
{
	textmsg( M_ERR, _("The game has already started. Try connecting as an observer."));
	desconectar();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS clitok_kick( char *name )
 */
TEG_STATUS clitok_kick( char *name )
{
	if( name && strlen(name) )
		textmsg( M_IMP, _("Player %s was kicked from the game"),name);
	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS clitok_serverfull( void )
 * Informs that a player has surrender
 */
TEG_STATUS clitok_surrender( char *str )
{
	int numjug;
	PJUGADOR pJ;

	numjug = atoi( str );
	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS) {
		/* no lo tengo en la base */
		textmsg( M_IMP,_("Player %d abandoned the game"), numjug );
		return TEG_STATUS_SUCCESS;
	}

	textmsg( M_IMP,_("Player %s(%s) abandoned the game"),
				pJ->nombre,
				_(g_colores[pJ->color])
				);

	if( pJ->numjug == WHOAMI() ) {
		ESTADO_SET(JUG_ESTADO_GAMEOVER);
		gui_sensi();
	}

	out_paises();

	gui_surrender(numjug);

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS clitok_exit( char *str )
 * Dice que un jugador perdio coneccion o se fue
 */
TEG_STATUS clitok_exit( char *str )
{
	int numjug;
	PJUGADOR pJ;

	numjug = atoi( str );
	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS) {
		/* no lo tengo en la base */
		textmsg( M_IMP,_("Player %d exit the game"), numjug );
		return TEG_STATUS_SUCCESS;
	}

	textmsg( M_IMP,_("Player %s(%s) exit the game"),
				pJ->nombre,
				_(g_colores[pJ->color])
				);

	/* dont delete the player, I need the status */
	/* jugador_del( pJ ); */

	if( WHOAMI() == numjug ) {
		/* por alguna razon el server quiere que abandone el juego */
		desconectar();
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS clitok_winner( char *str )
 * Dice que un jugador gano
 */
TEG_STATUS clitok_winner( char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int numjug;
	int objetivo;
	PJUGADOR pJ;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		objetivo = atoi( p.token );
	} else goto error;


	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS)
		goto error;

	if( objetivo == -1 ) objetivo = 0;

	if( objetivo < 0 || objetivo >= objetivos_cant() )
		goto error;

	textmsg( M_IMP,_("Player %s(%s) is the WINNER"),
				pJ->nombre,
				_(g_colores[pJ->color])
				);

	gui_winner( pJ->numjug, objetivo );

	ESTADO_SET( JUG_ESTADO_HABILITADO );
	gui_sensi();

	juego_finalize();
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_winner()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_lost( char *str )
 * Dice que un jugador perdio
 */
TEG_STATUS clitok_lost( char *str )
{
	int numjug;
	PJUGADOR pJ;

	numjug = atoi( str );
	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS)
		goto error;

	textmsg( M_IMP,_("Player %s(%s) lost the game\n"),
				pJ->nombre,
				_(g_colores[pJ->color])
				);

	if( pJ->numjug == WHOAMI() )
		ESTADO_SET(JUG_ESTADO_GAMEOVER);

	gui_lost( pJ->numjug );

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_lost()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_tropas( char *str)
 * Avisa que se puden pasar algunos ejercitos al pais conquistado
 */
TEG_STATUS clitok_tropas( char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int src,dst,cant;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		src = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		dst = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		cant = atoi( p.token );
	} else goto error;

	ESTADO_SET(JUG_ESTADO_TROPAS);

	gui_tropas( src,dst,cant);

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_tropas()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_ok( char *str)
 */
TEG_STATUS clitok_ok( char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_ok()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_error( char *str)
 * Recibe los mensajes de error del servidor
 */
TEG_STATUS clitok_error( char *str)
{
	int i;
	if( strlen(str)==0 )
		goto error;


	for(i = 0; i < NRERRCLITOKENS; i++) {
		if(strcasecmp( str, err_tokens[i].label )==0 ){
			if (err_tokens[i].func)
				return( (err_tokens[i].func)());
			textmsg(M_ERR,_("The server report an error in '%s'"),str);
			return TEG_STATUS_TOKENNULL;
		}
	}
	return TEG_STATUS_SUCCESS;

error:
	textmsg(M_ERR,"Error in clitok_error()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_pais( char *str)
 * Actualiza un solo pais (usado pora tropas, dados, y mas
 */
TEG_STATUS clitok_pais( char *str)
{
	int pais;
	int jug;
	int ejer;

	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pais = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		jug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		ejer = atoi( p.token );
	} else goto error;

	g_paises[pais].numjug = jug;
	g_paises[pais].ejercitos = ejer;

	gui_pais( pais );

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_pais()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_dados( char *str)
 */
TEG_STATUS clitok_dados( char *str)
{
	int i;

	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	/* atacante */
	if( parser_call( &p ) && p.hay_otro ) {
		g_juego.dados_srcpais = atoi( p.token );
		if( g_juego.dados_srcpais >= PAISES_CANT || g_juego.dados_srcpais < 0 ) {
			g_juego.dados_srcpais = -1;
			goto error;
		}
	}

	for(i=0;i<3;i++) {
		if( parser_call( &p ) && p.hay_otro ) {
			g_juego.dados_src[i] = atoi( p.token );
		} else goto error;
	}

	/* defensor */
	if( parser_call( &p ) && p.hay_otro ) {
		g_juego.dados_dstpais = atoi( p.token );
		if( g_juego.dados_dstpais >= PAISES_CANT || g_juego.dados_dstpais < 0 ) {
			g_juego.dados_dstpais = -1;
			goto error;
		}
	}

	for(i=0;i<2;i++) {
		if( parser_call( &p ) && p.hay_otro ) {
			g_juego.dados_dst[i] = atoi( p.token );
		} else goto error;
	}

	if( parser_call( &p ) && !p.hay_otro ) {
		g_juego.dados_dst[2] = atoi( p.token );
	} else goto error;

	textmsg(M_INF,_("Dices: %s: %d %d %d vs. %s: %d %d %d")
			,g_paises[g_juego.dados_srcpais].nombre
			,g_juego.dados_src[0]
			,g_juego.dados_src[1]
			,g_juego.dados_src[2]
			,g_paises[g_juego.dados_dstpais].nombre
			,g_juego.dados_dst[0]
			,g_juego.dados_dst[1]
			,g_juego.dados_dst[2] );

	gui_dados();

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_dados()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_ataque( char *str)
 * avisa que 'src' esta atacando a 'dst'
 */
TEG_STATUS clitok_ataque( char *str)
{
	int src,dst;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	PJUGADOR pJsrc, pJdst;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		src = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		dst = atoi( p.token );
	} else goto error;

	if( jugador_whois( g_paises[src].numjug, &pJsrc ) != TEG_STATUS_SUCCESS )
		goto error;

	if( jugador_whois( g_paises[dst].numjug, &pJdst ) != TEG_STATUS_SUCCESS )
		goto error;

	if( src<0 || src >= PAISES_CANT || dst <0 || dst >= PAISES_CANT )
		goto error;

	if( g_paises[src].numjug == WHOAMI() ) {
		ataque_reset();
	} else {
		ataque_show( src, dst );
	}

	textmsg(M_INF,_("%s(%s) is attacking %s(%s)"),g_paises[src].nombre, _(g_colores[pJsrc->color])
			,g_paises[dst].nombre, _(g_colores[pJdst->color]) );

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_ataque()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_turno( char *str)
 */
TEG_STATUS clitok_turno( char *str)
{
	int numjug;
	PJUGADOR j;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		numjug = atoi( p.token );
	} else goto error;

	if( jugador_whois( numjug, &j) != TEG_STATUS_SUCCESS)
		goto error;

	ataque_unshow();
	out_paises();

	if( numjug == g_juego.numjug ) {
		ESTADO_SET(JUG_ESTADO_ATAQUE);
		/* cosas a hacer cuando yo comienzo un nuevo turno */
		reagrupe_bigreset();
		ataque_init();
		textmsg(M_INF,_("Its your turn to attack!!"));
		gui_turno();
	} else  {
		/* No es mi turno, entonces yo tengo que estar en idle */
		textmsg(M_IMP,_("Player %s(%s) has the turn to attack!"),
				j->nombre,
				_(g_colores[j->color]) );
		ESTADO_SET(JUG_ESTADO_IDLE);
	}

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_turno()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_fichas( char *str)
 * Avisa que alguien esta poniendo fichas, y la cantidad que tiene que poner
 */
TEG_STATUS clitok_fichas( char *str)
{
	int numjug;
	int cant;
	PJUGADOR j;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		cant = atoi( p.token );
	} else goto error;

	if( jugador_whois( numjug, &j) != TEG_STATUS_SUCCESS)
		goto error;

	out_paises();

	if( numjug == g_juego.numjug ) {
		ESTADO_SET(JUG_ESTADO_FICHAS);
		fichas_init( cant, 0 );
		gui_fichas(cant,0);
	} else {
		textmsg(M_INF,_("Player %s(%s) is placing %d armies for 1st time"),
				j->nombre,
				_(g_colores[j->color]),
				cant);
	}

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_fichas()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_fichas2( char *str)
 * Avisa que alguien esta poniendo fichas2 y la cantidad que hay que poner
 */
TEG_STATUS clitok_fichas2( char *str)
{
	int numjug;
	int cant;
	PJUGADOR j;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		cant = atoi( p.token );
	} else goto error;

	if( jugador_whois( numjug, &j) != TEG_STATUS_SUCCESS)
		goto error;

	out_paises();

	if( numjug == g_juego.numjug ) {
		ESTADO_SET(JUG_ESTADO_FICHAS2);
		fichas_init( cant, 0 );
		gui_fichas(cant,0);
	} else {
		textmsg(M_INF,_("Player %s(%s) is placing %d armies for 2nd time"),
				j->nombre,
				_(g_colores[j->color]),
				cant);
	}

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_fichas2()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_fichasc( char *str)
 * Se encarga de entender el mensaje FICHASC
 * @param str string que contiene los datos
 * @return SUCCESS si esta bien construido el str
 */
TEG_STATUS clitok_fichasc( char *str)
{
	int numjug;
	int cant,tot_cant;
	unsigned long conts;
	PJUGADOR j;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		conts = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		cant = atoi( p.token );
	} else goto error;

	if( jugador_whois( numjug, &j) != TEG_STATUS_SUCCESS)
		goto error;

	ataque_unshow();

	tot_cant = cont_tot( conts ) + cant;

	out_paises();

	if( numjug == g_juego.numjug ) {
		ESTADO_SET(JUG_ESTADO_FICHASC);
		fichas_init( tot_cant, conts );
		gui_fichas(cant,conts);
	} else {
		textmsg(M_INF,_("Player %s(%s) is placing %d armies"),
				j->nombre,
				_(g_colores[j->color]),
				cant);
	}

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_fichasc()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_paises( char *str)
 * me dice los paises que tiene cierto jugador
 */
TEG_STATUS clitok_paises( char *str)
{
	int numjug;
	PJUGADOR j;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ '/', '/', '/' };


	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro )
		numjug = atoi( p.token );
	else 
		goto error;

	if( jugador_whois( numjug, &j) == TEG_STATUS_SUCCESS)
		return aux_paises( numjug, p.data );
error:
	textmsg(M_ERR,"Error in clitok_paises()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_playerid( char *str)
 * Me dice el player number y los colores disponibles
 */
TEG_STATUS clitok_playerid( char *str)
{
	char c[TEG_MAX_PLAYERS];
	PARSER p;
	int i;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( g_juego.myname, p.token, sizeof(g_juego.myname)-1);
		g_juego.myname[sizeof(g_juego.myname)-1]=0;
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		g_juego.numjug = atoi( p.token );
	} else goto error;

	for(i=0;i<TEG_MAX_PLAYERS-1;i++ ) {

		if( parser_call( &p ) && p.hay_otro ) {
			c[i] = atoi( p.token );	
		} else goto error;
	}

	if( parser_call( &p ) && !p.hay_otro ) {
		c[i] = atoi( p.token );	
	} else goto error;


	ESTADO_SET(JUG_ESTADO_CONNECTED);

	out_status();
	if( g_juego.observer )
		out_paises();

	gui_connected( c );

	textmsg( M_IMP,_("I'm player number:%d"),g_juego.numjug );
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_playerid()");
	return TEG_STATUS_ERROR;
}


TEG_STATUS clitok_reconnect( char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( g_juego.myname, p.token, sizeof(g_juego.myname)-1);
		g_juego.myname[sizeof(g_juego.myname)-1]=0;
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		g_juego.numjug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		g_juego.mycolor  = atoi( p.token );	
	} else goto error;


	ESTADO_SET(JUG_ESTADO_IDLE);

	out_status();
	out_paises();
	out_enum_cards();

	gui_reconnected();

	textmsg( M_IMP,_("Successful reconnection. I'm player number:%d"),g_juego.numjug );
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_reconnect()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_newplayer( char *str)
 * Un nuevo jugador entro
 */
TEG_STATUS clitok_newplayer( char *str)
{
	char name[PLAYERNAME_MAX_LEN];
	int color,numjug;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	JUGADOR j;
	PJUGADOR pJ;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( name, p.token, sizeof(name)-1 );
		name[sizeof(name)-1]=0;
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		color = atoi( p.token );
	} else goto error;

	if( jugador_whois( numjug, &pJ ) != TEG_STATUS_SUCCESS ) {
		memset(&j,0,sizeof(j));
		j.color = color;
		j.numjug = numjug;
		strncpy(j.nombre,name,sizeof(j.nombre)-1);
		j.nombre[sizeof(j.nombre)-1]=0;
		jugador_ins(&j);
	} else
		pJ->color =  color;

	if( numjug == WHOAMI() ) {
		g_juego.mycolor = color;
		ESTADO_SET( JUG_ESTADO_HABILITADO );
		textmsg( M_IMP,_("My color is: %s"),_(g_colores[color]) );
	} else {
		textmsg(M_IMP,_("Player[%d] '%s' is connected with color %s"),numjug,name,_(g_colores[color]));
	}
	gui_habilitado( numjug );

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_newplayer()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_message( char *str)
 * Me enviaron un mensaje
 */
TEG_STATUS clitok_message( char *str)
{
	char name[PLAYERNAME_MAX_LEN];
	int numjug;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	if( strlen(str) == 0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( name, p.token, sizeof(name)-1);
		name[sizeof(name)-1]=0;
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );		
	} else goto error;

	/* I dont care if there is one more or not */

	if( g_juego.msg_show & M_MSG ) {
		gui_textplayermsg(name,numjug,p.data);
	}
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_message()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_rem( char *str)
 * Ignorar. Usado para enviar help en ascii
 */
TEG_STATUS clitok_rem( char *str)
{
	if(strlen(str)>0)
		textmsg( M_IMP,str );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS clitok_status( char *str)
 * Muestra el estado de todos los jugadores
 */
TEG_STATUS clitok_status( char *str)
{
	JUGADOR j,*j_tmp;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ '/', '/', '/' };

	int i;

	jugador_flush();

	if( strlen(str)==0 )
		goto ok;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;


	do {
		if( (i=parser_call( &p )) ) {
			if( aux_status( &j, p.token ) != TEG_STATUS_SUCCESS )
				goto error;

			if( jugador_whois( j.numjug, &j_tmp ) == TEG_STATUS_SUCCESS )
				jugador_update( &j );
			else
				jugador_ins( &j );
		}
	} while( i && p.hay_otro);
ok:
	gui_status();
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_status()");
	return TEG_STATUS_PARSEERROR;
}


TEG_STATUS clitok_scores( char *str)
{
	PSCORES pS;
	SCORES score;
	PARSER p;
	DELIM igualador = DELIM_NULL;
	DELIM separador={ '\\', '\\', '\\' };

	int i;

	if( strlen(str)==0 )
		goto ok;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	scores_flush();

	do {
		if( (i=parser_call( &p )) ) {
			if( aux_scores( &score, p.token ) != TEG_STATUS_SUCCESS )
				goto error;

			pS = malloc( sizeof(*pS));
			if( ! pS )
				goto error;

			*pS = score;
			scores_insert_score( pS );
		}
	} while( i && p.hay_otro);

ok:
	gui_scores();
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_scores()");
	return TEG_STATUS_PARSEERROR;
}



/**
 * @fn TEG_STATUS clitok_start(char *str)
 * Avisa que empezo el juego
 */
TEG_STATUS clitok_start(char *str)
{
	JUGADOR j;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ '/', '/', '/' };
	int i;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	jugador_flush();
	do {
		if( (i=parser_call( &p )) ) {
			if( aux_status( &j, p.token ) != TEG_STATUS_SUCCESS )
				goto error;

			jugador_ins( &j );
		}
	} while( i && p.hay_otro);

	ESTADO_SET(JUG_ESTADO_START);

	out_paises();
	gui_start();
	return TEG_STATUS_SUCCESS;

error:
	textmsg(M_ERR,"Error in clitok_start()");
	return TEG_STATUS_ERROR;
}


TEG_STATUS clitok_enum_cards( char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int country, used;
	PLIST_ENTRY ltmp;

	g_juego.tarjetas_cant = 0;

	while( ! IsListEmpty( &g_juego.tarjetas_list ) )
		ltmp = RemoveHeadList( &g_juego.tarjetas_list );

	if(  ! str || strlen(str) == 0 )
		goto ok;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	do {
		if( parser_call( &p ) ) {
			country = atoi( p.token );
			used = atoi( p.value );
		} else 
			goto error;

		if( country < 0 || country >= PAISES_CANT )
			goto error;

		InsertTailList( &g_juego.tarjetas_list, (PLIST_ENTRY) &g_paises[ country ].tarjeta );
		g_juego.tarjetas_cant++;

		if( used ) tarjeta_usar( &g_paises[ country ].tarjeta );
		g_paises[ country ].tarjeta.numjug = WHOAMI();

	} while ( p.hay_otro );

ok:
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_enum_cards()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_canje(char *str)
 * Alguien realizo un canje y pude ser yo
 */
TEG_STATUS clitok_canje(char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int p1,p2,p3;
	int numjug,cant;
	PJUGADOR pJ;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		numjug = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		cant = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		p1 = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		p2 = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		p3 = atoi( p.token );		
	} else goto error;

	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS)
		goto error;

	if( numjug == WHOAMI() ) {
		PLIST_ENTRY pL = g_juego.tarjetas_list.Flink;

		while( !IsListEmpty( &g_juego.tarjetas_list ) && (pL != &g_juego.tarjetas_list )) {
			PPAIS pP;
			PTARJETA pT = (PTARJETA) pL;
			pP = (PPAIS ) PAIS_FROM_TARJETA( pT );

			if( pP->id == p1 || pP->id == p2 || pP->id == p3 ) {
				PLIST_ENTRY l;

				g_paises[ pP->id ].tarjeta.numjug = -1;
				l = RemoveHeadList( pL->Blink );
				g_juego.tarjetas_cant--;

			}
			pL = LIST_NEXT( pL );
		}

		fichas_add_wanted( cant );
		textmsg( M_IMP,_("Exchanged approved. Now you can place %d more armies!"),cant);
		gui_canje(cant,p1,p2,p3);
	} else {
		textmsg(M_IMP,_("Player %s(%s) exchanged 3 cards for %d armies"),
				pJ->nombre,
				_(g_colores[pJ->color]),
				cant);
	}

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_tarjeta()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_modalidad(char *str)
 * Dice con que reglas se va a jugar.
 */
TEG_STATUS clitok_modalidad(char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int objetivos,reglas;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		objetivos = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		reglas = atoi( p.token );		
	} else goto error;

	g_juego.reglas = reglas ;

	out_objetivos();

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_tarjeta()");
	return TEG_STATUS_ERROR;
}
/**
 * @fn TEG_STATUS clitok_objetivo(char *str)
 * Dice con que reglas se va a jugar.
 */
TEG_STATUS clitok_objetivo(char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int objetivo;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && !p.hay_otro ) {
		objetivo = atoi( p.token );
	} else goto error;

	if( objetivo < 0 || objetivo >= objetivos_cant() )
		goto error;

	g_juego.objetivo = objetivo;
	gui_objetivo();
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_objetivo()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_tarjeta(char *str)
 * I'm receiving the card I've requested after finishing my turn
 * @param str which card and if the server placed two armies there for me
 */
TEG_STATUS clitok_tarjeta(char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int pais,used;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;


	if( parser_call( &p ) && p.hay_otro ) {
		pais = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		used = atoi( p.token );		
	} else goto error;

	if( pais < 0 || pais >= PAISES_CANT )
		goto error;

	ESTADO_SET(JUG_ESTADO_TARJETA);

	InsertTailList( &g_juego.tarjetas_list, (PLIST_ENTRY) &g_paises[ pais ].tarjeta );
	g_juego.tarjetas_cant++;

	if( used ) tarjeta_usar( &g_paises[ pais ].tarjeta );
	g_paises[ pais ].tarjeta.numjug = WHOAMI();

	if( used ) {
		textmsg(M_IMP,_("You received card: '%s' and 2 armies where placed there"),g_paises[pais].nombre);
	} else {
		textmsg(M_IMP,_("You received card: '%s'"),g_paises[pais].nombre);
	}

	gui_tarjeta( pais );
	return TEG_STATUS_SUCCESS;

error:
	textmsg(M_ERR,"Error in clitok_tarjeta()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_pversion( char *str)
 * Servers's Protocol version. HIVER MUST be equal, otherwise wont work 
 */
TEG_STATUS clitok_pversion( char *str)
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int hi,lo;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( strlen(str)==0 )
		goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		hi = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		lo = atoi( p.token );
	} else goto error;

	if( hi != PROTOCOL_HIVER ) {
		textmsg(M_ERR,_("Aborting: Different protocols version. Server:%d Client:%d"),hi,PROTOCOL_HIVER);
		desconectar();
		return TEG_STATUS_ERROR;
	}

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_pversion()");
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS clitok_sversion( char *str)
 * Servers version
 */
TEG_STATUS clitok_sversion( char *str)
{
	if( strlen(str)==0 )
		goto error;

	textmsg(M_ALL,"%s",str);
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_pversion()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS clitok_ggz( char *str)
 */
#ifdef WITH_GGZ
TEG_STATUS clitok_ggz( char *str)
{
	if( !g_juego.with_ggz )
		goto error;

	out_pversion();
	out_id();
	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"Error in clitok_ggz()");
	return TEG_STATUS_ERROR;
}
#endif /* WITH_GGZ */



/*
 *	codigo de interpretacion
 */
TEG_STATUS client_lookup( int fd, PARSER *p )
{
	int i;

	for(i = 0; i < NRCLITOKENS; i++) {
		if(strcasecmp( p->token, tokens[i].label )==0 ){
			if (tokens[i].func)
				return( (tokens[i].func)(p->value));
			return TEG_STATUS_TOKENNULL;
		}
	}
	textmsg( M_ERR, _("Token '%s' not found"),p->token);
	return TEG_STATUS_TOKENNOTFOUND;
}

/**
 * @fn TEG_STATUS client_recv( int fd )
 * lee del fd los datos para luego parsearlos
 */
TEG_STATUS client_recv( int fd )
{
	int i,j;
	PARSER p;
	char str[PROT_MAX_LEN];
	DELIM igualador={ '=', '=', '=' };
	DELIM separador={ ';', ';', ';' };

	p.igualador = &igualador;
	p.separador = &separador;

	memset(str,0,sizeof(str));
	j=net_readline( fd, str, PROT_MAX_LEN );

	if( j<1 ) {
		desconectar();
		return TEG_STATUS_CONNCLOSED;
	}

	p.data = str;

	do {
		if( (i=parser_call( &p )) ) {
			if( client_lookup( fd,&p ) == TEG_STATUS_CONNCLOSED ) {
				return TEG_STATUS_CONNCLOSED;
			}
		}
	} while( i && p.hay_otro);

	return TEG_STATUS_SUCCESS;
}
