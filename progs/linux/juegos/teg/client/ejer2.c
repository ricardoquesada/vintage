/*	$Id: ejer2.c,v 1.5 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file ejer2.c
 * Contiene algunas funciones auxiliares para el manejo del estado 'ESTADO_EJER2'
 */

#include "client.h"

static int last_pais = -1;

/**
 * @fn TEG_STATUS ejer2_out( int pais )
 * Pone 2 ejercitos por la tarjeta que saco
 */
TEG_STATUS ejer2_out( int pais )
{
	if( g_paises[ pais ].numjug == WHOAMI() ) {
		if( ESTADO_ES( JUG_ESTADO_TARJETA ) ) {
			if( !tarjeta_es_usada( &g_paises[ pais ].tarjeta )) {
				tarjeta_usar( &g_paises[ pais ].tarjeta );
				last_pais = pais;
				net_printf(g_juego.fd,TOKEN_EJER2"=%d\n",pais);
				return TEG_STATUS_SUCCESS;
			} else {
				textmsg( M_ERR,_("Error, the 2 armies where placed before"));
				return TEG_STATUS_ERROR;
			}
		} else {
			textmsg( M_ERR,_("Error, it's not the time to put 2 armies."));
			return TEG_STATUS_ERROR;
		}
	} else {
		textmsg(M_ERR,_("Error, '%s' isnt one of your countries"),g_paises[pais].nombre);
		return TEG_STATUS_ERROR;
	}
}

TEG_STATUS ejer2_restore_from_error()
{
	if( last_pais != -1) {
		tarjeta_desusar( &g_paises[ last_pais ].tarjeta );
		gui_tarjeta( -1 );
		last_pais = -1;
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_ERROR;
}
