/*	$Id: outputfd.c,v 1.39 2001/12/09 19:32:10 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <riq@corest.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file outputfd.c
 * Envia los tokens. 
 */
#include <string.h>
#include <stdio.h>

#include "client.h"


/**
 * @fn TEG_STATUS out_mensaje()
 * Envia un mensaje a todos los jugadores
 */
TEG_STATUS out_mensaje( char *msg )
{
	if( ESTADO_MENOR(JUG_ESTADO_CONNECTED) )
		return TEG_STATUS_NOTCONNECTED;

	strip_invalid_msg(msg);
	net_printf( g_juego.fd, TOKEN_MESSAGE"=\"%s\"\n",msg);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_id( int modo )
 * Informa quien soy
 * @param modo 1 si es jugador, 0 si es observer
 */
TEG_STATUS out_id()
{
	if( ESTADO_MAYOR_IGUAL(JUG_ESTADO_CONNECTED ) )
		return TEG_STATUS_ERROR;

	strip_invalid(g_juego.myname);
	net_printf( g_juego.fd, TOKEN_PLAYERID"=%s,%d,%d\n", g_juego.myname, !g_juego.observer, g_juego.human );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_color()
 * Informa el color que quiero
 */
TEG_STATUS out_color( int color )
{
	if( ESTADO_MAYOR_IGUAL(JUG_ESTADO_HABILITADO) )
		return TEG_STATUS_ERROR;

	net_printf( g_juego.fd, TOKEN_COLOR"=%d\n", color );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_exit()
 * Abandona el juego
 */
TEG_STATUS out_exit()
{
	if( ESTADO_MENOR(JUG_ESTADO_CONNECTED) )
		return TEG_STATUS_NOTCONNECTED;

	net_printf( g_juego.fd, TOKEN_EXIT"\n");
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_paises()
 * Le pide al servidor el estado de los paises
 */
TEG_STATUS out_paises()
{
	if( ESTADO_MENOR(JUG_ESTADO_CONNECTED) ) {
		textmsg( M_ERR,_("Error, you must be connected"));
		return TEG_STATUS_NOTCONNECTED;
	}

	net_printf(g_juego.fd,TOKEN_PAISES"=-1\n");

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS out_enum_cards()
{
	if( ESTADO_MENOR(JUG_ESTADO_CONNECTED) ) {
		textmsg( M_ERR,_("Error, you must be connected"));
		return TEG_STATUS_NOTCONNECTED;
	}

	net_printf(g_juego.fd,TOKEN_ENUM_CARDS"\n");

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_status()
 * Le pide al servidor el estado de los jugadores
 */
TEG_STATUS out_status()
{
	if( ESTADO_MENOR(JUG_ESTADO_CONNECTED) )
		return TEG_STATUS_NOTCONNECTED;
	net_printf(g_juego.fd,TOKEN_STATUS"\n");
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_start()
 * Envia el 'start' al server
 */
TEG_STATUS out_start()
{
	if( ESTADO_ES(JUG_ESTADO_HABILITADO) ){
		net_printf(g_juego.fd,TOKEN_START"\n");
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_NOTCONNECTED;
}


/**
 * @fn TEG_STATUS out_tarjeta()
 * Sends to server a request for a 'get card'
 */
TEG_STATUS out_tarjeta()
{
	if( ESTADO_MENOR(JUG_ESTADO_ATAQUE) || ESTADO_MAYOR_IGUAL(JUG_ESTADO_TARJETA) ) {
		textmsg( M_ERR,_("Error, it's not the moment to get a card"));
		return TEG_STATUS_ERROR;
	}

	net_printf(g_juego.fd,TOKEN_TARJETA"\n");
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_tropas()
 * Envia al server el movimiento de tropas (luego de conq. un pais)
 */
TEG_STATUS out_tropas( int src, int dst, int cant)
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if(e==JUG_ESTADO_TROPAS || e==JUG_ESTADO_ATAQUE) {
		net_printf(g_juego.fd,TOKEN_TROPAS"=%d,%d,%d\n",src,dst,cant);
	} else {
		return TEG_STATUS_ERROR;
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_endturn()
 * Le dice al server que quiere terminar su turno
 */
TEG_STATUS out_endturn()
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if( e > JUG_ESTADO_TURNOSTART && e < JUG_ESTADO_TURNOEND ) {
		ataque_reset();
		reagrupe_reset();
		ESTADO_SET( JUG_ESTADO_IDLE );
		net_printf(g_juego.fd,TOKEN_TURNO"\n");
	} else {
		textmsg( M_ERR,_("Error, it's not your turn."));
		return TEG_STATUS_ERROR;
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_surrender()
 * Le dice al server que quiere terminar su turno
 */
TEG_STATUS out_surrender()
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if( e >= JUG_ESTADO_START ) {
		net_printf(g_juego.fd,TOKEN_SURRENDER"\n");
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS out_objetivos()
 * Le pedi al servidor un objetivo
 */
TEG_STATUS out_objetivos()
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if( e >= JUG_ESTADO_START ) {
		net_printf(g_juego.fd,TOKEN_OBJETIVO"\n");
	} else {
		textmsg( M_ERR,_("Error, the game is not started"));
		return TEG_STATUS_ERROR;
	}
	return TEG_STATUS_SUCCESS;
}
/**
 * @fn TEG_STATUS out_modalidad(int a)
 * Sets the Conquer-The-World option on/off
 * @param a if 1 conquer the world, else play with secret missions
 */
TEG_STATUS out_modalidad(int a)
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if( e >= JUG_ESTADO_START ) {
		textmsg( M_ERR,_("Error, the game is started so you cant change the type of game"));
		return TEG_STATUS_ERROR;
	} else {
		net_printf(g_juego.fd,TOKEN_SET"="OPTION_CONQWORLD"=%d\n",a);
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS out_loque( void )
 * Le pido al servidor que me diga que tengo que hacer
 */
TEG_STATUS out_loque( void )
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if( e >= JUG_ESTADO_CONNECTED ) {
		net_printf(g_juego.fd,TOKEN_LOQUE"\n");
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS out_echo( char *msg )
 * Le pido al servidor que me diga que me devuelta este mensaje
 */
TEG_STATUS out_echo( char *msg )
{
	JUG_ESTADO e;

	e = ESTADO_GET();
	if( e >= JUG_ESTADO_CONNECTED ) {
		net_printf(g_juego.fd,TOKEN_ECHO"=%s\n",msg);
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS out_pversion()
 * Sends the protocol version, request the server version, send the client version
 */
TEG_STATUS out_pversion()
{
	if( g_juego.fd > 0 ) {
		net_printf(g_juego.fd,TOKEN_PVERSION"=%d,%d\n",PROTOCOL_HIVER,PROTOCOL_LOVER);
		net_printf(g_juego.fd,TOKEN_SVERSION"\n");
		out_cversion();
	}

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS out_cversion()
{
	if( g_juego.fd > 0 )
		net_printf(g_juego.fd,TOKEN_CVERSION"=%s %s\n",_("TEG client version "),VERSION);

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS out_scores()
{
	JUG_ESTADO e = ESTADO_GET();

	if( e >= JUG_ESTADO_CONNECTED &&  g_juego.fd > 0 ) {
		net_printf(g_juego.fd,TOKEN_SCORES"\n");
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_ERROR;
}

/*
 * Funciones que estan en sus respectivos archivos ahora
 */
/* 
 * TEG_STATUS out_reagrupe( int src, int dst, int cant)
 * TEG_STATUS out_ejer2( int pais )
 * TEG_STATUS out_canje( int p1, int p2, int p3 )
 * TEG_STATUS out_fichas()
 * TEG_STATUS out_ataque()
 * TEG_STATUS out_reataque();
 */
