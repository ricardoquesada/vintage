/*	$Id: aux.c,v 1.22 2001/11/24 17:32:17 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <riq@corest.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file aux.c
 * Funciones auxiliares del cliente
 */

#include <string.h>

#include "client.h"

/**
 * @fn TEG_STATUS aux_status( PJUGADOR pj, char *str )
 * parsea el status de los jugadores
 */
TEG_STATUS aux_status( PJUGADOR pj, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	memset( pj, 0, sizeof(*pj));

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( pj->nombre, p.token, sizeof(pj->nombre)-1);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->color = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->score = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->numjug = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->estado = atoi( p.token);
	} else goto error;
	
	if( parser_call( &p ) && p.hay_otro ) {
		pj->tot_paises = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->tot_ejercitos = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->tot_tarjetas = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->empezo_turno = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pj->human = atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		strncpy( pj->addr, p.token, sizeof(pj->addr)-1);
	} else goto error;

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"error in aux_status()");
	return TEG_STATUS_ERROR;
}


TEG_STATUS aux_scores( PSCORES pS, char *str )
{
	PARSER p;
	DELIM igualador=DELIM_NULL;
	DELIM separador={ ',', ',', ',' };

	memset( pS, 0, sizeof(*pS));

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( pS->name, p.token, sizeof(pS->name)-1);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pS->color= atoi( p.token);
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		strncpy( pS->date, p.token, sizeof(pS->date)-1 );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		pS->stats.score = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		pS->human= atoi( p.token);
	} else goto error;

	return TEG_STATUS_SUCCESS;
error:
	textmsg(M_ERR,"error in aux_scores()");
	return TEG_STATUS_ERROR;
}


/**
 * @fn TEG_STATUS aux_paises( int numjug, char *str )
 * parsea los ejercitos en paises de un jugador
 */
TEG_STATUS aux_paises( int numjug, char *str )
{
	int i,pais,cant;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };

	if( strlen(str)==0 ) {
		return TEG_STATUS_SUCCESS;
	}

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	do {
		if((i = parser_call( &p ))) {
			pais = atoi(p.token);
			cant = atoi(p.value);
			if( g_paises[pais].numjug != numjug || g_paises[pais].ejercitos != cant ) {
				g_paises[pais].numjug = numjug;
				g_paises[pais].ejercitos = cant;
				gui_pais(g_paises[pais].id);
			}
		}
	} while(i && p.hay_otro );

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn void aux_draw_all_countries()
 * Dibuja todos los paises
 */
void aux_draw_all_countries()
{
	int i;
	for(i=0;i<PAISES_CANT;i++)
		gui_pais(i);
}

TEG_STATUS aux_jugador_init( PJUGADOR pJ, int numjug, char *name, int color )
{
	memset(pJ,0,sizeof(*pJ));

	pJ->numjug = numjug;
	strncpy( pJ->nombre, name, sizeof(pJ->nombre)-1);
	pJ->nombre[sizeof(pJ->nombre)-1]=0;
	pJ->color = color;
	pJ->estado = JUG_ESTADO_HABILITADO;
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS aux_start_error()
{
	textmsg(M_ERR,_("Error in start. Are there at least 2 players?"));
	return TEG_STATUS_SUCCESS;
}
