/*	$Id: ataque.h,v 1.8 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 * @file ataque.h
 * Contiene algunas funciones auxiliares para el manejo del estado 'ESTADO_ATAQUE'
 */

#ifndef __TEGC_ATAQUE_H
#define __TEGC_ATAQUE_H

TEG_STATUS ataque_init();
TEG_STATUS ataque_click( PPAIS p );
TEG_STATUS ataque_finish( int *ori, int *dst );
TEG_STATUS ataque_init();
void ataque_restore();
void ataque_backup();
void ataque_reset();
TEG_STATUS ataque_pre_reset();
TEG_STATUS ataque_out();
TEG_STATUS ataquere_out();
TEG_STATUS ataque_enter( PPAIS p );
TEG_STATUS ataque_leave( PPAIS p );
TEG_STATUS ataque_show( int src, int dst );
TEG_STATUS ataque_unshow();

#endif /* __TEGC_ATAQUE_H */
