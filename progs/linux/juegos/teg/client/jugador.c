/*	$Id: jugador.c,v 1.13 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file jugador.c
 * Para manejar funciones de los jugadores en el cliente
 */

#include <string.h>
#include "client.h"

LIST_ENTRY g_list_jugador;

/**
 * @fn TEG_STATUS jugador_whois( int numjug, PJUGADOR *j)
 * Dado un numjug devuelve el puntero al jugadore
 * @param numjug Numero de jugador
 * @param *j puntero a un puntero de jugador
 * @return TEG_STATUS_SUCCESS si se encontro ese jugador
 */
TEG_STATUS jugador_whois( int numjug, PJUGADOR *j)
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ = (PJUGADOR) l;
		if( pJ->numjug == numjug) {
			*j = pJ;
			return TEG_STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return TEG_STATUS_PLAYERNOTFOUND;
}

/**
 * @fn TEG_STATUS jugador_update( PJUGADOR j )
 * Hace un update del jugadore j de la lista
 */
TEG_STATUS jugador_update( PJUGADOR j )
{
	PJUGADOR i;

/*	textmsg(M_DBG,"jugdor_upt(%d):%s",j->numjug,j->nombre); */

	if( jugador_whois( j->numjug, &i ) != TEG_STATUS_SUCCESS )
		goto error; 

	memcpy(	&j->next, &i->next, sizeof(LIST_ENTRY));
	memcpy( i,j,sizeof(JUGADOR));
	return TEG_STATUS_SUCCESS;

error:
	return TEG_STATUS_PLAYERNOTFOUND;
}

/**
 * @fn PJUGADOR jugador_ins( PJUGADOR j)
 * Inserta el jugador j en la lista
 */
PJUGADOR jugador_ins( PJUGADOR j)
{
	PJUGADOR new = (PJUGADOR) malloc( sizeof(JUGADOR) );
	if( new==NULL)
		return NULL;

/*	textmsg(M_DBG,"jugdor_ins(%d):%s",j->numjug,j->nombre); */

	memmove( new, j, sizeof(JUGADOR));
	InitializeListHead( &new->next );
	InsertTailList( &g_list_jugador, (PLIST_ENTRY) new );

	g_juego.jugadores++;
	return new;
}

/**
 * @fn TEG_STATUS jugador_del( PJUGADOR pJ )
 * Borra un jugador de la lista de jugadores
 * @param pJ jugador a borrar
 */
TEG_STATUS jugador_del( PJUGADOR pJ )
{
	PLIST_ENTRY l = (PLIST_ENTRY) pJ;

	l = RemoveHeadList( l->Blink );
	free( l );
	g_juego.jugadores--;
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_flush()
 * Borra todos los jugadores de la lista
 */
TEG_STATUS jugador_flush()
{
	PLIST_ENTRY tmp;

	g_juego.jugadores=0;
	while( !IsListEmpty( &g_list_jugador ) ) {
		tmp = RemoveHeadList( &g_list_jugador );
		free( tmp );
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_init()
 * Inicializa la lista de jugadores
 */
TEG_STATUS jugador_init()
{
	InitializeListHead( &g_list_jugador );
	return TEG_STATUS_SUCCESS;
}
