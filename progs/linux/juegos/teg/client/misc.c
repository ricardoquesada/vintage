/*	$Id: misc.c,v 1.49 2001/12/16 01:29:46 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file misc.c
 */

#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include <glib.h>

#include "client.h"

#ifdef WITH_GGZ
#include "ggz_client.h"
#endif /* WITH_GGZ */

CJUEGO g_juego;			/**< client juego */

static void close_descriptors( void )
{
	int i, open_max;

	/* force manually the close of the socket */
	if( g_juego.fd >0 ) close (g_juego.fd);

	open_max = sysconf (_SC_OPEN_MAX);
	for (i = 3; i < open_max; i++)
		fcntl (i, F_SETFD, FD_CLOEXEC);
}

/**
 * @fn TEG_STATUS juego_init()
 * Inicializa un juego
 */
TEG_STATUS juego_reinit()
{
	int i;

	g_juego.jugadores = 0;
	g_juego.dados_srcpais=-1;
	g_juego.dados_dstpais=-1;
	for(i=0;i<3;i++) {
		g_juego.dados_src[i] = 0;
		g_juego.dados_dst[i] = 0;
	}

	InitializeListHead( &g_juego.tarjetas_list );
	g_juego.tarjetas_cant = 0;

	g_juego.objetivo = -1;
	g_juego.reglas = TEG_RULES_TEG;

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS juego_init()
 * Inicializa un juego
 */
TEG_STATUS juego_init()
{
	static int firsttime=1;
	g_juego.fd = -1;
	g_juego.numjug = -1;
	g_juego.observer = 0;

	/* variables seteadas por command line y que pueden cambiar estos defaults */
	if(firsttime) {
		dirs_create();
		g_juego.with_ggz = 0;
	}

	ESTADO_SET(JUG_ESTADO_DESCONECTADO);

	juego_reinit();

	/* name, server, serverport, color lo asiga la gui-xxx */

	firsttime=0;
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS juego_finalize()
 * Termina un juego preparandolo para otro
 */
TEG_STATUS juego_finalize()
{
	paises_init();
	juego_reinit();

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS conectar()
 * Se conectar un server
 */
TEG_STATUS conectar()
{
	if( ESTADO_ES(JUG_ESTADO_DESCONECTADO)) {

		/* standar mode */
		if( !g_juego.with_ggz ) {
			g_juego.fd = net_connect_tcp( (char*) &g_juego.sername, g_juego.serport );
			if( g_juego.fd < 0) {
				textmsg(M_ERR,_("Error while trying to connect to server '%s' at port %d"),g_juego.sername,g_juego.serport);
				return TEG_STATUS_ERROR;
			}

		} else {

#ifdef WITH_GGZ
			/* GGZ mode */
			g_juego.fd = ggz_client_connect();
			if( g_juego.fd < 0 ) {
				textmsg(M_ERR,_("Error while trying to connect to GGZ client"));
				return TEG_STATUS_ERROR;
			}
		}
#endif /* WITH_GGZ */

		if( !g_juego.with_ggz ) {
			out_pversion();
		}

		return TEG_STATUS_SUCCESS;
	} else {
		textmsg(M_ERR,_("Error, you are already connected"));
		return TEG_STATUS_ERROR;
	}
}

/**
 * @fn TEG_STATUS desconectar()
 * Se desconecta del server
 */
TEG_STATUS desconectar()
{
	ESTADO_SET(JUG_ESTADO_DESCONECTADO);
	gui_disconnect();

	jugador_flush();

	if( g_juego.fd > 0 ) {
		close( g_juego.fd );
		g_juego.fd = -1;
	}

	juego_finalize();
	juego_init();


	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS playerid_restore_from_error( void )
 */
TEG_STATUS playerid_restore_from_error( void )
{
	textmsg( M_ERR, _("The game has already started. Connect as an observer."));
	desconectar();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS launch_server( int port )
 * Launch a server in localost
 */
TEG_STATUS launch_server( int port )
{
	pid_t pid;
	char *args[5];

	if ( (pid = fork()) < 0) {
		perror("tegclient:");
		return TEG_STATUS_ERROR;
	} else if (pid == 0) {

		char buffer[100];
		close_descriptors();
		args[0] = "xterm";
		args[1] = "-e";
		args[2] = BINDIR"/tegserver";
		args[3] = "--port";
		snprintf(buffer, sizeof(buffer)-1, "%d", port); 
		buffer[ sizeof(buffer)-1 ] = 0;
		args[4] = buffer;
		args[5] = NULL;

		if( execvp(args[0], args) < 0) {
			perror(args[0]);
			/* last chance, launch tegserver without console */
			args[0] = BINDIR"/tegserver";
			args[1] = "--console";
			args[2] = "0";
			args[3] = "--port";
			snprintf(buffer, sizeof(buffer)-1, "%d", port); 
			buffer[ sizeof(buffer)-1 ] = 0;
			args[4] = buffer;
			args[5] = NULL;
			if( execv(args[0], args) < 0) {
				fprintf(stderr,"Launching server failed. Does the file '%s' exists ?\n",args[0]);
				perror("exe:");

				/* This saves a crash */
				args[0] = "/bin/true";
				args[1] = NULL;
				execv(args[0],args);
				exit(1);
			}
		}
		return TEG_STATUS_ERROR;
	} else {
		/* wait util server is launched */
		sleep(2);
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS launch_robot( void )
 * Launch robot in localhost
 */
TEG_STATUS launch_robot( void )
{
	pid_t pid;
	char *args[6];
	char port[50];

	if ( (pid = fork()) < 0) {
		perror("tegclient:");
		return TEG_STATUS_ERROR;
	} else if (pid == 0) {

		close_descriptors();

		sprintf(port,"%d",g_juego.serport);

		args[0] = BINDIR"/tegrobot";
		args[1] = "--server";
		args[2] = g_juego.sername;
		args[3] = "--port";
		args[4] = port;
		args[5] = "--quiet";
		args[6] = NULL;
	
		if( execv(args[0], args) < 0) {
			fprintf(stderr,"Launching robot failed. Does the file `%s' exists ?\n",args[0]);
			perror("exe:");

			/* This saves a crash */
			args[0] = "/bin/true";
			args[1] = NULL;
			execv(args[0],args);
			exit(1);
		}
		return TEG_STATUS_ERROR;
	} else {
		/* nothing */
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS textmsg( int level, char *format, ...)
{
        va_list args;
	char buf[PROT_MAX_LEN];

	va_start(args, format);
	vsnprintf(buf, sizeof(buf) -1, format, args);
	va_end(args);

	buf[ sizeof(buf) -1 ] = 0;

	if( g_juego.msg_show & level )
		gui_textmsg(buf);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS dirs_create()
 * Creates defaults dir for TEG
 */
TEG_STATUS dirs_create()
{
	DIR *dir;
	char buf[1000];

	memset(buf,0,sizeof(buf));

	snprintf(buf,sizeof(buf)-1,"%s/%s",g_get_home_dir(),TEG_DIRRC);

	if( (dir = opendir(buf)) == NULL )
		mkdir(buf,0755);
	else
		closedir(dir);

	snprintf(buf,sizeof(buf)-1,"%s/%s/themes",g_get_home_dir(),TEG_DIRRC);
	if( (dir = opendir(buf)) == NULL )
		mkdir(buf,0755);
	else
		closedir(dir);

	return TEG_STATUS_SUCCESS;
}
