/*	$Id: fichas.c,v 1.17 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file fichas.c
 * Contiene algunas funciones auxiliares para el manejo del estado 'ESTADO_FICHAS'
 */

#include <stdio.h>
#include <string.h>
#include "client.h"

static int	aFichas[PAISES_CANT];	/**< array de los paises */
static int	aConts[CONT_CANT];	/**< array de los continentes */
static int	fichas_tot;		/**< cantidad de fichas que hay en el array */
static int	wanted_tot;		/**< cantidad de fichas pedidas para enviar */
static int	wanted_conts;		/**< continentes pedidos */


/**
 * @fn static TEG_STATUS fichas_check()
 * Dice si se esta en condiciones de hacer algo con fichas
 */
TEG_STATUS fichas_check()
{
	JUG_ESTADO e;

	e = ESTADO_GET();

	if( e==JUG_ESTADO_FICHAS || e==JUG_ESTADO_FICHAS2 || e==JUG_ESTADO_FICHASC )
		return TEG_STATUS_SUCCESS;
	else
		return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS add_fichas( PPAIS p )
 * llamada desde la 'gui' y recuerda los paises que se fueron agregando
 * @params p pais que se esta agregando
 */
TEG_STATUS fichas_add( PPAIS p )
{
	if( fichas_check() != TEG_STATUS_SUCCESS ) {
		textmsg(M_ERR,_("Error, you cant add armies now"));
		return TEG_STATUS_UNEXPECTED;
	}

	if(p->numjug == WHOAMI() ) {
		if( fichas_tot < wanted_tot ) {
			fichas_tot++;
			p->ejercitos++;
			aFichas[p->id]++;
			aConts[p->continente]++;
			return TEG_STATUS_SUCCESS;
		} else {
			textmsg(M_ERR,_("Error, you cant put more than %d armies"),wanted_tot);
			fichas_enter( p );
			return TEG_STATUS_ERROR;
		}
	} else {
		textmsg(M_ERR,_("Error, '%s' isnt one of your countries"),p->nombre);
		return TEG_STATUS_UNEXPECTED;
	}
}

/**
 * @fn TEG_STATUS sub_fichas( PPAIS p )
 * llamada desde la 'gui' y recuerda los paises que se fueron agregando
 * @params p pais que se esta agregando
 */
TEG_STATUS fichas_sub( PPAIS p )
{
	if( fichas_check() != TEG_STATUS_SUCCESS ) {
		textmsg(M_ERR,_("Error, you cant sub armies now"));
		return TEG_STATUS_UNEXPECTED;
	}

	if(p->numjug == WHOAMI() ) {
		if( aFichas[p->id] ) {
			fichas_tot--;
			p->ejercitos--;
			aFichas[p->id]--;
			aConts[p->continente]--;
			fichas_enter( p );
			return TEG_STATUS_SUCCESS;
		} else return TEG_STATUS_UNEXPECTED;
	} else {
		textmsg(M_ERR,_("Error, '%s' isnt one of your countries"),p->nombre);
		return TEG_STATUS_UNEXPECTED;
	}
}

/**
 * @fn TEG_STATUS finish_fichas( int cant, int **ptr )
 * Funcion llamada cuando uno 'cree' que ya puso todas las fichas
 * @params cant Cantidad de fichas que tiene que haber
 * @params ptr Puntero al array
 * @return TEG_STATUS_SUCCESS si las fichas estan bien puestas
 */
TEG_STATUS fichas_finish( int **ptr )
{
	int i,c;
	if( fichas_check() != TEG_STATUS_SUCCESS )
		return TEG_STATUS_UNEXPECTED;

	if( wanted_tot != fichas_tot )
		return TEG_STATUS_ERROR;

	c = wanted_conts;
	for(i=0;i<CONT_CANT;i++) {
		if( (c & 1) && (aConts[i] < g_conts[i].fichas_x_cont) )
			return TEG_STATUS_ERROR;
		c >>= 1;
	}

	*ptr = aFichas;
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS init_fichas()
 * Llamada antes de empezar a poner fichas
 * @return TEG_STATUS_SUCCESS (siempre)
 */
TEG_STATUS fichas_init(int cant, int conts)
{
	int i;
	for(i=0;i<PAISES_CANT;i++)
		aFichas[i]=0;
	for(i=0;i<CONT_CANT;i++)
		aConts[i]=0;
	fichas_tot = 0;
	wanted_tot = cant;
	wanted_conts = conts;

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn void fichas_add_wanted( int i )
 * Si hace un canje, entonces se van a poder poner mas fichas
 */
void fichas_add_wanted( int i )
{
	wanted_tot += i ;
}

/**
 * @fn TEG_STATUS reset_fichas()
 * Si hay un error, se llama esta funcion que hace que empiece todo de nuevo
 * @return TEG_STATUS_SUCCESS (siempre)
 */
TEG_STATUS fichas_reset()
{
	int i;

	for( i=0;i<PAISES_CANT;i++) {
		if( aFichas[i] ) {
			g_paises[i].ejercitos -= aFichas[i];
			aFichas[i] = 0;
		}
	}
	for( i=0;i<CONT_CANT;i++)
		aConts[i] = 0;

	fichas_tot = 0;
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS fichas_restore_from_error()
{
	ESTADO_SET( JUG_ESTADO_FICHAS );
	fichas_reset();
	aux_draw_all_countries();
	gui_sensi();
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS fichas2_restore_from_error()
{
	ESTADO_SET( JUG_ESTADO_FICHAS2 );
	fichas_reset();
	aux_draw_all_countries();
	gui_sensi();
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS fichasc_restore_from_error()
{
	ESTADO_SET( JUG_ESTADO_FICHASC );
	fichas_reset();
	aux_draw_all_countries();
	gui_sensi();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS fichas_out()
 * Envia las fichas al server. Usada por FICHAS, FICHAS2 y FICHASC
 */
TEG_STATUS fichas_out()
{
	char buf[2000];
	char tmp[100];
	int *ptr;
	int i;
	int first_time;
	JUG_ESTADO e;

	if( fichas_check() != TEG_STATUS_SUCCESS ) {
		textmsg( M_ERR,_("Error, its not the time to send armies"));
		return TEG_STATUS_ERROR;
	}

	if( fichas_finish( &ptr ) != TEG_STATUS_SUCCESS )  {
		fichas_reset();
		textmsg( M_ERR,_("Error, put the correct number of armies"));
		return TEG_STATUS_ERROR;
	}

	buf[0]=0;
	tmp[0]=0;

	first_time = 1;

	for(i=0;i<PAISES_CANT;i++) {
		if( ptr[i]>0 ) {
			if( first_time )
				sprintf(tmp,"%d:%d",i,ptr[i]);
			else
				sprintf(tmp,",%d:%d",i,ptr[i]);
			strcat(buf,tmp);
			first_time = 0;
		}
	}

	e = ESTADO_GET();
	switch(e) {
	case JUG_ESTADO_FICHAS:
		net_printf(g_juego.fd,TOKEN_FICHAS"=%s\n",buf);
		ESTADO_SET( JUG_ESTADO_POSTFICHAS);
		break;
	case JUG_ESTADO_FICHAS2:
		net_printf(g_juego.fd,TOKEN_FICHAS2"=%s\n",buf);
		ESTADO_SET( JUG_ESTADO_POSTFICHAS2);
		break;
	case JUG_ESTADO_FICHASC:
		net_printf(g_juego.fd,TOKEN_FICHASC"=%s\n",buf);
		ESTADO_SET( JUG_ESTADO_POSTFICHASC);
		break;
	default:
		textmsg( M_ERR,_("Error, its not the moment to send your armies"));
		return TEG_STATUS_ERROR;
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS fichas_enter( PPAIS p )
{
	if( fichas_check() != TEG_STATUS_SUCCESS ) {
		return TEG_STATUS_ERROR;
	}

	if( p->numjug == WHOAMI() ) {
		if( fichas_tot >= wanted_tot )  {
			if(!( p->selected & PAIS_SELECT_FICHAS_OUT )) {
				p->selected &= ~PAIS_SELECT_FICHAS_IN;
				p->selected |= PAIS_SELECT_FICHAS_OUT;
				gui_pais_select( p->id );
			}
		} else {
			if(!( p->selected & PAIS_SELECT_FICHAS_IN )) {
				p->selected &= ~PAIS_SELECT_FICHAS_OUT;
				p->selected |= PAIS_SELECT_FICHAS_IN;
				gui_pais_select( p->id );
			}
		}
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS fichas_leave( PPAIS p )
{
	if( fichas_check() != TEG_STATUS_SUCCESS ) {
		return TEG_STATUS_ERROR;
	}

	if( p->numjug == WHOAMI() ) {
		p->selected &= ~PAIS_SELECT_FICHAS_IN;
		p->selected &= ~PAIS_SELECT_FICHAS_OUT;
		gui_pais_select( p->id );
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS fichas_get_wanted( int *cant, int *conts )
{
	*cant = wanted_tot - cont_tot(wanted_conts);
	*conts = wanted_conts;
	return TEG_STATUS_SUCCESS;
}
