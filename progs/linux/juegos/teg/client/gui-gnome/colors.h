/*	$Id: colors.h,v 1.7 2001/11/24 17:32:17 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file colors.h
 */

#ifndef __TEG_GUI_GNOME_COLORS_H
#define __TEG_GUI_GNOME_COLORS_H

enum {
	COLORS_WHITE,
	COLORS_BLACK,

	COLORS_LAST
};

enum {
	COLORS_P_RED,
	COLORS_P_YELLOW,
	COLORS_P_BLUE,
	COLORS_P_BLACK,
	COLORS_P_PINK,
	COLORS_P_GREEN,

	COLORS_P_LAST
};

struct _G_colores {
	char *ellip_color;
	char *text_color;
};
extern struct _G_colores G_colores[];
extern GdkImlibImage	*soldier[];
extern GdkGC *g_colors_gc;
extern GdkFont* g_pixmap_font10;

TEG_STATUS colors_allocate( void );
TEG_STATUS colors_free();
GdkColor* colors_get_player( int n );
GdkColor* colors_get_player_from_color( int color );
GdkColor* colors_get_player_ink(int n );
GdkColor* colors_get_player_ink_from_color(int color );
GdkColor* colors_get_player_virtual( int n );
GdkColor* colors_get_common( int n );

#endif /* __TEG_GUI_GNOME_COLORS_H */
