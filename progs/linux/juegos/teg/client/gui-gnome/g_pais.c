/*	$Id: g_pais.c,v 1.45 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file g_pais.c
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>

#include "gui.h"
#include "client.h"
#include "interface.h"
#include "g_pais.h"
#include "g_cont.h"
#include "support.h"
#include "armies.h"
#include "colors.h"
#include "themes.h"

#undef GENERATE_LIMITROFE
#undef DRAG_DROP_COUNTRIES

#define HELVETICA_20_BFONT "-adobe-helvetica-bold-r-normal-*-20-*-*-*-*-*-*-*"
#define HELVETICA_14_BFONT "-adobe-helvetica-bold-r-normal-*-14-*-*-*-*-*-*-*"
#define HELVETICA_12_BFONT "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"
#define HELVETICA_12_FONT  "-adobe-helvetica-medium-r-normal-*-12-*-*-*-*-*-*-*"
#define HELVETICA_10_FONT  "-adobe-helvetica-medium-r-normal-*-10-*-*-*-*-*-*-*"

struct _G_paises G_paises[PAISES_CANT];

static int initialized = 0;

static gint
item_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	static double x, y;
	double new_x, new_y;
#ifdef DRAG_DROP_COUNTRIES
	GdkCursor *fleur;
#endif /* DRAG_DROP_COUNTRIES */
	static int dragging;
	double item_x, item_y;
	PPAIS pais = (PPAIS) data;

#ifdef GENERATE_LIMITROFE
	static int borrame=0;
	static int buf[50];
	static FILE *fp=NULL;
#endif /* GENERATE_LIMITROFE */

	/* set item_[xy] to the event x,y position in the parent's item-relative coordinates */
	item_x = event->button.x;
	item_y = event->button.y;
	gnome_canvas_item_w2i (item->parent, &item_x, &item_y);

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		{
			char buff[250];
			snprintf(buff,sizeof(buff)-1,_("%s armies: %d"),_(pais->nombre),pais->ejercitos);

			gnome_appbar_push( GNOME_APPBAR(statusbar),  buff);

			switch( ESTADO_GET() ) {
			case JUG_ESTADO_FICHAS:
			case JUG_ESTADO_FICHAS2:
			case JUG_ESTADO_FICHASC:
				fichas_enter( pais );
				break;
			case JUG_ESTADO_ATAQUE:
			case JUG_ESTADO_TROPAS:
				ataque_enter( pais );
				break;
			case JUG_ESTADO_REAGRUPE:
				reagrupe_enter( pais );
				break;
			default:
				break;
			}
		}
		break;
	case GDK_LEAVE_NOTIFY:
		gnome_appbar_pop( GNOME_APPBAR(statusbar) );
		switch( ESTADO_GET() ) {
		case JUG_ESTADO_FICHAS:
		case JUG_ESTADO_FICHAS2:
		case JUG_ESTADO_FICHASC:
			fichas_leave( pais );
			break;
		case JUG_ESTADO_ATAQUE:
		case JUG_ESTADO_TROPAS:
			ataque_leave( pais );
			break;
		case JUG_ESTADO_REAGRUPE:
			reagrupe_leave( pais );
			break;
		default:
			break;
		}
		break;
	case GDK_BUTTON_PRESS:
		switch (event->button.button) {
		case 1:
			switch( ESTADO_GET() ) {
#ifdef GENERATE_LIMITROFE
			case JUG_ESTADO_DESCONECTADO:
				if( pais->id < borrame ) {
					textmsg(M_DBG,"no agregado :%s",pais->nombre);
					break;
				}
				buf[ pais->id ] = 1;
				textmsg(M_DBG,"agregado :%s",pais->nombre);
				break;
#endif /* GENERATE_LIMITROFE */
			case JUG_ESTADO_FICHAS:
			case JUG_ESTADO_FICHAS2:
			case JUG_ESTADO_FICHASC:
				if( fichas_add( pais ) == TEG_STATUS_SUCCESS ) {
					G_pais_draw_ejer(pais->id);
					armies_add( pais->id );
				}; 
				break;
			case JUG_ESTADO_ATAQUE:
			case JUG_ESTADO_TROPAS:
				ataque_click( pais );
				break;
			case JUG_ESTADO_REAGRUPE:
				reagrupe_click( pais );
				break;
			default:
				break;
			}

			/* XXX: Remove this block when the game is finished */
#ifdef DRAG_DROP_COUNTRIES
			{
				if (event->button.state & GDK_SHIFT_MASK)
					gtk_object_destroy (GTK_OBJECT (item));
				else {
					x = item_x;
					y = item_y;

					fleur = gdk_cursor_new (GDK_FLEUR);
					gnome_canvas_item_grab (item,
								GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
								fleur,
								event->button.time);
					gdk_cursor_destroy (fleur);
					dragging = TRUE;
				}
			}
#endif	/* DRAG_DROP_COUNTRIES */
			break;

		case 2:
			
#ifdef GENERATE_LIMITROFE
			{
				int i;
				for(i=0;i<50;i++)
					buf[i]=0;
				borrame=0;

				if( fp ) fclose( fp );
				fp = fopen( "test.h", "w+" );
				textmsg(M_DBG,"Procesando %s",g_paises[borrame].nombre);
				break;
			}
#endif /* GENERATE_LIMITROFE */
#ifdef DRAG_DROP_COUNTRIES 
			{
			int xx,yy;
			int cont = pais->continente;
			xx= G_conts[cont].x ;
			yy= G_conts[cont].y ;
			
			printf("%s :%f %f\n",pais->nombre,item->x1-xx,item->y1-yy);

			if (event->button.state & GDK_SHIFT_MASK)
				gnome_canvas_item_lower_to_bottom (item);
			else
				gnome_canvas_item_lower (item, 1);
			break;
			}
#endif /* DRAG_DROP_COUNTRIES */
			break;
		case 3:
			switch( ESTADO_GET() ) {
#ifdef GENERATE_LIMITROFE
			case JUG_ESTADO_DESCONECTADO:
				{
					int i;

					if( !fp ) break;
					buf[borrame] = 2;

					fprintf(fp,"{");
					for(i=0;i<50;i++) {
						fprintf( fp,"%d,",buf[i] );
					}
					fprintf(fp,"} /* %s (%d) */\n",g_paises[borrame].nombre,g_paises[borrame].id);

					borrame++;

					for(i=0;i<50;i++)
						buf[i]=0;

					if( borrame == 50 )
						fclose( fp );
					textmsg(M_DBG,"Procesando %s",g_paises[borrame].nombre);
					break;
				}
#endif /* GENERATE_LIMITROFE */
			case JUG_ESTADO_FICHAS:
			case JUG_ESTADO_FICHAS2:
			case JUG_ESTADO_FICHASC:
				if( fichas_sub( pais ) == TEG_STATUS_SUCCESS ) {
					G_pais_draw_ejer(pais->id);
					armies_del( pais->id );
				}
				break;
			default:
				break;
			}

#ifdef DRAG_DROP_COUNTRIES
			if (event->button.state & GDK_SHIFT_MASK)
				gnome_canvas_item_raise_to_top (item);
			else
				gnome_canvas_item_raise (item, 1);
			break;
#endif /* DRAG_DROP_COUNTRIES */

		default:
			break;
		}

		break;

	case GDK_MOTION_NOTIFY:
		if (dragging && (event->motion.state & GDK_BUTTON1_MASK)) {
			new_x = item_x;
			new_y = item_y;

			gnome_canvas_item_move (item, new_x - x, new_y - y);
			x = new_x;
			y = new_y;
		}
		break;

	case GDK_BUTTON_RELEASE:
		gnome_canvas_item_ungrab (item, event->button.time);
		dragging = FALSE;
		break;

	default:
		break;
	}

	return FALSE;
}

/**
 * @fn void G_pais_init()
 * Initialize the countrys from the theme
 */
void G_pais_init()
{
	int i;
	TCountry pais;

	if( initialized )
		return;

	for( i=0; i < PAISES_CANT ; i++ ) {

		/* find continent, find country number */
		int c = g_paises[i].continente;
		int sum,j,p;

		sum=0;
		for(j=0;j<c;j++)
			sum += g_conts[j].cant_paises;

		p = i - sum;

		if( theme_giveme_country(&pais,c,p) == TEG_STATUS_SUCCESS ) {
			G_paises[i].x = pais.pos_x;
			G_paises[i].y = pais.pos_y;
			G_paises[i].x_center = pais.army_x;
			G_paises[i].y_center = pais.army_y;
			G_paises[i].gfx_name = pais.file;
		}
	}

	initialized = 1;
}


/**
 * @fn void G_pais_create( int pais )
 * Crea el pais pais
 * @param pais numero de pais a crear
 */
void G_pais_create( int pais )
{
	if (!initialized)
		G_pais_init();

	G_paises[pais].pais_group = GNOME_CANVAS_GROUP(
			gnome_canvas_item_new (
				G_conts[g_paises[pais].continente].cont_group,
				gnome_canvas_group_get_type (),
				"x", (float) G_paises[pais].x,
				"y", (float) G_paises[pais].y,
//				"anchor", GTK_ANCHOR_NW,
				NULL));
	if( G_paises[pais].pais_group != NULL )
		gtk_signal_connect (GTK_OBJECT (G_paises[pais].pais_group), "event",
				    (GtkSignalFunc) item_event,
				    &g_paises[pais]);
}

/**
 * @fn void G_pais_draw( int pais )
 * Dibuja el pais pais
 * @param pais Pais a dibujar
 */
void G_pais_draw( int pais )
{
	GdkImlibImage *im;
	char name[200];
	GnomeCanvasItem *image;
	char *filename=NULL;

	snprintf( name,sizeof(name)-1,"%s",G_paises[pais].gfx_name);
	name[sizeof(name)-1]=0;

	filename = theme_load_file( name );

	im = gdk_imlib_load_image (filename);

	if( im == NULL ) {

		textmsg(M_ERR,_("Error, couldn't find file: %s"),G_paises[pais].gfx_name);
		g_warning ("Could not find country %s file",G_paises[pais].gfx_name);

	} else {

		G_paises[pais].x_len = im->rgb_width;
		G_paises[pais].y_len = im->rgb_height;

		image = gnome_canvas_item_new(
				G_paises[pais].pais_group,
				gnome_canvas_image_get_type (),
				"image", im,
				"x", 0.0,
				"y", 0.0,
				"width", (double) im->rgb_width,
				"height", (double) im->rgb_height,
				"anchor", GTK_ANCHOR_NW,
				NULL);
				
		gtk_signal_connect (GTK_OBJECT (image), "destroy",
				    (GtkSignalFunc) free_imlib_image,
				    im);

	};
}

/**
 * @fn void G_pais_draw_ejer(int pais) 
 * Dibuja los ejercitos de un pais (que tambien representa el color)
 * @param pais pais al cual hay que dibujarle los ejercitos
 */
void G_pais_draw_ejer(int pais) 
{
	PJUGADOR j;
	double size, size_tmp;
	int color;
	GnomeCanvasItem *e;
	int x,y;
	char size_string[10];

	color = 6;
	if( jugador_whois( g_paises[pais].numjug, &j) == TEG_STATUS_SUCCESS) {
		if( j->color >=0 && j->color <= 5)
			color = j->color;
	}

	switch( g_paises[pais].ejercitos) {
		case 0: size = 4; size_tmp = 0;break;
		case 1: size = 6; size_tmp = 0;break;
		case 2: size = 6; size_tmp = 1;break;
		case 3: size = 7; size_tmp = 0;break;
		case 4: size = 7; size_tmp = 1;break;
		case 5: size = 8; size_tmp = 0;break;
		case 6: size = 8; size_tmp = 1;break;
		case 7: size = 9; size_tmp = 0;break;
		case 8: size = 9; size_tmp = 1;break;
		case 9: size = 10; size_tmp = 0;break;
		case 10: size = 10; size_tmp = 1;break;
		default: size = 11; size_tmp = 0;break;
	}


	if( G_paises[pais].ellip_item != NULL)
		gtk_object_destroy(GTK_OBJECT ( G_paises[pais].ellip_item ) );

	if( G_paises[pais].text_item != NULL)
		gtk_object_destroy(GTK_OBJECT ( G_paises[pais].text_item ) );

	if( G_paises[pais].pais_group == NULL)
		g_warning("G_pais_draw_ejer()");

	/* crear elipse */
	x = G_paises[pais].x_len + G_paises[pais].x_center;
	y = G_paises[pais].y_len + G_paises[pais].y_center;

	e = gnome_canvas_item_new(
		G_paises[pais].pais_group,
		gnome_canvas_ellipse_get_type(),
		"x1", (double) x/2-size-size_tmp,
		"y1", (double) y/2-size,
		"x2", (double) x/2+size+size_tmp,
		"y2", (double) y/2+size,
		"fill_color", G_colores[color].ellip_color,
		"outline_color", "black",
		"width_units", (double) 1,
		NULL);

	/* 'e' puede ser NULL, pero si es NULL me interesa que ellip lo sea tambien */
	G_paises[pais].ellip_item = e;

	gui_pais_select( g_paises[pais].id );

	/* crear numero */
	if( g_paises[pais].ejercitos ) {
		sprintf(size_string,"%d", g_paises[pais].ejercitos);
		e = gnome_canvas_item_new(
			G_paises[pais].pais_group,
			gnome_canvas_text_get_type(),
			"text", size_string,
			"x", (double) x/2,
			"y", (double) y/2,
			"x_offset", (double) -1,
			"y_offset", (double) -1,
			"font", HELVETICA_10_FONT,
			"fill_color", G_colores[color].text_color,
			"anchor",GTK_ANCHOR_CENTER,
			NULL);

	} else {
		e  = NULL;
	}
	G_paises[pais].text_item = e;
}

/*
 * Funciones GUI
 */
/**
 * @fn TEG_STATUS gui_pais_select(int pais) 
 * selecciona un pais
 * @param pais pais a seleccionar
 */
TEG_STATUS gui_pais_select( int pais ) 
{
	if( pais < 0 || pais >= PAISES_CANT )
		return TEG_STATUS_ERROR;

	if( g_paises[pais].selected == PAIS_SELECT_NONE ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "black",
			"width_units", (double) 1,
			NULL);
	}
	if( g_paises[pais].selected & PAIS_SELECT_FICHAS_IN ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "white",
			"width_units", (double) 2,
			NULL);
	}
	if( g_paises[pais].selected & PAIS_SELECT_FICHAS_OUT ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "DarkOrange1",
			"width_units", (double) 2,
			NULL);
	}
	if( g_paises[pais].selected & PAIS_SELECT_ATTACK_ENTER ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "white",
			"width_units", (double) 2,
			NULL);
	}
#if 0
	if( g_paises[pais].selected & PAIS_SELECT_ATTACK ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "red",
			"width_units", (double) 3,
			NULL);
	}
#endif
	if( g_paises[pais].selected & PAIS_SELECT_REGROUP_ENTER ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "white",
			"width_units", (double) 2,
			NULL);
	}
#if 0
	if( g_paises[pais].selected & PAIS_SELECT_REGROUP ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "blue",
			"width_units", (double) 2,
			NULL);
	}
#endif

	if( g_paises[pais].selected & PAIS_SELECT_ATTACK_SRC ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "white",
			"width_units", (double) 2,
			NULL);
	}

	if( g_paises[pais].selected & PAIS_SELECT_ATTACK_DST ) {
		gnome_canvas_item_set(
			G_paises[pais].ellip_item, 
			"outline_color", "white",
			"width_units", (double) 2,
			NULL);
	}


	gnome_canvas_update_now( GNOME_CANVAS(canvas_map) );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS g_pais_init()
 * Initialize the countries
 */
TEG_STATUS g_pais_init()
{
	return TEG_STATUS_SUCCESS;
}
