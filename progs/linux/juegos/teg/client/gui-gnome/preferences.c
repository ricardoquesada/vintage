/*	$Id: preferences.c,v 1.14 2001/12/22 17:12:05 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2001 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <riq@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file preferences.c
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include "client.h"
#include "gui.h"
#include "priv.h"
#include "interface.h"
#include "support.h"
#include "status.h"

extern TTheme gui_theme;

static GtkWidget	*pref_dialog=NULL;

/** messages */
static GtkWidget	*conf_cb_showerr=NULL;
static GtkWidget	*conf_cb_showimp=NULL;
static GtkWidget	*conf_cb_showmsg=NULL;
static GtkWidget	*conf_cb_showmsgcolor=NULL;
static GtkWidget	*conf_cb_showinf=NULL;

/** themes */
static char		*theme_activated=NULL;
static GdkPixmap	*theme_pixmap=NULL;
static GdkBitmap	*theme_bitmap=NULL;
static GtkWidget	*theme_widget=NULL;
static GtkWidget	*theme_frame_prev=NULL;

/** status */
static GtkWidget	*conf_cb_stsnumber=NULL;
static GtkWidget	*conf_cb_stsscore=NULL;
static GtkWidget	*conf_cb_stsaddress=NULL;
static GtkWidget	*conf_cb_stshuman=NULL;


static void free_pixmap( GtkWidget *widget, void *data )
{
	if( theme_widget )
		gtk_widget_destroy( theme_widget );
	theme_widget = NULL;
}

static void
load_preview( char *theme )
{
	char *filename;

	if( theme_widget ) {
		gtk_widget_destroy( theme_widget );
		theme_widget = NULL;
	}

	filename = theme_load_fake_file("mini_shot.png",theme);
	if( gdk_imlib_load_file_to_pixmap ( filename, &theme_pixmap, &theme_bitmap ) ) {
		theme_widget = gtk_pixmap_new(theme_pixmap,theme_bitmap);
		if( theme_widget ) {
			gtk_container_add( GTK_CONTAINER(theme_frame_prev), theme_widget );
			gtk_widget_show(theme_widget);
			gtk_signal_connect (GTK_OBJECT(theme_widget), "destroy",
			    	GTK_SIGNAL_FUNC (free_pixmap), NULL);
		}
	} else
		g_warning (_("Could not find the %s file"),"mini_shot.png");
}

static void prop_box_changed_callback (GtkWidget *widget, gpointer data)
{
	if(pref_dialog==NULL)
		return;
	gnome_property_box_changed (GNOME_PROPERTY_BOX (pref_dialog));
}

static void theme_activated_callback (GtkWidget *widget, gpointer data)
{
	theme_activated = data;
	load_preview(data);
	prop_box_changed_callback (widget,data);
}

static void
free_str (GtkWidget *widget, void *data)
{
	free (data);
}



static void apply_cb (GtkWidget *widget, gint pagenum, gpointer data)
{
	if (pagenum != -1)
		return;
 
	/** messages **/
	if (GTK_TOGGLE_BUTTON( conf_cb_showerr )->active)
		g_juego.msg_show |= M_ERR;
	else
		g_juego.msg_show &= ~M_ERR;


	if (GTK_TOGGLE_BUTTON( conf_cb_showimp )->active)
		g_juego.msg_show |= M_IMP;
	else
		g_juego.msg_show &= ~M_IMP;


	if (GTK_TOGGLE_BUTTON( conf_cb_showmsg )->active)
		g_juego.msg_show |= M_MSG;
	else
		g_juego.msg_show &= ~M_MSG;


	if (GTK_TOGGLE_BUTTON( conf_cb_showmsgcolor )->active)
		gui_private.msg_show_colors = 1;
	else
		gui_private.msg_show_colors = 0;


	if (GTK_TOGGLE_BUTTON( conf_cb_showinf )->active)
		g_juego.msg_show |= M_INF;
	else
		g_juego.msg_show &= ~M_INF;

	/** status **/
	if (GTK_TOGGLE_BUTTON( conf_cb_stsnumber)->active)
		gui_private.status_show |= (1 << S_CLIST_N_NUMBER);
	else
		gui_private.status_show &= ~(1 << S_CLIST_N_NUMBER);

	if (GTK_TOGGLE_BUTTON( conf_cb_stsscore)->active)
		gui_private.status_show |= (1 << S_CLIST_N_SCORE);
	else
		gui_private.status_show &= ~(1 << S_CLIST_N_SCORE);

	if (GTK_TOGGLE_BUTTON( conf_cb_stsaddress )->active)
		gui_private.status_show |= (1 << S_CLIST_N_ADDR);
	else
		gui_private.status_show &= ~(1 << S_CLIST_N_ADDR);

	if (GTK_TOGGLE_BUTTON( conf_cb_stshuman)->active)
		gui_private.status_show |= (1 << S_CLIST_N_HUMAN);
	else
		gui_private.status_show &= ~(1 << S_CLIST_N_HUMAN);


//	gnome_config_set_int("/TEG/data/color",g_juego.mycolor);
	gnome_config_set_int("/TEG/data/msgshow",g_juego.msg_show);
	gnome_config_set_int("/TEG/data/msgshow_with_color",gui_private.msg_show_colors);
	gnome_config_set_int("/TEG/data/status_show",gui_private.status_show);

	if( theme_activated ){
		gnome_config_set_string("/TEG/data/theme",theme_activated);
		if( strcmp(theme_activated,g_juego.theme) )
			gnome_ok_dialog_parented(_("You have to restart TEG to use the new theme."),GTK_WINDOW(main_window));
	}
	gnome_config_sync();
}



static void
fill_menu (GtkWidget *menu)
{
	TInfo Info;
	pTInfo pI;
	char *s;
	GtkWidget *item;
	int i=0;

	if( theme_enum_themes(&Info) != TEG_STATUS_SUCCESS ) {
		textmsg(M_ERR,_("Error while loading information about themes!"));
		return;
	}

	for( pI=&Info; pI != NULL; pI = pI->next ) {
		s = strdup (pI->name);
		item = gtk_menu_item_new_with_label (s);
		gtk_widget_show (item);
		gtk_menu_append (GTK_MENU(menu), item);
		gtk_signal_connect (GTK_OBJECT(item), "activate",
			    GTK_SIGNAL_FUNC (theme_activated_callback),s);

		gtk_signal_connect (GTK_OBJECT(item), "destroy",
			    GTK_SIGNAL_FUNC (free_str), s);

		if( !strcmp(pI->name, g_juego.theme ))
			gtk_menu_set_active(GTK_MENU(menu), i);
		i++;
	}
}



/**
 * @fn void preferences_activate(void)
 */
void preferences_activate(void)
{
	GtkWidget *label;
	GtkWidget *msg_frame;
	GtkWidget *sts_frame;
	GtkWidget *theme_frame_sel, *theme_vbox;
	GtkWidget *vbox, *hbox;
	GtkWidget *menu, *omenu;

	if( pref_dialog != NULL ) {
		gdk_window_show( pref_dialog->window);
		gdk_window_raise( pref_dialog->window);
		return ;
	}


	/* Theme options */
	theme_vbox = gtk_vbox_new (FALSE, 0);
	
	theme_frame_sel = gtk_frame_new (_("Select"));
	gtk_container_border_width (GTK_CONTAINER (theme_frame_sel), GNOME_PAD);
	omenu = gtk_option_menu_new ();
	menu = gtk_menu_new ();
	fill_menu (menu);
	gtk_widget_show (omenu);
	gtk_option_menu_set_menu (GTK_OPTION_MENU(omenu), menu);
	label = gtk_label_new (_("Select theme:"));
	gtk_widget_show (label);

	hbox = gtk_hbox_new (TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (hbox), GNOME_PAD);

	gtk_box_pack_start_defaults( GTK_BOX( hbox ), label);
	gtk_box_pack_start_defaults( GTK_BOX( hbox ), omenu);
	gtk_container_add (GTK_CONTAINER (theme_frame_sel), hbox);
	gtk_widget_show(theme_frame_sel);

	theme_frame_prev = gtk_frame_new (_("Preview"));
	gtk_container_border_width (GTK_CONTAINER (theme_frame_prev), GNOME_PAD);
	gtk_widget_show(theme_frame_prev);

	load_preview(g_juego.theme);

	gtk_box_pack_start( GTK_BOX( theme_vbox ), theme_frame_sel, TRUE, FALSE, 0);
	gtk_box_pack_start( GTK_BOX( theme_vbox ), theme_frame_prev, TRUE, FALSE, 0);

	/* Message Options */
	msg_frame = gtk_frame_new (_("Message Options"));
	gtk_container_border_width (GTK_CONTAINER (msg_frame), GNOME_PAD);

	vbox = gtk_vbox_new (TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (vbox), GNOME_PAD);


	conf_cb_showerr = gtk_check_button_new_with_label(_("Show Error Messages"));
	GTK_TOGGLE_BUTTON(conf_cb_showerr)->active = (g_juego.msg_show & M_ERR) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showerr);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showerr), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showimp = gtk_check_button_new_with_label(_("Show Important Messages"));
	GTK_TOGGLE_BUTTON(conf_cb_showimp)->active = (g_juego.msg_show & M_IMP) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showimp);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showimp), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showmsg = gtk_check_button_new_with_label(_("Show Players Messages"));
	GTK_TOGGLE_BUTTON(conf_cb_showmsg)->active = (g_juego.msg_show & M_MSG) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showmsg);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showmsg), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showmsgcolor = gtk_check_button_new_with_label(_("Show Players Messages with colors"));
	GTK_TOGGLE_BUTTON(conf_cb_showmsgcolor)->active = (gui_private.msg_show_colors & 1) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showmsgcolor);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showmsgcolor), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showinf = gtk_check_button_new_with_label(_("Show Informative Messages"));
	GTK_TOGGLE_BUTTON(conf_cb_showinf)->active = (g_juego.msg_show & M_INF) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showinf);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showinf), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	gtk_container_add (GTK_CONTAINER (msg_frame), vbox);


	/** Status options **/

	sts_frame = gtk_frame_new (_("Status Options"));
	gtk_container_border_width (GTK_CONTAINER (sts_frame), GNOME_PAD);

	vbox = gtk_vbox_new (TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (vbox), GNOME_PAD);

#if 0
	conf_cb_showerr = gtk_check_button_new_with_label(_("Show Color"));
	GTK_TOGGLE_BUTTON(conf_cb_showerr)->active = (gui_private.status_show & M_ERR) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showerr);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showerr), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);
#endif


	conf_cb_stsnumber = gtk_check_button_new_with_label(_("Show Number"));
	GTK_TOGGLE_BUTTON(conf_cb_stsnumber)->active = (gui_private.status_show & (1 << S_CLIST_N_NUMBER) ) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_stsnumber);
	gtk_signal_connect (GTK_OBJECT (conf_cb_stsnumber), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

#if 0
	conf_cb_showmsg = gtk_check_button_new_with_label(_("Show Name"));
	GTK_TOGGLE_BUTTON(conf_cb_showmsg)->active = (gui_private.status_show & M_MSG) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showmsg);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showmsg), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);
#endif

	conf_cb_stsscore= gtk_check_button_new_with_label(_("Show Score"));
	GTK_TOGGLE_BUTTON(conf_cb_stsscore)->active = (gui_private.status_show & (1 << S_CLIST_N_SCORE) ) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_stsscore);
	gtk_signal_connect (GTK_OBJECT (conf_cb_stsscore), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_stsaddress = gtk_check_button_new_with_label(_("Show IP Address"));
	GTK_TOGGLE_BUTTON(conf_cb_stsaddress)->active = (gui_private.status_show & (1 << S_CLIST_N_ADDR) ) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_stsaddress);
	gtk_signal_connect (GTK_OBJECT (conf_cb_stsaddress), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_stshuman= gtk_check_button_new_with_label(_("Show Human/Robot"));
	GTK_TOGGLE_BUTTON(conf_cb_stshuman)->active = (gui_private.status_show & (1 << S_CLIST_N_HUMAN) ) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_stshuman);
	gtk_signal_connect (GTK_OBJECT (conf_cb_stshuman), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);
#if 0
	conf_cb_showinf = gtk_check_button_new_with_label(_("Show Countries"));
	GTK_TOGGLE_BUTTON(conf_cb_showinf)->active = (gui_private.status_show & M_INF) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showinf);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showinf), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showinf = gtk_check_button_new_with_label(_("Show Armies"));
	GTK_TOGGLE_BUTTON(conf_cb_showinf)->active = (gui_private.status_show & M_INF) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showinf);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showinf), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showinf = gtk_check_button_new_with_label(_("Show Cards"));
	GTK_TOGGLE_BUTTON(conf_cb_showinf)->active = (gui_private.status_show & M_INF) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showinf);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showinf), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showinf = gtk_check_button_new_with_label(_("Show Status"));
	GTK_TOGGLE_BUTTON(conf_cb_showinf)->active = (gui_private.status_show & M_INF) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showinf);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showinf), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);

	conf_cb_showinf = gtk_check_button_new_with_label(_("Show Who Started"));
	GTK_TOGGLE_BUTTON(conf_cb_showinf)->active = (gui_private.status_show & M_INF) ?1:0;
	gtk_box_pack_start_defaults( GTK_BOX( vbox ), conf_cb_showinf);
	gtk_signal_connect (GTK_OBJECT (conf_cb_showinf), "clicked", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);
#endif

	gtk_container_add (GTK_CONTAINER (sts_frame), vbox);


	/** end **/

	pref_dialog = gnome_property_box_new ();
	gnome_dialog_set_parent (GNOME_DIALOG (pref_dialog),
			GTK_WINDOW (main_window));
	gtk_window_set_title (GTK_WINDOW (pref_dialog),
			_("TEG Preferences"));
	gtk_signal_connect (GTK_OBJECT (pref_dialog), "destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed), &pref_dialog);


	label = gtk_label_new (_("Themes"));
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (pref_dialog),
			theme_vbox, label);

	label = gtk_label_new (_("Message Options"));
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (pref_dialog),
			msg_frame, label);

	label = gtk_label_new (_("Player's Status Options"));
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (pref_dialog),
			sts_frame, label);


	gtk_signal_connect (GTK_OBJECT (pref_dialog), "apply", GTK_SIGNAL_FUNC
			(apply_cb), NULL);

	if (!GTK_WIDGET_VISIBLE (pref_dialog))
		gtk_widget_show_all (pref_dialog);
	else
		gtk_widget_destroy (pref_dialog);
}
