/*	$Id: status.c,v 1.6 2000/10/24 03:22:58 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file status.c
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#include <gnome.h>

#include "gui.h"
#include "client.h"
#include "support.h"
#include "interface.h"
#include "callbacks.h"
#include "status.h"

#define STATUS_CLIST_COLUMNS 6

#define HELVETICA_10_FONT  "-adobe-helvetica-medium-r-normal-*-10-*-*-*-*-*-*-*"

GtkWidget *status_window=NULL;
static GtkWidget *ministatus = NULL;
static GtkWidget *status_clist=NULL;
static GdkGC *gc = NULL;
static GdkFont* pixmap_font = NULL;

void status_destroy( GtkWidget * widget, GtkWidget **window )
{
	if( gc )
		gdk_gc_unref( gc );

	if (pixmap_font)
		gdk_font_unref (pixmap_font);

	if( *window != NULL)
		gtk_widget_destroy(*window);

	*window = NULL;
	gc = NULL;
	pixmap_font = NULL;
}

static gint status_boton_clicked_cb(GtkWidget *area, GdkEventExpose *event, gpointer user_data)
{
	out_status();
	return FALSE;
}

/**
 * @fn TEG_STATUS status_paint_color( PJUGADOR pJ, GdkPixmap **pixmap )
 */
TEG_STATUS status_paint_color( PJUGADOR pJ, GdkPixmap **pixmap )
{
	GdkColor color_bg;
	GdkColor color_text;
	GdkColor black;
	GdkColormap *colormap;
	int i, h, w;

	assert( pixmap );
	assert( pJ );

	i = (pJ->color==-1) ? TEG_MAX_PLAYERS : pJ->color;

    	colormap = gtk_widget_get_colormap(status_clist);

	gdk_color_parse( G_colores[i].ellip_color, &color_bg);
	gdk_color_alloc( colormap, &color_bg);

	gdk_color_parse( "black", &black);
	gdk_color_alloc( colormap, &black);

	gdk_color_parse(G_colores[i].text_color, &color_text);
	gdk_color_alloc(colormap, &color_text);
	
	*pixmap = gdk_pixmap_new(status_window->window,
		48, 16, gtk_widget_get_visual(status_window)->depth);

	if( *pixmap == NULL )
		return TEG_STATUS_ERROR;

	if( gc == NULL )
		gc = gdk_gc_new(main_window->window);

	gdk_gc_set_foreground(gc, &color_bg);
	gdk_draw_rectangle( *pixmap, gc, TRUE, 0, 0, 47, 15);

	gdk_gc_set_foreground(gc, &black);
	gdk_draw_rectangle( *pixmap, gc, FALSE, 0, 0, 47, 15);

	gdk_gc_set_foreground(gc, &color_text);

	if( pixmap_font == NULL )
		pixmap_font = gdk_font_load (HELVETICA_10_FONT);
//		pixmap_font = gdk_font_load ("-bitstream-charter-medium-r-normal-*-10-*-*-*-*-*-*-*");


	h = gdk_string_height (pixmap_font, g_colores[i] );  
	w = gdk_string_width  (pixmap_font, g_colores[i] );

	gdk_draw_string( *pixmap, pixmap_font, gc, 
			((48 - w )/2),
			((16 - h)/2) + h, g_colores[i]);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn void status_view()
 * Funcion que sirve para ver el status
 */
void status_view()
{
	GtkWidget *vbox;
	GtkWidget *boton;
	gint i ;
	static char *titles[] = {
		N_("Color"), N_("Number"),N_("Name"), N_("Countries"), N_("Armies"), N_("Status")
	};

	if( status_window == NULL) {

		status_window = gtk_widget_new (gtk_window_get_type (),
			"GtkWindow::title", _("Players status window"),
			NULL);

		generic_window_set_parent (status_window, GTK_WINDOW(main_window));

		gtk_signal_connect( GTK_OBJECT(status_window),
				"delete_event",
				GTK_SIGNAL_FUNC(status_destroy),
				&status_window);

		gtk_signal_connect( GTK_OBJECT(status_window),
				"destroy",
				GTK_SIGNAL_FUNC(status_destroy),
				&status_window);

		vbox = gtk_vbox_new (FALSE, 0);
		gtk_container_add(GTK_CONTAINER(status_window), vbox);

		status_clist = gtk_clist_new_with_titles (STATUS_CLIST_COLUMNS, titles);
		
		gtk_clist_set_row_height (GTK_CLIST (status_clist), 18);
//		gtk_widget_set_usize (status_clist, -1, 300);

		gtk_box_pack_start_defaults( GTK_BOX(vbox), status_clist );

		gtk_clist_set_selection_mode (GTK_CLIST (status_clist), GTK_SELECTION_EXTENDED);
		for (i = 0; i < STATUS_CLIST_COLUMNS; i++) {
			gtk_clist_set_column_justification (GTK_CLIST (status_clist), i, GTK_JUSTIFY_CENTER);
			gtk_clist_set_column_auto_resize (GTK_CLIST (status_clist), i, TRUE);
		}

		boton = gnome_stock_button(GNOME_STOCK_PIXMAP_REFRESH);
		gtk_signal_connect(GTK_OBJECT(boton), "clicked",
			   GTK_SIGNAL_FUNC(status_boton_clicked_cb), NULL);
		gtk_widget_show( boton );

		gtk_box_pack_start_defaults( GTK_BOX(vbox), boton );
	}
	status_fill_clist( status_clist);
	gtk_widget_show_all(status_window);
	raise_and_focus(status_window);
	out_status();
}

/**
 * @fn TEG_STATUS status_fill_clist( GtkWidget *clist )
 */
TEG_STATUS status_fill_clist( GtkWidget *clist )
{
	PJUGADOR j;
	PLIST_ENTRY l = g_list_jugador.Flink;
	char clist_text[STATUS_CLIST_COLUMNS][50];
	char *clist_texts[STATUS_CLIST_COLUMNS];
	int i, row;
	GdkPixmap *pixmap;
	

	gtk_clist_clear ((GtkCList *)clist);

	for(i=0;i<STATUS_CLIST_COLUMNS;i++)
		clist_texts[i] = clist_text[i];

	row = 0;
	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		j = (PJUGADOR) l;

		/* sprintf(clist_texts[0],"%s",g_colores[j->color]); */
		sprintf(clist_texts[1],"%d",j->numjug);
		sprintf(clist_texts[2],"%s",j->nombre);
		sprintf(clist_texts[3],"%d",j->tot_paises);
		sprintf(clist_texts[4],"%d",j->tot_ejercitos);
		sprintf(clist_texts[5],"%s",g_estados[j->estado]);
		gtk_clist_append (GTK_CLIST (clist), clist_texts);

		if( status_paint_color( j, &pixmap ) == TEG_STATUS_SUCCESS )
			gtk_clist_set_pixmap(GTK_CLIST(clist), row, 0, pixmap, NULL);


		l = LIST_NEXT(l);
		row ++;
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS status_update()
 */
TEG_STATUS status_update()
{
	if( status_window != NULL)
		status_fill_clist( status_clist );

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS ministatus_update()
{
	if( ministatus == NULL )
		return TEG_STATUS_ERROR;

	gtk_widget_draw( ministatus, NULL);

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS_SUCCESS ministatus_update()
 */
static gint ministatus_expose_cb(GtkWidget *area, GdkEventExpose *event, gpointer user_data)
{
	static GdkGC *ms_gc = NULL;
	GdkColor black;
	GdkColor color;
	GdkColormap *colormap;

	if( area == NULL )
		return FALSE;

	if( area->window == NULL )
		return FALSE;;

	if( ms_gc == NULL )
		ms_gc = gdk_gc_new(area->window);

	colormap = gtk_widget_get_colormap( area );
	
	gdk_color_parse( "black", &black);
	gdk_color_alloc( colormap, &black);

	if( ESTADO_GET() == JUG_ESTADO_DESCONECTADO ) {
		gdk_color_parse( "grey", &color);
	} else {
		gdk_color_parse( G_colores[g_juego.mycolor].ellip_color, &color);
	}

	gdk_color_alloc( colormap, &color);
	gdk_gc_set_foreground(ms_gc, &color);
	gdk_draw_arc( area->window, ms_gc, TRUE, 0, 3, 10, 10, 0, 360 * 64);

	gdk_gc_set_foreground(ms_gc, &black);
	gdk_draw_arc( area->window, ms_gc, FALSE, 0, 3, 10, 10, 0, 360* 64);

	return FALSE;
}

/**
 * @fn GtkWidget *ministatus_build()
 */
GtkWidget *ministatus_build()
{
	if( ministatus == NULL ) {
		ministatus = gtk_drawing_area_new();

		gtk_signal_connect(GTK_OBJECT(ministatus), "expose_event",
			   GTK_SIGNAL_FUNC(ministatus_expose_cb), NULL);
	}
	gtk_widget_set_usize(ministatus, 15, -1);
	gtk_widget_show( ministatus );

	return ministatus;
}

