/*	$Id: dices.c,v 1.19 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file dices.c
 * Muestra los dados
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "gui.h"
#include "client.h"

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "priv.h"
#include "dices.h"
#include "themes.h"

#define HELVETICA_12_BFONT "-adobe-helvetica-bold-r-normal-*-12-*-*-*-*-*-*-*"

extern TTheme gui_theme;


static GdkImlibImage *dices[DICES_CANT] = { NULL, NULL, NULL, NULL, NULL, NULL };
static GnomeCanvasItem *images[DICES_CANT] = { NULL, NULL, NULL, NULL, NULL, NULL };
static GnomeCanvasItem *text[2] = {NULL,NULL};
static GnomeCanvasGroup* dices_group=NULL;
static int dices_initialized=0;


TEG_STATUS dices_load_from_file( char *name, int i )
{
	char *filename = NULL;

	filename = theme_load_file( name );

	dices[i] = gdk_imlib_load_image( filename );

	if( dices[i] == NULL ) {
		g_warning( _("Error, couldn't find file: %s"), name);
		return TEG_STATUS_ERROR;
	}

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS dices_load()
{
	int i;

	if( dices_initialized )
		return TEG_STATUS_SUCCESS;

	for(i=0;i<DICES_CANT;i++) {
#ifdef XML_WORKS_OK
		if(!dices[i])
			dices_load_from_file (gui_theme.dices_file[i],i);
#else
		char name[40];
		memset(name,0,sizeof(name));
		snprintf(name,sizeof(name)-1,"dice-%d.png",i+1);
		if(!dices[i])
			dices_load_from_file (name,i);
#endif /* !XML_WORKS_OK */
	}

	return TEG_STATUS_SUCCESS;
}

void dices_show_text( int pais, double y )
{
	static int i=0;

	if( text[i] )
		gtk_object_destroy( GTK_OBJECT( text[i] ));

	text[i] = gnome_canvas_item_new(
		dices_group,
		gnome_canvas_text_get_type(),
		"text", _(g_paises[pais].nombre),
		"x", (double) (dices[0]->rgb_width*3)/2,
		"y", (double) y,
		"x_offset", (double) -1,
		"y_offset", (double) -1,
		"font", HELVETICA_12_BFONT,
		"fill_color", gui_theme.dices_color,
		"anchor",GTK_ANCHOR_NORTH,
		NULL);
	i = (i+1) % 2;
}

void dices_show_image( int dado, double x, double y )
{
	static int i=0;

	if( images[i] == NULL ) {
		images[i] = gnome_canvas_item_new(
			dices_group,
			gnome_canvas_image_get_type (),
			"image", dices[dado],
			"x", x,
			"y", y,
			"width", (double) dices[dado]->rgb_width,
			"height", (double) dices[dado]->rgb_height,
			"anchor", GTK_ANCHOR_NW,
			NULL);

	} else {
		gnome_canvas_item_set( images[i],
			"image", dices[dado],
			"x", x,
			"y", y,
			"width", (double) dices[dado]->rgb_width,
			"height", (double) dices[dado]->rgb_height,
			"anchor", GTK_ANCHOR_NW,
			NULL);
		gnome_canvas_item_show( images[i] );
	}

	i = (i+1)% DICES_CANT;
}

void free_dices (GtkObject *object, gpointer *data)
{
	int i;

	for(i=0;i<DICES_CANT;i++) {
		if( dices[i] ) gdk_imlib_destroy_image(dices[i]);
		dices[i] = NULL;
	}
}

void dices_init( GnomeCanvasGroup *root)
{
	if( dices_initialized )
		return;

	if( !root )
		return;

	dices_group = GNOME_CANVAS_GROUP(
		gnome_canvas_item_new(
			root,
			gnome_canvas_group_get_type (),
			"x",(float) gui_theme.dices_x,
			"y",(float) gui_theme.dices_y,
			NULL)
		);

	if( !dices_group )
		return;

	gtk_signal_connect (GTK_OBJECT (dices_group), "destroy",
		(GtkSignalFunc) free_dices,
		NULL);

	dices_load();

	dices_initialized=1;
}

void dices_unview()
{
	int i;

	for(i=0;i<DICES_CANT;i++) {
		if( images[i] ) gnome_canvas_item_hide( images[i] );
	}

	for(i=0;i<2;i++) {
		if( text[i] ) gtk_object_destroy( GTK_OBJECT( text[i] ));
	}
}

void dices_view()
{
	int i;
	double  x,y;

	if( !dices_initialized )
		return;

	if( g_juego.dados_srcpais == -1 || g_juego.dados_dstpais == -1 )
		return;


	/* hide all dices */
	for(i=0;i<DICES_CANT;i++) {
		if( images[i] ) gnome_canvas_item_hide( images[i] );
	}

	/* show src */
	x=0, y=4;
	for(i=0;i<3;i++) {
		if( g_juego.dados_src[i] == 0) {
			break;
		}
		dices_show_image( g_juego.dados_src[i]-1, x,y );
		x += dices[0]->rgb_width;
	}
	dices_show_text( g_juego.dados_srcpais, y-10 );
	
	/* show dst */
	x=0;
	y = dices[0]->rgb_height + 14;

	for(i=0;i<3;i++) {
		if( g_juego.dados_dst[i] == 0)
			break;
		dices_show_image( g_juego.dados_dst[i]-1, x,y );
		x += dices[0]->rgb_width;
	}
	dices_show_text( g_juego.dados_dstpais, y-10 );
}
