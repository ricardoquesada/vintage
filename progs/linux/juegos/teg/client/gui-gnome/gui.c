/*	$Id: gui.c,v 1.93 2001/12/22 17:12:05 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file gui.c
 * gui-gnome functions
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>

#include "gui.h"
#include "client.h"

#include "priv.h"
#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "chatline.h"
#include "priv.h"
#include "g_pais.h"
#include "stock.h"
#include "cards.h"
#include "dices.h"
#include "status.h"
#include "objetivo.h"
#include "conectar.h"
#include "armies.h"
#include "colors.h"
#include "themes.h"
#include "g_scores.h"


struct _gui_private gui_private;

TTheme	gui_theme;

static TEG_STATUS get_default_values( void );

static const struct poptOption options[] = {
#ifdef WITH_GGZ
	{"ggz", '\0', POPT_ARG_NONE, &g_juego.with_ggz, 0, N_("Enables GGZ mode"), NULL},
#endif /* WITH_GGZ */
	{"observe", '\0', POPT_ARG_NONE, &g_juego.observer, 0, N_("Observe the game, dont play it"), NULL},
	{NULL, '\0', POPT_ARG_NONE, 0, 0, NULL, NULL}
};


/**
 * @fn TEG_STATUS gui_objetivo()
 * Muestra tu objetivo
 */
TEG_STATUS gui_objetivo()
{
	objetivo_view();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_habilitado( int numjug)
 */
TEG_STATUS gui_habilitado( int numjug )
{
	if( numjug == WHOAMI() ) {
		set_sensitive_tb();
		ministatus_update();
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_winner( int numjug, int mission )
{
	armies_unview();

	teg_dialog_gameover( numjug, mission );

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_lost(int numjug)
 */
TEG_STATUS gui_lost(int numjug)
{
	char buf[400];
	PJUGADOR pJ;

	memset(buf,0,sizeof(buf));
	armies_unview();

	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS) {
		/* */

	} else if( numjug == WHOAMI() ) {
		snprintf(buf,sizeof(buf)-1,_("You lost the game :("));
		teg_dialog(_("You lost"),_("Game Over"),buf);

	} else {
		snprintf(buf,sizeof(buf)-1,_("Player %s(%s) lost the game"),pJ->nombre,_(g_colores[pJ->color]));
		teg_dialog(_("A player lost the game"),_("Player lost the game"),buf);
	}
	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_surrender(int numjug)
 */
TEG_STATUS gui_surrender(int numjug)
{
	char buf[400];
	PJUGADOR pJ;

	memset(buf,0,sizeof(buf));
	armies_unview();

	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS) {
		/* */

	} else if( numjug == WHOAMI() ) {
		/* */

	} else {
		snprintf(buf,sizeof(buf)-1,_("Coward %s(%s) has surrendered"),pJ->nombre,_(g_colores[pJ->color]));
		teg_dialog(_("A player has surrendered"),_("A player has surrendered"),buf);
	}
	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_exit( char *str)
 */
TEG_STATUS gui_exit( char *str)
{
	gtk_main_quit();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_init( int argc, char **argv)
 */
TEG_STATUS gui_init( int argc, char **argv)
{
	GnomeClient *client;

	setlocale(LC_ALL,"");
	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);

	gnome_init_with_popt_table("TEG", VERSION, argc, argv, options, 0, NULL);

	stock_init();

	client = gnome_master_client();

	get_default_values();

	theme_load(g_juego.theme);

	if( theme_giveme_theme(&gui_theme) != TEG_STATUS_SUCCESS ) {
		fprintf(stderr,"Error loading theme!\n");
		return TEG_STATUS_ERROR;
	}

	main_window = create_mainwin ();
	if( !main_window )
		return TEG_STATUS_ERROR;

	priv_init();

	if( colors_allocate() != TEG_STATUS_SUCCESS )
		return TEG_STATUS_ERROR;

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_main(void)
 */
TEG_STATUS gui_main(void)
{

	/* show welcome message */
	textmsg(M_ALL,_("Tenes Empanadas Graciela - Gnome client v%s - by Ricardo Quesada"),VERSION);
	textmsg(M_ALL,_("Using theme '%s - v%s' by %s\n"),g_juego.theme, gui_theme.version,gui_theme.author);

	/* poner en sensitive los botones*/
	set_sensitive_tb();

	/* mostrar ventana de coneccion */
	conectar_view();

#ifdef WITH_GGZ
	if( g_juego.with_ggz && g_juego.fd < 0)
		return TEG_STATUS_ERROR;
#endif /* WITH_GGZ */

	gtk_main();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_disconnect(void)
 */
TEG_STATUS gui_disconnect(void)
{
	if( gui_private.tag >=0 )
		gdk_input_remove( gui_private.tag );
	gui_private.tag=-1;
	set_sensitive_tb();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_connected( char *c)
 */
TEG_STATUS gui_connected( char *c)
{
	set_sensitive_tb();
	if( !g_juego.observer)
		colortype_view( c );

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_reconnected()
{
	set_sensitive_tb();
	return TEG_STATUS_SUCCESS;
}

static TEG_STATUS get_default_values( void )
{
	gchar temporal[PLAYERNAME_MAX_LEN];
	snprintf(temporal,sizeof(temporal)-1,"/TEG/data/playername=%s",getenv("LOGNAME"));
	strncpy(g_juego.myname,gnome_config_get_string_with_default(temporal,NULL),PLAYERNAME_MAX_LEN);
	g_juego.mycolor = gnome_config_get_int_with_default("/TEG/data/color=1",NULL);

	strncpy(g_juego.sername,gnome_config_get_string_with_default("/TEG/data/servername=localhost",NULL),SERVER_NAMELEN);
	g_juego.serport = gnome_config_get_int_with_default("/TEG/data/port=2000",NULL);
	g_juego.msg_show = gnome_config_get_int_with_default("/TEG/data/msgshow=254",NULL);
	gui_private.msg_show_colors =  gnome_config_get_int_with_default("/TEG/data/msgshow_with_color=0",NULL);
	gui_private.status_show =  gnome_config_get_int_with_default("/TEG/data/status_show=65485",NULL);
	strncpy(g_juego.theme,gnome_config_get_string_with_default("/TEG/data/theme=sentimental",NULL),sizeof(g_juego.theme));
	
	gnome_config_set_int   ("/TEG/data/port",  g_juego.serport);
	gnome_config_set_string("/TEG/data/servername",g_juego.sername);
	gnome_config_set_string("/TEG/data/playername",g_juego.myname);
	gnome_config_set_string("/TEG/data/theme",g_juego.theme);
	gnome_config_set_int("/TEG/data/color",g_juego.mycolor);
	gnome_config_set_int("/TEG/data/msgshow",g_juego.msg_show);
	gnome_config_set_int("/TEG/data/msgshow_with_color",gui_private.msg_show_colors);
	gnome_config_set_int("/TEG/data/status_show",gui_private.status_show);
	gnome_config_sync();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_start(void)
 * Dice que se empezo a jugar
 */
TEG_STATUS gui_start(void)
{
	cards_flush();
	set_sensitive_tb();
	if( status_dialog!= NULL) out_status();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_status()
 * update the Players Status window
 */
TEG_STATUS gui_status()
{
	return status_update();
}

/**
 * @fn TEG_STATUS gui_scores()
 * update the scores
 */
TEG_STATUS gui_scores()
{
	return gui_scores_view();
}

/**
 * @fn TEG_STATUS gui_fichas(int cant, int conts)
 * Avisa la cantidad de ejercitos que hay que poner
 * @param cant cantidad de ejercitos
 * @conts fichas que hay que poner por continentes conquistados
 */
TEG_STATUS gui_fichas(int cant, int conts)
{
	char buf[500];
	JUG_ESTADO e;

	set_sensitive_tb();

	if( status_dialog != NULL) out_status();

	e = ESTADO_GET();

	armies_view( cant, conts );

	switch(e) {
	case JUG_ESTADO_FICHAS:
		sprintf(buf,_("1st part:\nPlace %d armies in your countries"),cant);
		break;
	case JUG_ESTADO_FICHAS2:
		sprintf(buf,_("2nd part:\nPlace %d armies in your countries"),cant);
		break;
	case JUG_ESTADO_FICHASC:
		{
			int i,p1,p2,p3;
			char buf2[500];
			int hubo_cont=0;

			buf[0]=0;
			for(i=0;i<CONT_CANT;i++) {
				if( conts & 1 ) {
					snprintf(buf2,sizeof(buf2)-1,_("Place %d armies in %s\n"),g_conts[i].fichas_x_cont,_(g_conts[i].nombre));
					buf2[ sizeof(buf2) -1 ] = 0;
					strcat(buf,buf2);
					hubo_cont=1;
				}
				conts >>= 1;
			}
			if( hubo_cont ) {
				snprintf(buf2,sizeof(buf2)-1,_("And place another %d armies in your countries\n"),cant);
				buf2[ sizeof(buf2) -1 ] = 0;
				strcat(buf,buf2);
			} else {
				snprintf(buf2,sizeof(buf2)-1,_("Place %d armies in your countries\n"),cant);
				buf2[ sizeof(buf2) -1 ] = 0;
				strcat(buf,buf2);
			}

			if( canje_puedo(&p1,&p2,&p3) == TEG_STATUS_SUCCESS ) {
				strcat(buf,_("\nIf you want to EXCHANGE your cards for armies,\nDO IT NOW!"));
				cards_select(p1,p2,p3);
			}
			break;
		}
	default:
		textmsg(M_ERR,"Error in out_fichas()");
		return TEG_STATUS_ERROR;
	}

	teg_dialog(_("Place armies"),_("Place armies"),buf);
	textmsg(M_IMP,"%s",buf);

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_turno()
 * Avisa que es mi turno de atacar
 */
TEG_STATUS gui_turno()
{
	set_sensitive_tb();

	if( status_dialog != NULL) out_status();

	teg_dialog(_("Its your turn"),_("Its your turn to attack"),
		_("Select two countries:\n1st: click on the source country\n2nd: click on the destination country"));

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_pais( int p )
 * muestra un pais
 * @param p Pais a mostrar
 */
TEG_STATUS gui_pais( int p )
{
//	if( status_dialog != NULL) out_status();

	if( p < PAISES_CANT ) {
		G_pais_draw_ejer( p );
	}

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_tropas( int src, int dst, int cant )
 * Dice que se conquisto un pais y le permite pasar ejercitos de un pais a otro
 * @param src Pais origen
 * @param dst Pais destino
 * @param cant Cantidad de ejercitos como max que se pueden pasar
 */
TEG_STATUS gui_tropas( int src, int dst, int cant )
{
	tropas_window( src, dst, cant );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_reagrupe( int src, int dst, int cant )
 * Dice las fichas que se van a pasar de un pais a otro
 * @param src Pais origen
 * @param dst Pais destino
 * @param cant Cantidad de ejercitos como max que se pueden pasar
 */
TEG_STATUS gui_reagrupe( int src, int dst, int cant )
{
	reagrupe_window( src, dst, cant );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_tarjeta( int pais, int usado )
 * Llamado cuando recibo una tarjeta
 * @param pais Tarjeta del pais que recibo
 */
TEG_STATUS gui_tarjeta( int pais )
{
	cards_view( pais );
	set_sensitive_tb();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_dados()
 * Llamado cuando recibo una tarjeta
 * @param pais Tarjeta del pais que recibo
 * @para usado Si la tarjeta la uso el server automaticamente (ver token_tarjeta en server)
 */
TEG_STATUS gui_dados()
{
	dices_view();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_sensi()
 * Para poner sentitiviness en algunos botones
 */
TEG_STATUS gui_sensi()
{
	set_sensitive_tb();
	cards_update();
	cards_update_para_canje();
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_canje()
 * Informa que se hizo bien el canje y borra las tarjetas si estan vistas
 * @param cant Cantidad de ejercitos que se pueden agregar por el canje hecho
 * @param p1 Pais 1 del canje hecho
 * @param p2 Pais 2 del canje hecho
 * @param p3 Pais 3 del canje hecho
 */
TEG_STATUS gui_canje( int cant, int p1, int p2, int p3)
{
	char buf[100];
	snprintf(buf, sizeof(buf)-1,_("Now you can add %d extra armies in your countries!"), cant);
	buf[ sizeof(buf) -1 ]=0;
	teg_dialog(_("Extra armies"),_("Extra armies"),buf);
	armies_view_more( cant );
	cards_delete(p1,p2,p3);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_ataque( int src, int dst )
 * Muestra que se esta atacando de un pais a otro
 * @param src pais que ataca
 * @param dst pais que se defiende
 */
TEG_STATUS gui_ataque( int src, int dst )
{
	gui_pais_select( src );
	gui_pais_select( dst );
	return TEG_STATUS_SUCCESS;
}
