/*	$Id: chatline.h,v 1.2 2001/05/01 18:28:51 riq Exp $	*/
/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/
/*
 * Modificado para que se adapte
 */
#ifndef __TEG_GUI_GNOME_CHATLINE_H
#define __TEG_GUI_GNOME_CHATLINE_H

void inputline_return(GtkWidget *w, gpointer data);
void output_window_clear(void);
TEG_STATUS gui_textmsg(char *astring);
TEG_STATUS gui_textplayermsg(char *name, int nj, char *msg);

#endif  /* __TEG_GUI_GNOME_CHATLINE_H */
