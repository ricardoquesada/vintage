/*	$Id: callbacks.c,v 1.58 2001/11/24 17:32:17 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file callbacks.c
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "gui.h"
#include "client.h"

#include "chatline.h"
#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "priv.h"
#include "g_pais.h"
#include "cards.h"
#include "dices.h"
#include "reagrupe.h"
#include "objetivo.h"
#include "status.h"
#include "conectar.h"
#include "armies.h"
#include "colors.h"
#include "preferences.h"
#include "g_scores.h"


/*
 * funciones auxiliares
 */

/* close a window */
void destroy_window( GtkWidget * widget, GtkWidget **window )
{
	if( *window != NULL) {
		gtk_widget_destroy(*window);
	}

	*window=NULL;
}


/* Brings attention to a window by raising it and giving it focus */
void raise_and_focus (GtkWidget *widget)
{
	g_assert (GTK_WIDGET_REALIZED (widget));
	gdk_window_show (widget->window);
	gtk_widget_grab_focus (widget);
}

/*
 * otras funciones
 */
TEG_STATUS pre_client_recv( gpointer data, int sock, GdkInputCondition GDK_INPUT_READ )
{
	client_recv( sock );
	return TEG_STATUS_SUCCESS;
}

/*
 * funciones callbacks
 */
/* game */
/**
 * @fn void on_connect_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_connect_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	conectar_view();
}

/**
 * @fn void on_disconnect_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_disconnect_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	desconectar();
}

/**
 * @fn void on_disconnect_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_launchrobot_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	launch_robot();
}

void on_scores_activate(GtkMenuItem *widget, gpointer user_data)
{
	out_scores();
	gui_scores_view();
}

/**
 * @fn void on_exit_activate (GtkWidget *widget, gpointer user_data)
 */
void on_exit_activate (GtkWidget *widget, gpointer user_data)
{
	desconectar();
	cards_free();
	colors_free();
	gtk_main_quit();
}

/* actions */
/**
 * @fn void on_enviarfichas_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_enviarfichas_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	int i;
	int cant, conts;

	if( fichas_out() != TEG_STATUS_SUCCESS )  {

		fichas_get_wanted( &cant, &conts );
		armies_view( cant, conts );

		for(i=0;i<PAISES_CANT;i++)
			G_pais_draw_ejer(i);
	} else {
		armies_unview();
	}
	set_sensitive_tb();
}

/**
 * @fn void on_updatemap_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_updatemap_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	out_paises();
}

/**
 * @fn void on_enviarreataque_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_enviarreataque_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	ataquere_out();
}

/**
 * @fn void on_enviarataquereset_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_enviarataquereset_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if( ataque_pre_reset() == TEG_STATUS_SUCCESS )
		textmsg(M_INF,_("The attack was reset. Please, select the source country to continue your attack"));
}

/**
 * @fn void on_reagrupe_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_reagrupe_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	if( reagrupe_init() == TEG_STATUS_SUCCESS ) {
		teg_dialog( _("Regrouping armies"),_("Regrouping armies"),
			_("Select two countries:\n1st: click on the source country\n2nd: click on the destination country"));
	}
}

/**
 * @fn void on_pedirtarjeta_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_pedirtarjeta_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	out_tarjeta();
	set_sensitive_tb();
}

/**
 * @fn void on_endturn_activate (GtkMenuItem *menuitem, gpointer user_data)
 */
void on_endturn_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	out_endturn();
	set_sensitive_tb();
}

/**
 * @fn void on_surrender_activate(GtkMenuItem *menuitem, gpointer user_data)
 */

static void surrender_cb (int button, gpointer data)
{
	if (button == 0)
		out_surrender();
}
void on_surrender_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	gnome_question_dialog_parented (_("Really surrender ?"), surrender_cb, NULL, GTK_WINDOW(main_window));
}


void
on_start_activate(GtkMenuItem  *menuitem, gpointer         user_data)
{
	gametype_view();
}


void
on_status_activate(GtkMenuItem  *menuitem, gpointer         user_data)
{
	status_view();
}

void
on_viewcards_activate(GtkMenuItem  *menuitem, gpointer         user_data)
{
	cards_view(-1);
}

void
on_viewdices_activate(GtkMenuItem  *menuitem, gpointer         user_data)
{
	dices_view();
}

void
on_viewmission_activate(GtkMenuItem  *menuitem, gpointer         user_data)
{
	objetivo_view();
}


void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	preferences_activate();
}


void on_about_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	GtkWidget *href, *hbox;
	static GtkWidget *about = NULL;
	static const char *authors[] = {
		"Ricardo Quesada, main coder",
		"Wolfgang Morawetz, main artist",
		"Sebastian Cativa Tolosa, idea contributor",
		"Luciano Notarfrancesco, package contributor",
		"Hernan Ochoa, first beta tester",
		"Benoit Timbert, french translator",
		"Thomas R. Koll, german translator",
		"Arkadiusz Lipiec, polish translator",
		"Benoit Rousseau, fixes",
		"David Haller, fixes",
		"Julio Santa Cruz, fixes",
		"Harmen, fixes",
		"Davide Puricelli, debian mantainer",
		"Jason Short, package contributor",
		"Justin Zaun, idea contributor",
		"Andreas Henningsson, idea contributor",
		NULL
	};

	if (!about) {
		about = gnome_about_new (
			_("Tenes Empanadas Graciela"),
			VERSION,
			_("Copyright (C) 2000, 2001 Ricardo Quesada"),
			authors,
			_("A clone of T.E.G. (a Risk clone)."),
			"teg_icono.png");
		gtk_signal_connect (GTK_OBJECT (about), "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &about);


		hbox = gtk_hbox_new (TRUE, 0);
		href = gnome_href_new ("http://teg.sourceforge.net", _("TEG Home Page"));
		gtk_box_pack_start (GTK_BOX (hbox), href, FALSE, FALSE, 0);
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (about)->vbox),
			    hbox, TRUE, FALSE, 0);
		gtk_widget_show_all (hbox);
	}

	gtk_widget_show_now (about);
	raise_and_focus (about);
}
