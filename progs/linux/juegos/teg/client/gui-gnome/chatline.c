/*	$Id: chatline.c,v 1.9 2001/05/05 16:38:42 riq Exp $	*/
/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gui.h"
#include "client.h"
#include "interface.h"
#include "chatline.h"
#include "colors.h"
#include "priv.h"

void inputline_return(GtkWidget *w, gpointer data)
{
	char *theinput;

	if( ESTADO_ES(JUG_ESTADO_DESCONECTADO) ) {
		gtk_entry_set_text(GTK_ENTRY(w), "");
		textmsg(M_INF,_("You need to be connected"));
		return;
	}

	theinput = gtk_entry_get_text(GTK_ENTRY(w));

	if(*theinput) {
		if(theinput[0]=='/') {
			net_printf( g_juego.fd, "%s\n",&theinput[1]);
		} else  {
			out_mensaje( theinput );
		}
	}
	gtk_entry_set_text(GTK_ENTRY(w), "");
}

/* funcion exportada a cliente */
TEG_STATUS gui_textmsg(char *astring)
{
	gtk_text_freeze(GTK_TEXT(main_message_area));
	gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL, astring, -1);
	gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL, "\n", -1);
	gtk_text_thaw(GTK_TEXT(main_message_area));

	/* move the scrollbar forward by a ridiculous amount */
	gtk_range_default_vmotion(GTK_RANGE(text_scrollbar), 0, 10000);
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_textplayermsg(char *n, int num, char *msg)
{
	char name[PLAYERNAME_MAX_LEN+30];

	snprintf(name,sizeof(name)-1,"<%s>",n);

	gtk_text_freeze(GTK_TEXT(main_message_area));

	if( gui_private.msg_show_colors) {
		gtk_text_insert(GTK_TEXT(main_message_area), NULL, colors_get_player_ink(num),colors_get_player(num), name, -1);
	} else {
		gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL,colors_get_player(num)," ", -1);
		gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL, name, -1);
	}
	gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL, " ", -1);
	gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL, msg, -1);
	gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL, "\n", -1);
	gtk_text_thaw(GTK_TEXT(main_message_area));

	/* move the scrollbar forward by a ridiculous amount */
	gtk_range_default_vmotion(GTK_RANGE(text_scrollbar), 0, 10000);
	return TEG_STATUS_SUCCESS;
}

void output_window_clear(void)
{
	gtk_text_freeze(GTK_TEXT(main_message_area));
	gtk_editable_delete_text(GTK_EDITABLE(main_message_area), 0, -1);
	gtk_text_insert(GTK_TEXT(main_message_area), NULL, NULL, NULL,
		_("Cleared output window.\n"), -1);
	gtk_text_thaw(GTK_TEXT(main_message_area));
}
