/*	$Id: g_pais.h,v 1.11 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * \file g_pais.h
 */
#ifndef __TEG_GUI_GNOME_GPAIS_H
#define __TEG_GUI_GNOME_GPAIS_H

#include <gnome.h>

struct _G_paises {
	char *gfx_name;			/**< nombre del archivo grafico del pais */
	gint x;				/**< x relativo al continente */
	gint y;				/**< idem anterior */
	gint x_len;			/**< valores updateados en real time. Dont fill	*/
	gint y_len;			/**<		"" 				*/
	gint x_center;			/**< si las fichas no estan centradas usar esto	*/
	gint y_center;			/**<              ""				*/
	GnomeCanvasGroup *pais_group;	/**< contenedor del pais */
	GnomeCanvasItem *ellip_item;	/**< elipse que representa al ejercito */
	GnomeCanvasItem *text_item;	/**< texto que dice la cantidad de ejercitos */
}; 

extern struct _G_paises G_paises[];

void G_pais_create( int pais );
void G_pais_draw( int pais );
void G_pais_draw_ejer(int pais) ;
int G_pais_tot();

/* gui */
TEG_STATUS gui_pais_select(int pais);

#endif /* __TEG_GUI_GNOME_GPAIS_H */
