/*	$Id: conectar.c,v 1.20 2001/11/25 06:39:43 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file conectar.c
 * Tiene los dialogos de coneccion y de seleccion de color y game type
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#include <gnome.h>

#include "gui.h"
#include "client.h"
#include "support.h"
#include "interface.h"
#include "callbacks.h"
#include "conectar.h"
#include "priv.h"
#include "colors.h"

static GtkWidget *conectar_window = NULL;
static GtkWidget *con_entry_name=NULL;
static GtkWidget *con_spinner_port=NULL;
static GtkWidget *con_entry_server=NULL;

static GtkWidget *button_launch=NULL;

static GtkWidget *button_observe=NULL;

static GtkWidget *colortype_dialog=NULL;

static GtkWidget *gametype_dialog=NULL;
static GtkWidget *gametype_button_secret=NULL;
static GtkWidget *gametype_button_conqworld=NULL;


static GtkWidget *boton_color[TEG_MAX_PLAYERS] = { NULL, NULL, NULL, NULL, NULL, NULL };

static TEG_STATUS conectar_real()
{
	if( conectar() == TEG_STATUS_SUCCESS ) {
		gui_private.tag = gdk_input_add( g_juego.fd, GDK_INPUT_READ, (GdkInputFunction) pre_client_recv, (gpointer) NULL );

		if( !g_juego.with_ggz ) {
			out_id();
			if( conectar_window ) destroy_window( conectar_window, &conectar_window );
		}
		paises_redraw_all();
		return TEG_STATUS_SUCCESS;
	}
	return TEG_STATUS_ERROR;
}

static gint conectar_button_con_cb(GtkWidget *area, GdkEventExpose *event, gpointer user_data)
{
	strncpy(g_juego.myname,gtk_entry_get_text(GTK_ENTRY(con_entry_name)),PLAYERNAME_MAX_LEN);
	strncpy(g_juego.sername,gtk_entry_get_text(GTK_ENTRY(con_entry_server)),SERVER_NAMELEN);
	g_juego.serport = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(con_spinner_port));

	g_juego.observer = GTK_TOGGLE_BUTTON(button_observe)->active;

	/* if you checked "start local server" we'll copy "localhost" to g_juego.sername */
	if( GTK_TOGGLE_BUTTON(button_launch)->active ) {
		launch_server(g_juego.serport);
		strncpy (g_juego.sername, "localhost", sizeof(g_juego.sername)-1);
	}

	gnome_config_set_int   ("/TEG/data/port",  g_juego.serport);
	gnome_config_set_string("/TEG/data/servername",g_juego.sername);
	gnome_config_set_string("/TEG/data/playername",g_juego.myname);

	gnome_config_sync();

	conectar_real();

	return FALSE;
}

/**
 * @fn void conectar_view()
 * Muestra la ventana de coneccion
 */
void conectar_view()
{
	GtkWidget *label;
	GtkWidget *table;
	GtkWidget *frame;
        GtkAdjustment *adj;

#ifdef WITH_GGZ
	if( g_juego.with_ggz ) {
		conectar_real();
		return;
	}
#endif /* WITH_GGZ */


	if( conectar_window != NULL ) {
		gtk_widget_show_all(conectar_window);
		raise_and_focus(conectar_window);
		return ;
	}

	conectar_window = teg_dialog_new(_("Connect to server"),_("Connect to server")); 
	gnome_dialog_append_buttons(GNOME_DIALOG(conectar_window),
		GNOME_STOCK_BUTTON_OK,
		GNOME_STOCK_BUTTON_CANCEL,
		NULL );
	gnome_dialog_set_default(GNOME_DIALOG(conectar_window),0);

	gnome_dialog_button_connect (GNOME_DIALOG(conectar_window),
		1, GTK_SIGNAL_FUNC(destroy_window),&conectar_window);

	gnome_dialog_button_connect (GNOME_DIALOG(conectar_window),
		0, GTK_SIGNAL_FUNC(conectar_button_con_cb),conectar_window);

	gtk_signal_connect( GTK_OBJECT(conectar_window),
			"delete_event", GTK_SIGNAL_FUNC(destroy_window),
			&conectar_window);

	gtk_signal_connect( GTK_OBJECT(conectar_window),
			"destroy", GTK_SIGNAL_FUNC(destroy_window),
			&conectar_window);

	/* server options */
	table = gtk_table_new (3, 2, TRUE);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);

	frame = gtk_frame_new (_("Server Options"));
	gtk_container_border_width (GTK_CONTAINER (frame), 1);
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(conectar_window)->vbox), frame);

	/* server port */
	label = gtk_label_new(_("Server port:"));
	gtk_table_attach_defaults( GTK_TABLE(table), label, 0, 1, 0, 1 );
	adj = (GtkAdjustment *) gtk_adjustment_new( g_juego.serport, 1.0, 65536.0, 1.0, 5.0, 1.0 );
	con_spinner_port = gtk_spin_button_new( adj, 0.0, 0);
	gtk_table_attach_defaults( GTK_TABLE(table), con_spinner_port, 1, 2, 0, 1 );

	/* server name */
	label = gtk_label_new(_("Server name:"));
	gtk_table_attach_defaults( GTK_TABLE(table), label, 0, 1, 1, 2 );
	con_entry_server = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( con_entry_server ), g_juego.sername);
	gtk_table_attach_defaults( GTK_TABLE(table), con_entry_server, 1, 2, 1, 2 );
	

	/* player name */
	label = gtk_label_new(_("Name:"));
	gtk_table_attach_defaults( GTK_TABLE(table), label, 0, 1, 2, 3 );
	con_entry_name = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( con_entry_name ), g_juego.myname);
	gtk_table_attach_defaults( GTK_TABLE(table), con_entry_name, 1, 2, 2, 3 );


	gtk_container_add(GTK_CONTAINER( frame), table );

	/* launch localhost server */
	button_launch = gtk_check_button_new_with_label(_("Start server locally"));
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(conectar_window)->vbox), button_launch);

	/* observer mode */
	button_observe = gtk_check_button_new_with_label(_("Dont play, just observe"));
	gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(conectar_window)->vbox), button_observe);

	/* end*/
	gtk_widget_show (GTK_WIDGET(table));
	gtk_widget_show_all(conectar_window);
	raise_and_focus(conectar_window);
}

/*
 * Select a color
 */
static void colortype_ok_cb (GtkWidget *window )
{
	int i;

	for(i=0;i<TEG_MAX_PLAYERS;i++)  {
		if( gtk_toggle_button_get_active( GTK_TOGGLE_BUTTON( boton_color[i])) )
			break;
	}

	out_color( i );

	gtk_widget_destroy(colortype_dialog);
	colortype_dialog = NULL;
}

static GtkWidget *create_color_button( int i )
{
	GtkWidget *vbox;
	GnomeCanvasItem *image;
	GtkWidget	*canvas;

	if( i < 0 || i >= TEG_MAX_PLAYERS )
		return NULL;

	vbox = gtk_vbox_new (FALSE, 0);
	if( ! vbox )
		return NULL;

	canvas = gnome_canvas_new();
	if( ! canvas )
		return NULL;

	gtk_widget_set_usize (canvas, soldier[i]->rgb_width, soldier[i]->rgb_height );
	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 0, 0, soldier[i]->rgb_width, soldier[i]->rgb_height );
	image = gnome_canvas_item_new(
		gnome_canvas_root(GNOME_CANVAS(canvas)),
		gnome_canvas_image_get_type (),
		"image", soldier[i],
		"x", 0.0,
		"y", 0.0,
		"width", (double) soldier[i]->rgb_width,
		"height", (double) soldier[i]->rgb_height,
		"anchor", GTK_ANCHOR_NW,
		NULL);

	if( ! image )
		return NULL;

	gtk_box_pack_start_defaults( GTK_BOX(vbox), GTK_WIDGET(canvas));
	gtk_widget_show (canvas);

	if( i==0 ) {
		boton_color[i] = gtk_radio_button_new_with_label (NULL, _(g_colores[i]) );
	} else {
		boton_color[i] = gtk_radio_button_new_with_label_from_widget ( GTK_RADIO_BUTTON(boton_color[0]), _(g_colores[i]));
	}

	gtk_box_pack_start_defaults(GTK_BOX (vbox), boton_color[i]);
	gtk_widget_show ( boton_color[i] );

	return vbox;
}

/**
 * @fn void colortype_view( char *c)
 * @param c colores activos
 * Muestra el dialogo para elegir el color deseado
 */
void colortype_view( char *c)
{
	GtkWidget *frame;
	GtkWidget *vbox_dia;
	GtkWidget *table;
	int i;
	int first_active =1;

	if( colortype_dialog != NULL ) {
		gdk_window_show( colortype_dialog->window);
		gdk_window_raise( colortype_dialog->window);
		return ;
	}

	colortype_dialog = teg_dialog_new(_("Select your color"),_("Select your color"));

	gtk_signal_connect (GTK_OBJECT (colortype_dialog), "destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed), &colortype_dialog);

	gnome_dialog_append_button( GNOME_DIALOG(colortype_dialog), GNOME_STOCK_BUTTON_OK);

	gnome_dialog_button_connect(GNOME_DIALOG(colortype_dialog), 0, GTK_SIGNAL_FUNC(colortype_ok_cb), NULL);

	vbox_dia = GNOME_DIALOG(colortype_dialog)->vbox;

	/* desired color */
	frame = gtk_frame_new (_("Select your desired color"));
	gtk_container_border_width (GTK_CONTAINER (frame), 0);

	table = gtk_table_new (2, 3, TRUE);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);


	for(i=0;i<TEG_MAX_PLAYERS;i++) {
		GtkWidget *button = create_color_button( i );

		if( i%2 == 0 ) {
			gtk_table_attach_defaults( GTK_TABLE(table), button, i/2, (i/2)+1, 0, 1 );
		} else {
			gtk_table_attach_defaults( GTK_TABLE(table), button, (i-1)/2, ((i-1)/2)+1, 1, 2 );
		}

		/* UGLY: I know that boton_color[i] is used... */
		gtk_widget_set_sensitive( boton_color[i], !c[i] );
		if( !c[i] & first_active ) {
			first_active = 0;
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (boton_color[i]), TRUE);
		}
	}

	gtk_container_add (GTK_CONTAINER (frame), table);
	gtk_container_add (GTK_CONTAINER( vbox_dia), frame );

	/* show all */
	if (!GTK_WIDGET_VISIBLE (colortype_dialog))
		gtk_widget_show_all (colortype_dialog);
	else
		gtk_widget_destroy (colortype_dialog);
}

/*
 * type of game: with secret missions, or to conquer the world
 */
static void gametype_ok_cb (GtkWidget *window )
{
	int a;

	a = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (gametype_button_conqworld));
	out_modalidad( a );
	out_start();

	gtk_widget_destroy(gametype_dialog);
	gametype_dialog = NULL;
}

/**
 * @fn void gametype_view( void )
 * Shows the dialog that choose the game type
 */
void gametype_view( void )
{
	GtkWidget *frame;
	GtkWidget *vbox_dia,*vbox;

	if( gametype_dialog != NULL ) {
		gdk_window_show( gametype_dialog->window);
		gdk_window_raise( gametype_dialog->window);
		return ;
	}

	gametype_dialog = teg_dialog_new(_("Select type of game"),_("Select type of game"));

	gtk_signal_connect (GTK_OBJECT (gametype_dialog), "destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed), &gametype_dialog);

	gnome_dialog_append_button( GNOME_DIALOG(gametype_dialog), GNOME_STOCK_BUTTON_OK);

	gnome_dialog_button_connect(GNOME_DIALOG(gametype_dialog), 0, GTK_SIGNAL_FUNC(gametype_ok_cb), NULL);

	vbox_dia = GNOME_DIALOG(gametype_dialog)->vbox;

	/* Select type of game */
	frame = gtk_frame_new (_("Select type of game"));
	gtk_container_border_width (GTK_CONTAINER (frame), 0);

	vbox = gtk_vbox_new (TRUE, 0);

	gametype_button_conqworld =
		gtk_radio_button_new_with_label (
				NULL, _("Play to conquer the world"));
	gtk_box_pack_start_defaults(GTK_BOX (vbox), gametype_button_conqworld);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gametype_button_conqworld), TRUE);

	gametype_button_secret =
		gtk_radio_button_new_with_label (
			gtk_radio_button_group (GTK_RADIO_BUTTON (gametype_button_conqworld)), _("Play with secret missions"));
	gtk_box_pack_start_defaults(GTK_BOX (vbox), gametype_button_secret);

	gtk_container_add (GTK_CONTAINER (frame), vbox);

	gtk_container_add (GTK_CONTAINER( vbox_dia), frame );

	/* show all */
	if (!GTK_WIDGET_VISIBLE (gametype_dialog))
		gtk_widget_show_all (gametype_dialog);
	else
		gtk_widget_destroy (gametype_dialog);
}
