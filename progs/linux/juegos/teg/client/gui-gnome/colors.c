/*	$Id: colors.c,v 1.6 2001/11/24 17:32:17 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file colors.c
 * alloc colors, etc.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <gnome.h>

#include "gui.h"
#include "client.h"

#include "colors.h"
#include "support.h"
#include "interface.h"

#define HELVETICA_10_FONT  "-adobe-helvetica-medium-r-normal-*-10-*-*-*-*-*-*-*"

GdkGC *g_colors_gc = NULL;
GdkFont* g_pixmap_font10 = NULL;

static int allocated=0;

struct _G_colores G_colores[] = {
	{"red", "white"},
	{"yellow", "black"},
	{"blue", "white"},
	{"grey27", "white"},
	{"PaleVioletRed1", "black"},
	{"green", "black"},
	{"grey", "black"},		/* Color no usado por jugadores */
};
#define NR_COLORS (sizeof(G_colores)/sizeof(G_colores[0]))
GdkColor colors_players[NR_COLORS];

char *G_colores_common[] = {
	"white", "black"
};
#define NR_COLORS_COMMON (2)
GdkColor colors_common[NR_COLORS_COMMON];

int colors_foreground[] = { 0,1,0,0,1,1,1 };

GdkImlibImage	*soldier[TEG_MAX_PLAYERS];


TEG_STATUS colors_load_images( void )
{
	char name[512];
	char *filename=NULL;

	int i;

	for(i=0; i < TEG_MAX_PLAYERS ; i++ ) {

		memset(name, 0, sizeof(name) );

		/* IMPORTANT:  Dont translate g_colores! */
		snprintf( name, sizeof(name) -1, "soldier_%s.png", g_colores[i] );
		filename = load_pixmap_file (name);
		soldier[i] = gdk_imlib_load_image ( filename );

		if(filename)
			g_free( filename );

		if( soldier[i] == NULL ) {
			g_warning(_("Error, couldn't find file: %s"),name);
			return TEG_STATUS_ERROR;
		}
	}

	return TEG_STATUS_SUCCESS;
}
/**
 * @fn TEG_STATUS colors_allocate( void )
 */
TEG_STATUS colors_allocate( void )
{
	GdkColormap *colormap;
	int i;

	if(allocated)
		return TEG_STATUS_ERROR;

    	colormap = gtk_widget_get_colormap(main_window);

	for(i=0;i<NR_COLORS;i++) {
		gdk_color_parse( G_colores[i].ellip_color, &colors_players[i]);
		gdk_colormap_alloc_color( colormap, &colors_players[i], FALSE, TRUE);
	}

	for(i=0;i<NR_COLORS_COMMON;i++) {
		gdk_color_parse( G_colores_common[i], &colors_common[i]);
		gdk_colormap_alloc_color( colormap, &colors_common[i], FALSE, TRUE);
	}

	g_colors_gc = gdk_gc_new(main_window->window);
	g_pixmap_font10 = gdk_font_load (HELVETICA_10_FONT);

	colors_load_images();

	allocated=1;

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS colors_free()
 */
TEG_STATUS colors_free()
{
	GdkColormap *colormap;
	int i;

	if(allocated)
		return TEG_STATUS_ERROR;

    	colormap = gtk_widget_get_colormap(main_window);

	gdk_colormap_free_colors(colormap,(GdkColor*)&colors_players,NR_COLORS);
	gdk_colormap_free_colors(colormap,(GdkColor*)&colors_common,COLORS_LAST);

	for(i=0; i < TEG_MAX_PLAYERS ; i++ ) {	
		if( soldier[i] ) {
			gdk_imlib_destroy_image (soldier[i]);
			soldier[i] = NULL;
		}
	}

	if( g_colors_gc ) {
		gdk_gc_unref( g_colors_gc );
		g_colors_gc = NULL;
	}

	if( g_pixmap_font10 ) {
		gdk_font_unref( g_pixmap_font10 );
		g_pixmap_font10 = NULL;
	}

	allocated=0;

	return TEG_STATUS_SUCCESS;
}

GdkColor* colors_get_player( int n )
{
	PJUGADOR pJ;

	if(jugador_whois(n,&pJ) != TEG_STATUS_SUCCESS)
		return &colors_players[NR_COLORS-1];

	if(pJ->color<0 || pJ->color>=NR_COLORS)
		return &colors_players[NR_COLORS-1];
	return &colors_players[pJ->color];
}

GdkColor* colors_get_player_from_color( int color )
{
	if(color<0 || color>=NR_COLORS)
		return &colors_players[NR_COLORS-1];
	return &colors_players[color];
}

GdkColor* colors_get_player_ink(int n )
{
	PJUGADOR pJ;

	if(jugador_whois(n,&pJ) != TEG_STATUS_SUCCESS)
		return &colors_common[COLORS_BLACK];

	if(pJ->color<0 || pJ->color>=NR_COLORS)
		return &colors_common[COLORS_BLACK];
	return &colors_common[colors_foreground[pJ->color]];
}

GdkColor* colors_get_player_ink_from_color(int color )
{
	if(color<0 || color>=NR_COLORS)
		return &colors_common[COLORS_BLACK];
	return &colors_common[colors_foreground[color]];
}

GdkColor* colors_get_player_virtual( int n )
{
	if( n == g_juego.numjug )
		return &colors_players[COLORS_P_BLUE];
	return &colors_players[COLORS_P_RED];
}

GdkColor* colors_get_common( int n )
{
	if(n<0 || n>=NR_COLORS_COMMON)
		return &colors_players[NR_COLORS_COMMON-1];
	return &colors_common[n];
}


