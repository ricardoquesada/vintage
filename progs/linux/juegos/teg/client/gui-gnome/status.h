/*	$Id: status.h,v 1.7 2001/12/22 03:41:12 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file status.h
 */
#ifndef __GUI_GNOME_STATUS_H
#define __GUI_GNOME_STATUS_H


enum {
	S_CLIST_N_COLOR,
	S_CLIST_N_NUMBER,
	S_CLIST_N_NAME,
	S_CLIST_N_SCORE,
	S_CLIST_N_ADDR,
	S_CLIST_N_HUMAN,
	S_CLIST_N_COUNTRIES,
	S_CLIST_N_ARMIES,
	S_CLIST_N_CARDS,
	S_CLIST_N_STATUS,
	S_CLIST_N_WHO,

	S_CLIST_N_LAST
};

extern GtkWidget *status_dialog;
extern GtkWidget *status_clist;

TEG_STATUS status_fill_clist( GtkWidget *clist );
TEG_STATUS status_update();
void status_view();

/* mini */
GtkWidget *ministatus_build();
TEG_STATUS ministatus_update();

/* main */
TEG_STATUS mainstatus_create( GtkWidget **window );

#endif /* __GUI_GNOME_STATUS_H */
