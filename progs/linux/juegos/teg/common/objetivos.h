/*	$Id: objetivos.h,v 1.4 2001/10/01 03:31:42 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file objetivos.h
 */

#ifndef __TEG_OBJETIVOS_H
#define __TEG_OBJETIVOS_H

typedef struct _objetivos {
	int	numjug;				/**< a quien se asigno el objetivo */
	char	*nombre;			/**< nombre del objetivo */
	int	continentes[CONTINENTE_LAST];	/**< continentes a conquistar */
	int	jugadores[TEG_MAX_PLAYERS];	/**< jugadores a eliminar */
	int	limitrofes;			/**< paises limitrofes a tener */
	int	tot_paises;			/**< cantidad de paises a conquistar */
} OBJETIVOS, *POBJETIVOS;

enum {
	OBJETIVO_CONQWORLD,			/**< objetivo 0. Conquistar el mundo */
	OBJETIVO_COMMON,			/**< objetivo comun: Conquistar 30 paises */
};

extern OBJETIVOS g_objetivos[];

/* prototipos */
int objetivos_cant();

#endif /* __TEG_OBJETIVOS_H */
