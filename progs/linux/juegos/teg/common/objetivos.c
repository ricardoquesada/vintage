/*	$Id: objetivos.c,v 1.8 2001/10/01 03:31:42 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file objetivos.c
 */

#include "all.h"

OBJETIVOS g_objetivos[]= {
	{ 0,	/* Objetivo cuando se juega sin objetivos */
		N_("Conquer the world"),
		{CONT_AMERICASUR_CANT, CONT_AMERICANORTE_CANT, CONT_AFRICA_CANT, CONT_OCEANIA_CANT, CONT_EUROPA_CANT, CONT_ASIA_CANT},
		{0, 0, 0, 0, 0, 0},
		0,
		0 },
	{ 0,	/* Objetivo comun */
		N_("Conquer 30 countries"),
		{0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0},
		0,
		30 },
	{ -1,
		N_("Conquer Africa,\n5 countries of North America and\n4 countries of Europe"),
		{0, 5, CONT_AFRICA_CANT, 0, 4, 0},
		{0, 0, 0, 0, 0, 0},
		0,
		0 },

#ifdef WITH_ALL_MISSIONS
	/* 
	 * esta mision es dificil de soportar porque los paises limitrofes
	 * tiene que estar fuera de los continentes, y el manual no dice nada,
	 * etc, etc, encontes para evitar malos entendidos la desabilito
	 */
	{ -1,
		N_("Conquer South America,\n7 countries of Europe and\n3 frontier countries"),
		{CONT_AMERICASUR_CANT, 0, 0, 0, 7, 0},
		{0, 0, 0, 0, 0, 0},
		3,
		0},
#endif /* WITH_ALL_MISSIONS */

	{ -1,
		N_("Conquer Asia and\n2 countries of South America"),
		{2, 0, 0, 0, 0, CONT_ASIA_CANT},
		{0, 0, 0, 0, 0, 0},
		0,
		0},

	{ -1,
		N_("Conquer Europe,\n4 countries of Asia and,\n2 of South America"),
		{2, 0, 0, 0, CONT_EUROPA_CANT, 4},
		{0, 0, 0, 0, 0, 0},
		0,
		0},

	{ -1,
		N_("Conquer North America,\n2 countries of Oceania and\n4 countries of Asia"),
		{0, CONT_AMERICANORTE_CANT, 0, 2, 0, 4},
		{0, 0, 0, 0, 0, 0},
		0,
		0},

	{ -1,
		N_("Conquer 2 countries of Oceania,\n2 of Africa, 2 of South America,\n3 of Europe, 4 of North America,\nand 3 of Asia"),
		{2, 4, 2, 2, 3, 3},
		{0, 0, 0, 0, 0, 0},
		0,
		0},

	{ -1,
		N_("Conquer Oceania,\nconquer North America and,\n2 countries of Europe"),
		{0, CONT_AMERICANORTE_CANT, 0, CONT_OCEANIA_CANT, 2, 0},
		{0, 0, 0, 0, 0, 0},
		0,
		0},

	{ -1,
		N_("Conquer South America,\nconquer Africa and,\n4 countries of Asia"),
		{CONT_AMERICASUR_CANT, 0, CONT_AFRICA_CANT, 0, 0, 4},
		{0, 0, 0, 0, 0, 0},
		0,
		0},

	{ -1,
		N_("Conquer Oceania,\nconquer Africa and,\n5 countries of North America"),
		{0, 5, CONT_AFRICA_CANT, CONT_OCEANIA_CANT, 0, 0},
		{0, 0, 0, 0, 0, 0},
		0,
		0},
};
#define NROBJETIVOS (sizeof(g_objetivos) /sizeof(g_objetivos[0]))

int objetivos_cant()
{
	return NROBJETIVOS;
}
