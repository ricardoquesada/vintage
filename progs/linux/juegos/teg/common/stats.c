/*	$Id: stats.c,v 1.3 2001/11/11 00:35:43 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file stats.c
 * Statistics for the players
 */

#include "stats.h"
#include "common.h"

TEG_STATUS stats_init( PPLAYER_STATS s )
{
	memset( s, 0, sizeof(*s) );
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS stats_score( PPLAYER_STATS s )
{
	s->score = ( s->countries_won - s->countries_lost ) * 5 +
			( s->armies_won - s->armies_lost ) * 2;

	return TEG_STATUS_SUCCESS;
}
