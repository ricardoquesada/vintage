/*	$Id: gui.c,v 1.43 2001/11/12 04:29:27 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file gui.c
 * Interfaz del robot. Es nula, pero de esta manera el robot se entera
 * el estado del juego.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "gui.h"
#include "client.h"
#include "ai.h"
#include "ai_msg.h"
#include "ai_misc.h"


int robot_seed = 0;			/* variable global del robot */
int robot_timeout = 1;			/* variable que dice los segundos que espera */

/*
 * private functions
 */
void tolowerstr(char *n, char *converted)
{
	int i;

	if( n==NULL || converted==NULL) return;

	for(i=0; n[i] != '\0' ; i++ )
		converted[i] = n[i] | 0x20;
}

/**
 * @fn TEG_STATUS gui_objetivo()
 * Muestra tu objetivo
 */
TEG_STATUS gui_objetivo()
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_textplayermsg(char *n, int num, char *msg)
 * Interpret that messages of the players
 * @param str the format of the message
 */
TEG_STATUS gui_textplayermsg(char *n, int num, char *msg)
{
	char converted[sizeof(g_juego.myname)+1];

	memset( converted,0,sizeof(converted));

	tolowerstr(msg,msg);
	tolowerstr(_(g_juego.myname), converted);

	/* a player, not a robot, sends me a message i respond */
	if( strstr(msg,converted ) && ai_findname(n) != TEG_STATUS_SUCCESS ) {
		ai_msg( AI_MSG_ANSWER, n );
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_habilitado( int numjug )
{
	PJUGADOR pJ;
	if( numjug != WHOAMI() ) {
		if( jugador_whois( numjug, &pJ ) == TEG_STATUS_SUCCESS )
			ai_msg(AI_MSG_HI, pJ->nombre );
	} else {
		out_status();
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_winner(int numjug, int objetivo )
{
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_lost(int numjug)
{
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_surrender(int numjug)
{
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_exit( char * str)
{
	int numjug;

	numjug = atoi( str );
	if( numjug == g_juego.numjug )
		return TEG_STATUS_CONNCLOSED;

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_init( int argc, char **argv)
{
	robot_seed = get_int_from_dev_random();
	srand( robot_seed );

	if( !g_juego.serport )
		g_juego.serport = 2000;

	if( !strlen(g_juego.sername))
		strcpy( g_juego.sername, "localhost");

	if( !strlen(g_juego.myname))
		strncpy( g_juego.myname, ai_nombre(), PLAYERNAME_MAX_LEN);
	textmsg(M_IMP,_("Robot name: %s"),g_juego.myname);

	robot_timeout = 1;

	ai_init();
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_main(void)
{
	fd_set readfds;
	struct timeval tout;
	int r;

	if( conectar() != TEG_STATUS_SUCCESS ) {
		textmsg(M_ERR,_("Robot: Error while trying to connect to server"));
		return TEG_STATUS_ERROR;
	}

	out_id();

	while(1) {
		tout.tv_sec =  robot_timeout;
		tout.tv_usec = 0;
		FD_ZERO( &readfds );
		FD_SET( g_juego.fd ,&readfds );
		r = select(g_juego.fd+1,&readfds,NULL,NULL,&tout);

		/* fd input */
		if( r > 0 ) {
			if (client_recv( g_juego.fd ) == TEG_STATUS_CONNCLOSED )
				return TEG_STATUS_CONNCLOSED;
		}

		/* timeout */
		else if (r == 0) {
			out_loque();
			ai_msg(AI_MSG_MISC, ai_fetch_a_name() );
		}

		/* error */
		else {
			perror("select()");
		}
	}
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_disconnect(void)
{
	exit(1);
	return TEG_STATUS_CONNCLOSED;
}

TEG_STATUS gui_connected( char *c)
{
	int s = RANDOM_MAX(0,TEG_MAX_PLAYERS-1);
	out_color(s);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_start(void)
 * Dice que se empezo a jugar
 */
TEG_STATUS gui_start(void)
{
	robot_timeout = 1;
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_status()
 * Hace update del 'players status window'
 */
TEG_STATUS gui_status()
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_fichas(int cant, int conts)
 * Avisa la cantidad de ejercitos que hay que poner
 * @param cant cantidad de ejercitos
 * @conts fichas que hay que poner por continentes conquistados
 */
TEG_STATUS gui_fichas(int cant, int conts)
{
	int e;

	e = ESTADO_GET();

	robot_timeout = 1;

	switch(e) {
	case JUG_ESTADO_FICHAS:
	case JUG_ESTADO_FICHAS2:
	case JUG_ESTADO_FICHASC: {
		static int hubo_loque = 0;

		/* averiguo el status para saber quien empezo */
		out_status();	

		if( !hubo_loque ) {
			out_loque();
			hubo_loque =1;
		}

		/* por las dudas, este chequeo no esta demas */
		if( g_paises[0].ejercitos == 0 ) {
			out_loque();
			break;
		}

		if( e == JUG_ESTADO_FICHASC ) {
			int p1,p2,p3;
			if( ai_puedocanje(&p1,&p2,&p3) == TEG_STATUS_SUCCESS) {
				if( canje_out(p1,p2,p3) == TEG_STATUS_SUCCESS )
					return TEG_STATUS_SUCCESS;
				else
					textmsg(M_ERR,"Robot: Unexpected error in canje_out()");
				/* else, fall through, y no hago canje */
			}
			if( ai_fichasc( cant, conts) != TEG_STATUS_SUCCESS ) {
				out_paises();
				printf("Error in ai_fichasc(%d,%d)\n",cant,conts);
				textmsg(M_ERR,"Robot: Abnormal error in ai_fichasc()");
				return TEG_STATUS_ERROR;
			}
		} else if( ai_fichas( cant ) != TEG_STATUS_SUCCESS ) {
			out_paises();
			textmsg(M_ERR,"Robot: Abnormal error in ai_fichas()");
			return TEG_STATUS_ERROR;
		}
		hubo_loque = 0;
		break;
	}
	default:
		textmsg(M_ERR,"Robot: Error in gui_fichas()");
		return TEG_STATUS_ERROR;
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_turno()
 * Avisa que es mi turno de atacar
 */
TEG_STATUS gui_turno()
{
	robot_timeout = 1;

	if( ai_turno() != TEG_STATUS_SUCCESS ) {
		ai_reagrupe();
		out_tarjeta();
		out_endturn();
		robot_timeout = 5;
	}

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_pais( int p )
 * muestra un pais
 * @param p Pais a mostrar
 */
TEG_STATUS gui_pais( int p )
{
	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_tropas( int src, int dst, int cant )
 * Dice que se conquisto un pais y le permite pasar ejercitos de un pais a otro
 * @param src Pais origen
 * @param dst Pais destino
 * @param cant Cantidad de ejercitos como max que se pueden pasar
 */
TEG_STATUS gui_tropas( int src, int dst, int cant )
{
	ai_tropas( src, dst, cant );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_reagrupe( int src, int dst, int cant )
 * Dice las fichas que se van a pasar de un pais a otro
 * @param src Pais origen
 * @param dst Pais destino
 * @param cant Cantidad de ejercitos como max que se pueden pasar
 */
TEG_STATUS gui_reagrupe( int src, int dst, int cant )
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_tarjeta( int pais, int usado )
 * Llamado cuando recibo una tarjeta
 * @param pais Tarjeta del pais que recibo
 */
TEG_STATUS gui_tarjeta( int pais )
{
	PTARJETA pT;
	PPAIS pP;
	PLIST_ENTRY l = g_juego.tarjetas_list.Flink;

	while( !IsListEmpty( &g_juego.tarjetas_list ) && (l != &g_juego.tarjetas_list) ) {
		pT = (PTARJETA) l;
		pP = (PPAIS) PAIS_FROM_TARJETA( pT );

		if( !pT->usada && g_paises[pP->id].numjug==WHOAMI() )
			ejer2_out( pP->id );

		l = LIST_NEXT( l );
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_dados()
 * Llamado cuando recibo los dados
 * @para usado Si la tarjeta la uso el server automaticamente (ver token_tarjeta en server)
 */
TEG_STATUS gui_dados()
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_sensi()
 * Para poner sentitiviness en algunos botones
 */
TEG_STATUS gui_sensi()
{
	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS gui_canje()
 * Informa que se hizo bien el canje y borra las tarjetas si estan vistas
 * @param cant Cantidad de ejercitos que se pueden agregar por el canje hecho
 * @param p1 Pais 1 del canje hecho
 * @param p2 Pais 2 del canje hecho
 * @param p3 Pais 3 del canje hecho
 */
TEG_STATUS gui_canje( int cant, int p1, int p2, int p3)
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_ataque( int src, int dst )
 * Muestra que se esta atacando de un pais a otro
 * @param src pais que ataca
 * @param dst pais que se defiende
 */
TEG_STATUS gui_ataque( int src, int dst )
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_textmsg(char *astring);
 * Muestra un texto
 * @param astring Texto a mostrar
 */
TEG_STATUS gui_textmsg(char *astring)
{
	printf("%s\n",astring);
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_scores( void )
{
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS gui_pais_select( int pais )
 */
TEG_STATUS gui_pais_select( int pais )
{
	return TEG_STATUS_SUCCESS;
}

TEG_STATUS gui_reconnected()
{
	return TEG_STATUS_SUCCESS;
}
