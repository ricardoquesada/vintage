/*	$Id: main.c,v 1.14 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file main.c
 * archivo principal del robot
 */

#include <stdio.h>
#include <string.h>

#include "client.h"
#include "ggz_client.h"


TEG_STATUS main_init()
{
	juego_init();
	g_juego.human = 0;
	jugador_init();
	return TEG_STATUS_SUCCESS;
	return TEG_STATUS_SUCCESS;
}

int main( int argc, char **argv)
{
	int i,quiet=0;
	char *option;

	init_nls();
	dont_run_as_root();

	g_juego.serport = 0;
	g_juego.myname[0] = 0;
	g_juego.sername[0] = 0;

	main_init();

	g_juego.msg_show = M_ERR | M_IMP | M_MSG;

	i = 1;
	while (i < argc) {
		if( is_option("--help", argv[i]) ) {
			fprintf(stderr, _("Usage: %s [option ...]\nValid options are:\n"), argv[0]);
			fprintf(stderr, _("  -h, --help\t\tPrint a summary of the options\n"));
			fprintf(stderr, _("  -n, --name NAME\tUse NAME as name\n"));
			fprintf(stderr, _("  -p, --port PORT\tConnect to server port PORT\n"));
			fprintf(stderr, _("  -s, --server HOST\tConnect to the server at HOST\n"));
			fprintf(stderr, _("  -v, --version\t\tPrint the version number\n"));
			fprintf(stderr, _("  -g, --ggz\t\tEnable GGZ mode\n"));
			fprintf(stderr, _("  -q, --quiet\t\tdont show messages\n"));
			exit(0);
		} else if (is_option("--version",argv[i])) {
			fprintf(stderr, TEG_NAME" v"VERSION"\n");
			exit(0);
		} else if ((option = get_option("--name",argv,&i,argc)) != NULL) {
			strncpy( g_juego.myname, option, PLAYERNAME_MAX_LEN );
		} else if ((option = get_option("--port",argv,&i,argc)) != NULL) {
			g_juego.serport=atoi(option);
		} else if ((option = get_option("--server",argv,&i,argc)) != NULL) {
			strncpy( g_juego.sername, option, SERVER_NAMELEN );
		} else if ( is_option("--ggz",argv[i])) {
			g_juego.with_ggz = 1;
		} else if ( is_option("--quiet",argv[i])) {
			quiet = 1;
		} else {
			fprintf(stderr, _("Unrecognized option: \"%s\"\n"), argv[i]);
			exit(1);
		}
		i++;
	}

	if(quiet) g_juego.msg_show = M_MSG;

	textmsg(M_IMP,_("Tenes Empanadas Graciela - Robot v%s - by Ricardo Quesada\n"),VERSION);
	textmsg(M_IMP,_("Robot intelligence: %d%%"),62);

	gui_init(argc, argv);

#ifdef WITH_GGZ
	if( g_juego.with_ggz) ggz_client_init("teg");
#endif /* WITH_GGZ */

	gui_main();

#ifdef WITH_GGZ
	if( g_juego.with_ggz) ggz_client_quit();
#endif /* WITH_GGZ */

	return 1;
}
