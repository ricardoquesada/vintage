/*	$Id: objetivos.c,v 1.12 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file objetivos.c
 * chequea que los objetivos se hallan cumplido
 */
#include <assert.h>
#include "server.h"

/**
 * @fn TEG_STATUS objetivo_chequear( PJUGADOR pJ )
 * Se fija si el jugador pJ cumplio su objetivo
 * @param pJ Jugador a chequear
 * @return TEG_STATUS_GAMEOVER si cumplio su objetivo, otro si no cumplio
 */
TEG_STATUS objetivo_chequear( PJUGADOR pJ )
{
	int i;
	int paises[CONT_CANT];

	assert( pJ );

	if( pJ->objetivo == -1 ) {
		if( objetivo_asignar( pJ ) != TEG_STATUS_SUCCESS )
			return TEG_STATUS_ERROR;
	}


	memset( paises, 0, sizeof(paises) );

	jugador_listar_paises( pJ, paises );

	/* 1st part. Check that the player has the correct number of countries */
	if( g_juego.cmission && pJ->objetivo != OBJETIVO_CONQWORLD ) {
		if( pJ->tot_paises >= g_objetivos[OBJETIVO_COMMON].tot_paises ) {
			pJ->objetivo = OBJETIVO_COMMON;
			goto gano;
		}
	};

	/* 2da parte. Chequear paises por contienente */
	for(i=0;i<CONT_CANT;i++) {
		if( paises[i] < g_objetivos[pJ->objetivo].continentes[i] )
			goto no_gano;
	}

	/* TODO: 3ra parte. Chequear si vencio a los otros jugadores */

	/* TODO: 4ta parte. Chequear si tiene los paises limitrofes que se piden */
	if( (i=g_objetivos[pJ->objetivo].limitrofes) ) {
		int j,k,i_tmp;
		int salir=0;
		for(j=0;j<PAISES_CANT && salir==0;j++) {
			if( g_paises[j].numjug != pJ->numjug )
				continue;
			i_tmp=0;

			for(k=0;k<PAISES_CANT;k++) {
				if( g_paises[k].numjug != pJ->numjug )
					continue;
				if( paises_eslimitrofe(j,k) ) {
					if( ++i_tmp >= i ) {
						/* objetivo cumplido, salir */
						salir=1;
						break;
					}
				}

			}
		}
		if( j>=PAISES_CANT)
			goto no_gano;
	}

	/* tiene todo, entonces gano! */
gano:
	return TEG_STATUS_GAMEOVER;

no_gano:
	return TEG_STATUS_ERROR;
}
/**
 * @fn TEG_STATUS objetivo_asignar( PJUGADOR pJ )
 * Asigna un objetivo a al jugador pJ
 * @param pJ Jugador a asignar un objetivo
 * @return TEG_STATUS_GAMEOVER si cumplio su objetivo, otro si no cumplio
 */
TEG_STATUS objetivo_asignar( PJUGADOR pJ )
{
	int i,obj;

	assert( pJ );

	/* tiene ya un objetivo asignado ? */
	if( pJ->objetivo != -1 )
		return TEG_STATUS_SUCCESS;

	/* Si se juega sin objetivos, es a conquistar el mundo */
	if( g_juego.objetivos == FALSE ) {
		pJ->objetivo = OBJETIVO_CONQWORLD;	/* objetivo de conquistar al mundo */
		return TEG_STATUS_SUCCESS;
	}

	obj = RANDOM_MAX(0,objetivos_cant()-1);

	i = obj;

	for( ; i < objetivos_cant() ; i++) {
		if( g_objetivos[i].numjug == -1 ) {
			g_objetivos[i].numjug = pJ->numjug;
			pJ->objetivo = i;
			return TEG_STATUS_SUCCESS;
		}
	}
	for( i=0; i < obj ; i++ ) {
		if( g_objetivos[i].numjug == -1 ) {
			g_objetivos[i].numjug = pJ->numjug;
			pJ->objetivo = i;
			return TEG_STATUS_SUCCESS;
		}
	}

	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS objetivo_init()
 * Inicializa los objetivos
 */ 
TEG_STATUS objetivo_init()
{
	int i;

	for(i=0;i<objetivos_cant();i++) {
		g_objetivos[i].numjug = -1;
	}
	g_objetivos[OBJETIVO_CONQWORLD].numjug = 0;
	g_objetivos[OBJETIVO_COMMON].numjug = 0;

	return TEG_STATUS_SUCCESS;
}
/**
 * @fn TEG_STATUS objetivo_set( int a )
 * Pone el juego en modalidad 'con objetivos' o 'conquistar el mundo'
 * @param a 0 = conquistar el mundo, 1= con objetivos
 */
TEG_STATUS objetivo_set( int a )
{
	if( JUEGO_EMPEZADO )
		return TEG_STATUS_ERROR;

	if( a!=0) a=1;
	g_juego.objetivos = a;
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS objetivo_common_mission( int a )
 * Enables/Disables playing with common secret mission
 */
TEG_STATUS objetivo_common_mission( int a )
{
	if( JUEGO_EMPEZADO )
		return TEG_STATUS_ERROR;

	if( a!=0) a=1;
	g_juego.cmission= a;
	return TEG_STATUS_SUCCESS;
}
