/*	$Id: jugador.h,v 1.42 2001/11/26 14:47:58 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file jugador.h
 */
#ifndef __TEGS_JUGADOR_H
#define __TEGS_JUGADOR_H

#include "server.h"
#include "stats.h"

typedef struct _jugador {
	LIST_ENTRY next;
	int numjug;				/**< player number */
	char nombre[PLAYERNAME_MAX_LEN];	/**< name */
	char addr[PLAYERADDR_MAX_LEN];		/**< internet address */
	int color;				/**< color */
	LIST_ENTRY paises;			/**< countries that he owns */
	LIST_ENTRY deals;			/**< FIXME: Not implemented yet */
	int objetivo;				/**< mission that the player has */
	BOOLEAN hizo_canje;			/**< exchange done ? */
	BOOLEAN is_player;			/**< player or observer */
	int turno_conq;				/**< Countries conquered in the turn.
						  A player must conquer 1 countrie to ask for cards.
						  And 2 countries, after the 3rd exchange */
	int tot_paises;				/**< number of countries (optimization) */
	int tot_ejercitos;			/**< number of armies (") */
	int tot_tarjetas;			/**< number of cards (") */
	int tot_canjes;				/**< number of exchanges */
	int fd;					/**< file descriptor */
	JUG_ESTADO estado;			/**< status of the player */
	JUG_ESTADO status_before_discon;	/**< status before disconn */
	int pais_src;				/**< country source (TOKEN_TROPAS) */
	int pais_dst;				/**< country dst (TOKEN_TROPAS) */
	BOOLEAN human;				/**< Is this player controlled by a human */

	int fichasc_armies;			/**< Number of armies to place */
	unsigned int fichasc_conts;		/**< Number of conquered continents */

	PLAYER_STATS player_stats;		/**< player statistics */

} JUGADOR, *PJUGADOR;

typedef TEG_STATUS (*jug_map_func)( PJUGADOR pJ);

/*
 * funciones y variables exportadas
 */
extern LIST_ENTRY g_list_jugador;

TEG_STATUS jugador_initplayer( PJUGADOR j );
void jugador_init( void );
PJUGADOR jugador_ins( PJUGADOR j, BOOLEAN esjugador );
#define jugador_ins_player(a) jugador_ins(a,TRUE)
#define jugador_ins_ro(a) jugador_ins(a,FALSE)
TEG_STATUS jugador_del_hard(PJUGADOR j);
TEG_STATUS jugador_del_soft( PJUGADOR pJ );
TEG_STATUS jugador_flush();
TEG_STATUS jugador_asignarpais( int numjug, PPAIS p);
TEG_STATUS jugador_whois( int numjug, PJUGADOR *j);
TEG_STATUS jugador_whoisfd( int fd, PJUGADOR *j);
BOOLEAN jugador_esta_xxx( int fd, int condicion, int strict );
BOOLEAN jugador_esta_xxx_plus( int fd, int condicion, int strict, PJUGADOR *j );
TEG_STATUS jugador_listar_paises( PJUGADOR pJ, int *paises );
TEG_STATUS jugador_listar_conts( PJUGADOR pJ, unsigned long *ret );
TEG_STATUS jugador_clear_turn( PJUGADOR j );
int jugador_fichasc_cant( PJUGADOR pJ );
TEG_STATUS jugador_all_set_status( JUG_ESTADO );
BOOLEAN jugador_is_lost( PJUGADOR pJ );
TEG_STATUS jugador_poner_perdio( PJUGADOR pJ );
TEG_STATUS jugador_numjug_libre( int *libre);
TEG_STATUS jugador_from_indice( int j, int *real_j );
TEG_STATUS jugador_map( jug_map_func func );
TEG_STATUS jugador_findbyname( char *name, PJUGADOR *pJ);
TEG_STATUS jugador_fillname( PJUGADOR pJ, char *name);
BOOLEAN jugador_is_playing( PJUGADOR pJ );
/*! return the PJUGADOR that is disconnected */
PJUGADOR jugador_return_disconnected( PJUGADOR pJ );
/*! return TRUE if pJ is a disconnected player */
BOOLEAN jugador_is_disconnected( PJUGADOR pJ );
/*! deletes the player if it disconnected */
TEG_STATUS jugador_delete_discon( PJUGADOR pJ );
/*! insert all players in scores but the ones in GAMEOVER */
TEG_STATUS jugador_insert_scores( PJUGADOR pJ );

#define JUGADOR_CONNECTED(a) jugador_esta_xxx(a,JUG_ESTADO_CONNECTED,0)
#define JUGADOR_HABILITADO(a) jugador_esta_xxx(a,JUG_ESTADO_HABILITADO,0)
#define JUGADOR_HABILITADO_P(a,j) jugador_esta_xxx_plus(a,JUG_ESTADO_HABILITADO,0,j)
#define JUGADOR_FICHAS(a) jugador_esta_xxx(a,JUG_ESTADO_FICHAS,1)
#define JUGADOR_FICHAS2(a) jugador_esta_xxx(a,JUG_ESTADO_FICHAS2,1)
#define JUGADOR_ATAQUE(a) jugador_esta_xxx(a,JUG_ESTADO_ATAQUE,1)
#define JUGADOR_ATAQUE_P(a,j) jugador_esta_xxx_plus(a,JUG_ESTADO_ATAQUE,1,j)
#define JUGADOR_TROPAS(a) jugador_esta_xxx(a,JUG_ESTADO_TROPAS,1)
#define JUGADOR_TROPAS_P(a,j) jugador_esta_xxx_plus(a,JUG_ESTADO_TROPAS,1,j)

#endif /* __TEGS_JUGADOR_H */
