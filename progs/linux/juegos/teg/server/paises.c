/*	$Id: paises.c,v 1.20 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file paises.c
 * Funciones para manejar paises en el servidor
 */
#include <stdio.h>
#include <stdlib.h>
#include "server.h"

/*
 * incluye matriz de adyacencia
 */

void paises_repartir()
{
	int i;
	int j=0;
	int real_j;
	int resto = PAISES_CANT%g_juego.playing;
	PPAIS p;

	for(i=0; i < PAISES_CANT-resto; i++) {
		p = get_random_pais( pais_libre );
		jugador_from_indice( j, &real_j );
		jugador_asignarpais(real_j,p);
		p->ejercitos=1;	
		j = (++j % g_juego.playing);
	}

	/* toma un jugador al azar, y a partir de el le da un pais a cada uno */
	if( resto ) {
		j = RANDOM_MAX(0,g_juego.playing-1);
		for( i=0;i<resto;i++) {
			p = get_random_pais( pais_libre );
			jugador_from_indice( j, &real_j );
			jugador_asignarpais( real_j,p);
			p->ejercitos=1;	
			j = (++j % g_juego.playing);
		}
	}
}
