/*	$Id: aux.c,v 1.49 2001/12/22 22:01:42 tomk32 Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file aux.c
 * Funciones auxiliares usadas en play
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include "server.h"

static char colors[TEG_MAX_PLAYERS];

/**
 * @fn void color_init()
 * Inicializa los colores
 */
void color_init()
{
	memset( colors, 0, sizeof(colors));
}

/**
 * @fn void color_del( int i )
 * libera un color
 */
void color_del( int i )
{
	colors[i]=0;
}

/**
 * @fn BOOLEAN color_libre( int *color )
 * Busca un color libre
 * @param color Dice el color libre que encontro
 * @return TRUE si pudo encontrar un color libre
 */
BOOLEAN color_libre( int *color )
{
	int i;
	if( (*color) < 0 || (*color) > (TEG_MAX_PLAYERS-1) )
		return FALSE;
	if( colors[*color]==0 ) {
		colors[*color]=1;
		return TRUE;
	}
	/* tratar de buscar un color libre */
	for(i=0;i<TEG_MAX_PLAYERS;i++) {
		if( colors[i]==0 ) {
			colors[i]=1;
			*color = i;
			return TRUE;
		}
	}
	return FALSE;
}

/**
 * @fn TEG_STATUS colores_libres( char *c )
 * Dice los colores libres que hay
 * @param c donde va a poner el resultado
 */
TEG_STATUS colores_libres( char *c )
{
	int i;
	for(i=0;i<TEG_MAX_PLAYERS;i++) {
		c[i] = colors[i];
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn static void ins_orden( char d, char *array, int len )
 * Inserta un char en orden en un array
 */
void ins_orden( char d, char *array, int len )
{
	int i,j;

	for( i=0;i < len ; i++ ) {
		if( d > array[i] )
			break;
	}
	if(array[i] == 0) {
		array[i] = d;
		return;
	}
	j=i;
	for( i=len-1; i>j; i--)
		array[i] = array[i-1];
	array[j]=d;
}

/**
 * @fn TEG_STATUS aux_token_fichas( int fd, char *str, int maximo, unsigned long conts )
 * Validates the total number of armies placed
 */
TEG_STATUS aux_token_fichas( int fd, char *str, int maximo, unsigned long conts )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	PJUGADOR j;
	int fichas,pais,cant,real;
	char *copia;
	int cptr[CONT_CANT];			/**< continent ptr */
	
	if( jugador_whoisfd(fd, &j) != TEG_STATUS_SUCCESS )
		goto error;

	if( g_juego.turno != j )
		goto error;

	if( strlen(str)==0 )
		goto error;

	copia = str;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	memset( cptr, 0, sizeof(cptr));
	fichas=0;
	real=0;

do_real:
	do {
		if( parser_call( &p )) {

			pais = atoi( p.token );		
			cant = atoi( p.value );		

			if( pais<0 || pais >= PAISES_CANT || cant<0 ||
				cant+fichas > maximo || g_paises[pais].numjug != j->numjug ) {
				goto error;
			}

			/* cuando real==1 se asignas los ejercitos, sino hace el test */
			if(real) {
				g_paises[pais].ejercitos += cant;
				j->tot_ejercitos += cant;
			}

			if( conts )
				cptr[ g_paises[pais].continente ] += cant;

			fichas += cant;
		} else {
			goto error;
		}

	} while( p.hay_otro );

	if( fichas != maximo ) {
		goto error;
	}

	/* Did I have to place armies in continents ? */
	if( conts ) {
		unsigned long conts_tmp = conts;
		int i;

		for(i=0;i<CONT_CANT;i++) {
			if( conts_tmp & 1 ) {
				if( cptr[i] < g_conts[i].fichas_x_cont ) {
					goto error;
				}
			}
			conts_tmp >>= 1;
		}
	}

	if(real==0) {
		fichas=0;
		real=1;
		memset( cptr, 0, CONT_CANT);
		p.data = copia;
		goto do_real;
	}

	return TEG_STATUS_SUCCESS;
error:
	return TEG_STATUS_PARSEERROR;
}


/**
 * @fn TEG_STATUS aux_token_attack( int src, int dst, int *src_lost, int *dst_lost, char *dados_src, char *dados_dst )
 * Simula el ataque entre 2 paises tirando los dados
 * @param src ejercitos del atacante
 * @param dst ejercitos del que se defiende
 * @param src_lost (OUT) Cantidad de ejercitos que perdio el que ataca
 * @param dst_lost (OUT) Cantidad de ejercitos que perdio el que se defiende
 * @param dados_src (OUT) Dados del atacante
 * @param dados_dst (OUT) Dados del que se defiende
 */
TEG_STATUS aux_token_attack( int src, int dst, int *src_lost, int *dst_lost, char *dados_src, char *dados_dst )
{
	int i,tmp;
	char src_d[4];
	char dst_d[4];

	memset( src_d,0,sizeof(src_d));
	memset( dst_d,0,sizeof(dst_d));

	(*dst_lost) = 0;
	(*src_lost) = 0;

	if( dst > 3 ) dst =3 ;
	src--;
	if( src > 3 ) src = 3;

	for(i=0;i<src;i++) {
		tmp = RANDOM_MAX(1,6);
		ins_orden( (char) tmp, src_d, 3 );
	}

	for(i=0;i<dst;i++) {
		tmp = RANDOM_MAX(1,6);
		ins_orden( (char) tmp, dst_d, 3 );
	}

	for(i=0; i< 3 && src_d[i]!=0 && dst_d[i]!=0; i++ ) {
		if(src_d[i] > dst_d[i])
			(*dst_lost)++;
		else
			(*src_lost)++;
	}
	memcpy( dados_src, src_d, 3);
	memcpy( dados_dst, dst_d, 3);

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS aux_token_stasta(char *strout)
 * Genera el status en un array
 * @param strout Array que va a contener el status
 */
TEG_STATUS aux_token_stasta(char *strout)
{
	int n;
	char strtmp[ PLAYERNAME_MAX_LEN + 200];

	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR j;

	strout[0]=0;

	n=0;
	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		j = (PJUGADOR) l;

		if( j->is_player ) {
			int color = (j->color==-1) ? TEG_MAX_PLAYERS : j->color;
			if(n==0) {
				snprintf(strtmp,sizeof(strtmp)-1,"%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s",j->nombre,color,j->player_stats.score,j->numjug,j->estado,j->tot_paises,j->tot_ejercitos,j->tot_tarjetas,(g_juego.empieza_turno && (g_juego.empieza_turno->numjug==j->numjug)),j->human,j->addr );
				n=1;
			} else 
				snprintf(strtmp,sizeof(strtmp)-1,"/%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s",j->nombre,color,j->player_stats.score,j->numjug,j->estado,j->tot_paises,j->tot_ejercitos,j->tot_tarjetas,(g_juego.empieza_turno && (g_juego.empieza_turno->numjug==j->numjug)),j->human,j->addr );

			strtmp[ sizeof(strtmp) -1 ] = 0;

			strcat(strout,strtmp);
		}

		l = l->Flink;
	}
	return TEG_STATUS_SUCCESS;
}


/**
 * @fn PPAIS get_random_pais( get_random_func func )
 * Busca un pais al azar
 * @param func Funcion a llamar que pone restricciones al pais encontrado
 * @return PPAIS Pais encontrado
 */
PPAIS get_random_pais( get_random_func func )
{
	int i;

again:
	i = RANDOM_MAX(0,PAISES_CANT-1);
	if( func(i) )
		return &g_paises[i];
	else {
		int r = RANDOM_MAX(0,2);
		switch(r) {
			/* search going down */
			case 0:
				for(;i>=0;i--) {
					if( func(i) )
						return &g_paises[i];
				}
				for(i=PAISES_CANT-1;i>=0;i--) {
					if( func(i) )
						return &g_paises[i];
				}
				PDEBUG("unpaiselibre(down): Abnormal error\n");
				break;
			/* search going up */
			case 1:
				for(;i<PAISES_CANT;i++) {
					if( func(i))
						return &g_paises[i];
				}
				for(i=0;i<PAISES_CANT;i++) {
					if( func(i))
						return &g_paises[i];
				}
				PDEBUG("unpaislibre(up):Abnormal error\n");
				break;
			/* search again */
			case 2:
			default:
				goto again;
		}
	}

	/* to prevent warnings at compile time */
	return &g_paises[0];
}

/**
 * @fn TEG_STATUS aux_token_fichasc( PJUGADOR pJ )
 * Sends the player the total number of armies it must place, and the continents
 * he has conquered
 */
TEG_STATUS aux_token_fichasc( PJUGADOR pJ )
{
	unsigned long conts=0;
	int armies;
	int x_canje=0;

	assert( pJ );

	if( pJ->fichasc_armies ) {
		conts = pJ->fichasc_conts;
		armies = pJ->fichasc_armies;
	} else {
		if( jugador_listar_conts( pJ, &conts ) != TEG_STATUS_SUCCESS )
			return TEG_STATUS_UNEXPECTED;
		armies = jugador_fichasc_cant( pJ );

		pJ->fichasc_conts = conts;
		pJ->fichasc_armies = armies;
	}

	if( pJ->hizo_canje )
		x_canje = cuantos_x_canje( pJ->tot_canjes );

	pJ->estado = JUG_ESTADO_FICHASC;
	netall_printf( TOKEN_FICHASC"=%d,%d,%d\n",pJ->numjug,conts,armies + x_canje );

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS fichas_next( void )
 * Pasa el token fichas al siguiente
 */
TEG_STATUS fichas_next( void )
{
	turno_2nextplayer( &g_juego.turno );
	if(  turno_is_round_complete() ) {
		/* ya di la vuelta */
		g_juego.turno->estado = JUG_ESTADO_FICHAS2;
		netall_printf( TOKEN_FICHAS2"=%d,%d\n",g_juego.turno->numjug,g_juego.fichas2);

	} else {
		/* paso el estado fichas al siguiente */
		g_juego.turno->estado = JUG_ESTADO_FICHAS;
		netall_printf( TOKEN_FICHAS"=%d,%d\n",g_juego.turno->numjug,g_juego.fichas);
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS fichas2_next( void )
 * Pasa el token fichas2 al siguiente
 */
TEG_STATUS fichas2_next( void )
{
	turno_2nextplayer( &g_juego.turno );
	if(  turno_is_round_complete() ) {
		/* ya di la vuelta, enconces a empezar a jugar  */
		g_juego.turno->estado = JUG_ESTADO_ATAQUE;
		netall_printf( TOKEN_TURNO"=%d\n",g_juego.turno->numjug);

	} else {
		/* paso el estado fichas2 al siguiente */
		g_juego.turno->estado = JUG_ESTADO_FICHAS2;
		netall_printf( TOKEN_FICHAS2"=%d,%d\n",g_juego.turno->numjug,g_juego.fichas2);
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS fichasc_next( void )
 * Pasa el token fichasc al siguiente
 */
TEG_STATUS fichasc_next( void )
{
	turno_2nextplayer( &g_juego.turno );

	if(  turno_is_round_complete() ) {
		/* ya di la vuelta */
		g_juego.turno->estado = JUG_ESTADO_ATAQUE;
		netall_printf( TOKEN_TURNO"=%d\n",g_juego.turno->numjug);

	} else {
		aux_token_fichasc( g_juego.turno );
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS aux_find_inaddr( PJUGADOR pJ )
 */
TEG_STATUS aux_find_inaddr( PJUGADOR pJ )
{
	struct sockaddr *sa;
	socklen_t slen = 128;

	assert(pJ);

	if( pJ->fd <= 0)
		return TEG_STATUS_ERROR;

	if( (sa=malloc(slen)) == NULL )
		return TEG_STATUS_ERROR;

	if( getpeername( pJ->fd, sa, &slen ) == -1) {
		con_text_out(M_ERR,"Error in getpeername()\n");
		strncpy(pJ->addr, _("Unknown"), sizeof(pJ->addr)-1);
		pJ->addr[sizeof(pJ->addr)-1]=0;

		free(sa);
		return TEG_STATUS_ERROR;
	}

	switch(sa->sa_family) {
	case AF_INET: {
		struct sockaddr_in *sin = (struct sockaddr_in*) sa;
		inet_ntop( AF_INET, &sin->sin_addr,pJ->addr, sizeof(pJ->addr)-1);
		break;
		}
	case AF_INET6: {
		struct sockaddr_in6 *sin6 = (struct sockaddr_in6*) sa;
		inet_ntop( AF_INET6, &sin6->sin6_addr,pJ->addr, sizeof(pJ->addr)-1);
		break;
		}
	case AF_UNIX: {
		struct sockaddr_un *unp = (struct sockaddr_un *) sa;
		if(unp->sun_path[0]==0)
			strncpy(pJ->addr,_("Unknown"),sizeof(pJ->addr)-1);
		else
			snprintf(pJ->addr, sizeof(pJ->addr)-1, "%s", unp->sun_path);
		break;
		}
	default:
		strncpy(pJ->addr,_("Unknown"),sizeof(pJ->addr)-1);
		break;
	}

	pJ->addr[sizeof(pJ->addr)-1]=0;

	strip_invalid(pJ->addr);

	free(sa);

	return TEG_STATUS_SUCCESS;
}

TEG_STATUS aux_token_countries( PJUGADOR pJ, char *buf, int buflen )
{
	PLIST_ENTRY pL;
	PPAIS pais;
	int n=0;
	char strtmp[100];

	assert(buf);

	memset(buf,0,buflen);

	pL = pJ->paises.Flink;
	while( !IsListEmpty( &pJ->paises ) && (pL != &pJ->paises) ) {
		pais = (PPAIS) pL;

		if(n==0) {
			snprintf(strtmp,sizeof(strtmp)-1,"%i:%d",pais->id,pais->ejercitos);
			n=1;
		} else 
			snprintf(strtmp,sizeof(strtmp)-1,",%i:%d",pais->id,pais->ejercitos);

		strtmp[ sizeof(strtmp) -1 ] = 0;

		strncat(buf,strtmp,buflen);
		pL = LIST_NEXT(pL);
	}
	buf[buflen]=0;

	return TEG_STATUS_SUCCESS;
}

