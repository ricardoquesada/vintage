/*	$Id: play.c,v 1.100 2001/12/22 22:03:40 tomk32 Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <riq@corest.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file play.c
 * Controls all the messages that receives the server
 */

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"
#include "tegggz.h"
#include "xmlscores.h"

#undef DEBUG_PLAY

#ifdef DEBUG_PLAY
# define PLAY_DEBUG(x...) PDEBUG(x)
# define STATIC
#else
# define PLAY_DEBUG(x...)
# define STATIC static
#endif


STATIC TEG_STATUS token_status(int, char *);
STATIC TEG_STATUS token_test(int, char *);
STATIC TEG_STATUS token_sversion(int, char *);
STATIC TEG_STATUS token_cversion(int, char *);
STATIC TEG_STATUS token_pversion(int, char *);
STATIC TEG_STATUS token_playerid(int, char *);
STATIC TEG_STATUS token_help(int, char *);
STATIC TEG_STATUS token_countries(int, char *);
STATIC TEG_STATUS token_fichas(int, char* );
STATIC TEG_STATUS token_fichas2(int, char *);
STATIC TEG_STATUS token_fichasc(int, char *);
STATIC TEG_STATUS token_attack(int , char *);
STATIC TEG_STATUS token_qumm(int fd, char *);
STATIC TEG_STATUS token_rem( int fd, char *);
STATIC TEG_STATUS token_message(int fd, char *);
STATIC TEG_STATUS token_tropas( int fd, char *str);
STATIC TEG_STATUS token_turn( int fd, char *str);
STATIC TEG_STATUS token_card( int fd, char *str );
STATIC TEG_STATUS token_regroup(int, char *);
STATIC TEG_STATUS token_canje(int, char*);
STATIC TEG_STATUS token_ejer2(int, char *);
STATIC TEG_STATUS token_mission(int, char *);
STATIC TEG_STATUS token_color(int, char*);
STATIC TEG_STATUS token_loque(int, char*);
STATIC TEG_STATUS token_echo(int, char*);
STATIC TEG_STATUS token_surrender(int, char*);
STATIC TEG_STATUS token_set(int, char*);
STATIC TEG_STATUS token_scores(int, char*);
STATIC TEG_STATUS token_enum_cards(int, char*);

struct {
	char *label;
	TEG_STATUS (*func) ();
	char *help;
} tokens[] = {
	{ TOKEN_START,		token_start,	N_("to start the game") },
	{ TOKEN_STATUS,		token_status,	N_("shows the status of the players") },
	{ TOKEN_MESSAGE,	token_message,	N_("to send a message") },
	{ TOKEN_EXIT,		token_exit,	N_("to exit the game") },
	{ TOKEN_TEST,		token_test,	N_("internal use. Dont use it.") },
	{ TOKEN_CVERSION,	token_cversion,	N_("client version") },
	{ TOKEN_SVERSION,	token_sversion,	N_("server version") },
	{ TOKEN_PVERSION,	token_pversion,	N_("protocol version") },
	{ TOKEN_PLAYERID,	token_playerid,	N_("to register as a player") },
	{ TOKEN_HELP,		token_help,	N_("to ask for help") },
	{ TOKEN_REM,		token_rem,	N_("to comment a command") },
	{ TOKEN_QUMM,		token_qumm,	N_("It makes you more happy") },
	{ TOKEN_PAISES,		token_countries,	N_("It shows info about the countries") },
	{ TOKEN_FICHAS,		token_fichas,	N_("to place the initials 5 armies") },
	{ TOKEN_FICHAS2,	token_fichas2,	N_("to place the initials 3 armies") },
	{ TOKEN_FICHASC,	token_fichasc,	N_("to place the armies after a turn have finished") },
	{ TOKEN_ATAQUE,		token_attack,	N_("to attack a country") },
	{ TOKEN_TROPAS,		token_tropas,	N_("to send armies to a conquered country") },
	{ TOKEN_TARJETA,	token_card,	N_("to pick up a country-card") },
	{ TOKEN_REAGRUPE,	token_regroup,	N_("to reorder your armies") },
	{ TOKEN_TURNO,		token_turn,	N_("to finish your turn") },
	{ TOKEN_CANJE,		token_canje,	N_("to exchange your cards for armies") },
	{ TOKEN_EJER2,		token_ejer2,	N_("to place 2 armies in a country. You must have that card") },
	{ TOKEN_OBJETIVO,	token_mission,	N_("request a mission") },
	{ TOKEN_COLOR,		token_color,	N_("to select a color") },
	{ TOKEN_LOQUE,		token_loque,	N_("to remind me what to do") },
	{ TOKEN_ECHO,		token_echo,	N_("to set an async callback") },
	{ TOKEN_SURRENDER,	token_surrender,N_("to surrender") },
	{ TOKEN_SET,		token_set,	N_("to set options") },
	{ TOKEN_SCORES,		token_scores,	N_("to show the highscores") },
	{ TOKEN_ENUM_CARDS,	token_enum_cards,N_("to show the cards a player has") },
};
#define	NTOKENS  (sizeof(tokens)/sizeof(tokens[0]))


/**
 * @fn STATIC TEG_STATUS token_set( int fd, char *str )
 * Sets a server option
 */
STATIC TEG_STATUS token_set( int fd, char *str )
{
	if( strlen(str)==0 )
		goto error;

	if( option_parse(fd, str) == TEG_STATUS_SUCCESS )
		return TEG_STATUS_SUCCESS;

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_SET"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_echo( int fd, char *msg )
 * Sends the player the message he requested. Usefull as callback
 */
STATIC TEG_STATUS token_echo( int fd, char *msg )
{
	PJUGADOR pJ;
	if( jugador_whoisfd(fd, &pJ )!=TEG_STATUS_SUCCESS || strlen(msg)==0 )
		goto error;

	net_printf(fd,"%s\n",msg);
	return TEG_STATUS_SUCCESS;

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_ECHO"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_loque( int fd, char *unused )
 * Sends the player the actions he must do. Usefull as callback
 * TODO: finish the rest of 'loque'
 */
STATIC TEG_STATUS token_loque( int fd, char *unused )
{
	PJUGADOR pJ;
	PLAY_DEBUG("token_loque()");
	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS ) {
		goto error;
	}

	switch( pJ->estado ) {
	case JUG_ESTADO_FICHAS:
		net_printf( fd, TOKEN_FICHAS"=%d,%d\n",g_juego.turno->numjug,g_juego.fichas);
		break;
	case JUG_ESTADO_FICHAS2:
		net_printf( fd, TOKEN_FICHAS2"=%d,%d\n",g_juego.turno->numjug,g_juego.fichas);
		break;
	case JUG_ESTADO_FICHASC:
	case JUG_ESTADO_CANJE:
		aux_token_fichasc( g_juego.turno );
		break;
	case JUG_ESTADO_ATAQUE:
	case JUG_ESTADO_TROPAS:
		net_printf( fd, TOKEN_TURNO"=%d\n",g_juego.turno->numjug );
		break;
	default:
		break;
	}

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_LOQUE"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_color( int fd, char *str )
 * Assigns the player a color
 * @param fd player's FileDescriptor
 * @param str wished color
 */
STATIC TEG_STATUS token_color( int fd, char *str )
{
	int color;
	int a;
	PJUGADOR pJ;
	PLAY_DEBUG("token_color()");

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS ) {
		goto error;
	}

	if(strlen(str)==0)
		goto error;

	if( pJ->estado != JUG_ESTADO_CONNECTED )
		goto error;

	if( pJ->is_player == FALSE )
		goto error;

	a = atoi( str );
	if (a < 0 ||  a >= TEG_MAX_PLAYERS )
		goto error;

	color = a;
	if ( color_libre( &color )  == FALSE )
		goto error;

	pJ->estado = JUG_ESTADO_HABILITADO;
	pJ->color = color;
	con_text_out(M_INF,_("Player %s(%d) has color %s\n"),pJ->nombre,pJ->numjug,_(g_colores[color]));

	netall_printf( TOKEN_NEWPLAYER"=%s,%d,%d\n", pJ->nombre, pJ->numjug, pJ->color );
	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_COLOR"\n");
	return TEG_STATUS_PARSEERROR;
}


/**
 * @fn STATIC TEG_STATUS token_mission( int fd )
 * The player is asking a mission
 * @param fd FileDescriptor del jugador
 */
STATIC TEG_STATUS token_mission( int fd, char *unused )
{
	PJUGADOR pJ;
	PLAY_DEBUG("token_mission()");

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS ) {
		goto error;
	}

	if( pJ->estado < JUG_ESTADO_START ) {
		goto error;
	}

	if( objetivo_asignar( pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	net_printf( fd, TOKEN_OBJETIVO"=%d\n",pJ->objetivo);
	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_OBJETIVO"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_turn( int fd, char *unused )
 * Called when a player finish his turn
 * @param fd FileDescriptor of the player
 */
STATIC TEG_STATUS token_turn( int fd, char *unused )
{
	PJUGADOR pJ;
	PLAY_DEBUG("token_turn()");

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS ) {
		goto error;
	}

	if( pJ->estado < JUG_ESTADO_TURNOSTART || pJ->estado > JUG_ESTADO_TURNOEND )
		goto error;

	if( pJ != g_juego.turno ) {
		con_text_out(M_ERR,_("BUG: The server believes that player `%s' does not have the turn"),pJ->nombre);
		goto error;
	}

	pJ->estado = JUG_ESTADO_IDLE;

	if( turno_end( pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	/* give turn to the next player */
	if( turno_next() != TEG_STATUS_SUCCESS )
		goto error;

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_TURNO"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_message( int fd, char *msg )
 * Sends all the player a message. An observer may use this function
 * @param fd FileDescriptor del jugador
 * @param msg Mensaje a enviar
 */
STATIC TEG_STATUS token_message( int fd, char *msg )
{
	PJUGADOR j;
	PLAY_DEBUG("token_message()\n");

	if( jugador_whoisfd(fd, &j )!=TEG_STATUS_SUCCESS || strlen(msg)==0 )
		goto error;

	strip_invalid_msg(msg);

	if( j->is_player )
		netall_printf(TOKEN_MESSAGE"=%s,%d,\"%s\"\n",j->nombre,j->numjug,msg);
	else
		netall_printf(TOKEN_MESSAGE"=observer-%s,%d,\"%s\"\n",j->nombre,j->numjug,msg);
	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_MESSAGE"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_rem( int fd, char *unused )
 * mostly-Useless function
 * @param fd FileDescriptor del jugador
 */
STATIC TEG_STATUS token_rem( int fd, char *unused )
{
	PLAY_DEBUG("token_rem()\n");

	if( !JUGADOR_HABILITADO(fd) )
		goto error;

	net_printf(fd,TOKEN_REM"=%s\n",_("Para que me envias un rem?"));
	return TEG_STATUS_SUCCESS;
error:
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_qumm( int fd, char *str )
 * I want a better world "Quiero Un Mundo Mejor"
 * @param fd FileDescriptor del jugador
 * @param str Parametros de qumm
 */
STATIC TEG_STATUS token_qumm( int fd, char *str )
{
	PLAY_DEBUG("token_qumm()\n");

	if( !JUGADOR_HABILITADO(fd) )
		goto error;

	net_printf(fd,TOKEN_REM"=%s\n",_("Yo tambien quiero un mundo mejor!"));
	return TEG_STATUS_SUCCESS;
error:
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_playerid( int fd, char *str )
 * Creates a Player
 * @param fd File descriptor
 * @param str Has 'name,mode,human'. Returns 'name,player_number'
 */
STATIC TEG_STATUS token_playerid( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	JUGADOR j, *pJ;
	char c[TEG_MAX_PLAYERS];
	char colores[100];
	int i;
	int reconnect = FALSE;

	PLAY_DEBUG("token_playerid()\n");

	/* si existe entonces da error, porque no tiene que existir */
	if( jugador_whoisfd(fd, &pJ ) == TEG_STATUS_SUCCESS )
		goto error;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	memset( &j, 0, sizeof(JUGADOR));

	/* averigua el nombre */
	if( parser_call( &p ) && p.hay_otro ) {
#ifdef WITH_GGZ
		if(g_server.with_ggz) {
			if( tegggz_find_ggzname(fd,j.nombre,sizeof(j.nombre)-1) != TEG_STATUS_SUCCESS ) {
				jugador_fillname( &j, p.token );
			}
		} else
#endif /* WITH_GGZ */
			jugador_fillname( &j, p.token );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		j.is_player = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		j.human = atoi( p.token );		
	} else goto error;


	if( j.is_player ) {
		if( JUEGO_EMPEZADO ) {
			if( ! (reconnect = jugador_is_disconnected(&j)) ) {
				net_printf(fd,TOKEN_GAMEINPROGRESS"\n");
				fd_remove(fd);
				return TEG_STATUS_CONNCLOSED;
			}
		}
		if( reconnect )
			pJ = jugador_return_disconnected( &j );
		else
			pJ = jugador_ins_player( &j );
	} else
		pJ = jugador_ins_ro( &j );


	if( pJ == NULL ) {
		net_printf(fd,TOKEN_SERVERFULL "\n");
		fd_remove(fd);
		return TEG_STATUS_CONNCLOSED;
	}

	pJ->fd = fd;

	aux_find_inaddr( pJ );

	if( reconnect ) {

		pJ->estado = pJ->status_before_discon;

		net_printf(fd,TOKEN_RECONNECT"=%s,%d,%d\n",pJ->nombre,pJ->numjug,pJ->color);
		con_text_out(M_INF,_("Player %s(%d) is re-connected from %s\n"),pJ->nombre,pJ->numjug,pJ->addr);

	} else {
		colores_libres( c );
		memset(colores,0,sizeof(colores));

		for(i=0;i<TEG_MAX_PLAYERS;i++) {
			char buf[100];
			sprintf( buf, ",%d",c[i] );
			strncat( colores, buf, sizeof(colores)-1 );
		}

		net_printf(fd,TOKEN_PLAYERID"=%s,%d%s\n",pJ->nombre,pJ->numjug,colores);

		con_text_out(M_INF,_("Player %s(%d) is connected from %s\n"),pJ->nombre,pJ->numjug,pJ->addr);
	}
	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_PLAYERID"\n");
	return TEG_STATUS_PARSEERROR;
}

STATIC TEG_STATUS token_cversion( int fd, char *str )
{
	con_text_out(M_INF,_("Using client version: %s\n"),str);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn STATIC TEG_STATUS token_fichas( int fd, char *str )
 * Parses the armies the player must place for the 1st time
 * @param fd file descriptor del jugador
 * @param str datos que envia el jugador
 * @return TEG_STATUS_SUCCESS si todo salio bien
 */
STATIC TEG_STATUS token_fichas( int fd, char *str )
{
	PLAY_DEBUG("token_fichas()\n");

	if( !JUGADOR_FICHAS(fd) )
		goto error;

	if( aux_token_fichas( fd, str, g_juego.fichas, 0) == TEG_STATUS_SUCCESS ) {

		g_juego.turno->estado = JUG_ESTADO_POSTFICHAS;

		fichas_next();
		return TEG_STATUS_SUCCESS;
	}

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_FICHAS"\n");
	return TEG_STATUS_PARSEERROR;
}


/**
 * @fn STATIC TEG_STATUS token_fichas2( int fd, char *str )
 * Parses the armies the player must place for the 2nd time
 * @param fd file descriptor del jugador
 * @param str datos que envia el jugador
 * @return TEG_STATUS_SUCCESS si todo salio bien
 */
STATIC TEG_STATUS token_fichas2( int fd, char *str )
{
	PLAY_DEBUG("token_fichas2()\n");

	if( !JUGADOR_FICHAS2(fd) )
		goto error;
	
	if( aux_token_fichas( fd, str, g_juego.fichas2, 0) == TEG_STATUS_SUCCESS ) {

		g_juego.turno->estado = JUG_ESTADO_POSTFICHAS2;

		fichas2_next();
		return TEG_STATUS_SUCCESS;
	}
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_FICHAS2"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_fichasc( int fd, char *str )
 * Parses the armies the player must place the rest of the times
 * @param fd file descriptor del jugador
 * @param str datos que envia el jugador
 * @return TEG_STATUS_SUCCESS si todo salio bien
 */
STATIC TEG_STATUS token_fichasc( int fd, char *str )
{
	PJUGADOR pJ;
	int total_armies;
	PLAY_DEBUG("token_fichasc()\n");

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	if( pJ->estado != JUG_ESTADO_FICHASC && pJ->estado != JUG_ESTADO_CANJE )
		goto error;

	total_armies = pJ->fichasc_armies + cont_tot(pJ->fichasc_conts);

	if( pJ->hizo_canje )
		total_armies += cuantos_x_canje( pJ->tot_canjes );

	if( aux_token_fichas( fd, str, total_armies, pJ->fichasc_conts) == TEG_STATUS_SUCCESS ) {

		pJ->estado = JUG_ESTADO_IDLE;

		fichasc_next();

		pJ->hizo_canje = FALSE;
		pJ->fichasc_armies = 0;
		pJ->fichasc_conts = 0;

		return TEG_STATUS_SUCCESS;
	}

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_FICHASC"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_attack( int fd, char *str )
 * A player is attacking from src to dst
 * @param fd File descriptor del jugador
 * @param str string que contiene 'pais_src,pais_dst'
 */
STATIC TEG_STATUS token_attack( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int src,dst,src_lost,dst_lost;
	char d_src[3],d_dst[3];
	PJUGADOR pJ_src, pJ_dst;
	int conq = 0;
	int tropas = 0;

	PLAY_DEBUG("token_attack()\n");

	if( strlen(str)==0)
		goto error;

	if( !JUGADOR_ATAQUE_P(fd,&pJ_src)) {
		if( JUGADOR_TROPAS_P(fd,&pJ_src)) {
			pJ_src->estado=JUG_ESTADO_ATAQUE;
			pJ_src->pais_src = pJ_src->pais_dst = -1;
		} else goto error;
	}

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		src = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		dst = atoi( p.token );		
	} else goto error;

	if( src >= PAISES_CANT || src < 0 || dst >= PAISES_CANT || dst < 0) {
		goto error;
	}

	if( pJ_src->numjug != g_paises[src].numjug || pJ_src->numjug == g_paises[dst].numjug )  {
		goto error;
	}

	if( g_paises[src].ejercitos < 2 || !paises_eslimitrofe( src, dst) ) {
		goto error;
	}

	if( jugador_whois( g_paises[dst].numjug, &pJ_dst ) != TEG_STATUS_SUCCESS ){
		goto error;
	}

	if( pactos_attack( src, dst ) != TEG_STATUS_SUCCESS )
		goto error;

	/* aviso a todos que hay un attack */
	netall_printf( TOKEN_ATAQUE"=%d,%d\n",src,dst );

	/* so far, attack... */
	aux_token_attack( g_paises[src].ejercitos, g_paises[dst].ejercitos, &src_lost, &dst_lost, d_src, d_dst );

	g_paises[src].ejercitos -= src_lost;
	g_paises[dst].ejercitos -= dst_lost;
	pJ_src->tot_ejercitos -= src_lost;
	pJ_dst->tot_ejercitos -= dst_lost;

	/* updated statistics */
	pJ_src->player_stats.armies_won += dst_lost;
	pJ_src->player_stats.armies_lost += src_lost;

	pJ_dst->player_stats.armies_won += src_lost;
	pJ_dst->player_stats.armies_lost += dst_lost;

	/* conquisto el pais */
	if( g_paises[dst].ejercitos == 0) {
		PLIST_ENTRY l;

		conq = 1;

		pJ_src->turno_conq++;
		pJ_src->tot_paises++;

		
		g_paises[dst].numjug = pJ_src->numjug;

		g_paises[dst].ejercitos++;		/* se pasa automaticamente */
		g_paises[src].ejercitos--;		/* un ejercito */

		tropas = g_paises[src].ejercitos - 1;	/* cantidad que se pueden pasar */
		if( tropas > 2 )			/* En verdad son 3, pero ya se le paso 1 */
			tropas =2;

		pJ_src->estado = JUG_ESTADO_TROPAS;
		pJ_src->pais_src = src;
		pJ_src->pais_dst = dst;
	
		pJ_dst->tot_paises--;

		l= RemoveHeadList( g_paises[dst].next.Blink );
		InsertTailList( &pJ_src->paises, l);

		/* updated statistics */
		pJ_src->player_stats.countries_won ++;
		pJ_dst->player_stats.countries_lost ++;
	}

	/* update the scores */
	stats_score( &pJ_src->player_stats );
	stats_score( &pJ_dst->player_stats );

	/* tell everybody the result of the attack */
	netall_printf(TOKEN_DADOS"=%d,%d,%d,%d,%d,%d,%d,%d;"TOKEN_PAIS"=%d,%d,%d;"TOKEN_PAIS"=%d,%d,%d\n",
			src,d_src[0],d_src[1],d_src[2], dst,d_dst[0],d_dst[1],d_dst[2],
			src,g_paises[src].numjug,g_paises[src].ejercitos,
			dst,g_paises[dst].numjug,g_paises[dst].ejercitos
			);

	if( conq == 1 ) {

		/* Did 'dst' player lose the game ? */
		if( jugador_is_lost( pJ_dst ) ) {
			con_text_out(M_INF,_("Player %s(%d) lost the game\n"),pJ_dst->nombre,pJ_dst->numjug);
			netall_printf( TOKEN_LOST"=%d\n",pJ_dst->numjug );
			jugador_poner_perdio(pJ_dst);
		}

		/* Did 'src' player win the game ? */
		if( objetivo_chequear( pJ_src ) == TEG_STATUS_GAMEOVER || game_is_finished() ) {
			con_text_out(M_INF,_("Player %s(%d) is the winner! Game Over\n"),pJ_src->nombre,pJ_src->numjug);
			pJ_src->estado = JUG_ESTADO_GAMEOVER;
			game_end( pJ_src );
			return TEG_STATUS_SUCCESS;
		}

		net_printf(fd,TOKEN_TROPAS"=%d,%d,%d\n",src,dst,tropas);
	}

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_ATAQUE"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_tropas( int fd, char *str )
 * Place armies in the conquered country
 */
STATIC TEG_STATUS token_tropas( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int src,dst,cant;
	PJUGADOR j;

	PLAY_DEBUG("token_tropas()\n");

	if( !JUGADOR_TROPAS_P(fd,&j) || strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		src = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		dst = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		cant = atoi( p.token );		
	} else goto error;

	if( src >= PAISES_CANT || src < 0 || dst >= PAISES_CANT || dst < 0) {
		goto error;
	}

	if( src != j->pais_src || dst != j->pais_dst )
		goto error;

	if( cant > g_paises[src].ejercitos-1 || cant > 3)
		goto error;

	if( cant > 0 ) {
		g_paises[src].ejercitos -= cant;
		g_paises[dst].ejercitos += cant;

		j->estado=JUG_ESTADO_ATAQUE;
		j->pais_src = j->pais_dst = -1;

		netall_printf(TOKEN_PAIS"=%d,%d,%d;"TOKEN_PAIS"=%d,%d,%d\n",
				src,g_paises[src].numjug,g_paises[src].ejercitos,
				dst,g_paises[dst].numjug,g_paises[dst].ejercitos
				);
	}


	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_TROPAS"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_card( int fd, char *str )
 * Ask for a card after the attack
 * @param fd File descriptor del jugador
 * @param str Parametros recibidos
 */
STATIC TEG_STATUS token_card( int fd, char *str )
{
	PJUGADOR j;
	PPAIS pP;

	PLAY_DEBUG("token_card()\n");


	/* Veo si puede sacar una tarjeta... */
	if( jugador_whoisfd( fd, &j ) != TEG_STATUS_SUCCESS )
		goto error;

	if( j->estado < JUG_ESTADO_TURNOSTART || j->estado >= JUG_ESTADO_TARJETA)
		goto error;

	if( j->tot_tarjetas >= TEG_MAX_TARJETAS )
		goto error;

	if( j->turno_conq < 1 )
		goto error;

	if( j->tot_canjes > 3 && j->turno_conq < 2 )
		goto error;


	/* Puede sacar tarjeta */
	j->tot_tarjetas++;
	pP = get_random_pais( tarjeta_es_libre );

	j->estado = JUG_ESTADO_TARJETA;

	tarjeta_sacar( &pP->tarjeta, j->numjug );

	/*
	 * Me fijo si el jugador es due�o del pa�s que dice la tarjeta. Si es as�
	 * le agrego 2 fichas automaticamente como dice el reglamento.
	 */
	if( pP->numjug == j->numjug ) {
		pP->ejercitos += 2;
		j->tot_ejercitos += 2;
		tarjeta_usar( &pP->tarjeta );
		netall_printf(TOKEN_PAIS"=%d,%d,%d\n",pP->id,pP->numjug,pP->ejercitos);
	} 
	net_printf(fd,TOKEN_TARJETA"=%d,%d\n",pP->id,pP->tarjeta.usada);

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_TARJETA"\n");
	return TEG_STATUS_ERROR;
}

STATIC TEG_STATUS token_enum_cards( int fd, char *str )
{
	PJUGADOR pJ;
	int i,first;
	char buffer[1024];

	PLAY_DEBUG("token_enum_cards()\n");


	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	memset( buffer, 0, sizeof(buffer) );

	first=1;
	for(i=0;i<PAISES_CANT;i++) {
		if( g_paises[i].tarjeta.numjug == pJ->numjug ) {
			char buffy[512];

			memset(buffy,0,sizeof(buffy));

			if( first ) {
				snprintf( buffy, sizeof(buffy)-1, "%d:%d", i, g_paises[i].tarjeta.usada );
				strncat( buffer, buffy, sizeof(buffer)-1 );
				first = 0;
			} else {
				snprintf( buffy, sizeof(buffy)-1, ",%d:%d", i, g_paises[i].tarjeta.usada );
				strncat( buffer, buffy, sizeof(buffer)-1 );
			}
		}
	}

	net_printf(fd,TOKEN_ENUM_CARDS"=%s\n",buffer);

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_ENUM_CARDS"\n");
	return TEG_STATUS_ERROR;

}

/**
 * @fn STATIC TEG_STATUS token_ejer2( int fd, char *str )
 * Place 2 armies in the card's country. The player must own the country
 */
STATIC TEG_STATUS token_ejer2( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int pais;
	PJUGADOR j;

	PLAY_DEBUG("token_ejer2()\n");

	if( strlen(str)==0)
		goto error;

	if( jugador_whoisfd( fd, &j ) != TEG_STATUS_SUCCESS )
		goto error;

	if( j->estado != JUG_ESTADO_TARJETA )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && !p.hay_otro ) {
		pais = atoi( p.token );		
	} else goto error;

	if( pais >= PAISES_CANT || pais < 0 ) {
		goto error;
	}

	if( tarjeta_es_usada( &g_paises[ pais ].tarjeta ))
		goto error;

	if( g_paises[ pais ].numjug == j->numjug && g_paises[ pais ].tarjeta.numjug == j->numjug ) {
		g_paises[ pais ].ejercitos += 2;
		j->tot_ejercitos += 2;
		tarjeta_usar( &g_paises[ pais ].tarjeta );
		netall_printf(TOKEN_PAIS"=%d,%d,%d\n"
				,pais
				,g_paises[ pais ].numjug
				,g_paises[ pais ].ejercitos);
	} 

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_EJER2"\n");
	return TEG_STATUS_PARSEERROR;
}


/**
 * @fn STATIC TEG_STATUS token_canje( int fd, char *str )
 * To exchange cards for armies
 */
STATIC TEG_STATUS token_canje( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	PJUGADOR pJ;
	int t1,t2,t3;
	int canj_ejer;

	PLAY_DEBUG("token_canje()\n");

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	if( pJ->estado != JUG_ESTADO_FICHASC )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		t1 = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		t2 = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		t3 = atoi( p.token );		
	} else goto error;

	/* se puede hacer el canje ? */
	if( !tarjeta_puedocanje( pJ->numjug, t1, t2, t3 ) )
		goto error;

	pJ->estado = JUG_ESTADO_CANJE;

	pJ->hizo_canje = TRUE;
	pJ->tot_canjes++;
	pJ->tot_tarjetas -= 3;

	canj_ejer = cuantos_x_canje( pJ->tot_canjes );

	/* quitarle las tarjetas al jugador */
	tarjeta_poner( &g_paises[t1].tarjeta );
	tarjeta_poner( &g_paises[t2].tarjeta );
	tarjeta_poner( &g_paises[t2].tarjeta );

	netall_printf(TOKEN_CANJE"=%d,%d,%d,%d,%d\n",
			pJ->numjug,canj_ejer,t1,t2,t3);
	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_CANJE"\n");
	return TEG_STATUS_PARSEERROR;
}


/**
 * @fn STATIC TEG_STATUS token_regroup( int fd, char *str )
 * Player is regrouping its armies
 * @param fd File descriptor del jugador
 * @param str Parametros recibidos
 */
STATIC TEG_STATUS token_regroup( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int src,dst,cant;
	int ejer_disp;
	PJUGADOR pJ;

	PLAY_DEBUG("token_regroup()\n");


	if( strlen(str)==0)
		goto error;

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	if( pJ->estado < JUG_ESTADO_TURNOSTART || pJ->estado > JUG_ESTADO_REAGRUPE)
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;


	if( parser_call( &p ) && p.hay_otro ) {
		src = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && p.hay_otro ) {
		dst = atoi( p.token );
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		cant = atoi( p.token );
	} else goto error;

	if( src >= PAISES_CANT || src < 0 || dst >= PAISES_CANT || dst < 0 || cant <= 0) {
		goto error;
	}

	if( g_paises[src].numjug != pJ->numjug || g_paises[dst].numjug != pJ->numjug ) {
		goto error;
	}

	if( !paises_eslimitrofe( src, dst ))
		goto error;

	ejer_disp = g_paises[src].ejercitos - g_paises[src].ejer_reagrupe - 1;
	if( cant > ejer_disp )
		goto error;

	pJ->estado = JUG_ESTADO_REAGRUPE;

	g_paises[dst].ejercitos += cant;
	g_paises[dst].ejer_reagrupe += cant;
	g_paises[src].ejercitos -= cant;

	netall_printf(TOKEN_PAIS"=%d,%d,%d;"TOKEN_PAIS"=%d,%d,%d\n",
		src,g_paises[src].numjug,g_paises[src].ejercitos,
		dst,g_paises[dst].numjug,g_paises[dst].ejercitos
	);

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_REAGRUPE"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_status( int fd, char &unused )
 * Show the status of all the players
 */
STATIC TEG_STATUS token_status( int fd, char *unused )
{
	char strout[PROT_MAX_LEN + PLAYERNAME_MAX_LEN * TEG_MAX_PLAYERS + 200];

	PLAY_DEBUG("token_status()\n");

	if( !JUGADOR_CONNECTED( fd ))
		goto error;

	if( aux_token_stasta(strout) != TEG_STATUS_SUCCESS )
		goto error;

	net_printf(fd,TOKEN_STATUS"=%s\n",strout);

	return TEG_STATUS_SUCCESS;

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_STATUS"\n");
	return TEG_STATUS_PARSEERROR;
}

STATIC TEG_STATUS token_scores( int fd, char *unused )
{
	char strout[PROT_MAX_LEN + PLAYERNAME_MAX_LEN * TEG_MAX_PLAYERS + 200];

	PLAY_DEBUG("token_scores()\n");

	if( !JUGADOR_CONNECTED( fd ))
		goto error;

	if( scores_dump(strout) != TEG_STATUS_SUCCESS )
		goto error;

	net_printf(fd,TOKEN_SCORES"=%s\n",strout);

	return TEG_STATUS_SUCCESS;

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_SCORES"\n");
	return TEG_STATUS_PARSEERROR;
}


/**
 * @fn STATIC TEG_STATUS token_countries( int fd, char *str )
 * Enums the players'countries , or of all players if player is -1
 * @param fd filedescriptor del que pide
 * @param str De que jugador se estan pidiendo
 */
STATIC TEG_STATUS token_countries( int fd, char *str )
{
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int i;
	PJUGADOR pJ;
	char strout[PROT_MAX_LEN];

	PLAY_DEBUG("token_countries()\n");

	if( !JUEGO_EMPEZADO )
		goto error;

	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && !p.hay_otro ) {
		i = atoi( p.token );		
	} else
		goto error;

	if( i != -1 && jugador_whois( i, &pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	if(i!=-1) {
		/* ask just for 1 player */
		if( aux_token_countries(pJ,strout,sizeof(strout)-1) !=TEG_STATUS_SUCCESS )
			goto error;
		net_printf(fd,TOKEN_PAISES"=%d/%s\n",pJ->numjug,strout);
	} else  {
		/* ask for all the players */
		PLIST_ENTRY pL = g_list_jugador.Flink;

		while( !IsListEmpty( &g_list_jugador ) && (pL != &g_list_jugador) ) {
			pJ = (PJUGADOR) pL;
			if( pJ->is_player ) {
				if( aux_token_countries(pJ,strout,sizeof(strout)-1) !=TEG_STATUS_SUCCESS )
					goto error;
				net_printf(fd,TOKEN_PAISES"=%d/%s\n",pJ->numjug,strout);
			}
			pL = LIST_NEXT(pL);
		}
	}
	return TEG_STATUS_SUCCESS;

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_PAISES"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_start( int fd )
 * To start the game
 * @param fd Filedescriptor del que quiere emepezar
 */
TEG_STATUS token_start( int fd )
{
	char strout[PROT_MAX_LEN + PLAYERNAME_MAX_LEN  * TEG_MAX_PLAYERS + 200];
	PJUGADOR pJ;
	PLAY_DEBUG("token_start()\n");

	if( JUEGO_EMPEZADO || g_juego.jugadores < 2 )
		goto error;

	if( g_server.with_console && fd != CONSOLE_FD) {
		if( !JUGADOR_HABILITADO_P(fd,&pJ) || !pJ->is_player )
			goto error;
	}

	JUEGO_EN_EMPEZAR;

	g_juego.playing = g_juego.jugadores;

	con_text_out(M_INF,_("Starting game number: %d with seed: %u\n"),g_juego.gamenumber,g_juego.seed);

	jugador_all_set_status ( JUG_ESTADO_START );
	paises_repartir();

	if(turno_init() != TEG_STATUS_SUCCESS ) {
		con_text_out(M_ERR,_("Error, cant initilize a new turn\n"));
		goto error;
	}

	JUEGO_EN_FICHAS;

	g_juego.turno->estado = JUG_ESTADO_FICHAS;

	aux_token_stasta(strout);

	netall_printf( TOKEN_START"=%s;"TOKEN_MODALIDAD"=%d,%d;"TOKEN_FICHAS"=%d,%d\n"
			,strout			/* jugadores disponibles */
			,g_juego.objetivos	/* se juega con objetivos ? */
			,g_juego.reglas		/* con que reglas ? */
			,g_juego.turno->numjug,	/* quien empiza */
			g_juego.fichas );	/* y cuantas fichas hay que poner */
	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_START"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_test( int fd, char *unused )
 * For testing only
 */
STATIC TEG_STATUS token_test( int fd, char *unused )
{
	PLAY_DEBUG("token_test()\n");

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn STATIC TEG_STATUS token_sversion( int fd, char* unused )
 * Server version
 */
STATIC TEG_STATUS token_sversion( int fd, char *unused )
{
	PLAY_DEBUG("token_sversion()\n");

	net_printf(fd,TOKEN_SVERSION"=%s %s\n",_("TEG server version "),VERSION);
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn STATIC TEG_STATUS token_pversion( int fd, char *str )
 * Protocol version
 */
STATIC TEG_STATUS token_pversion( int fd, char *str )
{
	PJUGADOR pJ;
	PARSER p;
	DELIM igualador={ ':', ':', ':' };
	DELIM separador={ ',', ',', ',' };
	int hi,lo;

	PLAY_DEBUG("token_pversion()\n");


	if( strlen(str)==0 )
		goto error;

	p.igualador = &igualador;
	p.separador = &separador;
	p.data = str;

	if( parser_call( &p ) && p.hay_otro ) {
		hi = atoi( p.token );		
	} else goto error;

	if( parser_call( &p ) && !p.hay_otro ) {
		lo = atoi( p.token );		
	} else goto error;

	net_printf(fd,TOKEN_PVERSION"=%i,%i\n",PROTOCOL_HIVER,PROTOCOL_LOVER);

	if( hi != PROTOCOL_HIVER ) {
		con_text_out(M_ERR,_("Client with incompatible protocol version (server:%d , client:%d)\n"),PROTOCOL_HIVER,hi);

		if( jugador_whoisfd( fd, &pJ ) == TEG_STATUS_SUCCESS )
			jugador_del_hard( pJ );
		return TEG_STATUS_CONNCLOSED;
	}

	return TEG_STATUS_SUCCESS;

error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_PVERSION"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_exit( int fd )
 * Quit TEG
 */
TEG_STATUS token_exit( int fd )
{
	PJUGADOR pJ;
	PLAY_DEBUG("token_exit\n");

	if( jugador_whoisfd( fd, &pJ ) == TEG_STATUS_SUCCESS ) {
		jugador_del_hard( pJ );

	} else
		con_text_out_wop(M_ERR,"Abnormal error in token_exit(%d)\n",fd);

	return TEG_STATUS_CONNCLOSED;
}

/**
 * @fn TEG_STATUS token_surrender( int fd, char *unused )
 * Puts the player in Game Over state
 */
TEG_STATUS token_surrender( int fd, char *unused )
{
	PJUGADOR pJ;
	PLAY_DEBUG("token_surrender\n");

	if( jugador_whoisfd( fd, &pJ ) != TEG_STATUS_SUCCESS )
		goto error;

	if( !pJ->is_player )
		goto error;

	if( pJ->estado < JUG_ESTADO_HABILITADO )
		goto error;

	con_text_out(M_INF,_("Player %s(%d) abandoned the game\n"),pJ->nombre,pJ->numjug);

	netall_printf(TOKEN_SURRENDER"=%d\n",pJ->numjug );

	jugador_del_soft( pJ );

	return TEG_STATUS_SUCCESS;
error:
	net_printf(fd,TOKEN_ERROR"="TOKEN_SURRENDER"\n");
	return TEG_STATUS_PARSEERROR;
}

/**
 * @fn STATIC TEG_STATUS token_help ( int fd, char* unused )
 * Ask for help
 */
STATIC TEG_STATUS token_help ( int fd, char *unused )
{
	int i;
	for(i=0;i<NTOKENS;i++) {
		if(tokens[i].func)
			net_printf(fd, TOKEN_REM"='%s' %s\n",tokens[i].label,_(tokens[i].help));
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn STATIC TEG_STATUS token_lookup( int fd, PARSER *p )
 * Parses the tokens
 */
STATIC TEG_STATUS token_lookup( int fd, PARSER *p )
{
	int i;

	for(i = 0; i < NTOKENS; i++) {
		if(strcasecmp( p->token, tokens[i].label )==0 ){
			if (tokens[i].func)
				return( (tokens[i].func)(fd,p->value));
			return TEG_STATUS_TOKENNULL;
		}
	}
	PLAY_DEBUG("Token '%s' no encontrado\n",p->token);
	return TEG_STATUS_TOKENNOTFOUND;
}

/**
 * @fn TEG_STATUS play_teg( int fd )
 * Read the file descriptor and call the apropiate function
 */
TEG_STATUS play_teg( int fd )
{
	int i,j;
	PARSER p;
	char str[PROT_MAX_LEN];
	DELIM igualador={ '=', '=', '=' };
	DELIM separador={ ';', ';', ';' };

	p.igualador = &igualador;
	p.separador = &separador;

	str[0]=0;

	j=net_readline( fd, str, PROT_MAX_LEN );

	if( j<1 ) {
		PJUGADOR pJ;
		if( jugador_whoisfd( fd, &pJ ) == TEG_STATUS_SUCCESS )
			jugador_del_hard( pJ );
		else
			fd_remove(fd);

		return TEG_STATUS_CONNCLOSED;
	}
	
	p.data = str;

	do {
		if( (i=parser_call( &p )) ) {
			if( token_lookup( fd,&p ) == TEG_STATUS_CONNCLOSED ) {
				return TEG_STATUS_CONNCLOSED;
			}
		}
	} while( i && p.hay_otro);

	return TEG_STATUS_SUCCESS;
}
