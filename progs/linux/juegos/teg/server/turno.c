/*	$Id: turno.c,v 1.30 2001/12/22 22:03:40 tomk32 Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file turno.c
 * Manejo de turnos
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "server.h"


#undef DEBUG_TURNO

#ifdef DEBUG_TURNO
# define TURNO_DEBUG(x...) PDEBUG(x)
#else
# define TURNO_DEBUG(x...)
#endif

/**
 * @fn TEG_STATUS turno_2nextplayer( PJUGADOR *j )
 * Gives turn to the next player
 */
TEG_STATUS turno_2nextplayer( PJUGADOR *ppJ )
{
	PJUGADOR pJ;
	PLIST_ENTRY first_node = (PLIST_ENTRY)*ppJ;
	PLIST_ENTRY l = LIST_NEXT( (*ppJ));

	TURNO_DEBUG("Old turn: '%s'\n",(*ppJ)->nombre);

	g_juego.old_turn = *ppJ;

	if( IsListEmpty( first_node ) )
		return TEG_STATUS_ERROR;

	while( l != first_node )  {
		pJ = (PJUGADOR) l;
		if( (l != &g_list_jugador) && jugador_is_playing(pJ) ) {
			(*ppJ) = pJ;
			TURNO_DEBUG("New turn: '%s'\n",pJ->nombre);
			return TEG_STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}

	con_text_out_wop(M_ERR,"Abnormal error in turno_2nextplayer\n");
	return TEG_STATUS_PLAYERNOTFOUND;
}

/**
 * @fn TEG_STATUS turno_2prevplayer( PJUGADOR *ppJ )
 * Gives turn to the previous player (used when the a player leaves the game)
 */
#if 0
TEG_STATUS turno_2prevplayer( PJUGADOR *ppJ )
{
	PJUGADOR pJ;
	PLIST_ENTRY first_node = (PLIST_ENTRY)*ppJ;
	PLIST_ENTRY l = LIST_PREV( (*ppJ));

	TURNO_DEBUG("Old turn: '%s'\n",(*ppJ)->nombre);

	g_juego.old_turn = *ppJ;

	if( IsListEmpty( first_node ) )
		return TEG_STATUS_ERROR;

	while( l != first_node ) {
		pJ = (PJUGADOR) l;
		if( (l != &g_list_jugador) && jugador_is_playing(pJ) ) {
			(*ppJ) = pJ;
			TURNO_DEBUG("New turn: '%s'\n",pJ->nombre);
			return TEG_STATUS_SUCCESS;
		}
		l = LIST_PREV(l);
	}

	con_text_out_wop(M_ERR,"Abnormal error in turno_2prevplayer\n");
	return TEG_STATUS_PLAYERNOTFOUND;
}
#endif

/**
 * @fn TEG_STATUS turno_end( PJUGADOR pJ )
 * Ends the player turn
 * @param pJ player that ends his turn
 * @return TEG_STATUS_GAMEOVER if the player won the game
 */
TEG_STATUS turno_end( PJUGADOR pJ )
{
	assert(pJ);

	/* FIXME: Creo que igual este chequeo no sirve, porque se chequea cuando
	 * se conquista un pais. Solo sirve si uno gana sin conquistar algun pais
	 * Puede pasar ???
	 */
	if( objetivo_chequear( pJ ) == TEG_STATUS_GAMEOVER ) {
		game_end( pJ );
		return TEG_STATUS_GAMEOVER;
	}

	jugador_clear_turn( pJ );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn void turno_next( void )
 * Gives turn to the next player
 */
TEG_STATUS turno_next( void )
{
	assert(g_juego.turno);

	/* el turno al siguiente */
	if( turno_2nextplayer( &g_juego.turno ) == TEG_STATUS_SUCCESS ) {

		/* the one who has the turn is the one that started ? */
		if( turno_is_round_complete() ) {
			/* So, a new round is started */
			turno_2nextplayer( &g_juego.empieza_turno );
			g_juego.turno = g_juego.empieza_turno;
			aux_token_fichasc( g_juego.turno );

			g_juego.round_number++;

		} else {
			g_juego.turno->estado = JUG_ESTADO_ATAQUE;
			netall_printf( TOKEN_TURNO"=%d\n",g_juego.turno->numjug);
		}
		return TEG_STATUS_SUCCESS;
	}

	return TEG_STATUS_ERROR;
}

/**
 * @fn TEG_STATUS turno_init(void)
 * Inicializa los turnos. Llamar a esta funcion antes de que empiece el juego
 * con los jugadores ya conectados
 */
TEG_STATUS turno_init(void)
{
	int i;
	int real_i;
	PJUGADOR j;

	i = RANDOM_MAX(0,g_juego.playing-1);

	jugador_from_indice( i , &real_i );
	if( jugador_whois( real_i, &j) != TEG_STATUS_SUCCESS )
		return TEG_STATUS_ERROR;

	g_juego.old_turn = NULL;
	g_juego.turno = j;
	g_juego.empieza_turno = j;

	return TEG_STATUS_SUCCESS;
}

BOOLEAN turno_is_round_complete()
{
	/* I want to know if the round is over. It is not enought to know 
	 * if newturn == started because if a player with the turn exit the game
	 * he will never receive the turn again, but the started turn will point 
	 * to him 
	 */
	PJUGADOR pJ;
	PLIST_ENTRY l;
	PLIST_ENTRY first_node;

	if( g_juego.old_turn == NULL )
		return FALSE;

	if( g_juego.empieza_turno == g_juego.turno )
		return TRUE;

	/*
	 * if the previous playing player of 'empieza turn' is 'old turn' then
	 * the round is over
	 */
	first_node = (PLIST_ENTRY) g_juego.old_turn;
	l = LIST_NEXT( first_node );


	while( l != first_node )  {
		pJ = (PJUGADOR) l;
		if( (l != &g_list_jugador) && pJ->is_player ) {
			if( pJ == g_juego.empieza_turno )
				return TRUE;
			else if( pJ == g_juego.turno )
				return FALSE;
		}
		l = LIST_NEXT(l);
	}

	/* abnormal error */
	fprintf(stderr,"Abnormal error in turno_is_round_complete()\n");
	return FALSE;
}
