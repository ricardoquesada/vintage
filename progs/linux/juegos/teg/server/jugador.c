/*	$Id: jugador.c,v 1.60 2001/12/21 22:10:01 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file jugador.c
 * Funciones que manejan a los jugadores
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include "server.h"
#include "scores.h"

LIST_ENTRY g_list_jugador;		/**< list of players */


/*
 * Private functions for Jugador
 */
/*
 * Public functions for Jugador
 */

/**
 * @fn TEG_STATUS jugador_whois( int numjug, PJUGADOR *pJ)
 * Dado un numjug, devuelve el ptr al jugador
 */
TEG_STATUS jugador_whois( int numjug, PJUGADOR *pJ)
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ_new;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ_new = (PJUGADOR) l;
		if( pJ_new->numjug == numjug) {
			*pJ = pJ_new;
			return TEG_STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return TEG_STATUS_PLAYERNOTFOUND;
}

/**
 * @fn TEG_STATUS jugador_findbyname( char *name, PJUGADOR *pJ)
 * finds a player given its name
 */
TEG_STATUS jugador_findbyname( char *name, PJUGADOR *pJ)
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ_new;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ_new = (PJUGADOR) l;
		if( !strncmp( pJ_new->nombre, name, sizeof(pJ_new->nombre) )) {
			*pJ = pJ_new;
			return TEG_STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return TEG_STATUS_PLAYERNOTFOUND;
}

TEG_STATUS jugador_delete_discon( PJUGADOR pJ )
{
	PLIST_ENTRY l = (PLIST_ENTRY) pJ;

	if( pJ->estado == JUG_ESTADO_DESCONECTADO || pJ->fd == -1) {
		if( pJ->color != -1 )
			color_del( pJ->color );
		l = RemoveHeadList( l->Blink );
		free(l);
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn void jugador_initplayer( PJUGADOR pJ )
 * Initialize the player.
 */
TEG_STATUS jugador_initplayer( PJUGADOR pJ )
{
	assert( pJ );

	InitializeListHead( &pJ->paises );
	InitializeListHead( &pJ->deals );
	pJ->hizo_canje = FALSE;
	pJ->tot_canjes = 0;
	pJ->tot_paises = 0;
	pJ->tot_tarjetas = 0;
	pJ->tot_ejercitos = 0;
	pJ->turno_conq = 0;
	pJ->pais_src = -1;
	pJ->pais_dst = -1;
	pJ->objetivo = -1;
	pJ->fichasc_armies = 0;
	pJ->fichasc_conts = 0;

	stats_init( &pJ->player_stats );
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn void jugador_init( void )
 * Inicializador global de los jugadores
 */
void jugador_init( void )
{
	InitializeListHead( &g_list_jugador );
}

/**
 * @fn TEG_STATUS jugador_numjug_libre( int *libre)
 * Devuelve un numjug libre
 * @param puntero para devolver el jug libre
 */
TEG_STATUS jugador_numjug_libre( int *libre)
{
	char jugs[TEG_MAX_PLAYERS];
	int i;
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ;

	assert( libre );

	memset(jugs,0,sizeof(jugs));

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ = (PJUGADOR) l;
		if( pJ->is_player ) {
			if( pJ->numjug >= 0 && pJ->numjug < TEG_MAX_PLAYERS ) {
				jugs[pJ->numjug] = 1;
			}
		}

		l = LIST_NEXT(l);
	}

	for(i=0;i<TEG_MAX_PLAYERS;i++) {
		if( jugs[i] == 0) {
			*libre = i;
			return TEG_STATUS_SUCCESS;
		}
	}

	/* server is full */
	return TEG_STATUS_ERROR;
}


/**
 * @fn PJUGADOR jugador_ins( PJUGADOR pJ, BOOLEAN esjugador )
 * Crea un jugador y lo inicializa
 * @param pJ Jugador
 * @param esjugador Si es un jugador o un 'RealOnly'
 * @returns ptr al jugador creado
 */
PJUGADOR jugador_ins( PJUGADOR pJ, BOOLEAN esjugador )
{
	int numjug;
	PJUGADOR newJ;

	assert( pJ );

	if( esjugador && jugador_numjug_libre( &numjug) != TEG_STATUS_SUCCESS )
		return NULL;

	newJ = (PJUGADOR) malloc( sizeof(JUGADOR) );
	if( newJ==NULL)
		return NULL;

	pJ->numjug = -1;
	pJ->color = -1;
	memmove( newJ, pJ, sizeof(JUGADOR));
	jugador_initplayer( newJ );
	InitializeListHead( &newJ->next );

	newJ->is_player = esjugador;
	newJ->estado = JUG_ESTADO_CONNECTED;

	if( esjugador ) {
		newJ->numjug = numjug;
		g_juego.jugadores++;
	}

	g_juego.conecciones++;
	InsertTailList( &g_list_jugador, (PLIST_ENTRY) newJ );

	return newJ;
}

/**
 * @fn TEG_STATUS jugador_flush()
 * Borra todos los jugadores
 */
TEG_STATUS jugador_flush()
{
	PLIST_ENTRY tmp;

	while( !IsListEmpty( &g_list_jugador ) ) {
		tmp = RemoveHeadList( &g_list_jugador );
		if( ((PJUGADOR)tmp)->fd > 0 ) {
			fd_remove( ((PJUGADOR)tmp)->fd );
			((PJUGADOR)tmp)->fd = 0;
		}
		con_text_out(M_INF,("Deleting %s\n"),((PJUGADOR)tmp)->nombre);
		free( tmp );
	}
	g_juego.conecciones = 0;
	g_juego.jugadores = 0;
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn BOOLEAN jugador_is_playing( PJUGADOR pJ )
 * Tells if a player is playing
 */
BOOLEAN jugador_is_playing( PJUGADOR pJ )
{
	if( ! pJ->is_player )
		return FALSE;

	if( pJ->estado < JUG_ESTADO_START || pJ->estado >= JUG_ESTADO_LAST )
		return FALSE;

	return TRUE;
}

/* release the turn, and give it to the next or prev one */
TEG_STATUS jugador_give_turn_away( PJUGADOR pJ )
{

	JUG_ESTADO status = pJ->estado;

	/* needed to prevent loops in xxx_next or xxx_prev algorithms */
	pJ->estado = JUG_ESTADO_GAMEOVER;

	/* si el jugador tenia el turno, lo tiene que pasar al sig*/
	if( g_juego.turno &&  g_juego.turno->numjug == pJ->numjug ) {
		if( status <= JUG_ESTADO_POSTFICHAS ) {
			fichas_next();
		} else if( status <= JUG_ESTADO_POSTFICHAS2 ) {
			fichas2_next();
		} else if ( status <= JUG_ESTADO_POSTFICHASC ) {
			fichasc_next();
		} else { /* tenia el turno */
			turno_next();
		}
	}

#if 0
	/* XXX: Dont do this, the last player may skip his turn */

	/* si el jugador empezo el turno, digo que lo empezo el anterior */
	if( g_juego.empieza_turno && g_juego.empieza_turno->numjug == pJ->numjug ) {
		turno_2prevplayer( &g_juego.empieza_turno );
	}
#endif

	pJ->estado = status;

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_del_soft( PJUGADOR pJ )
 * Puts a player in a GAME OVER state
 */
TEG_STATUS jugador_del_soft( PJUGADOR pJ )
{
	assert( pJ );

	if( ! jugador_is_playing(pJ) )
		return TEG_STATUS_ERROR;

	g_juego.playing--;

	/* game without players */
	if( JUEGO_EMPEZADO && g_juego.playing == 0 ) {

		con_text_out(M_INF,_("Game without players. Initializing another game.\n"));
		game_end(NULL);

	/* game with just one player... the winner */
	} else if( g_juego.playing == 1 && JUEGO_EMPEZADO ) {
		PLIST_ENTRY l = g_list_jugador.Flink;
		PJUGADOR pJ2=NULL;

		while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
			pJ2 = (PJUGADOR) l;
			if( pJ2->numjug!=pJ->numjug && jugador_is_playing(pJ2) ) {
				con_text_out(M_INF,_("Game with one player. Player %s(%d) is the winner\n"),pJ2->nombre,pJ2->numjug);
				pJ2->estado = JUG_ESTADO_GAMEOVER;
				break;
			}

			pJ2 = NULL;
			l = LIST_NEXT(l);
		}

		game_end(pJ2);

	/* game may continue normally */
	} else {
		jugador_give_turn_away( pJ );
		pJ->estado = JUG_ESTADO_GAMEOVER;
	}

/*	jugador_initplayer( pJ ); */

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS jugador_del_hard( PJUGADOR pJ )
 * Deletes a player
 * @param pJ player to delete
 */
TEG_STATUS jugador_del_hard( PJUGADOR pJ )
{
	PLIST_ENTRY l = (PLIST_ENTRY) pJ;

	assert( pJ );

	g_juego.conecciones--;

	/* close the connection */
	fd_remove(pJ->fd);
	pJ->fd = -1;

	if( pJ->is_player ) {

		con_text_out(M_INF,_("Player %s(%d) quit the game\n"),pJ->nombre,pJ->numjug);
		netall_printf( TOKEN_EXIT"=%d\n",pJ->numjug );

		if( jugador_is_playing ( pJ ) ) {


			jugador_del_soft( pJ );

			g_juego.jugadores--;
			pJ->status_before_discon = JUG_ESTADO_IDLE;
			pJ->estado = JUG_ESTADO_DESCONECTADO;
			return TEG_STATUS_SUCCESS;

		} else if( pJ->estado == JUG_ESTADO_GAMEOVER ) {

			g_juego.jugadores--;
			pJ->status_before_discon = JUG_ESTADO_GAMEOVER;
			pJ->estado = JUG_ESTADO_DESCONECTADO;
			return TEG_STATUS_SUCCESS;
		}

		color_del( pJ->color );
		g_juego.jugadores--;
	}

	/* free the player */
	l = RemoveHeadList( l->Blink );
	free( l );

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_from_indice( int j, int *real_j )
 * Dado un indice de jugador [0...TEG_MAX_PLAYERS] devulve el numjug asociado
 */
TEG_STATUS jugador_from_indice( int j, int *real_j )
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ;
	int i=0;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ = (PJUGADOR) l;
		if( pJ->is_player && pJ->estado>=JUG_ESTADO_HABILITADO ) {
			if( j == i ) {
				*real_j = pJ->numjug;
				return TEG_STATUS_SUCCESS;
			}
			i++;
		}
		l = LIST_NEXT(l);
	}
	return TEG_STATUS_PLAYERNOTFOUND;
}

/**
 * @fn TEG_STATUS jugador_asignarpais( int numjug, PPAIS p)
 * Asigna un pais a un jugador
 * @param numjug jugador que tendra un nuevo pais
 * @param p Pais que va a ser asignado
 */
TEG_STATUS jugador_asignarpais( int numjug, PPAIS p)
{
	PJUGADOR pJ;

	if( jugador_whois( numjug, &pJ) != TEG_STATUS_SUCCESS )
		return TEG_STATUS_PLAYERNOTFOUND;

	InsertTailList( &pJ->paises, (PLIST_ENTRY) p );
	p->numjug = numjug;
	pJ->tot_paises++;
	pJ->tot_ejercitos++;		/* cada pais viene con un ejercito */
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_whoisfd( int fd, PJUGADOR *j)
 * Dado un filedescriptor devuelve el jugador que es due�o
 * @param fd filedescriptor del jugador
 * @param j Tendra el jugador
 */
TEG_STATUS jugador_whoisfd( int fd, PJUGADOR *j)
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR i;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		i = (PJUGADOR) l;
		if( i->fd == fd) {
			*j = i;
			return TEG_STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return TEG_STATUS_PLAYERNOTFOUND;
}

/*
 * TODO: This should be a macro
 */
BOOLEAN jugador_esta_xxx( int fd, int condicion, int strict )
{
	PJUGADOR pJ;

	if( jugador_whoisfd( fd, &pJ) == TEG_STATUS_SUCCESS ) {
		if(strict)
			return (pJ->estado == condicion );
		else
			return (pJ->estado >= condicion );
	} else
		return FALSE;
}

BOOLEAN jugador_esta_xxx_plus( int fd, int condicion, int strict, PJUGADOR *j )
{
	if( jugador_whoisfd( fd, j) == TEG_STATUS_SUCCESS ) {
		if(strict)
			return ((*j)->estado == condicion );
		else
			return ((*j)->estado >= condicion );
	} else
		return FALSE;
}

/**
 * @fn TEG_STATUS jugador_listar_paises( PJUGADOR pJ, int *paises )
 * Dice los paises agrupados por cont que tiene un jugador
 * @param pJ Jugador
 * @param paises Array con los paises por continente que tiene
 * @returns Si fue SUCCESS o no
 */
TEG_STATUS jugador_listar_paises( PJUGADOR pJ, int *paises )
{
	PLIST_ENTRY list;
	PPAIS pP;

	assert( pJ );
	assert( paises );


	list = pJ->paises.Flink;

	while( !IsListEmpty( &pJ->paises ) && (list != &pJ->paises ) ) {
		pP = (PPAIS) list;

		paises[ pP->continente ]++;

		list = LIST_NEXT( list );
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_listar_conts( PJUGADOR pJ, unsigned long *ret )
 * Dice los continentes que tiene un jugador
 * @param pJ Jugador
 * @param ret Continentes construidos con 'or'
 * @returns Si fue SUCCESS o no
 */
TEG_STATUS jugador_listar_conts( PJUGADOR pJ, unsigned long *ret )
{
	int paises[CONT_CANT];
	int i;

	assert( pJ );
	assert( ret );


	memset( paises, 0, sizeof(paises) );


	if( jugador_listar_paises( pJ, paises ) != TEG_STATUS_SUCCESS ) {
		free( paises );
		return TEG_STATUS_ERROR;
	}

	*ret = 0;

	for(i=0;i< CONT_CANT; i++) {
		if( paises[i] == g_conts[i].cant_paises )
			*ret |= 1 << i;
	}

	return TEG_STATUS_SUCCESS;
}



/**
 * @fn TEG_STATUS jugador_clear_turn( PJUGADOR pJ )
 * Initialize the start turn variables
 * @param pJ Player
 * @return TEG_STATUS_SUCCESS if everything went OK
 */
TEG_STATUS jugador_clear_turn( PJUGADOR pJ )
{
	int i;

	assert( pJ );

	pJ->turno_conq = 0;
	pJ->pais_src = -1;
	pJ->pais_dst = -1;
	pJ->estado =  JUG_ESTADO_IDLE;

	/* clean all the regroups the player could have done */
	for(i=0;i<PAISES_CANT;i++) {
		if( g_paises[i].numjug == pJ->numjug )
			g_paises[i].ejer_reagrupe = 0;
	}

	return TEG_STATUS_SUCCESS;
}

/**
 * @fn int jugador_fichasc_cant( PJUGADOR pJ )
 * Dice la cant de paises que puede poner un jugador segun la cant de paises
 */
int jugador_fichasc_cant( PJUGADOR pJ )
{
	assert( pJ );

	if( pJ->tot_paises <= 6 )
		return 3;
	else
		return pJ->tot_paises/2;
}

/**
 * @fn TEG_STATUS jugador_all_set_status( JUG_ESTADO estado )
 * Pone a todos los jugadores en un cierto estado.
 * @param estado Estado en el que se pone a todos los jugadores
 */
TEG_STATUS jugador_all_set_status( JUG_ESTADO estado )
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ = (PJUGADOR) l;
		if( pJ->is_player && pJ->estado >= JUG_ESTADO_GAMEOVER ) {
			pJ->estado = estado;
		}
		l = LIST_NEXT(l);
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn TEG_STATUS jugador_map( jug_map_func func )
 * Map para jugador
 */
TEG_STATUS jugador_map( jug_map_func func )
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ;

	assert(func);

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ = (PJUGADOR) l;

		/* I dont know what (func)() will do, so next now */
		l = LIST_NEXT(l);

		if( pJ->is_player ) {
			(func)(pJ);
		}
	}
	return TEG_STATUS_SUCCESS;
}

/**
 * @fn BOOLEAN jugador_is_lost( PJUGADOR pJ )
 * Tell if the player lost the game
 * @param pJ Jugador a chequear
 * @return TEG_STATUS_GAMEOVER si perdio
 */
BOOLEAN jugador_is_lost( PJUGADOR pJ )
{
	assert( pJ );

	if( pJ->tot_paises > 0 )
		return 0;

	return 1;
}

/**
 * @fn TEG_STATUS jugador_poner_perdio( PJUGADOR pJ )
 * Put the player in GAMEOVER state
 * @param pJ Jugador a poner es estado 'perdio'
 */
TEG_STATUS jugador_poner_perdio( PJUGADOR pJ )
{
	assert( pJ );
	assert( pJ->is_player == TRUE );

	if( pJ->estado == JUG_ESTADO_DESCONECTADO ) {
		scores_insert_player( pJ );
		jugador_delete_discon( pJ );
		return TEG_STATUS_SUCCESS;
	}

	pJ->estado = JUG_ESTADO_GAMEOVER;

#if 0
	if( g_juego.empieza_turno && g_juego.empieza_turno->numjug == pJ->numjug ) {
		turno_2prevplayer( &g_juego.empieza_turno );
	}
#endif

	g_juego.playing--;

	return TEG_STATUS_SUCCESS;
}


/**
 * @fn TEG_STATUS jugador_fillname( PJUGADOR pJ, char *name )
 * Assigns a name to the player that does not conflict with another
 * names
 */
TEG_STATUS jugador_fillname( PJUGADOR pJ, char *name )
{
	PJUGADOR pJ_new;
	char new_name [ PLAYERNAME_MAX_LEN ];

	memset(new_name,0,sizeof(new_name));
	strncpy( new_name, name, sizeof(new_name) -1 );

	strip_invalid(new_name);
	if( jugador_findbyname(new_name,&pJ_new) == TEG_STATUS_SUCCESS && pJ_new->estado != JUG_ESTADO_DESCONECTADO ) {
		/* that name is already registered, assign a new name dinamically */
		int n = strlen(new_name);
		if( n < sizeof(pJ->nombre) - 2 ) {
			new_name[n] = '_';
			jugador_fillname( pJ, new_name );
		} else {
			if( new_name[n] < '0' || new_name[n] > '9' )
				new_name[n]='0';
			else
				new_name[n]++;
			jugador_fillname( pJ, new_name );
		}
	}
	else {
		strncpy( pJ->nombre, new_name, sizeof(pJ->nombre)-1);
		pJ->nombre[ sizeof(pJ->nombre) -1 ] = 0;
	}

	return TEG_STATUS_SUCCESS;
}

PJUGADOR jugador_return_disconnected( PJUGADOR pJ )
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ_new;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ_new = (PJUGADOR) l;

		if( (pJ_new->estado == JUG_ESTADO_DESCONECTADO) &&
				strcmp( pJ->nombre, pJ_new->nombre ) == 0 
				) {
			g_juego.jugadores++;
			g_juego.playing++;
			g_juego.conecciones++;
			return pJ_new;
		}

		l = LIST_NEXT(l);
	}
	return NULL;
}

BOOLEAN jugador_is_disconnected( PJUGADOR pJ )
{
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ_new;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		pJ_new = (PJUGADOR) l;

		if( (pJ_new->estado == JUG_ESTADO_DESCONECTADO) &&
				strcmp( pJ->nombre, pJ_new->nombre ) == 0 
				) {
			return TRUE;
		}

		l = LIST_NEXT(l);
	}
	return FALSE;
}

/* insert all the player but the ones in GAME OVER */
TEG_STATUS jugador_insert_scores( PJUGADOR pJ )
{
	scores_insert_player( pJ );
	return TEG_STATUS_SUCCESS;
}
