/*	$Id: globals.h,v 1.23 2001/12/14 14:36:01 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __TEGS_GLOBALS_H
#define __TEGS_GLOBALS_H

typedef struct _sjuego {
	int conecciones;	/**< quantity of connections */
	int jugadores;		/**< quantity of players */
	int playing;		/**< quantity of players playing */
	BOOLEAN objetivos;	/**< con objetivos o no */
	BOOLEAN cmission;	/**< with common secret mission or not */
	int reglas;		/**< reglas a jugar: TEG o RISK */
	PJUGADOR turno;		/**< de quien es el turno */
	PJUGADOR empieza_turno;	/**< quien empezo el turno 1ro */
	PJUGADOR old_turn;	/**< the player who had the previous turn */
	int estado;		/**< se empezo a jugar o no */
	int fichas;		/**< initial armies (default 5)*/
	int fichas2;		/**< initial armies2 (default 3)*/
	unsigned int seed;	/**< seed for random */
	int gamenumber;		/**< game number */
	int round_number;	/**< round number */
} SJUEGO, *PSJUEGO;
extern SJUEGO g_juego;

enum {
	JUEGO_ESTADO_ESPERANDO,	/* no se comenzo y pueder ingresar jugadores */
	JUEGO_ESTADO_EMPEZADO,	/* no pueden entrar mas jugadores */
	JUEGO_ESTADO_FICHAS,	/* poner las 5 fichas iniciales */
	JUEGO_ESTADO_FICHAS2,	/* poner las 3 fichas iniciales */
	JUEGO_ESTADO_TURNO,	/* esta en un cambio de turno */
	JUEGO_ESTADO_ATAQUE	/* se esta en pleno ataque */
};

#define JUEGO_EMPEZADO		(g_juego.estado >= JUEGO_ESTADO_EMPEZADO)
#define JUEGO_ESPERANDO		(g_juego.estado == JUEGO_ESTADO_ESPERANDO)

#define JUEGO_EN_TURNO		(g_juego.estado = JUEGO_ESTADO_TURNO)
#define JUEGO_EN_EMPEZAR	(g_juego.estado = JUEGO_ESTADO_EMPEZADO)
#define JUEGO_EN_FICHAS		(g_juego.estado = JUEGO_ESTADO_FICHAS)
#define JUEGO_EN_FICHAS2	(g_juego.estado = JUEGO_ESTADO_FICHAS2)

typedef struct _server {
	char name[SERVER_NAMELEN];
	BOOLEAN debug;		/* debug mode */
	BOOLEAN with_console;	/* si es con consola */
	int port;		/* port donde escucha (2000) */
	int max_players;	/* cantidad maxima de jugadores ( 6 ) */
	int max_connections;	/* cantidad maxima de coneccion ( 15 ) */
	BOOLEAN with_ggz;	/* modo GGZ */
} SERVER, *PSERVER;
extern SERVER g_server;
	

#endif /* __TEGS_GLOBALS_H */
