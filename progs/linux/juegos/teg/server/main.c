/*	$Id: main.c,v 1.67 2001/12/22 22:03:40 tomk32 Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file main.c
 * Funciones principales
 */

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>

#include "server.h"
#include "scores.h"
#include "xmlscores.h"


#undef DEBUG_MAIN

#ifdef DEBUG_MAIN
# define MAIN_DEBUG(x...) PDEBUG(x)
#else
# define MAIN_DEBUG(x)
#endif

/*
 * private variables
 */
static fd_set all_set;

/* 
 * public variables
 */
SJUEGO g_juego;
SERVER g_server;

void game_end( PJUGADOR winner )
{
	char strout[PROT_MAX_LEN + PLAYERNAME_MAX_LEN * TEG_MAX_PLAYERS + 200];
	PLIST_ENTRY l = g_list_jugador.Flink;
	PJUGADOR pJ;

	/* update scores */
	jugador_map( jugador_insert_scores );

	if( aux_token_stasta(strout) == TEG_STATUS_SUCCESS ) {

		/* send the last status to all the players */
		while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
			pJ = (PJUGADOR) l;

			if( pJ->is_player && pJ->fd != 1)
				net_printf(pJ->fd,TOKEN_STATUS"=%s\n",strout);

			l = LIST_NEXT(l);
		}
	}

	/* delete disconn players */
	jugador_map( jugador_delete_discon );

	/* send who is the winner */
	if( winner )
		netall_printf( TOKEN_WINNER"=%d,%d\n",winner->numjug,winner->objetivo );

	game_new();
}

/**
 * @fn void game_new()
 * Creates a new game
 */
void game_new()
{
	static int first_time = 1;

	paises_init();
	objetivo_init();
	pactos_flush();

	jugador_map( jugador_initplayer );
	jugador_all_set_status( JUG_ESTADO_HABILITADO );
	g_juego.playing = 0;
	

	g_juego.turno = NULL;
	g_juego.empieza_turno = NULL;
	g_juego.estado = JUEGO_ESTADO_ESPERANDO;
	g_juego.round_number = 0;

	if( first_time ) {
		first_time = 0;
		g_juego.gamenumber = 0;
		g_juego.seed = get_int_from_dev_random();
	} else {
		g_juego.gamenumber++;
		g_juego.seed = rand();
		srand( g_juego.seed );
	}
}

BOOLEAN game_is_finished( void )
{
	if( JUEGO_EMPEZADO && g_juego.playing < 2 )
		return TRUE;
	return FALSE;
}

/**
 * @fn void game_init()
 * Initialize the server for the first game
 */
void game_init()
{
	jugador_init();
	color_init();
	pactos_init();
	scores_init();
	xmlscores_load();

	g_juego.conecciones = 0;
	g_juego.jugadores = 0;
	g_juego.playing = 0;

	/* valores por default */
	g_juego.fichas = 5;
	g_juego.fichas2 = 3;
	g_juego.objetivos = FALSE;
	g_juego.cmission = TRUE;
	g_juego.reglas = TEG_RULES_TEG;

	game_new();
}

/**
 * @fn void server_init( void )
 */
void server_init( void )
{
	gethostname(g_server.name,SERVER_NAMELEN);
	g_server.port=TEG_DEFAULT_PORT;
	g_server.debug=FALSE;
	g_server.with_console=TRUE;
	g_server.with_ggz=FALSE;
}


void server_exit( int sock )
{
	console_quit();
	close(sock);
	if( g_server.with_console )
		close(CONSOLE_FD);
	jugador_flush();
	printf(_("Goodbye.\n"));
	exit(1);
}


void main_loop( void )
{
	int listenfd,fd, nready;
	struct sockaddr client;
	ssize_t client_len;
	int max_fd;
	fd_set read_set;

	listenfd = net_listen(NULL,g_server.port);
	if( listenfd < 0 )
		return;

	max_fd=listenfd;

	FD_ZERO(&all_set);
	FD_SET(listenfd,&all_set);

	if( g_server.with_console ) {
		FD_SET(CONSOLE_FD, &all_set);
		con_show_prompt();
	}
	while(1) {
		read_set = all_set;
		nready = select( max_fd+1, &read_set, NULL, NULL, NULL );

		if(nready==-1) { 
			if(errno!=EINTR) {
				fprintf(stderr,_("tegserver: Abnormal error in select()\n"));
				perror("tegserver:");
			}
			continue;
		}

		/* new client */
		if(FD_ISSET( listenfd, &read_set) ) {	
			MAIN_DEBUG("new client\n");
			client_len = sizeof( client );
			fd = accept( listenfd, (struct sockaddr *)&client, &client_len );

			if( fd==-1)
				continue;

			FD_SET(fd,&all_set);

			if(fd>max_fd) 
				max_fd=fd;

			if(--nready <= 0)
				continue;

		/* input from console */
		} else if( g_server.with_console && FD_ISSET(CONSOLE_FD, &read_set)) {
			TEG_STATUS ts = console_handle(CONSOLE_FD);

			if(ts==TEG_STATUS_GAMEOVER || ts==TEG_STATUS_CONNCLOSED)
				server_exit(listenfd);


		/* input from players */
    		} else for(fd=0;fd<=max_fd;fd++) {
			if( (fd!=listenfd && fd!=CONSOLE_FD) && FD_ISSET(fd,&read_set) ) {
				/* should this be a thread ? */
				if(play_teg( fd )==TEG_STATUS_CONNCLOSED) {
					MAIN_DEBUG("cerrando coneccion\n");
				}

				if(--nready <= 0)
					break;
			}
		}
	}
}

/**
 * @fn fd_remove( int fd )
 * Clears a fd from the all_set descriptors
 */
void fd_remove( int fd )
{
	if( fd > 0 ) {
		close(fd);
		FD_CLR(fd,&all_set);
	}
}

/**
 * @fn void argument_init( int argc, char **argv)
 */
void argument_init( int argc, char **argv)
{
	int i;
	char *option;

	i = 1;
	while (i < argc) {
		if( is_option("--help", argv[i]) ) {
			fprintf(stderr, _("Usage: %s [option ...]\nValid options are:\n"), argv[0]);
			fprintf(stderr, _("  -h, --help\t\tPrint a summary of the options\n"));
			fprintf(stderr, _("  -p, --port PORT\tBind the server to port PORT (default 2000)\n"));
			fprintf(stderr, _("  -s, --seed SEED\tNew seed for random\n"));
			fprintf(stderr, _("  -v, --version\t\tPrint the version number\n"));
			fprintf(stderr, _("  -c, --console BOOLEAN\tEnable the console or not (default 1)\n"));
#ifdef WITH_GGZ
			fprintf(stderr, _("  -g, --ggz\t\tEnable the GGZ mode (default OFF)\n"));
#endif /* WITH_GGZ */
			exit(0);
		} else if (is_option("--version",argv[i])) {
			fprintf(stderr, TEG_NAME" v"VERSION"\n\n");
			exit(0);
		} else if ((option = get_option("--port",argv,&i,argc)) != NULL) {
			g_server.port=atoi(option);
		} else if ((option = get_option("--seed",argv,&i,argc)) != NULL) {
			g_juego.seed=atoi(option);
		} else if ((option = get_option("--console",argv,&i,argc)) != NULL) {
			g_server.with_console=atoi(option);
#ifdef WITH_GGZ
		} else if (is_option("--ggz",argv[i])) {
			g_server.with_ggz=1;
			g_server.with_console=FALSE;
#endif /* WITH_GGZ */
		} else {
			fprintf(stderr, _("Unrecognized option: \"%s\"\n"), argv[i]);
			exit(1);
		}
		i++;
	}
}

/**
 * @fn int main( int argc, char **argv)
 */
int main( int argc, char **argv)
{
	init_nls();
	printf( TEG_NAME" v"VERSION" server - by Ricardo Quesada\n\n");

	dont_run_as_root();

	server_init();		/* default values for server */
	game_init();		/* default values for the game */

	argument_init(argc, argv);	/* parse command line */
	printf( _("Bound to port: %d\n"),g_server.port );
	printf( _("Using seed: %u\n\n"),g_juego.seed );

	if( g_server.with_console ) {
		console_init();		/* initialize console */
		printf( _("Type '%s' for more help\n"),TOKEN_HELP);
	} else {
		if( !g_server.with_ggz )
			printf(_("Standalone server.\n"));
		else
			printf(_("GGZ mode activated.\n"));
	}

	srand( g_juego.seed );

#ifdef WITH_GGZ
	if( g_server.with_ggz )
		tegggz_main_loop();
	else
#endif /* WITH_GGZ */

	main_loop();

	if( g_server.with_console )
		console_quit();

	return 1;
}
