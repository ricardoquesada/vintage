/*	$Id: objetivos.h,v 1.6 2001/09/03 00:55:40 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; only version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file objetivos.h
 */

#ifndef __TEGS_OBJETIVOS_H
#define __TEGS_OBJETIVOS_H

TEG_STATUS objetivo_chequear( PJUGADOR pJ );
TEG_STATUS objetivo_asignar( PJUGADOR pJ );
TEG_STATUS objetivo_init();
TEG_STATUS objetivo_set( int a );
TEG_STATUS objetivo_common_mission( int a );

#endif /* __TEGS_OBJETIVOS_H */
