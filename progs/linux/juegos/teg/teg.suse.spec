# spec file for package teg
# (c)2000 David Haller

%define  ver     0.4.21
%define  RELEASE 1
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define  prefix  /opt/gnome
%define  sysdocs /usr/doc/packages

Name:      		teg
Summary:   		Clone of a Risk clone
Version:		%ver
Release:		%rel
Distribution:	SuSE Linux 6.2 (i686)
Copyright: 	  	GPL
Group: 			Amusements/Games
URL:			http://teg.sourceforge.net/
Packager:     	David Haller <david@dhaller.de>
Requires:		glibc gnlibs esound audiofil gtk imlib glib xshared libz
Provides:		teg
#Autoreqprov:		On

Docdir:    %{sysdocs}
BuildRoot: /var/tmp/rpm/teg-%{PACKAGE_VERSION}-root

Source:     teg-%{PACKAGE_VERSION}.tar.gz

%description
Tenes Emapandas Graciela (TEG) is a clone of 'Plan T�ctico y Estrat�gico
de la Guerra' (Tactical and Strategic plan of the War), which is a
pseudo-clone of Risk, a turn-based strategy game. Some rules are different.

Authors:
--------
	Idea to do the game: Sebasti�n Cativa Tolosa (scativa@hotmail.com)
	Design and implementation by: Ricardo Quesada (riq@core-sdi.com)

%prep
%setup
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{prefix} --with-included-gettext

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT%{prefix} install
strip $RPM_BUILD_ROOT%{prefix}/bin/*

%clean
#rm -rf $RPM_BUILD_DIR/teg-%{ver}
#rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%{prefix}/bin/tegclient
%{prefix}/bin/tegserver
%{prefix}/share/gnome/help/teg/C/*
%{prefix}/share/locale
%{prefix}/share/pixmaps

%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL NEWS README

%changelog
