#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="teg"

(test -f $srcdir/configure.in \
  && test -d $srcdir/server \
  && test -d $srcdir/client \
  && test -d $srcdir/common) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level directory"
    exit 1
}

. $srcdir/macros/autogen.sh
