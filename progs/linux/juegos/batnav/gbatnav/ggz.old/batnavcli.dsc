# TEG description file
[ModuleInfo]
Game = Batalla Naval
Author = Ricardo Quesada
# CommandLine path is figured relative to GameDir unless starting with /
CommandLine = /usr/local/bin/gbnclient --ggz
Frontend = gtk
Homepage = http://batnav.sourceforge.net
Name = batnav
Protocol = 0.0.6
Version = 0.0.6
