# BATNAV description file
Name = batnav
Version = 0.0.6
Description = A Battleship Game
Author = Ricardo Quesada
Homepage = http://batnav.sourceforge.net
# PlayersAllowed and BotsAllowed should be repeated as many times as
# necessary to specify the valid distinct options which can appear
PlayersAllowed = 2
PlayersAllowed = 3
PlayersAllowed = 4
PlayersAllowed = 5
PlayersAllowed = 6
PlayersAllowed = 7
PlayersAllowed = 8
PlayersAllowed = 9
PlayersAllowed = 10
BotsAllowed = 0
BotsAllowed = 1
BotsAllowed = 2
BotsAllowed = 3
BotsAllowed = 4
BotsAllowed = 5
BotsAllowed = 6
BotsAllowed = 7
BotsAllowed = 8
BotsAllowed = 9
# Set AllowLeave to 1 if game supports players leaving during gameplay
AllowLeave = 1
# ExecutablePath is figured relative to GameDir unless starting with /
ExecutablePath = /usr/local/bin/gbnserver --ggz
# GameDisabled is a quick way to turn off the game if necessary
#GameDisabled
