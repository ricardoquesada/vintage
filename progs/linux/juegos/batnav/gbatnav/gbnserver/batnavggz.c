/*	$Id: batnavggz.c,v 1.7 2001/05/04 02:47:36 riq Exp $	*/
/* Batalla Naval
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file batnavggz.c
 * Funcion que maneja un poco el soporte para GGZ
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>

#include "server.h"
#include "common.h"

#include "easysock.h"
#include "ggz_protocols.h"
#include "ggz_server.h"

static int binded_port;
static fd_set active_fd_set;
static int ggz_sock;

BATNAV_STATUS batnavggz_launch_bot()
{
	pid_t pid;
	char *args[5];
	char port[50];


	if( !binded_port )
		return BATNAV_STATUS_ERROR;

	if ( (pid = fork()) < 0) {
		perror("batnavggz_launch_bot:");
		return BATNAV_STATUS_ERROR;
	} else if (pid == 0) {
		sprintf(port,"%d",binded_port);

		args[0] = BINDIR"/gbnrobot";
		args[1] = "--port";
		args[2] = port;
		args[3] = "--nogui";
		args[4] = NULL;
	
		printf("Launching robot with options: %s %s %s %s\n",args[0],args[1],args[2],args[3]);
		if( execv(args[0], args) < 0) {
			fprintf(stderr,"Launching robot failed. Does the file `%s' exists ?\n",args[0]);
			perror("exe:");
		}
		exit(1);
	} else {
		/* nothing */
	}
	return BATNAV_STATUS_SUCCESS;
}

int batnavggz_del_fd( int fd )
{
	FD_CLR(fd,&active_fd_set);
	return 0;
}

int game_handle_ggz(int ggz_fd, int* p_fd)
{
	int op, seat, status = -1;
	int i;

	if (es_read_int(ggz_fd, &op) < 0)
		return -1;

	switch (op) {

	case REQ_GAME_LAUNCH:
		if (ggz_game_launch() == 0) {

			for (i = 0; i < ggz_seats_num(); i++) {
				if( ggz_seats[i].assign == GGZ_SEAT_BOT ) {
					batnavggz_launch_bot();
				}
			}
		} else {
			perror("Error en game launch()");
		}
		status = 0;
		break;
		
	case REQ_GAME_JOIN:
		if (ggz_player_join(&seat, p_fd) == 0) {
			bnwrite(*p_fd,BN_GGZ);
			status = 1;
		}
		break;

	case REQ_GAME_LEAVE:
		if ( (status = ggz_player_leave(&seat, p_fd)) == 0) {
			token_exit(*p_fd);
			status = 2;

			/*
			 * if all the ggz players left the game send a
			 * GAME_OVER to the ggz server
			 * This fixes the `phantom table' effect.
			 * TODO: Remove this after version 0.0.5 of ggz
			 */
			for (i = 0; i < ggz_seats_num(); i++) {
				if( ggz_seats[i].assign == GGZ_SEAT_PLAYER )
					break;
			}
			if(i==ggz_seats_num() ) {
				if( es_write_int(ggz_sock, REQ_GAME_OVER) < 0)
					fprintf(stderr,"Error sending REQ_GAME_OVER\n");
			}
		}
		break;
		
	case RSP_GAME_OVER:
		status = 3; /* Signal safe to exit */
		break;

	default:
		/* Unrecognized opcode */
		status = -1;
		break;
	}

	return status;
}

BATNAV_STATUS batnavggz_find_ggzname( int fd, char *n, int len )
{
	int i;
	if(!n)
		return BATNAV_STATUS_ERROR;

	for (i = 0; i < ggz_seats_num(); i++) {
		if( ggz_seats[i].fd == fd ) {
			if( ggz_seats[i].name ) {
				strncpy(n,ggz_seats[i].name,len);
				n[len]=0;
				return BATNAV_STATUS_SUCCESS;
			} else
				return BATNAV_STATUS_ERROR;
		}
	}

	return BATNAV_STATUS_ERROR;
}

BATNAV_STATUS batnavggz_create_aux_socket( int *s )
{
	struct sockaddr_in servaddr;
	size_t addrLenght;
	int sock;

	sock=socket(AF_INET, SOCK_STREAM, 0 );
	if( sock < 0 ) {
		perror("batnavggz_create_aux_socket:");
		return BATNAV_STATUS_ERROR;
	}


	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family=AF_INET;
//	servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
	servaddr.sin_addr.s_addr=htonl(0x7f000001); /* 127.0.0.1 */
	servaddr.sin_port=htons(0);

	addrLenght = sizeof(servaddr);
	if(bind(sock,(struct sockaddr *) &servaddr, addrLenght )) {
		perror("batnavggz_create_aux_socket:");
		return BATNAV_STATUS_ERROR;
	}

	if(listen(sock,10)<0) {
		perror("batnavggz_create_aux_socket:");
		return BATNAV_STATUS_ERROR;
	}

	*s = sock;

	addrLenght = sizeof(servaddr);
	getsockname(sock, (struct sockaddr*) &servaddr, &addrLenght );
	binded_port =  htons( servaddr.sin_port );
	fprintf(stderr,"BATNAV-GGZ is binded to ephimeral port %d.\n",binded_port);

	return BATNAV_STATUS_SUCCESS;
}

int batnavggz_main_loop(void)
{
	char game_over = 0;
	int i, fd, fd_max, status;
	fd_set read_fd_set;
	int af_sock=0;
	int nready;

	struct sockaddr client;
	ssize_t client_len;
	
	/* Initialize ggz */
	if (ggz_server_init("batnav") < 0)
		return -1;

	if ( (ggz_sock = ggz_server_connect()) < 0) {
		ggz_server_quit();
		fprintf(stderr,"Only the GGZ server must call BATNAV server in GGZ mode!\n");
		if(af_sock) close(af_sock);
		return -1;
	}

	FD_ZERO(&active_fd_set);
	FD_SET(ggz_sock, &active_fd_set);

	if( batnavggz_create_aux_socket(&af_sock) == BATNAV_STATUS_SUCCESS ) {
		FD_SET( af_sock, &active_fd_set );
	}

	fd_max = MAX( ggz_sock, af_sock );

	while(!game_over) {
		
		read_fd_set = active_fd_set;
		
		nready = select((fd_max+1), &read_fd_set, NULL, NULL, NULL);
		
		if (nready < 0) {
			if (errno == EINTR)
				continue;
			else {
				game_over=1;
				continue;
			}
		}

		/* Check messages from batnav af_local */
		if (FD_ISSET(af_sock, &read_fd_set)) {

			client_len = sizeof( client );
			fd = accept( af_sock, (struct sockaddr *)&client, &client_len );
			if( fd > 0) {
				if( accept_new_player(fd) < 0)
					continue;

				FD_SET(fd,&active_fd_set);
				if(fd>fd_max) 
					fd_max=fd;
			}

		/* Check for message from GGZ server */
		} else if (FD_ISSET(ggz_sock, &read_fd_set)) {
			status = game_handle_ggz(ggz_sock, &fd);
			switch (status) {
				
			case -1:  /* Big error!! */
				game_over=1;
				continue;
				
			case 0: /* All ok, how boring! */
				break;

			case 1: /* A player joined */
				if( accept_new_player(fd) < 0)
					continue;

				FD_SET(fd, &active_fd_set);
				if( fd > fd_max )
					fd_max = fd;
				break;
				
			case 2: /* A player left */
				FD_CLR(fd, &active_fd_set);
				break;
				
			case 3: /*Safe to exit */
				game_over = 1;
				break;
			}

		/* Check for message from player */
		} else for (i=0; i<=fd_max; i++) {
			if (i!=ggz_sock && i!=af_sock && FD_ISSET(i, &read_fd_set)) {
				play_batnav(NULL,i,NULL);
			}
		}
	}

	if(af_sock) close(af_sock);
	if(ggz_sock) close(ggz_sock);
	ggz_server_quit();
	return 0;
}
