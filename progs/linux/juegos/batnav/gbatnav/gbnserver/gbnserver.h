/*	$Id: gbnserver.h,v 1.3 2001/04/04 21:31:01 riq Exp $	*/

#ifndef __BN_GBNSERVER_H__
#define __BN_GBNSERVER_H__ 

int eshundido( int, int, int );
void ctable( int );
int wtable( int, char *);
void borrar_jugador( int );
void salir_bien( void );
void broadcast( char *fmt, ...);
int accept_new_player( int fd );

#endif /* __BN_GBNSERVER_H__ */
