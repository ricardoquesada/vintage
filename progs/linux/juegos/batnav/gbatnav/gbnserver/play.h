/*	$Id: play.h,v 1.1.1.1 2000/02/13 00:56:49 riq Exp $	*/
#ifndef __BN_PLAY_H__
# define __BN_PLAY_H__

void play_batnav( gpointer , gint , GdkInputCondition );
gint quejugador( gint );
void token_exit( gint );
void token_start( gint );

#endif /* __BN_PLAY__ */
