/*	$Id: g_interface.c,v 1.3 2001/04/11 23:53:32 riq Exp $	*/
/******************************************************************************
 *                               g_interface                                  *
 *                             gnome interface				      *
 *****************************************************************************/
#include <config.h>
#include <gnome.h>
#include <stdarg.h>

#include "server.h"
#include "version.h"
#include "gbnserver.h"
#include "play.h"

#include "robot.xpm"

#define CLIST_COLUMNS 6
static GtkWidget *text[MAXPLAYER];
static GtkWidget *vscrollbar[MAXPLAYER];

static GtkWidget *clist=NULL;


void say_in_clist( gint x, gint y, const gchar *str)
{
	if (clist==NULL) return;
	gtk_clist_freeze (GTK_CLIST(clist));
	gtk_clist_set_text( GTK_CLIST(clist),x, y,str);
	gtk_clist_thaw (GTK_CLIST(clist));
}
	
static void on_exit_activate(GtkWidget *widget, gpointer data)
{
	salir_bien();	
	gtk_main_quit();
}

static void g_launch_robot(void)
{
	gnome_execute_shell(NULL,"gbnrobot");
}

void g_start_game(void)
{
	/* Simulo que alguien envio el token START */
	token_start( -1 );
}

static void about(GtkWidget *widget, gpointer data)
{
   GtkWidget *about;
   gchar *authors[] = {
      "riq (riq@core-sdi.com)",
      NULL
   };
   
   about = gnome_about_new (_("Batalla Naval server"), VERSION,
			    "(C) 1998-2001 Ricardo C. Quesada",
			    (const char**) authors,
			    _("A multiplayer, multirobot, networked battleship game"),
			    "gnome-gbatnav.png");
   gtk_widget_show (about);
}

GnomeUIInfo gamemenu[] = 
{
	{ GNOME_APP_UI_ITEM, N_("_Launch a robot in server"), NULL, g_launch_robot, NULL, NULL, 
		GNOME_APP_PIXMAP_DATA, robot_xpm, 0, 0, NULL }, 
	{ GNOME_APP_UI_ITEM, N_("_Start the game"), NULL, g_start_game, NULL, NULL, 
		GNOME_APP_PIXMAP_DATA, NULL , 0, 0, NULL }, 
	GNOMEUIINFO_MENU_EXIT_ITEM(on_exit_activate,NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] = 
{
	GNOMEUIINFO_HELP("gbatnav"),
	GNOMEUIINFO_MENU_ABOUT_ITEM(about,NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = 
{
	GNOMEUIINFO_MENU_GAME_TREE(gamemenu),
	GNOMEUIINFO_MENU_HELP_TREE(helpmenu),
	GNOMEUIINFO_END
};


/*****************************************************************************
 *                                    INIT SCREEN                            *
 ****************************************************************************/
void init_screen(void) 
{
	GtkWidget *window;
	GtkWidget *clist_box;
	char clist_text[CLIST_COLUMNS][50];
	char *clist_texts[CLIST_COLUMNS];
	GtkWidget *boton;
	static char *titles[] =
	{
		N_("Number"), N_("Name"), N_("hostname"), N_("Client version"), N_("Status"), N_("Last Token")
	};
	gint i ;

#ifdef ENABLE_NLS
# define ELEMENTS(x) (sizeof(x) / sizeof(x[0])) 
	{
		int i;
		for (i = 0; i < ELEMENTS(mainmenu); i++)
			mainmenu[i].label = gettext(mainmenu[i].label);
		for (i = 0; i < ELEMENTS(gamemenu); i++)
			gamemenu[i].label = gettext(gamemenu[i].label);
		for (i = 0; i < ELEMENTS(helpmenu); i++)
			helpmenu[i].label = gettext(helpmenu[i].label);
	}
#endif /* ENABLE_NLS */
   
	window = gnome_app_new ("gbnserver", _("Gnome Batalla Naval server") );
	gnome_app_create_menus(GNOME_APP(window), mainmenu);
	gtk_menu_item_right_justify(GTK_MENU_ITEM(mainmenu[1].widget));
	gtk_widget_realize (window);
   
	gtk_signal_connect ( GTK_OBJECT( window), "destroy",
		GTK_SIGNAL_FUNC( on_exit_activate ),
		GTK_OBJECT(window) );
	gtk_signal_connect ( GTK_OBJECT( window), "delete_event",
		GTK_SIGNAL_FUNC( on_exit_activate ),
		GTK_OBJECT(window) );

	
	clist_box = gtk_vbox_new (FALSE, 0);
	gnome_app_set_contents(GNOME_APP(window), clist_box);
	

	clist = gtk_clist_new_with_titles (CLIST_COLUMNS, titles);
	gtk_widget_show (clist);
	

	gtk_clist_set_row_height (GTK_CLIST (clist), 18);
	gtk_widget_set_usize (clist, -1, 300);

	
	gtk_box_pack_start( GTK_BOX( clist_box ), clist, TRUE, TRUE, 0);
	gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_EXTENDED);
      
	for (i = 0; i < CLIST_COLUMNS; i++) {
		gtk_clist_set_column_justification (GTK_CLIST (clist), i, GTK_JUSTIFY_CENTER);
		gtk_clist_set_column_auto_resize (GTK_CLIST (clist), i, TRUE);

		clist_texts[i] = clist_text[i];
		switch(i) {
		case 0:
			break;
		default:
			sprintf(clist_text[i],"- - -");
			break;
		}
	}

	for (i = 0; i < MAXPLAYER; i++) {
		sprintf(clist_texts[0],"%d",i);
		gtk_clist_append (GTK_CLIST (clist), clist_texts);
	}

	boton = gnome_stock_button(GNOME_STOCK_BUTTON_OK);
	gtk_box_pack_start( GTK_BOX( clist_box), boton, TRUE, TRUE, 0);

	gtk_widget_show_all (window);
}
