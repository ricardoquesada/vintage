/*	$Id: batnavggz.h,v 1.1 2001/04/04 21:31:01 riq Exp $	*/
/* Batalla Naval
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file tegggz.h
 * Funcion que maneja un poco el soporte para GGZ
 */

#ifndef __BATNAVS_BATNAVGGZ_H
#define __BATNAVS_BATNAVGGZ_H

int batnavggz_main_loop();
int batnavggz_find_ggzname( int fd, char *n, int len );
int batnavggz_del_fd( int fd );

#endif /* __BATNAVS_BATNAVGGZ_H */
