/*	$Id: version.h,v 1.2 2001/04/04 15:37:28 riq Exp $	*/
#ifndef __VERSION_H__
#define __VERSION_H__

#include <gnome.h>
#include <config.h>

#ifdef INET6
	#define BATVER    "Gnome Batalla Naval Server v"VERSION"+IPv6"
#else
	#define BATVER    "Gnome Batalla Naval Server v"VERSION
#endif
#define BNVERSION BATVER

#endif /* __VERSION_H__ */
