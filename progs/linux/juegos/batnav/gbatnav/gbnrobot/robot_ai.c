/*	$Id: robot_ai.c,v 1.2 2001/04/04 15:37:28 riq Exp $	*/

#include <gnome.h>
#include "robot_cliente.h"
#include "protocol.h"
#include "robot_ai.h"

static void pcell( int b, int x, int y, int estado )
{
	if(!(x<0 || x>9 || y<0 || y>9))
		cliente.boards[b][x][y]=estado;
}

static void inteliclient( int b)
{
	gint i,x,y;
   
	x=0;
	y=0;
   
	for(i=0;i<100;i++) {
		switch(cliente.boards[b][x][y]) {
		case HUNDIDO:
		case TOCADO:
			pcell(b,x-1,y-1,AGUA);
			pcell(b,x-1,y+1,AGUA);
			pcell(b,x+1,y-1,AGUA);							
			pcell(b,x+1,y+1,AGUA);
			break;
		case NOBARCO:
			if(x<9 && cliente.boards[b][x+1][y]==HUNDIDO)
				pcell(b,x,y,AGUA);
	     
			if(x>0 && cliente.boards[b][x-1][y]==HUNDIDO)
				pcell(b,x,y,AGUA);
     
			if(y<9 && cliente.boards[b][x][y+1]==HUNDIDO)
				pcell(b,x,y,AGUA);
     
			if(y>0 && cliente.boards[b][x][y-1]==HUNDIDO)
				pcell(b,x,y,AGUA);
			break;
		default:
			break;
		}
		x++;
		if(x==10) {
			x=0;
			y++;
		}
	}
}

static void buscar_next_free( int b, int i, int j, int *x, int *y )
{
	gint k;	

	for(k=0;k<100;k++) {
		if( cliente.boards[b][i][j]==NOBARCO ) {
			*x = i;
			*y = j;
			return;
		}
		i++;
		if(i==10) {
			i=0;
			j++;
			if(j==10)
				j=0;
		}

	}
	printf("gbnrobot %d: buscar_next_free %d: Aca no tengo que llegar\n",cliente.numjug,b);
	*x = 2;
	*y = 2;
}

static int buscar_tocado( int b, int *x, int *y)
{
	gint k;	
	gint i,j;

	i=0;j=0;
	for(k=0;k<100;k++) {
		if( cliente.boards[b][i][j]==TOCADO ) {
			*x = i;
			*y = j;
			return TRUE;
		}
		i++;
		if(i==10) {
			i=0;
			j++;
		}
	}
	return FALSE;
}


static int que_soy_board( int b, int x , int y )
{
	if( x<0 || x>9 || y<0 || y>9)
		return AGUA;	/* -1 */
	else
		return cliente.boards[b][x][y];
}

static int r_tocado_hit( int b, int *x, int *y, int i, int j )
{
	gint a;

	a=que_soy_board( b,*x,*y);
	if(a==NOBARCO)
		return TRUE;
	if(a==TOCADO) {
		*x=*x+i;
		*y=*y+j;
		return r_tocado_hit( b, x, y, i, j);
	} else
		return FALSE;
}

static void buscar_tocado_hit( int b, int *xx, int *yy )
{
	int x,y;

	
	x=*xx;
	y=*yy;
	
	if(!(que_soy_board(b,x,y-1)==TOCADO || que_soy_board(b,x,y+1)==TOCADO )) {
		if( r_tocado_hit( b, &x, &y, -1, 0 )) {
			*xx=x;
			*yy=y;
			return;
		}
		x=*xx;
		y=*yy;
		if( r_tocado_hit( b, &x, &y, +1, 0 )) {
			*xx=x;
			*yy=y;
			return;
		}
	}

	x=*xx;
	y=*yy;
	if( r_tocado_hit( b, &x, &y, 0, -1 )) {
		*xx=x;
		*yy=y;
		return;
	}
	x=*xx;
	y=*yy;
	if( r_tocado_hit( b, &x, &y, 0, +1 )) {
		*xx=x;
		*yy=y;
		return;
	}

}

void robot_ai( int *x, int*y)
{
	int xx,yy;

	switch( cliente.ai ) {

		case ROBOT_AI_VERYMORON:
			*x = (int) (10.0*rand()/(RAND_MAX+1.0));
			*y = (int) (10.0*rand()/(RAND_MAX+1.0));
			break;
		case ROBOT_AI_MORON:
			xx = (int) (10.0*rand()/(RAND_MAX+1.0));
			yy = (int) (10.0*rand()/(RAND_MAX+1.0));
			buscar_next_free( cliente.usrfrom, xx, yy, &xx, &yy );
			*x = xx;
			*y = yy;
			break;
		case ROBOT_AI_AVERAGE:
		case ROBOT_AI_HI:		/* TODO */
			inteliclient(cliente.usrfrom);
			if( buscar_tocado( cliente.usrfrom, &xx, &yy) ) {
				buscar_tocado_hit( cliente.usrfrom, &xx, &yy );
				*x = xx;
				*y = yy;
			} else {
				xx = (int) (10.0*rand()/(RAND_MAX+1.0));
				yy = (int) (10.0*rand()/(RAND_MAX+1.0));
				buscar_next_free( cliente.usrfrom, xx, yy, &xx, &yy );
				*x = xx;
				*y = yy;
			}
			break;
	}
}
