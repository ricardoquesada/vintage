/*	$Id: robot_cliente.h,v 1.4 2001/04/08 19:16:43 riq Exp $	*/
/*
 * Batalla Naval by riq
 * (riq@ciudad.com.ar)
 */

#ifndef __BN_ROBOT_CLIENTE__
#define __BN_ROBOT_CLIENTE__

#include <gnome.h>
#include "protocol.h"

#define ROBOTVER "Robot Client v0.2.3" 

typedef enum {
	ROBOT_AI_VERYMORON,			/* random shoots */
	ROBOT_AI_MORON,				/* random shoots w/o collision */
	ROBOT_AI_AVERAGE,
	ROBOT_AI_HI
} robotai_t;

typedef struct cliente {
	char server[MAXSERVERNAME];		/* server */
	gint sock;				/* socket */
	gint random;				/* Random Number */	
	gchar names[MAXPLAYER][MAXNAMELEN];	/* other players's name */
	gchar boards[MAXPLAYER][10][10];	/* Board de los enemigos */ 
	gint play[MAXPLAYER];			/* Estado de los jugadores */
	gint numjug;				/* number of player */
	gint usrfrom;				/* player who is owner of enemy window */
	gchar mitabla[10][10];			/* mi tabla de barcos */
	gint autostart;				/* autostart the game */
	gint tag;				/* FIXME: para que es esto */
	char mi_nombre[MAXNAMELEN];		/* mi nombre */
	robotai_t ai;				/* mi skill */
	int port;
	int with_nogui;
} CLIENTE, *PCLIENTE;



CLIENTE cliente;

GtkWidget *window;
GtkWidget *box;
GtkWidget *imagen;
GtkWidget *label;
GtkWidget *status_label;

gchar* pix;

GtkWidget *pbar;
GtkAdjustment *adj;

#endif
