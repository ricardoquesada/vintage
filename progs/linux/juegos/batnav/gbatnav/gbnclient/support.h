/*	$Id: support.h,v 1.2 2001/04/09 18:32:32 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __TEG_GUI_GNOME_SUPPORT_H
#define __TEG_GUI_GNOME_SUPPORT_H

void generic_window_set_parent (GtkWidget * dialog, GtkWindow   * parent);
char * load_pixmap_file( char *name );
void teg_dialog( char* title, char* bigtitle, char* data );
GtkWidget* teg_dialog_new( char* title, char* bigtitle );
void raise_and_focus (GtkWidget *widget);
void destroy_window( GtkWidget * widget, GtkWidget **window );

#endif /* __TEG_GUI_GNOME_SUPPORT_H */
