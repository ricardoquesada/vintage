/*	$Id: gbnclient.h,v 1.3 2001/04/06 15:15:22 riq Exp $	*/

/* 
	Includes de gbnclient
	Funcion que chequea el port y esas cosas
*/


#ifndef  __BN_GBNCLIENT__
# define __BN_GBNCLIENT__

# ifdef __cplusplus
extern "C" {
# endif __cplusplus

gint sacar_usrpage( gint jugador );

void remove_page( gint );

gint buscar_usr( gint usr );

gint expose_event( GtkWidget *widget, GdkEventExpose *event );

gint button_press_event (GtkWidget *widget, GdkEventButton *event);

gint expose_event_about (GtkWidget *widget, GdkEventExpose *event);

gint page_switch( GtkWidget *widget, GtkNotebookPage *page, gint page_num );

gint expose_event_right( GtkWidget *widget, GdkEventExpose *event );

gint button_press_event_right( GtkWidget *widget, GdkEventButton *event );

void iwtable( char *dest);

void filtermiboard();

void putintemp( char *table );

void showboard( GdkPixmap *pixmap );

void fillboard( char *filltable, int a );

char* inteliclient( char *table);

int send_start( void );
int send_board( void );
int send_status( void );
int send_robot( void );
int send_connect( void );
int send_disconnect( void );

void init_datos( void );

int init_cliente( void );


void bn_help( void );
   
# ifdef __cplusplus
}
# endif __cplusplus

# endif __BN_GBNCLIENT__
