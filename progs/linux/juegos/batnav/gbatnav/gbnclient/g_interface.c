/*	$Id: g_interface.c,v 1.12 2001/04/18 02:52:03 riq Exp $	*/
/******************************************************************************
 *                        FUNCIONES GNOMES  y/o GTK                           *
 *****************************************************************************/

#include <string.h>

#include <config.h>
#include <gnome.h>

#include "protocol.h"
#include "gbnclient.h"
#include "cliente.h"
#include "proceso.h"
#include "pantalla.h"
#include "random_board.h"
#include "configure.h"
#include "sendmsg.h"
#include "version.h"
#include "g_connect.h"
#include "support.h"

/* pixmaps */
#include "about.xpm"
#include "agua.xpm"
#include "icono.xpm"
#include "fondo.xpm"
#include "barco1.xpm"
#include "barco1_t.xpm"
#include "barco1_h.xpm"
#include "barco2h.xpm"
#include "barco2h_t.xpm"
#include "barco2h_h.xpm"
#include "barco2v.xpm"
#include "barco2v_t.xpm"
#include "barco2v_h.xpm"
#include "barco3h.xpm"
#include "barco3h_t.xpm"
#include "barco3h_h.xpm"
#include "barco3v.xpm"
#include "barco3v_t.xpm"
#include "barco3v_h.xpm"
#include "barco4h.xpm"
#include "barco4h_t.xpm"
#include "barco4h_h.xpm"
#include "barco4v.xpm"
#include "barco4v_t.xpm"
#include "barco4v_h.xpm"
#include "robot.xpm"
#include "random.xpm"
#include "ships.xpm"


/* callback de los botones */
void on_connect_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	conectar_view();
}
void on_disconnect_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	send_disconnect();
}
void on_launchrobot_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	char buf[2000];

	snprintf(buf,sizeof(buf)-1,BINDIR"/gbnrobot --port %d --server %s",usuario.port,usuario.server);
	buf[sizeof(buf)-1]=0;
	gnome_execute_shell(NULL,buf);
}
void on_random_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	generar();
}
void on_start_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	send_start();
}
void on_sendships_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	send_board();
}
void on_status_activate (GtkMenuItem *menuitem, gpointer user_data)
{
	send_status();
}
void on_exit_activate(GtkWidget *widget, gpointer data)
{
	send_disconnect();
	gtk_main_quit();
}


static void g_cerrar(GtkWidget *widget, int button,gpointer data)
{
	gnome_dialog_close(GNOME_DIALOG(widget));
}

gboolean g_box_lost()
{
	static GtkWidget *box = NULL;

	if (box == NULL ) {
		box = gnome_message_box_new (_("Game Over: You lost!"),
			GNOME_MESSAGE_BOX_INFO,
			GNOME_STOCK_BUTTON_OK,
			NULL);
		if( box==NULL) return FALSE;

		generic_window_set_parent( box, GTK_WINDOW(window) );
		
		gtk_signal_connect (GTK_OBJECT (box), "clicked",
			GTK_SIGNAL_FUNC (g_cerrar), NULL);

		gtk_window_set_modal( GTK_WINDOW(box),TRUE);
		gnome_dialog_close_hides(GNOME_DIALOG(box), TRUE);
	}
	gtk_widget_show( box );
	return TRUE;
}

gboolean g_box_win()
{
	static GtkWidget *box = NULL;

	if (box == NULL ) {
		box = gnome_message_box_new (_("Game Over: You are the Winner!"),
			GNOME_MESSAGE_BOX_INFO,
			GNOME_STOCK_BUTTON_OK,
			NULL);

		if( box==NULL) return FALSE;

		generic_window_set_parent( box, GTK_WINDOW(window) );

		gtk_signal_connect (GTK_OBJECT (box), "clicked",
			GTK_SIGNAL_FUNC (g_cerrar), NULL);

		gtk_window_set_modal( GTK_WINDOW(box),TRUE);
		gnome_dialog_close_hides(GNOME_DIALOG(box), TRUE);
	}
	gtk_widget_show( box );
	return TRUE;
}

void about( GtkWidget *widget, gpointer data )
{
	GtkWidget *href, *hbox;
	static GtkWidget *about = NULL;
	static const char *authors[] = {
		"Ricardo C. Quesada (riq@core-sdi.com)",
		NULL
	};

	if (!about) {

		about = gnome_about_new (_("Batalla Naval client"),
				VERSION,
				_("(C) 1998-2001 Ricardo C. Quesada"),
				(const char**) authors,
				_("A multiplayer networked battleship game."),
				"gnome-gbatnav.png");

		gtk_signal_connect (GTK_OBJECT (about), "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &about);


		hbox = gtk_hbox_new (TRUE, 0);
		href = gnome_href_new ("http://batnav.sourceforge.net", _("Batalla Naval Home Page"));
		gtk_box_pack_start (GTK_BOX (hbox), href, FALSE, FALSE, 0);
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (about)->vbox),
			    hbox, TRUE, FALSE, 0);
		gtk_widget_show_all (hbox);
	}

	gtk_widget_show_now (about);
	raise_and_focus (about);
}


enum {
	WID_G_CONNEC,
	WID_G_DISCON,
	WID_G_XXX0,
	WID_G_ROBOT,
};

GnomeUIInfo gamemenu[] = 
{
	{ GNOME_APP_UI_ITEM, N_("_Connect"), NULL, on_connect_activate, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, NULL, 0, 0, NULL },
	{ GNOME_APP_UI_ITEM, N_("_Disconnect"), NULL, on_disconnect_activate, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, NULL, 0, 0, NULL },
	GNOMEUIINFO_SEPARATOR, 
	{ GNOME_APP_UI_ITEM, N_("_Launch a robot"), NULL, on_launchrobot_activate, NULL, NULL, 
		GNOME_APP_PIXMAP_DATA, robot_xpm, 0, 0, NULL }, 
	GNOMEUIINFO_SEPARATOR, 
	GNOMEUIINFO_MENU_EXIT_ITEM(on_exit_activate,NULL),
	GNOMEUIINFO_END
};

enum {
	WID_A_RANDOM,
	WID_A_SEND,
	WID_A_START,
	WID_A_STATUS,
};

GnomeUIInfo actionsmenu[] =
{
	{ GNOME_APP_UI_ITEM, N_("_Generate random board"), NULL, on_random_activate, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, NULL, 0, 0, NULL },

	{ GNOME_APP_UI_ITEM, N_("_Send ships"), NULL, on_sendships_activate, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, NULL, 0, 0, NULL },

	{ GNOME_APP_UI_ITEM, N_("_Start"), NULL, on_start_activate, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, NULL, 0, 0, NULL },

	{ GNOME_APP_UI_ITEM, N_("Status"), NULL, on_status_activate, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, NULL, 0, 0, NULL },

	GNOMEUIINFO_END
};

GnomeUIInfo viewmenu[] = {
	{ GNOME_APP_UI_TOGGLEITEM, N_("View lot of messages"), N_("View lot of messages"),
	configure, NULL, NULL,
	GNOME_APP_PIXMAP_NONE, NULL,
	0, 0, NULL },

	GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] = 
{
	GNOMEUIINFO_HELP("gbatnav"),
	GNOMEUIINFO_MENU_ABOUT_ITEM(about,NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = 
{
        GNOMEUIINFO_MENU_GAME_TREE(gamemenu),
	{ GNOME_APP_UI_SUBTREE_STOCK, N_("_Actions"), NULL, actionsmenu, NULL, NULL, \
		(GnomeUIPixmapType) 0, NULL, 0,	(GdkModifierType) 0, NULL },
	GNOMEUIINFO_MENU_VIEW_TREE(viewmenu),
        GNOMEUIINFO_MENU_HELP_TREE(helpmenu),
	GNOMEUIINFO_END
};

enum {
	WID_CONNEC,
	WID_XXX0,
	WID_RANDOM,
	WID_SEND,
	WID_XXX1,
	WID_ROBOT,
	WID_XXX2,
	WID_START,
};


GnomeUIInfo main_toolbarinfo[] =
{
	{GNOME_APP_UI_ITEM, N_("Connect"), N_("Connect to server"),
	on_connect_activate, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW, 0, 0, NULL},

	GNOMEUIINFO_SEPARATOR,
	
	{GNOME_APP_UI_ITEM, N_("Random"), N_("Generate a random board"),
	on_random_activate, NULL, NULL,
	GNOME_APP_PIXMAP_DATA, random_xpm, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Send ships"), N_("Send ships to server"),
	on_sendships_activate, NULL, NULL,
	GNOME_APP_PIXMAP_DATA, ships_xpm, 0, 0, NULL},


	GNOMEUIINFO_SEPARATOR,

	{GNOME_APP_UI_ITEM, N_("Robot"), N_("Launch a robot"),
	on_launchrobot_activate, NULL, NULL,
	GNOME_APP_PIXMAP_DATA, robot_xpm, 0, 0, NULL},
  
	GNOMEUIINFO_SEPARATOR,

	{GNOME_APP_UI_ITEM, N_("Start"), N_("Start the game"),
	on_start_activate, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_TIMER, 0, 0, NULL},
	
	{GNOME_APP_UI_ENDOFINFO}
};

/*****************************************************************************
 *                                   INIT X                                  *
 *****************************************************************************/
void init_X( )
{
	GtkTooltips *tooltips;
	GtkWidget *inputline;
	GtkWidget *vbox_buttons;

#ifdef ENABLE_NLS
# define ELEMENTS(x) (sizeof(x) / sizeof(x[0])) 
	{
		int i;
		for (i = 0; i < ELEMENTS(mainmenu); i++)
			mainmenu[i].label = gettext(mainmenu[i].label);
		for (i = 0; i < ELEMENTS(gamemenu); i++)
			gamemenu[i].label = gettext(gamemenu[i].label);
		for (i = 0; i < ELEMENTS(helpmenu); i++)
			helpmenu[i].label = gettext(helpmenu[i].label);
	}
#endif /* ENABLE_NLS */
   
	window = gnome_app_new ("gbnclient", _("Gnome Batalla Naval client") );
	gtk_window_set_policy(GTK_WINDOW(window), FALSE, FALSE, TRUE);

	gnome_app_create_menus(GNOME_APP(window), mainmenu);
	gnome_app_create_toolbar(GNOME_APP(window), main_toolbarinfo);

/*	gtk_menu_item_right_justify(GTK_MENU_ITEM(mainmenu[3].widget)); */
	gtk_widget_realize (window);
   
	gtk_signal_connect ( GTK_OBJECT( window), "destroy",
		GTK_SIGNAL_FUNC( on_exit_activate ),
		GTK_OBJECT(window) );
	gtk_signal_connect ( GTK_OBJECT( window), "delete_event",
		GTK_SIGNAL_FUNC( on_exit_activate ),
		GTK_OBJECT(window) );
  
	tooltips=gtk_tooltips_new();
	gtk_object_set_data (GTK_OBJECT (window), "tooltips", tooltips);


	/* Box que contiene a box_left, box_button, box_right  */
	vbox = gtk_vbox_new ( FALSE, 0);
	gnome_app_set_contents(GNOME_APP(window), vbox );
   
	gtk_container_border_width ( GTK_CONTAINER(vbox), 0);
	gtk_widget_show ( vbox ); 

	/* box horizontal */
	hbox = gtk_hbox_new ( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(vbox), hbox );
	gtk_container_border_width ( GTK_CONTAINER(hbox), 0);
	gtk_widget_show( hbox );
   
	/******************** LEFT *******************/
	notebook_left = gtk_notebook_new();
	gtk_notebook_set_tab_pos( GTK_NOTEBOOK(notebook_left),GTK_POS_TOP );
	gtk_box_pack_start( GTK_BOX(hbox),notebook_left,FALSE,TRUE,0);
	gtk_widget_show (notebook_left);
   
	/* respecto al drawing_left */
	drawing_left = gtk_drawing_area_new();
	gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_left),200,200);
	gtk_widget_show( drawing_left );
   

	/* se�ales de la drawing area */
	gtk_signal_connect (GTK_OBJECT (drawing_left), "expose_event",
		(GtkSignalFunc) expose_event, NULL);
	gtk_signal_connect (GTK_OBJECT (drawing_left), "button_press_event",
		(GtkSignalFunc) button_press_event, NULL);
	gtk_widget_set_events (drawing_left, GDK_EXPOSURE_MASK
		|GDK_BUTTON_PRESS_MASK);

   
	/* respecto al drawing_about */
	hbox_text_help = gtk_hbox_new ( FALSE, 0);
	gtk_widget_show( hbox_text_help );
   
	text_help = gtk_text_new(NULL,NULL);
	gtk_box_pack_start( GTK_BOX(hbox_text_help), text_help, FALSE,TRUE,0);
	gtk_widget_show(text_help);
   
	vscrollbar_help = gtk_vscrollbar_new (GTK_TEXT (text_help)->vadj);
	gtk_box_pack_start( GTK_BOX(hbox_text_help), vscrollbar_help, FALSE,TRUE,0);
	gtk_widget_show (vscrollbar_help);
   
	label_left = gtk_label_new(_("My board"));
	gtk_notebook_append_page ( GTK_NOTEBOOK(notebook_left),drawing_left,label_left);
	label_left = gtk_label_new(_("Quick Help"));
	gtk_notebook_append_page ( GTK_NOTEBOOK(notebook_left),hbox_text_help,label_left);

	bn_help();
      
	/* center */

	vbox_buttons = gtk_vbox_new ( TRUE, 10);
	gtk_container_add ( GTK_CONTAINER (hbox), vbox_buttons );
	gtk_container_border_width( GTK_CONTAINER(vbox_buttons),20);
	gtk_widget_show( vbox_buttons );


	/* right */
	notebook_right = gtk_notebook_new();
	gtk_signal_connect ( GTK_OBJECT ( notebook_right ), "switch_page",
		GTK_SIGNAL_FUNC ( page_switch ), NULL );
	gtk_notebook_set_tab_pos( GTK_NOTEBOOK(notebook_right),GTK_POS_TOP );
	gtk_box_pack_start( GTK_BOX(hbox),notebook_right,FALSE,TRUE,0);
 	gtk_container_border_width( GTK_CONTAINER( notebook_right), 0 );
	gtk_notebook_set_scrollable( GTK_NOTEBOOK(notebook_right) , TRUE );
	gtk_notebook_set_show_tabs( GTK_NOTEBOOK(notebook_right) , TRUE );
	gtk_notebook_popup_enable( GTK_NOTEBOOK(notebook_right) );
	gtk_widget_realize( notebook_right );
	gtk_widget_show (notebook_right);

	drawing_right_about = gtk_drawing_area_new();
	gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_right_about),200,200);
	gtk_signal_connect (GTK_OBJECT (drawing_right_about), "expose_event",
		(GtkSignalFunc) expose_event_about, NULL );
	gtk_widget_set_events (drawing_right_about, GDK_EXPOSURE_MASK );
	gtk_widget_show( drawing_right_about);
	label_right_about = gtk_label_new("Batalla Naval" );
	gtk_widget_show( drawing_right_about);
	gtk_notebook_append_page( GTK_NOTEBOOK( notebook_right), drawing_right_about, label_right_about );
   

	/* Ventana de texto de abajo */
	separator = gtk_hseparator_new ();
	gtk_box_pack_start ( GTK_BOX(vbox), separator, FALSE,TRUE, 0);

	gtk_widget_show(separator);
   
	hbox_text = gtk_hbox_new ( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(vbox), hbox_text );
	gtk_widget_show( hbox_text );
   
	text = gtk_text_new(NULL,NULL);
	gtk_box_pack_start( GTK_BOX(hbox_text), text, TRUE,TRUE,0);
	gtk_widget_show(text);
   
	vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
	gtk_range_set_update_policy( GTK_RANGE( vscrollbar ), GTK_UPDATE_CONTINUOUS );
	gtk_box_pack_start( GTK_BOX(hbox_text), vscrollbar, FALSE,TRUE,0);
	gtk_widget_show (vscrollbar);
   
	gtk_text_freeze(GTK_TEXT(text));
	gtk_widget_realize(text); 
	gtk_text_insert( GTK_TEXT(text),NULL,NULL,NULL,"Gnome Batalla Naval client v"IPVERSION" by riq (c) 1998-2001" ,-1);
	gtk_text_thaw(GTK_TEXT(text));

  
	inputline = gtk_entry_new();
	gtk_box_pack_start( GTK_BOX( vbox ), inputline, FALSE, FALSE, 0 );
	gtk_widget_show(inputline);
	gtk_signal_connect(GTK_OBJECT(inputline), "activate",
		GTK_SIGNAL_FUNC(inputline_return), NULL);


	/* StatusBar */
	hbox_status = gtk_hbox_new ( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(vbox), hbox_status );
	gtk_container_border_width ( GTK_CONTAINER(hbox_status), 0);
	gtk_widget_show ( hbox_status ); 
   
	statusbar_right = gtk_statusbar_new();
	gtk_box_pack_end( GTK_BOX( hbox_status ), statusbar_right, TRUE,TRUE, 0);
	gtk_widget_show( statusbar_right );

	statusbar_left = gtk_statusbar_new();
	gtk_box_pack_end( GTK_BOX( hbox_status ), statusbar_left, TRUE,TRUE, 0);
	gtk_widget_show( statusbar_left );

	foot_left("Batalla Naval");
	foot_right("Gnome client v"VERSION);

	      
	/* Pixmaps */
	barco1 =    gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco1_xpm );
	barco1_t =  gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco1_t_xpm );
	barco1_h =  gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco1_h_xpm );

	barco2h =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2h_xpm );
	barco2h_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2h_t_xpm );
	barco2h_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2h_h_xpm );

	barco2v =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2v_xpm );
	barco2v_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2v_t_xpm );
	barco2v_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2v_h_xpm );

	barco3h =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3h_xpm );
	barco3h_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3h_t_xpm );
	barco3h_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3h_h_xpm );

	barco3v =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3v_xpm );
	barco3v_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3v_t_xpm );
	barco3v_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3v_h_xpm );

	barco4h =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4h_xpm );
	barco4h_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4h_t_xpm );
	barco4h_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4h_h_xpm );

	barco4v =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4v_xpm );
	barco4v_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4v_t_xpm );
	barco4v_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4v_h_xpm );

	fondo =     gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],fondo_xpm );
	agua =      gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],agua_xpm );
	about_pix = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],about_xpm );
	icono =     gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],icono_xpm );

	gdk_window_set_icon (window->window, NULL, icono , mask );

   
	/* Principal */   
	gtk_widget_show( window);
}

/**
 * @fn void update_sensi()
 */
void update_sensi()
{

	/* connect & disconnect */
	if( usuario.play == DISCON ) {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_CONNEC].widget, TRUE);
		gtk_widget_set_sensitive (gamemenu[WID_G_CONNEC].widget, TRUE);
		gtk_widget_set_sensitive (gamemenu[WID_G_DISCON].widget, FALSE);
	} else {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_CONNEC].widget, FALSE);
		gtk_widget_set_sensitive (gamemenu[WID_G_CONNEC].widget, FALSE);
		gtk_widget_set_sensitive (gamemenu[WID_G_DISCON].widget, TRUE);
	}

	/* random y send*/
	if( usuario.play == CONNEC ) {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_RANDOM].widget, TRUE);
		gtk_widget_set_sensitive (main_toolbarinfo[WID_SEND].widget, TRUE);
		gtk_widget_set_sensitive (actionsmenu[WID_A_RANDOM].widget, TRUE);
		gtk_widget_set_sensitive (actionsmenu[WID_A_SEND].widget, TRUE);

	} else {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_RANDOM].widget, FALSE);
		gtk_widget_set_sensitive (main_toolbarinfo[WID_SEND].widget, FALSE);
		gtk_widget_set_sensitive (actionsmenu[WID_A_RANDOM].widget, FALSE);
		gtk_widget_set_sensitive (actionsmenu[WID_A_SEND].widget, FALSE);
	}

	/* robot */
	if( usuario.play == CONNEC || usuario.play == BOARD ) {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_ROBOT].widget, TRUE);
		gtk_widget_set_sensitive (gamemenu[WID_G_ROBOT].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_ROBOT].widget, FALSE);
		gtk_widget_set_sensitive (gamemenu[WID_G_ROBOT].widget, FALSE);
	}
	
	/* start */
	if( usuario.play == BOARD ) {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_START].widget, TRUE);
		gtk_widget_set_sensitive (actionsmenu[WID_A_START].widget, TRUE);
	} else {
		gtk_widget_set_sensitive (main_toolbarinfo[WID_START].widget, FALSE);
		gtk_widget_set_sensitive (actionsmenu[WID_A_START].widget, FALSE);
	}

	/* status */
	if( usuario.play == DISCON ) {
		gtk_widget_set_sensitive (actionsmenu[WID_A_STATUS].widget, FALSE);
	} else {
		gtk_widget_set_sensitive (actionsmenu[WID_A_STATUS].widget, TRUE);
	}

	/* ggz */
	if( usuario.with_ggz ) {
		/* connect */
		gtk_widget_set_sensitive (main_toolbarinfo[WID_CONNEC].widget, FALSE);
		gtk_widget_set_sensitive (gamemenu[WID_G_CONNEC].widget, FALSE);

		/* robot */
		gtk_widget_set_sensitive (main_toolbarinfo[WID_ROBOT].widget, FALSE);
		gtk_widget_set_sensitive (gamemenu[WID_G_ROBOT].widget, FALSE);
	} 

	return;
}
