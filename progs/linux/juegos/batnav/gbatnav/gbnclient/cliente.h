/*	$Id: cliente.h,v 1.4 2001/04/06 15:15:22 riq Exp $	*/
/*
 * Batalla Naval (c) 1995,2000 Ricardo Quesada
 * (riq@ciudad.com.ar)
 * 
 * Funciones del Batalla Naval.
 * 
 */


#ifndef  __BN_CLIENTE__
# define __BN_CLIENTE__

# define LARGO 20
# define ANCHO 20
   
# include <gtk/gtk.h>
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <unistd.h>
# include "protocol.h"


/*
 * Estructuras y Funciones Principales
 */

struct usuario {
	char server[MAXSERVERNAME];	/* server name */
	int port;			/* server port */
	int random;			/* Random Number 0.59.38 */	
	char nombre[MAXNAMELEN];	/* user name */
	char names[MAXPLAYER][MAXNAMELEN];/* other players's name */
   
	int play;			/* already playing ? */
	int numjug;			/* number of player */
	int usrfrom;			/* player who is owner of enemy window */
   
	char tomsg[PROT_MAX_LEN];	/* usuadas para mandar mensajes */
   

	int  hide;			/* flag de hide my board  */
   
	int tag;			/* propio de GDK_INPUT_ADD */
	char tempclit[10*10];		/* Variable usada por el cliente */

	int pages[MAXPLAYER];		/* Variable de las paginas . only GTK ? */
/*	int autorobot;		*/	/* Autolanzamiento del robot */				
	int debug_level;		/* Debug level usado por ttyfill */
	int sock;			/* Socket del cliente al server */
	char mitabla[10][10];		/* mi tabla de barcos */
	char barquitos[4];		/* tama�os de los barcos 1 de 4, 2 de 3, etc. */

	int with_ggz;			/* enable ggz mode */
} usuario;

/* Variables - GTK */
/* contenedores */
GtkWidget *window;
GtkWidget *hbox;
GtkWidget *vbox;

/* left */
GtkWidget *notebook_left;
GtkWidget *label_left; 
GtkWidget *drawing_left;
GtkWidget *hbox_text_help;
GtkWidget *text_help;
GtkWidget *vscrollbar_help;

/* right */
GtkWidget *notebook_right;
GtkWidget *label_right_about;
GtkWidget *drawing_right_about;
GtkWidget *label_right[MAXPLAYER];
GtkWidget *label_right2[MAXPLAYER];
GtkWidget *drawing_right[MAXPLAYER];

/* otros */
GtkWidget *hbox_text;
GtkWidget *vscrollbar;
GtkWidget *separator;
GtkWidget *text;


/* Pixmaps */
GdkPixmap *barco1 ;
GdkPixmap *barco1_t ;
GdkPixmap *barco1_h ;
GdkPixmap *barco2h ;
GdkPixmap *barco2h_t ;
GdkPixmap *barco2h_h ;
GdkPixmap *barco2v ;
GdkPixmap *barco2v_t ;
GdkPixmap *barco2v_h ;
GdkPixmap *barco3h ;
GdkPixmap *barco3h_t ;
GdkPixmap *barco3h_h ;
GdkPixmap *barco3v ;
GdkPixmap *barco3v_t ;
GdkPixmap *barco3v_h ;
GdkPixmap *barco4h ;
GdkPixmap *barco4h_t ;
GdkPixmap *barco4h_h ;
GdkPixmap *barco4v ;
GdkPixmap *barco4v_t ;
GdkPixmap *barco4v_h ;

GdkPixmap *fondo ;
GdkPixmap *agua ;
GdkPixmap *about_pix ;
GdkPixmap *icono ;
GdkBitmap *mask ;

GtkWidget *hbox_status;
GtkWidget *statusbar_left;
GtkWidget *statusbar_right;

/* Usadas por Configure */
GtkWidget *conf_spinner_port;
GtkWidget *conf_entry_server;
GtkWidget *conf_entry_name;
GtkWidget *conf_check_button;
GtkWidget *conf_cb_ttyfill;

/* Usadas por Sendmsg */
GtkWidget *send_spinner_towho;
GtkWidget *send_entry_message;
GtkWidget *send_toggle_button;


#endif __BN_CLIENTE__
