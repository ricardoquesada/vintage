/*	$Id: random_board.h,v 1.1.1.1 2000/02/13 00:56:10 riq Exp $	*/
#ifndef __BN_RANDOM__
# define __BN_RANDOM__

# ifdef __cplusplus
extern "C" {
# endif __cplusplus

void
generar( void );

gint
posible( gint x, gint y, gint d, gint t);

void
buscar_poscicion( gint x, gint y, gint d, gint t);

gint
tamano( gint i );

void
poner_barco( gint x, gint y, gint d, gint t);

gint
entra( gint x, gint y, gint d, gint t);

gint
vacio( gint x, gint y);

# ifdef __cplusplus
}
# endif __cplusplus


#endif __BN_RANDOM__
