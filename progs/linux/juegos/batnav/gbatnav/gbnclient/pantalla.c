/*	$Id: pantalla.c,v 1.2 2001/04/04 15:37:27 riq Exp $	*/
/*
 * BATALLA NAVAL
 * Funciones de manejo de pantalla y otras
 */

#include <gnome.h>

#include "protocol.h"
#include "gbnclient.h"
#include "cliente.h"
#include "proceso.h"
#include "g_interface.h"
#include "random_board.h"


#define ABAJO 0
#define ARRIBA 1
#define IZQUIERDA 2
#define DERECHA 3

gint
vacio_derecha( gint x, gint y)
{
	gint k;
	if(x<1 || x>10 || y<1 || y>10)
		return TRUE;

	k=(x-1)+(y-1)*10;

	if( usuario.tempclit[k]==NOBARCO || usuario.tempclit[k]==AGUA )
		return TRUE;
	else
		return FALSE;
}

gint barco_contar( gint dir, gint x , gint y, gint lugar )
{
	if(lugar==0) { /* lugar==0 : izquierda */
		if(vacio(x+1,y+1))
			return 0;
	} else { /* lugar==1  : derecha */
		if(vacio_derecha(x+1,y+1))
			return 0;
	}
	switch(dir) {
	case ARRIBA:
		return barco_contar( dir,x,y-1,lugar)+1;
		break;
	case ABAJO:
		return barco_contar( dir,x,y+1,lugar)+1;
		break;
	case IZQUIERDA:
		return barco_contar( dir,x-1,y,lugar)+1;
		break;
	case DERECHA:
		return barco_contar( dir,x+1,y,lugar)+1;
		break;
	}
	/* Aca no tiene que llegar */
	return 0;
}
/* in: x,y (pos barco)  lugar( 0=izq, 1=der usado por barco_contar
   out: size (tama#o ), pos(0,1,2,3), hor= (horizontal==1)
*/
void barco_quesoy( gint x, gint y, gint *size, gint *pos, gint *hor, gint lugar)
{
	gint a=0,b=0;

	*hor=FALSE;
	a = barco_contar( ARRIBA, x , y-1, lugar );
	b = barco_contar( ABAJO, x, y+1, lugar );

	if(a+b==0) {
		*hor=TRUE;	
		a= barco_contar( IZQUIERDA, x-1, y, lugar );
		b= barco_contar( DERECHA, x+1, y, lugar );
	}
	*size=a+b;
	*pos=a;
}

void barco_dibu( int x,			/* x */ 
		int y,			/* y */ 
		int estado,		/* BARCO, TOCADO, HUNDIDO */ 
		int lugar,		/* 0=left,  1=der */
		int ii			/* se usa cuando lugar=1 (tab del notebook) */
		)
{
	gint size=0,pos=0,hor=0;
	GdkPixmap *barcos_enteros[2][4];
	gint verti=0;
	gint horiz=1;

	if (estado==BARCO) {
		barcos_enteros[0][0]=barco1;
		barcos_enteros[1][0]=barco1;
		barcos_enteros[0][1]=barco2v;
		barcos_enteros[1][1]=barco2h;
		barcos_enteros[0][2]=barco3v;
		barcos_enteros[1][2]=barco3h;
		barcos_enteros[0][3]=barco4v;
		barcos_enteros[1][3]=barco4h;
	} else if( estado==TOCADO) {
		barcos_enteros[0][0]=barco1_t;
		barcos_enteros[1][0]=barco1_t;
		barcos_enteros[0][1]=barco2v_t;
		barcos_enteros[1][1]=barco2h_t;
		barcos_enteros[0][2]=barco3v_t;
		barcos_enteros[1][2]=barco3h_t;
		barcos_enteros[0][3]=barco4v_t;
		barcos_enteros[1][3]=barco4h_t;
	} else { /* Hundido */
		barcos_enteros[0][0]=barco1_h;
		barcos_enteros[1][0]=barco1_h;
		barcos_enteros[0][1]=barco2v_h;
		barcos_enteros[1][1]=barco2h_h;
		barcos_enteros[0][2]=barco3v_h;
		barcos_enteros[1][2]=barco3h_h;
		barcos_enteros[0][3]=barco4v_h;
		barcos_enteros[1][3]=barco4h_h;
	}
	if(lugar==0) { /* lado izquierdo */
		if( usuario.play>=BOARD && usuario.play<=TURN ) {
			barco_quesoy( x,y, &size, &pos, &hor, lugar );
			if(hor==0) {
				verti=1;
				horiz=0;
			}
			gdk_draw_pixmap( drawing_left->window,
	     			drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
				barcos_enteros[hor][size],
				horiz*pos*ANCHO,verti*pos*LARGO,
				x*ANCHO+1,y*LARGO+1,
				ANCHO-1,LARGO-1);
		}
		else { /* Pone solo un barco. No sabe como estan */
 			gdk_draw_pixmap( drawing_left->window,
	     			drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
		     		barcos_enteros[0][0],
		     		0,0,
	     			x*ANCHO+1,y*LARGO+1,
	     			ANCHO-1,LARGO-1);
		}
	} else { /* lado derecho */
		if( usuario.play >= BOARD ) {
			barco_quesoy( x,y, &size, &pos, &hor, lugar );
			if(hor==0) {
				verti=1;
				horiz=0;
			}
     			gdk_draw_pixmap( drawing_right[ii]->window,
		     		drawing_right[ii]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[ii])],
				barcos_enteros[hor][size],
				horiz*pos*ANCHO,verti*pos*LARGO,
				x*ANCHO+1,y*LARGO+1,
				ANCHO-1,LARGO-1);
		} else { /* Pone solo un barco. No sabe como estan */
     			gdk_draw_pixmap( drawing_right[ii]->window,
		     		drawing_right[ii]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[ii])],
		     		barcos_enteros[0][0],
		     		0,0,
	     			x*ANCHO+1,y*LARGO+1,
	     			ANCHO-1,LARGO-1);
		}
	}

}

void
textfill(gint dl, gchar *fmt, ... )
{
	gchar messg[200];
	gchar messg2[200];
   	gfloat new_value ;
	gint h,w;

	va_list args;

   	if(dl <= usuario.debug_level) {
		va_start( args, fmt );

		sprintf(messg,"\n");	
		vsprintf(messg2,fmt,args);
		strncat(messg,messg2,200);
		va_end( args );

	   	gtk_text_freeze(GTK_TEXT(text));
   		gtk_widget_realize(text);
   		gtk_text_insert( GTK_TEXT(text),NULL,NULL,NULL,messg,-1);
   		gtk_text_thaw(GTK_TEXT(text));
  
   		gdk_window_get_size( GTK_TEXT( text )->text_area, &w, &h );

   		if( GTK_RANGE( vscrollbar )->adjustment->upper >= h ) {
			new_value = GTK_RANGE( vscrollbar )-> adjustment->upper - h;
			GTK_RANGE(vscrollbar)->adjustment->value = new_value;
			GTK_RANGE(vscrollbar)->timer = 0; /* FIXME para que sirver ? */
			gtk_signal_emit_by_name (GTK_OBJECT (GTK_RANGE(vscrollbar)->adjustment), "value_changed");
     		}
	}
}


/* Pinta mi tabla con algun color o tipo de barco */
void pmicell(int x,int y,int color)
{
   	if(x<10 && x>=0 && y>=0 && y<10) {
		switch( color ) {
   		case NOBARCO:
     			gdk_draw_pixmap( drawing_left->window,
		     		drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
		     		fondo,
		     		x*ANCHO+1,y*LARGO+1,
		     		x*ANCHO+1,y*LARGO+1,
		     		ANCHO-1,LARGO-1);
     			break;
   		case AGUA:
     			gdk_draw_pixmap( drawing_left->window,
		     		drawing_left->style->fg_gc[GTK_WIDGET_STATE(drawing_left)],
		     		agua,
		     		0,0,
		     		x*ANCHO+1,y*LARGO+1,
		     		ANCHO-1,LARGO-1);
     			break;
   		case BARCO:
			barco_dibu( x, y, BARCO, 0, 0 );
     			break;
   		case TOCADO:
			barco_dibu( x, y, TOCADO, 0, 0);
			break;
   		case HUNDIDO:
			barco_dibu( x, y, HUNDIDO, 0, 0);
     			break;
	  	}
     	}
}

/* Pinta alguna celda del enemigo con algun color o barco */
void 
ptucell(int x,int y,int color)
{
   	gint i;
   
	i = gtk_notebook_current_page( GTK_NOTEBOOK( notebook_right ) );
	i = usuario.pages[ i ]; 

   	if(x<10 && x>=0 && y>=0 && y<10) {
		switch( color ) {
		case NOBARCO:
     			gdk_draw_pixmap( drawing_right[i]->window,
	     			drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
	     			fondo,
	     			x*ANCHO+1,y*LARGO+1,
	     			x*ANCHO+1,y*LARGO+1,
	     			ANCHO-1,LARGO-1);
     			break;
   		case AGUA:
     			gdk_draw_pixmap( drawing_right[i]->window,
		     		drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
		     		agua,
		     		0,0,
		     		x*ANCHO+1,y*LARGO+1,
		     		ANCHO-1,LARGO-1);
     			break;
   		case BARCO:
			barco_dibu( x, y, BARCO, 1, i );
     			break;
   		case TOCADO:
     			gdk_draw_pixmap( drawing_right[i]->window,
		     		drawing_right[i]->style->fg_gc[GTK_WIDGET_STATE(drawing_right[i])],
		     		barco1_t,
		     		0,0,
		     		x*ANCHO+1,y*LARGO+1,
		     		ANCHO-1,LARGO-1);
			break;
   		case HUNDIDO:
			barco_dibu( x, y, HUNDIDO, 1, i );
     			break;
	  	}
     	}
}

/* Funcion que pone mensaje en la parte inferior derecha de la pantalla */
void 
foot_right( char* text )
{
	gtk_statusbar_pop(  GTK_STATUSBAR(statusbar_right), 1 );
	gtk_statusbar_push( GTK_STATUSBAR(statusbar_right), 1, text);
}

/* Funcion que pone mensaje en la parte inferior izquierda de la pantalla */
void 
foot_left( char* text )
{
	gtk_statusbar_pop(  GTK_STATUSBAR(statusbar_left), 1 );
	gtk_statusbar_push( GTK_STATUSBAR(statusbar_left), 1, text);
}
