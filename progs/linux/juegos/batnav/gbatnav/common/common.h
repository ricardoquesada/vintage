/*	$Id: common.h,v 1.2 2001/04/06 15:37:16 riq Exp $	*/
/* Batalla Naval
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file common.h
 */
#ifndef __BATNAV_COMMON_H
#define __BATNAV_COMMON_H

typedef int BOOLEAN, *PBOOLEAN;

typedef enum {
	BATNAV_STATUS_SUCCESS = 0,
	BATNAV_STATUS_ERROR = 1,
	BATNAV_STATUS_NOTFOUND = 2,
	BATNAV_STATUS_NOMEM = 3,
	BATNAV_STATUS_CONNCLOSED = 4,
	BATNAV_STATUS_TOKENNOTFOUND = 5,
	BATNAV_STATUS_TOKENNULL = 6,
	BATNAV_STATUS_PLAYERNOTFOUND = 7,
	BATNAV_STATUS_PARSEERROR = 8,
	BATNAV_STATUS_NOTCONNECTED = 9,
	BATNAV_STATUS_UNEXPECTED = 10,
	BATNAV_STATUS_FILENOTFOUND = 11,
	BATNAV_STATUS_GAMEOVER = 12,
} BATNAV_STATUS, *PBATNAV_STATUS;

void dont_run_as_root();

#endif /* __BATNAV_COMMON_H */
