/*	$Id: bnwrite.c,v 1.3 2001/04/05 17:00:40 riq Exp $	*/
/*
 * Funcion bnwrite
 */

#include <stdarg.h>
#include <stdio.h>

#include "protocol.h"
#include "net.h"

int
bnwrite(int fd, char *fmt, ...)
{
	int a;
	char t[1000];
	va_list args;
	va_start( args, fmt );

	if(fd==-1) return -1;

	vsprintf(t,fmt,args);
	va_end(args);
	
	a = net_printf(fd,"%s\n",t);
	if(a<1) {
		perror("bnwrite error:\n");
	}
	return a;
}
