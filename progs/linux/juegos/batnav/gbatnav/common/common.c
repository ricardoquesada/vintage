/*	$Id: common.c,v 1.1 2001/04/06 15:37:16 riq Exp $	*/
/********************************************************************** 
 Freeciv - Copyright (C) 1996 - A Kjeldberg, L Gregersen, P Unold
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
***********************************************************************/
/* Funciones sacadas del freeciv con algunas modificaciones */

#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

void dont_run_as_root()
{
	if (getuid()==0 || geteuid()==0) {
		fprintf(stderr, "Fatal error: You can't run this program as superuser\n");
		fprintf(stderr, "Try to run it as a different user\n");
		exit(1);
	}
}
