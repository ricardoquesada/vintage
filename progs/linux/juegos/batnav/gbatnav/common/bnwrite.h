/*	$Id: bnwrite.h,v 1.1 2001/04/04 15:37:27 riq Exp $	*/
/*
 * Funcion bnwrite
 */

#ifndef __BNWRITE_H__
#define __BNWRITE_H__

void broadcast( char *fmt, ...);
int bnwrite(int, gchar *fmt, ...);

#endif /* __BNWRITE_H__ */
