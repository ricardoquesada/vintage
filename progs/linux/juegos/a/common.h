/*	$Id: common.h,v 1.23 2000/10/28 20:08:00 riq Exp $	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file common.h
 */
#ifndef __COMMON_H
#define __COMMON_H

#ifndef TRUE 
#define TRUE 1
#define FALSE 0
#endif /* !TRUE */

typedef int BOOLEAN, *PBOOLEAN;

typedef enum {
	STATUS_SUCCESS = 0,
	STATUS_ERROR = 1,
	STATUS_NOTFOUND = 2,
	STATUS_NOMEM = 3,
	STATUS_CONNCLOSED = 4,
	STATUS_TOKENNOTFOUND = 5,
	STATUS_TOKENNULL = 6,
	STATUS_PLAYERNOTFOUND = 7,
	STATUS_PARSEERROR = 8,
	STATUS_NOTCONNECTED = 9,
	STATUS_UNEXPECTED = 10,
	STATUS_FILENOTFOUND = 11,
	STATUS_GAMEOVER = 12,
} STATUS, *PSTATUS;

enum {
	M_INF,
	M_DBG,
	M_ERR,
};

enum {
	DIRECCION_IZQ = 1,
	DIRECCION_DER = 2,
	DIRECCION_UP = 4,
	DIRECCION_DOWN = 8,
	DIRECCION_JUMP_UP = 16,
	DIRECCION_JUMP_DOWN = 32,
};


#ifndef MAX
#define MAX(a,b)	(((a) > (b)) ? (a): (b))
#endif

#ifndef MIN
#define MIN(a,b)	(((a) < (b)) ? (a): (b))
#endif


/*
 * Estas macros fueron sacadas del DDK de Windows NT 4.0
 */
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY *Flink;
	struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;


#define LENTRY_NULL {NULL,NULL}

#define LIST_NEXT(Entry) (((PLIST_ENTRY)Entry)->Flink)
#define LIST_PREV(Entry) (((PLIST_ENTRY)Entry)->Blink)

//
//  Doubly-linked list manipulation routines.  Implemented as macros
//  but logically these are procedures.
//

//
//  VOID
//  InitializeListHead(
//      PLIST_ENTRY ListHead
//      );
//

#define InitializeListHead(ListHead) (\
    (ListHead)->Flink = (ListHead)->Blink = (ListHead))

//
//  BOOLEAN
//  IsListEmpty(
//      PLIST_ENTRY ListHead
//      );
//

#define IsListEmpty(ListHead) \
    ((ListHead)->Flink == (ListHead))

//
//  PLIST_ENTRY
//  RemoveHeadList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveHeadList(ListHead) \
    (ListHead)->Flink;\
    {RemoveEntryList((ListHead)->Flink)}

//
//  PLIST_ENTRY
//  RemoveTailList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveTailList(ListHead) \
    (ListHead)->Blink;\
    {RemoveEntryList((ListHead)->Blink)}

//
//  VOID
//  RemoveEntryList(
//      PLIST_ENTRY Entry
//      );
//

#define RemoveEntryList(Entry) {\
    PLIST_ENTRY _EX_Blink;\
    PLIST_ENTRY _EX_Flink;\
    _EX_Flink = (Entry)->Flink;\
    _EX_Blink = (Entry)->Blink;\
    _EX_Blink->Flink = _EX_Flink;\
    _EX_Flink->Blink = _EX_Blink;\
    }

//
//  VOID
//  InsertTailList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertTailList(ListHead,Entry) {\
    PLIST_ENTRY _EX_Blink;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_Blink = _EX_ListHead->Blink;\
    (Entry)->Flink = _EX_ListHead;\
    (Entry)->Blink = _EX_Blink;\
    _EX_Blink->Flink = (Entry);\
    _EX_ListHead->Blink = (Entry);\
    }

//
//  VOID
//  InsertHeadList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertHeadList(ListHead,Entry) {\
    PLIST_ENTRY _EX_Flink;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_Flink = _EX_ListHead->Flink;\
    (Entry)->Flink = _EX_Flink;\
    (Entry)->Blink = _EX_ListHead;\
    _EX_Flink->Blink = (Entry);\
    _EX_ListHead->Flink = (Entry);\
    }

#endif /* __COMMON_H */
