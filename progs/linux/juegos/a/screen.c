/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file screen.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>

#include "common.h"
#include "data.h"

STATUS screen_init( int x, int y )
{
	screen = SDL_SetVideoMode(x, y, 0, SDL_SWSURFACE);
	if ( screen == NULL ) {
		fprintf(stderr, "Couldn't set %dx%d video mode: %s\n",x,y, SDL_GetError());
		return STATUS_ERROR;
	}
	return STATUS_SUCCESS;
}

STATUS screen_drawimage(SDL_Surface * surf, int x, int y, int update)
{
	SDL_Rect dest;
	
	dest.x = x;
	dest.y = y;
	dest.w = surf->w;
	dest.h = surf->h;
	
	SDL_BlitSurface(surf, NULL, screen, &dest);
	
	if (update)
		SDL_UpdateRect(screen, dest.x, dest.y, dest.w, dest.h);

	return STATUS_SUCCESS;
}
