/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file global.c
 * Contine las variables globales relacionadas con data
 */

#include <SDL.h>
#include <SDL_mixer.h>
#include "data.h"

/* Surfaces */
SDL_Surface *screen = NULL;		/**< main screen */
SDL_Surface *img_heroe_izq[MAX_SURF_HEROE];	/**< sprites del heroe izq */
SDL_Surface *img_tiles[MAX_SURF_TILE];	/**< tiles */
SDL_Surface *img_malo[MAX_SURF_MALO];	/**< sprite de los malos */

Mix_Music *music;

char cur_tile[MAX_MAP_Y][MAX_MAP_X];

