/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file setup.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include "common.h"
#include "game.h"
#include "data.h"

SDL_Surface *LoadImage(char *datafile, int transparent)
{
	SDL_Surface *image, *surface;

	image = IMG_Load(datafile);
	if ( image == NULL ) {
		fprintf(stderr, "Couldn't load image %s: %s\n",
					datafile, IMG_GetError());
		return(NULL);
	}

	if ( transparent ) {
		/* Assuming 8-bit BMP image */
		SDL_SetColorKey(image, (SDL_SRCCOLORKEY|SDL_RLEACCEL),
						*(Uint8 *)image->pixels);
	}
	surface = SDL_DisplayFormat(image);
	SDL_FreeSurface(image);
	return(surface);
}

STATUS setup_sprites()
{
	img_heroe_izq[0] = LoadImage( "data/sprites/izq_01.png",1);
	img_heroe_izq[1] = LoadImage( "data/sprites/izq_02.png",1);
	img_heroe_izq[2] = LoadImage( "data/sprites/izq_03.png",1);
	img_heroe_izq[3] = LoadImage( "data/sprites/izq_04.png",1);
	img_heroe_izq[4] = LoadImage( "data/sprites/izq_05.png",1);

	img_malo[0] = LoadImage( "data/sprites/malo_01.png",1);
	img_malo[1] = NULL;
	img_malo[2] = NULL;

	return STATUS_SUCCESS;
}

STATUS setup_tiles()
{
	img_tiles[0] = LoadImage( "data/tiles/tile_01.png", 0);
	img_tiles[1] = LoadImage( "data/tiles/tile_02.png", 0);
	img_tiles[2] = LoadImage( "data/tiles/tile_03.png", 0);
	img_tiles[3] = LoadImage( "data/tiles/tile_04.png", 0);
	img_tiles[4] = LoadImage( "data/tiles/tile_05.png", 0);
	img_tiles[5] = LoadImage( "data/tiles/tile_06.png", 0);
	return STATUS_SUCCESS;
}

STATUS setup_map( int number )
{
	char * filename;
	char line[MAX_MAP_X + 5];
	FILE * fi;
	int y;

	filename = strdup("data/maps/map_01.dat");
	fi = fopen(filename, "r");
	if (fi == NULL) {
		perror(filename);
		return STATUS_ERROR;
	}

	free(filename);
  
	for (y = 0; y < MAX_MAP_Y; y++) {
		fgets(line, MAX_MAP_X + 5, fi);
		line[strlen(line) - 1] = '\0';
		strcpy(cur_tile[y], line);
	}
	
	fclose(fi);
	return STATUS_SUCCESS;
}

STATUS setup_music()
{
	music = Mix_LoadMUS("commando.mid");
	if( music )
		return STATUS_SUCCESS;
	return STATUS_ERROR;
}

STATUS free_data()
{
	int i;

	for(i=0;i<MAX_SURF_HEROE;i++)
		if( img_heroe_izq[i] ) SDL_FreeSurface( img_heroe_izq[i] );
	for(i=0;i<MAX_SURF_TILE;i++)
		if( img_tiles[i] ) SDL_FreeSurface( img_tiles[i] );
//	for(i=0;i<MAX_SURF_MALO;i++)
//		if( img_malo[i] ) SDL_FreeSurface( img_malo[i] );

	return STATUS_SUCCESS;
}

STATUS free_music()
{
	Mix_FreeMusic(music);
	return STATUS_SUCCESS;
}
