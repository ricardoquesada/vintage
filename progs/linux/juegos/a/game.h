/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file game.h
 */

#ifndef __GAME_H
#define __GAME_H

typedef struct _game {
	int level;	/**< nivel del juego */
	int time;	/**< time left */
	int scroll_x;	/**< pos x del mapa */
	int scroll_y;	/**< pos y del mapa */
} game_t, pgame_t;


typedef struct _sprite {
	LIST_ENTRY next;	/**< siguiente en la lista */
	int x,y,z;		/**< pos x, y, z */
	SDL_Surface *surf;	/**< surface, width, heigth */
} sprite_t, *psprite_t;

/* deriva de sprite */
typedef struct _jugador {
	sprite_t spr;		/**< sprite. Tiene que ser 1ro */
	int score;		/**< puntos */
	int vidas;		/**< vidas restantes */
	int bombas;		/**< bombas restantes */
	int direccion;		/**< direccion */
	int energy;		/**< energia */
} jugador_t, *pjugador_t;

/* deriva de sprite */
typedef struct _enemy {
	sprite_t spr;		/**< energia */
	int energy;
} enemy_t, *penemy_t;

/* prototipos */
STATUS game_init();
STATUS game_run();

#endif /* __GAME_H */
