/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file global.c
 * Contine las variables globales relacionadas con data
 */

#ifndef __DATA_H
#define __DATA_H

#include <SDL.h>
#include <SDL_mixer.h>

/* cuantos frames de cada sprite */
#define MAX_SURF_HEROE	(5)
#define MAX_SURF_TILE	(6)
#define MAX_SURF_MALO	(3)

/* tamanio de los mapas (levels) */
#define MAX_MAP_X	(30)
#define MAX_MAP_Y	(16)

/* size de cada tile */
#define TILE_SIZE_X	(32)
#define TILE_SIZE_Y	(32)

/* maximo que puede saltar */
#define MAX_JUMP_Z	(60)


#define FPS (1000 / 500)

/* Surfaces */
extern SDL_Surface *screen;				/**< main screen */
extern SDL_Surface *img_heroe_izq[MAX_SURF_HEROE];	/**< sprites del heroe izq */
extern SDL_Surface *img_tiles[MAX_SURF_TILE];		/**< tiles */
extern SDL_Surface *img_malo[MAX_SURF_MALO];		/**< sprite de los malos */

extern Mix_Music *music;
extern char cur_tile[MAX_MAP_Y][MAX_MAP_X];

#endif /* __DATA_H */
