/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file main.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <SDL.h>
#include <SDL_mixer.h>
#include <SDL_image.h>

#include "common.h"
#include "setup.h"
#include "game.h"
#include "data.h"
#include "screen.h"


int main(int argc, char *argv[])
{
	/* Initialize the SDL library */
	if ( SDL_Init(SDL_INIT_AUDIO|SDL_INIT_VIDEO) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL: %s\n",SDL_GetError());
		exit(2);
	}
	atexit(SDL_Quit);

	/* Open the audio device */
	if ( Mix_OpenAudio(11025, AUDIO_U8, 1, 512) < 0 ) {
		fprintf(stderr,
		"Warning: Couldn't set 11025 Hz 8-bit audio\n- Reason: %s\n",
							SDL_GetError());
	}

	/* Open the display device */

	if( screen_init( SCREEN_X, SCREEN_Y ) != STATUS_SUCCESS )
		goto error;

	if( setup_sprites() != STATUS_SUCCESS )
		goto error;

	if( setup_music() != STATUS_SUCCESS )
		goto error;
	
	if( setup_tiles() != STATUS_SUCCESS )
		goto error;

	if( ! Mix_PlayingMusic() )
		Mix_PlayMusic(music,0);

	if( game_init() != STATUS_SUCCESS )
		goto error;

	do {
		int level=0;
		if( setup_map(level++) != STATUS_SUCCESS )
			goto error;
		game_run();
	} while( 0);

	/* Quit */
error:
	free_data();
	free_music();
	Mix_CloseAudio();
	exit(0);
}
