/*	$Id$	*/
/* QUE NOMBRE LE PONGO
 *
 * Copyright (C) 2000 Ricardo Quesada
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/**
 * @file game.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <SDL.h>
#include "common.h"
#include "game.h"
#include "data.h"
#include "screen.h"

game_t	game;
jugador_t jug;
enemy_t enemy;

LIST_ENTRY g_sprites_l;

void drawshape(int x, int y, unsigned char c)
{
	int i;
	if (c == '.')
		screen_drawimage(img_tiles[0], x, y, 0);
	else {
		i = (c - 'A') + 1;

		if( i < 0 || i >=  MAX_SURF_TILE ) i = 0;

		screen_drawimage(img_tiles[i], x, y, 0);
	}
}

void put_in_z( psprite_t new )
{
	PLIST_ENTRY l = g_sprites_l.Flink;
	psprite_t s;
	assert(new);

	while( !IsListEmpty( &g_sprites_l ) && (l != &g_sprites_l) ) {
		s = (psprite_t) l;

		if( s->y + s->surf->h > new->y+new->surf->h ) {
			InsertHeadList( l->Blink, (PLIST_ENTRY) new );
			return;
		}
		l = LIST_NEXT(l);
	}
	/* si llego, aca lo inserto */
	InsertTailList( &g_sprites_l, (PLIST_ENTRY) new );
}

void draw_sprites()
{
	SDL_Rect dst;
	while( !IsListEmpty( &g_sprites_l )) {
		psprite_t s = (psprite_t) RemoveHeadList( &g_sprites_l );
		dst.x = s->x;
		dst.y = s->y - s->z;
		dst.w = 0;
		dst.h = 0;
		SDL_BlitSurface( s->surf, NULL, screen, &dst );
	}
}

void draw_background()
{
	int yy,xx;
	for (yy = 0; yy < 13; yy++) {
		for (xx = 0; xx < 13; xx++) {
			drawshape(xx * TILE_SIZE_X - (game.scroll_x % TILE_SIZE_X),
				yy * TILE_SIZE_Y - (game.scroll_y % TILE_SIZE_Y),
				cur_tile[yy + (game.scroll_y / TILE_SIZE_Y)][xx + (game.scroll_x / TILE_SIZE_X)]);
		}
	}
}

STATUS game_init()
{
	game.level = 0;
	game.time = 200;
	game.scroll_x =0;
	game.scroll_y =0;


	jug.score = 0;
	jug.vidas = 5;
	jug.bombas = 3;
	jug.energy = 100;
	jug.direccion = DIRECCION_IZQ;
	jug.spr.x = 0;
	jug.spr.y = 100;
	jug.spr.z = 0;
	jug.spr.surf = img_heroe_izq[0];

	enemy.spr.x = 100;
	enemy.spr.y = 100;
	enemy.spr.surf = img_malo[0];

	InitializeListHead( &g_sprites_l );

	return STATUS_SUCCESS;
}

void update_heroe_surf()
{
	static int i=0;
	static int ptr=0;
	if( i++ % 25 ==0 ) {
		jug.spr.surf = img_heroe_izq[ ptr++ % MAX_SURF_HEROE ];
	}
}

STATUS game_run(void)
{
	SDL_Event event;
	SDL_Rect dst;
	int left=0;
	int rigth=0;
	int up=0;
	int down=0;
	int jump=0;
	Uint32 last_time, now_time;

	/* Paint sprite */
	dst.x = screen->w/2;
	dst.y = screen->h/2;
	dst.w = 0;
	dst.h = 0;
	SDL_BlitSurface( jug.spr.surf, NULL, screen, &dst );

	SDL_UpdateRect(screen, 0, 0, 0, 0);

	do {
		
		last_time = SDL_GetTicks();

		while (SDL_PollEvent(&event)) {

			if (event.type == SDL_QUIT) {
				return STATUS_SUCCESS;
			} else if (event.type == SDL_KEYDOWN) {
				SDLKey key;
				key = event.key.keysym.sym;
				
				if (key == SDLK_ESCAPE)
					return STATUS_SUCCESS;

				if (key == SDLK_RIGHT) {
					rigth=1;
				} else if (key == SDLK_LEFT) {
					left=1;
				}

				if (key == SDLK_UP) {
					up=1;
				} else if (key == SDLK_DOWN) {
					down=1;
				}

				if( key == SDLK_RCTRL || key == SDLK_LCTRL )
					jump=1;



			} else if (event.type == SDL_KEYUP) {
				SDLKey key;
				key = event.key.keysym.sym;
				
				if (key == SDLK_ESCAPE)
					return STATUS_SUCCESS;

				if (key == SDLK_RIGHT) {
					rigth=0;
				} else if (key == SDLK_LEFT) {
					left=0;
				}
				if (key == SDLK_UP) {
					up=0;
				} else if (key == SDLK_DOWN) {
					down=0;
				}

				if( key == SDLK_RCTRL || key == SDLK_LCTRL )
					jump=0;
			}
		}
		if( left ) {
			if( --jug.spr.x < 20 ) {
				jug.spr.x=20;
				if( --game.scroll_x < 0) 
					game.scroll_x = 0;
			}
		} else if ( rigth ) {
			jug.spr.x++;
			if( jug.spr.x>(SCREEN_X-150) ) { 
				jug.spr.x=(SCREEN_X-150);
				if( ++game.scroll_x > (TILE_SIZE_X*MAX_MAP_X-SCREEN_X) )
					game.scroll_x = (TILE_SIZE_X*MAX_MAP_X-SCREEN_X );
			}
		}
		if( up ) {
			if( --jug.spr.y + jug.spr.surf->h < TILE_SIZE_Y*3 ) {
				jug.spr.y=TILE_SIZE_Y*3 -jug.spr.surf->h;
				if( --game.scroll_y < 0)
					game.scroll_y=0;
			}
		} else if( down ) {
			if( ++jug.spr.y> (SCREEN_Y-100) ) {
				jug.spr.y=(SCREEN_Y-100);
				if( ++game.scroll_y > (TILE_SIZE_Y*MAX_MAP_Y-SCREEN_Y) )
					game.scroll_y = (TILE_SIZE_Y*MAX_MAP_Y-SCREEN_Y );
			}
		}

		if( jump ) {
			/* si no esta saltando, salta */
			if( !(jug.direccion & (DIRECCION_JUMP_UP | DIRECCION_JUMP_DOWN)) )
				jug.direccion |= DIRECCION_JUMP_UP;
		}

		if( jug.direccion & DIRECCION_JUMP_UP ) {
			if( ++jug.spr.z >= MAX_JUMP_Z ) {
				jug.spr.z = MAX_JUMP_Z;
				jug.direccion &= ~DIRECCION_JUMP_UP;
				jug.direccion |= DIRECCION_JUMP_DOWN;
			}
				
		}

		if( jug.direccion & DIRECCION_JUMP_DOWN ) {
			if( --jug.spr.z <= 0 ) {
				jug.spr.z = 0;
				jug.direccion &= ~DIRECCION_JUMP_DOWN;
			}
		}

		if( up || down || left || rigth )
			update_heroe_surf();

		/* dibuja el fondo de la pantalla */

		draw_background();

		put_in_z( (psprite_t) &jug );
		put_in_z( (psprite_t) &enemy );
		draw_sprites();

//		draw_header();

		SDL_UpdateRect(screen, 0, 0, 0, 0);

		now_time = SDL_GetTicks();
		if (now_time < last_time + FPS)
			SDL_Delay(last_time + FPS - now_time);

	} while(1);
}
