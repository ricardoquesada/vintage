/*
	config.h
	usado por btrom.c y mbtrom.c
*/


/*
	Modificar segun los gustos 
*/

/* Numero que uno supone que esta vacio en la sys_call_table */
#define NUMERO_VACIO 216

/* Path al archivo System.map */
/* Si Ud. nunca compilo el kernel tal vez sea /boot/System.map */
/* FIXME: Usar el define del Makefile para no definir esto en 2 partes */
#ifndef SYSTEM_MAP
 #define SYSTEM_MAP "/usr/src/linux/System.map"
#endif

/* Hay problemas con old y new. Gralmente no es problema de la System.map */
#define SYSMAP_LIMIT 8


/* Path al archivo asm/unistd.h */
#define ASM_UNISTD "/usr/include/asm/unistd.h"

/* Prefijo a buscar en asm/unistd.h*/
#define AU_PREFIX "#define*__NR_*" 

/* Hasta donde llega el kernel space */
/* FIXME: No se cual es el limite realmente. Igual con esto anda :-) */
#define LIMITE_SYSCALL 0x00300000 

/*
	No modificar
*/ 
/* Version del btrom */
#define VERSION "0.3"

/* BIG_KERNEL y SMALL_KERNEL*/
#define BIG_KERNEL 0xc0000000
#define SMALL_KERNEL 0x00100000
