/*
 * prueba de jump
 */
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char **argv )
{
	/* Chequeo si es BIG_KERNEL o no */
	__asm__ volatile (
/*
		"nop ; nop ; nop  ; ljmp $0,$0+2; jmp *%eax ; jmp 0; jmp 0x01234567;"
		"jmpl 1; jmpl 0x1234567"
*/
		"mov $0x1234567, %eax; "
		"jmp *%eax ;"
		"nop; nop; nop; nop; nop ;"
		);

	return 0;
}
