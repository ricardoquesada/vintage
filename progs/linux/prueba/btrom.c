#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv, char **envp )
{
	int i,retval;

	/* __NR_mbtrom number */
	if(argc!=2) {
		printf("Pone un numer. ok?\n");
		return 1;
	}

	i = atoi( argv[1] );
	if(!i) {
		printf("Pone un numero. ok?\n");
		return 1;
	}

	/* Chequeo si es BIG_KERNEL o no */
	__asm__ volatile (
		"int $0x80":"=a" (retval):"0"(216),
		"b"((long) (i)),
		"c"((long) (2)),
		"d"((long) (0)));


	return 0;
}
