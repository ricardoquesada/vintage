/*
 * modulo del btrom - Borra Trojanos Modulo
 * 25/11/98 - por Riq
 * 
 * compile with:
 *   gcc -c -O3 -fomit-frame-pointer mbtrom.c
 * 
 */
#define MODULE
#define __KERNEL__

#include <linux/config.h>
#ifdef MODULE
#include <linux/module.h>
#include <linux/version.h>
#else
#define MOD_INC_USE_COUNT
#define MOD_DEC_USE_COUNT
#endif

#include <syscall.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/malloc.h>
#include <linux/dirent.h>
#include <linux/sys.h>
#include <linux/linkage.h>
#include <asm/segment.h>

#include "config.h"
#include "sys_null.h"

extern void *sys_call_table[];

int __NR_mbtrom;

int* funcion( int numero, int modo, unsigned int *address )
{
	switch(modo){
		case 0:
			return sys_call_table[numero];
			break;
		case 2:
			return (void *)&sys_call_table;
		case 1:
		default:
			sys_call_table[numero]=address;
			break;
	}
	return (void *)0;
}

int init_module(void)
{
	__NR_mbtrom = NUMERO_VACIO ;

	/* Chequea direccion vacia desde NUMERO_VACIO hasta 0 */
	while ( __NR_mbtrom!= 0 &&
		sys_call_table[__NR_mbtrom] != 0 &&
		sys_call_table[__NR_mbtrom] != (void *)SYS_NULL )
		__NR_mbtrom--;
	if(!__NR_mbtrom ) { /* Si es 0 me voy */
		printk("mbtrom: Oh no\n");
		return 1;
	}
		
	sys_call_table[__NR_mbtrom] = (void *) funcion;


	if( __NR_mbtrom != NUMERO_VACIO )
		printk("mbtrom: Mmm...\n");
	printk("mbtrom: -> %i <-\n",__NR_mbtrom);
	return 0;
}

void cleanup_module(void)
{
	sys_call_table[__NR_mbtrom] = 0;
	printk("mbtrom: Bye.\n");
}
