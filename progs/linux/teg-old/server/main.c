/*	$Id$	*/
/*
 * T.E.G
 * Tenes Empanadas Graciela
 */

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include "teg.h"

struct juego g_juego;

static void juego_init()
{
	jugador_init();
	turno_init();
	paises_init();

	g_juego.conecciones = 0;
	g_juego.jugadores = 0;
}

int main( int argc, char **argv)
{
	JUGADOR t;

	juego_init();

	strcpy(t.nombre,"ricardo");
	jugador_ins( &t );

	strcpy(t.nombre,"pipi");
	jugador_ins( &t );

	strcpy(t.nombre,"bombi");
	jugador_ins( &t );

	return 1;
}
