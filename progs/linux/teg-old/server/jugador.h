/*	$Id$	*/

#ifndef __TEG_JUGADOR_H
#define __TEG_JUGADOR_H

#include "common.h"

#define PLAYERNAME_MAX_LEN	50


typedef struct _jugador {
	LIST_ENTRY next;
	char nombre[PLAYERNAME_MAX_LEN];	/* nombre */
	int color;				/* ejercito o color */
	LIST_ENTRY paises;			/* paises que tiene */
	LIST_ENTRY pactos;			/* pactos que hizo */
	LIST_ENTRY tarjetas;			/* tarjetas sacadas */
	int objetivo;				/* numero de objetivo */
	int canjes;				/* cant canjes que hizo */
	BOOLEAN jugador;			/* readonly o jugador */
	int conquistados;			/* conquistados en el turno */
} JUGADOR, *PJUGADOR;


/*
 * funciones y variables exportadas
 */
extern LIST_ENTRY g_list_jugador;

void jugador_init( void );
void jugador_ins(PJUGADOR j);
TEG_STATUS jugador_del(PJUGADOR j);

#endif /* __JUGADOR_H */
