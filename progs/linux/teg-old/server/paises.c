/*	$Id$	*/

#include <stdio.h>

#include "teg.h"

PAIS paises[] = {
	{ {NULL,NULL}, "ARGENTINA", 0, 0, AMERICA_SUR, {{NULL,NULL}, TARJ_COMODIN} },
	{ {NULL,NULL}, "BRASIL", 0, 0, AMERICA_SUR, {{NULL,NULL}, TARJ_COMODIN} },
	{ {NULL,NULL}, "CHILE", 0, 0, AMERICA_SUR, {{NULL,NULL}, TARJ_CANION} },

	{ {NULL,NULL}, "MEXICO", 0, 0, AMERICA_NORTE, {{NULL,NULL}, TARJ_GALEON }}
};
static int paises_tot = sizeof (paises) / sizeof (paises[0]);

void paises_initpais( PPAIS p )
{
	p->jugador=0;
	p->ejercitos=0;
}

void paises_init()
{
	int i;
	
	for(i=0;i<paises_tot;i++)
		paises_initpais( &paises[i] );
}
