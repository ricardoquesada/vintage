/*	$Id$	*/

#ifndef __TEG_COMMON_H
#define __TEG_COMMON_H

#define MAX_PLAYERS	6

#if !(defined(TRUE) && defined(FALSE))
#define TRUE 1
#define FALSE 0
#endif

typedef int BOOLEAN, *PBOOLEAN;

typedef enum {
	EJERCITO_AZUL = 0,
	EJERCITO_NEGRO = 1,
	EJERCITO_ROJO = 2,
	EJERCITO_ROSA = 3,
	EJERCITO_VERDE = 4,
	EJERCITO_LOCO = 5
} EJERCITO, *PEJERCITO;

typedef enum {
	TEG_STATUS_SUCCESS = 0,
	TEG_STATUS_ERROR = 1,
	TEG_STATUS_NOTFOUND = 2,
	TEG_STATUS_NOMEM = 3
} TEG_STATUS, *	PTEG_STATUS;

#define MODALIDAD_READONLY	0
#define MODALIDAD_PLAYER	1

typedef enum {
	AMERICA_SUR,
	AMERICA_NORTE,
	AFRICA,
	EUROPA,
	ASIA,
	OCEANIA
} CONTINENTE, *PCONTINENTE;


struct juego {
	int conecciones;	/* cantidad de conecciones */
	int jugadores;		/* cantidad de jugadores */
	int objetivos;		/* con objetivos o no */
	int turno;		/* de quien es el turno */
};
extern struct juego g_juego;

/*
 * Estas macros fueron sacadas del DDK de Windows NT 4.0
 */
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY *Flink;
	struct _LIST_ENTRY *Blink;
} LIST_ENTRY, *PLIST_ENTRY;

//
//  Doubly-linked list manipulation routines.  Implemented as macros
//  but logically these are procedures.
//

//
//  VOID
//  InitializeListHead(
//      PLIST_ENTRY ListHead
//      );
//

#define InitializeListHead(ListHead) (\
    (ListHead)->Flink = (ListHead)->Blink = (ListHead))

//
//  BOOLEAN
//  IsListEmpty(
//      PLIST_ENTRY ListHead
//      );
//

#define IsListEmpty(ListHead) \
    ((ListHead)->Flink == (ListHead))

//
//  PLIST_ENTRY
//  RemoveHeadList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveHeadList(ListHead) \
    (ListHead)->Flink;\
    {RemoveEntryList((ListHead)->Flink)}

//
//  PLIST_ENTRY
//  RemoveTailList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveTailList(ListHead) \
    (ListHead)->Blink;\
    {RemoveEntryList((ListHead)->Blink)}

//
//  VOID
//  RemoveEntryList(
//      PLIST_ENTRY Entry
//      );
//

#define RemoveEntryList(Entry) {\
    PLIST_ENTRY _EX_Blink;\
    PLIST_ENTRY _EX_Flink;\
    _EX_Flink = (Entry)->Flink;\
    _EX_Blink = (Entry)->Blink;\
    _EX_Blink->Flink = _EX_Flink;\
    _EX_Flink->Blink = _EX_Blink;\
    }

//
//  VOID
//  InsertTailList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertTailList(ListHead,Entry) {\
    PLIST_ENTRY _EX_Blink;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_Blink = _EX_ListHead->Blink;\
    (Entry)->Flink = _EX_ListHead;\
    (Entry)->Blink = _EX_Blink;\
    _EX_Blink->Flink = (Entry);\
    _EX_ListHead->Blink = (Entry);\
    }

//
//  VOID
//  InsertHeadList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertHeadList(ListHead,Entry) {\
    PLIST_ENTRY _EX_Flink;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_Flink = _EX_ListHead->Flink;\
    (Entry)->Flink = _EX_Flink;\
    (Entry)->Blink = _EX_ListHead;\
    _EX_Flink->Blink = (Entry);\
    _EX_ListHead->Flink = (Entry);\
    }

#endif /* __TEG_COMMON_H */
