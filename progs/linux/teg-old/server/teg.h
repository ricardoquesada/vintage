/*	$Id$	*/
/*
 * Includes generales para todos los archivos del TEG
 */

#ifndef __TEG_TEG_H
#define __TEG_TEG_H

#include "common.h"
#include "tarjeta.h"
#include "paises.h"
#include "jugador.h"
#include "turno.h"

#endif /* __TEG_TEG_H */
