/*	$Id$	*/

#include <stdlib.h>
#include <string.h>

#include "teg.h"

LIST_ENTRY g_list_jugador;

static void jugador_initplayer( PJUGADOR j )
{
	InitializeListHead( &j->next );
	InitializeListHead( &j->paises );
	InitializeListHead( &j->pactos );
	InitializeListHead( &j->tarjetas );
	j->canjes = 0;
	j->jugador = TRUE;
}

void jugador_init( void )
{
	InitializeListHead( &g_list_jugador );
}

void jugador_ins( PJUGADOR j )
{
	PJUGADOR new = (PJUGADOR) malloc( sizeof(JUGADOR) );
	memmove( new, j, sizeof(JUGADOR));
	jugador_initplayer( new );
	InsertHeadList( &g_list_jugador, (PLIST_ENTRY) new );
}

TEG_STATUS jugador_del( PJUGADOR j )
{
	PLIST_ENTRY l;

	l = g_list_jugador.Flink;

	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		if( ((PJUGADOR)l)->color == j->color ) {
			l = RemoveHeadList( l->Blink );
			free( l );
			return TEG_STATUS_SUCCESS;
		}
		l = l->Flink;
	}
	return TEG_STATUS_NOTFOUND;
}

