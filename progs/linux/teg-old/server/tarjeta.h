/*	$Id$	*/

#ifndef __TEG_TARJETA_H
#define __TEG_TARJETA_H 

typedef enum {
	TARJ_COMODIN,
	TARJ_GALEON,
	TARJ_CANION,
	TARJ_GLOBO
} TARJTIPO, *PTARJTIPO;

typedef struct _tarjeta {
	LIST_ENTRY next;
	TARJTIPO tarjeta;
} TARJETA, *PTARJETA;

/* 
 * Funciones y varialbes exportadas
 */
//BOOLEAN tarjeta_puedocanje( PJUGADOR j, PAIS p);

#endif /* __TEG_TARJETA_H */
