/*	$Id$	*/

#ifndef __TEG_PAISES_H
#define __TEG_PAISES_H

#include "teg.h"

#define COUNTRYNAME_MAX_LEN	50

typedef struct _pais {
	LIST_ENTRY next;
	char nombre[COUNTRYNAME_MAX_LEN];	/* nombre */
	int jugador;				/* duenio */
	int ejercitos;				/* cant de ejercitos */
	CONTINENTE continente;
	TARJETA tarjeta;			/* tipo de dibujo de tarjeta */
} PAIS, *PPAIS;

/*
 * Funciones, variables exportadas
 */
extern PAIS paises[];

void paises_initpais( PPAIS p );
void paises_init();

#endif /* __TEG_PAISES_H */
