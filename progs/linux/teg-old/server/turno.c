/*	$Id$	*/

#include "teg.h"

static int whoisnext( void )
{
	PLIST_ENTRY l = g_list_jugador.Flink;

	/* inicializa variables */
	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		PJUGADOR j = (PJUGADOR) l;
		l = l->Flink;
		if( j->color == g_juego.turno )
			return( ((PJUGADOR)l)->color );
	}
	return -1;
}

/*
 * Pasa al siguiente turno
 */
void turno_next()
{
	PLIST_ENTRY l;

	/* inicializa variables */
	l = g_list_jugador.Flink;
	while( !IsListEmpty( &g_list_jugador ) && (l != &g_list_jugador) ) {
		( (PJUGADOR)l )->conquistados = 0;
		l = l->Flink;
	}

	/* el turno al siguiente */

}

/*
 * Iniciala las variables para empezar a jugar
 */
void turno_init()
{
	g_juego.turno = 0;
	turno_next();
}

void turno_nextplayer()
{
	/* soy el ganador */
	g_juego.turno = whoisnext();
}
