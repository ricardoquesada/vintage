#include <stdlib.h>

void help(void)
{
	printf(
		"Modo de uso:\n"
		"Debe ingresar un operando (*,+,-,/) y al menos 2 numeros.\n"
		"Ejemplo:\n\tmodulo6 * 3 6\n"
		);
}

int main(int argc, char **argv)
{
	int r;
	int j;

	if(argc<4) {
		help();
		return 1;
	}

	r=atoi(argv[2]);
	switch( argv[1][0] )
	{
		case '*':
			for(j=4;j<=argc;j++)
				r*=atoi(argv[j-1]);
			break;
		case '+':
			for(j=4;j<=argc;j++)
				r+=atoi(argv[j-1]);
			break;
		case '-':
			for(j=4;j<=argc;j++)
				r-=atoi(argv[j-1]);
			break;
		case '/':
			for(j=4;j<=argc;j++)
				r/=atoi(argv[j-1]);
			break;
		default:
			help();
	}
	printf("El resultado es:%d\n",r);
}
