/* 
 * MODULO 2 - EJERCICIO 1
 *
 * Ingresar dos numeros enteros no nulos A y B. Se pide hallar la potencia
 * de B^A y A^B sabiendo que no se cuenta con la operacion potencia
 */
#include <stdio.h>
int elevar( int a, int b )
{
	int i;
	int j=a;
	if(!b)
		return 1;
	for(i=1;i<b;a *=j,i++);
	return a;
}

int main( int argc, char **argv)
{
	int a;
	int b;

	char ca[10];
	char cb[10];

	printf("Ingrese el numero entero 'A':\n");
	gets( ca );
	printf("Ingrese el numero entero 'B':\n");
	gets( cb );

	a = atoi(  ca );
	b = atoi(  cb );

	printf(" %d elevado a %d es = %d\n",a,b,elevar(a,b));
	printf(" %d elevado a %d es = %d\n",b,a,elevar(b,a));
	return 0;
}
