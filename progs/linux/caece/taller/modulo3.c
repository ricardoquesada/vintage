/* 
 * MODULO 3 - EJERCICIO 1
 *
 * Se tiene cargado en memoria un vector de enteros de 50 posiciones. Se
 * pide generar un programa que intercambie todas las posiciones equidistantes
 * al punto medio.
 */
#include <stdio.h>
#include <stdlib.h>

void init_v ( char *v)
{
	int i,j;
	for(i=0;i<50;i++) {
		j=64+(int) (24.0*rand()/(RAND_MAX+1.0));
		v[i] = (char) j;
	}
}
void mostrar_v( char *v)
{
	int i;
	for(i=0;i<50;i++)
		printf("%c",v[i]);

	printf("\n");
}

int main( int argc, char **argv )
{
	int i;
	char v[50];
	char a;

	init_v( v);
	mostrar_v( v );

	for(i=0;i<50/2;i++) {
		a=v[50-i-1];
		v[50-i-1]=v[i];
		v[i]=a;
	}

	mostrar_v( v );
}
