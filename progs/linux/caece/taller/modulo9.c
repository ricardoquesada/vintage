/*
   Taller de Programacion
   Modulo 9. Ejercicio 1
   Realice la implementacion del nodo de una lista simplementa enlzada. Realizar
   las primitivas de crear, insertar, recorrer la lista. Realizar un programa que
   demuestre su funcionamiento
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct nodo {
	struct	nodo *next;
	char	nombre[40];
	char	apellido[40];
} NODO, *pNODO;

pNODO	np=NULL;

void insertar( pNODO n )
{
	if(np) {
		n->next = np->next;
		np->next = n;
	} else {
		np = n;
	}
}

pNODO crear()
{
	pNODO n;
	char nombre[40];
	char apellido[40];

	printf("Ingrese nombre:\n");
	scanf("%s",nombre);
	printf("Ingrese apellido:\n");
	scanf("%s",apellido);

	n = (pNODO) malloc(sizeof(NODO));
	
	strcpy(n->nombre,nombre);
	strcpy(n->apellido,apellido);

	return n;
}

void recorrer()
{
	pNODO n;

	for(n=np;n;n=n->next){
		printf("Nombre: %s\n",n->nombre);
		printf("Apellido: %s\n",n->apellido);
	}
}

int main()
{
	int a;
	pNODO n;


	while(1){
		printf("1-Crear Nodo\n");
		printf("2-Insertar Nodo\n");
		printf("3-Recorrer Lista\n");
		printf("s- Salir\n");

		a = getchar();
		switch( (char) a) {
			case '1':
				n = crear();
				break;
			case '2':
				insertar(n);
				break;
			case '3':
				recorrer();
				break;
			case 's':
				return 0;
		}
		getchar();
	}
}
