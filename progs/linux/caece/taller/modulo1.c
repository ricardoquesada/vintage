/* 
 * MODULO 1 - EJERCICIO 1
 *
 * Se pide ingresar una secuencia de caracteres, el fin de ingreso se
 * determina cuando se lee EOF. Generar un programa que emita por pantalla
 * el total de las palabras con 3 letras, en la cual figure una sola vocal.
 * Por fin de transaccion informar el total de palabras con 3 letras, pero 
 * discriminando por vocal
 *
 * Disculpe profesora, pero no pude comprender bien que es lo que se me 
 * pedia con "...pero discrimiando por vocal." y no lo hice.
 */
#include <stdio.h>
#include <unistd.h>

int main( int argc, char **argv)
{ 
	char a;
	int cantidad_total=0;
	int palabra_nueva=0;
	int tiene_vocal=0;
	int longitud_temp=0;

	while( read(0, &a, 1 )) {
		a |=0x20;		/* tolower */
		switch(a) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				tiene_vocal++;
				longitud_temp++;
			break;
			case ' ':
			case '\t':
			case ',':
			case ';':
			case '.':
			case '\n':
			case '\r':
			case 0x2a:	/* EOL */
				if(longitud_temp==3 && tiene_vocal==1)
					cantidad_total++;
				tiene_vocal=0;
				longitud_temp=0;
				break;
			default:
				longitud_temp++;
				break;
		}
	}
	printf("Total de palabras de longitud 3 con una vocal:%d\n",cantidad_total);
	return 0;
}
