/* 
 * MODULO 4 - EJERCICIO 1
 *
 * Cargar en memoria una matriz de 3x3, se pide generar un algoritmo
 * mediante el cual se pueda recorrer la matriz desde cada uno de 
 * sus vertices hacia cualquier direccion
 */
#include <stdlib.h>
#include <stdio.h>

void init_m ( int m[3][3] )
{
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			m[i][j] = 1+(int) (50.0*rand()/(RAND_MAX+1.0));
		}
	}
}

void recorrer( int m[3][3], int der, int abajo, int invertir)
{
	int i,j,x;
	int k;

	printf("\n");
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			if(invertir) { x=i; i=j; j=x; }
			if(abajo) {
				if(der)
					k = m[i][j];
				else
					k = m[i][j];
			} else {
				if(der)
					k = m[2-i][j];
				else
					k = m[2-i][2-j];
			}
			if(invertir) { x=i; i=j; j=x; }
			printf("%d ",k);
		}
		printf("\n");
	}

}

int main( int argc, char **argv)
{
	int m[3][3];

	init_m( m );

	/* Mostrar las 8 posibles matrices */
	recorrer( m, 0,0,0);
	recorrer( m, 0,0,1);

	recorrer( m, 0,1,0);
	recorrer( m, 0,1,1);

	recorrer( m, 1,0,0);
	recorrer( m, 1,0,1);

	recorrer( m, 1,1,0);
	recorrer( m, 1,1,1);

	return 0;
}
