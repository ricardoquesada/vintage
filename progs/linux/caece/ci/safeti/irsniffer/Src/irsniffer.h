/****************************************************************************/
#ifndef  _IRSNIFFER_H_INCLUDED__
#define  _IRSNIFFER_H_INCLUDED__
/****************************************************************************/

/* file inclusions **********************************************************/

/* symbol definitions *******************************************************/

/* type definitions *********************************************************/

/* global variables *********************************************************/

/* macros *******************************************************************/

/* function prototypes ******************************************************/

/*
 * Defines
 */
#define appID             'BatL'
#define appVersionNum     0x01
#define appPrefID         0x00
#define appPrefVersionNum 0x01
#define board_Square      10
#define board_Size        10
#define board_Left        0
#define board_Top         20
#define ship_Destroyer    0
#define ship_Cruisor      1
#define ship_Sub          2
#define ship_Battleship   3
#define ship_Carrier      4
#define ship_Max          ship_Carrier+1
#define orient_Horz       0
#define orient_Vert       1
#define fire_Hit          1
#define fire_Miss         2
#define fire_Won          3
#define fire_Sink         4 /* minus fire_Sink to determine ship */
#define packet_type_startup   1
#define packet_type_fire      2
#define packet_type_report    3
#define packet_type_done      4
#define nextstep_none         0
#define nextstep_discovery    1
#define nextstep_irlap        2
#define nextstep_irlmp        3
#define nextstep_new          4
#define nextstep_load         5
#define nextstep_play         6
#define nextstep_response     7
#define action_new            1
#define action_load           2

/* 
 * static data 
 */
 
static const char *ships[] = {
	"Destroyer",
	"Cruisor",
	"Sub",
	"BattleShip",
	"Carrier"
	};
	
static const char *forms[] = {
	"Planning",
	"Damage Control",
	"Fire Control"
	};

static const char palmgame[] = "vs. Palm";

/*
 * Macros
 */
#define minRomVersion sysMakeROMVersion(3,0,0,sysROMStageRelease,0)
#define calc_ship_id(shp,dir,pos) (3000 + ((shp)*10) + ((dir)*5) + (pos))

/*
 * typedefs
 */
typedef struct
{
	Boolean sound;
} BattleShipPrefs;

typedef struct
{
	UInt8 type;
	UInt8 action;
	char name[31];
} IRStartup;

typedef struct
{
	UInt8 type;
	UInt16 x, y;
} IRFire;

typedef struct
{
	UInt8 type;
	UInt16 response;
} IRReport;

typedef struct
{
	UInt8 type;
} IRDone;

typedef struct
{
	UInt8 version;
} BattleShipAppInfo;

typedef struct
{
	UInt8 type;
	UInt8 length;
	UInt8 x,y;
	UInt8 orient;
	Boolean hit[5]; // maximum ship length
} Ship;

typedef struct
{
	UInt8 square[board_Size][board_Size];
} Board;

typedef struct
{
	char name[31];
	Ship ships[5];
	Board board;
	unsigned short recno;
	
	Boolean started,completed,won;
	UInt16 hits, misses;
	
	Board fire;

	DateTimeType when_started;
		
	Boolean IRGame;
	Boolean myTurn;
	
	Boolean my_sinks[ship_Max], opp_sinks[ship_Max];
	
	union
	{
		struct
		{
			char serialNo[12];
		} human;
		struct
		{
			Ship ships[5];
			Board fire;
			Board board;
		} palm;
	} u;
} Game;

typedef struct
{
	int recno;
} StoredGame;

/*
 * Prototypes
 */
static void StartApplication(void);
static void StopApplication(void);
static void AppEventLoop(void);
static Boolean AppHandleEvent(EventPtr event);
static Boolean StartupFormEventHandler(EventPtr event);
static Boolean NewGameEventHandler(EventPtr event);
static Boolean LoadGameEventHandler(EventPtr event);
static Boolean PlacementFormEventHandler(EventPtr event);
static Boolean ShipsFormEventHandler(EventPtr event);
static Boolean IRStatusEventHandler(EventPtr event);
static Boolean FireControlFormEventHandler(EventPtr event);
static void DrawRawGrid();
static void SetupPlacementForm(FormPtr frm);
static void PlaceShip(FormPtr frm, UInt8 type, UInt8 x, UInt8 y);
static void MoveShip(FormPtr frm, UInt8 Type, UInt8 x, UInt8 y);
static Boolean CheckPenEvent(FormPtr frm, UInt16 x, UInt16 y);
static void NewGame();
static void PlayButton();
static void DrawShips(Boolean withHits);
static void PlacementDone();
static void ChangeForm(UInt16 listId);
static void SetupShipsForm();
static void SetupFireControlForm();
static Boolean OpenDatabase();
static void LoadGamesList(ListPtr list);
static void ShowGameDetails(FormPtr frm, UInt16 game_num);
static void LoadGame(UInt16 game_num);
static Boolean MakeMove(FormPtr frm, UInt16 x, UInt16 y);
static void TakeRandomShot(Board *fireBoard, UInt16 *x, UInt16 *y);
static void PalmPlaceShips();
static void PalmTakeShot();
static int PalmRecieveShot(UInt16 x, UInt16 y);
static void TakeShot(UInt16 x, UInt16 y);
static void ShotStatus(int stat, UInt16 bx, UInt16 by);
static int ReceiveShot(UInt16 x, UInt16 y);
static void DrawFireControlStatus();
static void MyRandomShot();
static void PlaceShipsAtRandom(Ship *ships, Board *board);
static void UpdateHitInds(FormPtr frm, Boolean *stats);
static void BSIrCallBack(IrConnect *conn, IrCallBackParms *parms);
static void StoreDiscovery(IrDeviceList* deviceList);
static void LoadGameFromPeer();
/*
 * Globals
 */
static MenuBarPtr CurrentMenu = 0;
static UInt16 CurrentView;
static Game game;
static DmOpenRef bsdb;
static StoredGame *GamesList = 0;
static UInt16 sgames = 0;
static char *games[100]; 
static int game_selected;
static Boolean irAvail;
static UInt16 irref;
static Boolean packet_in_use = false;
static IrConnect connect;
static IrPacket packet;
static IrDeviceAddr dev;
static UInt8 last_packet_type;
static IRStartup irStartup;
static IRDone irDone;
static IRFire irFire;
static IRReport irReport;
static UInt8 * serialNo;
static UInt16 serialNoLen;
static UInt8 nextstep = nextstep_none;
static UInt8 pending_nextstep = nextstep_none;
static Boolean controller;
static Boolean im_done = false;
static Boolean opp_done = false;

static IrStatus      g_irstatus;


/****************************************************************************/
#endif   /* _IRSNIFFER_H_INCLUDED__ */
/****************************************************************************/
/* eof */
