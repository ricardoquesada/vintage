/******************************************************************************
 *
 * Copyright (c) 1999 Palm Computing, Inc. or its subsidiaries.
 * All rights reserved.
 *
 * File: Starter.c
 *
 *****************************************************************************/

#include <PalmOS.h>
#include <irlib.h>
#include <systemmgr.h>
#include <SysEvtMgr.h>
#include "irsniffer.h"
#include "StarterRsc.h"


void StoreDiscovery(IrDeviceList* deviceList);

/***********************************************************************
 *
 *   Entry Points
 *
 ***********************************************************************/

/***********************************************************************
 *
 *   Internal Structures
 *
 ***********************************************************************/
typedef struct 
	{
	UInt8 replaceme;
	} StarterPreferenceType;

typedef struct 
	{
	UInt8 replaceme;
	} StarterAppInfoType;

typedef StarterAppInfoType* StarterAppInfoPtr;


/***********************************************************************
 *
 *   Global variables
 *
 ***********************************************************************/


/***********************************************************************
 *
 *   Internal Constants
 *
 ***********************************************************************/
#define appFileCreator				 'IRSN'
#define appVersionNum              0x01
#define appPrefID                  0x00
#define appPrefVersionNum          0x01

// Define the minimum OS version we support (2.0 for now).
#define ourMinVersion	sysMakeROMVersion(2,0,0,sysROMStageRelease,0)

char buffer[256];
static IrPacket          stPacket = { 0 };


/***********************************************************************
 *
 *   Internal Functions
 *
 ***********************************************************************/

int SetStatus( char * a_pszString, int len );

int SetStatus( char * a_pszString, int len )
{
   int err = 0;
   FormPtr           pfrmPtr;
   FieldPtr          pfldPtr;
   UInt16            idFormId;
   UInt16            idFieldId;

   pfrmPtr = FrmGetActiveForm();
   idFormId = FrmGetActiveFormID();
   #if 0
   switch   ( idFormId )
   {
      case  MainForm :
      {
         idFieldId = MainStatusField;
         break ;
      } /* end case */

      case  RecvForm :
      {
         idFieldId = RecvRecvField ;
         break ;
      } /* end case */
      
      case  SendForm :
      {
         idFieldId = SendSendField ;
         break ;
      } /* end case */
      

      default  :
         break ;
   
   }  /* end switch */
   #endif
   
   idFieldId = MainStatusField ;
   pfldPtr = FrmGetObjectPtr( 
         pfrmPtr, 
         FrmGetObjectIndex( 
               pfrmPtr, 
               idFieldId
            )
         );

   if ( len == 0 )
   {
      FldInsert( pfldPtr, a_pszString, StrLen( a_pszString ) );
   }
   else
   {
      FldInsert( pfldPtr, a_pszString, 23 );
   }  /* end if */
   FldInsert( pfldPtr, "\n", 1 );

   return err;
}

static int SetStatusRecv( char * a_pszString, int len )
{
   int err = 0;
   FormPtr           pfrmPtr;
   FieldPtr          pfldPtr;
   UInt16            idFieldId;

   pfrmPtr = FrmGetActiveForm();
   idFieldId = MainRecvField ;
   pfldPtr = FrmGetObjectPtr( 
         pfrmPtr, 
         FrmGetObjectIndex( 
               pfrmPtr, 
               idFieldId
            )
         );

   if ( len == 0 )
   {
      FldInsert( pfldPtr, a_pszString, StrLen( a_pszString ) );
   }
   else
   {
      FldInsert( pfldPtr, a_pszString, 23 );
   }  /* end if */
   FldInsert( pfldPtr, "\n", 1 );

   return err;
}

static int print_irstatus( char * a_pszString, IrStatus status )
{
   char buf[256];

   switch   ( status )
   {
      case IR_STATUS_MEDIA_BUSY :
      {
         StrPrintF(buf, "%s(IR_STATUS_MEDIA_BUSY)", a_pszString );
         break ;
      } /* end case */
      case IR_STATUS_PENDING :
      {
         StrPrintF(buf, "%s(IR_STATUS_PENDING)", a_pszString );
         break ;
      } /* end case */

      case  IR_STATUS_FAILED :
      {
         StrPrintF(buf, "%s(IR_STATUS_FAILED)", a_pszString );
         break ;
      } /* end case */

      case  IR_STATUS_NO_IRLAP :
      {
         StrPrintF(buf, "%s(IR_STATUS_NO_IRLAP)", a_pszString );
         break ;
      } /* end case */
      
      case  IR_STATUS_SUCCESS :
      {
         StrPrintF(buf, "%s(IR_STATUS_SUCCESS)", a_pszString );
         break ;
      } /* end case */
   
      default  :
         StrPrintF(buf, "%s(UNKNOWN STATUS)", a_pszString );      
         break ;
   
   }  /* end switch */
   
   SetStatus( buf, 0 );

   return 0;
}


static void BSIrCallBack(IrConnect *conn, IrCallBackParms *parms)
{
	switch (parms->event)
	{
	case LEVENT_DISCOVERY_CNF:
		SetStatus("LEVENT_DISCOVERY_CNF", 0); 
		StoreDiscovery(parms->deviceList); 
		pending_nextstep = nextstep_irlap;
		EvtWakeup();
		break;
	case LEVENT_LAP_CON_CNF:
		SetStatus("LEVENT_LAP_CON_CNF", 0); 
		nextstep = nextstep_irlmp;
		controller = true;
		EvtWakeup();
		break;
	case LEVENT_LAP_CON_IND:
		SetStatus("LEVENT_LAP_CON_IND", 0);
		controller = false;
		game.IRGame = true;
	   nextstep = 0;
		EvtWakeup();	   
		break;
	case LEVENT_LAP_DISCON_IND:
		SetStatus("LEVENT_LAP_DISCON_IND", 0);
		EvtWakeup();		
		//IrDisconnectIrLap( irref );
		break;
	case LEVENT_LM_CON_CNF:
		SetStatus("LEVENT_LM_CON_CNF", 0);
		// got a connection
		//MemMove(game.u.human.serialNo,parms->rxBuff,parms->rxLen);
		if (irStartup.action == action_new)
			nextstep = nextstep_new;
		else
			nextstep = nextstep_load;
		EvtWakeup();

		break;
	case LEVENT_LM_CON_IND:
		SetStatus("LEVENT_LM_CON_IND", 0);
      SetStatus("Connecting...", 0 );
		nextstep = nextstep_response;
		break;
	case LEVENT_LM_DISCON_IND:
		SetStatus("LEVENT_LM_DISCON_IND", 0);
		break;
	case LEVENT_PACKET_HANDLED:
		SetStatus("LEVENT_PACKET_HANDLED", 0);
		packet_in_use = false;
		break;
	case LEVENT_DATA_IND:
	   {
		   char buf1[100];

         MemSet(buf1,sizeof(buf1),0);
		   SetStatus("LEVENT_DATA_IND", 0);
		   StrCat( buf1, (const char*)parms->rxBuff );
		   SetStatusRecv( buf1, 0 );
		   break;
	   }
	case LEVENT_STATUS_IND:
		// SetStatus("LEVENT_STATUS_IND", 0);
		switch (parms->status)
		{
         case IR_STATUS_NO_PROGRESS:
         SetStatus("LEVENT_STATUS_IND (IR_STATUS_NO_PROGRESS) ", 0);
			break;
		case IR_STATUS_LINK_OK:
         SetStatus("LEVENT_STATUS_IND (IR_STATUS_LINK_OK) ", 0);
			break;
		case IR_STATUS_MEDIA_NOT_BUSY:
         SetStatus("LEVENT_STATUS_IND (IR_STATUS_MEDIA_NOT_BUSY) ", 0);
			nextstep = pending_nextstep;
			pending_nextstep = nextstep_none;
			EvtWakeup();
			break;
		}
		break;
	case LEVENT_TEST_CNF:
		SetStatus("LEVENT_TEST_CNF", 0);
		switch (parms->status)
		{
		case IR_STATUS_SUCCESS:
			break;
		case IR_STATUS_FAILED:
			break;
		}
		break;
	case LEVENT_TEST_IND:
		SetStatus("LEVENT_TEST_IND", 0);
		break;
	}

   switch (nextstep)
	{
   case nextstep_discovery:
      SetStatus("nextstep_discovery", 0);
		print_irstatus( "IrDiscoverReq", IrDiscoverReq(irref,&connect) );
		break;
	case nextstep_irlap:
      SetStatus( "nextstep_irlap", 0);
		print_irstatus( "IrConnectIrLap", IrConnectIrLap(irref,dev) );
		break;
   case nextstep_irlmp:
      SetStatus( "nextstep_irlmp", 0);
		packet.buff = serialNo;
		packet.len = serialNoLen;
  		print_irstatus( "IrConnectReq", IrConnectReq(irref,&connect,&packet,0) );
		break;
	case nextstep_new:
      SetStatus( "nextstep_new", 0);
		break;
	case nextstep_load:
      SetStatus( "nextstep_load", 0);
      MemMove(game.name,game.u.human.serialNo,sizeof(game.u.human.serialNo));
		game.name[(sizeof(game.u.human.serialNo))] = 0;
		break;
	case nextstep_response:
	   SetStatus( "nexstep_response", 0);
		print_irstatus( "IrConnectRsp", IrConnectRsp(irref,&connect,&packet,0) );
	}
	nextstep = nextstep_none;

}



/***********************************************************************
 *
 * FUNCTION:    RomVersionCompatible
 *
 * DESCRIPTION: This routine checks that a ROM version is meet your
 *              minimum requirement.
 *
 * PARAMETERS:  requiredVersion - minimum rom version required
 *                                (see sysFtrNumROMVersion in SystemMgr.h 
 *                                for format)
 *              launchFlags     - flags that indicate if the application 
 *                                UI is initialized.
 *
 * RETURNED:    error code or zero if rom is compatible
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Err RomVersionCompatible(UInt32 requiredVersion, UInt16 launchFlags)
{
	UInt32 romVersion;

	// See if we're on in minimum required version of the ROM or later.
	FtrGet(sysFtrCreator, sysFtrNumROMVersion, &romVersion);
	if (romVersion < requiredVersion)
		{
		if ((launchFlags & (sysAppLaunchFlagNewGlobals | sysAppLaunchFlagUIApp)) ==
			(sysAppLaunchFlagNewGlobals | sysAppLaunchFlagUIApp))
			{
			FrmAlert (RomIncompatibleAlert);
		
			// Palm OS 1.0 will continuously relaunch this app unless we switch to 
			// another safe one.
			if (romVersion < ourMinVersion)
				{
				AppLaunchWithCommand(sysFileCDefaultApp, sysAppLaunchCmdNormalLaunch, NULL);
				}
			}
		
		return sysErrRomIncompatible;
		}

	return errNone;
}


/***********************************************************************
 *
 * FUNCTION:    GetObjectPtr
 *
 * DESCRIPTION: This routine returns a pointer to an object in the current
 *              form.
 *
 * PARAMETERS:  formId - id of the form to display
 *
 * RETURNED:    void *
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void * GetObjectPtr(UInt16 objectID)
{
	FormPtr frmP;

	frmP = FrmGetActiveForm();
	return FrmGetObjectPtr(frmP, FrmGetObjectIndex(frmP, objectID));
}


/***********************************************************************
 *
 * FUNCTION:    MainFormInit
 *
 * DESCRIPTION: This routine initializes the MainForm form.
 *
 * PARAMETERS:  frm - pointer to the MainForm form.
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void MainFormInit(FormPtr /*frmP*/)
{
}


/***********************************************************************
 *
 * FUNCTION:    MainFormDoCommand
 *
 * DESCRIPTION: This routine performs the menu command specified.
 *
 * PARAMETERS:  command  - menu item id
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean MainFormDoCommand(UInt16 command)
{
	Boolean handled = false;
   FormPtr frmP;

	switch (command)
		{
		case MainOptionsAboutiRSniffer:
			MenuEraseStatus(0);					// Clear the menu status from the display.
			frmP = FrmInitForm (AboutForm);
			FrmDoDialog (frmP);					// Display the About Box.
			FrmDeleteForm (frmP);
			handled = true;
			break;

		}
	
	return handled;
}

#if 0

/***********************************************************************
 *
 * FUNCTION:    SendFormHandleEvent
 *
 * DESCRIPTION: This routine is the event handler for the 
 *              "MainForm" of this application.
 *
 * PARAMETERS:  eventP  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean SendFormHandleEvent(EventPtr eventP)
{
   Boolean handled = false;
   FormPtr frmP;
   ControlPtr        pctrlPush;
   static char              msg[] = "hola\0";

	switch (eventP->eType) 
		{

      case  ctlSelectEvent :
      {
         switch   ( eventP->data.ctlSelect.controlID )
         {
            case  SendUnnamed1205Button :
            {
               char     buff[50];

               stPacket.len = 4;
               stPacket.buff = ( unsigned char * ) msg;
               StrPrintF( buff, "Sending %s", msg);
               SetStatus( buff, 0 );
               print_irstatus("IrDataReq", IrDataReq( irref, &connect, &stPacket ) );
               handled = true;
               break ;
            } /* end case */
            

            case  SendStatusPushButton :
            {         
               FrmGotoForm( MainForm );
               handled = true;               
               break ;
            } /* end case */
            

            default  :
               break ;
         
         }  /* end switch */
         break ;
      } /* end case */
      
		case menuEvent:
			return MainFormDoCommand(eventP->data.menu.itemID);

		case frmOpenEvent:
			frmP = FrmGetActiveForm();
         pctrlPush = FrmGetObjectPtr(
               frmP,
               FrmGetObjectIndex(
                     frmP,
                     SendSendPushButton
                  )
            );
         CtlSetValue( pctrlPush, 1);
			FrmDrawForm ( frmP);
         SetStatus( "Checking send...", 0);
//         IrDiscoverReq( irref, &connect );
			handled = true;
			break;

		default:
			break;
		
		}
	
	return handled;
}


/***********************************************************************
 *
 * FUNCTION:    MainFormHandleEvent
 *
 * DESCRIPTION: This routine is the event handler for the 
 *              "MainForm" of this application.
 *
 * PARAMETERS:  eventP  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean RecvFormHandleEvent(EventPtr eventP)
{
   Boolean handled = false;
   FormPtr frmP;
   ControlPtr        pctrlPush;

	switch (eventP->eType) 
		{

      case  ctlSelectEvent :
      {
         switch   ( eventP->data.ctlSelect.controlID )
         {
            case  RecvStatusPushButton :
            {         
               FrmGotoForm( MainForm );
               break ;
            } /* end case */
            

            default  :
               break ;
         
         }  /* end switch */
         break ;
      } /* end case */
      
		case menuEvent:
			return MainFormDoCommand(eventP->data.menu.itemID);

		case frmOpenEvent:
			frmP = FrmGetActiveForm();
         pctrlPush = FrmGetObjectPtr(
               frmP,
               FrmGetObjectIndex(
                     frmP,
                     RecvRecvPushButton
                  )
            );
         CtlSetValue( pctrlPush, 1);
			FrmDrawForm ( frmP);
         SetStatus( "Checking Receive...", 0);
//         IrDiscoverReq( irref, &connect );
			handled = true;
			break;

		default:
			break;
		
		}
	
	return handled;
}
#endif

/***********************************************************************
 *
 * FUNCTION:    MainFormHandleEvent
 *
 * DESCRIPTION: This routine is the event handler for the 
 *              "MainForm" of this application.
 *
 * PARAMETERS:  eventP  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean MainFormHandleEvent(EventPtr eventP)
{
   Boolean handled = false;
   FormPtr frmP;
   FieldPtr          pfldField;
   char  *           pcChar;

   frmP = FrmGetActiveForm();

	switch (eventP->eType) 
		{

      case  ctlSelectEvent :
      {
         switch   ( eventP->data.ctlSelect.controlID )
         {
            case  MainDiscoverButton :
            {
               IrDiscoverReq( irref, &connect );
               handled = true;
               break ;
            } /* end case */
            
            case  MainSendButton :
            {
               pfldField = FrmGetObjectPtr( frmP, FrmGetObjectIndex( frmP, MainSendField ));
               pcChar = FldGetTextPtr( pfldField );
               if ( pcChar != NULL && *pcChar != 0 )
               {
                  StrCopy( buffer, pcChar);
               stPacket.len = StrLen(buffer)+1;
               stPacket.buff = (unsigned char *) buffer;
               print_irstatus("IrDataReq", IrDataReq( irref, &connect, &stPacket ) );
               }  /* end if */
               handled = true;
               break ;
            } /* end case */
            

            default  :
               break ;
         
         }  /* end switch */
         break ;
      } /* end case */
      
		case menuEvent:
			return MainFormDoCommand(eventP->data.menu.itemID);

		case frmOpenEvent:
			frmP = FrmGetActiveForm();
			MainFormInit( frmP);
			FrmDrawForm ( frmP);
			pfldField = FrmGetObjectPtr(
                  frmP, 
                  FrmGetObjectIndex( 
                     frmP, 
                     MainStatusField
                  )
            );
         FldInsert( pfldField, "Checking IR,,,", StrLen("Checking IR,,,") );
         SysTaskDelay( sysTicksPerSecond*1 );
         FldInsert( pfldField, "OK\n", StrLen("OK\n"));
         FldInsert( pfldField, "Initializating,,,", StrLen("Initializating,,,") );
         SysTaskDelay( sysTicksPerSecond*1 );
         FldInsert( pfldField, "OK\n", StrLen("OK\n"));
         SysTaskDelay( sysTicksPerSecond*1 );
         FldInsert( pfldField, "Ready\n", StrLen("Ready\n"));
			handled = true;
			break;

		default:
			break;
		
		}
	
	return handled;
}


/***********************************************************************
 *
 * FUNCTION:    AppHandleEvent
 *
 * DESCRIPTION: This routine loads form resources and set the event
 *              handler for the form loaded.
 *
 * PARAMETERS:  event  - a pointer to an EventType structure
 *
 * RETURNED:    true if the event has handle and should not be passed
 *              to a higher level handler.
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Boolean AppHandleEvent(EventPtr eventP)
{
	UInt16 formId;
	FormPtr frmP;

	if (eventP->eType == frmLoadEvent)
		{
		// Load the form resource.
		formId = eventP->data.frmLoad.formID;
		frmP = FrmInitForm(formId);
		FrmSetActiveForm(frmP);

		// Set the event handler for the form.  The handler of the currently
		// active form is called by FrmHandleEvent each time is receives an
		// event.
		switch (formId)
			{
			case MainForm:
				FrmSetEventHandler(frmP, MainFormHandleEvent);
				break;

   #if 0
         case  RecvForm :
         {
				FrmSetEventHandler(frmP, RecvFormHandleEvent);
            break ;
         } /* end case */
         
         case  SendForm :
         {
				FrmSetEventHandler(frmP, SendFormHandleEvent);
            break ;
         } /* end case */
   #endif


			default:
//				ErrFatalDisplay("Invalid Form Load Event");
				break;

			}
		return true;
		}
	
	return false;
}


/***********************************************************************
 *
 * FUNCTION:    AppEventLoop
 *
 * DESCRIPTION: This routine is the event loop for the application.  
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void AppEventLoop(void)
{
	UInt16 error;
	EventType event;

	do {
		EvtGetEvent(&event, evtWaitForever);

		if (! SysHandleEvent(&event))
			if (! MenuHandleEvent(0, &event, &error))
				if (! AppHandleEvent(&event))
					FrmDispatchEvent(&event);

	} while (event.eType != appStopEvent);
}


/***********************************************************************
 *
 * FUNCTION:     AppStart
 *
 * DESCRIPTION:  Get the current application's preferences.
 *
 * PARAMETERS:   nothing
 *
 * RETURNED:     Err value 0 if nothing went wrong
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static Err AppStart(void)
{
    StarterPreferenceType prefs;
    UInt16 prefsSize;

	// Read the saved preferences / saved-state information.
	prefsSize = sizeof(StarterPreferenceType);
	if (PrefGetAppPreferences(appFileCreator, appPrefID, &prefs, &prefsSize, true) != 
		noPreferenceFound)
		{
		}

	// check for ir
	if (SysLibFind(irLibName,&irref) != 0)
	{
		irAvail = false;
	}
	else
	{
//		if (IrOpen(irref,irOpenOptSpeed115200) != 0)
		if (IrOpen(irref,irOpenOptSpeed9600) != 0)
			irAvail = false;
		else
		{
			irAvail = true;
			IrBind(irref,&connect,BSIrCallBack);
			IrSetDeviceInfo(irref,( UInt8 * )&connect.lLsap,1);
			IrSetConTypeLMP(&connect);
		}
	}

	if (!irAvail)
		FrmCustomAlert(InfoCustomAlert, "NoIR", " "," ");

	SysGetROMToken(0,sysROMTokenSnum,&serialNo,&serialNoLen);		
	
   return errNone;
}


/***********************************************************************
 *
 * FUNCTION:    AppStop
 *
 * DESCRIPTION: Save the current state of the application.
 *
 * PARAMETERS:  nothing
 *
 * RETURNED:    nothing
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static void AppStop(void)
{
   StarterPreferenceType prefs;

	// Write the saved preferences / saved-state information.  This data 
	// will be backed up during a HotSync.
	PrefSetAppPreferences (appFileCreator, appPrefID, appPrefVersionNum, 
		&prefs, sizeof (prefs), true);
		
	// Close all the open forms.
	FrmCloseAllForms ();

 IrUnbind( irref, &connect );
      if (irAvail)
		IrClose(irref);

}


/***********************************************************************
 *
 * FUNCTION:    StarterPalmMain
 *
 * DESCRIPTION: This is the main entry point for the application.
 *
 * PARAMETERS:  cmd - word value specifying the launch code. 
 *              cmdPB - pointer to a structure that is associated with the launch code. 
 *              launchFlags -  word value providing extra information about the launch.
 *
 * RETURNED:    Result of launch
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
static UInt32 StarterPalmMain(UInt16 cmd, MemPtr /*cmdPBP*/, UInt16 launchFlags)
{
	Err error;

	error = RomVersionCompatible (ourMinVersion, launchFlags);
	if (error) return (error);

	switch (cmd)
		{
		case sysAppLaunchCmdNormalLaunch:
			error = AppStart();
			if (error) 
				return error;
				
			FrmGotoForm(MainForm);
			AppEventLoop();
			AppStop();
			break;

		default:
			break;

		}
	
	return errNone;
}


/***********************************************************************
 *
 * FUNCTION:    PilotMain
 *
 * DESCRIPTION: This is the main entry point for the application.
 *
 * PARAMETERS:  cmd - word value specifying the launch code. 
 *              cmdPB - pointer to a structure that is associated with the launch code. 
 *              launchFlags -  word value providing extra information about the launch.
 * RETURNED:    Result of launch
 *
 * REVISION HISTORY:
 *
 *
 ***********************************************************************/
UInt32 PilotMain( UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
{
    return StarterPalmMain(cmd, cmdPBP, launchFlags);
}

/*
 * records information we found
 * during discovery
 */
void StoreDiscovery(IrDeviceList* deviceList)
{
	int i;
   char buf[50];

	for (i = 0; i < deviceList->nItems; i++)
	{
		dev = deviceList->dev[i].hDevice;
		connect.rLsap = deviceList->dev[i].xid[0];

      StrPrintF( buf, "Disc: %d Addr: 0x%8x",i,dev);

      SetStatus( buf, 0 );
   }
}
