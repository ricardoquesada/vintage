
#ifndef _PALM_H
#define _PALM_H

// system includes
#include <PalmOS.h>

// application constants and structures
#define appCreator         'SNIF'


// resource
#include "../rsc/irsniff_res.h"

#include "device.h"

#ifdef __GNUC__
#include "gccfix.h"
#endif

// functions
extern UInt32  PilotMain(UInt16, MemPtr, UInt16);
extern Boolean ApplicationHandleEvent(EventType *);
extern void    ApplicationDisplayDialog(UInt16);
extern void    EventLoop(void);
extern void    EndApplication(void);

#endif 
