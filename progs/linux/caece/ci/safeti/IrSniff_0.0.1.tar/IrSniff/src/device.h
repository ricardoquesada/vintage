
#ifndef _DEVICE_H
#define _DEVICE_H

#include "palm.h"

#define romVersion1   sysMakeROMVersion(1,0,0,sysROMStageRelease,0)
#define romVersion2   sysMakeROMVersion(2,0,0,sysROMStageRelease,0)
#define romVersion3   sysMakeROMVersion(3,0,0,sysROMStageRelease,0)
#define romVersion3_1 sysMakeROMVersion(3,1,0,sysROMStageRelease,0)
#define romVersion3_2 sysMakeROMVersion(3,2,0,sysROMStageRelease,0)
#define romVersion3_5 sysMakeROMVersion(3,5,0,sysROMStageRelease,0)
#define romVersion4   sysMakeROMVersion(3,5,0,sysROMStageRelease,0)

extern Boolean DeviceCheckCompatability();
extern void    DeviceInitialize();
extern UInt32  DeviceGetSupportedDepths();
extern Boolean DeviceSupportsVersion(UInt32);
extern void    *DeviceWindowGetPointer(WinHandle);
extern void    DeviceTerminate();

#endif
