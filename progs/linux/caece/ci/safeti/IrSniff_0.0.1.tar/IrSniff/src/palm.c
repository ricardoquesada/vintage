
#include "palm.h"
#include "sniff.h"

typedef struct
{
	Int32  evtTimeOut;
	UInt32 timerDiff;
	UInt32 ticksPerFrame;
	UInt32 timerPointA;
	UInt32 timerPointB;
} Globals;

// interface
static Boolean MainFormEventHandler(EventType *);
static Boolean InfoFormEventHandler(EventType *);
static Boolean HelpFormEventHandler(EventType *);
static Boolean XmemFormEventHandler(EventType *);

/**
 * The Form:MainForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
MainFormEventHandler(EventType *event)
{
	Globals *globals;
	Boolean processed = false;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	switch (event->eType) 
	{
		case frmOpenEvent:
				 FrmDrawForm(FrmGetActiveForm());

				 // initialize the cube environment
//				 CubeInitialize(35);

				 // update the form (draw stuff)
				 {
					 EventType event;

					 MemSet(&event, sizeof(EventType), 0);
					 event.eType = frmUpdateEvent;
					 event.data.frmUpdate.formID = FrmGetActiveFormID();
					 EvtAddEventToQueue(&event);
				 }

				 processed = true;
				 break;

		case frmUpdateEvent:
				 FrmDrawForm(FrmGetActiveForm());

				 // draw seperators
				 WinDrawLine(  0, 145, 159, 145);
				 WinDrawLine(  0, 146, 159, 146);

				 processed = true;
				 break;

		case ctlSelectEvent:
	
				 switch (event->data.ctlSelect.controlID)
				 {
					case MenuStart:
						sniff_init();
						break;
					case MenuStop:
						break;
				 }
				 processed = true;
				 break;

		case nilEvent:

				 // make sure the active window is the form
				 if (WinGetActiveWindow() == (WinHandle)FrmGetActiveForm()) 
				 {
				 }
				 processed = true;
				 break;

		case frmCloseEvent:

				 // terminate
				 break;

		default:
				 break;
	}

	return processed;
}

/**
 * The Form:infoForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
InfoFormEventHandler(EventType *event)
{
	Globals *globals;
	Boolean processed = false;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	switch (event->eType) 
	{
		case frmOpenEvent:
			FrmDrawForm(FrmGetActiveForm());
			processed = true;
			break;
	 
		case ctlSelectEvent:

			switch (event->data.ctlSelect.controlID)
			{
				default:
					break;
			}
			break;

		default:
			break;
	}

	return processed;
}

/**
 * The Form:HelpForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
HelpFormEventHandler(EventType *event)
{
	Globals *globals;
	Boolean processed = false;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	switch (event->eType) 
	{
		case frmOpenEvent:
				 processed = true;
				 break;
	 
		case ctlSelectEvent:

				 break;

		case sclRepeatEvent:

		case keyDownEvent:

				 switch (event->data.keyDown.chr)
				 {
					case pageUpChr:
						processed = true;
						break;

					case pageDownChr:
						processed = true;
						break;

					default:
						break;
				 }
				 break;

		case frmCloseEvent:

				 // clean up
				 break;

		default:
				 break;
	}

	return processed;
}

/**
 * The Form:XmemForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
XmemFormEventHandler(EventType *event)
{
	Globals *globals;
	Boolean processed = false;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	switch (event->eType) 
	{
		case frmOpenEvent:
				 FrmDrawForm(FrmGetActiveForm());
				 processed = true;
				 break;
	 
		case ctlSelectEvent:

				 switch (event->data.ctlSelect.controlID)
				 {
					default:
						break;
				 }
				 break;

		default:
				 break;
	}

	return processed;
}

/**
 * The Palm Computing Platform initialization routine.
 */
void	
InitApplication()
{
	Globals *globals;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	globals->evtTimeOut		 = evtWaitForever;
	globals->ticksPerFrame = (Int16)(SysTicksPerSecond() / 10 );

	// goto the main form
	FrmGotoForm(MainForm);
}

/**
 * The Palm Computing Platform entry routine (mainline).
 *
 * @param cmd					a word value specifying the launch code.
 * @param cmdPBP			pointer to a structure associated with the launch code.
 * @param launchFlags additional launch flags.
 * @return zero if launch successful, non zero otherwise.
 */
UInt32	
PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
{
	UInt32 result = 0;

	// what type of launch was this?
	switch (cmd) 
	{
		case sysAppLaunchCmdNormalLaunch:
				 {
					 // is this device compatable?
					 if (DeviceCheckCompatability())
					 {
						 Globals *globals;

						 // initialize device
						 DeviceInitialize();

						 // create the globals object, and register it
						 globals = (Globals *)MemPtrNew(sizeof(Globals));
						 MemSet(globals, sizeof(Globals), 0);
						 FtrSet(appCreator, ftrGlobals, (UInt32)globals);

						 // run the application :))
						 InitApplication();
						 EventLoop();
						 EndApplication();

						 // unregister the feature
						 FtrUnregister(appCreator, ftrGlobals);

						 // restore device state
						 DeviceTerminate();
					 }
				 }
				 break;

		default:
				 break;
	}

	return result;
}

/**
 * The application event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
Boolean 
ApplicationHandleEvent(EventType *event)
{
	Globals *globals;
	Boolean processed = false;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	switch (event->eType)
	{
		case frmLoadEvent:
				 {
					 UInt16		formID = event->data.frmLoad.formID;
					 FormType *frm	 = FrmInitForm(formID);

					 FrmSetActiveForm(frm);
					 switch (formID) 
					 {
						 case MainForm:
									FrmSetEventHandler(frm, 
										(FormEventHandlerPtr)MainFormEventHandler);

									processed = true;
									break;

						 default:
									break;
					 }
				 }
				 break;
				 
		case winEnterEvent:
				 {
					 if (event->data.winEnter.enterWindow == 
								(WinHandle)FrmGetFormPtr(MainForm)) {
						 globals->evtTimeOut = 1;
						 processed					 = true;
					 }
				 }
				 break;

		case winExitEvent:
				 {
					 if (event->data.winEnter.enterWindow == 
								(WinHandle)FrmGetFormPtr(MainForm)) {
						 globals->evtTimeOut = evtWaitForever;
						 processed					 = true;
					 }
				 }
				 break;

		case menuEvent:

				 switch (event->data.menu.itemID) 
				 {
					 default:
								break;
				 }
				 break;

		default:
				 break;
	}

	return processed;
}

/**
 * Display a MODAL dialog to the user (this is a modified FrmDoDialog)
 *
 * @param formID the ID of the form to display.
 */
void
ApplicationDisplayDialog(UInt16 formID)
{
	Globals							*globals;
	FormActiveStateType frmCurrState;
	FormType						*frmActive			= NULL;
	WinHandle						winDrawWindow		= NULL;
	WinHandle						winActiveWindow = NULL;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	// save the active form/window
	if (DeviceSupportsVersion(romVersion3)) 
	{
		FrmSaveActiveState(&frmCurrState);
	}
	else 
	{
		frmActive				= FrmGetActiveForm();
		winDrawWindow		= WinGetDrawWindow();
		winActiveWindow = WinGetActiveWindow();  // < palmos3.0, manual work
	}

	{
		EventType event;
		UInt16		err;
		Boolean		keepFormOpen;

		MemSet(&event, sizeof(EventType), 0);

		// send a load form event
		event.eType = frmLoadEvent;
		event.data.frmLoad.formID = formID;
		EvtAddEventToQueue(&event);

		// send a open form event
		event.eType = frmOpenEvent;
		event.data.frmLoad.formID = formID;
		EvtAddEventToQueue(&event);

		// handle all events here (trap them before the OS does) :)
		keepFormOpen = true;
		while (keepFormOpen) 
		{
			EvtGetEvent(&event, globals->evtTimeOut);

			// this is our exit condition! :)
			keepFormOpen = (Boolean)(event.eType != frmCloseEvent);

			if (!SysHandleEvent(&event))
				if (!MenuHandleEvent(0, &event, &err))
					if (!ApplicationHandleEvent(&event))
						FrmDispatchEvent(&event);

			if (event.eType == appStopEvent) 
			{
				keepFormOpen = false;
				EvtAddEventToQueue(&event);  // tap "applications", need to exit
			}
		}
	}

	// restore the active form/window
	if (DeviceSupportsVersion(romVersion3)) 
	{
		FrmRestoreActiveState(&frmCurrState);
	}
	else 
	{
		FrmSetActiveForm(frmActive);
		WinSetDrawWindow(winDrawWindow);
		WinSetActiveWindow(winActiveWindow);		 // < palmos3.0, manual work
	}
}

/**
 * The Palm Computing Platform event processing loop.
 */
void	
EventLoop()
{
	Globals		*globals;
	EventType event;
	FormType	*frm;
	UInt16		err;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	// reset the timer (just incase)
	globals->timerPointA = TimGetTicks();

	do {

		EvtGetEvent(&event, globals->evtTimeOut);
		frm = FrmGetActiveForm();

		//
		// ANIMATION HANDLING
		//

		// nil event occurs.. register time state
		if (
				(WinGetActiveWindow() == (WinHandle)frm) &&
				(frm == FrmGetFormPtr(MainForm)) &&
				(event.eType == nilEvent)
			 )
		{
			globals->timerPointA = TimGetTicks();
		}

		//
		// EVENT HANDLING
		//

		if (!SysHandleEvent(&event)) 
			if (!MenuHandleEvent(0, &event, &err)) 
				if (!ApplicationHandleEvent(&event)) 
					FrmDispatchEvent(&event);

		//
		// ANIMATION HANDLING
		//

		if (
				(WinGetActiveWindow() == (WinHandle)frm) &&
				(frm == FrmGetFormPtr(MainForm))
			 )
		{
			globals->timerPointB = TimGetTicks();

			// calculate the delay required
			globals->timerDiff = (globals->timerPointB - globals->timerPointA);
			globals->evtTimeOut = (globals->timerDiff > globals->ticksPerFrame) ?
				1 : (globals->ticksPerFrame - globals->timerDiff);

			// manually add nilEvent if needed (only when pen held down)
			if ((globals->evtTimeOut <= 1) && (event.eType == penMoveEvent))
			{
				EventType event;

				MemSet(&event, sizeof(EventType), 0);
				event.eType = nilEvent;
				EvtAddEventToQueue(&event);
			}
		}

	} while (event.eType != appStopEvent);
}

/**
 * The Palm Computing Platform termination routine.
 */
void	
EndApplication()
{
	Globals *globals;

	// get globals reference
	FtrGet(appCreator, ftrGlobals, (UInt32 *)&globals);

	// ensure all forms are closed
	FrmCloseAllForms();

	// clean up
	MemPtrFree(globals);
}
