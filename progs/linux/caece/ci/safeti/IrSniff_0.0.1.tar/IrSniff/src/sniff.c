#include "palm.h"
#include <Core/System/IrLib.h>

/* globals */
static Boolean irAvail;
static UInt16 irref;
static IrConnect connect;
static IrPacket packet;
static IrDeviceAddr dev;

/*
 * records information we found
 * during discovery
 */
void sniff_store_discovery(IrDeviceList* deviceList)
{
	int i;

	for (i = 0; i < deviceList->nItems; i++)
	{
		dev = deviceList->dev[i].hDevice;
		connect.rLsap = deviceList->dev[i].xid[0];
	}
}

static void sniff_ir_cb(IrConnect *conn, IrCallBackParms *parms)
{
	switch (parms->event)
	{
	case LEVENT_DISCOVERY_CNF: 
		sniff_store_discovery(parms->deviceList); 
		break;
	case LEVENT_LAP_CON_CNF: 
		break;
	case LEVENT_LAP_CON_IND:
		break;
	case LEVENT_LAP_DISCON_IND:
		break;
	case LEVENT_LM_CON_CNF:
		break;
	case LEVENT_LM_CON_IND:
		break;
	case LEVENT_LM_DISCON_IND:
		break;
	case LEVENT_PACKET_HANDLED:
		break;
	case LEVENT_DATA_IND:
		break;
	case LEVENT_STATUS_IND:
		switch (parms->status)
		{
		case IR_STATUS_NO_PROGRESS:
			break;
		case IR_STATUS_LINK_OK:
			break;
		case IR_STATUS_MEDIA_NOT_BUSY:
			break;
		}
		break;
	case LEVENT_TEST_CNF:
		switch (parms->status)
		{
		case IR_STATUS_SUCCESS:
			break;
		case IR_STATUS_FAILED:
			break;
		}
		break;
	case LEVENT_TEST_IND:
		break;
	}
	
	if (parms->event == LEVENT_LM_CON_IND)
		IrConnectRsp(irref,&connect,&packet,0);
}

int sniff_end()
{
	IrClose(irref);
	return 0;
}

int sniff_init()
{
	
typedef struct 
{
  UInt32    keyMask;
  WinHandle helpWindow;
} HelpGlobals;

  const RectangleType     rect  = {{0,0},{142,234}};
  const CustomPatternType erase = {0,0,0,0,0,0,0,0};
  HelpGlobals *globals;
  UInt16      err;
  Int16       result = 0;



        EventType event;
	// check for ir
	if (SysLibFind(irLibName,&irref) != 0)
		irAvail = false;

	else
	{
		if (IrOpen(irref,irOpenOptSpeed115200) != 0)
			irAvail = false;
		else
		{
			irAvail = true;
			IrBind(irref,&connect,sniff_ir_cb);
			IrSetDeviceInfo(irref,(UInt8*)&connect.lLsap,1);
			IrSetConTypeLMP(&connect);
		}

		{
		    FontID    font;
		    WinHandle currWindow;

		    currWindow = WinGetDrawWindow();
		    font       = FntGetFont();

		    // draw to help window
		    WinSetDrawWindow(globals->helpWindow);
		    WinSetPattern(&erase);
		    WinFillRectangle(&rect,0);

		    {
		      Char  *str, *ptrStr;
		      Coord x, y;

		      // initialize
		      y   = 2;
		      str = (Char *)MemPtrNew(256 * sizeof(Char));

		      // draw title
		      StrCopy(str, "WIREFRAME CUBE");
		      x = (rect.extent.x - FntCharsWidth(str, StrLen(str))) >> 1;

		      WinSetUnderlineMode(grayUnderline);
		      WinDrawChars(str, StrLen(str), x, y); y += FntLineHeight();
		      WinSetUnderlineMode(noUnderline);

		      // add space (little)
		      y += FntLineHeight() >> 1;
		}

#if 0 
		      // general text 
		      x = 4;
		      StrCopy(str, "Cube3D is a simple wireframe cube demonstration that uses perspective \
		geometry for the Palm Computing Platform.");
		      ptrStr = str;
		      while (StrLen(ptrStr) != 0) {
			UInt16 count = FntWordWrap(ptrStr, (UInt16)rect.extent.x-x);

			x = (rect.extent.x - FntCharsWidth(ptrStr, (Int16)count)) >> 1;
			WinDrawChars(ptrStr, (Int16)count, x, y); y += FntLineHeight(); x = 4;
		 
			ptrStr += count;
		      }

		      // add space (little)
		      y += FntLineHeight() >> 1;
		  
		      // show the screen teaser
		      x = 8;
		      {
			MemHandle bitmapHandle = DmGet1Resource('Tbmp', HelpAnimationBitmapFamily);
			WinDrawBitmap((BitmapType *)MemHandleLock(bitmapHandle), x, y);
			MemHandleUnlock(bitmapHandle);
			DmReleaseResource(bitmapHandle);
		      }

		      // add space (little)
		      // y += 48 + (FntLineHeight() >> 1);

#endif

//		FormDoDialog(FormNoIRDA);

//		if (!irAvail) {
//			FrmAlert(FormNoIRDA);
		    // send a load form event
		    event.eType = frmLoadEvent;
		    event.data.frmLoad.formID = FormNoIRDA;
		    EvtAddEventToQueue(&event);
#if 0
			// regenerate menu event
			{
			  EventType event;

			  MemSet(&event, sizeof(EventType), 0);
			  event.eType            = menuEvent;
			  event.data.menu.itemID = MainHelpAbout;
			  EvtAddEventToQueue(&event);
			}
#endif
		}


		IrDiscoverReq(irref,&connect);
	}
	return 0;
}
