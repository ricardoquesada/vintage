
#ifndef _PALM_H
#define _PALM_H

// system includes
#include <PalmOS.h>

// application constants and structures
#define appCreator         'SNIF'
#define ftrGlobals         1000
#define ftrGraphicsGlobals 1001
#define ftrDeviceGlobals   1002
#define ftrHelpGlobals     1003
#define ftrCubeGlobals     1004


// resource
#include "../rsc/irsniff_res.h"

#include "device.h"

#ifdef __GNUC__
#include "gccfix.h"
#endif

// functions
extern UInt32  PilotMain(UInt16, MemPtr, UInt16);
extern void    InitApplication(void);
extern Boolean ApplicationHandleEvent(EventType *);
extern void    ApplicationDisplayDialog(UInt16);
extern void    EventLoop(void);
extern void    EndApplication(void);

#endif 
