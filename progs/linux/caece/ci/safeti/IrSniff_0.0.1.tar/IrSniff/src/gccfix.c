
#include "palm.h"

#ifdef __GNUC__

void
_GccReleaseCode(UInt16 cmd UNUSED, void *pbp UNUSED, UInt16 flags)
{
  if (flags & sysAppLaunchFlagNewGlobals) {

    MemHandle codeH;
    int resno;

    for (resno = 2; (codeH = DmGet1Resource ('code', resno)) != NULL; resno++) {
      MemHandleUnlock (codeH);
      DmReleaseResource (codeH);
    }
  }
}

#endif
