/*
 * @(#)help.c
 *
 * Copyright 1999-2001, Aaron Ardiri (mailto:aaron@ardiri.com)
 * All rights reserved.
 *
 * The  source code  outlines a number of basic Palm Computing Programming
 * principles and you  should be able to take the core structure and write 
 * a large complex program. It is distributed WITHOUT ANY WARRANTY; use it
 * "AS IS" and at your own risk.
 *
 * The code presented is Copyright 1999-2001 by Aaron Ardiri. It should be
 * used for  educational purposes only.  You  shall not modify  the Cube3D 
 * source code in any way and  re-distribute it as your  own,  however you
 * are free to use  the code as  a guide for  developing  programs  on the 
 * Palm Computing Platform.
 */

#include "palm.h"

typedef struct 
{
  UInt32    keyMask;
  WinHandle helpWindow;
} HelpGlobals;

/**
 * Initialize the instructions screen.
 * 
 * @return the height in pixels of the instructions data area.
 */
Int16
InitInstructions()
{
  const RectangleType     rect  = {{0,0},{142,234}};
  const CustomPatternType erase = {0,0,0,0,0,0,0,0};
  HelpGlobals *globals;
  UInt16      err;
  Int16       result = 0;

  // create the globals object, and register it
  globals = (HelpGlobals *)MemPtrNew(sizeof(HelpGlobals));
  MemSet(globals, sizeof(HelpGlobals), 0);
  FtrSet(appCreator, ftrHelpGlobals, (UInt32)globals);

  // setup the valid keys available at this point in time
  globals->keyMask = KeySetMask(~(keyBitsAll ^ 
                                 (keyBitPower    | keyBitCradle   |
                                  keyBitPageUp   | keyBitPageDown |
                                  keyBitContrast)));

  // initialize windows
  globals->helpWindow = 
    WinCreateOffscreenWindow(rect.extent.x,rect.extent.y,screenFormat,&err);
  err |= (Boolean)(globals->helpWindow == NULL);

  // did something go wrong?
  if (err != errNone)
  {
    result = 0;
    ApplicationDisplayDialog(XmemForm);
  }

  // draw the help
  else {
  
    FontID    font;
    WinHandle currWindow;

    currWindow = WinGetDrawWindow();
    font       = FntGetFont();

    // draw to help window
    WinSetDrawWindow(globals->helpWindow);
    WinSetPattern(&erase);
    WinFillRectangle(&rect,0);

    {
      Char  *str, *ptrStr;
      Coord x, y;

      // initialize
      y   = 2;
      str = (Char *)MemPtrNew(256 * sizeof(Char));

      // draw title
      StrCopy(str, "WIREFRAME CUBE");
      x = (rect.extent.x - FntCharsWidth(str, StrLen(str))) >> 1;

      WinSetUnderlineMode(grayUnderline);
      WinDrawChars(str, StrLen(str), x, y); y += FntLineHeight();
      WinSetUnderlineMode(noUnderline);

      // add space (little)
      y += FntLineHeight() >> 1;

      // general text 
      x = 4;
      StrCopy(str,
"Cube3D is a simple wireframe cube demonstration that uses perspective \
geometry for the Palm Computing Platform.");
      ptrStr = str;
      while (StrLen(ptrStr) != 0) {
        UInt16 count = FntWordWrap(ptrStr, (UInt16)rect.extent.x-x);

        x = (rect.extent.x - FntCharsWidth(ptrStr, (Int16)count)) >> 1;
        WinDrawChars(ptrStr, (Int16)count, x, y); y += FntLineHeight(); x = 4;
 
        ptrStr += count;
      }

      // add space (little)
      y += FntLineHeight() >> 1;
  
      // show the screen teaser
      x = 8;
      {
        MemHandle bitmapHandle = DmGet1Resource('Tbmp', HelpAnimationBitmapFamily);
        WinDrawBitmap((BitmapType *)MemHandleLock(bitmapHandle), x, y);
        MemHandleUnlock(bitmapHandle);
        DmReleaseResource(bitmapHandle);
      }

      // add space (little)
      y += 48 + (FntLineHeight() >> 1);

      // general text
      x = 4;
      StrCopy(str,
"The source code is available for educational purposes only and \
shall not be redistributed or modified without the consent of \
the original author.");
      ptrStr = str;
      while (StrLen(ptrStr) != 0) {
        UInt16 count = FntWordWrap(ptrStr, (UInt16)rect.extent.x-x);

        x = (rect.extent.x - FntCharsWidth(ptrStr, (Int16)count)) >> 1;
        WinDrawChars(ptrStr, (Int16)count, x, y); y += FntLineHeight(); x = 4;
 
        ptrStr += count;
      }
 
      // add space (little)
      y += FntLineHeight() >> 1;

      x = 4;
      StrCopy(str,
"Interested in developing for the Palm Computing Platform?");
      ptrStr = str;
      while (StrLen(ptrStr) != 0) {
        UInt16 count = FntWordWrap(ptrStr, (UInt16)rect.extent.x-x);

        x = (rect.extent.x - FntCharsWidth(ptrStr, (Int16)count)) >> 1;
        WinDrawChars(ptrStr, (Int16)count, x, y); y += FntLineHeight(); x = 4;

        ptrStr += count;
      }

      // add space (little)
      y += FntLineHeight() >> 1;

      StrCopy(str, "http://www.palmos.com/");
      FntSetFont(boldFont);
      x = (rect.extent.x - FntCharsWidth(str, StrLen(str))) >> 1;
      WinDrawChars(str, StrLen(str), x, y); y += FntLineHeight();

      // add space (little)
      y += FntLineHeight() >> 1;
 
      StrCopy(str, "Happy Hacking!");
      FntSetFont(font);
      x = (rect.extent.x - FntCharsWidth(str, StrLen(str))) >> 1;
      WinDrawChars(str, StrLen(str), x, y); y += FntLineHeight();

      // clean up
      MemPtrFree(str);
    }

    FntSetFont(font);
    WinSetDrawWindow(currWindow);

    result = rect.extent.y;
  }

  return result;
}

/**
 * Draw the instructions on the screen.
 * 
 * @param offset the offset height of the window to start copying from.
 */
void 
DrawInstructions(Int16 offset)
{
  RectangleType helpArea;
  HelpGlobals *globals;

  helpArea.topLeft.x = 0;
  helpArea.topLeft.y = offset;
  helpArea.extent.x  = 142;
  helpArea.extent.y  = 116;
  
  // get globals reference
  FtrGet(appCreator, ftrHelpGlobals, (UInt32 *)&globals);

  // blit the required area
  WinCopyRectangle(globals->helpWindow, 
                   WinGetDrawWindow(), &helpArea, 3, 16, winPaint);
}

/**
 * Terminate the instructions screen.
 */
void
QuitInstructions()
{
  HelpGlobals *globals;

  // get globals reference
  FtrGet(appCreator, ftrHelpGlobals, (UInt32 *)&globals);

  // return the state of the key processing
  KeySetMask(globals->keyMask);

  // clean up memory
  if (globals->helpWindow != NULL) WinDeleteWindow(globals->helpWindow, false);
  MemPtrFree(globals);

  // unregister global data
  FtrUnregister(appCreator, ftrHelpGlobals);
}
