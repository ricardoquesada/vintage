/*
 * @(#)fixed.h
 *
 * Copyright 1999-2001, Aaron Ardiri (mailto:aaron@ardiri.com)
 * All rights reserved.
 *
 * The  source code  outlines a number of basic Palm Computing Programming
 * principles and you  should be able to take the core structure and write 
 * a large complex program. It is distributed WITHOUT ANY WARRANTY; use it
 * "AS IS" and at your own risk.
 *
 * The code presented is Copyright 1999-2001 by Aaron Ardiri. It should be
 * used for  educational purposes only.  You  shall not modify  the Cube3D 
 * source code in any way and  re-distribute it as your  own,  however you
 * are free to use  the code as  a guide for  developing  programs  on the 
 * Palm Computing Platform.
 */
 
#include "palm.h"

#ifndef _FIXED_H
#define _FIXED_H

// data types
typedef long fixed; // 32 bit --> 24:8 precision
#define precision 8

// fixed mathematical operations
#define Addfx(a,b) (fixed)((fixed)(a) + (fixed)(b))
#define Subfx(a,b) (fixed)((fixed)(a) - (fixed)(b))
#define Mulfx(a,b) (fixed)(((fixed)(a) * (fixed)(b)) >> precision)
#define Divfx(a,b) (fixed)(((fixed)(a) << precision) / (fixed)(b))

// fixed mathematical conversions
#define itofx(x) ((fixed)(x) << precision)
#define ftofx(x) ((fixed)(x) * (fixed)(0x01 << precision))
#define fxtoi(x) (((fixed)(x)) >> precision)
#define fxtof(x) (((fixed)(x)) / (float)(0x01 << precision))

#endif
