/*
 * @(#)palm.h
 *
 * Copyright 1999-2001, Aaron Ardiri (mailto:aaron@ardiri.com)
 * All rights reserved.
 *
 * The  source code  outlines a number of basic Palm Computing Programming
 * principles and you  should be able to take the core structure and write 
 * a large complex program. It is distributed WITHOUT ANY WARRANTY; use it
 * "AS IS" and at your own risk.
 *
 * The code presented is Copyright 1999-2001 by Aaron Ardiri. It should be
 * used for  educational purposes only.  You  shall not modify  the Cube3D 
 * source code in any way and  re-distribute it as your  own,  however you
 * are free to use  the code as  a guide for  developing  programs  on the 
 * Palm Computing Platform.
 */

#ifndef _PALM_H
#define _PALM_H

// system includes
#include <PalmOS.h>

// application constants and structures
#define appCreator         'CUBE'
#define ftrGlobals         1000
#define ftrGraphicsGlobals 1001
#define ftrDeviceGlobals   1002
#define ftrHelpGlobals     1003
#define ftrCubeGlobals     1004

#define CUBE_FPS      16  // this is "adequate" for animations :)
#define SCREEN_WIDTH  160
#define SCREEN_HEIGHT 128 // 160x128 display area

// resource
#include "../rsc/cube3D_res.h"

// local includes
#include "fixed.h"
#include "device.h"
#include "help.h"
#include "graphics.h"
#include "cube3D.h"

#ifdef __GNUC__
#include "gccfix.h"
#endif

// functions
extern UInt32  PilotMain(UInt16, MemPtr, UInt16);
extern void    InitApplication(void);
extern Boolean ApplicationHandleEvent(EventType *);
extern void    ApplicationDisplayDialog(UInt16);
extern void    EventLoop(void);
extern void    EndApplication(void);

#endif 
