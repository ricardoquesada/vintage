//	Header generated by Constructor for Palm OS� 1.6
//
//	Generated at 3:19:15 PM on Sunday, May 27, 2001
//
//	THIS IS AN AUTOMATICALLY GENERATED HEADER FILE FROM CONSTRUCTOR FOR PALM OS�;
//	- DO NOT EDIT - CHANGES MADE TO THIS FILE WILL BE LOST
//
//	Palm App Name:   		"Untitled"
//
//	Palm App Version:		"1.0"


//	Resource: tFRM 3100
#define MainForm                                  3100
#define MainFormHelpButton                        3101
#define MainFormAboutButton                       3102
#define MainFormHelpBitMap                        1004
#define MainFormAboutBitMap                       1003
#define MainFormCopyrightLabel                    3105

//	Resource: tFRM 2100
#define HelpForm                                  2100
#define HelpFormDoneButton                        2102
#define HelpFormCopyrightLabel                    2103
#define HelpFormScrollBar                         2101

//	Resource: tFRM 2000
#define InfoForm                                  2000
#define InfoFormOkButton                          2001
#define InfoFormIconBitMap                        1000
#define InfoFormPalmBitMap                        1001
#define InfoFormWebLinkLabel                      2003
#define InfoFormEmailLabel                        2004
#define InfoFormCopyright1Label                   2006
#define InfoFormCopyright2Label                   2007
#define InfoFormCopyright3Label                   2008
#define InfoFormApplicationLabel                  2009
#define InfoFormVersionLabel                      2010
#define InfoFormUsage1Label                       2011
#define InfoFormUsage2Label                       2012

//	Resource: tFRM 2200
#define XmemForm                                  2200
#define XmemFormOkButton                          2201
#define XmemFormPawBitMap                         1002
#define XmemFormWebSiteLabel                      2203
#define XmemFormEmailLabel                        2204
#define XmemFormMessage1Label                     2205
#define XmemFormMessage2Label                     2206
#define XmemFormMessage3Label                     2207
#define XmemFormMessage4Label                     2208
#define XmemFormMessage5Label                     2209


//	Resource: MBAR 3100
#define MainMenuBar                               3100


//	Resource: MENU 3101
#define MainHelpMenu                              3101
#define MainHelpInstructions                      3101
#define MainHelpAbout                             3102


//	Resource: PICT 1111
#define IconSmall_1bppBitmap                      1111

//	Resource: PICT 1112
#define IconSmall_2bppBitmap                      1112

//	Resource: PICT 1113
#define IconSmall_8bppBitmap                      1113

//	Resource: PICT 1101
#define Icon_1bppBitmap                           1101

//	Resource: PICT 1102
#define Icon_2bppBitmap                           1102

//	Resource: PICT 1103
#define Icon_8bppBitmap                           1103

//	Resource: PICT 1201
#define Palm_1bppBitmap                           1201

//	Resource: PICT 1202
#define Palm_2bppBitmap                           1202

//	Resource: PICT 1203
#define Palm_8bppBitmap                           1203

//	Resource: PICT 1301
#define Paw_1bppBitmap                            1301

//	Resource: PICT 1302
#define Paw_2bppBitmap                            1302

//	Resource: PICT 1401
#define About_1bppBitmap                          1401

//	Resource: PICT 1501
#define Help_1bppBitmap                           1501

//	Resource: PICT 1601
#define HelpAnimation_1bppBitmap                  1601


//	Resource: tbmf 1001
#define PalmBitmapFamily                          1001

//	Resource: tbmf 1002
#define PawBitmapFamily                           1002

//	Resource: tbmf 1000
#define IconBitmapFamily                          1000

//	Resource: tbmf 1003
#define AboutBitmapFamily                         1003

//	Resource: tbmf 1004
#define HelpBitmapFamily                          1004

//	Resource: tbmf 1005
#define HelpAnimationBitmapFamily                 1005


//	Resource: taif 1000
#define NormalAppIconFamily                       1000

//	Resource: taif 1001
#define SmallAppIconFamily                        1001

