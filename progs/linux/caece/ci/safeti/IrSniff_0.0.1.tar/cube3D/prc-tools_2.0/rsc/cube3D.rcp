/*
 * @(#)cube3D.rcp
 *
 * Copyright 1999-2000, Aaron Ardiri (mailto:aaron@ardiri.com)
 * All rights reserved.
 *
 * The  source code  outlines a number of basic Palm Computing Programming
 * principles and you  should be able to take the core structure and write 
 * a large complex program. It is distributed WITHOUT ANY WARRANTY; use it
 * "AS IS" and at your own risk.
 *
 * The code presented is Copyright 1999-2001 by Aaron Ardiri. It should be
 * used for  educational purposes only.  You  shall not modify  the Cube3D 
 * source code in any way and  re-distribute it as your  own,  however you
 * are free to use  the code as  a guide for  developing  programs  on the 
 * Palm Computing Platform.
 */

#include "../../rsc/cube3D_res.h"

FORM ID InfoForm AT (2 2 156 156) MODAL
BEGIN
  TITLE "About Cube3D"

  FORMBITMAP AT (8 16) BITMAP InfoFormIconBitMap
  LABEL "http://www.ardiri.com/" ID InfoFormWebLinkLabel AT (52 PrevTop)
  LABEL "aaron@ardiri.com" ID InfoFormEmailLabel AT (54 PrevBottom) FONT 1

  FORMBITMAP AT (88 PrevBottom+4) BITMAP InfoFormPalmBitMap

  LABEL "\251 1999-2001" ID InfoFormCopyright1Label AT (8 PrevTop+2)
  LABEL "ardiri.com" ID InfoFormCopyright2Label AT (8 PrevBottom)
  LABEL "All rights reserved" ID InfoFormCopyright3Label AT (8 PrevBottom)

  LABEL "Cube 3D" ID InfoFormApplicationLabel AT (8 PrevBottom+3) FONT 1
  LABEL "demonstration" InfoFormVersionLabel AT (8 PrevBottom-1) FONT 1

  LABEL "The source code is provided for" ID InfoFormUsage1Label AT (8 PrevBottom+3) 
  LABEL "educational purposes only." ID InfoFormUsage2Label AT (8 PrevBottom)

  BUTTON "Ok" ID InfoFormOkButton AT (CENTER 138 40 AUTO)
END

FORM ID HelpForm AT (2 2 156 156) MODAL
BEGIN
  TITLE "Instructions"

  SCROLLBAR ID HelpFormScrollBar AT (147 16 7 116) VALUE 0 MIN 0 MAX 0 PAGESIZE 100

  BUTTON "Done" ID HelpFormDoneButton AT (6 138 AUTO AUTO)
  LABEL "\251 ardiri.com" HelpFormCopyrightLabel AT (92 PrevTop+1) FONT 1
END

FORM ID XmemForm AT (2 2 156 156) MODAL
BEGIN
  TITLE "System Memory"

  FORMBITMAP AT (6 20) BITMAP XmemFormPawBitMap
  LABEL "http://www.ardiri.com/" ID XmemFormWebSiteLabel AT (50 PrevTop+4)
  LABEL "aaron@ardiri.com" ID XmemFormEmailLabel AT (52 PrevBottom) FONT 1

  LABEL "Insufficent memory available to" ID XmemFormMessage1Label AT (CENTER PrevTop+24)
  LABEL "do the operation you requested." ID XmemFormMessage2Label AT (CENTER PrevTop+12)

  LABEL "Please disable any unecessary" ID XmemFormMessage3Label AT (CENTER PrevTop+20)
  LABEL "hacks and try using the standard" ID XmemFormMessage4Label AT (CENTER PrevTop+12)
  LABEL "application launcher of the device." ID XmemFormMessage5Label AT (CENTER PrevTop+12)

  BUTTON "Ok" ID XmemFormOkButton AT (CENTER 138 40 AUTO)
END

FORM ID MainForm AT (0 0 160 160) NOSAVEBEHIND
MENUID MainMenuBar
BEGIN
  TITLE "Cube3D"

  BUTTON "" ID MainFormHelpButton AT (133 1 12 12) NOFRAME
  FORMBITMAP AT (PrevLeft PrevTop) BITMAP MainFormHelpBitMap
  BUTTON "" ID MainFormAboutButton AT (PrevLeft+14 PrevTop 12 12) NOFRAME
  FORMBITMAP AT (PrevLeft PrevTop) BITMAP MainFormAboutBitMap

  LABEL "\251 1999-2001 ardiri.com" AUTOID AT (CENTER 148) FONT 1
END

MENU ID MainMenuBar
BEGIN
  PULLDOWN "Help"
  BEGIN
    MENUITEM "Instructions" MainHelpInstructions "I"
    MENUITEM "About"        MainHelpAbout
  END
END

VERSION "demo"

ICONFAMILY 
  "images/icon1bpp.bmp" "images/icon2bpp.bmp" "" "images/icon8bpp.bmp" 
TRANSPARENCY 0 255 0

SMALLICONFAMILY 
  "images/smic1bpp.bmp" "images/smic2bpp.bmp" "" "images/smic8bpp.bmp"
TRANSPARENCY 0 255 0

BITMAPFAMILY IconBitmapFamily   
  "images/icon1bpp.bmp" "images/icon2bpp.bmp" "" "images/icon8bpp.bmp" COMPRESS
TRANSPARENCY 0 255 0

BITMAPFAMILY PalmBitmapFamily
  "images/palm1bpp.bmp" "images/palm2bpp.bmp" "" "images/palm8bpp.bmp" COMPRESS

BITMAPFAMILY PawBitmapFamily
  "images/_paw1bpp.bmp" "images/_paw2bpp.bmp" COMPRESS

BITMAPFAMILY HelpAnimationBitmapFamily
  "images/hani1bpp.bmp" COMPRESS

BITMAPFAMILY AboutBitmapFamily 
  "images/info1bpp.bmp" COMPRESS

BITMAPFAMILY HelpBitmapFamily 
  "images/help1bpp.bmp" COMPRESS
