
#include "palm.h"

// globals variable structure
typedef struct
{
	UInt32	romVersion;						 // the rom version of the device
	UInt32	depthState;						 // the screen depth state (old)
} DeviceGlobals;

/**
 * Initialize the device.
 */
void
DeviceInitialize()
{
	DeviceGlobals *gbls;
 
	// create the globals objects, and register it
	gbls = (DeviceGlobals *)MemPtrNew(sizeof(DeviceGlobals));
	MemSet(gbls, sizeof(DeviceGlobals), 0);
	FtrSet(appCreator, ftrDeviceGlobals, (UInt32)gbls);

	// get the rom version and ram size for this device
	FtrGet(sysFtrCreator, sysFtrNumROMVersion, &gbls->romVersion);

	// only OS 3.0 and above have > 1bpp display via API's
	if (DeviceSupportsVersion(romVersion3)) {

		// save the current display state
		WinScreenMode(winScreenModeGet,NULL,NULL,&gbls->depthState,NULL);

		// change into the "highest" possible mode :P
		{
			UInt32 depthsToTry[] = { 8, 4, 2, 1 };
			UInt32 *depthPtr = &depthsToTry[0];

			// loop until a valid mode is found
			while (WinScreenMode(winScreenModeSet,NULL,NULL,depthPtr,NULL)) {

				// try the next depth
				depthPtr++;
			}
		}
	}
}

/**
 * Check the compatability status of the device we are working with.
 *
 * @return true if the device is supported, false otherwise.
 */
Boolean
DeviceCheckCompatability()
{
	Boolean result = true;

	// the device is only compatable if it is running palmos 2.0+
	result = DeviceSupportsVersion(romVersion2);

	// not compatable :(
	if (!result)
	{
		// .. maybe display dialog here?

		// lets exit the application "gracefully" :>
		if (!DeviceSupportsVersion(romVersion2)) {
			AppLaunchWithCommand(sysFileCDefaultApp,sysAppLaunchCmdNormalLaunch,NULL);
		}
	}

	return result;
}

/**
 * Get the supported depths the device can handle. 
 *
 * @return the depths supported (1011b = 2^3 | 2^1 | 2^0 = 4,2,1 bpp).
 */
UInt32	
DeviceGetSupportedDepths()
{
	UInt32 result = 0x00000001;

	// only OS 3.0 and above have > 1bpp display via API's
	if (DeviceSupportsVersion(romVersion3)) {
		WinScreenMode(winScreenModeGetSupportedDepths,NULL,NULL,&result,NULL);
	}

	return result;
}

/**
 * Check if the device is compatable with a particular ROM version.
 *
 * @param version the ROM version to compare against.
 * @return true if it is compatable, false otherwise.
 */
Boolean 
DeviceSupportsVersion(UInt32 version)
{
	UInt32 romVersion;

	// get the rom version for this device
	FtrGet(sysFtrCreator, sysFtrNumROMVersion, &romVersion);

	return (Boolean)(romVersion >= version);
}

/**
 * Determine the pointer to the bitmap data chunk for a specific window.
 *
 * @param win the window.
 * @return a pointer to the bitmap data chunk.
 */
void *
DeviceWindowGetPointer(WinHandle win)
{
	void *result = NULL;

	// palmos 3.5			 - use BmpGetBits()
	if (DeviceSupportsVersion(romVersion3_5)) {
		result = BmpGetBits(WinGetBitmap(win));
	}

	// palmos pre 3.5		 - use standard technique
	else
		result = (void *)win->displayAddrV20;
	
	return result;
}

/**
 * Reset the device to its original state.
 */
void
DeviceTerminate()
{
	DeviceGlobals *gbls;

	// get a globals reference
	FtrGet(appCreator, ftrDeviceGlobals, (UInt32 *)&gbls);

	// restore the current display state
	if (DeviceSupportsVersion(romVersion3)) {
		WinScreenMode(winScreenModeSet,NULL,NULL,&gbls->depthState,NULL);
	}

	// clean up memory
	MemPtrFree(gbls);

	// unregister global data
	FtrUnregister(appCreator, ftrDeviceGlobals);
}
