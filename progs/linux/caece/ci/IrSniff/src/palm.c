
#include "palm.h"
#include "sniff.h"

// globals
static UInt16 irref;
static Boolean irAvail;
static IrConnect connect;
static IrPacket packet;
static IrDeviceAddr dev;
static UInt8 *serialNo;
static UInt16 serialNoLen;
static UInt16 CurrentView;

// interface
static Boolean MainFormEventHandler(EventType *);
static Boolean InfoFormEventHandler(EventType *);
static Boolean HelpFormEventHandler(EventType *);
static Boolean XmemFormEventHandler(EventType *);

static void AppEventLoop(void);
static Boolean AppHandleEvent(EventType *);
static void BSIrCallBack(IrConnect *conn, IrCallBackParms *parms);

/**
 * The Form:MainForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
MainFormEventHandler(EventType *event)
{
	Boolean processed = false;

	switch (event->eType) 
	{
	case frmOpenEvent:
		 processed = true;
		 break;

	case frmUpdateEvent:
		 FrmDrawForm(FrmGetActiveForm());

		 processed = true;
		 break;

	case ctlSelectEvent:

		 switch (event->data.ctlSelect.controlID)
		 {
			case MenuStart:
				break;
			case MenuStop:
				break;
		 }
		 processed = true;
		 break;

	case frmCloseEvent:

		 // terminate
		 break;

	default:
			 break;
	}

	return processed;
}

/**
 * The Form:infoForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
InfoFormEventHandler(EventType *event)
{
	Boolean processed = false;

	switch (event->eType) 
	{
		case frmOpenEvent:
			FrmDrawForm(FrmGetActiveForm());
			processed = true;
			break;
	 
		case ctlSelectEvent:

			switch (event->data.ctlSelect.controlID)
			{
				default:
					break;
			}
			break;

		default:
			break;
	}

	return processed;
}

/**
 * The Form:HelpForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
HelpFormEventHandler(EventType *event)
{
	Boolean processed = false;

	switch (event->eType) 
	{
		case frmOpenEvent:
				 processed = true;
				 break;
	 
		case ctlSelectEvent:

				 break;

		case sclRepeatEvent:

		case keyDownEvent:

				 switch (event->data.keyDown.chr)
				 {
					case pageUpChr:
						processed = true;
						break;

					case pageDownChr:
						processed = true;
						break;

					default:
						break;
				 }
				 break;

		case frmCloseEvent:

				 // clean up
				 break;

		default:
				 break;
	}

	return processed;
}

/**
 * The Form:XmemForm event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean 
XmemFormEventHandler(EventType *event)
{
	Boolean processed = false;

	switch (event->eType) 
	{
		case frmOpenEvent:
				 FrmDrawForm(FrmGetActiveForm());
				 processed = true;
				 break;
	 
		case ctlSelectEvent:

				 switch (event->data.ctlSelect.controlID)
				 {
					default:
						break;
				 }
				 break;

		default:
				 break;
	}

	return processed;
}

/**
 * The Palm Computing Platform initialization routine.
 */
static void InitApplication(void )
{
	// check for ir
	if (SysLibFind(irLibName,&irref) != 0)
	{
		irAvail = false;
	}
	else
	{
		if (IrOpen(irref,irOpenOptSpeed115200) != 0)
			irAvail = false;
		else
		{
			irAvail = true;
			IrBind(irref,&connect,BSIrCallBack);
			IrSetDeviceInfo(irref,(UInt8*)&connect.lLsap,1);
			IrSetConTypeLMP(&connect);
		}
	}
	
//	if (!irAvail)
//		FrmAlert(FormNoIRDA);
		
	SysGetROMToken(0,sysROMTokenSnum,&serialNo,&serialNoLen);
	SysRandom(TimGetSeconds());
	CurrentView = MainForm;
	FrmGotoForm(CurrentView);
}


UInt32  
PilotMain(UInt16 cmd, MemPtr cmdPBP, UInt16 launchFlags)
{
	if (cmd == sysAppLaunchCmdNormalLaunch)
	{
		InitApplication();
		AppEventLoop();
		EndApplication();
	}
	
	return 0;
}

/**
 * The application event handling routine.
 *
 * @param event the event to process.
 * @return true if the event was handled, false otherwise.
 */
static Boolean AppHandleEvent(EventType *event)
{
	FormPtr form;
	UInt16 formId;
	Boolean handled = false;
	
	if (event->eType == frmLoadEvent)
	{
		formId = event->data.frmLoad.formID;
		form = FrmInitForm(formId);
		FrmSetActiveForm(form);
		
		switch (formId)
		{

		case MainForm:
			FrmSetEventHandler(form, (FormEventHandlerPtr)MainFormEventHandler);
			break;
#if 0
		case StartupFormForm:
			FrmSetEventHandler(form,StartupFormEventHandler);
			break;
		case NewGameForm:
			FrmSetEventHandler(form,NewGameEventHandler);
			break;
		case PlacementForm:
			FrmSetEventHandler(form,PlacementFormEventHandler);
			break;
		case ShipsForm:
			FrmSetEventHandler(form,ShipsFormEventHandler);
			break;
		case FireControlForm:
			FrmSetEventHandler(form,FireControlFormEventHandler);
			break;
		case LoadGameForm:
			FrmSetEventHandler(form,LoadGameEventHandler);
			break;
		case IRStatusForm:
			FrmSetEventHandler(form,IRStatusEventHandler);
#endif
		}
		
		handled = true;
	}
	
	return handled;
}


/**
 * The Palm Computing Platform event processing loop.
 */
static void AppEventLoop(void)
{
	EventType event;
	UInt16 error;
	
	do
	{
		EvtGetEvent(&event, evtWaitForever);
		if (SysHandleEvent(&event))
			continue;
		if (MenuHandleEvent(0, &event, &error)) 
			continue;
		if (AppHandleEvent(&event))
			continue;
		FrmDispatchEvent(&event);
	} while (event.eType != appStopEvent);
}

/**
 * The Palm Computing Platform termination routine.
 */
void	
EndApplication()
{
	// ensure all forms are closed
	FrmCloseAllForms();

	if (irAvail)
		IrClose(irref);
}

static void BSIrCallBack(IrConnect *conn, IrCallBackParms *parms)
{
#if 0
	switch (parms->event)
	{
	case LEVENT_DISCOVERY_CNF: 
		StoreDiscovery(parms->deviceList); 
		pending_nextstep = nextstep_irlap;
//		EvtWakeup();
		break;
	case LEVENT_LAP_CON_CNF: 
		nextstep = nextstep_irlmp;
		controller = true;
		EvtWakeup();
		break;
	case LEVENT_LAP_CON_IND:
//		FrmAlert(GotIRAlert);
		controller = false;
		game.IRGame = true;
		if (!game.started)
		{
			CurrentView = IRStatusForm;
			nextstep = 0;
			FrmPopupForm(CurrentView);
			/*
			FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
			"IrLAP from Peer");
			*/
		}
		break;
	case LEVENT_LAP_DISCON_IND:
		break;
	case LEVENT_LM_CON_CNF:
		// got a connection
		MemMove(game.u.human.serialNo,parms->rxBuff,parms->rxLen);
		if (irStartup.action == action_new)
			nextstep = nextstep_new;
		else
			nextstep = nextstep_load;
		EvtWakeup();

		break;
	case LEVENT_LM_CON_IND:
	/*
		if (!game.started)
		{
		
			CurrentView = IRStatusForm;
			FrmPopupForm(CurrentView);*/
			FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
			"Connecting...");
			FldDrawField(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)));
			MemMove(game.u.human.serialNo,parms->rxBuff,parms->rxLen);
			game.u.human.serialNo[parms->rxLen] = 0;
			packet.buff = serialNo;
			packet.len = serialNoLen;
			packet_in_use = true;
			last_packet_type = 0;
		//}
		break;
	case LEVENT_LM_DISCON_IND:
		break;
	case LEVENT_PACKET_HANDLED:
		packet_in_use = false;
		break;
	case LEVENT_DATA_IND:
		//SetData(parms->rxBuff,parms->rxLen); 
		if (parms->rxBuff[0] == packet_type_startup)
		{
			MemMove(&irStartup,parms->rxBuff,sizeof(irStartup));
			StrCopy(game.name,irStartup.name);
			switch (irStartup.action)
			{
			case action_new:
				NewGame();
				break;
			case action_load:
				LoadGameFromPeer();
				break;
			}
		}
		else if (parms->rxBuff[0] == packet_type_done)
		{
			opp_done = true;
			if (im_done && opp_done)
				game.started = true;
		}
		else if (parms->rxBuff[0] == packet_type_fire)
		{
			MemMove(&irFire,parms->rxBuff,sizeof(irFire));
			irReport.response = ReceiveShot(irFire.x,irFire.y);
			irReport.type = packet_type_report;
			packet.buff = (BytePtr)&irReport;
			packet.len = sizeof(irReport);
			packet_in_use = true;
			IrDataReq(irref,&connect,&packet);
		}
		else if (parms->rxBuff[0] == packet_type_report)
		{
			MemMove(&irReport,parms->rxBuff,sizeof(irReport));
			ShotStatus(irReport.response,irFire.x,irFire.y);
		}


		break;
	case LEVENT_STATUS_IND:
		switch (parms->status)
		{
		case IR_STATUS_NO_PROGRESS:
			break;
		case IR_STATUS_LINK_OK:
			break;
		case IR_STATUS_MEDIA_NOT_BUSY:
			nextstep = pending_nextstep;
			pending_nextstep = nextstep_none;
			EvtWakeup();
			break;
		}
		break;
	case LEVENT_TEST_CNF:
		switch (parms->status)
		{
		case IR_STATUS_SUCCESS:
			break;
		case IR_STATUS_FAILED:
			break;
		}
		break;
	case LEVENT_TEST_IND:
		break;
	}
	
	if (parms->event == LEVENT_LM_CON_IND)
		IrConnectRsp(irref,&connect,&packet,0);
#endif
}
