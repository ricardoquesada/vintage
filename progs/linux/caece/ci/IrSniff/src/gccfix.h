
#ifndef _GCCFIX_H
#define _GCCFIX_H

#ifdef __GNUC__

#define UNUSED  __attribute__ ((unused))
extern void _GccReleaseCode (UInt16, void *, UInt16);

#endif

#endif
