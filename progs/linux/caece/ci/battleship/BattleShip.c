/*
 * BattleShip.c
 *    9/1/1998 - A.J. Musgrove
 *
 * This program was originally written without IR for
 * Palm OS 2.0 with the GNU compiler. I did a complete
 * rewrite for this port to Metrowerks, Palm OS 3.0 
 * IR and the article for Dr. Dobb's. 
 */

#include <Pilot.h>
#include <irlib.h>
#include <SysEvtMgr.h>
#include "BattleShip_res.h" // Metrowerks generated

/*
 * Defines
 */
#define appID             'BatL'
#define appVersionNum     0x01
#define appPrefID         0x00
#define appPrefVersionNum 0x01
#define board_Square      10
#define board_Size        10
#define board_Left        0
#define board_Top         20
#define ship_Destroyer    0
#define ship_Cruisor      1
#define ship_Sub          2
#define ship_Battleship   3
#define ship_Carrier      4
#define ship_Max          ship_Carrier+1
#define orient_Horz       0
#define orient_Vert       1
#define fire_Hit          1
#define fire_Miss         2
#define fire_Won          3
#define fire_Sink         4 /* minus fire_Sink to determine ship */
#define packet_type_startup   1
#define packet_type_fire      2
#define packet_type_report    3
#define packet_type_done      4
#define nextstep_none         0
#define nextstep_discovery    1
#define nextstep_irlap        2
#define nextstep_irlmp        3
#define nextstep_new          4
#define nextstep_load         5
#define nextstep_play         6
#define action_new            1
#define action_load           2

/* 
 * static data 
 */
 
static const char *ships[] = {
	"Destroyer",
	"Cruisor",
	"Sub",
	"BattleShip",
	"Carrier"
	};
	
static const char *forms[] = {
	"Planning",
	"Damage Control",
	"Fire Control"
	};

static const char palmgame[] = "vs. Palm";

/*
 * Macros
 */
#define minRomVersion sysMakeROMVersion(3,0,0,sysROMStageRelease,0)
#define calc_ship_id(shp,dir,pos) (3000 + ((shp)*10) + ((dir)*5) + (pos))

/*
 * typedefs
 */
typedef struct
{
	Boolean sound;
} BattleShipPrefs;

typedef struct
{
	Byte type;
	Byte action;
	char name[31];
} IRStartup;

typedef struct
{
	Byte type;
	Word x, y;
} IRFire;

typedef struct
{
	Byte type;
	Word response;
} IRReport;

typedef struct
{
	Byte type;
} IRDone;

typedef struct
{
	Byte version;
} BattleShipAppInfo;

typedef struct
{
	Byte type;
	Byte length;
	Byte x,y;
	Byte orient;
	Boolean hit[5]; // maximum ship length
} Ship;

typedef struct
{
	Byte square[board_Size][board_Size];
} Board;

typedef struct
{
	char name[31];
	Ship ships[5];
	Board board;
	unsigned short recno;
	
	Boolean started,completed,won;
	Word hits, misses;
	
	Board fire;

	DateTimeType when_started;
		
	Boolean IRGame;
	Boolean myTurn;
	
	Boolean my_sinks[ship_Max], opp_sinks[ship_Max];
	
	union
	{
		struct
		{
			char serialNo[12];
		} human;
		struct
		{
			Ship ships[5];
			Board fire;
			Board board;
		} palm;
	} u;
} Game;

typedef struct
{
	int recno;
} StoredGame;

/*
 * Prototypes
 */
static void StartApplication(void);
static void StopApplication(void);
static void AppEventLoop(void);
static Boolean AppHandleEvent(EventPtr event);
static Boolean StartupFormEventHandler(EventPtr event);
static Boolean NewGameEventHandler(EventPtr event);
static Boolean LoadGameEventHandler(EventPtr event);
static Boolean PlacementFormEventHandler(EventPtr event);
static Boolean ShipsFormEventHandler(EventPtr event);
static Boolean IRStatusEventHandler(EventPtr event);
static Boolean FireControlFormEventHandler(EventPtr event);
static void DrawRawGrid();
static void SetupPlacementForm(FormPtr frm);
static void PlaceShip(FormPtr frm, Byte type, Byte x, Byte y);
static void MoveShip(FormPtr frm, Byte Type, Byte x, Byte y);
static Boolean CheckPenEvent(FormPtr frm, Word x, Word y);
static void NewGame();
static void PlayButton();
static void DrawShips(Boolean withHits);
static void PlacementDone();
static void ChangeForm(Word listId);
static void SetupShipsForm();
static void SetupFireControlForm();
static Boolean OpenDatabase();
static void LoadGamesList(ListPtr list);
static void ShowGameDetails(FormPtr frm, UInt game_num);
static void LoadGame(UInt game_num);
static Boolean MakeMove(FormPtr frm, Word x, Word y);
static void TakeRandomShot(Board *fireBoard, Word *x, Word *y);
static void PalmPlaceShips();
static void PalmTakeShot();
static int PalmRecieveShot(Word x, Word y);
static void TakeShot(Word x, Word y);
static void ShotStatus(int stat, Word bx, Word by);
static int ReceiveShot(Word x, Word y);
static void DrawFireControlStatus();
static void MyRandomShot();
static void PlaceShipsAtRandom(Ship *ships, Board *board);
static void UpdateHitInds(FormPtr frm, Boolean *stats);
static void BSIrCallBack(IrConnect *conn, IrCallBackParms *parms);
static void StoreDiscovery(IrDeviceList* deviceList);
static void LoadGameFromPeer();
/*
 * Globals
 */
static MenuBarPtr CurrentMenu = 0;
static Word CurrentView;
static Game game;
static DmOpenRef bsdb;
static StoredGame *GamesList = 0;
static UInt sgames = 0;
static char *games[100]; 
static int game_selected;
static Boolean irAvail;
static UInt irref;
static Boolean packet_in_use = false;
static IrConnect connect;
static IrPacket packet;
static IrDeviceAddr dev;
static Byte last_packet_type;
static IRStartup irStartup;
static IRDone irDone;
static IRFire irFire;
static IRReport irReport;
static BytePtr serialNo;
static Word serialNoLen;
static Byte nextstep = nextstep_none;
static Byte pending_nextstep = nextstep_none;
static Boolean controller;
static Boolean im_done = false;
static Boolean opp_done = false;

/*
 * Implementation
 */

DWord PilotMain(Word cmd, Ptr cmdBPB, Word launchFlags)
{
	if (cmd == sysAppLaunchCmdNormalLaunch)
	{
		StartApplication();
		AppEventLoop();
		StopApplication();
	}
	
	return 0;
}

static void StartApplication(void )
{
	// check for ir
	if (SysLibFind(irLibName,&irref) != 0)
	{
		irAvail = false;
	}
	else
	{
		if (IrOpen(irref,irOpenOptSpeed115200) != 0)
			irAvail = false;
		else
		{
			irAvail = true;
			IrBind(irref,&connect,BSIrCallBack);
			IrSetDeviceInfo(irref,(BytePtr)&connect.lLsap,1);
			IrSetConTypeLMP(&connect);
		}
	}
	
	if (!irAvail)
		FrmAlert(NoIRAlert);
		
	SysGetROMToken(0,sysROMTokenSnum,&serialNo,&serialNoLen);
	SysRandom(TimGetSeconds());
	game.recno = 65535;
	game.started = false;
	CurrentView = StartupFormForm;
	CurrentMenu = MenuInit(StartupMenuBar);
	FrmGotoForm(CurrentView);

	OpenDatabase();	
}

static void StopApplication(void)
{
	VoidHand hand;
	VoidPtr ptr;
	
	if (game.recno != 65535)
	{
		hand = DmGetRecord(bsdb,game.recno);
		ptr = MemHandleLock(hand);
		DmWrite(ptr,0,&game,sizeof(game));
		MemHandleUnlock(hand);
		DmReleaseRecord(bsdb,game.recno,true);
	}
	
	if (irAvail)
		IrClose(irref);
}

static void AppEventLoop(void)
{
	EventType event;
	Word error;
	
	do
	{
		EvtGetEvent(&event, evtWaitForever);
		if (SysHandleEvent(&event))
			continue;
		if (MenuHandleEvent(CurrentMenu, &event, &error))
			continue;
		if (AppHandleEvent(&event))
			continue;
		FrmDispatchEvent(&event);
	} while (event.eType != appStopEvent);
}

static Boolean AppHandleEvent(EventPtr event)
{
	FormPtr form;
	Word formId;
	Boolean handled = false;
	
	if (event->eType == frmLoadEvent)
	{
		formId = event->data.frmLoad.formID;
		form = FrmInitForm(formId);
		FrmSetActiveForm(form);
		
		switch (formId)
		{
		case StartupFormForm:
			FrmSetEventHandler(form,StartupFormEventHandler);
			break;
		case NewGameForm:
			FrmSetEventHandler(form,NewGameEventHandler);
			break;
		case PlacementForm:
			FrmSetEventHandler(form,PlacementFormEventHandler);
			break;
		case ShipsForm:
			FrmSetEventHandler(form,ShipsFormEventHandler);
			break;
		case FireControlForm:
			FrmSetEventHandler(form,FireControlFormEventHandler);
			break;
		case LoadGameForm:
			FrmSetEventHandler(form,LoadGameEventHandler);
			break;
		case IRStatusForm:
			FrmSetEventHandler(form,IRStatusEventHandler);
		}
		
		handled = true;
	}
	
	return handled;
}

static Boolean StartupFormEventHandler(EventPtr event)
{
	Boolean handled = false;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		FrmDrawForm(FrmGetActiveForm());
		handled = true;
		break;
	case ctlSelectEvent:
		switch (event->data.ctlSelect.controlID)
		{
		case StartupFormButtonID_playButton:
			PlayButton();
			handled = true;
			break;
		}
	}
	
	return handled;
}

static void PlayButton()
{
	if (FrmGetControlValue(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),
		StartupFormPushbuttonID_humanPushButton))==1)
	{
		game.IRGame = true;
		if (!irAvail)
		{
			FrmAlert(NoIRAlert);
			return;
		}
	}
	else if (FrmGetControlValue(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),
		StartupFormPushbuttonID_palmPushButton))==1)
	{
		StrCopy(game.name,palmgame);
		game.IRGame = false;
	}
	else
	{
		FrmAlert(PickOppAlert);
		return;
	}
		
	if (FrmGetControlValue(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),
		StartupFormPushbuttonID_newPushButton))==1)
	{
		if (game.IRGame)
		{
			irStartup.action = action_new;
			CurrentView = IRStatusForm;
			FrmPopupForm(CurrentView);
			nextstep = nextstep_discovery;

		}
		else
		{
			CurrentView = NewGameForm;	
			FrmPopupForm(CurrentView);
		}
	}
	else if (FrmGetControlValue(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),
		StartupFormPusbbuttonID_loadPushButton))==1)

	{
		if (game.IRGame)
		{
			irStartup.action = action_load;
			CurrentView = IRStatusForm;
			FrmPopupForm(CurrentView);
			nextstep = nextstep_discovery;

		}
		else
		{
			CurrentView = LoadGameForm;
			FrmPopupForm(CurrentView);
		}
	}
	else
	{
		FrmAlert(PickStartupTypeAlert);
		return;
	}
		
}

static Boolean NewGameEventHandler(EventPtr event)
{
	Boolean handled = false;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		FrmDrawForm(FrmGetActiveForm());
		handled = true;
		break;
	case ctlSelectEvent:
		switch (event->data.ctlSelect.controlID)
		{
		case NewGameButtonID_cancelButton:
			CurrentView = StartupFormForm;
			FrmReturnToForm(CurrentView);
			handled = true;
			break;
		case NewGameButtonID_okButton:
			NewGame();
			FrmReturnToForm(StartupFormForm);
			CurrentView = PlacementForm;
			FrmGotoForm(CurrentView);
			handled = true;
			break;
		}
	}
	
	return handled;
}

static Boolean LoadGameEventHandler(EventPtr event)
{
	Boolean handled = false;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		game_selected = -1;
		LoadGamesList(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),LoadGameGameListList)));
		FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),LoadGameGameTypeField)),
			game.name);
		FrmDrawForm(FrmGetActiveForm());
		handled = true;
		break;
	case ctlSelectEvent:
		switch (event->data.ctlSelect.controlID)
		{
		case LoadGameCancelButton:
			CurrentView = StartupFormForm;
			FrmReturnToForm(CurrentView);
			FrmDrawForm(FrmGetFormPtr(CurrentView));
			handled = true;
			break;
		case LoadGameOKButton:
			if (game_selected < 0)
			{
				FrmAlert(NoLoadAlert);
			}
			else
			{
				LoadGame(game_selected);
				CurrentView = StartupFormForm;
				FrmReturnToForm(CurrentView);
				CurrentView = PlacementForm;
				FrmGotoForm(CurrentView);
			}
			handled = true;
			break;
		}
	case popSelectEvent:
		switch (event->data.popSelect.listID)
		{
		case LoadGameGameListList:
			ShowGameDetails(FrmGetActiveForm(),
				event->data.popSelect.selection);
			game_selected = event->data.popSelect.selection;
			handled = false;
			break;
		}
		break;
	}
	
	return handled;
}


static Boolean PlacementFormEventHandler(EventPtr event)
{
	Boolean handled = false;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		SetupPlacementForm(FrmGetActiveForm());
		FrmDrawForm(FrmGetActiveForm());
		handled = true;
		break;
	case penDownEvent:
		handled = CheckPenEvent(FrmGetActiveForm(),
			event->screenX,event->screenY);
		break;
	case ctlSelectEvent:
		switch (event->data.ctlSelect.controlID)
		{
		case PlacementButtonID_doneButton:
			PlacementDone();
			handled = true;
			break;
		case PlacementRandomButton:
			PlaceShipsAtRandom(game.ships,&game.board);
			DrawRawGrid();
			DrawShips(false);
			handled = true;
			break;
		}
		break;
	case popSelectEvent:
		switch (event->data.popSelect.listID)
		{
		case PlacementListID_formsList:
			ChangeForm(event->data.popSelect.selection);
			handled = true;
			break;

		}
		break;
	}
	
	return handled;
}

static void ChangeForm(Word listId)
{
	switch (listId)
	{
	case 0: // placement
		CurrentView = PlacementForm;
		break;
	case 1: // ships
		CurrentView = ShipsForm;
		break;
	case 2: // fire control
		CurrentView = FireControlForm;
		break;
	}
	
	FrmGotoForm(CurrentView);
}

static void PlacementDone()
{
	int i;
	
		
	for (i = 0; i < ship_Max; i++)
	{
		if (game.ships[i].x == 0 || game.ships[i].y == 0)
		{
			FrmAlert(PlaceShipsAlert);
			return;
		}
	}
	
	im_done = true;
	if (!game.IRGame)
	{
		game.started = true;
	}
	else
	{
		irDone.type = packet_type_done;
		packet.buff = (BytePtr)&irDone;
		packet.len = sizeof(irDone);
		packet_in_use = true;
		IrDataReq(irref,&connect,&packet);
		
		if (opp_done)
		{
			game.started = true;
		}
	}
	FrmGotoForm(CurrentView);
	
}

static void SetupPlacementForm(FormPtr frm)
{
	ListPtr formList, shipList;
	
	DrawRawGrid();
	DrawShips(false);
	
	formList = FrmGetObjectPtr(frm,
		FrmGetObjectIndex(frm,PlacementListID_formsList));
	LstSetListChoices(formList,(char **)forms,3);
		
	if (game.started || im_done) // get rid of some stuff
	{
		FrmHideObject(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementButtonID_doneButton));
		FrmHideObject(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementRandomButton));
		FrmHideObject(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementShipPopupPopTrigger));
		FrmHideObject(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementPushbuttonID_horzPushButton));
		FrmHideObject(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementPusbbuttonID_vertPushButton));
		
	}
	else
	{
		
		shipList = FrmGetObjectPtr(frm,
			FrmGetObjectIndex(frm,PlacementListID_shipsList));
		
			LstSetListChoices(shipList,(char **)ships,5);
	}
}

static void DrawRawGrid()
{
	int i;
	RectangleType rec;

	rec.topLeft.x = board_Left;
	rec.topLeft.y = board_Top;
	rec.extent.x = board_Square*board_Size;
	rec.extent.y = board_Square*board_Size;
	WinEraseRectangle(&rec,0);

	/*
	 * Draw the Grid
	 */
	for (i = 0; i < board_Size+1; i++)
	{
		WinDrawGrayLine(board_Left,(i*board_Square)+board_Top,board_Square * 10,(i*board_Square)+board_Top);
		WinDrawGrayLine((i*board_Square)+board_Left,board_Top,(i*board_Square)+board_Left,(board_Square * 10)+board_Top);
	}

}

static void DrawShips(Boolean withHits)
{
	int i,w;
	Word id;
	VoidHand hbmp;
	BitmapPtr bmp;
	
	for (i = 0; i < ship_Max; i++)
	{
		if (game.ships[i].x > 0 && game.ships[i].y > 0)
		{
			for (w = 0; w < game.ships[i].length; w++)
			{
				if (withHits && game.ships[i].hit[w])
					id = hitBitmap;
				else
					id = calc_ship_id(i,game.ships[i].orient,w);
				hbmp =  DmGetResource(bitmapRsc,id);
				bmp = MemHandleLock(hbmp);
				if (game.ships[i].orient == orient_Horz)
				{
					WinDrawBitmap(
						bmp,
						(((game.ships[i].x-1)+w)*board_Square)+board_Left,
						(game.ships[i].y-1)*board_Square+board_Top);
				}
				else
				{
					WinDrawBitmap(
						bmp,
						(game.ships[i].x-1)*board_Square+board_Left,
						(((game.ships[i].y-1)+w)*board_Square)+board_Top);
				}
				MemHandleUnlock(hbmp);
				DmReleaseResource(hbmp);
			}
		}
	}
}

static Boolean CheckPenEvent(FormPtr frm, Word x, Word y)
{
	Word bx, by;
	Word orient,ship;	
	int i,w;
	Word nbx,nby;
	
	if ( (x >= board_Left) && (x <= board_Left + (board_Square*board_Size))
		&& (y >= board_Top) && (y <= board_Top + (board_Square*board_Size)))
	{
		// compute logical board coordinates
		bx = ((x-board_Left)/board_Square)+1;
		by = ((y-board_Top)/board_Square)+1;
		
		// get orientation
		if (FrmGetControlValue(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementPushbuttonID_horzPushButton))==1)
		{
			orient = orient_Horz;
		}
		else if (FrmGetControlValue(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),
			PlacementPusbbuttonID_vertPushButton))==1)
		{
			orient = orient_Vert;
		}
		else
		{
			FrmAlert(SelectOrientAlert);
			return true;
		}
		
		// get the selected ship
		ship = LstGetSelection(
			FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),PlacementListID_shipsList)));
	
		// see if there are any conflicts or fitting problems
		for (i = 0; i < game.ships[ship].length; i++)
		{
			if (orient == orient_Horz)
			{
				nbx = bx + i;
				nby = by;
			}
			else
			{
				nbx = bx;
				nby = by + i;
			}
			
			// fit on board?
			if (nbx > board_Size || nby > board_Size)
			{
				FrmAlert(NoFitAlert);
				return true;
			}
			
			// conflict with other ship?
			if (game.board.square[nbx-1][nby-1] != ship_Max &&
			game.board.square[nbx-1][nby-1] != ship)
			{
				FrmAlert(ShipConflictAlert);
				return true;
			}
		}
		
		// ok, it fits, lets clear any old placement
		for (i = 0; i < board_Size; i++)
			for (w = 0; w < board_Size; w++)
				if (game.board.square[i][w] == ship)
					game.board.square[i][w] = ship_Max;
					
		// update board
		for (i = 0; i < game.ships[ship].length; i++)
		{
			if (orient == orient_Horz)
			{
				nbx = bx + i;
				nby = by;
			}
			else
			{
				nbx = bx;
				nby = by + i;
			}
			
			game.board.square[nbx-1][nby-1] = ship;
		}
		
		// update ship
		game.ships[ship].x = bx;
		game.ships[ship].y = by;
		game.ships[ship].orient = orient;

		// redraw the playfield
		DrawRawGrid();
		DrawShips(false);
				
		return true;
	}
	else
	{
		return false;
	}
}

static void NewGame()
{	
	int x,y;
	VoidHand hand;
	VoidPtr ptr;
	
	MemSet(&game.board,sizeof(game.board),0);
	MemSet(&game.ships,sizeof(game.ships),0);
	MemSet(game.my_sinks,sizeof(game.my_sinks),0);
	MemSet(game.opp_sinks,sizeof(game.opp_sinks),0);
	
	if (CurrentView == NewGameForm)
	{
		StrCopy(game.name,
		FldGetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),NewGameFieldID_nameField))));
	}
	
	TimSecondsToDateTime (TimGetSeconds (), &game.when_started);
			
	game.ships[ship_Destroyer].type = ship_Destroyer;
	game.ships[ship_Destroyer].length = 2;
	game.ships[ship_Cruisor].type = ship_Cruisor;
	game.ships[ship_Cruisor].length = 3;
	game.ships[ship_Sub].type = ship_Sub;
	game.ships[ship_Sub].length = 3;
	game.ships[ship_Battleship].type = ship_Battleship;
	game.ships[ship_Battleship].length = 4;
	game.ships[ship_Carrier].type = ship_Carrier;
	game.ships[ship_Carrier].length = 5;	
	
	for (x = 0; x < board_Size; x++)
		for (y = 0; y < board_Size; y++)
			game.board.square[x][y] = ship_Max;

	game.started = game.completed = game.won = false;
	
	if (game.IRGame == false)
	{
		game.myTurn = true;
		
		PalmPlaceShips();
	}
	else
	{
		if (controller)
		{
			game.myTurn = true;
			irStartup.type = packet_type_startup;
			irStartup.action = action_new;
			StrCopy(irStartup.name,game.name);
			packet_in_use = true;
			packet.buff = (BytePtr)&irStartup;
			packet.len = sizeof(irStartup);
			IrDataReq(irref,&connect,&packet);
		}
		else
		{
			game.myTurn = false;
		}
		
		FrmReturnToForm(StartupFormForm);		
		CurrentView = PlacementForm;
		FrmGotoForm(CurrentView);
	}
		
	hand = DmNewRecord(bsdb,&game.recno,sizeof(game));
	ptr = MemHandleLock(hand);
	DmWrite(ptr,0,&game,sizeof(game));
	MemHandleUnlock(hand);
	DmReleaseRecord(bsdb,game.recno,true);

}

static Boolean ShipsFormEventHandler(EventPtr event)
{
	Boolean handled = false;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		FrmDrawForm(FrmGetActiveForm());
		SetupShipsForm();
		DrawRawGrid();
		DrawShips(true);
		UpdateHitInds(FrmGetActiveForm(),game.my_sinks);
		handled = true;
		break;
	case popSelectEvent:
		switch (event->data.popSelect.listID)
		{
		case ShipsListID_formsList:
			ChangeForm(event->data.popSelect.selection);
			handled = true;
			break;

		}
		break;
	}
	
	return handled;
}

static Boolean IRStatusEventHandler(EventPtr event)
{
	Boolean handled = false;
	Word step;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		FrmDrawForm(FrmGetActiveForm());
		handled = true;
		break;
	}

	step = nextstep;
	nextstep = 0;
	switch (step)
	{
	case nextstep_discovery:
		FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
		"Discovery");
		
		FldDrawField(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)));
		
		IrDiscoverReq(irref,&connect);
		break;
	case nextstep_irlap:
		FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
		"IrLAP");
		
		FldDrawField(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)));
		
		IrConnectIrLap(irref,dev);
		break;
	case nextstep_irlmp:
		FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
		"IrLMP");
		
		FldDrawField(FrmGetObjectPtr(FrmGetActiveForm(),
		FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)));
		
		packet.buff = serialNo;
		packet.len = serialNoLen;
		IrConnectReq(irref,&connect,&packet,0);
		break;
	case nextstep_new:
		CurrentView = NewGameForm;
		FrmGotoForm(CurrentView);
		break;
	case nextstep_load:
		MemMove(game.name,game.u.human.serialNo,sizeof(game.u.human.serialNo));
		game.name[(sizeof(game.u.human.serialNo))] = 0;
		CurrentView = LoadGameForm;
		FrmGotoForm(CurrentView);
		break;
	}
	
	return handled;
}

static Boolean FireControlFormEventHandler(EventPtr event)
{
	Boolean handled = false;
	
	switch (event->eType)
	{
	case frmOpenEvent:
		FrmDrawForm(FrmGetActiveForm());
		SetupFireControlForm();
		DrawRawGrid();
		DrawFireControlStatus();
		UpdateHitInds(FrmGetActiveForm(),game.opp_sinks);
		handled = true;
		break;
	case penDownEvent:
		handled = MakeMove(FrmGetActiveForm(),
			event->screenX,event->screenY);

		break;
	case ctlSelectEvent:
		switch (event->data.ctlSelect.controlID)
		{
		case FireControlRandomButton:
			MyRandomShot();
			handled = true;
			break;
		}
		break;
	case popSelectEvent:
		switch (event->data.popSelect.listID)
		{
		case FireControlListID_formsList:
			ChangeForm(event->data.popSelect.selection);
			handled = true;
			break;

		}
		break;
	}
	
	return handled;
}

static void SetupShipsForm()
{
	FormPtr frm;
	ListPtr formList;
	
	frm = FrmGetActiveForm();
	formList = FrmGetObjectPtr(frm,
		FrmGetObjectIndex(frm,ShipsListID_formsList));
	LstSetListChoices(formList,(char **)forms,3);
		
}

static void SetupFireControlForm()
{
	FormPtr frm;
	ListPtr formList;
	
	frm = FrmGetActiveForm();
	formList = FrmGetObjectPtr(frm,
		FrmGetObjectIndex(frm,FireControlListID_formsList));
	LstSetListChoices(formList,(char **)forms,3);
}

static Boolean OpenDatabase()
{
	unsigned short at;
	
	at = 30000;
	// do a create, hurts nothing if it fails
	DmCreateDatabase(0,"BattleShip Data",appID,'Data',false);
	bsdb = DmOpenDatabaseByTypeCreator('Data',appID,dmModeReadWrite);

	if (!bsdb)
	{
		FrmAlert(DatabaseCreationAlert);
		return false;
	}
	
	return true;
}

static void LoadGamesList(ListPtr list)
{
	UInt recs, i;
	VoidHand hand;
	Ptr ptr;
	StoredGame *tmp;
	Game g;
	Boolean goodGame;

	// clear old memory
	if (GamesList != 0)
	{
		for (i = 0; i < sgames; i++)
			MemPtrFree(games[i]);
		sgames = 0;
		MemPtrFree(GamesList);
		GamesList = 0;
	}
	
	LstEraseList(list);
		
	recs = DmNumRecords(bsdb);
	
	for (i = 0; i < recs && sgames < 100; i++)
	{
		// get the record into 'g'
		hand = DmGetRecord(bsdb,i);
		ptr = MemHandleLock(hand);
		MemMove(&g,ptr,sizeof(g));
		MemHandleUnlock(hand);
		DmReleaseRecord(bsdb,i,false);
		
		goodGame = false;
		// see if the record matches our criteria
		if (game.IRGame == g.IRGame)
		{
			if (game.IRGame == true)
			{
				if (StrCompare(game.u.human.serialNo,g.u.human.serialNo)==0)
				{
					goodGame = true;
				}
			}
			else
			{
				goodGame = true;
			}
		}
		
		if (goodGame)
		{	
			// allocate space
			if (GamesList == 0)
			{
				GamesList = MemPtrNew(sizeof(StoredGame));
			}
			else
			{
				// i think there is a bug in this function, so i am having to alwaya realloc/copy
				//if (MemPtrResize(GamesList,sizeof(StoredGame)*(sgames+1))==memErrNotEnoughSpace)
//				{
					tmp = MemPtrNew(sizeof(StoredGame)*(sgames+1));
					MemMove(tmp,GamesList,sizeof(StoredGame)*sgames);
					MemPtrFree(GamesList);
					GamesList = tmp;
//				}
			}
			
			games[sgames] = MemPtrNew(31);
			
			// copy the game
			GamesList[sgames].recno = i;
			StrCopy(games[sgames],g.name);
			sgames++;
		}
	}
	
	LstSetListChoices(list,games,sgames);		
}

static void LoadGame(UInt game_num)
{
	VoidHand hand;
	Ptr ptr;
	
	hand = DmGetRecord(bsdb,game_num);
	ptr = MemHandleLock(hand);
	MemMove(&game,ptr,sizeof(game));
	MemHandleUnlock(hand);
	DmReleaseRecord(bsdb,game_num,false);
	
	if (game.IRGame)
	{
		irStartup.type = packet_type_startup;
		irStartup.action = action_load;
		StrCopy(irStartup.name,game.name);
		packet.buff = (BytePtr)&irStartup;
		packet.len = sizeof(irStartup);
		packet_in_use = true;
		IrDataReq(irref,&connect,&packet);
	}
}

static void ShowGameDetails(FormPtr frm, UInt game_num)
{
	Game g;
	VoidHand hand;
	Ptr ptr;
	char the_time[timeStringLength];
	
	hand = DmGetRecord(bsdb,game_num);
	ptr = MemHandleLock(hand);
	MemMove(&g,ptr,sizeof(g));
	MemHandleUnlock(hand);
	DmReleaseRecord(bsdb,game_num,false);
	
	// check status
	if (g.completed)
	{
		if (g.won)
		{
			FldSetTextPtr(FrmGetObjectPtr(frm,FrmGetObjectIndex(
				frm,LoadGameStatusField)),"Won");
		}
		else
		{
			FldSetTextPtr(FrmGetObjectPtr(frm,FrmGetObjectIndex(
				frm,LoadGameStatusField)),"Lost");

		}
	}
	else if (g.started)
	{
		FldSetTextPtr(FrmGetObjectPtr(frm,FrmGetObjectIndex(
			frm,LoadGameStatusField)),"Started");
	}
	else
	{
		FldSetTextPtr(FrmGetObjectPtr(frm,FrmGetObjectIndex(
			frm,LoadGameStatusField)),"Planning");
	}
	
	DateToDOWDMFormat(g.when_started.month,g.when_started.day,
		g.when_started.year,dfMDYLongWithComma,the_time);
		
	FldSetTextPtr(FrmGetObjectPtr(frm,FrmGetObjectIndex(
		frm,LoadGameStartedField)),the_time);
		
	FldDrawField(FrmGetObjectPtr(frm,
		FrmGetObjectIndex(frm,LoadGameStartedField)));
	FldDrawField(FrmGetObjectPtr(frm,
		FrmGetObjectIndex(frm,LoadGameStatusField)));
}

static Boolean MakeMove(FormPtr frm, Word x, Word y)
{
	Word bx, by;
	
	if ( (x >= board_Left) && (x <= board_Left + (board_Square*board_Size))
		&& (y >= board_Top) && (y <= board_Top + (board_Square*board_Size)))
	{
		// compute logical board coordinates
		bx = ((x-board_Left)/board_Square)+1;
		by = ((y-board_Top)/board_Square)+1;
		
		if (!game.started)
		{
			FrmAlert(GameNotStartedAlert);
			return true;
		}
				
		if (game.completed)
		{
			FrmAlert(AlreadyCompletedAlert);
			return true;
		}
		
		if (!game.myTurn)
		{
			FrmAlert(NotYourMoveAlert);
			return true;
		}
	
		if (game.completed)
		{
			FrmAlert(AlreadyCompletedAlert);
			return true;
		}
		

		if (game.fire.square[bx-1][by-1] != 0)
		{
			FrmAlert(AlreadyShotAlert);
			return true;
		}
		
		game.myTurn = false;
		
		// if we are here we have a valid shot attempted
		TakeShot(bx,by);
		
		return true;
	}
	else
	{
		return false;
	}
}

static void MyRandomShot()
{
	Word x,y;
	
	if (game.completed)
	{
		FrmAlert(AlreadyCompletedAlert);
		return;
	}
		
	if (!game.myTurn)
	{
		FrmAlert(NotYourMoveAlert);
		return;
	}
	
	if (game.completed)
	{
		FrmAlert(AlreadyCompletedAlert);
		return;
	}
		
	if (!game.started)
	{
		FrmAlert(GameNotStartedAlert);
		return;
	}
		
	game.myTurn = false;
		
	TakeRandomShot(&game.fire,&x,&y);
	TakeShot(x,y);

}

static void TakeShot(Word x, Word y)
{
	if (game.IRGame == false)
	{
		ShotStatus(PalmRecieveShot(x,y),x,y);
		if (!game.completed) // is it over
		{
			PalmTakeShot();
		}
	}
	else
	{
		irFire.x = x;
		irFire.y = y;
		irFire.type = packet_type_fire;
		packet_in_use = true;
		packet.buff = (BytePtr)&irFire;
		packet.len = sizeof(irFire);
		IrDataReq(irref,&connect,&packet);
	}
}

static void ShotStatus(int stat, Word bx, Word by)
{
	if (stat == fire_Hit)
	{
		game.hits++;
		game.fire.square[bx-1][by-1] = fire_Hit;
	}
	else if (stat == fire_Miss)
	{
		game.misses++;
		game.fire.square[bx-1][by-1] = fire_Miss;
	}
	else if (stat == fire_Won)
	{
		game.fire.square[bx-1][by-1] = fire_Hit;
		game.completed = true;
		game.won = true;
		MemSet(game.opp_sinks,sizeof(game.opp_sinks),true);		
		FrmAlert(GameWonAlert);
	}
	else if (stat >= fire_Sink)
	{
		stat -= fire_Sink;
		game.opp_sinks[stat] = true;
		game.fire.square[bx-1][by-1] = fire_Hit;
		FrmCustomAlert(ShipSankAlert,(CharPtr)ships[stat],"","");
	}
	
	if (CurrentView == FireControlForm)
	{
		DrawRawGrid();
		DrawFireControlStatus();
		UpdateHitInds(FrmGetActiveForm(),game.opp_sinks);
	}
}

static void PalmPlaceShips()
{
	MemSet(&game.u.palm.ships,sizeof(game.u.palm.ships),0);
	
	game.u.palm.ships[ship_Destroyer].type = ship_Destroyer;
	game.u.palm.ships[ship_Destroyer].length = 2;
	game.u.palm.ships[ship_Cruisor].type = ship_Cruisor;
	game.u.palm.ships[ship_Cruisor].length = 3;
	game.u.palm.ships[ship_Sub].type = ship_Sub;
	game.u.palm.ships[ship_Sub].length = 3;
	game.u.palm.ships[ship_Battleship].type = ship_Battleship;
	game.u.palm.ships[ship_Battleship].length = 4;
	game.u.palm.ships[ship_Carrier].type = ship_Carrier;
	game.u.palm.ships[ship_Carrier].length = 5;	

	PlaceShipsAtRandom(game.u.palm.ships,&game.u.palm.board);
	
	return;
}

static void PalmTakeShot()
{
	Word x, y;
	
	TakeRandomShot(&game.u.palm.fire,&x,&y);
	game.u.palm.fire.square[x-1][y-1] = ReceiveShot(x,y);
	
	game.myTurn = true; // once the shot has been taken
	
	return;
}

static int PalmRecieveShot(Word x, Word y)
{
	Word bx, by, ship, i, w;
	Boolean sunk, won;
	
	if (game.u.palm.board.square[x-1][y-1] == ship_Max)
		return fire_Miss;
		
	// hit something, figure out what
	ship = game.u.palm.board.square[x-1][y-1];
	bx = game.u.palm.ships[ship].x;
	by = game.u.palm.ships[ship].y;
	
	i = 0;
	while ( (bx != x) || (by != y) )
	{
		if (game.u.palm.ships[ship].orient == orient_Horz)
		{
			bx++;
		}
		else
		{
			by++;
		}
		i++;
	}
	
	// ok, found the spot, mark it as hit
	if (game.u.palm.ships[ship].hit[i]) // already hit
		return fire_Miss;
		
	game.u.palm.ships[ship].hit[i] = true;
	
	// see if it won
	won = true;
	for (i = 0; i < ship_Max; i++)
		for (w = 0; w < game.u.palm.ships[i].length; w++)
			if (game.u.palm.ships[i].hit[w] == false)
				won = false;
				
	if (won)
		return fire_Won;
		
	// see if it is sunk
	sunk = true;
	for (i = 0; i < game.u.palm.ships[ship].length; i++)
		if (game.u.palm.ships[ship].hit[i] == false)
			sunk = false;
			
	if (sunk)
		return fire_Sink + ship;
	else
		return fire_Hit;
}

static int ReceiveShot(Word x, Word y)
{
	// see what effect it had
	Word bx, by, ship, i, w;
	Boolean sunk, won;

	game.myTurn = true;
		
	if (game.board.square[x-1][y-1] == ship_Max)
		return fire_Miss;
		
	// hit something, figure out what
	ship = game.board.square[x-1][y-1];
	bx = game.ships[ship].x;
	by = game.ships[ship].y;
	
	i = 0;
	while ( (bx != x) || (by != y) )
	{
		if (game.ships[ship].orient == orient_Horz)
		{
			bx++;
		}
		else
		{
			by++;
		}
		i++;
	}
	
	// ok, found the spot, mark it as hit
	if (game.ships[ship].hit[i]) // already hit
		return fire_Miss;
		
	game.ships[ship].hit[i] = true;
	
	// see if it won
	won = true;
	for (i = 0; i < ship_Max; i++)
		for (w = 0; w < game.ships[i].length; w++)
			if (game.ships[i].hit[w] == false)
				won = false;
				
	if (won)
	{
		game.completed = true;
		game.won = false;
		game.my_sinks[ship] = true;
		if (CurrentView == ShipsForm)
		{
			UpdateHitInds(FrmGetActiveForm(),game.my_sinks);
			DrawRawGrid();
			DrawShips(true);
		}
		FrmAlert(GameLostAlert);
		return fire_Won;
	}
		
	// see if it is sunk
	sunk = true;
	for (i = 0; i < game.ships[ship].length; i++)
		if (game.ships[ship].hit[i] == false)
			sunk = false;
			
	if (sunk)
	{
		game.my_sinks[ship] = true;
		if (CurrentView == ShipsForm)
		{
			UpdateHitInds(FrmGetActiveForm(),game.my_sinks);
			DrawRawGrid();
			DrawShips(true);
	
		}
		FrmCustomAlert(MyShipSankAlert,(CharPtr)ships[ship],"","");
		return fire_Sink + ship;
	}
	else
	{
		if (CurrentView == ShipsForm)
		{
			DrawRawGrid();
			DrawShips(true);
		}
		return fire_Hit;
	}

}

static void DrawFireControlStatus()
{
	Word x,y;
	VoidHand hbmp_hit, hbmp_miss;
	BitmapPtr bmp_hit, bmp_miss;
	
	hbmp_hit = DmGetResource(bitmapRsc,hitBitmap);
	hbmp_miss = DmGetResource(bitmapRsc,missBitmap);
	bmp_hit = MemHandleLock(hbmp_hit);
	bmp_miss = MemHandleLock(hbmp_miss);
	
	for (x = 0; x < board_Size; x++)
	{
		for (y = 0; y < board_Size; y++)
		{
			if (game.fire.square[x][y] == fire_Hit)
			{
				WinDrawBitmap(bmp_hit,
				x*board_Square+board_Left,
				y*board_Square+board_Top);
			}
			else if (game.fire.square[x][y] == fire_Miss)
			{
				WinDrawBitmap(bmp_miss,
				x*board_Square+board_Left,
				y*board_Square+board_Top);

			}
		}
	}
	
	MemHandleUnlock(hbmp_hit);
	MemHandleUnlock(hbmp_miss);
	DmReleaseResource(hbmp_hit);
	DmReleaseResource(hbmp_miss);
}

static void PlaceShipsAtRandom(Ship *ships, Board *board)
{
	int i,ship,pos;
	Word bx, by, orient;
	Word nbx, nby;
	Boolean placed,badpos;
	
	SysRandom(TimGetSeconds());
	
	// clear old board
	for (bx = 0; bx < board_Size; bx++)
		for (by = 0; by < board_Size; by++)
			board->square[bx][by] = ship_Max;
			
	for (ship = 0; ship < ship_Max; ship++)
	{
		ships[ship].x = ships[ship].y = 0;
		
		// pick a random location
		pos = SysRandom(0) % (board_Size*board_Size);	
		orient = SysRandom(0) % 2;
		placed = false;
		
		while (!placed)
		{
			by = pos / 10;
			bx = pos % 10;
			
			badpos = false;
			// see if there are any conflicts or fitting problems
			for (i = 0; i < ships[ship].length; i++)
			{
				if (orient == orient_Horz)
				{
					nbx = bx + i;
					nby = by;
				}
				else
				{
					nbx = bx;
					nby = by + i;
				}
			
				// fit on board?
				if (nbx >= board_Size || nby >= board_Size)
				{
					pos++;
					pos = pos % (board_Size * board_Size);
					badpos = true;
					continue;
				}
			
				// conflict with other ship?
				if (board->square[nbx][nby] != ship_Max)
				{
					pos++;
					pos = pos % (board_Size * board_Size);
					badpos = true;
					continue;
				}

			}
			
			if (badpos)
				continue;
			
			// put ship			
			placed = true;
			ships[ship].x = bx+1;
			ships[ship].y = by+1;
			ships[ship].orient = orient;
			
			// update board
			for (i = 0; i < ships[ship].length; i++)
			{
				if (orient == orient_Horz)
				{
					nbx = bx + i;
					nby = by;
				}
				else
				{
					nbx = bx;
					nby = by + i;
				}
			
				board->square[nbx][nby] = ship;
			}

		}
	}
}

// returns shot to take
static void TakeRandomShot(Board *fireBoard, Word *x, Word *y)
{
	Word bx,by,shot;

	// take a shot
	shot = SysRandom(TimGetSeconds()) % (board_Size*board_Size);
	by = shot / 10;
	bx = shot % 10;
	
	while (fireBoard->square[bx][by] != 0)
	{
		// start looking
		shot++;
		if (shot >= (board_Size * board_Size))
			shot = 0;
			
		by = shot / 10;
		bx = shot % 10;
	}
	
	*x = bx + 1;
	*y = by + 1;
	
	return;
}

static void UpdateHitInds(FormPtr frm, Boolean *stats)
{
	int i, fid;
	
	fid = FrmGetFormId(frm);
	
	for (i = 0; i < ship_Max; i++)
	{
		if (stats[i])
		{
			CtlSetValue(FrmGetObjectPtr(frm,FrmGetObjectIndex(frm,i+fid+50)),1);
			CtlDrawControl(FrmGetObjectPtr(frm,FrmGetObjectIndex(frm,i+fid+50)));
		}
		else
		{
			CtlSetValue(FrmGetObjectPtr(frm,FrmGetObjectIndex(frm,i+fid+50)),0);
			CtlDrawControl(FrmGetObjectPtr(frm,FrmGetObjectIndex(frm,i+fid+50)));
		}
	}
}

static void BSIrCallBack(IrConnect *conn, IrCallBackParms *parms)
{
	switch (parms->event)
	{
	case LEVENT_DISCOVERY_CNF: 
		StoreDiscovery(parms->deviceList); 
		pending_nextstep = nextstep_irlap;
//		EvtWakeup();
		break;
	case LEVENT_LAP_CON_CNF: 
		nextstep = nextstep_irlmp;
		controller = true;
		EvtWakeup();
		break;
	case LEVENT_LAP_CON_IND:
//		FrmAlert(GotIRAlert);
		controller = false;
		game.IRGame = true;
		if (!game.started)
		{
			CurrentView = IRStatusForm;
			nextstep = 0;
			FrmPopupForm(CurrentView);
			/*
			FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
			"IrLAP from Peer");
			*/
		}
		break;
	case LEVENT_LAP_DISCON_IND:
		break;
	case LEVENT_LM_CON_CNF:
		// got a connection
		MemMove(game.u.human.serialNo,parms->rxBuff,parms->rxLen);
		if (irStartup.action == action_new)
			nextstep = nextstep_new;
		else
			nextstep = nextstep_load;
		EvtWakeup();

		break;
	case LEVENT_LM_CON_IND:
	/*
		if (!game.started)
		{
		
			CurrentView = IRStatusForm;
			FrmPopupForm(CurrentView);*/
			FldSetTextPtr(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)),
			"Connecting...");
			FldDrawField(FrmGetObjectPtr(FrmGetActiveForm(),
			FrmGetObjectIndex(FrmGetActiveForm(),IRStatusStatusField)));
			MemMove(game.u.human.serialNo,parms->rxBuff,parms->rxLen);
			game.u.human.serialNo[parms->rxLen] = 0;
			packet.buff = serialNo;
			packet.len = serialNoLen;
			packet_in_use = true;
			last_packet_type = 0;
		//}
		break;
	case LEVENT_LM_DISCON_IND:
		break;
	case LEVENT_PACKET_HANDLED:
		packet_in_use = false;
		break;
	case LEVENT_DATA_IND:
		//SetData(parms->rxBuff,parms->rxLen); 
		if (parms->rxBuff[0] == packet_type_startup)
		{
			MemMove(&irStartup,parms->rxBuff,sizeof(irStartup));
			StrCopy(game.name,irStartup.name);
			switch (irStartup.action)
			{
			case action_new:
				NewGame();
				break;
			case action_load:
				LoadGameFromPeer();
				break;
			}
		}
		else if (parms->rxBuff[0] == packet_type_done)
		{
			opp_done = true;
			if (im_done && opp_done)
				game.started = true;
		}
		else if (parms->rxBuff[0] == packet_type_fire)
		{
			MemMove(&irFire,parms->rxBuff,sizeof(irFire));
			irReport.response = ReceiveShot(irFire.x,irFire.y);
			irReport.type = packet_type_report;
			packet.buff = (BytePtr)&irReport;
			packet.len = sizeof(irReport);
			packet_in_use = true;
			IrDataReq(irref,&connect,&packet);
		}
		else if (parms->rxBuff[0] == packet_type_report)
		{
			MemMove(&irReport,parms->rxBuff,sizeof(irReport));
			ShotStatus(irReport.response,irFire.x,irFire.y);
		}


		break;
	case LEVENT_STATUS_IND:
		switch (parms->status)
		{
		case IR_STATUS_NO_PROGRESS:
			break;
		case IR_STATUS_LINK_OK:
			break;
		case IR_STATUS_MEDIA_NOT_BUSY:
			nextstep = pending_nextstep;
			pending_nextstep = nextstep_none;
			EvtWakeup();
			break;
		}
		break;
	case LEVENT_TEST_CNF:
		switch (parms->status)
		{
		case IR_STATUS_SUCCESS:
			break;
		case IR_STATUS_FAILED:
			break;
		}
		break;
	case LEVENT_TEST_IND:
		break;
	}
	
	if (parms->event == LEVENT_LM_CON_IND)
		IrConnectRsp(irref,&connect,&packet,0);
}

/*
 * records information we found
 * during discovery
 */
void StoreDiscovery(IrDeviceList* deviceList)
{
	int i;

	for (i = 0; i < deviceList->nItems; i++)
	{
		dev = deviceList->dev[i].hDevice;
		connect.rLsap = deviceList->dev[i].xid[0];
	}
}

static void LoadGameFromPeer()
{
	UInt recs, i;
	VoidHand hand;
	Ptr ptr;
	Game g;

	recs = DmNumRecords(bsdb);
	
	for (i = 0; i < recs && sgames < 100; i++)
	{
		// get the record into 'g'
		hand = DmGetRecord(bsdb,i);
		ptr = MemHandleLock(hand);
		MemMove(&g,ptr,sizeof(g));
		MemHandleUnlock(hand);
		DmReleaseRecord(bsdb,i,false);
		
		if (g.IRGame == true)
		{
			if (StrCompare(game.u.human.serialNo,g.u.human.serialNo)==0)
			{
				if (StrCompare(game.name,g.name)==0)
				{
					MemMove(&game,&g,sizeof(g));
					FrmReturnToForm(StartupFormForm);
					CurrentView = PlacementForm;
					FrmGotoForm(CurrentView);
					return;
				}
			}
		}
	}
	
	FrmReturnToForm(StartupFormForm);
	CurrentView = PlacementForm;
	FrmGotoForm(CurrentView);
}
