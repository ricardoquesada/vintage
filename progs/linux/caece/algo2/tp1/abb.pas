{ Implementacion de ABB }

unit abb;

interface

uses crt;

type
	TipoDatoABB = record
		clave: string;
	end;

	TipoABB = ^Nodo;

	Nodo = record
		inf: TipoDatoABB;
		izq: TipoABB;
		der: TipoABB;
		hoja: boolean;
	end;

procedure CrearHoja(var A: TipoABB; x: TipoDatoABB; b: boolean);
procedure CrearABB(var L: TipoABB);
procedure InsertarHoja(var L: TipoABB; x: TipoDatoABB );
procedure InsertarLevel(var L: TipoABB; x: TipoDatoABB );
procedure BorrarHoja( L: TipoABB );
procedure MostrarArbol( L: TipoABB );

implementation

{ funciones auxiliares }
procedure preguntar( L: TipoABB; x: string; var s: char);
begin
	writeln(x,':',L^.inf.clave);
	writeln('[F]also o [V]erdadero');
	readln(s);
	s := UpCase(s);
end;

procedure MostrarArbolAux( L:TipoABB; x: integer; y: integer; inc: integer );
begin
        if(L <> Nil) then begin
                inc := inc div 2;
                gotoxy(x,y);
                writeln(L^.inf.clave);
                MostrarArbolAux( L^.izq, x+10,y-inc, inc );
                MostrarArbolAux( L^.der, x+10,y+inc, inc );
        end;
end;

procedure MostrarArbol(L: TipoABB);
begin
        clrscr;
        MostrarArbolAux( L, 1, 12, 12);
        gotoxy(1,24);
end;


procedure CrearHoja(var A: TipoABB; x: TipoDatoABB; b: boolean);
begin
	new(A);
	A^.inf := x;
	A^.der := Nil;
	A^.izq := Nil;
	A^.hoja := b;
end;

procedure CrearABB(var L: TipoABB);
begin
	L := Nil;
end;

{ Precondicion: No se puede insertar un nodo repetido.  }
procedure InsertarHoja(var L: TipoABB; x: TipoDatoABB );
var
	A: TipoABB;
	s:  char;
begin
	if( L = Nil ) then begin
		CrearHoja(A, x, TRUE);
		L := A;
	end else begin
		preguntar(L,x.clave,s);
		if (s='F') then
		begin
			InsertarHoja( L^.izq, x );
		end else begin
			InsertarHoja( L^.der, x );
		end;
	end;
end;
procedure InsertarLevel(var L: TipoABB; x: TipoDatoABB );
var
	A: TipoABB;
	s:  char;
begin
	if( L = Nil ) then begin
		CrearHoja(A, x, FALSE);
		L := A;
	end else begin
		InsertarLevel( L^.izq, x );
		InsertarLevel( L^.der, x );
	end;
end;

procedure BorrarHoja( L: TipoABB);
var
	A: TipoABB;
	r,s:  char;
begin
	if(L <> Nil) then begin
		if( L^.hoja = FALSE) then begin
			preguntar(L,'El nodo es ',s);
			if (s='F') then begin
				A := L^.izq;
			end else begin
				A := L^.der;
			end;
			if (A<>Nil) and (A^.hoja) then begin
				writeln('Desea borrar el nodo ',A^.inf.clave,' ? [s/n]');
				readln(r);
				r := UpCase(r);
				if(r='S') then begin
					dispose(A);
					if(s='F') then begin
						L^.izq := Nil;
					end else begin
						L^.der := Nil;
					end;
				end;
			end else begin
				BorrarHoja( A );
			end;
		end;
	end;
end;

end.
