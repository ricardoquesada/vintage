{ Algoritmos y Estrutura de datos II }

{ Trabajo Practico Numero 1 }


program main;


uses abb, crt;


procedure InsHoja( var L: TipoABB; s :string );
var
	x: TipoDatoABB;
begin
	x.clave := s;

	InsertarHoja( L, x );
end;

procedure InsLevel( var L: TipoABB; s :string );
var
	x: TipoDatoABB;
begin
	x.clave := s;

	InsertarLevel( L, x );
end;

procedure CrearArbolOriginal( var L:TipoABB );
begin
       InsLevel( L, 'Es Femenino' );
       InsLevel( L, 'Tiene Pelo Claro' );
       InsLevel( L, 'Tiene Talla Chica' );
end;

procedure InitABB( var L: TipoABB );
begin
	CrearABB( L );
end;


var
	A: TipoABB;
	C: TipoDatoABB;
	ch: char;
	s: string;

begin
	InitABB( A );

	repeat
        clrscr;
	writeln;
	writeln('-=ABB=-');
	writeln;
	writeln('1 - Crear el arbol modelo del T.P.');
        writeln('2 - Insertar un nuevo nivel en el arbol');
	writeln('3 - Insertar una hoja en el arbol');
	writeln('4 - Borrar una hoja en el arbol');
	writeln('5 - Mostrar el arbol');
	writeln;
	writeln('0 - Salir');

	readln(ch);
	case ch of
                '1':    CrearArbolOriginal( A );
		'2':	begin;
			writeln('Ingrese nombre del nivel a insertar:');
			readln(s);
			InsLevel( A, s);
			end;
		'3':	begin;
			writeln('Ingrese el nombre de la hoja a insertar:');
			readln(s);
			InsHoja( A, s);
			end;
		'4':
			BorrarHoja( A );
		'5':
			MostrarArbol( A );

	end;
        writeln('Aprete una tecla');
        readkey;
	until ch= '0';
end.
