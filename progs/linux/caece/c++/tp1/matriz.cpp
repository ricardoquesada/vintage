//
// matriz.h
// C++. Grupo Numero X
//

#include <iostream.h>
#include <string.h>
#include <stdlib.h>

#include "matriz.h"
#define INFINITO (-1)

void Mat::Mat_Cargar( int m[MAX_NODES][MAX_NODES], int src )
{
	int i,j;
	for(i=0;i<MAX_NODES;i++) {
		for(j=0;j<MAX_NODES;j++){ 
			mat[i][j]=m[i][j];
		}
	}
	mat_src = src;
}

Mat::Mat( void )
{
	mat_src = -1;

	memset( (void*) mat, 0, sizeof(mat));
}

Mat::~Mat( void )
{
	mat_src = -1;
}

// Setea los pesos por default
void Mat::Mat_PonerDefaults( void )
{
	int i;
	for(i=0;i<MAX_NODES;i++) {
		et[i].origen = -1;
		if( i == mat_src ) {
			et[i].peso = 0;
			et[i].modificado = TRUE;
		} else {
			et[i].peso = INFINITO;
			et[i].modificado = FALSE;
		}
	}
}

int Mat::Mat_HayModificados( int *id )
{
	int i;
	for(i=0;i<MAX_NODES;i++) {
		if( et[i].modificado ) {
			*id = i;
			return TRUE;
		}
	}
	return FALSE;
}
void Mat::Mat_CalcularPeso( int s, int d )
{
	// Si los 2 son distintos a  INFINITO
	if( mat[s][d] != INFINITO && et[s].peso != INFINITO) {

		// Si es menor pongo el nuevo valor
		if( (et[d].peso >=  et[s].peso + mat[s][d] ) 
			|| et[d].peso==INFINITO) {

			// Nuevo valor para destino
			et[d].peso = et[s].peso + mat[s][d];
			et[d].modificado = TRUE;
			et[d].origen = s;
		}
	}
	// else como alguno es infinito no tiene sentido
}

// Aplica el algorimo de Dijstra para resolver la matriz.
void Mat::Mat_Resolver( void )
{
	int s;			// source
	int d;			// destino

	Mat_PonerDefaults();

	while( Mat_HayModificados( &s ) ) {
		for(d=0;d<MAX_NODES;d++) {
			if(d==s) continue;	// opminizacion

			if( mat[s][d] > 0 ) {	// es adyacente ?
				Mat_CalcularPeso(s,d);
			}
		}
		et[s].modificado = FALSE;	// limpia el flag de modif
	}
}

void Mat::Mat_MostrarCamino( int d )
{
	// hubo camino ?
	if( et[d].origen == -1 )
		return;

	cout << "Estaciones:\n";

	while( d != mat_src ) {
		cout << d << " <- ";
		d = et[d].origen;
	}
	cout << mat_src << "\n";
}

int main( int argc, char **argv )
{
	class Mat M;
	int d = 3;
	int s = 0;

	if( argc > 2 ) {
		s = atoi( argv[1] );
		d = atoi( argv[2] );
	}

	cout << "\nOrigen: " << s << "  Destino: " << d << "\n";

	// matriz de adyacencia y de pesos
	int mat[MAX_NODES][MAX_NODES] = {
		//0  1  2  3  4  5  6  7  8  9
		{ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 }, // 0
		{ 0, 0, 0, 1, 0, 0, 0, 0, 0, 3 }, // 1 
		{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, // 2 
		{ 0, 1, 0, 0, 0, 0, 1, 0, 0, 0 }, // 3 
		{ 1, 0, 0, 0, 0, 2, 0, 0, 0, 0 }, // 4
		{ 0, 0, 1, 0, 2, 0, 1, 1, 0, 0 }, // 5
		{ 0, 0, 0, 1, 0, 1, 0, 0, 2, 0 }, // 6
		{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, // 7
		{ 0, 0, 0, 0, 0, 0, 2, 0, 0, 1 }, // 8
		{ 0, 3, 0, 0, 0, 0, 0, 0, 1, 0 }, // 9
	};

	M.Mat_Cargar( mat, s );
	M.Mat_Resolver();
	M.Mat_MostrarCamino( d );
}
