//
// matriz.h
// C++. Grupo Numero X
//

#ifndef __MATRIZ_H
#define __MATRIZ_H

#define MAX_NODES	(10)

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE 
#define FALSE 0
#endif

struct _etiqueta {
	int peso;
	int origen;
	int modificado;
};

class Mat {

public:
	// miembros
	void Mat_Cargar( int m[MAX_NODES][MAX_NODES], int src );
	void Mat_Resolver( void );
	void Mat_MostrarCamino( int dst );
	Mat(void);
	~Mat( void );

private:
	// data
	struct _etiqueta et[MAX_NODES];
	int mat[MAX_NODES][MAX_NODES];
	int mat_src;

	// miembros
	void Mat_PonerDefaults( void );
	int Mat_HayModificados( int *m);
	void Mat_CalcularPeso( int s, int d );
};

#endif /* __MATRIZ_H */
