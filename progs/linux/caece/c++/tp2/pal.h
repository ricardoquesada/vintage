//
// clase palabra

#ifndef __PALABRA_H
#define __PALABRA_H

#define FROM_KEY_2_PAL( pT ) ( ((char*)(pT)) - (( ((char*)&((palabra*)pT)->next_key) - ((char*)pT) )))
class palabra {
public:
	LIST_ENTRY next_pal;
	LIST_ENTRY next_key;
	char nombre[50];
	char key[50];

	palabra( char * pal);
	palabra& operator=( const palabra& p );
	int operator<( const palabra& p );
	int operator>( const palabra& p );
	int operator==( const palabra& p );
	int keycmp( const palabra&p );
	void generar_key(void);
	void mostrar_key(void);
	void mostrar(void);
};


#endif // __PALABRA_H
