//
// clase diccionario
//

#include <iostream.h>
#include <stdio.h>
#include <ctype.h>
#include "common.h"
#include "dic.h"
#include "pal.h"


diccionario::diccionario( char* file )
{
	int i;
	for(i=0;i<HASH_SIZE;i++) {
		InitializeListHead( &lista_pal[i] );
		InitializeListHead( &lista_key[i] );
	}
	fp = NULL;
	fp_local = NULL;
	fp_out = NULL;
	pal_corregidas=0;
	pal_ignoradas=0;
	pal_bien=0;
	pal_agregadas=0;
}

diccionario::~diccionario()
{
	int i;
	palabra *w;

	for(i=0;i<HASH_SIZE;i++) {
		while( !IsListEmpty( &lista_pal[i] ) ) {
			w = (palabra *) RemoveHeadList( &lista_pal[i] );
			delete w;
		}
	}
	if( fp )
		fclose( fp );
	if ( fp_local )
		fclose( fp_local);
	if ( fp_out )
		fclose( fp_out);
	
	cout << "Palabras: " << endl;
	cout << "\tcorregidas: " << pal_corregidas << endl;
	cout << "\tignoradas: " << pal_ignoradas << endl;
	cout << "\tbien escritas: " << pal_bien << endl;
	cout << "\n\tTotal de palabras: " << pal_bien+pal_corregidas+pal_ignoradas << endl;
	cout << "\n\tAgregadas al diccionario local: " << pal_agregadas << endl;
}

int diccionario::hash_get( char *name )
{
	char a = name[0];
	a |= 0x60;
	if( a <= 0x60 ) return 0;
	if( a >= 0x60+HASH_SIZE ) return (HASH_SIZE -1);
	return  (a-0x60);
}

int diccionario::cargar( char *file )
{
	char pal[200];
	int i;

	fp = fopen(file,"r");
	if( fp == NULL  ) {
		cout << "Error al abrir el archivo " << file << endl;
		return -1;
	}
	#define MAX_PAL (20000)

	for(i=0;i<MAX_PAL;i++) {
		if( feof(fp) )
			break;
		fscanf(fp,"%s",pal);
		agregar(pal);
		if( i % (MAX_PAL/10) == 0 )
			cout << "Cargando principal " << i << "/" << MAX_PAL << endl;
	}
	return 0;
}

int diccionario::cargar_local( char *file )
{
	char pal[200];

	fp_local = fopen(file,"a+");
	if( fp_local == NULL  ) {
		cout << "Error al abrir el archivo " << file << endl;
		return -1;
	}
	
	fseek(fp_local, 0L, SEEK_SET);
	cout << "Cargando local...";
	while( !feof(fp_local) ) {
		fscanf(fp_local,"%s",pal);
		agregar(pal);
	}
	cout << " OK" << endl;
	return 0;
}

void diccionario::insorden_pal( palabra *p )
{
	int lista = hash_get(p->nombre);
	PLIST_ENTRY l = lista_pal[lista].Flink;

	if( IsListEmpty( &lista_pal[lista] )) {
		InsertTailList( &lista_pal[lista], (PLIST_ENTRY) p );
		return;
	}

	while( l != &lista_pal[lista]) {
		palabra *w = (palabra *) l;
		if( *p < *w ) {
			InsertHeadList( l->Blink, (PLIST_ENTRY) p );
			return;
		}
		l = LIST_NEXT( l );
	}
	InsertTailList( &lista_pal[lista], (PLIST_ENTRY) p );
}

void diccionario::insorden_key( palabra *p )
{
	int lista = hash_get(p->key);
	PLIST_ENTRY l = lista_key[lista].Flink;

	if( IsListEmpty( &lista_key[lista] )) {
		InsertTailList( &lista_key[lista], (PLIST_ENTRY) &p->next_key );
		return;
	}

	while( l != &lista_key[lista]) {
		palabra *w = (palabra *) l;
		w = (palabra *) FROM_KEY_2_PAL( w );
		if( p->keycmp( *w ) < 0) {
			InsertHeadList( l->Blink, (PLIST_ENTRY) &p->next_key );
			return;
		}
		l = LIST_NEXT( l );
	}
	InsertTailList( &lista_key[lista], (PLIST_ENTRY) &p->next_key );
}


int diccionario::agregar( char *pal )
{
	palabra *p;

	p = new palabra(pal);

	p->generar_key();

	insorden_pal( p );
	insorden_key( p );

	return 1;
}

int diccionario::quiere_agregar( char *pal)
{
	char c;
	cout << "Desea agregar la palabra '" << pal << "' al diccionario local (S/N)" << endl;
	cin >> c;
	if( tolower(c) == 's' ) {
		agregar(pal);
		if( fp_local ) {
			fprintf(fp_local,"%s\n",pal);
		}
		pal_agregadas++;
	}
	return 0;
}

int diccionario::esta_pal( char *pal )
{
	char c[200];
	palabra *w;
	unsigned int i;
	int lista;

	for(i=0;i<strlen(pal);i++)
		c[i] = tolower(pal[i]);
	c[i] = 0;
	lista = hash_get(c);

	palabra p(pal);

	p.generar_key();

	PLIST_ENTRY l = lista_pal[lista].Flink;
	while( !IsListEmpty( &lista_pal[lista] ) && (l != &lista_pal[lista]) ) {
		w = (palabra *) l;
		if( *w < p )
			l = LIST_NEXT(l);
		else if( p == *w )
			return 1;
		else
			return 0;
	}
	return 0;
}

int diccionario::mostrar()
{
	int i=0;

	for(i=0;i< HASH_SIZE;i++) {
		PLIST_ENTRY l = lista_pal[i].Flink;
		palabra *p;

		while( !IsListEmpty( &lista_pal[i] ) && (l != &lista_pal[i]) ) {
			p = (palabra *) l;
			p->mostrar();
			l = LIST_NEXT(l);
		}
	}
	return 0;
}

int diccionario::mostrar_keys()
{
	int i=0;

	for(i=0;i<HASH_SIZE;i++) {
		PLIST_ENTRY l = lista_key[i].Flink;
		palabra *p;


		while( !IsListEmpty( &lista_key[i] ) && (l != &lista_key[i]) ) {
			p = (palabra *) l;
			p = (palabra *) FROM_KEY_2_PAL( p );
			p->mostrar_key();
			l = LIST_NEXT(l);
		}
	}
	return 0;
}

int diccionario::sugerir( char *c )
{
	int lista;
	PLIST_ENTRY l;
	palabra *p;

	palabra pal(c);
	pal.generar_key();
	int n,i=1;

	lista = hash_get( pal.key );
	l = lista_key[lista].Flink;

	cout << "Sugerencias para '" << c << "': " ;

	while( !IsListEmpty( &lista_key[lista] ) && (l != &lista_key[lista]) ) {
		p = (palabra *) l;
		p = (palabra *) FROM_KEY_2_PAL( p );
		int r = p->keycmp( pal );

		if( r == 0) {
			cout << i++ << ") " ;
			p->mostrar();
			cout << ", ";
		} else if ( r > 0)
			break;

		l = LIST_NEXT(l);
	}
	if( i==0)
		cout << " (No hay sugerencias disponibles)";
	cout << endl;

	do {
		cout << "Ingrese numero de sugerencia o 0 (cero) para ignorar" << endl;
		cin >> n;
	} while ( n<0 || n>=i );

	if( n==0 ) {
		pal_ignoradas++;
		fprintf(fp_out,"%s ",c);
		return 0;
	}

	i=0;
	l = lista_key[lista].Flink;
	while( !IsListEmpty( &lista_key[lista] ) && (l != &lista_key[lista]) ) {
		p = (palabra *) l;
		p = (palabra *) FROM_KEY_2_PAL( p );
		int r = p->keycmp( pal );

		if( r == 0 ) {
			i++;
			if( i == n ) {
				fprintf(fp_out,"%s ",p->nombre);
				pal_corregidas++;
				break;
			}
		} else if ( r > 0)
			break;

		l = LIST_NEXT(l);
	}
	return 1;
}

int diccionario::leer( char *file )
{
	char pal[200];

	fp = fopen(file,"r");
	if( fp == NULL  ) {
		cout << "Error al abrir el archivo " << file << endl;
		return -1;
	}

	while( !feof(fp) ) {
		fscanf(fp,"%s",pal);
		if( !esta_pal(pal) ) {
			if (!sugerir(pal))
				quiere_agregar(pal);
		} else {
			fprintf(fp_out,"%s ",pal);
			pal_bien++;
		}
	}
	return 0;
}

int diccionario::crear_salida( char *file )
{
	fp_out = fopen(file,"w");
	if( fp_out == NULL  ) {
		cout << "Error al abrir el archivo " << file << endl;
		return -1;
	}
	return 0;
}
