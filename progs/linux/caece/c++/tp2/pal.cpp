//
// clase palabra
//

#include <iostream.h>
#include <string.h>
#include <ctype.h>
#include "common.h"
#include "pal.h"

palabra::palabra( char *n) 
{
	strncpy( nombre, n, sizeof(nombre) );
	return;
}

palabra& palabra::operator=( const palabra& p )
{
	strcpy( nombre, p.nombre );
	strcpy( key, p.key );
	return *this;
}

int palabra::keycmp( const palabra& p )
{
	return( strcmp( key, p.key ));
}

int palabra::operator<( const palabra& p )
{
	return( strcmp( nombre, p.nombre ) < 0);
}

int palabra::operator>( const palabra& p )
{
	return( strcmp( nombre, p.nombre ) > 0);
}

int palabra::operator==( const palabra& p )
{
	return( strcmp( nombre, p.nombre ) == 0);
}

void palabra::mostrar(void)
{
	cout << nombre;
}

void palabra::mostrar_key(void)
{
	cout << key;
}

void palabra::generar_key(void)
{
	char bien[]="qzvm������";
	char  mal[]="ksbnaeiouu";
	char nombre_tmp[sizeof(key)];

	unsigned int i,j;
	char last;

	last = '\0';

	// convierte todo a minuscula
	//	for(i=0;i<strlen(nombre);i++)
	//		nombre[i] = tolower(nombre[i]);
	

	// convierte 'll' por 'y' y 'cc' por 'x'
	last = 0;
	j=0;
	for(i=0;i<strlen(nombre);i++) {
		if( nombre[i] == last && last == 'c' ) {
			nombre_tmp[j-1] = 'x';
			continue;
		}
		if( nombre[i] == last && last == 'l' ) {
			nombre_tmp[j-1] = 'y';
			continue;
		}
		if( nombre[i] == 'u' && last == 'q' ) {
			nombre_tmp[j-1] = 'k';
			continue;
		}
		if( i >=2 && i>=2 && nombre[i-2] == 'n' && nombre[i-1]=='i' && 
			(nombre[i]=='a' ||
			nombre[i]=='e' ||
			nombre[i]=='i' ||
			nombre[i]=='o' ||
			nombre[i]=='u' ))
		{
			nombre_tmp[j-2] = '�';
			nombre_tmp[j-1] = nombre[i];
			continue;
		}

		if( last == 'c' && ( nombre[i] =='a' || nombre[i] =='o' || nombre[i]=='u' ))
			nombre_tmp[j-1] = 'k';

		if( last == 'c' && ( nombre[i]=='i' || nombre[i]=='e' ))
			nombre_tmp[j-1] = 's';

		last = nombre_tmp[j] = nombre[i];
		j++;
	}
	nombre_tmp[j]=0;


	// elimina caracteres repetidos y caracteres silenciosos
	j=0;
	for(i=0;i<strlen(nombre_tmp);i++) {
		if( nombre_tmp[i] != last && nombre_tmp[i]!='h')
			nombre_tmp[j++] = nombre_tmp[i];
		last = nombre_tmp[i];
	}
	nombre_tmp[j]=0;

		
	// convierte por tabla
	for(i=0;i<strlen(nombre_tmp);i++) {
		for(j=0;j<sizeof(bien);j++) {
			if( bien[j] == nombre_tmp[i] )
				break;
		}
		if( j < sizeof(bien) )
			key[i] = mal[j];
		else
			key[i] = nombre_tmp[i];
	}
	key[i]=0;
}
