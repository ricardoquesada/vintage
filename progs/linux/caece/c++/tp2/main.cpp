#include <iostream.h>
#include "common.h"
#include "dic.h"
#include "pal.h"

int main (int argc, char **argv )
{
	int c = argc;
	char **v = argv;

	if( c <= 2 ) {
		cout << "Modo de uso: tp2 entrada.txt salida.txt [local_dic.txt]" << endl;
		return 0 ;
	}

	diccionario dic("spanish");
	dic.cargar("mini3.dic");

	if( c >3 )
		dic.cargar_local(v[3]);

	dic.crear_salida( v[2] );
	dic.leer( v[1] );
	return 0;
}
