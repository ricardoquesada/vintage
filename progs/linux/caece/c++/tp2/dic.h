//
// clase diccionario
//

#ifndef __DICCIONARIO_H
#define __DICCIONARIO_H

#include <stdio.h>
#include "pal.h"

#define HASH_SIZE 26

class diccionario {
private:
	LIST_ENTRY lista_pal[HASH_SIZE];
	LIST_ENTRY lista_key[HASH_SIZE];
	FILE *fp;	// texto de entrada
	FILE *fp_out;	// texto de salida
	FILE *fp_local;	// diccionario local

	int pal_corregidas;	// palabras corregidas
	int pal_ignoradas;	// palabras ignoradas
	int pal_bien;		// palabras bien escritas
	int pal_agregadas;	// palbras agregadas al dicc local

public:
	diccionario(char *file);
	~diccionario();
	int cargar( char *file );
	int cargar_local( char *file );
	int agregar( char *pal );
	int mostrar();
	int mostrar_keys();
	void insorden_pal( palabra *p );
	void insorden_key( palabra *p );
	int sugerir( char *p );
	int hash_get(char *a);
	int leer( char *filename);
	int esta_pal( char *p);
	int quiere_agregar( char *pal);
	int crear_salida( char *file );
};

#endif // __DICCIONARIO_H
