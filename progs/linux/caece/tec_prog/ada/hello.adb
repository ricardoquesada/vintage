With Gnat.IO; use Gnat.IO;

procedure Hello is
	N: Integer;
begin

	Get (N);

	case N is
 
		when 1 =>
			Put(1);
			New_Line (1);
		when 2 =>
			Put(2);
			New_Line (1);
		when 3 =>
			Put(3);
			New_Line (1);
		when 4 =>
			Put(4);
			New_Line (1);
		when others =>
			Put(8);
			New_Line (1);

	end case; -- State
          
end; 
