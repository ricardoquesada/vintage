With Gnat.IO; use Gnat.IO;

procedure Hello is
	N: Integer :=0;
	I: Integer :=0;
	X: Integer := 12;
begin
	N := X;

	for X in 1..N loop
		I := I + X;
		N := N + 1;
	end loop;

   	New_Line (1);
	Put(I);
   	New_Line (1);
	Put(N);
   	New_Line (1);
	Put(X);
end; 
