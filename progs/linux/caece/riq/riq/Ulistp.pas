{Implementaci�n de Listas con Punteros.}
{Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a}

unit Ulistp;

interface

{uses ;}

type
	TipoDatoLis = integer;

        Nodeptr = ^Nodo;
	Nodo = record
		inf: TipoDatoLis;
		sig: Nodeptr;
	end;
	TipoLista = Nodeptr;

function ListaVacia(L: TipoLista): boolean;
function ListaLlena(L: TipoLista): boolean;
function FinLista(L: TipoLista): boolean;
procedure CrearNodo(var A: TipoLista; x: TipoDatoLis);
procedure CrearLista(var L: TipoLista);
procedure DestruirLista(var L: TipoLista);
procedure Primero(var L: TipoLista);
procedure Siguiente(var L: TipoLista);
procedure Ubicar(var L: TipoLista; x: TipoDatoLis; var b: boolean);
procedure Info(L: TipoLista; var x: TipoDatoLis);
procedure Borrar(var L: TipoLista; n: integer);
procedure Insertar(var L: TipoLista; x: TipoDatoLis; n: integer);
procedure InsertarCabeza(var L: TipoLista; x: TipoDatoLis);
procedure InsertarFin(var L: TipoLista; x: TipoDatoLis);
procedure InsertarOrden(var L: TipoLista; x: TipoDatoLis; ord: char);
procedure Modificar(var L: TipoLista; x: TipoDatoLis);

implementation

procedure CrearNodo(var A: TipoLista; x: TipoDatoLis);
begin
	new(A);
	A ^.inf := x;
	A ^.sig := Nil;
end;

procedure CrearLista(var L: TipoLista);
begin
	L := Nil;
end;

procedure DestruirLista(var L: TipoLista);
var
	A: TipoLista;
begin
	while L <> Nil do begin
		A := L;
		L := L^.Sig;
		dispose(A);
	end;
end;

function ListaVacia(L: TipoLista): boolean;
begin
	ListaVacia := L = Nil;
end;

function ListaLlena(L: TipoLista): boolean;
begin
	ListaLlena := False;
end;

function FinLista(L: TipoLista): boolean;
begin
	FinLista := L = Nil;
end;

procedure Primero(var L: TipoLista);
{Precondici�n: }{La lista L no puede estar vac�a, es decir, L no puede ser igual a Nil.}
begin

end;

procedure Siguiente(var L: TipoLista);
{Precondici�n: }{Para llamar al procedimiento Siguiente es necesario haber llamado
previamente al procedimiento Primero.}
begin
	L := L ^.sig;
end;

procedure Ubicar(var L: TipoLista; x: TipoDatoLis; var b: boolean);
{Comentario: }{La b�squeda comienza siempre desde la posici�n actual de la lista L,
convirtiendo en actual la primer ocurrencia del elemento buscado, y asignando
verdadero a la variable booleana b. Si necesitamos volver a llamar al procedimiento
para ubicar otras ocurrencias del elemento buscado, entonces ser� necesario llamar
previamente a la primitiva Siguiente. El programa que hace la llamada es responsable
de resguardar el valor de la variable L, ya que de lo contrario perder�amos el puntero a
la cabeza de la lista.}
begin
	while (L <> Nil) and (L ^.inf <> x) do
              L := L ^.sig;
	b := L <> Nil;
end;

procedure Info(L: TipoLista; var x: TipoDatoLis);
{Precondici�n: }{L no puede ser igual a Nil.}
begin
	x := L ^.inf;
end;

procedure Borrar(var L: TipoLista; n: integer);
{Precondici�n: }{El entero n debe ser mayor o igual que 1 y menor o igual que la 
cantidad de elementos de la lista.}
var
	i: integer;
	A, p: TipoLista;
begin
	if n = 1 then begin
		A := L;
		L := L ^.sig;
		dispose(A);
	end
	else begin
		i := 2; p := L;
		while i < n do begin
			i := i + 1; p := p ^.sig;
		end;
		A := p ^.sig;
		p ^.sig := p ^.sig ^.sig;
		dispose(A);
	end;
end;

procedure Insertar(var L: TipoLista; x: TipoDatoLis; n: integer);
{Precondici�n: }{El entero n debe ser mayor o igual que 1 y menor o igual que la cantidad de elementos de la lista.}
var
	i: integer;
	A, p: TipoLista;
begin
	CrearNodo(A, x);
	if n = 1 then begin
		A ^.sig := L;
		L := A;
	end
	else begin
		i := 2; p := L;
		while i < n do begin
			i := i + 1; p := p ^.sig;
		end;
		A ^.sig := p ^.sig;
		p ^.sig := A;
	end;
end;

procedure InsertarCabeza(var L: TipoLista; x: TipoDatoLis);
var
	A: TipoLista;
begin
	CrearNodo(A, x);
	A ^.sig := L;
	L := A;
end;

procedure InsertarFin(var L: TipoLista; x: TipoDatoLis);
var
	A, p: TipoLista;
begin
	CrearNodo(A, x);
	if L = Nil then L := A
	else begin
		p := L;
		while p ^.sig <> Nil do p := p ^.sig;
		p ^.sig := A;
	end;
end;

procedure InsertarOrden(var L: TipoLista; x: TipoDatoLis; ord: char);
{Precondici�n: }{La lista L se encuentra ordenada y el orden ascendente o
descendente es indicado por el par�metro 'ord'.}
var
	A, p: TipoLista;
begin
	CrearNodo(A, x);
	if L = Nil then L := A
	else if ord = 'a' then begin
		if x < L ^.inf then begin
			A ^.sig := L;
			L := A;
		end
		else begin
			p := L;
			while (p ^.sig <> Nil) and (x > p ^.sig ^.inf) do p := p ^.sig;
			A ^.sig := p ^.sig;
			p ^.sig := A;
		end;
	end
	else if ord = 'd' then begin
		if x > L ^.inf then begin
			A ^.sig := L;
			L := A;
		end
		else begin
			p := L;
			while (p ^.sig <> Nil) and (x < p ^.sig ^.inf) do p := p ^.sig;
			A ^.sig := p ^.sig;
			p ^.sig := A;
		end;
	end;
end;

procedure Modificar(var L: TipoLista; x: TipoDatoLis);
{Precondici�n: }{L no puede ser igual a Nil.}
begin
	L ^.inf := x;
end;

end.

