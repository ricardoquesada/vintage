/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <stdlib.h>
#include <assert.h>

#include "estad.h"
#include "tipos.h"

LIST_ENTRY g_estad_list;

int estad_vacia()
{
	return IsListEmpty( &g_estad_list );
}

STATUS estad_init()
{
	InitializeListHead( &g_estad_list );
	return STATUS_SUCCESS;
}

STATUS estad_insert( PPCB data )
{
	InsertTailList( &g_estad_list, (PLIST_ENTRY) data );

	return STATUS_SUCCESS;
}

STATUS estad_flush()
{
	PLIST_ENTRY tmp;

	while( !IsListEmpty( &g_estad_list ) ) {
		tmp = RemoveHeadList( &g_estad_list );
		free( tmp );
	}

	return STATUS_SUCCESS;
}
