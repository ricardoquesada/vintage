/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_METRIC_H
#define __TP1_METRIC_H

#include "tipos.h"

typedef struct _metric {
	LIST_ENTRY next;
	char nombre[100];	/* nombre de la metrica */
	int id;			/* id de la metrica */

	/*
	 * FIXME: Deberian ser floats estos ?
	 */
	int min;		/* minimo */
	int max;		/* max */
	int cur;		/* current */
	int prom;		/* promedio */

	float scale_y;		/* escala de y (x es el tiempo) */
	int attrib;		/* attributo (color, o algo asi ) */
	STATUS (*update_fn)( struct _metric* ); /* funcions que actualiza los datos */
} METRIC, *PMETRIC;

int metric_vacia();
STATUS metric_init();
STATUS metric_register( PMETRIC data );
STATUS metric_flush(void);
STATUS metric_display_all( void );
STATUS metric_display( int id );
STATUS metric_setup( PMETRIC pM );

#endif /* __TP1_METRIC_H */
