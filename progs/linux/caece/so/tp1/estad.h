/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_ESTAD_H
#define __TP1_ESTAD_H

#include "tipos.h"

extern LIST_ENTRY g_estad_list;

int estad_vacia();
STATUS estad_init();
STATUS estad_insert( PPCB data );
STATUS estad_flush();

#endif /* __TP1_ESTAD_H */
