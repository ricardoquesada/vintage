#include <curses.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cola.h"
#include "proc.h"
#include "cons.h"
#include "tipos.h"
#include "estad.h"
#include "metric.h"
#include "archivo.h"
#include "ncola_comms.h"
#include "ncola_graph.h"
#include "ncola_common.h"

/**
 * Info sobre el programa, el grupo (autores) y cualquier otra yerba \b ACA.
 */

#define NCOLA_FILAS	20			/**< El ancho del pGPad (ver TNCola). */
#define NCOLA_COLUMNAS	60	/**< El alto del pGPad (ver TNCola). */
#define NCOLA_CONSOLE_BUFFER 80 /**< Tama�o del buffer de entrada para la
	consola.*/

static void ncola_init( TNCola *pTncola );
static void ncola_end( TNCola *pTncola );
static void ncola_init_screen( TNCola *pTncola );
static void ncola_comms_init( TNCola *pTncola );

/**
 * Inicia las rutinas del programa.
 * Actualmente esto es:
 *	- Incializar el ncurses.
 *	- Inicializar la estructura de comandos y agregar los mismos.
 *	- Inicializar la cola de procesos.
 *	- Inicializar la lista de estados.
 *	- Inicializar la lista de m�tricas.
 *	- Dibujar la pantalla (inicializando las estructuras internas).
 *
 * El \c ncurses est� siendo utilizado para la representaci�n de los datos de la
 * cola en pantalla, los gr�ficos y las m�tricas; tambi�n se lo utiliza para
 * el manejo del teclado.
 * @param pTncola La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
static void ncola_init( TNCola *pTncola )
{
	assert( pTncola != NULL );

	initscr();
	start_color();

	init_pair( NCOLA_GREEN, COLOR_GREEN, COLOR_BLACK );
	init_pair( NCOLA_RED, COLOR_RED, COLOR_BLACK );
	init_pair( NCOLA_CYAN, COLOR_CYAN, COLOR_BLACK );
	init_pair( NCOLA_WHITE, COLOR_WHITE, COLOR_BLACK );

	cbreak();
	noecho();

	pTncola->iClock = 0;
	pTncola->iSpeed = 1000;

	ncola_init_screen( pTncola );
	ncola_comms_init( pTncola );

	cola_init();
	estad_init();
	metric_init();

	pTncola->pTMetric = metric_get( 0 );	/* Metrica 'default' */
	pTncola->ipGraph = ncola_graph_metric;
}



/**
 * Inicia la consola.
 * Incicia las estructuras de datos pertinentes y agrega los comandos
 * definidos.
 * @param pTncola La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
static void ncola_comms_init( TNCola *pTncola )
{
	assert( pTncola != NULL );

	pTncola->pTCommands = cons_new();
	assert( pTncola->pTCommands != NULL );

	ncola_comms_add_help( pTncola, pTncola->pGConsole );

	cons_command_add( &pTncola->pTCommands, "clear", "Limpia la pantalla",
		ncola_comms_clear, pTncola->pGPad );

	cons_command_add( &pTncola->pTCommands, "reset", 
		"Pone en cero al clock interno.", ncola_comms_clock_reset, pTncola );

	cons_command_add( &pTncola->pTCommands, "speed",
		"speed [velocidad] - Modifica la velocidad del clock, si no se pasan \
par�metros muestra la velocidad actual", ncola_comms_clock_set_speed, 
		pTncola );

	cons_command_add( &pTncola->pTCommands, "ps",
		"Muestra el estado actual de todos los procesos", ncola_comms_ps, pTncola );

	cons_command_add( &pTncola->pTCommands, "list",
		"Muestra la lista de comandos disponibles", ncola_comms_list, pTncola );

	cons_command_add( &pTncola->pTCommands, "graph",
		"Cambia la m�trica que se desea ver", ncola_comms_graph, pTncola );



}

/**
 * Termina el programa.
 * Actualmente esto es:
 *	- Eliminar la estructura de comandos.
 *	- Terminar el ncurses.
 *	- Eliminar las estructuras creadas y liberar la memoria.
 */
static void ncola_end( TNCola *pTncola )
{
	assert( pTncola != NULL );

	nocbreak();
	cons_destroy( pTncola->pTCommands );

	archivo_close();
	metric_flush();
	estad_flush();

	delwin( pTncola->pGPad );
	delwin( pTncola->pGMetrics );
	delwin( pTncola->pGConsole );
	endwin();
}

/**
 * Dibuja la pantalla inicial.
 * Crea las estructuras necesarias, como ser las diversas ventanas.
 */
static void ncola_init_screen( TNCola *pTncola )
{
	assert( pTncola != NULL );

	/* Algunas l�neas para mejorar la apariencia */
	mvhline( 0, 1, ACS_HLINE, COLS - 2 );
	mvhline( LINES - 1, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, 0, ACS_VLINE, LINES - 2 );
	mvvline( 1, COLS - 1, ACS_VLINE, LINES - 2 );

	mvhline( NCOLA_FILAS, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, NCOLA_COLUMNAS - 1, ACS_VLINE, NCOLA_FILAS - 1 );

	/* Ahora las esquinas e intersecciones */
	mvaddch( 0, 0, ACS_ULCORNER );
	mvaddch( 0, COLS - 1, ACS_URCORNER );
	mvaddch( LINES - 1, 0, ACS_LLCORNER );
	mvaddch( LINES - 1, COLS - 1, ACS_LRCORNER );

	mvaddch( NCOLA_FILAS, NCOLA_COLUMNAS - 1, ACS_BTEE );
	mvaddch( 0, NCOLA_COLUMNAS - 1, ACS_TTEE );
	mvaddch( NCOLA_FILAS, 0, ACS_LTEE );
	mvaddch( NCOLA_FILAS, COLS - 1, ACS_RTEE );
	refresh();

	/* Graficos */
	pTncola->pGPad = newwin( NCOLA_FILAS - 1, NCOLA_COLUMNAS - 2, 1, 1 );
	scrollok( pTncola->pGPad, TRUE );
	wnoutrefresh( pTncola->pGPad );

	/* Metricas */
	pTncola->pGMetrics = newwin( NCOLA_FILAS - 1, COLS -
		NCOLA_COLUMNAS - 2, 1, NCOLA_COLUMNAS );
	scrollok( pTncola->pGMetrics, TRUE );
	wnoutrefresh( pTncola->pGMetrics );

	/* Consola */
	pTncola->pGConsole = newwin( LINES - NCOLA_FILAS - 2, COLS - 2,
		NCOLA_FILAS + 1, 1 );
	scrollok( pTncola->pGConsole, TRUE );
	keypad( pTncola->pGConsole, TRUE );
	nodelay( pTncola->pGConsole, TRUE );
	wnoutrefresh( pTncola->pGConsole );

	doupdate();
}

/**
 * Muestra el estado actual de los procesos.
 * \author Picorelli Marcelo.
 */
static void ncola_status_refresh( TNCola *pTncola )
{
	PLIST_ENTRY pPcbTmp;
	PLIST_ENTRY pPcbFirst;

	int ivEstado[5];

	assert( pTncola != NULL );

	mvwprintw( pTncola->pGMetrics, 0, 0, "Clock: %d", pTncola->iClock );
	wclrtoeol( pTncola->pGMetrics );

	/* Calculo diversos datos de los procesos, lo cual *NO* debe estar aca.*/
	pPcbFirst = &(g_estad_list);
  pPcbTmp = LIST_NEXT( pPcbFirst ); 

	memset( &ivEstado, 0, sizeof( int ) * 5 );
	while( pPcbTmp != pPcbFirst  )
	{
		ivEstado[ ((PPCB)(pPcbTmp))->estado]++;
		pPcbTmp = LIST_NEXT( pPcbTmp );
	}

	mvwprintw( pTncola->pGMetrics, 3, 0, "Encolados  : %d", 
		ivEstado[ PROC_ENCOLADO ] );
	wclrtoeol( pTncola->pGMetrics );

	mvwprintw( pTncola->pGMetrics, 4, 0, "Ejecutando : %d", 
		ivEstado[ PROC_CORRIENDO ] );
	wclrtoeol( pTncola->pGMetrics );

	mvwprintw( pTncola->pGMetrics, 5, 0, "Terminados : %d", 
		ivEstado[ PROC_TERMINADO ] );
	wclrtoeol( pTncola->pGMetrics );

	wrefresh( pTncola->pGMetrics );
	
}

static int ncola_console( TNCola *pTncola )
{
	char cpBuffer[ NCOLA_CONSOLE_BUFFER ];
	char tokens[2][80];
	char *cpTemp;

	assert( pTncola != NULL );

	echo();
	nodelay( pTncola->pGConsole, FALSE );
	waddch( pTncola->pGConsole, '$' );
	wgetnstr( pTncola->pGConsole, cpBuffer, NCOLA_CONSOLE_BUFFER );
	if( strlen( cpBuffer ) )
	{
		/* Leo la linea de comando, por ahora a lo sumo 2 palabras.*/
		if( ( cpTemp = strtok( cpBuffer, " " ) ) )
		{
			strcpy( tokens[0], cpTemp );
			if( ( cpTemp = strtok( NULL, " " ) ) )
				strcpy( tokens[1], cpTemp );
			else
				tokens[1][0] = 0;

			cons_command_execute( pTncola->pTCommands, tokens[0],
				tokens[1] );
		}
	}

	noecho();
	nodelay( pTncola->pGConsole, TRUE );

	return 1;
}

/**
 * Ciclo principal.
 * Se encarga de llamar a la consola cuando sea apropiado, y de incrementar
 * los \e ticks del reloj interno.
 */
static int main_loop( TNCola *Tncola )
{
	int iKey;
	int iContinue = 1;
	int iTickTock = 0;

	PCB TPcb;

	while( iContinue )
	{
		/* Tenemos que entrar en modo consola? */
		if( ( ( iKey = wgetch( Tncola->pGConsole )  ) != ERR ) &&
			  ( iKey == KEY_IC ) )	
		{
				ncola_console( Tncola );
		}

		/* Leemos el proximo evento y lo colocamos en la cola.*/
		if( archivo_read( &TPcb, Tncola ) == STATUS_SUCCESS )
		{
			PPCB pPcb;

			if( !(pPcb = malloc( sizeof( PCB)) ))
				return STATUS_NOMEM;

			TPcb.id = proc_get_pid();
			TPcb.t_in = jiffies;
			TPcb.estado = PROC_ENCOLADO;

			memcpy( pPcb, &(TPcb), sizeof(PCB));

			cola_insert( (void *)pPcb );
			estad_insert( pPcb );
		}


		/* Incremento el reloj segun la velocidad actual */
		if( !(iTickTock++ % Tncola->iSpeed ) )
		{
			proc_run( Tncola );

			/* Me fijo qu� metrica estamos dibujando */
			Tncola->pTMetric->update_fn( Tncola->pTMetric );
			Tncola->ipGraph( Tncola, Tncola->pTMetric );
			
			Tncola->iClock++;
			jiffies = Tncola->iClock;	/* TODO v2: Sacar el jiffies de aca. */
		}
		ncola_status_refresh( Tncola );
	}

	return 1;
}


/*********************************************/
int main(void)
{
	TNCola Tncola;

	ncola_init( &Tncola );

/**/	archivo_init( "data.txt" );

	main_loop( &Tncola );

	ncola_end( &Tncola );

/*????
	nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
*/
	return 1;

}
