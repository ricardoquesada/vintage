/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "metric.h"
#include "tipos.h"
#include "estad.h"

static LIST_ENTRY g_metric_list;

/*
 * Funciones propias de cada metrica implementadas como 'modulos'
 * Estas funciones pueden ser llamadas manualmente, aunque son llamadas
 * automaticamente, ya que cada METRIC tiene un ptr a la funcion
 * actualizadora.
 */

/*
 * Tiempo que corrio un proceso
 */
static STATUS metric_psruntime_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_estad_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;

	while( !IsListEmpty( &g_estad_list) && ( pList != &g_estad_list ) ) {

		pPcb = (PPCB) pList;

		if( pPcb->estado == PROC_TERMINADO ) {
			if( pM->min > pPcb->t_run )
				pM->min = pPcb->t_run;
			if( pM->max < pPcb->t_run )
				pM->max = pPcb->t_run;

			prom += pPcb->t_run;
			cant++;
		}
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = -1;
	return STATUS_SUCCESS;
}

static STATUS metric_psruntime_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"psruntime");
	pM->update_fn = metric_psruntime_update;
	pM->attrib = NCOLA_GREEN;

	return metric_register( pM );
}


/*
 * Tiempo de Espera en cola
 */
static STATUS metric_queuewait_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_estad_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;
	while( !IsListEmpty( &g_estad_list ) && ( pList != &g_estad_list) ) {
		pPcb = (PPCB) pList;
		if( pPcb->estado > PROC_ENCOLADO ) {
			if( pM->min > ( pPcb->t_run_start - pPcb->t_in ))
				pM->min = pPcb->t_run_start - pPcb->t_in;
			if( pM->max < ( pPcb->t_run_start - pPcb->t_in ))
				pM->max = pPcb->t_run_start - pPcb->t_in;

			prom += pPcb->t_run_start - pPcb->t_in;
			cant++;
		}
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = -1;
	return STATUS_SUCCESS;
}

static STATUS metric_queuewait_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"queuewait");
	pM->update_fn = metric_queuewait_update;
	pM->attrib = NCOLA_CYAN;

	return metric_register( pM );
}



static STATUS metric_queuesize_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_estad_list.next;
	PPCB pPcb;
	int cant=0;

	pM->cur = 0;

	while( !IsListEmpty( &g_estad_list ) && ( pList != &g_estad_list) )
	{
		pPcb = (PPCB) pList;
		if( pPcb->estado == PROC_ENCOLADO )
			pM->cur++;
		cant++;

		pList = LIST_NEXT(pList);
	}

	if( pM->max < pM->cur ) 
		pM->max = pM->cur;

	/* No tiene sentido calcular el m�nimo en esta metrica */
	pM->min = -1;
	pM->prom = -1;
	return STATUS_SUCCESS;
}


/**
 * Calcula la cantidad de procesos encolados.
 * \author Picorelli Marcelo.
 */
static STATUS metric_queuesize_register( void )
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"queuesize");
	pM->update_fn = metric_queuesize_update;
	pM->attrib = NCOLA_WHITE;

	return metric_register( pM );
}

/*
 * Funciones de las metricas en general
 */
STATUS metric_init()
{
	InitializeListHead( &g_metric_list );

	/* Inicializa las metricas */
	metric_psruntime_register();
	metric_queuewait_register();
	metric_queuesize_register();
	
	/*
	 * Agregar mas aca
	 */

	return STATUS_SUCCESS;
}

STATUS metric_register( PMETRIC data )
{
	InsertTailList( &g_metric_list, (PLIST_ENTRY) data );
	return STATUS_SUCCESS;
}

STATUS metric_flush(void)
{
	PLIST_ENTRY tmp;

	while( !IsListEmpty( &g_metric_list ) ) {
		tmp = RemoveHeadList( &g_metric_list );
		free( tmp );
	}

	return STATUS_SUCCESS;
}

/*
 * Muestra todas las metricas
 */
STATUS metric_display_all( void )
{
	PLIST_ENTRY l = g_metric_list.next;
	PMETRIC pM;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;

		printf("procesando %s\n",pM->nombre);
		if(pM->update_fn)
			pM->update_fn( pM );

		l = LIST_NEXT(l);
	}
	return STATUS_SUCCESS;
}

/*
 * Muestra la metrica que contiene el id 'id'
 */
STATUS metric_display( int id )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( pM->id == id) {
			return STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return STATUS_SUCCESS;
}

/**
 * Devuelve un puntero a la metrica pedida.
 * @param id Identificador de la m�trica buscada.
 * @returns Un puntero a la m�trica si se la encuentra, o \c NULL en caso
 * contrario.
 * \author Quesada Ricardo.
 * \author Picorelli Marcelo.
 */
PMETRIC metric_get( int id )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( pM->id == id) {
			return pM;
		}
		l = LIST_NEXT(l);
	}
	return NULL;
}

/**
 * Devuelve el \c id de la m�trica que se le pide.
 * @param cpMetric El nombre de la metrica que se est� buscando, no puede ser
 * \c NULL.
 * @returns El \c id de la m�trica, o -1 de no encontrarla.
 * \author Quesada Ricardo.
 * \author Picorelli Marcelo.
 */
int metric_id_get( char *cpMetric )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( !strcmp( pM->nombre, cpMetric ) )
		{
			return pM->id;
		}
		l = LIST_NEXT(l);
	}
	return -1;
}



STATUS metric_setup( PMETRIC pM )
{
	static int id=0;
	assert( pM );
	strcpy( pM->nombre,"default");
	pM->id=id++;
	pM->min=0.0;
	pM->max=0.0;
	pM->cur=0.0;
	pM->prom=0.0;
	pM->attrib=0;
	pM->scale_y=0.0;
	pM->attrib=0;
	pM->update_fn=NULL;

	return STATUS_SUCCESS;
}
