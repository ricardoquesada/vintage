#ifndef _NCOLA_GRAPH_
#define _NCOLA_GRAPH_

#include "metric.h"
#include "ncola_common.h"

int ncola_graph_ps( TNCola *pTncola );
int ncola_graph_gqueued( TNCola *pTncola );
int ncola_graph_metric( TNCola *pTncola, PMETRIC pTMetric );
#endif 
