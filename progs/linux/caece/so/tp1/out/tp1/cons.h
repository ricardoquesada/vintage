#ifndef _CONS_
#define _CONS_

/**
 * Une un comando con la funciona apropiada.
 * Utilizado dentro de la mini-consola, asi no hay que utilizar sentencias \c
 * if, sino que solo hay que buscar el comando y llamar a la funci�n asociada.
 */
typedef struct console_bind
{
	char *cpCommand;		/**< Nombre que identifica a este comando.*/
	char *cpHelp;			/**< Mensaje que se mostrar� cuando se pida ayuda sobre
										este comando. */
	int (*ipExecute)( char *cpArg, void *);	/**< Rutina a ejecutar
								cuando es invocado este comando.*/
	void *pUserData;		/**< Datos adicionales que se le pueden pasar a la
									rutina \c ipExecute */
}TConsoleBind;


TConsoleBind **cons_new( void );

TConsoleBind *cons_command_get(TConsoleBind **pvComms, char *cpCommName);

int cons_command_add( TConsoleBind ***pvComms, char *pcCommName,
	char *help, int (*ipFunc)(), void *pUserData );

int cons_command_execute( TConsoleBind **pvComms, char *pcCommName,
	char *cpArg );

void cons_destroy( TConsoleBind **pvComms );
#endif
