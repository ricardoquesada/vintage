#include <assert.h>
#include <curses.h>

#include "tipos.h"
#include "estad.h"
#include "ncola_graph.h"
#include "ncola_common.h"

/**
 * Muestra el estado de los procesos.
 * @param pTncola Las variables que conforman al programa, no puede ser \c
 * NULL.
 * \author Picorelli Marcelo.
 */
int ncola_graph_ps( TNCola *pTncola )
{
  PLIST_ENTRY pPcbTmp;
  PLIST_ENTRY pPcbFirst;
  char *cpEstado[] = { "ENCOLADO", "CORRIENDO", "TERMINADO", "ESPERANDO",
    "DURMIENDO" };

  assert( pTncola != NULL );

  pPcbFirst = &g_estad_list;
  pPcbTmp = LIST_NEXT( pPcbFirst ); 

	wclear( pTncola->pGPad );
  mvwaddstr( pTncola->pGPad, 0,0, "Nombre\tEstado\t\tStart\tEnd\n" );
  while( pPcbTmp != pPcbFirst  )
  {
    wprintw( pTncola->pGPad, "%s\t%s\t%d\t%d\n", ((PPCB)(pPcbTmp))->nombre,
      cpEstado[((PPCB)(pPcbTmp))->estado], ((PPCB)(pPcbTmp))->t_in,
      ((PPCB)(pPcbTmp))->t_run_start );
    pPcbTmp = LIST_NEXT( pPcbTmp );
  }
	waddstr( pTncola->pGPad, "Presione una tecla." );
  wrefresh( pTncola->pGPad );

	nodelay( pTncola->pGPad, FALSE );
	wgetch( pTncola->pGPad );
	nodelay( pTncola->pGPad, TRUE );
	wclear( pTncola->pGPad );

  return 1; 
}

/**
 *
 */
int ncola_graph_gqueued( TNCola *pTncola )
{
	static int iMaxQueued = 0;/* TODO: colocar los datos en sus propias estructs*/
	static int iXLastPos = 0;
	static int iXMaxCoord = 0;
	static int iYMaxCoord = 0;
	int iQueued = 0;

  PLIST_ENTRY pPcbTmp;
  PLIST_ENTRY pPcbFirst;

	assert( pTncola != NULL );

	if( !iXMaxCoord )
		getmaxyx( pTncola->pGPad, iYMaxCoord, iXMaxCoord );

  pPcbFirst = &( g_estad_list );
  pPcbTmp = LIST_NEXT( pPcbFirst ); 

  while( pPcbTmp != pPcbFirst  )
	{
		if( ((PPCB)(pPcbTmp))->estado == PROC_ENCOLADO )
			iQueued++;
    pPcbTmp = LIST_NEXT( pPcbTmp );
	}

	if( !iXLastPos )
		wclear( pTncola->pGPad );

	if( iQueued > iMaxQueued )
	{
		mvwaddch( pTncola->pGPad, iYMaxCoord - iMaxQueued - 1, 0, ' ' );
		mvwaddch( pTncola->pGPad, iYMaxCoord - iMaxQueued - 1,iXMaxCoord - 2, ' ' );
		iMaxQueued = iQueued;
	}

	mvwaddch( pTncola->pGPad, iYMaxCoord - iMaxQueued - 1, 0, '>' );
	mvwaddch( pTncola->pGPad, iYMaxCoord - iMaxQueued - 1, iXMaxCoord - 2, '<' );
	mvwaddch( pTncola->pGPad, iYMaxCoord - iQueued - 1, iXLastPos++, '+' );

	wnoutrefresh( pTncola->pGPad );

	if( iXLastPos == iXMaxCoord - 1)
		iXLastPos = 0;

	return 1;
}

/**
 * Grafica las metricas.
 * Se encarga de realizar los graficos de las metricas, mostrando m�ximo,
 * m�nimo, promedio y actual de la m�trica que se le pasa.
 * @param pTncola Las variables que conforman al programa, no puede ser \c
 * NULL.
 * @param pTMetric Las variables que hacen a la m�trica.
 * \author Picorelli Marcelo.
 * \bug En este momento no dibuja la escala, ni cambia la misma con lo que si
 * algun parametro excede el tama�o disponible en pantalla, simplemente
 * desaparece. Adem�s ciertas m�tricas no proveen un valor para actual, con lo
 * que el mismo siempre figura como 0.
 */
int ncola_graph_metric( TNCola *pTncola, PMETRIC pTMetric  )
{
	static int iXLastPos = 0;		/* Ultima posicion dibujada. */
	static int iXMaxCoord = 0;	/* Ancho de la ventana. */
	static int iYMaxCoord = 0;	/* Altura de la ventana. */
	static int iLastMax = 0;		/* Ultimo valor m�ximo. */

	assert( pTncola != NULL );
	assert( pTMetric != NULL );

	if( !iXMaxCoord )
		getmaxyx( pTncola->pGPad, iYMaxCoord, iXMaxCoord );

	/* Alcanzamos el final de la pantalla. */
	if( !iXLastPos )
		wclear( pTncola->pGPad );

	if( iLastMax < pTMetric->max )
	{
		mvwaddch( pTncola->pGPad, iYMaxCoord - iLastMax - 1, 0, ' ' );
		mvwaddch( pTncola->pGPad, iYMaxCoord - iLastMax - 1,iXMaxCoord - 2, ' ' );
		iLastMax = pTMetric->max;
	}


	/* Marcas de m�ximo valor alcanzado */
	wattron( pTncola->pGPad, (attr_t)COLOR_PAIR( NCOLA_RED ) );
	mvwaddch( pTncola->pGPad, iYMaxCoord - iLastMax - 1, 0, '>' );
	mvwaddch( pTncola->pGPad, iYMaxCoord - iLastMax - 1, iXMaxCoord - 2, '<' );

	/* Marcas de m�nimo valor alcanzado */
	mvwaddch( pTncola->pGPad, iYMaxCoord - pTMetric->min- 1, 0, '>' );
	mvwaddch( pTncola->pGPad, iYMaxCoord - pTMetric->min- 1, iXMaxCoord - 2, '<');
	wattroff( pTncola->pGPad, (attr_t)COLOR_PAIR( NCOLA_RED ) );

	/* Marca Promedio */
	wattron( pTncola->pGPad, (attr_t)COLOR_PAIR( pTMetric->attrib ) );
	mvwaddch( pTncola->pGPad, iYMaxCoord - pTMetric->prom - 1,iXLastPos, '+' );
	wattroff( pTncola->pGPad, (attr_t)COLOR_PAIR( pTMetric->attrib ) );

	/* Marca actual */
	mvwaddch( pTncola->pGPad, iYMaxCoord - pTMetric->cur - 1, iXLastPos++, '*' );

	/* Que estamos dibujando ? */
	wattron( pTncola->pGMetrics, (attr_t)COLOR_PAIR( pTMetric->attrib ) );
	mvwaddstr( pTncola->pGMetrics, 18, 0, pTMetric->nombre );
	wattroff( pTncola->pGMetrics, (attr_t)COLOR_PAIR( pTMetric->attrib ) );

	wnoutrefresh( pTncola->pGPad );

	if( iXLastPos == iXMaxCoord - 1)
		iXLastPos = 0;

	return 1;
}
