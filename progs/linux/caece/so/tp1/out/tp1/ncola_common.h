#ifndef _NCOLA_COMMON_
#define _NCOLA_COMMON_

#include <curses.h>

#include "tipos.h"
#include "metric.h"
#include "cons.h"

/**
 * Esta estructura mantiene las propiedades que hacen al programa.
 */
typedef struct _TNCola_
{
	WINDOW *pGPad;	/**< Regi�n de la pantalla donde se muestran los gr�ficos de
		evoluci�n de las �m�tricas?. */
	WINDOW *pGMetrics; /**< Regi�n de la pantalla donde se muestran las
		m�tricas.*/
	WINDOW *pGConsole; /**< Regi�n de la pantalla donde se muestran distintos
		mensajes y donde puede aparecer la consola.*/
	TConsoleBind **pTCommands;	/**< Commandos que acepta al estar en modo \b
		Consola.*/

	int (*ipGraph)( void *, void * );	/**< Rutina para mostrar el estado de los
		procesos. */

	PMETRIC	pTMetric; /**< Puntero a la m�trica que se est� dibujando.*/

	int iClock; /**< Clock interno. */
	int iSpeed; /**< Velocidad del clock.*/
}TNCola;
#endif
