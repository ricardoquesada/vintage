/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <stdio.h>
#include <assert.h>

#include "tipos.h"
#include "proc.h"
#include "cola.h"
#include "ncola_common.h"

unsigned long jiffies=0;		/* ultimo tick del procesador */

static unsigned long new_pid=0;		/* ultimo id asignado */
static int proc_estatus=0;
static PPCB pCur_pcb=NULL;

int proc_get_pid()
{
	return new_pid++;
}

/*
 * Devuelvo 1 si esta ejecutando.
 * 0 si esta 'idle'
 */
int proc_get_status()
{
  return proc_estatus;
}


STATUS  proc_run( TNCola *pTncola )
{
	if( proc_estatus ) {
		if( pCur_pcb->t_run_start + pCur_pcb->t_run <= jiffies ) {

			mvwprintw( pTncola->pGMetrics, 2, 0, "Last : %s",
				pCur_pcb->nombre);
			wclrtoeol( pTncola->pGMetrics );
			wrefresh( pTncola->pGMetrics );

			pCur_pcb->estado = PROC_TERMINADO;
			pCur_pcb=NULL;
			proc_estatus=0;
		}
	} else {

	/*
	 * FIXME: Puede en un mismo ciclo terminar un proc
	 * y empezar otro ? (if TRUE sacar el 'else' y poner otro 'if' )
	 */

		if( cola_remove( (void **)&pCur_pcb )==STATUS_SUCCESS) {
			pCur_pcb->estado = PROC_CORRIENDO;
			pCur_pcb->t_run_start = jiffies;
			proc_estatus = 1;
		}
	}

	/* Actualizamos el estado de la 'CPU' */	
	mvwprintw( pTncola->pGMetrics, 1, 0, "CPU  : %s", 
		proc_estatus ? pCur_pcb->nombre : "IDLE" );
	wclrtoeol( pTncola->pGMetrics );
	wnoutrefresh( pTncola->pGMetrics );

	return STATUS_SUCCESS;
}
