/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <assert.h>

#include "cola.h"
#include "tipos.h"

static COLA g_cola;

int cola_vacia ()
{
	return ( g_cola.front == g_cola.rear );
}

STATUS cola_init()
{
	g_cola.rear = g_cola.front = 0;
	return STATUS_SUCCESS;
}

STATUS cola_insert( void * data )
{
	assert( data );

	if( g_cola.rear == MAXQUEUE-1)
		g_cola.rear = 0;
	else
		(g_cola.rear)++;

	if( g_cola.rear == g_cola.front ) {
		return STATUS_OVERFLOW;
	}

	g_cola.items[g_cola.rear] = data;
	return STATUS_SUCCESS;
}


STATUS cola_remove( void ** data )
{
	assert( data );

	if( cola_vacia()) {
		return STATUS_UNDERFLOW;
	}

	if( g_cola.front == MAXQUEUE-1)
		g_cola.front = 0;
	else
		(g_cola.front)++;

	*data = (g_cola.items[g_cola.front]);
	return STATUS_SUCCESS;
}
