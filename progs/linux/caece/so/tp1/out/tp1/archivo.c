/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>

#include "proc.h"
#include "tipos.h"
#include "archivo.h"
#include "ncola_common.h"

FILE *fp=NULL;

STATUS archivo_init(char *nombre)
{
	if( (fp = fopen( nombre, "rt" )) == NULL)
		return STATUS_FILEERROR;

	return STATUS_SUCCESS;
}

STATUS archivo_read( PPCB ppcb, /**/TNCola *pTncola )
{
	static PCB last_pcb;
	static unsigned long last = 0;
	static int have_data=0;
	static int delta=0;


	if( !have_data ) {
		if(!fp)
			return STATUS_FILEERROR;

		if( feof( fp ))
			return STATUS_FILEEOF;


		have_data = 1;
		fscanf( fp, "%s\t%d\t%d\n", last_pcb.nombre, &delta, (unsigned*)&last_pcb.t_run);
		if( delta < 0 ) {
			wprintw( pTncola->pGConsole, 
				"Error: Tiempo relativo (%d) debe ser positivo\n", delta );
			wrefresh( pTncola->pGConsole );
			have_data=0;
			return STATUS_ERROR;
		}

			wprintw( pTncola->pGConsole, 
				"Ok: Se leyo el proc:%s que durara %d ciclos\n", last_pcb.nombre,
				(unsigned)last_pcb.t_run);
			wrefresh( pTncola->pGConsole );
	}


	if( ( have_data ) && ( delta + last <= jiffies )) {
		last = jiffies;
		memcpy( ppcb, &last_pcb, sizeof(PCB));
		have_data=0;
		return STATUS_SUCCESS;
	}
	return STATUS_NOTREADY;
}

STATUS archivo_close(void)
{
	if(fp)
		fclose( fp );
	return STATUS_SUCCESS;
}
