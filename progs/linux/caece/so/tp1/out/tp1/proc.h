/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_PROC_H
#define __TP1_PROC_H

#include "tipos.h"
#include "ncola_common.h"

STATUS  proc_run( TNCola *pTncola );
int proc_get_pid();
int proc_get_status();

extern unsigned long jiffies;		

#endif /* __TP1_PROC_H */
