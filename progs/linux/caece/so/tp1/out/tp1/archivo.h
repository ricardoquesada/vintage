/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_ARCHIVO_H
#define __TP1_ARCHIVO_H

#include "tipos.h"
#include "ncola_common.h"

STATUS archivo_init(char *nombre);
STATUS archivo_read( PPCB ppcb, TNCola *pTncola );
STATUS archivo_close(void);

#endif  /* __TP1_ARCHIVO_H */
