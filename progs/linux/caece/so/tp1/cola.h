/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_COLA_H
#define __TP1_COLA_H

#include "tipos.h"

#define MAXQUEUE 1000

typedef struct _cola {
	void * items[MAXQUEUE];
	int front, rear;
} COLA, *PCOLA;

int cola_vacia();
STATUS cola_init();
STATUS cola_insert( void * data );
STATUS cola_remove( void ** data );

#endif /* __TP1_COLA_H */
