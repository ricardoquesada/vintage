/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>

#include "tipos.h"
#include "archivo.h"
#include "proc.h"

FILE *fp=NULL;

STATUS archivo_init(char *nombre)
{
	if( (fp = fopen( nombre, "rt" )) == NULL)
		return STATUS_FILEERROR;

	return STATUS_SUCCESS;
}

STATUS archivo_read( PPCB ppcb )
{
	static PCB last_pcb;
	static unsigned long last = 0;
	static int have_data=0;
	static int delta=0;


	if( !have_data ) {
		if(!fp)
			return STATUS_FILEERROR;

		if( feof( fp ))
			return STATUS_FILEEOF;


		have_data = 1;
		fscanf( fp, "%s\t%d\t%d\n", last_pcb.nombre, &delta, (unsigned*)&last_pcb.t_run);
		if( delta < 0 ) {
			printf("El tiempo relativo debe ser positivo\n");
			have_data=0;
			return STATUS_ERROR;
		}

		printf("se leyo el proc:%s que durara %d ciclos\n",last_pcb.nombre,(unsigned)last_pcb.t_run);
	}


	if( ( have_data ) && ( delta + last <= jiffies )) {
		last = jiffies;
		memcpy( ppcb, &last_pcb, sizeof(PCB));
		have_data=0;
		return STATUS_SUCCESS;
	}
	return STATUS_NOTREADY;
}

STATUS archivo_close(void)
{
	if(fp)
		fclose( fp );
	return STATUS_SUCCESS;
}
