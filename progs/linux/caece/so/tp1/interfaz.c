/*
 * Funciones que se encargan de graficar
 */

#include <stdio.h>

#include "tipos.h"
#include "metric.h"
#include "interfaz.h"


/* 
 * Esta es la funcion que hay que rescribir
 * usando ncurses o lo que quieras
 */
STATUS interfaz_gfx( PMETRIC pM )
{
	printf("nombre:%s\n",pM->nombre);
	printf("min:%d\n",pM->min);
	printf("max:%d\n",pM->max);
	printf("prom:%d\n",pM->prom);
	printf("cur:%d\n",pM->cur);

	return STATUS_SUCCESS;
}
