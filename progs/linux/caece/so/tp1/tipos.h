/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_TIPOS_H
#define __TP1_TIPOS_H


typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY *next;
	struct _LIST_ENTRY *prev;
} LIST_ENTRY, *PLIST_ENTRY;

typedef enum {
	STATUS_SUCCESS,
	STATUS_ERROR,
	STATUS_OVERFLOW,
	STATUS_UNDERFLOW,
	STATUS_FILEERROR,
	STATUS_FILEEOF,
	STATUS_NOTREADY,
	STATUS_NOMEM,
} STATUS, *PSTATUS;

typedef enum {
	PROC_ENCOLADO,
	PROC_CORRIENDO,
	PROC_TERMINADO,
	PROC_WAITING,
	PROC_SLEEPING,
} PROC, *PPROC;

typedef struct _pcb {
	LIST_ENTRY next;
	unsigned long id;		/* identificador unico del proceso */
	PROC estado;
	char nombre[100];

	unsigned long t_in;		/* tiempo en que se encolo */
	unsigned long t_run_start;	/* tiempo es que se empezo a ejecutar */
	unsigned long t_run;		/* tiempo que tarda en consumirse */

	void *priv;			/* priv */

} PCB, *PPCB;

extern unsigned long jiffies;


#define LENTRY_NULL {NULL,NULL}

#define LIST_NEXT(Entry) (((PLIST_ENTRY)Entry)->next)
#define LIST_PREV(Entry) (((PLIST_ENTRY)Entry)->prev)

//
//  Doubly-linked list manipulation routines.  Implemented as macros
//  but logically these are procedures.
//

//
//  VOID
//  InitializeListHead(
//      PLIST_ENTRY ListHead
//      );
//

#define InitializeListHead(ListHead) (\
    (ListHead)->next = (ListHead)->prev = (ListHead))

//
//  BOOLEAN
//  IsListEmpty(
//      PLIST_ENTRY ListHead
//      );
//

#define IsListEmpty(ListHead) \
    ((ListHead)->next == (ListHead))

//
//  PLIST_ENTRY
//  RemoveHeadList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveHeadList(ListHead) \
    (ListHead)->next;\
    {RemoveEntryList((ListHead)->next)}

//
//  PLIST_ENTRY
//  RemoveTailList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveTailList(ListHead) \
    (ListHead)->prev;\
    {RemoveEntryList((ListHead)->prev)}

//
//  VOID
//  RemoveEntryList(
//      PLIST_ENTRY Entry
//      );
//

#define RemoveEntryList(Entry) {\
    PLIST_ENTRY _EX_prev;\
    PLIST_ENTRY _EX_next;\
    _EX_next = (Entry)->next;\
    _EX_prev = (Entry)->prev;\
    _EX_prev->next = _EX_next;\
    _EX_next->prev = _EX_prev;\
    }

//
//  VOID
//  InsertTailList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertTailList(ListHead,Entry) {\
    PLIST_ENTRY _EX_prev;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_prev = _EX_ListHead->prev;\
    (Entry)->next = _EX_ListHead;\
    (Entry)->prev = _EX_prev;\
    _EX_prev->next = (Entry);\
    _EX_ListHead->prev = (Entry);\
    }

//
//  VOID
//  InsertHeadList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertHeadList(ListHead,Entry) {\
    PLIST_ENTRY _EX_next;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_next = _EX_ListHead->next;\
    (Entry)->next = _EX_next;\
    (Entry)->prev = _EX_ListHead;\
    _EX_next->prev = (Entry);\
    _EX_ListHead->next = (Entry);\
    }


#endif /* __TP1_TIPOS_H */
