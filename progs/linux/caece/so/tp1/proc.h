/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __TP1_PROC_H
#define __TP1_PROC_H

#include "tipos.h"

extern unsigned long jiffies;		

STATUS  proc_run();
int proc_get_pid();
int proc_get_status();

#endif /* __TP1_PROC_H */
