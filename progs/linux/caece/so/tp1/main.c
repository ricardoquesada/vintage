/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNIX
#include <unistd.h>
#endif

#include "tipos.h"
#include "cola.h"
#include "archivo.h"
#include "estad.h"
#include "proc.h"
#include "metric.h"

STATUS main_loop(void)
{ 
	PCB pcb;

	while(1) {
		printf("--> ciclo %d <--\n",(unsigned)jiffies);

		if (archivo_read( &pcb ) == STATUS_SUCCESS ) {

			PPCB pPcb;

			if( !(pPcb = malloc( sizeof( PCB)) ))
				return STATUS_NOMEM;

			pcb.id = proc_get_pid();
			pcb.t_in = jiffies;
			pcb.estado = PROC_ENCOLADO;

			memcpy( pPcb, &pcb, sizeof(PCB));

			cola_insert( (void *)pPcb );

			estad_insert( pPcb );
		}
			
		proc_run();

		metric_display_all();

		/* siguiente ciclo */
#ifdef HAVE_UNIX
		usleep(5000);
#endif
		jiffies++;
	}
}

int main( void )
{
	cola_init();
	estad_init();
	archivo_init("data.txt");
	metric_init();


	main_loop();

	archivo_close();
	metric_flush();
	estad_flush();

	return 0;
}
