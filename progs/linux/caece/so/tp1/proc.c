/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <stdio.h>
#include <assert.h>

#include "tipos.h"
#include "proc.h"
#include "cola.h"

unsigned long jiffies=0;		/* ultimo tick del procesador */

static unsigned long new_pid=0;		/* ultimo id asignado */
static int proc_estatus=0;		/* estado del "procesador" */
static PPCB pCur_pcb=NULL;		/* ptr al proceso activo */

int proc_get_pid()
{
	return new_pid++;
}

/*
 * Devuelvo 1 si esta ejecutando.
 * 0 si esta 'idle'
 */
int proc_get_status()
{
	return proc_estatus;
}

STATUS  proc_run()
{
	if( proc_estatus ) {
		if( pCur_pcb->t_run_start + pCur_pcb->t_run <= jiffies ) {
			printf("Proceso %s terminado\n",pCur_pcb->nombre);
			pCur_pcb->estado = PROC_TERMINADO;
			pCur_pcb=NULL;
			proc_estatus=0;
		}
	} else {

	/*
	 * FIXME: Puede en un mismo ciclo terminar un proc
	 * y empezar otro ? (if TRUE sacar el 'else' y poner otro 'if' )
	 */

		if( cola_remove( (void **)&pCur_pcb )==STATUS_SUCCESS) {
			pCur_pcb->estado = PROC_CORRIENDO;
			pCur_pcb->t_run_start = jiffies;
			proc_estatus = 1;
		}
	}

	if(proc_estatus)
		printf("Ejecutando :%s\n",pCur_pcb->nombre);
	else
		printf("Procesador sin nada que hacer\n");

	return STATUS_SUCCESS;
}
