/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */
/**
 * \file metric.c
 * Funciones propias de cada metrica implementadas como 'modulos'
 * Estas funciones pueden ser llamadas manualmente, aunque son llamadas
 * automaticamente, ya que cada METRIC tiene un ptr a la funcion
 * actualizadora.
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "metric.h"
#include "tipos.h"
#include "interfaz.h"
#include "sched.h"


static LIST_ENTRY g_metric_list;	/**< Lista de metricas */

/*
 * Tiempo que corrio un proceso
 */
/**
 * \fn STATUS metric_psruntime_update( PMETRIC pM )
 * \brief Funcion que calcula el 'Tiempo de ejecucion'
 * \param pM Metrica donde va a poner el resultado
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_psruntime_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		if( pPcb->p_estado == PROC_TERMINADO ) {
			if( pM->min > pPcb->p_t_run )
				pM->min = pPcb->p_t_run;
			if( pM->max < pPcb->p_t_run )
				pM->max = pPcb->p_t_run;

			prom += pPcb->p_t_run;
			cant++;
		}
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = 0;
	return STATUS_SUCCESS;
}

/**
 * \fn static STATUS metric_psruntime_register()
 * \brief Funcion que calcula el 'Tiempo de Espera'
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_psruntime_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"psruntime");
	pM->update_fn = metric_psruntime_update;

	return metric_register( pM );
}

/**
 * \fn STATUS metric_queuewait_update( PMETRIC pM )
 * \brief Funcion principal de 'Tiempo de Espera'
 * \param pM Metrica donde va a poner el resultado
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_queuewait_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;
	while( !IsListEmpty( &g_proc_list ) && ( pList != &g_proc_list) ) {
		pPcb = proclist2pcb( pList );
		if( pPcb->p_estado > PROC_IDL ) {
			if( pM->min > ( pPcb->p_t_run_start - pPcb->p_t_in ))
				pM->min = pPcb->p_t_run_start - pPcb->p_t_in;
			if( pM->max < ( pPcb->p_t_run_start - pPcb->p_t_in ))
				pM->max = pPcb->p_t_run_start - pPcb->p_t_in;

			prom += pPcb->p_t_run_start - pPcb->p_t_in;
			cant++;
		}
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = 0;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_queuewait_register()
 * \brief Funcion que registra la metrica 'Tiempo de Espera'
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_queuewait_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"queuewait");
	pM->update_fn = metric_queuewait_update;

	return metric_register( pM );
}


/**
 * \fn STATUS metric_init()
 * \brief Inicializa las metricas
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_init()
{
	InitializeListHead( &g_metric_list );

	/* Inicializa las metricas */
	metric_psruntime_register();
	metric_queuewait_register();
	
	/*
	 * Agregar mas aca
	 */

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_register( PMETRIC data )
 * \brief Inicializa las metricas
 * \param data puntero a la metrica
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_register( PMETRIC data )
{
	InsertTailList( &g_metric_list, (PLIST_ENTRY) data );
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_flush(void)
 * \brief flushea las metricas
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_flush(void)
{
	PLIST_ENTRY tmp;

	while( !IsListEmpty( &g_metric_list ) ) {
		tmp = RemoveHeadList( &g_metric_list );
		free( tmp );
	}

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_display_all( void )
 * \brief Funcion que llama a las metricas registradas y luego las graficas
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_display_all( void )
{
	PLIST_ENTRY l = g_metric_list.next;
	PMETRIC pM;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;

#ifdef DEBUG_METRIC
		printf("procesando %s\n",pM->nombre);
#endif
		if(pM->update_fn)
			pM->update_fn( pM );

		interfaz_gfx( pM );

		l = LIST_NEXT(l);
	}

	interfaz_refresh();

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_display( int id )
 * \brief Funcion que llama a una metrica en particular y luego la grafica
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_display( int id )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( pM->id == id) {
			interfaz_gfx( pM );
			return STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_setup( PMETRIC pM )
 * \brief Inicializa una metrica con los valores por default
 * \param pM puntero a la metrica
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_setup( PMETRIC pM )
{
	static int id=0;
	assert( pM );
	strcpy( pM->nombre,"default");
	pM->id=id++;
	pM->min=0.0;
	pM->max=0.0;
	pM->cur=0.0;
	pM->prom=0.0;
	pM->attrib=0;
	pM->scale_y=0.0;
	pM->attrib=0;
	pM->update_fn=NULL;

	return STATUS_SUCCESS;
}

/**
 * Devuelve un puntero a la metrica pedida.
 * @param id Identificador de la m�trica buscada.
 * @returns Un puntero a la m�trica si se la encuentra, o \c NULL en caso
 * contrario.
 * \author Quesada Ricardo.
 * \author Picorelli Marcelo.
 */
PMETRIC metric_get( int id )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( pM->id == id) {
			return pM;
		}
		l = LIST_NEXT(l);
	}
	return NULL;
}

/**
 * Devuelve el \c id de la m�trica que se le pide.
 * @param cpMetric El nombre de la metrica que se est� buscando, no puede ser
 * \c NULL.
 * @returns El \c id de la m�trica, o -1 de no encontrarla.
 * \author Quesada Ricardo.
 * \author Picorelli Marcelo.
 */
int metric_id_get( char *cpMetric )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( !strcmp( pM->nombre, cpMetric ) )
		{
			return pM->id;
		}
		l = LIST_NEXT(l);
	}
	return -1;
}
