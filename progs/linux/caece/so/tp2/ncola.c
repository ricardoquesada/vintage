/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <curses.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cons.h"
#include "tipos.h"
#include "metric.h"
#include "archivo.h"
#include "ncola_comms.h"
#include "ncola_graph.h"
#include "ncola_common.h"

/**
 * \file ncola.c
 * Manejo de pantalla
 */

/**
 * Info sobre el programa, el grupo (autores) y cualquier otra yerba \b ACA.
 */

#define NCOLA_FILAS	20			/**< El ancho del pGPad (ver TNCola). */
#define NCOLA_COLUMNAS	60	/**< El alto del pGPad (ver TNCola). */
#define NCOLA_CONSOLE_BUFFER 80 /**< Tama�o del buffer de entrada para la
	consola.*/

static void ncola_init_screen( TNCola *pTncola );
static void ncola_comms_init( TNCola *pTncola );

/**
 * Inicia las rutinas del programa.
 * Actualmente esto es:
 *	- Incializar el ncurses.
 *	- Inicializar la estructura de comandos y agregar los mismos.
 *	- Inicializar la cola de procesos.
 *	- Inicializar la lista de estados.
 *	- Inicializar la lista de m�tricas.
 *	- Dibujar la pantalla (inicializando las estructuras internas).
 *
 * El \c ncurses est� siendo utilizado para la representaci�n de los datos de la
 * cola en pantalla, los gr�ficos y las m�tricas; tambi�n se lo utiliza para
 * el manejo del teclado.
 * @param pTncola La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
void pantalla_init( TNCola *pTncola )
{
	assert( pTncola != NULL );

	initscr();
	start_color();

	init_pair( NCOLA_GREEN, COLOR_GREEN, COLOR_BLACK );
	init_pair( NCOLA_RED, COLOR_RED, COLOR_BLACK );
	init_pair( NCOLA_CYAN, COLOR_CYAN, COLOR_BLACK );
	init_pair( NCOLA_WHITE, COLOR_WHITE, COLOR_BLACK );

	cbreak();
	noecho();

	pTncola->iClock = 0;
	pTncola->iSpeed = 1000;

	ncola_init_screen( pTncola );
	ncola_comms_init( pTncola );

	pTncola->pTMetric = metric_get( 0 );	/* Metrica 'default' */
	pTncola->ipGraph = ncola_graph_metric;
}


/**
 * Inicia la consola.
 * Incicia las estructuras de datos pertinentes y agrega los comandos
 * definidos.
 * @param pTncola La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
static void ncola_comms_init( TNCola *pTncola )
{
	assert( pTncola != NULL );

	pTncola->pTCommands = cons_new();
	assert( pTncola->pTCommands != NULL );

	ncola_comms_add_help( pTncola, pTncola->pGConsole );

	cons_command_add( &pTncola->pTCommands, "clear", "Limpia la pantalla",
		ncola_comms_clear, pTncola->pGPad );

	cons_command_add( &pTncola->pTCommands, "reset", 
		"Pone en cero al clock interno.", ncola_comms_clock_reset, pTncola );

	cons_command_add( &pTncola->pTCommands, "speed",
		"speed [velocidad] - Modifica la velocidad del clock, si no se pasan \
par�metros muestra la velocidad actual", ncola_comms_clock_set_speed, 
		pTncola );

	cons_command_add( &pTncola->pTCommands, "ps",
		"Muestra el estado actual de todos los procesos", ncola_comms_ps, pTncola );

	cons_command_add( &pTncola->pTCommands, "list",
		"Muestra la lista de comandos disponibles", ncola_comms_list, pTncola );

	cons_command_add( &pTncola->pTCommands, "graph",
		"Cambia la m�trica que se desea ver", ncola_comms_graph, pTncola );

}

/**
 * Termina el programa.
 * Actualmente esto es:
 *	- Eliminar la estructura de comandos.
 *	- Terminar el ncurses.
 *	- Eliminar las estructuras creadas y liberar la memoria.
 */
void pantalla_end( TNCola *pTncola )
{
	assert( pTncola != NULL );

	nocbreak();
	cons_destroy( pTncola->pTCommands );

	delwin( pTncola->pGPad );
	delwin( pTncola->pGMetrics );
	delwin( pTncola->pGConsole );
	endwin();
}

/**
 * Dibuja la pantalla inicial.
 * Crea las estructuras necesarias, como ser las diversas ventanas.
 */
static void ncola_init_screen( TNCola *pTncola )
{
	assert( pTncola != NULL );

	/* Algunas l�neas para mejorar la apariencia */
	mvhline( 0, 1, ACS_HLINE, COLS - 2 );
	mvhline( LINES - 1, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, 0, ACS_VLINE, LINES - 2 );
	mvvline( 1, COLS - 1, ACS_VLINE, LINES - 2 );

	mvhline( NCOLA_FILAS, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, NCOLA_COLUMNAS - 1, ACS_VLINE, NCOLA_FILAS - 1 );

	/* Ahora las esquinas e intersecciones */
	mvaddch( 0, 0, ACS_ULCORNER );
	mvaddch( 0, COLS - 1, ACS_URCORNER );
	mvaddch( LINES - 1, 0, ACS_LLCORNER );
	mvaddch( LINES - 1, COLS - 1, ACS_LRCORNER );

	mvaddch( NCOLA_FILAS, NCOLA_COLUMNAS - 1, ACS_BTEE );
	mvaddch( 0, NCOLA_COLUMNAS - 1, ACS_TTEE );
	mvaddch( NCOLA_FILAS, 0, ACS_LTEE );
	mvaddch( NCOLA_FILAS, COLS - 1, ACS_RTEE );
	refresh();

	/* Graficos */
	pTncola->pGPad = newwin( NCOLA_FILAS - 1, NCOLA_COLUMNAS - 2, 1, 1 );
	scrollok( pTncola->pGPad, TRUE );
	wnoutrefresh( pTncola->pGPad );

	/* Metricas */
	pTncola->pGMetrics = newwin( NCOLA_FILAS - 1, COLS -
		NCOLA_COLUMNAS - 2, 1, NCOLA_COLUMNAS );
	scrollok( pTncola->pGMetrics, TRUE );
	wnoutrefresh( pTncola->pGMetrics );

	/* Consola */
	pTncola->pGConsole = newwin( LINES - NCOLA_FILAS - 2, COLS - 2,
		NCOLA_FILAS + 1, 1 );
	scrollok( pTncola->pGConsole, TRUE );
	keypad( pTncola->pGConsole, TRUE );
	nodelay( pTncola->pGConsole, TRUE );
	wnoutrefresh( pTncola->pGConsole );

	doupdate();
}

/**
 * Muestra el estado actual de los procesos.
 * \author Picorelli Marcelo.
 */
void ncola_status_refresh( TNCola *pTncola )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int ivEstado[PROC_LAST];
	int i;

	assert( pTncola != NULL );

	mvwprintw( pTncola->pGMetrics, 0, 0, "Clock: %d", jiffies );
	wclrtoeol( pTncola->pGMetrics );


	memset( &ivEstado, 0, sizeof( ivEstado ));

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		ivEstado[ pPcb->p_estado ]++;
		pList = LIST_NEXT( pList );
	}

	mvwprintw( pTncola->pGMetrics, 3, 0, "Parados  : %d", ivEstado[ PROC_STOP ] );
	wclrtoeol( pTncola->pGMetrics );

	mvwprintw( pTncola->pGMetrics, 4, 0, "Dormidos  : %d", ivEstado[ PROC_SLEEP ] );
	wclrtoeol( pTncola->pGMetrics );

	mvwprintw( pTncola->pGMetrics, 5, 0, "Ejecutando : %d", ivEstado[ PROC_RUN ] );
	wclrtoeol( pTncola->pGMetrics );

	mvwprintw( pTncola->pGMetrics, 6, 0, "Terminados : %d", ivEstado[ PROC_TERMINADO ] );
	wclrtoeol( pTncola->pGMetrics );

	mvwprintw( pTncola->pGMetrics, 8, 0, "CPUS");
	wclrtoeol( pTncola->pGMetrics );

	for(i=0;i<NRCPU;i++) {
		mvwprintw( pTncola->pGMetrics, 9+i, 0, "%i:%s",i,
				(cpu[i].c_estado==CPU_RUNNING ? cpu[i].c_proceso->p_nombre : "IDLE" )
				);
		wclrtoeol( pTncola->pGMetrics );
	}

	wrefresh( pTncola->pGMetrics );
	
}

int ncola_console( TNCola *pTncola )
{
	char cpBuffer[ NCOLA_CONSOLE_BUFFER ];
	char tokens[2][80];
	char *cpTemp;

	assert( pTncola != NULL );

	echo();
	nodelay( pTncola->pGConsole, FALSE );
	waddch( pTncola->pGConsole, '$' );
	wgetnstr( pTncola->pGConsole, cpBuffer, NCOLA_CONSOLE_BUFFER );
	if( strlen( cpBuffer ) )
	{
		/* Leo la linea de comando, por ahora a lo sumo 2 palabras.*/
		if( ( cpTemp = strtok( cpBuffer, " " ) ) )
		{
			strcpy( tokens[0], cpTemp );
			if( ( cpTemp = strtok( NULL, " " ) ) )
				strcpy( tokens[1], cpTemp );
			else
				tokens[1][0] = 0;

			cons_command_execute( pTncola->pTCommands, tokens[0],
				tokens[1] );
		}
	}

	noecho();
	nodelay( pTncola->pGConsole, TRUE );

	return 1;
}
