!
! cat.prg
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	68	0	0
	lda	13
	burst	80
	deca
	jnz	-3

	lda	5
	burst	80
	read	1	10
	deca
	jnz	-6

	lda	93
	burst	56
	deca
	jnz	-3

	read	0	3
	write	1	1

	lda	93
	burst	87
	deca
	jnz	-3

	lda	8
	read	0	3
	write	1	1
	deca
	jnz	-7

	exit	0
.end
