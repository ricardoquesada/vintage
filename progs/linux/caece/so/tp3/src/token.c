/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "interfaz.h"
#include "load.h"
#include "token.h"
#include "tokens.h"

/**
 * \file token.c
 * pseudo 'JIT' Just In Time compiler.
 * Cuando se carga el programa, se lo compila a 'bytecode', 
 * y eso es lo que se guarda en memoria, y se ejecuta.
 * \author Ricardo Quesada
 */

struct _tokens {
	char *nombre;		/* nombre del token */
	int params;		/* parametros que recibe */
	char bytecode;		/* codigo del bytecode */
	STATUS (*func)();	/* funcion a llamar */
} tokens[] = {
	/* Para que la ejecucion sea 'optimizada' hacer el bytecode '0'
	 * este en el indice 0, el '1' en el 1, etc */
	{TOKEN_NILL	,0, 0, token_nill },
	{TOKEN_EXIT	,1, 1, token_exit },
	{TOKEN_FORK	,0, 2, token_fork },
	{TOKEN_READ	,2, 3, token_read },
	{TOKEN_WRITE	,2, 4, token_write },
	{TOKEN_OPEN	,1, 5, token_open },
	{TOKEN_CLOSE	,1, 6, token_close },
	{TOKEN_WAIT	,1, 7, token_wait },
	{TOKEN_BURST	,1, 8, token_burst },
	{TOKEN_LDA	,1, 9, token_lda },
	{TOKEN_JZ	,1,10, token_jz },
	{TOKEN_JNZ	,1,11, token_jnz },
	{TOKEN_INCA	,0,12, token_inca },
	{TOKEN_DECA	,0,13, token_deca },
	{TOKEN_JUMP	,1,14, token_jump },
};
#define	NRTOKENS  (sizeof(tokens)/sizeof(tokens[0]))

/**
 * \fn static STATUS get_specific( FILE *fp )
 * \brief Evalua codigo especifico del interprete
 * \author Ricardo Quesada
 */
static STATUS get_specific( FILE *fp )
{
	int r;
	char c;
	char quesoy[1000];

	PDEBUG("get_specific()");

read_loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	r=fread(  &c, 1 ,1, fp);
	if(r!=1 || c=='\n')
		goto read_loop;

	if( c=='.') {
		fscanf( fp,"%s\n", quesoy );
		if(strcmp( quesoy,"end")==0)
			return STATUS_END;
		else
			return STATUS_ERROR;
	}
	fseek( fp, -1 ,SEEK_CUR);
	return STATUS_SUCCESS;
}

/**
 * \fn static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
 * \brief Convierte un string a un bytecode
 * \author Ricardo Quesada
 */
static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
{
	char token[100];
	int  param1;
	int  param2;
	int  param3;
	int  param4;
	int i;

	PDEBUG("convert_2_bytecode()");

	fscanf(fp, "%s %d %d %d %d\n",token,&param1,&param2,&param3,&param4);

	for(i = 0; i < NRTOKENS; i++) {
		if(strcmp( token, tokens[i].nombre )==0 ){
			pPcb->p_codeseg[(*indice)++] = tokens[i].bytecode;
			if( tokens[i].params >= 1 ) 
				pPcb->p_codeseg[(*indice)++] = (char) param1;
			if( tokens[i].params >= 2 )
				pPcb->p_codeseg[(*indice)++] = (char) param2; 
			if( tokens[i].params >= 3 )
				pPcb->p_codeseg[(*indice)++] = (char) param3;
			if( tokens[i].params >= 4 )
				pPcb->p_codeseg[(*indice)++] = (char) param4;
			return STATUS_SUCCESS;
		}
	}
	console_out("Error en %s: Token '%s' no encontrado\n",pPcb->p_nombre,token);
	return STATUS_ERROR;
}

/**
 * \fn STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
 * \brief Funcion que se encarga de convertir a 'bytecodes' un programa
 * \author Ricardo Quesada
 */
STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
{
	int indice=0;

	PDEBUG("tokenize_to_mem()");

loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	/* es un REM */
	if( get_rem( fp ) == STATUS_FILEEOF )
		return STATUS_FILEEOF;

	/* es algun valor del interprete */
	switch( get_specific( fp )) {
		case STATUS_END:
			return STATUS_SUCCESS;
		case STATUS_FILEEOF:
			return STATUS_FILEEOF;
		case STATUS_SUCCESS:
			if( convert_2_bytecode( pPcb, fp, &indice ) != STATUS_SUCCESS )
				return STATUS_ERROR;
			break;
		default:
			break;
	}
	goto loop;
}

/**
 * \fn STATUS run_proc( PPCB pPcb )
 * \brief Hace 'andar' los procesos
 * \author Ricardo Quesada
 */
STATUS run_proc( PPCB pPcb )
{
	char t = pPcb->p_codeseg[pPcb->p_ip];

	pPcb->p_cpu_usada++;

	if( t >= NRTOKENS ) {
		console_out("Instruccion invalida en: %s\n",pPcb->p_nombre);
		return STATUS_INVALID;
	}

	return (tokens[(int)t].func)(pPcb);
}
