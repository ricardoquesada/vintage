/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#ifndef _EST_PANTALLA_
#define _EST_PANTALLA_

#include <curses.h>

#include "tipos.h"
#include "metric.h"
#include "cons.h"

/**
 * \file est_pantalla.h
 * Esta estructura mantiene las propiedades que hacen al programa.
 */
typedef struct _TSPant_
{
	WINDOW *pGPad;	/**< Regi�n de la pantalla donde se muestran los gr�ficos de
		evoluci�n de las �m�tricas?. */
	WINDOW *pGMetrics; /**< Regi�n de la pantalla donde se muestran las
		m�tricas.*/
	WINDOW *pGConsole; /**< Regi�n de la pantalla donde se muestran distintos
		mensajes y donde puede aparecer la consola.*/
	TConsoleBind **pTCommands;	/**< Commandos que acepta al estar en modo \b
		Consola.*/

	int (*ipGraph)( void *, void * );	/**< Rutina para mostrar el estado de los
		procesos. */

	PMETRIC	pTMetric; /**< Puntero a la m�trica que se est� dibujando.*/

	int iClock; /**< Clock interno. */
}TSPant;


/* prototipos */
void pantalla_init( TSPant *pTspant );
void pantalla_end( TSPant *pTspant );
void pantalla_status_refresh( TSPant *pTspant );
int pantalla_console( TSPant *pTspant );

#endif
