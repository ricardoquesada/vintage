/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */
/**
 * \file metric.c
 * Funciones propias de cada metrica implementadas como 'modulos'
 * Estas funciones pueden ser llamadas manualmente, aunque son llamadas
 * automaticamente, ya que cada METRIC tiene un ptr a la funcion
 * actualizadora.
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "metric.h"
#include "tipos.h"
#include "interfaz.h"
#include "sched.h"


static LIST_ENTRY g_metric_list;	/**< Lista de metricas */

/**
 * \fn static STATUS metric_cpuload_update( PMETRIC pM )
 * \brief Funcion que calcula el 'cpu load'
 * \param pM Metrica donde va a poner el resultado
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_cpuload_update( PMETRIC pM )
{
	int i,cant;
	cant=0;
	for(i=0;i<NRCPU;i++) {
		if( cpu[i].c_estado == CPU_RUNNING )
			cant++;
	}
	cant = cant * 4;

	if( pM->min > cant )
		pM->min = cant;
	if( pM->max < cant )
		pM->max = cant;
	pM->cur = cant;
	pM->prom = (pM->max - pM->min );
	return STATUS_SUCCESS;
}
/**
 * \fn static STATUS metric_cpuload_register()
 * \brief Funcion que calcula el 'CPU LOAD'
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_cpuload_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"cpuload");
	pM->update_fn = metric_cpuload_update;

	return metric_register( pM );
}

/**
 * \fn STATUS metric_psruntime_update( PMETRIC pM )
 * \brief Funcion que calcula el 'Tiempo de ejecucion'
 * \param pM Metrica donde va a poner el resultado
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_psruntime_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		if( pPcb->p_estado == PROC_TERMINADO ) {
			if( pM->min > pPcb->p_cpu_usada )
				pM->min = pPcb->p_cpu_usada;
			if( pM->max < pPcb->p_cpu_usada )
				pM->max = pPcb->p_cpu_usada;

			prom += pPcb->p_cpu_usada;
			cant++;
		}
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = 0;
	return STATUS_SUCCESS;
}

/**
 * \fn static STATUS metric_psruntime_register()
 * \brief Funcion que calcula el 'Tiempo de Espera'
 * \return STATUS
 * \author Ricardo Quesada
 */
static STATUS metric_psruntime_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"psruntime");
	pM->update_fn = metric_psruntime_update;

	return metric_register( pM );
}

/**
 * \fn STATUS metric_overhead_update( PMETRIC pM )
 * \brief Funcion que calcula el 'Tiempo de Overhead'
 * \param pM Metrica donde va a poner el resultado
 * \return STATUS
 * \author P Almejun / L Colaner
 */
static STATUS metric_overhead_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;
	int Overh=0;

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );
		/*  Overhead: El unico Overhead que se produce es el tiempo de
		    espera en la cola ?? */
		if( pPcb->p_estado == PROC_TERMINADO ) {
 			Overh = (pPcb->p_t_run_end - pPcb->p_t_run_start);

			if( pM->min > Overh )
				pM->min = Overh;

			if( pM->max < Overh )
				pM->max = Overh;

			prom += Overh;
			cant++;
		} 
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = 0;
	return STATUS_SUCCESS;
}

 /**
 * \fn static STATUS metric_overhead_register()
 * \brief Funcion que calcula el 'Tiempo de Overhead'
 * \return STATUS
 * \author P Almejun / L Colaner
 */
static STATUS metric_overhead_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"overhead");
	pM->update_fn = metric_overhead_update;

	return metric_register( pM );
}

/**
 * \fn STATUS metric_system_update( PMETRIC pM )
 * \brief Funcion principal de 'Tiempo Promedio en el Sistema'
 * \param pM Metrica donde va a poner el resultado
 * \return STATUS
 * \author Pablo Almejun y Leonardo Colaneri
 */
static STATUS metric_system_update( PMETRIC pM )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int cant=0;
	int prom=0;
	while( !IsListEmpty( &g_proc_list ) && ( pList != &g_proc_list) ) {
		pPcb = proclist2pcb( pList );
		if( pPcb->p_estado > PROC_TERMINADO ) {
			if( pM->min > ( pPcb->p_cpu_usada + ( pPcb->p_t_run_end - pPcb->p_t_run_start )))
				pM->min = pPcb->p_cpu_usada + ( pPcb->p_t_run_end - pPcb->p_t_run_start );

			if( pM->max < ( pPcb->p_cpu_usada + ( pPcb->p_t_run_end - pPcb->p_t_run_start )))
				pM->max = pPcb->p_cpu_usada + ( pPcb->p_t_run_end - pPcb->p_t_run_start );

			prom += pPcb->p_cpu_usada + ( pPcb->p_t_run_end - pPcb->p_t_run_start );
			cant++;
		}
		pList = LIST_NEXT(pList);
	}

	if( cant )
		pM->prom =  prom / cant ;

	/* No tiene sentido calcular 'current' en esta metrica */
	pM->cur = 0;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_system_register()
 * \brief Funcion que registra la metrica 'Tiempo Promedio en el Sistema'
 * \return STATUS
 * \author Pablo Almejun y Leonardo Colaneri
 */
static STATUS metric_system_register()
{
	PMETRIC pM = malloc(sizeof(METRIC));
	if(!pM)
		return STATUS_NOMEM;

	metric_setup( pM );
	strcpy( pM->nombre,"system");
	pM->update_fn = metric_system_update;

	return metric_register( pM );
}


/**
 * \fn STATUS metric_init()
 * \brief Inicializa las metricas
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_init()
{
	InitializeListHead( &g_metric_list );

	/* Inicializa las metricas */
	metric_cpuload_register();
	metric_psruntime_register();
	metric_system_register();	
	metric_overhead_register();
	
	/*
	 * Agregar mas aca
	 */

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_register( PMETRIC data )
 * \brief Inicializa las metricas
 * \param data puntero a la metrica
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_register( PMETRIC data )
{
	InsertTailList( &g_metric_list, (PLIST_ENTRY) data );
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_flush(void)
 * \brief flushea las metricas
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_flush(void)
{
	PLIST_ENTRY tmp;

	while( !IsListEmpty( &g_metric_list ) ) {
		tmp = RemoveHeadList( &g_metric_list );
		free( tmp );
	}

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_display_all( void )
 * \brief Funcion que llama a las metricas registradas y luego las graficas
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_display_all( void )
{
	PLIST_ENTRY l = g_metric_list.next;
	PMETRIC pM;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;

#ifdef DEBUG_METRIC
		printf("procesando %s\n",pM->nombre);
#endif
		if(pM->update_fn)
			pM->update_fn( pM );

		interfaz_gfx( pM );

		l = LIST_NEXT(l);
	}

	interfaz_refresh();

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_display( int id )
 * \brief Funcion que llama a una metrica en particular y luego la grafica
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_display( int id )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( pM->id == id) {
			interfaz_gfx( pM );
			return STATUS_SUCCESS;
		}
		l = LIST_NEXT(l);
	}
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS metric_setup( PMETRIC pM )
 * \brief Inicializa una metrica con los valores por default
 * \param pM puntero a la metrica
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS metric_setup( PMETRIC pM )
{
	static int id=0;
	assert( pM );
	strcpy( pM->nombre,"default");
	pM->id=id++;
	pM->min=1000.0;
	pM->max=0.0;
	pM->cur=0.0;
	pM->prom=0.0;
	pM->attrib=0;
	pM->scale_y=0.0;
	pM->attrib=0;
	pM->update_fn=NULL;

	return STATUS_SUCCESS;
}

/**
 * Devuelve un puntero a la metrica pedida.
 * @param id Identificador de la m�trica buscada.
 * @returns Un puntero a la m�trica si se la encuentra, o \c NULL en caso
 * contrario.
 * \author Quesada Ricardo.
 * \author Picorelli Marcelo.
 */
PMETRIC metric_get( int id )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( pM->id == id) {
			return pM;
		}
		l = LIST_NEXT(l);
	}
	return NULL;
}

/**
 * Devuelve el \c id de la m�trica que se le pide.
 * @param cpMetric El nombre de la metrica que se est� buscando, no puede ser
 * \c NULL.
 * @returns El \c id de la m�trica, o -1 de no encontrarla.
 * \author Quesada Ricardo.
 * \author Picorelli Marcelo.
 */
int metric_id_get( char *cpMetric )
{
	PMETRIC pM;
	PLIST_ENTRY l = g_metric_list.next;

	while( !IsListEmpty( &g_metric_list ) && (l != &g_metric_list) ) {
		pM = (PMETRIC) l;
		if( !strcmp( pM->nombre, cpMetric ) )
		{
			return pM->id;
		}
		l = LIST_NEXT(l);
	}
	return -1;
}
