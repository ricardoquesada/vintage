/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <curses.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include "cons.h"

/**
 * \file cons.c
 * cons.
 * Provee las funciones necesarias para trabajar con los comandos de la
 * 'consola', incluyendo la inicializacion, el agregado y la destrucci�n de
 * los comandos. Estos comandos son din�micos, es decir el vector que los
 * contiene crece a medida que se necesita (anque en este momento no se
 * proveen mecanismos para eliminar comandos en forma particular).
 * La idea de realizar un vector con los comandos tiene por objectivo el
 * evitar realizar decenas de l�neas de c�digo realizando consultas para
 * saber qu� funci�n ejecutar dado un determinado comando.
 * La estructura creada ademas brinda un mecanismo muy simple de pasaje de
 * par�metros y la posibilidad de agregar una ayuda limitada al usuario
 * mediante la utilizacion del campo \c cpHelp.
 * Toda funci�n que se agregue debe aceptar como par�metro un \c char \c *
 * que es el valor del comando (ingresado por el usuario).
 * \author Picorelli Marcelo.
 * \date 31/03/2000
 * \version 0.01
 */
/***********************************************************************/

/**
 * Inicializa la estructura.
 * \author Picorelli Marcelo.
 */
TConsoleBind **cons_new( void )
{
	TConsoleBind **tmp;

	tmp = (TConsoleBind **)malloc( sizeof( TConsoleBind *) );
	tmp[0] = NULL;

	return tmp;
}

/**
 * Busca el comando pedido en el vector de comandos.
 * @param pvComms El vector en donde se buscara el comando, no puede ser \c
 * NULL.
 * @param cpCommName El nombre del comando a buscar, no puede ser \c NULL.
 * @returns Un puntero a la estructura del comando, o \c NULL si no lo
 * encuentra.
 */
TConsoleBind *cons_command_get( TConsoleBind **pvComms, char *cpCommName )
{
	int i;

	assert( pvComms != NULL );
	assert( cpCommName != NULL );

	/* Avanzo hasta el final, o hasta encontrar el cpCommName */
	for(i = 0; pvComms[i] && strcmp( pvComms[i]->cpCommand, cpCommName ); i++ );

	return pvComms[i];
}


/**
 * Agrega un comando.
 * Busca el final del vector, o la posicion del comando -seg�n lo que ocurra
 * primero-; si llega al final del vector, aumenta el tama�o del mismo y
 * agrega el comando, por otro lado, si encuentra el nombre del comando,
 * reemplaza la funcion anterior por la nueva funcion.
 * El nombre del comando es guardado, por lo que el mismo puede ser liberado
 * luego de llamar a esta funci�n.
 * @param pvComms El vector de comandos, no puede ser \c NULL.
 * @param cpCommName El nombre del comando a agregar(reemplazar), no puede ser
 * \c NULL.
 * @param func La funcion asociada al comando, no puede ser \c NULL.
 * @returns 1 -\b SIEMPRE-
 * \bug Mejorar el c�digo de \c return.
 * \author Picorelli Marcelo.
 */
int cons_command_add( TConsoleBind ***pvComms, char *cpCommName,
	char *cpHelp, int (* func)(), void *pUserData )
{
	int i;
	TConsoleBind *tmp = NULL;
	TConsoleBind **pvTmp = *pvComms;

	assert( pvComms != NULL );
	assert( cpCommName != NULL );
	assert( func != NULL );

	/* Avanzo hasta el final, o hasta encontrar el cpCommName */
	for(i = 0; pvTmp[i] && strcmp( pvTmp[i]->cpCommand, cpCommName ); i++ );

	/* Llegue al final ? */
	if( !pvTmp[i] )
		pvTmp = ( TConsoleBind **)realloc( (TConsoleBind**) pvTmp,
			sizeof( TConsoleBind * ) * (i+2) );

	/* Creo el item y lo agrego al vector */
	tmp = (TConsoleBind *)malloc( sizeof( TConsoleBind ) );
	tmp->cpCommand = strdup( cpCommName );
	tmp->cpHelp = cpHelp ? strdup( cpHelp ) : NULL;
	tmp->ipExecute = func;
	tmp->pUserData = pUserData;

	pvTmp[i] = tmp;
	pvTmp[i+1] = NULL;

	*pvComms = pvTmp;

	return 1;
}

/**
 * Ejecuta un comando.
 * Busca el nombre del comando, si lo encuentra ejecuta la acci�n
 * correspondiente.
 * @param pvComms El vector de comandos, no puede ser \c NULL.
 * @param cpCommName El nombre del comando a ejecutar, no puede ser \c NULL.
 * @param cpArg El argumento de la l�nea de comando, provisto por la
 * aplicaci�n, no por el programador.
 * @returns 1 Si pudo ejecutar el comando, 0 en otro caso.
 * \bug Mejorar el manejo de errores.
 * \author Picorelli Marcelo.
 */
int cons_command_execute( TConsoleBind **pvComms, char *cpCommName,
	char *cpArg )
{
	int i;

	assert( pvComms != NULL );
	assert( cpCommName != NULL );

	/* Avanzo hasta el final, o hasta encontrar el cpCommName */
	for(i = 0; pvComms[i] && strcmp( pvComms[i]->cpCommand, cpCommName ); i++ );

	if( pvComms[i] )
	{
		return (* pvComms[i]->ipExecute)( cpArg, pvComms[i]->pUserData );
	}

	return 0;
}

/**
 * Elimina el vector de comandos.
 * Recorre el vector eliminando los componentes del mismo.
 * @param pvComms El vector de comandos, no puede ser \c NULL.
 * \author Picorelli Marcelo.
 */
void cons_destroy( TConsoleBind **pvComms )
{
	int i;
	assert( pvComms != NULL );

	/* Avanzo hasta el final */
	for( i = 0; pvComms[i]; i++ )
	{
		free( pvComms[i]->cpCommand );
		free( pvComms[i]->cpHelp );
		free( pvComms[i] );
	}
	free( pvComms );
}
