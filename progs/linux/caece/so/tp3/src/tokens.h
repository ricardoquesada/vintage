#ifndef __TOKENS_H
#define __TOKENS_H

#include "sched.h"

STATUS token_nill( PPCB pPcb );
STATUS token_exit( PPCB pPcb );
STATUS token_fork( PPCB pPcb );
STATUS token_read( PPCB pPcb );
STATUS token_write( PPCB pPcb );
STATUS token_open( PPCB pPcb );
STATUS token_close( PPCB pPcb );
STATUS token_wait( PPCB pPcb );
STATUS token_burst( PPCB pPcb );
STATUS token_lda( PPCB pPcb );
STATUS token_jz( PPCB pPcb );
STATUS token_jnz( PPCB pPcb );
STATUS token_inca( PPCB pPcb );
STATUS token_deca( PPCB pPcb );
STATUS token_jump( PPCB pPcb );

#endif /* __TOKENS_H */
