/*
 * CAECE
 * Sistemas Operativos
 * TP3
 * Grupo MRM
 */
/**
 * \file tokens.c
 * Valores de los tokens
 * \author Ricardo Quesada
 */

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tipos.h"
#include "tokens.h"
#include "events.h"
#include "interfaz.h"

/**
 * \fn STATUS token_nill( PPCB pPcb )
 * \brief Implementacion de 'nop'
 * \author Ricardo Quesada
 */
STATUS token_nill( PPCB pPcb )
{
	pPcb->p_ip++; 
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_exit( PPCB pPcb )
 * \brief Implementacion de 'exit'
 * \author Ricardo Quesada
 */
STATUS token_exit( PPCB pPcb )
{
	int tardo;
	long p;

	tardo = jiffies - pPcb->p_t_run_start;
	p = (tardo * 100 )/ pPcb->p_cpu_usada;

	console_out("'%s' tardo %d ciclos, y era de %d. (%d%%)\n",pPcb->p_nombre
			,tardo
			,pPcb->p_cpu_usada
			,p
			);
	pPcb->p_ip = pPcb->p_ip + 2;
	return STATUS_END;
}

/**
 * \fn STATUS token_fork( PPCB pPcb )
 * \brief Implementacion de 'fork'
 * \author Ricardo Quesada
 */
STATUS token_fork( PPCB pPcb )
{
	pPcb->p_ip = pPcb->p_ip + 2;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_read( PPCB pPcb )
 * \brief Implementacion de 'read a b' (a=Dispositivo b=unidades a leer)
 * \author Ricardo Quesada
 */
STATUS token_read( PPCB pPcb )
{
	int dsp = (int)pPcb->p_codeseg[ pPcb->p_ip +1 ];
	int tot = (int)pPcb->p_codeseg[ pPcb->p_ip +2 ];

	wait_event( pPcb, dsp*2, tot );

	pPcb->p_ip = pPcb->p_ip + 3;
	return STATUS_EVENT;
}

/**
 * \fn STATUS token_write( PPCB pPcb )
 * \brief Implementacion de 'write a b' (a=Dispositivo b=unidades a escribir)
 * \author Ricardo Quesada
 */
STATUS token_write( PPCB pPcb )
{
	int dsp = (int)pPcb->p_codeseg[ pPcb->p_ip +1 ];
	int tot = (int)pPcb->p_codeseg[ pPcb->p_ip +2 ];

	wait_event( pPcb, dsp*2 + 1, tot );

	pPcb->p_ip = pPcb->p_ip + 3;
	return STATUS_EVENT;
}

/**
 * \fn STATUS token_open( PPCB pPcb )
 * \brief Implementacion de 'open'
 * \author Ricardo Quesada
 */
STATUS token_open( PPCB pPcb )
{
	pPcb->p_ip = pPcb->p_ip + 2;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_close( PPCB pPcb )
 * \brief Implementacion de 'close'
 * \author Ricardo Quesada
 */
STATUS token_close( PPCB pPcb )
{
	pPcb->p_ip = pPcb->p_ip + 2;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_wait( PPCB pPcb )
 * \brief Implementacion de 'wait'
 * \author Ricardo Quesada
 */
STATUS token_wait( PPCB pPcb )
{
	char event = pPcb->p_codeseg[ pPcb->p_ip +1 ];

	pPcb->p_ip = pPcb->p_ip + 2;
	wait_event( pPcb, (int) event, 1 );
	return STATUS_EVENT;
}

/**
 * \fn STATUS token_burst( PPCB pPcb )
 * \brief Implementacion de 'burst'
 * \author Ricardo Quesada
 */
STATUS token_burst( PPCB pPcb )
{
	if( pPcb->p_codeseg[ pPcb->p_ip +1 ] == 0 )
		pPcb->p_ip = pPcb->p_ip + 2;
	else
		pPcb->p_codeseg[ pPcb->p_ip+1 ]--;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_lda( PPCB pPcb )
 * \brief Implementacion de 'lda'(load acummulator)
 * \author Ricardo Quesada
 */
STATUS token_lda( PPCB pPcb )
{
	pPcb->p_eax = (int) pPcb->p_codeseg[ pPcb->p_ip +1 ];
	pPcb->p_ip = pPcb->p_ip + 2;

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_jz( PPCB pPcb )
 * \brief Implementacion de 'jz' (jump if zero)
 * \author Ricardo Quesada
 */
STATUS token_jz( PPCB pPcb )
{
	char where = pPcb->p_codeseg[ pPcb->p_ip +1 ];

	if( pPcb->p_eax == 0)
		pPcb->p_ip = pPcb->p_ip + where;
	else
		pPcb->p_ip = pPcb->p_ip + 2;

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_jnz( PPCB pPcb )
 * \brief Implementacion de 'jnz' (jump if no zero)
 * \author Ricardo Quesada
 */
STATUS token_jnz( PPCB pPcb )
{
	char where = pPcb->p_codeseg[ pPcb->p_ip +1 ];

	if( pPcb->p_eax != 0)
		pPcb->p_ip = pPcb->p_ip + where;
	else
		pPcb->p_ip = pPcb->p_ip + 2;

	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_inca( PPCB pPcb )
 * \brief Implementacion de 'inca' (inc acumulator)
 * \author Ricardo Quesada
 */
STATUS token_inca( PPCB pPcb )
{
	pPcb->p_eax++;
	pPcb->p_ip++;

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_deca( PPCB pPcb )
 * \brief Implementacion de 'deca' (dec acumulator)
 * \author Ricardo Quesada
 */
STATUS token_deca( PPCB pPcb )
{
	pPcb->p_eax--;
	pPcb->p_ip++;

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_jump( PPCB pPcb )
 * \brief Implementacion de 'deca' (dec acumulator)
 * \author Ricardo Quesada
 */
STATUS token_jump( PPCB pPcb )
{
	pPcb->p_ip = pPcb->p_ip + pPcb->p_codeseg[ pPcb->p_ip +1 ];
	return STATUS_SUCCESS;
}
