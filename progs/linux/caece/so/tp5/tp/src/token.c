/*
 * Trabajo Practico Numero 4.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "interfaz.h"
#include "load.h"
#include "token.h"
#include "tokens.h"
#include "sem.h"
#include "label.h"

#undef TOKEN_DEBUG
/**
 * \file token.c
 * pseudo 'JIT' Just In Time compiler.
 * Cuando se carga el programa, se lo compila a 'bytecode', 
 * y eso es lo que se guarda en memoria, y se ejecuta.
 * \author Ricardo Quesada
 */

struct _tokens {
	char *nombre;		/**< nombre del token */
	int params;		/**< parametros que recibe */
	char bytecode;		/**< codigo del bytecode */
	STATUS (*func)();	/**< funcion a llamar */
	STATUS (*pre)();	/**< funcion que hace algo antes de la compilacion */
} tokens[] = {
	/* Para que la ejecucion sea 'optimizada' hacer que el bytecode '0'
	 * este en el indice 0, el '1' en el 1, etc */

	{TOKEN_LDB	,3,0x80, token_ldb, NULL },
	{NULL		,3,0x81, token_ldbi, NULL },
	{TOKEN_LDW	,3,0x80, token_ldw, NULL },
	{NULL		,3,0x81, token_ldwi, NULL },
	{TOKEN_LDA	,3,0x22, token_add, NULL },
	{NULL		,3,0x23, token_addi, NULL },
	{TOKEN_STB	,3,0x80, token_stb, NULL },
	{NULL		,3,0x81, token_stbi, NULL },
	{TOKEN_STW	,3,0x84, token_stw, NULL },
	{NULL		,3,0x85, token_stwi, NULL },
	{TOKEN_ADD	,3,0x84, token_add, NULL },
	{NULL		,3,0x85, token_addi, NULL },
	{TOKEN_SUB	,3,0x85, token_sub, NULL },
	{NULL		,3,0x85, token_subi, NULL },
	{TOKEN_MUL	,3,0x85, token_mul, NULL },
	{NULL		,3,0x85, token_muli, NULL },
	{TOKEN_DIV	,3,0x85, token_div, NULL },
	{NULL		,3,0x85, token_divi, NULL },
	{TOKEN_NEG	,3,0x85, token_neg, NULL },
	{NULL		,3,0x85, token_negi, NULL },
	{TOKEN_SL	,3,0x85, token_sl, NULL },
	{NULL		,3,0x85, token_sli, NULL },
	{TOKEN_SR	,3,0x85, token_sr, NULL },
	{NULL		,3,0x85, token_sri, NULL },
	{TOKEN_CMP	,3,0x85, token_cmp, NULL },
	{NULL		,3,0x85, token_cmpi, NULL },

	{TOKEN_BN	,2,0x85, token_bn, pre_branch },
	{NULL		,2,0x85, token_bnb, pre_branch },
	{TOKEN_BZ	,2,0x85, token_bz, pre_branch },
	{NULL		,2,0x85, token_bzb, pre_branch },
	{TOKEN_BP	,2,0x85, token_bp, pre_branch },
	{NULL		,2,0x85, token_bpb, pre_branch },
	{TOKEN_BOD	,2,0x85, token_bod, pre_branch },
	{NULL		,2,0x85, token_bodb, pre_branch },
	{TOKEN_BNN	,2,0x85, token_bnn, pre_branch },
	{NULL		,2,0x85, token_bnnb, pre_branch },
	{TOKEN_BNZ	,2,0x85, token_bnz, pre_branch },
	{NULL		,2,0x85, token_bnzb, pre_branch },
	{TOKEN_BNP	,2,0x85, token_bnp, pre_branch },
	{NULL		,2,0x85, token_bnpb, pre_branch },
	{TOKEN_BEV	,2,0x85, token_bev, pre_branch },
	{NULL		,2,0x85, token_bevb, pre_branch },
	{TOKEN_PBN	,2,0x85, token_bn, pre_branch },
	{NULL		,2,0x85, token_bnb, pre_branch },
	{TOKEN_PBZ	,2,0x85, token_bz, pre_branch },
	{NULL		,2,0x85, token_bzb, pre_branch },
	{TOKEN_PBP	,2,0x85, token_bp, pre_branch },
	{NULL		,2,0x85, token_bpb, pre_branch },
	{TOKEN_PBOD	,2,0x85, token_bod, pre_branch },
	{NULL		,2,0x85, token_bodb, pre_branch },
	{TOKEN_PBNN	,2,0x85, token_bnn, pre_branch },
	{NULL		,2,0x85, token_bnnb, pre_branch },
	{TOKEN_PBNZ	,2,0x85, token_bnz, pre_branch },
	{NULL		,2,0x85, token_bnzb, pre_branch },
	{TOKEN_PBNP	,2,0x85, token_bnp, pre_branch },
	{NULL		,2,0x85, token_bnpb, pre_branch },
	{TOKEN_PBEV	,2,0x85, token_bev, pre_branch },
	{NULL		,2,0x85, token_bevb, pre_branch },

	{TOKEN_SETH	,2,0x85, token_seth, NULL },
	{TOKEN_SETMH	,2,0x85, token_setmh, NULL },
	{TOKEN_SETML	,2,0x85, token_setml, NULL },
	{TOKEN_SETL	,2,0x85, token_setl, NULL },
	{TOKEN_INCH	,2,0x85, token_inch, NULL },
	{TOKEN_INCMH	,2,0x85, token_incmh, NULL },
	{TOKEN_INCML	,2,0x85, token_incml, NULL },
	{TOKEN_INCL	,2,0x85, token_incl, NULL },
	{TOKEN_JMP	,1,0x85, token_jmp, pre_jump },
	{NULL		,1,0x85, token_jmpb, pre_jump },
	{TOKEN_GO	,3,0x85, token_go, NULL },
	{NULL		,3,0x85, token_goi, NULL },
	{TOKEN_TRAP	,3,0x85, token_trap, pre_trap },
	{TOKEN_AND	,3,0x85, token_and, NULL },
	{NULL		,3,0x85, token_andi, NULL },
	{TOKEN_OR	,3,0x85, token_or, NULL },
	{NULL		,3,0x85, token_ori, NULL },
	{TOKEN_XOR	,3,0x85, token_xor, NULL },
	{NULL		,3,0x85, token_xori, NULL },
	{TOKEN_ANDN	,3,0x85, token_andn, NULL },
	{NULL		,3,0x85, token_andni, NULL },
	{TOKEN_ORN	,3,0x85, token_orn, NULL },
	{NULL		,3,0x85, token_orni, NULL },
	{TOKEN_NAND	,3,0x85, token_nand, NULL },
	{NULL		,3,0x85, token_nandi, NULL },
	{TOKEN_NOR	,3,0x85, token_nor, NULL },
	{NULL		,3,0x85, token_nori, NULL },
	{TOKEN_NXOR	,3,0x85, token_nxor, NULL },
	{NULL		,3,0x85, token_nxori, NULL },
	{TOKEN_2ADD	,3,0x85, token_2add, NULL },
	{NULL		,3,0x85, token_2addi, NULL },
	{TOKEN_4ADD	,3,0x85, token_4add, NULL },
	{NULL		,3,0x85, token_4addi, NULL },
	{TOKEN_8ADD	,3,0x85, token_8add, NULL },
	{NULL		,3,0x85, token_8addi, NULL },
	{TOKEN_16ADD	,3,0x85, token_16add, NULL },
	{NULL		,3,0x85, token_16addi, NULL },
};
#define	NRTOKENS  (sizeof(tokens)/sizeof(tokens[0]))


void pre_generic( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int *tv )
{
	if( s1 && strlen(s1) != 0) {
		if( s1[0]=='$' )
			*i1 = atoi( &s1[1] );
		else
			*i1 = atoi( s1 ); 
	}

	if( s2 && strlen(s2) != 0) {
		if( s2[0]=='$' )
			*i2 = atoi( &s2[1] );
		else
			*i2 = atoi( s2 ); 
	}

	if( s3 && strlen(s3) != 0) {
		if( s3[0]=='$' )
			*i3 = atoi( &s3[1] );
		else {
			(*tv)++;		/* Es inmmdiate */
			*i3 = atoi( s3 ); 
		}
	}
}

/**
 * \fn static STATUS get_specific( FILE *fp )
 * \brief Evalua codigo especifico del interprete
 * \author Ricardo Quesada
 */
static STATUS get_specific( FILE *fp )
{
	int r;
	char c;
	char quesoy[1000];

	PDEBUG("get_specific()");

read_loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	r=fread(  &c, 1 ,1, fp);
	if(r!=1 || c=='\n' || c==' ' || c=='\t')
		goto read_loop;

	/* Es un comando especial? */
	if( c=='.') {
		fscanf( fp,"%s\n", quesoy );
		if(strcasecmp( quesoy,"end")==0)
			return STATUS_END;
		else
			return STATUS_ERROR;
	}

	/* Es un REM? */
	if( c=='!' ) {
		while(c!='\n')
			fread(  &c, 1 ,1, fp);
		goto read_loop;
	}

	/* Es un label ?*/
	if( c=='@')
		return STATUS_LABEL;

	fseek( fp, -1 ,SEEK_CUR);

	/* Entonces es un mnemonico */
	return STATUS_COMMAND;
}

/**
 * \fn static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
 * \brief Convierte un string a un bytecode
 * \author Ricardo Quesada
 */
static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
{
	char buffer[1000];
	char token[100];
	char param1[100];
	char param2[100];
	char param3[100];
	int  iparam1;
	int  iparam2;
	int  iparam3;
	int i;
	int j;

	PDEBUG("convert_2_bytecode()");

	/*
	 * Dado que hay un bug horrible en libc hago esto para evitarlo
	 * 1ro un readline y luego un sscanf en vez de un fscanf
	 */

	memset(buffer,0,sizeof(buffer));
	{
		char c;
		int ii=0;
		
		do {
			fread(  &c, 1 ,1, fp);
			buffer[ii++]=c;
		} while( c !='\n');
	}

	memset( param1,0,sizeof(param1));
	memset( param2,0,sizeof(param2));
	memset( param3,0,sizeof(param3));

	j = sscanf(buffer, "%s %s , %s , %s\n",token,param1,param2,param3);

#ifdef TOKEN_DEBUG
	fprintf(stderr,"Bytecompiling '%s' '%d'\n",token,j);
#endif
	for(i = 0; i < NRTOKENS; i++) {
		if( tokens[i].nombre && strlen(token)==strlen(tokens[i].nombre) && 
			strncasecmp( tokens[i].nombre, token, strlen( tokens[i].nombre) )==0 ){

			int token_value;

#ifdef WITH_MMIX_FULL_COMPATIBILITY
			token_value = tokens[i].bytecode;
#else
			token_value = i;
#endif
			if( tokens[i].pre )
				tokens[i].pre( param1, &iparam1, param2, &iparam2, param3, &iparam3, *indice, &token_value );
			else {
				pre_generic( param1, &iparam1, param2, &iparam2, param3, &iparam3, &token_value );
			}

			pPcb->p_codeseg[(*indice)++] = token_value;

			if( tokens[i].params == 1 )  {
				pPcb->p_codeseg[(*indice)++] = (char)((iparam1 & 0x00ff0000)>>16);
				pPcb->p_codeseg[(*indice)++] = (char)((iparam1 & 0x0000ff00)>> 8);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x000000ff);
			}

			else if( tokens[i].params == 2 ) {
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x000000ff);
				pPcb->p_codeseg[(*indice)++] = (char)((iparam2 & 0x0000ff00)>>8);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam2 & 0x000000ff);
			}

			else if( tokens[i].params == 3 ) {
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x000000ff);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam2 & 0x000000ff);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam3 & 0x000000ff);
			}

			else return STATUS_UNEXPECTED;

			return STATUS_SUCCESS;
		}
	}
	console_out("Error en %s: Token '%s' no encontrado\n",pPcb->p_nombre,token);
	return STATUS_ERROR;
}

/**
 * \fn STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
 * \brief Funcion que se encarga de convertir a 'bytecodes' un programa
 * \author Ricardo Quesada
 */
STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
{
	int indice=0;

	PDEBUG("tokenize_to_mem()");


	label_init();
loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	/* es algun valor del interprete */
	switch( get_specific( fp )) {
		case STATUS_END:
			label_deinit();
			return STATUS_SUCCESS;
		case STATUS_FILEEOF:
			console_out("Error: EOF\n");
			label_deinit();
			return STATUS_FILEEOF;
		case STATUS_LABEL:
			label_add( fp, indice );
			break;
		case STATUS_COMMAND:
			if( convert_2_bytecode( pPcb, fp, &indice ) != STATUS_SUCCESS ){
				label_deinit();
				return STATUS_ERROR;
			}
			break;
		default:
			break;
	}
	goto loop;
}

/**
 * \fn STATUS run_proc( PPCB pPcb )
 * \brief Hace 'andar' los procesos
 * \author Ricardo Quesada
 */
STATUS run_proc( PPCB pPcb )
{
	char t = pPcb->p_codeseg[pPcb->p_ip];

	pPcb->p_cpu_usada++;

	if( t >= NRTOKENS ) {
		console_out("Instruccion invalida en: %s\n",pPcb->p_nombre);
		fprintf(stderr,"%s %d\n",pPcb->p_nombre, (int)pPcb->p_ip);
		return STATUS_INVALID;
	}

	return (tokens[(int)t].func)(pPcb);
}
