/*
 * CAECE
 * Sistemas Operativos
 * TP5
 * Grupo MRM
 */
/**
 * \file tokens.c
 * Valores de los tokens, basados en MMIX (Millenium MIX)
 * El assembler es muy implementacion muy limitada del MMIX,
 * no cuenta con muchas de las funciones, pero tiene las
 * necesarias como para hacer alguna demo.
 * \author Ricardo Quesada
 */

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tipos.h"
#include "tokens.h"
#include "events.h"
#include "interfaz.h"
#include "sem.h"
#include "sched.h"


#define INC_IP(a) (pPcb->p_ip = pPcb->p_ip + (a))
#define LOAD_X(x) ((x) = pPcb->p_codeseg[ pPcb->p_ip +1 ])
#define LOAD_Y(y) ((y) = pPcb->p_codeseg[ pPcb->p_ip +2 ])
#define LOAD_Z(z) ((z) = pPcb->p_codeseg[ pPcb->p_ip +3 ])
#define LOAD_XYZ(x,y,z) LOAD_X(x);LOAD_Y(y);LOAD_Z(z);


struct _syscalls {
	char *nombre;			/**< nombre de la syscall */
	STATUS (*funcion)();		/**< funcion de la syscall */
	STATUS (*pre)();		/**< funcion pre de la syscall */
} syscalls[] = {
	{"exit", syscall_exit, NULL },
	{"signal", syscall_signal, pre_sem },
	{"wait", syscall_wait, pre_sem },
	{"read", syscall_read, NULL },
	{"write", syscall_write, NULL},
	{"kill", syscall_kill, NULL}, 
};
#define NRSYSCALLS ( sizeof(syscalls) /sizeof(syscalls[0]) )

/**
 * \fn STATUS syscall_exit( PPCB pPcb )
 * \brief Implementacion de 'exit'
 */
STATUS syscall_exit( PPCB pPcb )
{
	int tardo;
	long p;

	tardo = jiffies - pPcb->p_t_run_start;
	p = (tardo * 100 )/ pPcb->p_cpu_usada;

	console_out("'%s' tardo %d ciclos, y era de %d. (%d%%)\n",pPcb->p_nombre
			,tardo
			,pPcb->p_cpu_usada
			,p
			);
	return STATUS_END;
}


/**
 * \fn STATUS syscall_read( PPCB pPcb )
 * \brief Implementacion de 'read a b' (a=Dispositivo b=unidades a leer)
 */
STATUS syscall_read( PPCB pPcb )
{
	int x,dsp,tot;

	LOAD_XYZ(x,dsp,tot);

	wait_event( pPcb, dsp*2, tot );

	return STATUS_EVENT;
}

/**
 * \fn STATUS syscall_write( PPCB pPcb )
 * \brief Implementacion de 'write a b' (a=Dispositivo b=unidades a escribir)
 */
STATUS syscall_write( PPCB pPcb )
{
	int x,dsp,tot;

	LOAD_XYZ(x,dsp,tot);

	wait_event( pPcb, dsp*2 + 1, tot );

	return STATUS_EVENT;
}

/**
 * \fn STATUS syscall_wait( PPCB pPcb )
 * \brief Implementacion de 'wait' (semaforo)
 */
STATUS syscall_wait( PPCB pPcb )
{
	STATUS s;
	int id;

	LOAD_Y(id);

	if( (s=sem_wait( id, pPcb )) == STATUS_ERROR ) {
		console_out("Proceso '%s' causo un error usando token_wait\n",pPcb->p_nombre);
		return STATUS_INVALID;
	}
	return s;
}

/**
 * \fn STATUS syscall_signal( PPCB pPcb )
 * \brief Implementacion 'signal' (semaforo)
 */
STATUS syscall_signal( PPCB pPcb )
{
	int id;

	LOAD_Y(id);

	if( sem_signal( id, pPcb ) == STATUS_SUCCESS ) {
		return STATUS_SUCCESS;
	}

	console_out("Proceso '%s' causo un error usando token_signal\n",pPcb->p_nombre);
	return STATUS_INVALID;
}

/**
 * \fn STATUS syscall_kill( PPCB pPcb )
 * \brief Mata a algun proceso
 */
STATUS syscall_kill( PPCB pPcb )
{
	int id;

	LOAD_Y(id);
	kill_procid( id );
	return STATUS_SUCCESS;
}

/******************************************************/

/* * **** LOAD **** */
/**
 * \fn STATUS token_ldb( PPCB pPcb )
 * \brief load byte
 */
STATUS token_ldb( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = pPcb->p_codeseg[ r ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_ldbi( PPCB pPcb )
 * \brief load byte inmmediate
 */
STATUS token_ldbi( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = pPcb->p_codeseg[ r ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_ldw( PPCB pPcb )
 * \brief load wyde
 */
STATUS token_ldw( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_mmix.reg[(int)x] = pPcb->p_codeseg[ r ] * 256 + pPcb->p_codeseg[r + 1 ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_ldwi( PPCB pPcb )
 * \brief load wyde inmmediate.
 */
STATUS token_ldwi( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + z;

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_mmix.reg[(int)x] = pPcb->p_codeseg[ r ] * 256 + pPcb->p_codeseg[r + 1 ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_lda( PPCB pPcb )
 * \brief load address.
 * \brief Es lo mismo que token_add
 */

/**
 * \fn STATUS token_ldai( PPCB pPcb )
 * \brief load address inmmediate.
 * \brief Es lo mismo que token_addi
 */


/* * **** STORE **** */
/**
 * \fn STATUS token_stb( PPCB pPcb )
 * \brief store byte.
 */
STATUS token_stb( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_codeseg[ r ] = pPcb->p_mmix.reg[(int)x];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_stbi( PPCB pPcb )
 * \brief store byte inmmediate.
 */
STATUS token_stbi( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_codeseg[ r ] = pPcb->p_mmix.reg[(int)x];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_stwi( PPCB pPcb )
 * \brief store wyde.
 */
STATUS token_stw( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_codeseg[ r ] = (pPcb->p_mmix.reg[(int)x] >> 8);
	pPcb->p_codeseg[ r+1 ] = (pPcb->p_mmix.reg[(int)x] && 0x000000ff);
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_stwi( PPCB pPcb )
 * \brief store wyde inmmediate.
 */
STATUS token_stwi( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + z;

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_codeseg[ r ] = (pPcb->p_mmix.reg[(int)x] >> 8);
	pPcb->p_codeseg[ r+1 ] = (pPcb->p_mmix.reg[(int)x] && 0x000000ff);
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/* * **** ARITHMETIC **** */
/**
 * \fn STATUS token_add( PPCB pPcb )
 * \brief add registers.
 */
STATUS token_add( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_addi( PPCB pPcb )
 * \brief add registers inmmediate.
 */
STATUS token_addi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_2add( PPCB pPcb )
 * \brief 2add registers
 */
STATUS token_2add( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 2*pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_2addi( PPCB pPcb )
 * \brief 2add registers inmmediate.
 */
STATUS token_2addi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 2*pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_4add( PPCB pPcb )
 * \brief 4 times add registers
 */
STATUS token_4add( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 4*pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_4addi( PPCB pPcb )
 * \brief 4add registers inmmediate.
 */
STATUS token_4addi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 4*pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_8add( PPCB pPcb )
 * \brief 8 times add registers
 */
STATUS token_8add( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 8*pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_8addi( PPCB pPcb )
 * \brief 8add registers inmmediate.
 */
STATUS token_8addi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 8*pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}
/**
 * \fn STATUS token_16add( PPCB pPcb )
 * \brief 16 times add registers
 */
STATUS token_16add( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 16*pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_16addi( PPCB pPcb )
 * \brief 16add registers inmmediate.
 */
STATUS token_16addi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = 16*pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief sub registers.
 */
STATUS token_sub( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] - pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief sub registers inmmediate.
 */
STATUS token_subi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] - z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief mul registers.
 */
STATUS token_mul( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] * pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief mul registers inmmediate.
 */
STATUS token_muli( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] * z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_div( PPCB pPcb )
 * \brief div registers.
 */
STATUS token_div( PPCB pPcb )
{
	signed long long r,rR;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] / pPcb->p_mmix.reg[(int)z];
	rR = pPcb->p_mmix.reg[(int)y] % pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	pPcb->p_mmix.special_reg[REG_rR] = rR;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_divi( PPCB pPcb )
 * \brief mul registers inmmediate.
 */
STATUS token_divi( PPCB pPcb )
{
	signed long long r,rR;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] * z;


	r = pPcb->p_mmix.reg[(int)y] / pPcb->p_mmix.reg[(int)z];
	rR = pPcb->p_mmix.reg[(int)y] % pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	pPcb->p_mmix.special_reg[REG_rR] = rR;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_neg( PPCB pPcb )
 * \brief negetate registers.
 */
STATUS token_neg( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = y - pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief negetate registers inmmediate
 */
STATUS token_negi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = y - z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sl( PPCB pPcb )
 * \brief shift left registers.
 */
STATUS token_sl( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] << pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief shift left inmmediate
 */
STATUS token_sli( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] << z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sl( PPCB pPcb )
 * \brief shift right registers.
 */
STATUS token_sr( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] >> pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief shift right inmmediate
 */
STATUS token_sri( PPCB pPcb )
{
	unsigned long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] >> z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_cmp( PPCB pPcb )
 * \brief compare registers.
 */
STATUS token_cmp( PPCB pPcb )
{
	int r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (pPcb->p_mmix.reg[(int)y] > pPcb->p_mmix.reg[(int)z]) - (pPcb->p_mmix.reg[(int)z] > pPcb->p_mmix.reg[(int)y]);

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief compare inmmediate
 */
STATUS token_cmpi( PPCB pPcb )
{
	int r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (pPcb->p_mmix.reg[(int)y] > z) - (z > pPcb->p_mmix.reg[(int)y]);

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/* **** * INMMEDIATE * ****/

/**
 * \fn STATUS token_seth( PPCB pPcb )
 * \brief set high wyde
 */
STATUS token_seth( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	y = (y<<8) | z;

	r = y << 16;
	r <<= 16;
	r <<= 16;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_setmh( PPCB pPcb )
 * \brief set medium high wyde
 */
STATUS token_setmh( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	y = (y<<8) | z;

	r = y << 16;
	r <<= 16;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_setml( PPCB pPcb )
 * \brief set medium low wyde
 */
STATUS token_setml( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	y = (y<<8) | z;

	r = y << 16;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_setl( PPCB pPcb )
 * \brief set low wyde
 */
STATUS token_setl( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z;

	pPcb->p_mmix.reg[(int)x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_inch( PPCB pPcb )
 * \brief increment high wyde
 */
STATUS token_inch( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	y = (y<<8) | z;

	r = y << 16;
	r = y << 16;
	r = y << 16;

	pPcb->p_mmix.reg[(int)x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_incmh( PPCB pPcb )
 * \brief increment medium high wyde
 */
STATUS token_incmh( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	y = (y << 8) | z;

	r = y << 16;
	r = y << 16;

	pPcb->p_mmix.reg[(int)x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_incml( PPCB pPcb )
 * \brief increment medium low wyde
 */
STATUS token_incml( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	y = (y <<8) | z;

	r = y << 16;

	pPcb->p_mmix.reg[(int)x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_incl( PPCB pPcb )
 * \brief increment low wyde
 */
STATUS token_incl( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z;

	pPcb->p_mmix.reg[(int)x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**** * JUMP * ****/

/**
 * \fn STATUS token_jmp( PPCB pPcb )
 * \brief jump relative address
 */
STATUS token_jmp( PPCB pPcb )
{
	int r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (x<<16) | (y<<8) | z ;

	pPcb->p_ip += (long) r;
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_jmpb( PPCB pPcb )
 * \brief jump relative address backwards
 */
STATUS token_jmpb( PPCB pPcb )
{
	int r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (x<<16) | (y<<8) | z ;

	pPcb->p_ip -= (long) r;
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_go( PPCB pPcb )
 * \brief jump absolute address
 */
STATUS token_go( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + pPcb->p_mmix.reg[(int)z];

	pPcb->p_mmix.reg[(int)x] = pPcb->p_ip + 4;

	pPcb->p_ip = (long) r;
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_goi( PPCB pPcb )
 * \brief jump absolute address inmmediate
 */
STATUS token_goi( PPCB pPcb )
{
	signed long long r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[(int)y] + z;

	pPcb->p_mmix.reg[(int)x] = pPcb->p_ip + 4;

	pPcb->p_ip = (long) r;
	
	return STATUS_SUCCESS;
}

/*** foward ***/
/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief branch if negative
 */
STATUS token_bn( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( pPcb->p_mmix.reg[(int)x] < 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief branch if zero
 */
STATUS token_bz( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( pPcb->p_mmix.reg[(int)x] == 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief branch if possitive
 */
STATUS token_bp( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( pPcb->p_mmix.reg[(int)x] > 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bod( PPCB pPcb )
 * \brief branch if odd
 */
STATUS token_bod( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( (pPcb->p_mmix.reg[(int)x] % 2) == 1)
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bnn( PPCB pPcb )
 * \brief branch non negative
 */
STATUS token_bnn( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( pPcb->p_mmix.reg[(int)x] >= 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}
/**
 * \fn STATUS token_bnz( PPCB pPcb )
 * \brief branch non zero
 */
STATUS token_bnz( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( pPcb->p_mmix.reg[(int)x] != 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bnp( PPCB pPcb )
 * \brief branch if non possitive
 */
STATUS token_bnp( PPCB pPcb )
{
	short r;
	char x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) | z ;

	if( pPcb->p_mmix.reg[(int)x] <= 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bev( PPCB pPcb )
 * \brief branch if even
 */
STATUS token_bev( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( (pPcb->p_mmix.reg[(int)x] % 2) == 0)
		pPcb->p_ip += r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}

/*** backwards ***/
/**
 * \fn STATUS token_bnb( PPCB pPcb )
 * \brief branch if negative backwards
 */
STATUS token_bnb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( pPcb->p_mmix.reg[(int)x] < 0 )
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bzb( PPCB pPcb )
 * \brief branch if zero backwards 
 */
STATUS token_bzb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( pPcb->p_mmix.reg[(int)x] == 0 )
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bpb( PPCB pPcb )
 * \brief branch if possitive back
 */
STATUS token_bpb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( pPcb->p_mmix.reg[(int)x] > 0 )
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bodb( PPCB pPcb )
 * \brief branch if odd backwards
 */
STATUS token_bodb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( (pPcb->p_mmix.reg[(int)x] % 2) == 1)
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bnnb( PPCB pPcb )
 * \brief branch non negative backwards
 */
STATUS token_bnnb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( pPcb->p_mmix.reg[(int)x] >= 0 )
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}
/**
 * \fn STATUS token_bnzb( PPCB pPcb )
 * \brief branch non zero backwards
 */
STATUS token_bnzb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( pPcb->p_mmix.reg[(int)x] != 0 )
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bnpb( PPCB pPcb )
 * \brief branch if non possitive backwards
 */
STATUS token_bnpb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( pPcb->p_mmix.reg[(int)x] <= 0 )
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bevb( PPCB pPcb )
 * \brief branch if even backwards
 */
STATUS token_bevb( PPCB pPcb )
{
	short r;
	char x,y,z;
	LOAD_XYZ(x,y,z);
	r = (y<<8) | z ;
	if( (pPcb->p_mmix.reg[(int)x] % 2) == 0)
		pPcb->p_ip -= r;
	else
		INC_IP(4);
	return STATUS_SUCCESS;
}

/**** * INTERRUPTS * ****/
/**
 * \fn STATUS token_trap( PPCB pPcb )
 * \brief trap
 */
STATUS token_trap( PPCB pPcb )
{
	STATUS s;
	int x;

	LOAD_X(x);

	if( x > NRSYSCALLS || x < 0 )
		return STATUS_INVALID;

	s = syscalls[x].funcion( pPcb );

	INC_IP(4);
	return s;
}

STATUS pre_trap( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice, int *tv )
{
	int i;

	for( i=0;i<NRSYSCALLS;i++) {
		if( strlen( syscalls[i].nombre)==strlen(s1) &&
			strncasecmp(  syscalls[i].nombre, s1, strlen(syscalls[i].nombre))==0) {
			*i1 = i;
			if( syscalls[i].pre ) {
				syscalls[i].pre( s2, i2, s3, i3 );
			} else {
				*i2 = atoi( s2 );
				*i3 = atoi( s3 );
			}
		}
	}

	return STATUS_SUCCESS;
}


/**** * BITWISE * ****/
/**
 * \fn STATUS token_and( PPCB pPcb )
 * \brief bitwise and
 */
STATUS token_and( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] & pPcb->p_mmix.reg[(int)z];
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_or( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] | pPcb->p_mmix.reg[(int)z];
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_xor( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] ^ pPcb->p_mmix.reg[(int)z];
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_andn( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] & (~pPcb->p_mmix.reg[(int)z]);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_orn( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] | (~pPcb->p_mmix.reg[(int)z]);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_nand( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = ~(pPcb->p_mmix.reg[(int)y] & pPcb->p_mmix.reg[(int)z]);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_nor( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = ~(pPcb->p_mmix.reg[(int)y] | pPcb->p_mmix.reg[(int)z]);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_nxor( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = ~(pPcb->p_mmix.reg[(int)y] ^ pPcb->p_mmix.reg[(int)z]);
	INC_IP(4);
	return STATUS_SUCCESS;
}
/* bitwise inmmediate */
STATUS token_andi( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] & z;
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_ori( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] | z;
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_xori( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] ^ z;
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_andni( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] & (~z);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_orni( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = pPcb->p_mmix.reg[(int)y] | (~z);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_nandi( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = ~(pPcb->p_mmix.reg[(int)y] & z);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_nori( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = ~(pPcb->p_mmix.reg[(int)y] | z);
	INC_IP(4);
	return STATUS_SUCCESS;
}
STATUS token_nxori( PPCB pPcb )
{
	char x,y,z;
	LOAD_XYZ(x,y,z);
	pPcb->p_mmix.reg[(int)x] = ~(pPcb->p_mmix.reg[(int)y] ^ z);
	INC_IP(4);
	return STATUS_SUCCESS;
}
