/**
 * \file label.c
 * Convierte label a pocisiones de memoria.
 * Estas funciones son las encargadas de poder
 * referenciar posciciones de memoria con labesl. De
 * esta manera los jump y branches pueden funcionar
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "label.h"
#include "token.h"


static LIST_ENTRY labels_list;

/**
 * \fn STATUS label_add( FILE *fd, int indice )
 */
STATUS label_add( FILE *fd, int indice )
{
	PLABEL pL;
	pL = malloc( sizeof(LABEL));

	fscanf(fd,"%s",pL->nombre );
	pL->indice = indice;

	InsertTailList( &labels_list, (PLIST_ENTRY) pL );

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS label_find( char *nombre, int *indice )
 */
STATUS label_find( char *nombre, int *indice )
{
	PLIST_ENTRY pList = labels_list.next;
	PLABEL pL;

	while( !IsListEmpty( &labels_list) && ( pList != &labels_list ) ) {

		pL = (PLABEL)  pList;

		if( strlen(nombre)==strlen(pL->nombre) &&
			strncasecmp( pL->nombre, nombre, strlen(pL->nombre)) == 0) {

			*indice = pL->indice;
			return STATUS_SUCCESS;

		}
		
		pList = LIST_NEXT(pList);
	}

	return STATUS_ERROR;
}

/**
 * \fn STATUS label_init()
 */
STATUS label_init()
{
	InitializeListHead( &labels_list );
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS label_init()
 */
STATUS label_deinit()
{
	PLIST_ENTRY pList;

	while( !IsListEmpty( &labels_list) ) {

		pList = RemoveHeadList( &labels_list );

		free( pList );
	}
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS pre_branch( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice )
 */
STATUS pre_branch( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice, int *tv )
{
	PLIST_ENTRY pList = labels_list.next;
	PLABEL pL;
	char *nombre;

	pre_generic( s1,i1,s2,i2,NULL,i3,tv);

	if( s2[0]!='@' ) 
		return STATUS_SUCCESS;

	nombre = &s2[1];

	while( !IsListEmpty( &labels_list) && ( pList != &labels_list ) ) {

		pL = (PLABEL)  pList;

		if( strlen(nombre)==strlen(pL->nombre) &&
			strncasecmp( pL->nombre, nombre, strlen(pL->nombre))==0) {

			*i2 = pL->indice - indice ;

			if( *i2<0 ) {		/* Si es negativo, tonces usa el backward */
				*i2 = -(*i2);
				(*tv)++;
			}
			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}
	return STATUS_ERROR;
}

/**
 * \fn STATUS pre_branch( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice )
 */
STATUS pre_jump( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice, int *tv )
{
	PLIST_ENTRY pList = labels_list.next;
	PLABEL pL;
	char *nombre;

	pre_generic( s1,i1,NULL,i2,NULL,i3,tv);

	if( s1[0]!='@' ) 
		return STATUS_SUCCESS;

	nombre = &s1[1];

	while( !IsListEmpty( &labels_list) && ( pList != &labels_list ) ) {

		pL = (PLABEL)  pList;

		if( strlen(nombre)==strlen(pL->nombre) &&
			strncasecmp( pL->nombre, nombre, strlen(pL->nombre)) == 0) {

			*i1 = pL->indice - indice ;
			if( *i1 <  0) { /* usa el token del backward */
				*i1 = -(*i1);
				(*tv)++;
			}
			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}
	return STATUS_ERROR;
}
