/**
 * \file tokens.h
 */
#ifndef __TOKENS_H
#define __TOKENS_H

#include "sched.h"

STATUS syscall_exit( PPCB pPcb );
STATUS syscall_fork( PPCB pPcb );
STATUS syscall_read( PPCB pPcb );
STATUS syscall_write( PPCB pPcb );
STATUS syscall_wait( PPCB pPcb );
STATUS syscall_signal( PPCB pPcb );
STATUS syscall_kill( PPCB pPcb );


#define TOKEN_LDB	"ldb"
#define TOKEN_LDW	"ldw"
#define TOKEN_LDA	"lda"
#define TOKEN_STB	"stb"
#define TOKEN_STW	"stw"
#define TOKEN_ADD	"add"
#define TOKEN_SUB	"sub"
#define TOKEN_MUL	"mul"
#define TOKEN_DIV	"div"
#define TOKEN_NEG	"neg"
#define TOKEN_SL	"sl"
#define TOKEN_SR	"sr"
#define TOKEN_CMP	"cmp"
#define TOKEN_BN	"bn"
#define TOKEN_BZ	"bz"
#define TOKEN_BP	"bp"
#define TOKEN_BOD	"bod"
#define TOKEN_BNN	"bnn"
#define TOKEN_BNZ	"bnz"
#define TOKEN_BNP	"bnp"
#define TOKEN_BEV	"bev"
#define TOKEN_PBN	"pbn"
#define TOKEN_PBZ	"pbz"
#define TOKEN_PBP	"pbp"
#define TOKEN_PBOD	"pbod"
#define TOKEN_PBNN	"pbnn"
#define TOKEN_PBNZ	"pbnz"
#define TOKEN_PBNP	"pbnp"
#define TOKEN_PBEV	"pbev"
#define TOKEN_SETH	"seth"
#define TOKEN_SETMH	"setmh"
#define TOKEN_SETML	"setml"
#define TOKEN_SETL	"setl"
#define TOKEN_INCH	"inch"
#define TOKEN_INCMH	"incmh"
#define TOKEN_INCML	"incml"
#define TOKEN_INCL	"incl"
#define TOKEN_JMP	"jmp"
#define TOKEN_GO	"go"
#define TOKEN_TRAP	"trap"
#define	TOKEN_AND	"and"
#define	TOKEN_OR	"or"
#define	TOKEN_XOR	"xor"
#define	TOKEN_ANDN	"andn"
#define	TOKEN_ORN	"orn"
#define	TOKEN_NAND	"nand"
#define	TOKEN_NOR	"nor"
#define	TOKEN_NXOR	"nxor"
#define TOKEN_2ADD	"2add"
#define TOKEN_4ADD	"4add"
#define TOKEN_8ADD	"8add"
#define TOKEN_16ADD	"16add"


STATUS token_ldb( PPCB pPcb );
STATUS token_ldbi( PPCB pPcb );
STATUS token_ldw( PPCB pPcb );
STATUS token_ldwi( PPCB pPcb );
STATUS token_stb( PPCB pPcb );
STATUS token_stbi( PPCB pPcb );
STATUS token_stw( PPCB pPcb );
STATUS token_stwi( PPCB pPcb );
STATUS token_add( PPCB pPcb );
STATUS token_addi( PPCB pPcb );
STATUS token_sub( PPCB pPcb );
STATUS token_subi( PPCB pPcb );
STATUS token_mul( PPCB pPcb );
STATUS token_muli( PPCB pPcb );
STATUS token_div( PPCB pPcb );
STATUS token_divi( PPCB pPcb );
STATUS token_neg( PPCB pPcb );
STATUS token_negi( PPCB pPcb );
STATUS token_sl( PPCB pPcb );
STATUS token_sli( PPCB pPcb );
STATUS token_sr( PPCB pPcb );
STATUS token_sri( PPCB pPcb );
STATUS token_cmp( PPCB pPcb );
STATUS token_cmpi( PPCB pPcb );
STATUS token_bn( PPCB pPcb );
STATUS token_bnb( PPCB pPcb );
STATUS token_bz( PPCB pPcb );
STATUS token_bzb( PPCB pPcb );
STATUS token_bp( PPCB pPcb );
STATUS token_bpb( PPCB pPcb );
STATUS token_bod( PPCB pPcb );
STATUS token_bodb( PPCB pPcb );
STATUS token_bnn( PPCB pPcb );
STATUS token_bnnb( PPCB pPcb );
STATUS token_bnz( PPCB pPcb );
STATUS token_bnzb( PPCB pPcb );
STATUS token_bnp( PPCB pPcb );
STATUS token_bnpb( PPCB pPcb );
STATUS token_bev( PPCB pPcb );
STATUS token_bevb( PPCB pPcb );
STATUS token_seth( PPCB pPcb );
STATUS token_setmh( PPCB pPcb );
STATUS token_setml( PPCB pPcb );
STATUS token_setl( PPCB pPcb );
STATUS token_inch( PPCB pPcb );
STATUS token_incmh( PPCB pPcb );
STATUS token_incml( PPCB pPcb );
STATUS token_incl( PPCB pPcb );
STATUS token_jmp( PPCB pPcb );
STATUS token_jmpb( PPCB pPcb );
STATUS token_go( PPCB pPcb );
STATUS token_goi( PPCB pPcb );
STATUS token_trap( PPCB pPcb );
STATUS token_and( PPCB pPcb );
STATUS token_andi( PPCB pPcb );
STATUS token_or( PPCB pPcb );
STATUS token_ori( PPCB pPcb );
STATUS token_xor( PPCB pPcb );
STATUS token_xori( PPCB pPcb );
STATUS token_andn( PPCB pPcb );
STATUS token_andni( PPCB pPcb );
STATUS token_orn( PPCB pPcb ); 
STATUS token_orni( PPCB pPcb ); 
STATUS token_nand( PPCB pPcb );
STATUS token_nandi( PPCB pPcb );
STATUS token_nor( PPCB pPcb ); 
STATUS token_nori( PPCB pPcb ); 
STATUS token_nxor( PPCB pPcb );
STATUS token_nxori( PPCB pPcb );
STATUS token_2add( PPCB pPcb );
STATUS token_2addi( PPCB pPcb );
STATUS token_4add( PPCB pPcb );
STATUS token_4addi( PPCB pPcb );
STATUS token_8add( PPCB pPcb );
STATUS token_8addi( PPCB pPcb );
STATUS token_16add( PPCB pPcb );
STATUS token_16addi( PPCB pPcb );

STATUS pre_trap( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice, int *tv );

#endif /* __TOKENS_H */
