/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <curses.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tipos.h"
#include "cons.h"
#include "metric.h"
#include "archivo.h"
#include "adm_comms.h"
#include "adm_graph.h"
#include "adm_common.h"
#include "mem.h"

/**
 * \file pantalla.c
 * Manejo de pantalla
 */

/**
 * Info sobre el programa, el grupo (autores) y cualquier otra yerba \b ACA.
 */

#define PANT_FILAS	.80			/**< El ancho del pGPad (ver TSPant). */
#define PANT_COLUMNAS	.75	/**< El alto del pGPad (ver TSPant). */
#define PANT_CONSOLE_BUFFER 80 /**< Tama�o del buffer de entrada para la
	consola.*/

static void pant_init_screen( TSPant *pTspant );
static void pant_comms_init( TSPant *pTspant );
static void pant_metric_init( TSPant *pTspant );

/**
 * Inicia las rutinas del programa.
 * Actualmente esto es:
 *	- Incializar el ncurses.
 *	- Inicializar la estructura de comandos y agregar los mismos.
 *	- Inicializar la cola de procesos.
 *	- Inicializar la lista de estados.
 *	- Inicializar la lista de m�tricas.
 *	- Dibujar la pantalla (inicializando las estructuras internas).
 *
 * El \c ncurses est� siendo utilizado para la representaci�n de los datos de la
 * cola en pantalla, los gr�ficos y las m�tricas; tambi�n se lo utiliza para
 * el manejo del teclado.
 * @param pTspant La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
void pantalla_init( TSPant *pTspant )
{
	assert( pTspant != NULL );

	initscr();
	start_color();

	init_pair( PANT_GREEN, COLOR_GREEN, COLOR_BLUE );
	init_pair( PANT_RED, COLOR_RED, COLOR_BLUE );
	init_pair( PANT_CYAN, COLOR_CYAN, COLOR_BLUE );
	init_pair( PANT_WHITE, COLOR_WHITE, COLOR_BLUE );

	bkgdset( ' ' | COLOR_PAIR( PANT_WHITE ) );

	cbreak();
	noecho();

	pTspant->iXLastPos = 0;

	pant_init_screen( pTspant );
	pant_comms_init( pTspant );
	pant_metric_init( pTspant );

	pTspant->pTMetric = metric_get( 0 );	/* Metrica 'default' */
	pTspant->ipGraph = adm_graph_metric;
}


/**
 * Inicia la consola.
 * Incicia las estructuras de datos pertinentes y agrega los comandos
 * definidos.
 * @param pTspant La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
static void pant_comms_init( TSPant *pTspant )
{
	assert( pTspant != NULL );

	pTspant->pTCommands = cons_new();
	assert( pTspant->pTCommands != NULL );

	adm_comms_add_help( pTspant, pTspant->pGConsole );

	cons_command_add( &pTspant->pTCommands, "clear", "Limpia la pantalla",
		adm_comms_clear, pTspant->pGPad );

	cons_command_add( &pTspant->pTCommands, "speed",
		"speed [vel] - Modifica la velocidad del clock", adm_comms_clock_set_speed, pTspant );

	cons_command_add( &pTspant->pTCommands, "graph",
		"Cambia la m�trica que se desea ver", adm_comms_graph, pTspant ); 

	cons_command_add( &pTspant->pTCommands, "mem",
		"Muestra el estado actual de la memoria", adm_comms_mem, pTspant ); 

	cons_command_add( &pTspant->pTCommands, "ps",
		"Muestra el estado actual de todos los procesos", adm_comms_ps, pTspant );

	cons_command_add( &pTspant->pTCommands, "sem",
		"Muestra el estado actual de todos los semaforos", adm_comms_sem, pTspant );

/* Ahora 'help' es help + list
	cons_command_add( &pTspant->pTCommands, "list",
		"Muestra la lista de comandos disponibles", adm_comms_list, pTspant );
*/

	cons_command_add( &pTspant->pTCommands, "kill",
		"kill id - Mata al proceceso con el 'id'", adm_comms_killproc, pTspant );

	cons_command_add( &pTspant->pTCommands, "exit",
		"Salir", adm_comms_exit, pTspant );
}

/**
 * Termina el programa.
 * Actualmente esto es:
 *	- Eliminar la estructura de comandos.
 *	- Terminar el ncurses.
 *	- Eliminar las estructuras creadas y liberar la memoria.
 */
void pantalla_end( TSPant *pTspant )
{
	assert( pTspant != NULL );

	nocbreak();
	cons_destroy( pTspant->pTCommands );

	delwin( pTspant->pGPad );
	delwin( pTspant->pGMetrics );
	delwin( pTspant->pGConsole );
	delwin( pTspant->pGMetOptions );
	endwin();
}

/**
 * Dibuja la pantalla inicial.
 * Crea las estructuras necesarias, como ser las diversas ventanas.
 */
static void pant_init_screen( TSPant *pTspant )
{
	int iFilas;
	int iColumnas;
	int i;

	assert( pTspant != NULL );

	iFilas = LINES * PANT_FILAS;
	iColumnas = COLS * PANT_COLUMNAS;

	/* Algunas l�neas para mejorar la apariencia */
	mvhline( 0, 1, ACS_HLINE, COLS - 2 );
	mvhline( LINES - 1, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, 0, ACS_VLINE, LINES - 2 );
	mvvline( 1, COLS - 1, ACS_VLINE, LINES - 2 );

	mvhline( iFilas, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, iColumnas - 1, ACS_VLINE, LINES - 2 );

	/* Ahora las esquinas e intersecciones */
	mvaddch( 0, 0, ACS_ULCORNER );
	mvaddch( 0, COLS - 1, ACS_URCORNER );
	mvaddch( LINES - 1, 0, ACS_LLCORNER );
	mvaddch( LINES - 1, COLS - 1, ACS_LRCORNER );

	mvaddch( iFilas, iColumnas - 1, ACS_PLUS );
	mvaddch( LINES - 1, iColumnas - 1, ACS_BTEE );
	mvaddch( 0, iColumnas - 1, ACS_TTEE );
	mvaddch( iFilas, 0, ACS_LTEE );
	mvaddch( iFilas, COLS - 1, ACS_RTEE );

	/* Regla */
	for( i = 1; i < iFilas; i++ )
		mvaddch( i, 0, ACS_LTEE );

	refresh();

	/* Graficos */
	pTspant->pGPad = newwin( iFilas - 1, iColumnas - 2, 1, 1 );
	wbkgdset( pTspant->pGPad, ' ' | COLOR_PAIR( PANT_WHITE ) );
	wclear( pTspant->pGPad );
	scrollok( pTspant->pGPad, TRUE );
	wnoutrefresh( pTspant->pGPad );


	/* Metricas */
   pTspant->pGMetrics = newwin( iFilas - 1, COLS -iColumnas - 1, 1, iColumnas);
	wbkgdset( pTspant->pGMetrics, ' ' | COLOR_PAIR( PANT_WHITE ) );
	wclear( pTspant->pGMetrics );
	scrollok( pTspant->pGMetrics, FALSE );

	mvwprintw( pTspant->pGMetrics, 3, 0, "Parados    :" );
	mvwprintw( pTspant->pGMetrics, 4, 0, "Dormidos   :" );
	mvwprintw( pTspant->pGMetrics, 5, 0, "Ejecutando :" );
	mvwprintw( pTspant->pGMetrics, 6, 0, "Terminados :" );

	mvwprintw( pTspant->pGMetrics, 14, 0, "M�trica :" );
	mvwprintw( pTspant->pGMetrics, 15, 0, " Max.   :" );
	mvwprintw( pTspant->pGMetrics, 16, 0, " Min.   :" );

	wattron( pTspant->pGMetrics, (attr_t)COLOR_PAIR( PANT_GREEN ) );
	mvwaddch( pTspant->pGMetrics, 17, 0, ACS_BULLET );
	wprintw( pTspant->pGMetrics, "Prom.  :" );
	wattroff( pTspant->pGMetrics, (attr_t)COLOR_PAIR( PANT_GREEN ) );

	wattron( pTspant->pGMetrics, (attr_t)COLOR_PAIR( PANT_CYAN ) | A_BOLD );
	mvwaddch( pTspant->pGMetrics, 18, 0, ACS_DIAMOND );
	mvwprintw( pTspant->pGMetrics, 18, 1, "Actual :" );
	wattroff( pTspant->pGMetrics, (attr_t)COLOR_PAIR( PANT_CYAN ) | A_BOLD );

	wnoutrefresh( pTspant->pGMetrics );

	/* Consola */
	pTspant->pGConsole = newwin( LINES - iFilas - 2, iColumnas - 2,
		iFilas + 1, 1 );
	scrollok( pTspant->pGConsole, TRUE );
	keypad( pTspant->pGConsole, TRUE );
	nodelay( pTspant->pGConsole, TRUE );
	wnoutrefresh( pTspant->pGConsole );

  /* Eleccion de metricas */
	pTspant->pGMetOptions = newwin( LINES - iFilas - 2, COLS - iColumnas -1,
		iFilas + 1, iColumnas );
	scrollok( pTspant->pGMetOptions, FALSE );
	wbkgdset( pTspant->pGMetOptions, ' ' | COLOR_PAIR( PANT_WHITE ) );
	wclear( pTspant->pGMetOptions );

	wnoutrefresh( pTspant->pGMetOptions );

	doupdate();
}

/**
 * Muestra el estado actual de los procesos.
 * \author Picorelli Marcelo.
 */
void pantalla_status_refresh( TSPant *pTspant )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int ivEstado[PROC_LAST];
	int i;

	assert( pTspant != NULL );

	mvwprintw( pTspant->pGMetrics, 0, 0, "Clock   : " );
	wattron( pTspant->pGMetrics, A_BOLD );
	wprintw( pTspant->pGMetrics, "%d", jiffies );
	wattroff( pTspant->pGMetrics, A_BOLD );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics, 1, 0, "FreeMem : " );
	wattron( pTspant->pGMetrics, A_BOLD );
	wprintw( pTspant->pGMetrics, "%d", mem_code_tot );
	wattroff( pTspant->pGMetrics, A_BOLD );
	wclrtoeol( pTspant->pGMetrics );

	memset( &ivEstado, 0, sizeof( ivEstado ));

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		ivEstado[ pPcb->p_estado ]++;
		pList = LIST_NEXT( pList );
	}

	wattron( pTspant->pGMetrics, A_BOLD );
	mvwprintw( pTspant->pGMetrics, 3, 13, "%d",ivEstado[ PROC_STOP ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics,4,13, "%d",ivEstado[ PROC_SLEEP ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics, 5,13, "%d", ivEstado[ PROC_RUN ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics,6,13,"%d",
	ivEstado[PROC_TERMINADO]);
	wclrtoeol( pTspant->pGMetrics );
	wattroff( pTspant->pGMetrics, A_BOLD );

	mvwprintw( pTspant->pGMetrics, 8, 0, "CPUS");
	wclrtoeol( pTspant->pGMetrics );

	for( i = 0; i < NRCPU; i++ )
	{
		mvwprintw( pTspant->pGMetrics, 8+i, 0, "%i : ", i );

		wattron( pTspant->pGMetrics, A_BOLD );
		wprintw( pTspant->pGMetrics, "%s",
			(cpu[i].c_estado==CPU_RUNNING ? cpu[i].c_proceso->p_nombre : "IDLE" ));
		wattroff( pTspant->pGMetrics, A_BOLD );
		wclrtoeol( pTspant->pGMetrics );
	}

	wrefresh( pTspant->pGMetrics );
}

int pantalla_console( TSPant *pTspant )
{
	char cpBuffer[ PANT_CONSOLE_BUFFER ];
	char tokens[2][80];
	char *cpTemp;

	assert( pTspant != NULL );

	echo();
	nodelay( pTspant->pGConsole, FALSE );
	waddch( pTspant->pGConsole, '>' );
	waddch( pTspant->pGConsole, ' ' );
	wgetnstr( pTspant->pGConsole, cpBuffer, PANT_CONSOLE_BUFFER );
	if( strlen( cpBuffer ) )
	{
		/* Leo la linea de comando, por ahora a lo sumo 2 palabras.*/
		if( ( cpTemp = strtok( cpBuffer, " " ) ) )
		{
			strcpy( tokens[0], cpTemp );
			if( ( cpTemp = strtok( NULL, " " ) ) )
				strcpy( tokens[1], cpTemp );
			else
				tokens[1][0] = 0;

			if( cons_command_execute( pTspant->pTCommands, tokens[0], tokens[1] ) == 0) {
				wprintw( pTspant->pGConsole, "Comando '%s' no reconocido. Tipee 'help' para ayuda\n", tokens[0] );
			}
		}
	}

	noecho();
	nodelay( pTspant->pGConsole, TRUE );

	return 1;
}

/* Esto tiene que ver con el menucito de graficas disponibles */

static void pant_metric_init( TSPant *pTspant )
{
	PMETRIC pM = metric_get( 0 );
	int i = 1;
	int iFilas = LINES * PANT_FILAS;

	assert( pTspant != NULL );

	if( !pM )
		return;

	wprintw( pTspant->pGMetOptions, " 0) " );
	wattron( pTspant->pGMetOptions, A_BOLD );
	wprintw( pTspant->pGMetOptions, "%s\n", pM->nombre );
	wattroff( pTspant->pGMetOptions, A_BOLD );

	while( ( pM = metric_get( i ) )  && i + iFilas + 1 < LINES - 1)
	{
		wprintw( pTspant->pGMetOptions, "%2d) %s\n", i, pM->nombre );
		i++;
	}
	wnoutrefresh( pTspant->pGMetOptions );
}

/**
 * Dibuja el menu de metricas.
 * @param pTspant La pantalla donde dibujar
 * @param step cuanto subir o bajar el menu
 * \author Picorelli Marcelo
 */
int pantalla_metric_draw_menu( TSPant *pTspant, int step )
{
	static int min_item = 0;
	static int max_item = 3; //LINES - ( LINES * PANT_FILAS );
	static int act_item = 0;
	PMETRIC  pM = NULL;
	int i;

	assert( pTspant != NULL );
	assert( step <= 1 || step >=-1 );

	/* Si vamos hacia arriba */
	if( step == -1 )
	{
		/* Y el item en el que estamos ahora esta visible...*/
		if( ( act_item - 1 ) >= min_item )
		{
			act_item--;
		}
		else
			if( ( act_item - 1 ) >= 0 )
			{
				min_item--;
				max_item--;
				act_item--;
			}
	}
	else
		if( step == 1 )
		{
			if( ( act_item + 1 ) < max_item )
			{
				act_item++;
			}
			else
				if( metric_get( act_item + 1) )
				{
					act_item++;
					max_item++;
					min_item++;
				}
		}

	/* Dibujo el menu desde el principio */
	i = min_item;
	wclear( pTspant->pGMetOptions );

	while( ( pM = metric_get( i ) )  && ( i < max_item ) )
	{
		wprintw( pTspant->pGMetOptions, "%2d) ", i );
		if( i == act_item )
		{
			wattron( pTspant->pGMetOptions, A_BOLD);
			wprintw( pTspant->pGMetOptions, "%s\n", pM->nombre);
			wattroff( pTspant->pGMetOptions, A_BOLD );
			i++;
			continue;
		}
		wprintw( pTspant->pGMetOptions, "%s\n", pM->nombre);
		i++;
	}
	wnoutrefresh( pTspant->pGMetOptions );

	return act_item;
}
