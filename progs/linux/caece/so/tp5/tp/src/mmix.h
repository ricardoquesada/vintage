/**
 * \file mmix.h
 * estructura de la CPU del MMIX
 */

#ifndef __MMIX_H
#define __MMIX_H

typedef struct _bignum { 
	signed long h,l;			/**< un 64 bit long */
} BIGNUM, *PBIGNUM;

typedef struct _mmix {
	signed long long reg[256];		/**< registros generales */
	signed long long special_reg[32];	/**< registros especiales */
} MMIX, *PMMIX;

typedef enum{
	REG_rB,REG_rD,REG_rE,REG_rH,REG_rJ,REG_rM,REG_rR,REG_rBB,
	REG_rC,REG_rN,REG_rO,REG_rS,REG_rI,REG_rT,REG_rTT,REG_rK,
	REG_rQ,REG_rU,REG_rV,REG_rG,REG_rL,REG_rA,REG_rF,REG_rP,
	REG_rW,REG_rX,REG_rY,REG_rZ,REG_rWW,REG_rXX,REG_rYY,REG_rZZ
} special_reg;

typedef enum{
	Halt,Fopen,Fclose,Fread,Fgets,Fgetws,
	Fwrite,Fputs,Fputws,Fseek,Ftell
} sys_call;

#endif /* __MMIX_H */
