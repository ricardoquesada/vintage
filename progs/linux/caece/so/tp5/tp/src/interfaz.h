/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file interfaz.h
 */
#ifndef __INTERFAZ_H
#define __INTERFAZ_H

#include "tipos.h"
#include "metric.h"


STATUS interfaz_gfx( PMETRIC pM );
STATUS interfaz_init();
STATUS interfaz_deinit();
STATUS interfaz_refresh();
STATUS console_out( char *str,...);
STATUS console_input();

#endif /* __INTERFAZ_H */
