/*
 * Trabajo Practico Numero 4. Semaforos
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file sem.h
 * Semaforos
 * \author Ricardo Quesada
 */

#ifndef __SEM_H
#define __SEM_H

#define SEM_MAX_NAME 50	

typedef struct _sem  {
	LIST_ENTRY next;	/**< siguiente semaforo */
	int	id;		/**< id de este semaforo */
	char	nombre[SEM_MAX_NAME];	/**< nombre del semaforo */
	int	max;		/**< recursos del semaforo */
	LIST_ENTRY pcbs;	/**< lista de los procesos que estan waiting este semaforo */
} SEM, *PSEM;


extern LIST_ENTRY g_sem_list;

STATUS sem_create( char *nombre, int max );
STATUS sem_wait( int id, PPCB pPcb );
STATUS sem_signal( int id, PPCB pPcb );
STATUS pre_sem( char *nombre, int *id, char *s2, int *i2 );
STATUS sem_init();
STATUS sem_free( PPCB pPcb );

#endif /* __SEM_H */
