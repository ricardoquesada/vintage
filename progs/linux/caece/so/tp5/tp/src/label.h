/**
 * \file label.h
 */

#ifndef __LABEL_H
#define __LABEL_H

typedef struct _label {
	LIST_ENTRY next;
	char nombre[50];
	unsigned int indice; 
} LABEL, *PLABEL;


STATUS label_add( FILE *fd, int indice );
STATUS label_find( char *nombre, int *indice );
STATUS label_init();
STATUS label_deinit();
STATUS pre_branch( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice, int *tv );
STATUS pre_jump( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice, int *tv );

#endif /* __LABEL_H */
