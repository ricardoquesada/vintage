/**
 * \file token.h
 */
#ifndef __TOKEN_H
#define __TOKEN_H


STATUS tokenize_to_mem( PPCB pPcb, FILE *fp );
STATUS run_proc( PPCB pPcb );
void pre_generic( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int *tv );

#endif /* __TOKEN_H */
