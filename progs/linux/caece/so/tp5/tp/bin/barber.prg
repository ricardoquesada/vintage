!
! Problema del barbero 
! Grupo MRM
!
! Modulo: Barbero
! 
! .param	code_len	data_len	stack_len
!
.version	2
.param	44	0	0
.sem	MAX_CAPACITY	20
.sem	SOFA		4
.sem	COORD		3
.sem	BARBER_CHAIR	3
.sem	CUST_READY	0
.sem	FINISHED	0
.sem	LEAVE_B_CHAIR	0
.sem	PAYMENT		0
.sem	RECEIPT		0


@main_loop
	trap	WAIT ,CUST_READY ,0
	trap	WAIT ,COORD ,0

	! Cut Hair
	setl	$0 ,25
@loop1
	incl	$0 ,-1
	bnz	$0 ,@loop1


	trap	SIGNAL	,COORD ,0
	trap	SIGNAL	,FINISHED ,0

	trap	WAIT ,LEAVE_B_CHAIR ,0
	
	trap	SIGNAL ,BARBER_CHAIR ,0

	! Forever
	jmp	@main_loop

.end
