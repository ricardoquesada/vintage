!
! filosofo 3
! Primer acercamiento al problema de los filosofos, Stallings.
!
! .param	code_len	data_len	stack_len
!
.version	2
.param	100	0	0
.sem	SEM_3	1
.sem	SEM_4	1



	! Repite todo 10 veces
	setl	$0 ,10

@loop
	trap	WAIT ,SEM_4 ,0
	trap	WAIT ,SEM_3 ,0

	! esta comiendo y tarda 25
	setl	$1 ,25
@loop2
	incl	$1 ,-1
	bnz	$1 ,@loop2


	trap	SIGNAL ,SEM_3 ,0
	trap	SIGNAL ,SEM_4 ,0

	incl	$0 ,-1
	bnz	$0 ,@loop

	trap	EXIT ,0 ,0
.end
