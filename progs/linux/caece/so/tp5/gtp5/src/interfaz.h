/*
 * Trabajo Practico Numero 4.
 * Sistemas Operativos.
 * Grupo MRM
 */

#ifndef __INTERFAZ_H
#define __INTERFAZ_H

#include "tipos.h"
#include "metric.h"

/* TODO ver como trato esto con GNOME */
STATUS interfaz_gfx( PMETRIC pM );

STATUS interfaz_init( void );
STATUS interfaz_deinit( void );
STATUS interfaz_refresh( void );
STATUS console_out( char *str,...);
STATUS console_input( void );

#endif /* __INTERFAZ_H */
