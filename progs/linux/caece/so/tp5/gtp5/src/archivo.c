/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "interfaz.h"

/**
 * \file archivo.c
 * Funciones que manejan la entrada de procesos a traves de
 * un archivo de datos
 * \author Ricardo Quesada
 */

FILE *fp=NULL;

/**
 * \fn STATUS archivo_init(char *nombre)
 * \brief Inicializa el archivo (lo abre)
 * \param nombre Nombre del archivo a abrir
 * \return STATUS
 */
STATUS archivo_init(char *nombre)
{
	if( (fp = fopen( nombre, "rt" )) == NULL)
		return STATUS_FILEERROR;

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS archivo_read( PPCB ppcb )
 * \brief Inicializa el archivo (lo abre)
 * \param ppcb Puntero al PCB donde se van a poner los datos leidos
 * \return STATUS
 */
STATUS archivo_read( PPCB ppcb )
{
	static PCB last_pcb;
	static unsigned long last = 0;
	static int have_data=0;
	static int delta=0;
	char c;
	int r;

	PDEBUG("archivo_read()\n");

	if( !have_data ) {
		if(!fp)
			return STATUS_FILEERROR;

read_loop:
		if( feof( fp ))
			return STATUS_FILEEOF;


		r=fread(  &c, 1 ,1, fp);
		if( feof( fp ))
			return STATUS_FILEEOF;
		
		if(r!=1 || c=='\n')
			goto read_loop;
		if( c=='!') {
			while(c!='\n') {
				fread(  &c, 1 ,1, fp);
				if( feof( fp ))
					return STATUS_FILEEOF;
			}
			goto read_loop;
		}
		fseek( fp, -1 ,SEEK_CUR);

		fscanf( fp, "%s\t%d\t%d\t%d\t%d\t%d\n", 
				last_pcb.p_nombre,
				&delta,
				(unsigned*)&last_pcb.p_basepriority,
				&last_pcb.p_cpu,
				(unsigned*)&last_pcb.p_politica,
				&last_pcb.p_nice
				);

		if( delta < 0 ) {
			console_out("El tiempo relativo debe ser positivo\n");
			have_data=0;
			return STATUS_ERROR;
		}

		have_data = 1;

#if 0
		console_out("Procesando: %s\n",last_pcb.p_nombre);
		printf("\tdelta=%d\n",delta);
		printf("\tp_t_run:%d\n",(unsigned)last_pcb.p_t_run);
		printf("\tp_basepriority:%d\n",last_pcb.p_basepriority);
		printf("\tp_cpu:%d\n",last_pcb.p_cpu);
		printf("\tp_politica:%d\n",last_pcb.p_politica);
		printf("\tp_nice:%d\n",last_pcb.p_nice);
#endif
	}


	if( ( have_data ) && ( delta + last <= jiffies )) {
		last = jiffies;
		*ppcb = last_pcb;
		have_data=0;
		return STATUS_SUCCESS;
	}
	return STATUS_NOTREADY;
}

/**
 * \fn STATUS archivo_close(void)
 * \brief Cierra el archivo abierto
 * \return STATUS
 */
STATUS archivo_close(void)
{
	if(fp)
		fclose( fp );
	return STATUS_SUCCESS;
}
