/*
 * CAECE
 * Sistemas Operativos
 * TP5
 * Grupo MRM
 */
/**
 * \file tokens.c
 * Valores de los tokens, basados en MMIX (Millenium MIX)
 * El assembler es muy implementacion muy limitada del MMIX,
 * no cuenta con muchas de las funciones, pero tiene las
 * necesarias como para hacer alguna demo.
 * \author Ricardo Quesada
 */

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "tipos.h"
#include "tokens.h"
#include "events.h"
#include "interfaz.h"
#include "sem.h"
#include "sched.h"


#define INC_IP(a) (pPcb->p_ip = pPcb->p_ip + (a))
#define LOAD_X(x) ((x) = pPcb->p_codeseg[ pPcb->p_ip +1 ])
#define LOAD_Y(y) ((y) = pPcb->p_codeseg[ pPcb->p_ip +2 ])
#define LOAD_Z(z) ((z) = pPcb->p_codeseg[ pPcb->p_ip +3 ])
#define LOAD_XYZ(x,y,z) LOAD_X(x);LOAD_Y(y);LOAD_Z(z);


struct _syscalls {
	char *nombre;			/**< nombre de la syscall */
	STATUS (*funcion)();		/**< funcion de la syscall */
	STATUS (*pre)();		/**< funcion pre de la syscall */
} syscalls[] = {
	{"exit", syscall_exit, NULL },
	{"signal", syscall_signal, sem_a2i },
	{"wait", syscall_wait, sem_a2i },
	{"read", syscall_read, NULL },
	{"write", syscall_write, NULL},
	{"kill", syscall_kill, NULL}, 
};
#define NRSYSCALLS ( sizeof(syscalls) /sizeof(syscalls[0]) )

/**
 * \fn STATUS syscall_exit( PPCB pPcb )
 * \brief Implementacion de 'exit'
 * \author Ricardo Quesada
 */
STATUS syscall_exit( PPCB pPcb )
{
	int tardo;
	long p;

	tardo = jiffies - pPcb->p_t_run_start;
	p = (tardo * 100 )/ pPcb->p_cpu_usada;

	console_out("'%s' tardo %d ciclos, y era de %d. (%d%%)\n",pPcb->p_nombre
			,tardo
			,pPcb->p_cpu_usada
			,p
			);
	return STATUS_END;
}


/**
 * \fn STATUS syscall_read( PPCB pPcb )
 * \brief Implementacion de 'read a b' (a=Dispositivo b=unidades a leer)
 * \author Ricardo Quesada
 */
STATUS syscall_read( PPCB pPcb )
{
	int x,dsp,tot;

	LOAD_XYZ(x,dsp,tot);

	wait_event( pPcb, dsp*2, tot );

	return STATUS_EVENT;
}

/**
 * \fn STATUS syscall_write( PPCB pPcb )
 * \brief Implementacion de 'write a b' (a=Dispositivo b=unidades a escribir)
 * \author Ricardo Quesada
 */
STATUS syscall_write( PPCB pPcb )
{
	int x,dsp,tot;

	LOAD_XYZ(x,dsp,tot);

	wait_event( pPcb, dsp*2 + 1, tot );

	return STATUS_EVENT;
}

/**
 * \fn STATUS syscall_wait( PPCB pPcb )
 * \brief Implementacion de 'wait' (semaforo)
 * \author Ricardo Quesada
 */
STATUS syscall_wait( PPCB pPcb )
{
	STATUS s;
	int id;

	LOAD_Y(id);

	if( (s=sem_wait( id, pPcb )) == STATUS_ERROR ) {
		console_out("Proceso '%s' causo un error usando token_wait\n",pPcb->p_nombre);
		return STATUS_INVALID;
	}
	return s;
}

/**
 * \fn STATUS syscall_signal( PPCB pPcb )
 * \brief Implementacion 'signal' (semaforo)
 * \author Ricardo Quesada
 */
STATUS syscall_signal( PPCB pPcb )
{
	int id;

	LOAD_Y(id);

	if( sem_signal( id, pPcb ) == STATUS_SUCCESS ) {
		return STATUS_SUCCESS;
	}

	console_out("Proceso '%s' causo un error usando token_signal\n",pPcb->p_nombre);
	return STATUS_INVALID;
}

/**
 * \fn STATUS syscall_kill( PPCB pPcb )
 * \brief Mata a algun proceso
 * \author Ricardo Quesada
 */
STATUS syscall_kill( PPCB pPcb )
{
	int id;

	LOAD_Y(id);
	kill_procid( id );
	return STATUS_SUCCESS;
}

/******************************************************/

/* * **** LOAD **** */
/**
 * \fn STATUS token_ldb( PPCB pPcb )
 * \brief load byte
 * \author Ricardo Quesada
 */
STATUS token_ldb( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	x = pPcb->p_codeseg[ pPcb->p_ip +1 ];
	y = pPcb->p_codeseg[ pPcb->p_ip +2 ];
	z = pPcb->p_codeseg[ pPcb->p_ip +3 ];

	r = pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = pPcb->p_codeseg[ r ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_ldbi( PPCB pPcb )
 * \brief load byte inmmediate
 * \author Ricardo Quesada
 */
STATUS token_ldbi( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	x = pPcb->p_codeseg[ pPcb->p_ip +1 ];
	y = pPcb->p_codeseg[ pPcb->p_ip +2 ];
	z = pPcb->p_codeseg[ pPcb->p_ip +3 ];

	r = pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = pPcb->p_codeseg[ r ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_ldw( PPCB pPcb )
 * \brief load wyde
 * \author Ricardo Quesada
 */
STATUS token_ldw( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	x = pPcb->p_codeseg[ pPcb->p_ip +1 ];
	y = pPcb->p_codeseg[ pPcb->p_ip +2 ];
	z = pPcb->p_codeseg[ pPcb->p_ip +3 ];

	r = pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_mmix.reg[x] = pPcb->p_codeseg[ r ] * 256 + pPcb->p_codeseg[r + 1 ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_ldwi( PPCB pPcb )
 * \brief load wyde inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_ldwi( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	x = pPcb->p_codeseg[ pPcb->p_ip +1 ];
	y = pPcb->p_codeseg[ pPcb->p_ip +2 ];
	z = pPcb->p_codeseg[ pPcb->p_ip +3 ];

	r = pPcb->p_mmix.reg[y] + z;

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_mmix.reg[x] = pPcb->p_codeseg[ r ] * 256 + pPcb->p_codeseg[r + 1 ];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_lda( PPCB pPcb )
 * \brief load address.
 * \brief Es lo mismo que token_add
 * \author Ricardo Quesada
 */

/**
 * \fn STATUS token_ldai( PPCB pPcb )
 * \brief load address inmmediate.
 * \brief Es lo mismo que token_addi
 * \author Ricardo Quesada
 */


/* * **** STORE **** */
/**
 * \fn STATUS token_stb( PPCB pPcb )
 * \brief store byte.
 * \author Ricardo Quesada
 */
STATUS token_stb( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_codeseg[ r ] = pPcb->p_mmix.reg[x];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_stbi( PPCB pPcb )
 * \brief store byte inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_stbi( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + z;

	pPcb->p_codeseg[ r ] = pPcb->p_mmix.reg[x];
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_stwi( PPCB pPcb )
 * \brief store wyde.
 * \author Ricardo Quesada
 */
STATUS token_stw( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_codeseg[ r ] = (pPcb->p_mmix.reg[x] >> 8);
	pPcb->p_codeseg[ r+1 ] = (pPcb->p_mmix.reg[x] && 0x000000ff);
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_stwi( PPCB pPcb )
 * \brief store wyde inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_stwi( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + z;

	/* FIXME: To be mmix fully compliant it should be aligned */
#ifdef WITH_MMIX_FULL_COMPATIBILITY
	r -= (r % 4);
#endif

	pPcb->p_codeseg[ r ] = (pPcb->p_mmix.reg[x] >> 8);
	pPcb->p_codeseg[ r+1 ] = (pPcb->p_mmix.reg[x] && 0x000000ff);
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/* * **** ARITHMETIC **** */
/**
 * \fn STATUS token_add( PPCB pPcb )
 * \brief add registers.
 * \author Ricardo Quesada
 */
STATUS token_add( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_addi( PPCB pPcb )
 * \brief add registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_addi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_2add( PPCB pPcb )
 * \brief 2add registers
 * \author Ricardo Quesada
 */
STATUS token_2add( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 2*pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_2addi( PPCB pPcb )
 * \brief 2add registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_2addi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 2*pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_4add( PPCB pPcb )
 * \brief 4 times add registers
 * \author Ricardo Quesada
 */
STATUS token_4add( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 4*pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_4addi( PPCB pPcb )
 * \brief 4add registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_4addi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 4*pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_8add( PPCB pPcb )
 * \brief 8 times add registers
 * \author Ricardo Quesada
 */
STATUS token_8add( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 8*pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_8addi( PPCB pPcb )
 * \brief 8add registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_8addi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 8*pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}
/**
 * \fn STATUS token_16add( PPCB pPcb )
 * \brief 16 times add registers
 * \author Ricardo Quesada
 */
STATUS token_16add( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 16*pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_16addi( PPCB pPcb )
 * \brief 16add registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_16addi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = 16*pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief sub registers.
 * \author Ricardo Quesada
 */
STATUS token_sub( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] - pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief sub registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_subi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] - z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief mul registers.
 * \author Ricardo Quesada
 */
STATUS token_mul( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] * pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sub( PPCB pPcb )
 * \brief mul registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_muli( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] * z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_div( PPCB pPcb )
 * \brief div registers.
 * \author Ricardo Quesada
 */
STATUS token_div( PPCB pPcb )
{
	signed long long r,rR;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] / pPcb->p_mmix.reg[z];
	rR = pPcb->p_mmix.reg[y] % pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	pPcb->p_mmix.special_reg[REG_rR] = rR;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_divi( PPCB pPcb )
 * \brief mul registers inmmediate.
 * \author Ricardo Quesada
 */
STATUS token_divi( PPCB pPcb )
{
	signed long long r,rR;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] * z;


	r = pPcb->p_mmix.reg[y] / pPcb->p_mmix.reg[z];
	rR = pPcb->p_mmix.reg[y] % pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	pPcb->p_mmix.special_reg[REG_rR] = rR;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_neg( PPCB pPcb )
 * \brief negetate registers.
 * \author Ricardo Quesada
 */
STATUS token_neg( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = y - pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief negetate registers inmmediate
 * \author Ricardo Quesada
 */
STATUS token_negi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = y - z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sl( PPCB pPcb )
 * \brief shift left registers.
 * \author Ricardo Quesada
 */
STATUS token_sl( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] << pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief shift left inmmediate
 * \author Ricardo Quesada
 */
STATUS token_sli( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] << z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_sl( PPCB pPcb )
 * \brief shift right registers.
 * \author Ricardo Quesada
 */
STATUS token_sr( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] >> pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief shift right inmmediate
 * \author Ricardo Quesada
 */
STATUS token_sri( PPCB pPcb )
{
	unsigned long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] >> z;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_cmp( PPCB pPcb )
 * \brief compare registers.
 * \author Ricardo Quesada
 */
STATUS token_cmp( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (pPcb->p_mmix.reg[y] > pPcb->p_mmix.reg[z]) - (pPcb->p_mmix.reg[z] > pPcb->p_mmix.reg[y]);

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_negi( PPCB pPcb )
 * \brief compare inmmediate
 * \author Ricardo Quesada
 */
STATUS token_cmpi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (pPcb->p_mmix.reg[y] > z) - (z > pPcb->p_mmix.reg[y]);

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/* **** * INMMEDIATE * ****/

/**
 * \fn STATUS token_seth( PPCB pPcb )
 * \brief set high wyde
 * \author Ricardo Quesada
 */
STATUS token_seth( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 16;
	r = y << 16;
	r = y << 16;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_setmh( PPCB pPcb )
 * \brief set medium high wyde
 * \author Ricardo Quesada
 */
STATUS token_setmh( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 16;
	r = y << 16;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_setml( PPCB pPcb )
 * \brief set medium low wyde
 * \author Ricardo Quesada
 */
STATUS token_setml( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 16;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_setl( PPCB pPcb )
 * \brief set low wyde
 * \author Ricardo Quesada
 */
STATUS token_setl( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 0 ;

	pPcb->p_mmix.reg[x] = r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_inch( PPCB pPcb )
 * \brief increment high wyde
 * \author Ricardo Quesada
 */
STATUS token_inch( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 16;
	r = y << 16;
	r = y << 16;

	pPcb->p_mmix.reg[x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_incmh( PPCB pPcb )
 * \brief increment medium high wyde
 * \author Ricardo Quesada
 */
STATUS token_incmh( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 16;
	r = y << 16;

	pPcb->p_mmix.reg[x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_incml( PPCB pPcb )
 * \brief increment medium low wyde
 * \author Ricardo Quesada
 */
STATUS token_incml( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 16;

	pPcb->p_mmix.reg[x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_incl( PPCB pPcb )
 * \brief increment low wyde
 * \author Ricardo Quesada
 */
STATUS token_incl( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	y = y *256 + z;

	r = y << 0 ;

	pPcb->p_mmix.reg[x] += r;
	
	INC_IP(4);
	return STATUS_SUCCESS;
}

/**** * JUMP * ****/

/**
 * \fn STATUS token_jmp( PPCB pPcb )
 * \brief jump relative address
 * \author Ricardo Quesada
 */
STATUS token_jmp( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (x<<16) + (y<<8) + z ;

	pPcb->p_ip += (long) r;
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_jmpb( PPCB pPcb )
 * \brief jump relative address backwards
 * \author Ricardo Quesada
 */
STATUS token_jmpb( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (x<<16) + (y<<8) + z ;

	pPcb->p_ip -= (long) r;
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_go( PPCB pPcb )
 * \brief jump absolute address
 * \author Ricardo Quesada
 */
STATUS token_go( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + pPcb->p_mmix.reg[z];

	pPcb->p_mmix.reg[x] = pPcb->p_ip + 4;

	pPcb->p_ip = (long) r;
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief jump absolute address inmmediate
 * \author Ricardo Quesada
 */
STATUS token_goi( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = pPcb->p_mmix.reg[y] + z;

	pPcb->p_mmix.reg[x] = pPcb->p_ip + 4;

	pPcb->p_ip = (long) r;
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief branch if negative
 * \author Ricardo Quesada
 */
STATUS token_bn( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( pPcb->p_mmix.reg[x] < 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief branch if zero
 * \author Ricardo Quesada
 */
STATUS token_bz( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( pPcb->p_mmix.reg[x] == 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bn( PPCB pPcb )
 * \brief branch if possitive
 * \author Ricardo Quesada
 */
STATUS token_bp( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( pPcb->p_mmix.reg[x] > 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bod( PPCB pPcb )
 * \brief branch if odd
 * \author Ricardo Quesada
 */
STATUS token_bod( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( (pPcb->p_mmix.reg[x] % 2) == 1)
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bnn( PPCB pPcb )
 * \brief branch non negative
 * \author Ricardo Quesada
 */
STATUS token_bnn( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( pPcb->p_mmix.reg[x] >= 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}
/**
 * \fn STATUS token_bnz( PPCB pPcb )
 * \brief branch non zero
 * \author Ricardo Quesada
 */
STATUS token_bnz( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( pPcb->p_mmix.reg[x] != 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS token_bnp( PPCB pPcb )
 * \brief branch if non possitive
 * \author Ricardo Quesada
 */
STATUS token_bnp( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( pPcb->p_mmix.reg[x] <= 0 )
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS token_bev( PPCB pPcb )
 * \brief branch if even
 * \author Ricardo Quesada
 */
STATUS token_bev( PPCB pPcb )
{
	signed long long r;
	int x,y,z;

	LOAD_XYZ(x,y,z);

	r = (y<<8) + z ;

	if( (pPcb->p_mmix.reg[x] % 2) == 0)
		pPcb->p_ip += r;
	else
		INC_IP(4);
	
	return STATUS_SUCCESS;
}

/**** * INTERRUPTS * ****/
/**
 * \fn STATUS token_trap( PPCB pPcb )
 * \brief trap
 * \author Ricardo Quesada
 */
STATUS token_trap( PPCB pPcb )
{
	STATUS s;
	int x;

	LOAD_X(x);

	if( x > NRSYSCALLS || x < 0 )
		return STATUS_INVALID;

	s = syscalls[x].funcion( pPcb );

	INC_IP(4);
	return s;
}

STATUS pre_trap( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice )
{
	int i;

	for( i=0;i<NRSYSCALLS;i++) {
		if( strlen( syscalls[i].nombre)==strlen(s1) &&
			strncasecmp(  syscalls[i].nombre, s1, strlen(syscalls[i].nombre))==0) {
			*i1 = i;
			if( syscalls[i].pre ) {
				syscalls[i].pre( s2, i2, s3, i3 );
			} else {
				*i2 = atoi( s2 );
				*i3 = atoi( s3 );
			}
		}
	}

	return STATUS_SUCCESS;
}
