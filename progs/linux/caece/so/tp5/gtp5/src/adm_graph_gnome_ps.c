#ifdef WITH_GNOME
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include "tipos.h"
#include "sched.h"
#include "adm_common.h"
#include "adm_graph_gnome_ps.h"


/* Para saber si debo o no crear una nueva ventana. */
//static GtkWidget *Wps = NULL;
static GtkWidget *clist = NULL;
static GtkWidget *create_Wps ( void );

int adm_graph_gnome_ps( TSPant *pTspant )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;

	int i = 0;

	gchar clist_data_tmp[ 3 ][10];
	gchar *clist_data[5];

	char *cpEstado[] = { "CREADO", "CORRIENDO", "DORMIDO", "PARADO", "ZOMBIE",
		"TERMINADO"};

	g_return_val_if_fail( pTspant != NULL, 0 );

	if( !clist )
	{
		create_Wps();
	}

	gtk_clist_freeze( GTK_CLIST( clist ) );

	/* TODO Seria mejor conseguir la cantidad de filas existentes, insertar las
	 * necesarias y despues comparar i con el valor inicial, si todavia quedan,
	 * entonces eliminarlas.*/
	gtk_clist_clear( GTK_CLIST( clist ) );

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) )
	{
		pPcb = proclist2pcb( pList );

		/* Convertimos las variables numericas */
		clist_data_tmp[0][0] = clist_data_tmp[1][0] = clist_data_tmp[2][0] = NULL;

		snprintf( clist_data_tmp[ 0 ], 10, "%ld", pPcb->p_t_run_start );
		snprintf( clist_data_tmp[ 1 ], 10, "%ld", pPcb->p_t_run_end );
		snprintf( clist_data_tmp[ 2 ], 10, "%ld", pPcb->p_cpu_usada );

		clist_data[0] = pPcb->p_nombre;
		clist_data[1] = cpEstado[ pPcb->p_estado ];
		clist_data[2] = clist_data_tmp[0];
		clist_data[3] = clist_data_tmp[1];
		clist_data[4] = clist_data_tmp[2];

/*
		if( pPcb->p_estado == PROC_TERMINADO ) {
			int tardo = pPcb->p_t_run_end - pPcb->p_t_run_start;
			wprintw( pTspant->pGPad," (%d%%)", (tardo * 100 )/ pPcb->p_cpu_usada );      }
			*/
		gtk_clist_insert( GTK_CLIST( clist ), i++, clist_data );
		pList = LIST_NEXT( pList );
	}

	gtk_clist_columns_autosize( GTK_CLIST( clist ) );
	gtk_clist_thaw( GTK_CLIST( clist ) );

	return 1;
}


GtkWidget *create_Wps( void )
{
	GtkWidget *Wps;
	GtkWidget *wps_vbox;
	GtkWidget *wps_ps_frm;
	GtkWidget *wps_ps_scrl;
  //GtkWidget *clist1;
  //GtkWidget *wps_action;
  //GtkWidget *wps_action_close_btn;

  char *cpTitle[] = { "Nombre", "Estado", "Start", "End", "Wasted" };


	Wps = gnome_dialog_new (_("Estado de los procesos (ps)"),
		GNOME_STOCK_BUTTON_CANCEL, NULL);

	gtk_widget_set_usize( GTK_WIDGET( Wps ), 300, -2 );

//  gtk_object_set_data (GTK_OBJECT (Wps), "Wps", Wps);
 // GTK_WINDOW (Wps)->type = GTK_WINDOW_DIALOG;
  gtk_window_set_policy (GTK_WINDOW (Wps), FALSE, TRUE, TRUE);

  wps_vbox = GNOME_DIALOG (Wps)->vbox;
  gtk_object_set_data (GTK_OBJECT (Wps), "wps_vbox", wps_vbox);
  gtk_widget_show (wps_vbox);

  /* Frame */
  wps_ps_frm = gtk_frame_new (_(" PS "));
  /*
  gtk_widget_ref (wps_ps_frm);
  gtk_object_set_data_full (GTK_OBJECT (Wps), "wps_ps_frm", wps_ps_frm,
                            (GtkDestroyNotify) gtk_widget_unref);
									 */
  gtk_widget_show (wps_ps_frm);
  gtk_box_pack_start (GTK_BOX (wps_vbox), wps_ps_frm, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (wps_ps_frm), 2);

  wps_ps_scrl = gtk_scrolled_window_new (NULL, NULL);
  /*gtk_widget_ref (wps_ps_scrl);
	gtk_object_set_data_full (GTK_OBJECT (Wps), "wps_ps_scrl", wps_ps_scrl,
		(GtkDestroyNotify) gtk_widget_unref);
		*/
  gtk_widget_show (wps_ps_scrl);
  gtk_container_add (GTK_CONTAINER (wps_ps_frm), wps_ps_scrl);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (wps_ps_scrl),
		GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

  /* Columned list */
  clist = gtk_clist_new_with_titles( 5, cpTitle );
  gtk_widget_show (clist);
  gtk_container_add (GTK_CONTAINER (wps_ps_scrl), clist);
  //gtk_clist_column_titles_show (GTK_CLIST (clist));


//  gnome_dialog_button_connect( GNOME_DIALOG( Wps ), 0,
//	on_wps_action_close_btn_clicked, NULL );
  /*
  wps_action = GNOME_DIALOG (Wps)->action_area;
  gtk_object_set_data (GTK_OBJECT (Wps), "wps_action", wps_action);
  gtk_widget_show (wps_action);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (wps_action), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (wps_action), 8);

  gnome_dialog_append_button (GNOME_DIALOG (Wps), GNOME_STOCK_BUTTON_CLOSE);
  wps_action_close_btn = g_list_last (GNOME_DIALOG (Wps)->buttons)->data;
  gtk_widget_ref (wps_action_close_btn);
  gtk_object_set_data_full (GTK_OBJECT (Wps), "wps_action_close_btn", wps_action_close_btn,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (wps_action_close_btn);
  GTK_WIDGET_SET_FLAGS (wps_action_close_btn, GTK_CAN_DEFAULT);
  */

  gtk_widget_show( Wps );
  return Wps;
}

#endif
