#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <assert.h>

#include "gtp4.h"
#include "cons.h"
#include "callbacks.h"
#include "adm_common.h"


void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_properties1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_tbone_new_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_tbone_help_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_action_tbar_play_btn_clicked        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_act_tbar_stop_btn_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_act_tbar_speed_scale_drag_end       (GtkWidget       *widget,
                                        GdkDragContext  *drag_context,
                                        gpointer         user_data)
{

}

/*
 * Entrada de texto por consola.
 * Esta rutina solo entra en efecto con el \c GNOME, con \c ncurses hacemos las
 * cosas de una manera \b muy distinta ( ver \c console_input y
 * \c pantalla_console.
 * \author Marcelo Picorelli.
 */
void on_console_frm_itext_activate (GtkEditable *editable, TSPant *pTspant )
{
	gchar  *cpBuffer = NULL;
	gchar **cpTokens = NULL;

	assert( editable != NULL );

	cpBuffer = gtk_editable_get_chars( GTK_EDITABLE( editable ), 0, -1 );
	g_strstrip( cpBuffer  );

	/* Leo la linea de comando, por ahora a lo sumo 2 palabras.*/
	cpTokens = g_strsplit( cpBuffer, " ", 2 );
	cons_command_execute( pTspant->pTCommands, cpTokens[0], cpTokens[1] );

	g_strfreev( cpTokens );
	g_free( cpBuffer );
}
