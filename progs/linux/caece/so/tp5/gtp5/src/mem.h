/**
 * \file mem.h
 * Declaraciones y estructura de mem
 */
#ifndef __MEM_H
#define __MEM_H

#define	MEM_FREE	0
#define	MEM_BUSY	1

#define	MEM_DATA	0
#define	MEM_CODE	0

typedef struct _mem_nodo {
	LIST_ENTRY	next;
	char	*ptr;
	int	tot;
	int	status;
	PPCB	pPcb;
} MEM_NODO, *PMEM_NODO;
extern int	mem_code_tot;

STATUS get_mem( PPCB pPcb );
STATUS free_mem( PPCB pPcb );
STATUS init_mem();
STATUS deinit_mem();
PLIST_ENTRY ptr_mem( void ); /**/

#endif /* __MEM_H */
