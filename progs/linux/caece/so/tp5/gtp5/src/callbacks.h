#include <gnome.h>
#include "adm_common.h"


void
on_open1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_properties1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_tbone_new_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_tbone_help_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_action_tbar_play_btn_clicked        (GtkButton       *button,
                                        gpointer         user_data);

void
on_act_tbar_stop_btn_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_act_tbar_speed_scale_drag_end       (GtkWidget       *widget,
                                        GdkDragContext  *drag_context,
                                        gpointer         user_data);

void on_console_frm_itext_activate (GtkEditable *editable, TSPant *pTspant);
