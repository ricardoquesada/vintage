/*
 * Trabajo Pr�ctico N�mero 4.
 * Sistemas Operativos.
 * Grupo MRM.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>
#include "gmetrica.h"

GtkWidget *create_WEstado ( void )
{
  GtkWidget *WEstado;
  GtkWidget *dialog_vbox1;
  GtkWidget *estado_main_vbox;
  GtkWidget *estado_global_frm;
  GtkWidget *estado_global_tbl;
  GtkWidget *label;
  GtkWidget *entry2;
  GtkWidget *estado_global_clock;
  GtkWidget *estado_proc_frm;
  GtkWidget *estado_proc_tbl;
  GtkWidget *estado_proc_parados;
  GtkWidget *estado_proc_dormidos;
  GtkWidget *estado_proc_ejecutando;
  GtkWidget *estado_proc_terminados;
  GtkWidget *estado_cpu_frm;
  GtkWidget *estado_cpu_tbl;
  GtkWidget *estado_cpu_0;
  GtkWidget *estado_cpu_1;
  GtkWidget *estado_cpu_2;
  GtkWidget *estado_cpu_3;
  GtkWidget *dialog_action_area;
  GtkWidget *estado_close_btn;

  WEstado = gnome_dialog_new (_("Estados"), NULL);
  GTK_WINDOW (WEstado)->type = GTK_WINDOW_DIALOG;
  gtk_window_set_position (GTK_WINDOW (WEstado), GTK_WIN_POS_MOUSE);
  gtk_window_set_policy (GTK_WINDOW (WEstado), FALSE, TRUE, TRUE);
  gtk_widget_set_usize (WEstado, 122, -2);

  dialog_vbox1 = GNOME_DIALOG (WEstado)->vbox;
  gtk_object_set_data (GTK_OBJECT (WEstado), "dialog_vbox1", dialog_vbox1);
  gtk_widget_show (dialog_vbox1);

  estado_main_vbox = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (estado_main_vbox);
  gtk_box_pack_start (GTK_BOX (dialog_vbox1), estado_main_vbox, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (estado_main_vbox), 2);

  estado_global_frm = gtk_frame_new (_(" Global "));
  gtk_widget_show (estado_global_frm);
  gtk_box_pack_start (GTK_BOX (estado_main_vbox), estado_global_frm, TRUE,
	FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (estado_global_frm), 2);

  estado_global_tbl = gtk_table_new (2, 2, FALSE);
  gtk_widget_show (estado_global_tbl);
  gtk_container_add (GTK_CONTAINER (estado_global_frm), estado_global_tbl);
  gtk_container_set_border_width (GTK_CONTAINER (estado_global_tbl), 4);
  gtk_table_set_row_spacings (GTK_TABLE (estado_global_tbl), 2);
  gtk_table_set_col_spacings (GTK_TABLE (estado_global_tbl), 3);

  /*
	* GLOBALES
	*/
  label = gtk_label_new (_("Clock"));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_global_tbl), label, 0, 1, 0, 1,
		(GtkAttachOptions) (GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5 );

  label = gtk_label_new (_("FreeMem"));
  gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_global_tbl), label, 0, 1, 1, 2,
		(GtkAttachOptions) (GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5 );

  /* FreeMem */
  entry2 = gtk_entry_new();
  gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_global_freemem", entry2 );
  gtk_widget_show (entry2);
	gtk_table_attach (GTK_TABLE (estado_global_tbl), entry2, 1, 2, 1, 2,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
	gtk_entry_set_editable( GTK_ENTRY( entry2 ), FALSE );

	/* Clock */
	estado_global_clock = gtk_entry_new();
	gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_global_clock",
		estado_global_clock );
	gtk_widget_show (estado_global_clock);

	gtk_table_attach (GTK_TABLE (estado_global_tbl), estado_global_clock, 1, 2, 0,
		1, (GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
	gtk_entry_set_editable (GTK_ENTRY (estado_global_clock), FALSE);

	/*
	 * PROCESOS
	 */
  estado_proc_frm = gtk_frame_new (_(" Procesos "));
  gtk_widget_show (estado_proc_frm);
  gtk_box_pack_start(GTK_BOX(estado_main_vbox), estado_proc_frm, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (estado_proc_frm), 2);

  estado_proc_tbl = gtk_table_new (4, 2, FALSE);
  gtk_widget_show (estado_proc_tbl);
  gtk_container_add (GTK_CONTAINER (estado_proc_frm), estado_proc_tbl);
  gtk_container_set_border_width (GTK_CONTAINER (estado_proc_tbl), 4);
  gtk_table_set_row_spacings (GTK_TABLE (estado_proc_tbl), 2);
  gtk_table_set_col_spacings (GTK_TABLE (estado_proc_tbl), 4);

	label = gtk_label_new (_("Parados"));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_proc_tbl), label, 0, 1, 0, 1,
		(GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
		(GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	label = gtk_label_new (_("Dormidos"));
	gtk_widget_show( label );
	gtk_table_attach (GTK_TABLE (estado_proc_tbl), label, 0, 1, 1, 2,
		(GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
		(GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	label = gtk_label_new (_("Ejecutando"));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_proc_tbl), label, 0, 1, 2, 3,
		(GtkAttachOptions) (GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	label = gtk_label_new (_("Terminados"));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_proc_tbl), label, 0, 1, 3, 4,
		(GtkAttachOptions) (GTK_FILL | GTK_SHRINK),
		(GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	/* Parados */
	estado_proc_parados = gtk_entry_new ();
	gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_proc_parados",
		estado_proc_parados );
	gtk_widget_show (estado_proc_parados);

	gtk_table_attach (GTK_TABLE (estado_proc_tbl), estado_proc_parados, 1, 2, 0, 1,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
	gtk_entry_set_editable (GTK_ENTRY (estado_proc_parados), FALSE);

	estado_proc_dormidos = gtk_entry_new ();
	gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_proc_dormidos",
		estado_proc_dormidos );
	gtk_widget_show (estado_proc_dormidos);
	gtk_table_attach(GTK_TABLE (estado_proc_tbl), estado_proc_dormidos, 1, 2, 1, 2,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_editable (GTK_ENTRY (estado_proc_dormidos), FALSE);

  estado_proc_ejecutando = gtk_entry_new ();
	gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_proc_ejecutando",
		estado_proc_ejecutando );
  gtk_widget_show (estado_proc_ejecutando);
	gtk_table_attach (GTK_TABLE (estado_proc_tbl), estado_proc_ejecutando, 
		1, 2, 2, 3, (GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_editable (GTK_ENTRY (estado_proc_ejecutando), FALSE);

	estado_proc_terminados = gtk_entry_new ();
	gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_proc_terminados",
		estado_proc_terminados );
	gtk_widget_show (estado_proc_terminados);
	gtk_table_attach (GTK_TABLE (estado_proc_tbl), estado_proc_terminados,
		1, 2, 3, 4, (GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
	gtk_entry_set_editable (GTK_ENTRY (estado_proc_terminados), FALSE);


	/* CPU's */
  estado_cpu_frm = gtk_frame_new (_(" CPU's "));
  gtk_widget_show (estado_cpu_frm);
  gtk_box_pack_start(GTK_BOX (estado_main_vbox), estado_cpu_frm, FALSE, FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (estado_cpu_frm), 2);

  estado_cpu_tbl = gtk_table_new (4, 2, FALSE);
  gtk_widget_show (estado_cpu_tbl);
  gtk_container_add (GTK_CONTAINER (estado_cpu_frm), estado_cpu_tbl);
  gtk_container_set_border_width (GTK_CONTAINER (estado_cpu_tbl), 4);
  gtk_table_set_row_spacings (GTK_TABLE (estado_cpu_tbl), 2);
  gtk_table_set_col_spacings (GTK_TABLE (estado_cpu_tbl), 3);

  label = gtk_label_new (_("3"));
  gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), label, 0, 1, 3, 4,
		(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);

  label = gtk_label_new (_("2"));
  gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), label, 0, 1, 2, 3,
		(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);

  label = gtk_label_new (_("1"));
  gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), label, 0, 1, 1, 2,
		(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);

  label = gtk_label_new (_("0"));
  gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), label, 0, 1, 0, 1,
		(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);

  estado_cpu_0 = gtk_entry_new ();
  gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_cpu_0", estado_cpu_0 );
  gtk_widget_show (estado_cpu_0);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), estado_cpu_0, 1, 2, 0, 1,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_editable (GTK_ENTRY (estado_cpu_0), FALSE);

	estado_cpu_1 = gtk_entry_new ();
	gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_cpu_1", estado_cpu_1 );
	gtk_widget_show (estado_cpu_1);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), estado_cpu_1, 1, 2, 1, 2,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_editable (GTK_ENTRY (estado_cpu_1), FALSE);

  estado_cpu_2 = gtk_entry_new ();
  gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_cpu_2", estado_cpu_2 );
  gtk_widget_show (estado_cpu_2);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), estado_cpu_2, 1, 2, 2, 3,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_editable (GTK_ENTRY (estado_cpu_2), FALSE);

  estado_cpu_3 = gtk_entry_new ();
  gtk_object_set_data( GTK_OBJECT( WEstado ), "estado_cpu_3", estado_cpu_3 );
  gtk_widget_show (estado_cpu_3);
	gtk_table_attach (GTK_TABLE (estado_cpu_tbl), estado_cpu_3, 1, 2, 3, 4,
		(GtkAttachOptions) (GTK_SHRINK | GTK_FILL),
		(GtkAttachOptions) (0), 0, 0);
  gtk_entry_set_editable (GTK_ENTRY (estado_cpu_3), FALSE);

  dialog_action_area = GNOME_DIALOG (WEstado)->action_area;
	gtk_object_set_data (GTK_OBJECT (WEstado), "dialog_action_area",
		dialog_action_area);
  gtk_widget_show (dialog_action_area);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area),
		GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area), 8);

  gnome_dialog_append_button (GNOME_DIALOG (WEstado), GNOME_STOCK_BUTTON_CLOSE);
  estado_close_btn = g_list_last (GNOME_DIALOG (WEstado)->buttons)->data;
  gtk_widget_ref (estado_close_btn);
  gtk_object_set_data_full (GTK_OBJECT (WEstado), "estado_close_btn",
	estado_close_btn, (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (estado_close_btn);
  GTK_WIDGET_SET_FLAGS (estado_close_btn, GTK_CAN_DEFAULT);

  /*
  gtk_signal_connect (GTK_OBJECT (WEstado), "close",
                      GTK_SIGNAL_FUNC (on_WEstado_close),
                      NULL);
							 */

  gtk_widget_show( WEstado );
  return WEstado;
}

