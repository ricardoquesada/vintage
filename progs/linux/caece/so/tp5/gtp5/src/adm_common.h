/*
 * Trabajo Practico Numero 5.
 * Sistemas Operativos.
 * Grupo MRM
 */
#ifndef _EST_PANTALLA_
#define _EST_PANTALLA_

#ifdef  WITH_NCURSES
#include <curses.h>
#elif WITH_GNOME
#include <gnome.h>
#endif

#include "cons.h"
#include "tipos.h"
#include "metric.h"


#ifdef WITH_GNOME
/**
 * Define la estructura a los \i widgets que vamos a utilizar.
 */
typedef struct _GTPWindow_
{
	GtkWidget *pGMainWindow;   /**< La ventana principal. */
	GtkWidget *pGOutput;       /**< El �rea donde se muestra el output de los
														       comandos y diversos mensajes.*/
/* GtkWidget *pGMetrics;   */ /**< La ventana donde se muestran
															 las m�tricas.*/
	GtkWidget *pConsole;       /**< El �rea de texto de la consola. */
}GTPWindow;
#endif


/**
 * est_pantalla.h
 * Esta estructura mantiene las propiedades que hacen al programa.
 */
typedef struct _TSPant_
{
#ifdef WITH_NCURSES
	WINDOW *pGPad;	/**< Regi�n de la pantalla donde se muestran los gr�ficos de
		evoluci�n de las �m�tricas?. */
	WINDOW *pGMetrics; /**< Regi�n de la pantalla donde se muestran las
		m�tricas.*/
	WINDOW *pGConsole; /**< Regi�n de la pantalla donde se muestran distintos
		mensajes y donde puede aparecer la consola.*/
	WINDOW *pGMetOptions; /**< Regi�n de la pantalla donde se muestran
		                las m�tricas disponibles (se seleccionan con el cursor )*/
#elif WITH_GNOME
	GTPWindow *pGWindow;
/**/	GtkWidget *pGMetrics;

#endif

	TConsoleBind **pTCommands;	/**< Commandos que acepta al estar en modo \b
		Consola.*/

	int (*ipGraph)( void *, void * );	/**< Rutina para mostrar el estado de los
		procesos. */

	PMETRIC	pTMetric; /**< Puntero a la m�trica que se est� dibujando.*/

//	int iClock; /**< Clock interno. */
	int iXLastPos; /**< Ultima posicion dibujada. */
}TSPant;


/* prototipos */
void pantalla_init( TSPant *pTspant );
void pantalla_end( TSPant *pTspant );
void pantalla_status_refresh( TSPant *pTspant );
int pantalla_console( TSPant *pTspant );
int pantalla_metric_draw_menu( TSPant *pTspant, int i );

#endif
