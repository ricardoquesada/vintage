/*
 * Trabajo Practico Numero 5.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * interfaz.c
 * Funciones que tienen que ver con la interfaz
 * \author Ricardo Quesada
 * \author Marcelo Picorelli
 */

#include <stdio.h>
#include <stdarg.h>

#include "tipos.h"
#include "sched.h"
#include "metric.h"
#include "interfaz.h"
#include "adm_comms.h"
#include "adm_graph.h"
#include "adm_common.h"

#if defined WITH_NCURSES || defined WITH_GNOME
static TSPant pantalla;
#endif /* WITH_NCURSES */

/**
 * \fn STATUS interfaz_gfx( PMETRIC pM )
 * \brief Muestra el estado de cada cola
 */
STATUS interfaz_gfx( PMETRIC pM )
{
#ifdef DEBUG_INTERFAZ
	printf("nombre:%s\n",pM->nombre);
	printf("min:%d\n",pM->min);
	printf("max:%d\n",pM->max);
	printf("promedio:%d\n",pM->prom);
	printf("current:%d\n",pM->cur);
#endif
#if defined WITH_NCURSES || defined WITH_GNOME
	pantalla.ipGraph( &pantalla, pM );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}

/**
 * STATUS interfaz_init()
 * Inicializa la interfaz
 */
STATUS interfaz_init()
{
#ifdef WITH_GNOME
	if( !(pantalla.pGWindow = (GTPWindow *)malloc( sizeof( GTPWindow ) ) ) )
		return STATUS_ERROR;
#endif
#if defined WITH_NCURSES || defined WITH_GNOME
	pantalla_init( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}

/**
 * STATUS interfaz_deinit()
 * Desinicializa la interfaz
 */
STATUS interfaz_deinit()
{
#if defined WITH_NCURSES || defined WITH_GNOME
	pantalla_end( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}


/**
 * Refresca la pantalla
 */
STATUS interfaz_refresh()
{
//#ifdef WITH_NCURSES
	pantalla_status_refresh( &pantalla );
//#endif
	return STATUS_SUCCESS;
}


/**
 * Muestra por consola un mensaje.
 */
STATUS console_out( char *str, ...)
{
   va_list args;
	char buf[2000];

	va_start(args, str);
	vsprintf(buf, str, args);
	va_end(args);

#ifdef WITH_NCURSES
	wprintw( pantalla.pGConsole, "%s", buf );
	wrefresh( pantalla.pGConsole );
#elif WITH_GNOME
	gtk_text_insert( GTK_TEXT( pantalla.pGWindow->pGOutput ), NULL, NULL,
		NULL, buf, -1 );
#else
	printf( buf );
#endif /* WITH_NCURSES */

	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_input();
 * \brief Espera una tecla por consola
 */
STATUS console_input()
{
#ifdef WITH_NCURSES
	int iKey;
	int iGraph = 0;
	PMETRIC pM = NULL;

	if( ( iKey = wgetch( pantalla.pGConsole )  ) != ERR )
	{
		switch( iKey )
		{
			case KEY_IC:
				pantalla_console( &pantalla );
				break;

			/* Metrica previa */
			case KEY_UP:
				iGraph = pantalla_metric_draw_menu( &pantalla, -1 );
				pM = metric_get( iGraph);
				adm_comms_graph( pM->nombre, &pantalla);
				wclear( pantalla.pGPad );
				/**/pantalla.ipGraph = adm_graph_metric;
				break;

			/* Metrica siguiente */
			case KEY_DOWN:
				iGraph = pantalla_metric_draw_menu( &pantalla, 1);
				pM = metric_get( iGraph);
				adm_comms_graph( pM->nombre, &pantalla);
				wclear( pantalla.pGPad );
				/**/pantalla.ipGraph = adm_graph_metric;
				break;

			/* Aumentar la velocidad (10%) */
			case KEY_RIGHT:
				if( ( speed - ( speed *.1 ) ) >= 100 )
					speed -= speed *.1;
				else
					speed = 100;

				console_out( pantalla.pGConsole, "Speed (%d)\n", speed );
				break;

			/* Disminuir la velocidad (10%) */
			case KEY_LEFT:
				speed += speed *.1;
				console_out( pantalla.pGConsole, "Speed (%d)\n", speed );
				break;

			/* Aumentar la escala (25%) */
			case KEY_NPAGE:
				pantalla.pTMetric->scale_y *= 1.25;
				wclear( pantalla.pGPad );
				console_out( pantalla.pGConsole, "Zoom In 25% (zoom factor %.3f)\n",
					pantalla.pTMetric->scale_y );
				break;

			case KEY_PPAGE:
				pantalla.pTMetric->scale_y *= .75;
				wclear( pantalla.pGPad );
				console_out( pantalla.pGConsole, "Zoom Out 25% (zoom factor %.3f)\n",
					pantalla.pTMetric->scale_y );
				break;
		}
	}
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}
