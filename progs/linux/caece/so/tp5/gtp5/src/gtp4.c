#ifdef WITH_GNOME	/* COmo se hace una compilacion condicional con el make??*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <gnome.h>

#include "gtp4.h"
#include "callbacks.h"
#include "adm_common.h"

static GnomeUIInfo file1_menu_uiinfo[] =
{
  GNOMEUIINFO_MENU_OPEN_ITEM (on_open1_activate, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo view1_menu_uiinfo[] =
{
  GNOMEUIINFO_END
};

static GnomeUIInfo settings1_menu_uiinfo[] =
{
  GNOMEUIINFO_MENU_PREFERENCES_ITEM (on_preferences1_activate, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo menubar_uiinfo[] =
{
  GNOMEUIINFO_MENU_FILE_TREE (file1_menu_uiinfo),
  GNOMEUIINFO_MENU_PROPERTIES_ITEM (on_properties1_activate, NULL),
  GNOMEUIINFO_MENU_VIEW_TREE (view1_menu_uiinfo),
  GNOMEUIINFO_MENU_SETTINGS_TREE (settings1_menu_uiinfo),
  GNOMEUIINFO_END
};

/* FIXME DEVOLVER STATUS */
void create_tp4_main ( TSPant *pTspant )
{
  GtkWidget *tp4_main;
  GtkWidget *dock1;
  GtkWidget *toolbar_one;
  GtkWidget *tbone_new_btn;
  GtkWidget *tbone_help_btn;
  GtkWidget *action_tbar_play_btn;
  GtkWidget *act_tbar_stop_btn;
  GtkWidget *tbar_aux_vbox;
  GtkWidget *hscale2;
  GtkWidget *tbar_speed_lbl;
  GtkWidget *vbox1;
  GtkWidget *out_frm;
  GtkWidget *out_frm_scrl;
  GtkWidget *out_frm_text;
  GtkWidget *console_frm;
  GtkWidget *console_frm_itext;
  GtkWidget *appbar1;


  tp4_main = gnome_app_new ("Gtp4", NULL);
//  gtk_object_set_data (GTK_OBJECT (tp4_main), "tp4_main", tp4_main);
	gtk_widget_set_uposition( tp4_main, 20, 20 );
	gtk_widget_set_usize( tp4_main, 400, 250 );

  dock1 = GNOME_APP (tp4_main)->dock;
  gtk_widget_show (dock1);

  gnome_app_create_menus (GNOME_APP (tp4_main), menubar_uiinfo);

  /* Creaci�n del toolbar */
  toolbar_one= gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
  gtk_widget_show (toolbar_one);

	gnome_app_add_toolbar (GNOME_APP (tp4_main), GTK_TOOLBAR (toolbar_one),
		"toolbar_one", GNOME_DOCK_ITEM_BEH_EXCLUSIVE,
		GNOME_DOCK_TOP, 1, 0, 0);
	gtk_toolbar_set_space_size (GTK_TOOLBAR (toolbar_one), 20);
	gtk_toolbar_set_space_style (GTK_TOOLBAR (toolbar_one),
		GTK_TOOLBAR_SPACE_LINE);

	tbone_new_btn = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar_one),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL, _("Nuevo"), _("Muestra una nueva m�trica"), NULL,
		gnome_stock_pixmap_widget (tp4_main, GNOME_STOCK_PIXMAP_NEW),
		NULL, NULL);

	/* M�trica nueva */
  gtk_widget_show (tbone_new_btn);
  gtk_container_set_border_width (GTK_CONTAINER (tbone_new_btn), 3);

  /* Ayuda */
	tbone_help_btn = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar_one),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL,
		_("Ayuda"),
		_("Muestra la lista de comandos"), NULL,
		gnome_stock_pixmap_widget (tp4_main, GNOME_STOCK_PIXMAP_HELP),
		NULL, NULL);
	gtk_widget_show (tbone_help_btn);
	gtk_container_set_border_width (GTK_CONTAINER (tbone_help_btn), 3);

	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar_one));

	/* Play */
	action_tbar_play_btn = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar_one),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL, _("Play"), NULL, NULL,
		gnome_stock_pixmap_widget (tp4_main, GNOME_STOCK_PIXMAP_FORWARD),
		NULL, NULL);
  gtk_widget_show (action_tbar_play_btn);
  gtk_container_set_border_width (GTK_CONTAINER (action_tbar_play_btn), 3);

  /* Stop */
	act_tbar_stop_btn = gtk_toolbar_append_element (GTK_TOOLBAR (toolbar_one),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL, _("Stop"), NULL, NULL,
		gnome_stock_pixmap_widget (tp4_main, GNOME_STOCK_PIXMAP_STOP),
		NULL, NULL);
  gtk_widget_show (act_tbar_stop_btn);
  gtk_container_set_border_width (GTK_CONTAINER (act_tbar_stop_btn), 3);


	/* VBoxes para la barra de velocidad */
  tbar_aux_vbox = gtk_vbox_new (FALSE, 4);
  gtk_widget_show (tbar_aux_vbox);
	gtk_toolbar_append_widget( GTK_TOOLBAR (toolbar_one), tbar_aux_vbox, NULL,
		NULL);

	hscale2 = gtk_hscale_new (GTK_ADJUSTMENT( gtk_adjustment_new( 100, 0, 100,
		1, 5, 0)));
	gtk_widget_show (hscale2);
	gtk_box_pack_start (GTK_BOX (tbar_aux_vbox), hscale2, TRUE, TRUE, 0);
	gtk_scale_set_value_pos (GTK_SCALE (hscale2), GTK_POS_RIGHT);
	gtk_scale_set_digits (GTK_SCALE (hscale2), 0);

  tbar_speed_lbl = gtk_label_new (_("Velocidad"));
  gtk_widget_show (tbar_speed_lbl);
  gtk_box_pack_start (GTK_BOX (tbar_aux_vbox), tbar_speed_lbl, FALSE, FALSE, 0);

  vbox1 = gtk_vbox_new (FALSE, 0);
  gtk_widget_show (vbox1);
  gnome_app_set_contents (GNOME_APP (tp4_main), vbox1);

  out_frm = gtk_frame_new (_(" Output "));
  gtk_widget_show (out_frm);
  gtk_box_pack_start (GTK_BOX (vbox1), out_frm, TRUE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (out_frm), 5);
  gtk_frame_set_shadow_type (GTK_FRAME (out_frm), GTK_SHADOW_IN);

  out_frm_scrl = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_show (out_frm_scrl);
  gtk_container_add (GTK_CONTAINER (out_frm), out_frm_scrl);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (out_frm_scrl),
		GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);

	/*
	 * Area de OUTPUT del texto. 
	 */
  out_frm_text = gtk_text_new (NULL, NULL);
  gtk_widget_show (out_frm_text);
  gtk_container_add (GTK_CONTAINER (out_frm_scrl), out_frm_text);

  console_frm = gtk_frame_new (_(" Consola "));
  gtk_widget_show (console_frm);
  gtk_box_pack_start (GTK_BOX (vbox1), console_frm, FALSE, TRUE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (console_frm), 5);

  console_frm_itext = gtk_entry_new ();
  gtk_widget_show (console_frm_itext);
  gtk_container_add (GTK_CONTAINER (console_frm), console_frm_itext);

  appbar1 = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
  gtk_widget_ref (appbar1);
  gtk_object_set_data_full (GTK_OBJECT (tp4_main), "appbar1", appbar1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (appbar1);
  gnome_app_set_statusbar (GNOME_APP (tp4_main), appbar1);

	gtk_signal_connect (GTK_OBJECT (tbone_new_btn), "clicked",
		GTK_SIGNAL_FUNC (on_tbone_new_btn_clicked),
		NULL);

	gtk_signal_connect (GTK_OBJECT (tbone_help_btn), "clicked",
		GTK_SIGNAL_FUNC (on_tbone_help_btn_clicked), NULL);

	gtk_signal_connect (GTK_OBJECT (action_tbar_play_btn), "clicked",
		GTK_SIGNAL_FUNC (on_action_tbar_play_btn_clicked), NULL);

	gtk_signal_connect (GTK_OBJECT (act_tbar_stop_btn), "clicked",
		GTK_SIGNAL_FUNC (on_act_tbar_stop_btn_clicked), NULL);

	gtk_signal_connect (GTK_OBJECT (console_frm_itext), "activate",
		GTK_SIGNAL_FUNC (on_console_frm_itext_activate), pTspant );

	pTspant->pGWindow->pGMainWindow = tp4_main;
	pTspant->pGWindow->pGOutput = out_frm_text;

	gtk_widget_show( tp4_main );

//  return main_window;
}
#endif
