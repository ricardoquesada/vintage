/*
 * Trabajo Practico Numero 5.
 * Sistemas Operativos.
 * Grupo MRM
 */


#ifdef WITH_NCURSES
#include <curses.h>
#include "adm_graph.h"
#elif WITH_GNOME
#include <gnome.h>
#include "adm_graph_gnome_ps.h"
#endif

#include <assert.h>
#include <stdlib.h>

#include "cons.h"
#include "tipos.h"
#include "archivo.h"
#include "interfaz.h"
#include "adm_comms.h"
#include "adm_common.h"

/**
 * \file adm_comms.c
 * Funciones para el manejo de pantalla
 */

static int adm_comms_help( char *cpArg, TSPantComms *data );

/**
 * Limpia la pantalla que se le indica.
 * En este momento solo es capaz de limpiar una pantalla a la vez, no acepta
 * par�metros, y no se puede modificar la ventana que limpia.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTClearWin La ventana a limpiar, no puede ser \c NULL.
 */
int adm_comms_clear( char *cpArg, TSPant *pTspant )
{
	assert( pTspant != NULL );
#ifdef WITH_NCURSES
	wclear( pTspant->pGPad );
	wrefresh( pTspant->pGPad );
#elif WITH_GNOME
	gtk_editable_delete_text( GTK_EDITABLE( pTspant->pGWindow->pGOutput ), 0, -1 );
#endif

	return 1;
}

/**
 * Wrapper para la funci�on ncola_graph_ps.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTspant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * \author Picorelli Marcelo.
 *
 */
int adm_comms_ps( char *cpArg, TSPant *pTspant )
{
	assert( pTspant != NULL );
#ifdef WITH_NCURSES
	wclear( pTspant->pGPad );
	pTspant->ipGraph = adm_graph_ps;
#elif WITH_GNOME
	/* TODO Hacer que cree una ventana nueva y que lo meta en un vector de
	 * graficas */
	pTspant->ipGraph = adm_graph_gnome_ps;
#endif

	return 1;
}



/**
 * Cambia la velocidad del clock interno.
 * @param cpArg La nueva velocidad.
 * @param pTspant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * \bug No estoy revisando bien el parametro, capaz que falle la conversi�n.
 * \author Picorelli Marcelo.
 */
int adm_comms_clock_set_speed( char *cpArg, TSPant *pTspant )
{
	assert( pTspant != NULL );

	if( cpArg && strlen( cpArg ) )
	{
		speed = atoi( cpArg );
		console_out( "Nueva velocidad del clock = %d\n", speed );
	} else
	{
		console_out( "Velocidad del clock = %d\n", speed );
	}
	return 1;
}


/**
 * \fn int adm_comms_killproc( char *cpArg, TSPant *pTspant )
 * \brief Mata a un proceso (por consola)
 * \param cpArg La nueva velocidad.
 * \param pTspant Es la estructura que mantiene todos los datos del programa,
 * \author Ricardo Quesada
 */
int adm_comms_killproc( char *cpArg, TSPant *pTspant )
{
        int id;
        assert( pTspant != NULL );

        if( strlen( cpArg ) ) {
                id = atoi( cpArg );
                if( kill_procid( id ) != STATUS_SUCCESS )
                        console_out( "Id inexistente\n" );
                else
                        console_out( "Proceso %d matado\n",id );
        }
        return 1;
}


int adm_comms_exit( char *cpArg, TSPant *pTspant )
{
        seguir = 0;
        return 1;
}


#ifdef WITH_NCURSES
/**
 * Brinda ayuda sobre el comando que se le pide.
 * Busca el comando que se le pasa y de encontrarlo, busca la descripci�n del
 * mismo mostr�ndolo en la ventana que se le indica. De no encotrar el
 * comando, muestra un mensaje de error predefinido.
 * @param TSPant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * @param cpArg Es el nombre del comando, y es proporcionado por la
 * aplicaci�n, no por el programador.
 * @param pTWindow Ventana en la cual se desplegar� el resultado del comando,
 *		no puede ser \c NULL.
 * @returns 1 En caso de �xito, 0 en caso de falla.
 */
void adm_comms_add_help( TSPant *pTspant, WINDOW *pDisplayWin )
{
	static TSPantComms TData;

	assert( pTspant != NULL );
	assert( pDisplayWin != NULL );

	TData.pTspant = pTspant;
	TData.pTWindow = pDisplayWin;

	cons_command_add( &pTspant->pTCommands, "help",
		"Muestra la ayuda sobre un comando",
		adm_comms_help, &TData );
}

/**
 * Wrapper para la funci<E7>on ncola_graph_mem.
 * @param cpArg Par<E1>metro en com<FA>n de todos los \e callbacks, ignorado.
 * @param pTspant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci<F3>n, no por el programador.
 * \author Picorelli Marcelo.
 *
 */
int adm_comms_mem( char *cpArg, TSPant *pTspant )
{
        assert( pTspant != NULL );
        wclear( pTspant->pGPad );
        pTspant->ipGraph = adm_graph_mem;

        return 1;
}

/**
 * \fn int adm_comms_sem( char *cpArg, TSPant *pTspant )
 * \brief Selecciona la metrica 'sem' (Mostrador del estado de los semaforos)
 * \param cpArg Par<E1>metro en com<FA>n de todos los \e callbacks, ignorado.
 * \param pTspant Es la estructura que mantiene todos los datos del programa,
 * \returns 1 En caso de <E9>xito, -1 en caso de error;
 * \author Ricardo Quesada
 */
int adm_comms_sem( char *cpArg, TSPant *pTspant )
{
        assert( pTspant != NULL );
        wclear( pTspant->pGPad );
        pTspant->ipGraph = adm_graph_sem;

        return 1;
}


/**
 * Muestra todos los comandos disponibles.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTspant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * \author Picorelli Marcelo.
 */
int adm_comms_list( char *cpArg, TSPant *pTspant )
{
	int i;
	TConsoleBind *pTCommand;
	TConsoleBind **pComms = pTspant->pTCommands;

	assert( pTspant != NULL );

	wclear( pTspant->pGPad );
	waddstr( pTspant->pGPad, "Lista de comandos\n\n" );
	for( i = 0; pComms[i]; i++ )
	{
		pTCommand = cons_command_get( pComms, pComms[i]->cpCommand );
		wprintw( pTspant->pGPad, "%s: %s\n", pComms[i]->cpCommand,
			pTCommand->cpHelp ? pTCommand->cpHelp : "No hay ayuda disponible." );
	}
	waddstr( pTspant->pGPad, "Presione una tecla." );
	wrefresh( pTspant->pGPad );

	nodelay( pTspant->pGPad, FALSE );
	wgetch( pTspant->pGPad );
	nodelay( pTspant->pGPad, TRUE );
	wclear( pTspant->pGPad );

	return 1;
}

/**
 * Brinda ayuda sobre el comando que se le pide.
 * Busca el comando que se le pasa y de encontrarlo, busca la descripci<F3>n del
 * mismo mostr<E1>ndolo en la ventana que se le indica. De no encotrar el
 * comando, muestra un mensaje de error predefinido.
 * @param TSPant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci<F3>n, no por el programador.
 * @param cpArg Es el nombre del comando, y es proporcionado por la
 * aplicaci<F3>n, no por el programador.
 * @param pTWindow Ventana en la cual se desplegar<E1> el resultado del comando,
 *              no puede ser \c NULL.
 * @returns 1 En caso de <E9>xito, 0 en caso de falla.
 */
static int adm_comms_help( char *cpArg, TSPantComms *pTsComms )
{
        TSPant  *pTspant = pTsComms->pTspant;
        TConsoleBind *pTCommand = NULL;
        TConsoleBind **pComms = pTspant->pTCommands;

        assert( pTspant != NULL );

        if( cpArg )
        {
                if( (pTCommand = cons_command_get( pComms, cpArg )) )
                {
                        if( pTCommand->cpHelp )
                        {
                                wprintw( pTsComms->pTWindow, "%s: %s\n", cpArg, 
pTCommand->cpHelp );
                        }
                        else
                        {
                                wprintw( pTsComms->pTWindow, "%s: %s\n", cpArg, 
ADM_COMMS_NOHELP );
                        }

                        wrefresh( pTsComms->pTWindow );
                        return 1;
                }
                else
                {
                        adm_comms_list( cpArg, pTspant );

                }
        }

        return 1;
}


/**
 * Permite seleccionar la m�trica a graphicar.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTspant Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * @returns 1 En caso de �xito, -1 en caso de error;
 * \author Picorelli Marcelo.
 *
 */
int adm_comms_graph( char *cpArg, TSPant *pTspant )
{
	int iMetr;
	PMETRIC pTMetric = NULL;

	assert( pTspant != NULL );

	if( !cpArg )
		return -1;

	/* Tratamos de interpretar el argumento como un n�mero */
	/* El problema es que si queremos ver la metrica cero no funciona */
	if( atoi( cpArg ) )
	{
		if( ( pTMetric = metric_get( atoi( cpArg ) ) ) != NULL )
		{
			pTspant->pTMetric = pTMetric;
			console_out( "OK: Nueva m�trica %s\n", cpArg );
			wclear( pTspant->pGPad );
			return 1;
		}
	}

	/* Si pidieron la m�trica como un nombre, pasamos por ac�. */
	if( ( iMetr = metric_id_get( cpArg ) ) != -1 )
	{
		pTspant->pTMetric = metric_get( iMetr );
		console_out( "OK: Nueva m�trica %s\n", cpArg );
		wclear( pTspant->pGPad );
		return 1;
	}

	console_out( "Error: La m�trica %s no existe\n", cpArg );
	return -1;
}
#endif
