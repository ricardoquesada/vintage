!
! Problema del barbero 
! Grupo MRM
!
! Modulo: Cashier
! 
! .param	code_len	data_len	stack_len
!
.version	2
.param	40	0	0
.sem	MAX_CAPACITY	20
.sem	SOFA		4
.sem	COORD		3
.sem	BARBER_CHAIR	3
.sem	CUST_READY	0
.sem	FINISHED	0
.sem	LEAVE_B_CHAIR	0
.sem	PAYMENT		0
.sem	RECEIPT		0


@main_loop
	trap	WAIT ,PAYMENT ,0
	trap	WAIT ,COORD ,0

	! Accept Pay
	setl	$0 ,12
@loop1
	incl	$0 ,-1
	bnz	$0 ,@loop1

	trap	SIGNAL ,COORD ,0
	trap	SIGNAL ,RECEIPT ,0

	! Forever
	jmp	@main_loop

	trap	EXIT ,0 ,0
.end
