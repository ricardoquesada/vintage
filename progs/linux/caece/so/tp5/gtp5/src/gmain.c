#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "gtp4.h"
#include "support.h"

int main(int argc, char *argv[])
{
  GtkWidget *tp4_main;

#ifdef ENABLE_NLS
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);
#endif

  gnome_init ("gtp4", VERSION, argc, argv);

  tp4_main = create_tp4_main ();
  gtk_widget_show (tp4_main);

  gtk_main ();
  return 0;
}
