/**
 * \file tokens.h
 */
#ifndef __TOKENS_H
#define __TOKENS_H

#include "sched.h"

STATUS syscall_exit( PPCB pPcb );
STATUS syscall_fork( PPCB pPcb );
STATUS syscall_read( PPCB pPcb );
STATUS syscall_write( PPCB pPcb );
STATUS syscall_wait( PPCB pPcb );
STATUS syscall_signal( PPCB pPcb );
STATUS syscall_kill( PPCB pPcb );


#define TOKEN_LDB	"ldb"
#define TOKEN_LDBI	"ldbi"
#define TOKEN_LDW	"ldw"
#define TOKEN_LDWI	"ldwi"
#define TOKEN_LDA	"lda"
#define TOKEN_LDAI	"ldai"
#define TOKEN_STB	"stb"
#define TOKEN_STBI	"stbi"
#define TOKEN_STW	"stw"
#define TOKEN_STWI	"stwi"
#define TOKEN_ADD	"add"
#define TOKEN_ADDI	"addi"
#define TOKEN_SUB	"sub"
#define TOKEN_SUBI	"subi"
#define TOKEN_MUL	"mul"
#define TOKEN_MULI	"muli"
#define TOKEN_DIV	"div"
#define TOKEN_DIVI	"divi"
#define TOKEN_BN	"bn"
#define TOKEN_BZ	"bz"
#define TOKEN_BP	"bp"
#define TOKEN_BOD	"bod"
#define TOKEN_BNN	"bnn"
#define TOKEN_BNZ	"bnz"
#define TOKEN_BNP	"bnp"
#define TOKEN_BEV	"bev"
#define TOKEN_SETH	"seth"
#define TOKEN_SETMH	"setmh"
#define TOKEN_SETML	"setml"
#define TOKEN_SETL	"setl"
#define TOKEN_INCH	"inch"
#define TOKEN_INCMH	"incmh"
#define TOKEN_INCML	"incml"
#define TOKEN_INCL	"incl"
#define TOKEN_JMP	"jmp"
#define TOKEN_JMPB	"jmpb"
#define TOKEN_GO	"go"
#define TOKEN_GOI	"goi"
#define TOKEN_TRAP	"trap"


STATUS token_ldb( PPCB pPcb );
STATUS token_ldbi( PPCB pPcb );
STATUS token_ldw( PPCB pPcb );
STATUS token_ldwi( PPCB pPcb );
STATUS token_stb( PPCB pPcb );
STATUS token_stbi( PPCB pPcb );
STATUS token_stw( PPCB pPcb );
STATUS token_stwi( PPCB pPcb );

STATUS token_add( PPCB pPcb );
STATUS token_addi( PPCB pPcb );
STATUS token_sub( PPCB pPcb );
STATUS token_subi( PPCB pPcb );
STATUS token_mul( PPCB pPcb );
STATUS token_muli( PPCB pPcb );
STATUS token_div( PPCB pPcb );
STATUS token_divi( PPCB pPcb );

STATUS token_bn( PPCB pPcb );
STATUS token_bz( PPCB pPcb );
STATUS token_bp( PPCB pPcb );
STATUS token_bod( PPCB pPcb );
STATUS token_bnn( PPCB pPcb );
STATUS token_bnz( PPCB pPcb );
STATUS token_bnp( PPCB pPcb );
STATUS token_bev( PPCB pPcb );

STATUS token_seth( PPCB pPcb );
STATUS token_setmh( PPCB pPcb );
STATUS token_setml( PPCB pPcb );
STATUS token_setl( PPCB pPcb );
STATUS token_inch( PPCB pPcb );
STATUS token_incmh( PPCB pPcb );
STATUS token_incml( PPCB pPcb );
STATUS token_incl( PPCB pPcb );


STATUS token_jmp( PPCB pPcb );
STATUS token_jmpb( PPCB pPcb );
STATUS token_go( PPCB pPcb );
STATUS token_goi( PPCB pPcb );

STATUS token_trap( PPCB pPcb );


STATUS pre_trap( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3, int indice );

#endif /* __TOKENS_H */
