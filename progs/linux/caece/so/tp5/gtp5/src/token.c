/*
 * Trabajo Practico Numero 4.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "interfaz.h"
#include "load.h"
#include "token.h"
#include "tokens.h"
#include "sem.h"
#include "label.h"

#undef TOKEN_DEBUG
/**
 * \file token.c
 * pseudo 'JIT' Just In Time compiler.
 * Cuando se carga el programa, se lo compila a 'bytecode', 
 * y eso es lo que se guarda en memoria, y se ejecuta.
 * \author Ricardo Quesada
 */

struct _tokens {
	char *nombre;		/**< nombre del token */
	int params;		/**< parametros que recibe */
	char bytecode;		/**< codigo del bytecode */
	STATUS (*func)();	/**< funcion a llamar */
	STATUS (*pre)();	/**< funcion que hace algo antes de la compilacion */
} tokens[] = {
	/* Para que la ejecucion sea 'optimizada' hacer que el bytecode '0'
	 * este en el indice 0, el '1' en el 1, etc */

	{TOKEN_LDB	,3,0x80, token_ldb, NULL },
	{TOKEN_LDBI	,3,0x81, token_ldbi, NULL },
	{TOKEN_LDW	,3,0x80, token_ldw, NULL },
	{TOKEN_LDWI	,3,0x81, token_ldwi, NULL },
	{TOKEN_LDA	,3,0x22, token_add, NULL },
	{TOKEN_LDAI	,3,0x23, token_addi, NULL },
	{TOKEN_STB	,3,0x80, token_stb, NULL },
	{TOKEN_STBI	,3,0x81, token_stbi, NULL },
	{TOKEN_STW	,3,0x84, token_stw, NULL },
	{TOKEN_STWI	,3,0x85, token_stwi, NULL },
	{TOKEN_ADD	,3,0x84, token_add, NULL },
	{TOKEN_ADDI	,3,0x85, token_addi, NULL },
	{TOKEN_SUB	,3,0x85, token_sub, NULL },
	{TOKEN_SUBI	,3,0x85, token_subi, NULL },
	{TOKEN_MUL	,3,0x85, token_mul, NULL },
	{TOKEN_MULI	,3,0x85, token_muli, NULL },
	{TOKEN_DIV	,3,0x85, token_div, NULL },
	{TOKEN_DIVI	,3,0x85, token_divi, NULL },
	{TOKEN_BN	,2,0x85, token_bn, pre_branch },
	{TOKEN_BZ	,2,0x85, token_bz, pre_branch },
	{TOKEN_BP	,2,0x85, token_bp, pre_branch },
	{TOKEN_BOD	,2,0x85, token_bod, pre_branch },
	{TOKEN_BNN	,2,0x85, token_bnn, pre_branch },
	{TOKEN_BNZ	,2,0x85, token_bnz, pre_branch },
	{TOKEN_BNP	,2,0x85, token_bnp, pre_branch },
	{TOKEN_BEV	,2,0x85, token_bev, pre_branch },
	{TOKEN_SETH	,2,0x85, token_seth, NULL },
	{TOKEN_SETMH	,2,0x85, token_setmh, NULL },
	{TOKEN_SETML	,2,0x85, token_setml, NULL },
	{TOKEN_SETL	,2,0x85, token_setl, NULL },
	{TOKEN_INCH	,2,0x85, token_inch, NULL },
	{TOKEN_INCMH	,2,0x85, token_incmh, NULL },
	{TOKEN_INCML	,2,0x85, token_incml, NULL },
	{TOKEN_INCL	,2,0x85, token_incl, NULL },
	{TOKEN_JMP	,1,0x85, token_jmp, pre_jump },
	{TOKEN_JMPB	,1,0x85, token_jmpb, pre_jump },
	{TOKEN_GO	,3,0x85, token_go, pre_jump },
	{TOKEN_GOI	,3,0x85, token_goi, pre_jump },
	{TOKEN_TRAP	,3,0x85, token_trap, pre_trap },
};
#define	NRTOKENS  (sizeof(tokens)/sizeof(tokens[0]))


void pre_generic( char *s1, int *i1, char *s2, int *i2, char *s3, int *i3 )
{
	if( strlen(s1) != 0) {
		if( s1[0]=='$' )
			*i1 = atoi( &s1[1] );
		else
			*i1 = atoi( s1 ); 
	}

	if( strlen(s2) != 0) {
		if( s2[0]=='$' )
			*i2 = atoi( &s2[1] );
		else
			*i2 = atoi( s2 ); 
	}

	if( strlen(s3) != 0) {
		if( s3[0]=='$' )
			*i3 = atoi( &s3[1] );
		else
			*i3 = atoi( s3 ); 
	}
}

/**
 * \fn static STATUS get_specific( FILE *fp )
 * \brief Evalua codigo especifico del interprete
 * \author Ricardo Quesada
 */
static STATUS get_specific( FILE *fp )
{
	int r;
	char c;
	char quesoy[1000];

	PDEBUG("get_specific()");

read_loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	r=fread(  &c, 1 ,1, fp);
	if(r!=1 || c=='\n' || c==' ' || c=='\t')
		goto read_loop;

	/* Es un comando especial? */
	if( c=='.') {
		fscanf( fp,"%s\n", quesoy );
		if(strcasecmp( quesoy,"end")==0)
			return STATUS_END;
		else
			return STATUS_ERROR;
	}

	/* Es un REM? */
	if( c=='!' ) {
		while(c!='\n')
			fread(  &c, 1 ,1, fp);
		goto read_loop;
	}

	/* Es un label ?*/
	if( c=='@')
		return STATUS_LABEL;

	fseek( fp, -1 ,SEEK_CUR);

	/* Entonces es un mnemonico */
	return STATUS_COMMAND;
}

/**
 * \fn static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
 * \brief Convierte un string a un bytecode
 * \author Ricardo Quesada
 */
static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
{
	char buffer[1000];
	char token[100];
	char param1[100];
	char param2[100];
	char param3[100];
	int  iparam1;
	int  iparam2;
	int  iparam3;
	int i;
	int j;

	PDEBUG("convert_2_bytecode()");

	/*
	 * Dado que hay un bug horrible en libc hago esto para evitarlo
	 * 1ro un readline y luego un sscanf en vez de un fscanf
	 */

	memset(buffer,0,sizeof(buffer));
	{
		char c;
		int ii=0;
		
		do {
			fread(  &c, 1 ,1, fp);
			buffer[ii++]=c;
		} while( c !='\n');
	}


	j = sscanf(buffer, "%s %s , %s , %s\n",token,param1,param2,param3);

#ifdef TOKEN_DEBUG
	fprintf(stderr,"Bytecompiling '%s' '%d'\n",token,j);
#endif
	for(i = 0; i < NRTOKENS; i++) {
		if( strlen(token)==strlen(tokens[i].nombre) && 
			strncasecmp( tokens[i].nombre, token, strlen( tokens[i].nombre) )==0 ){

			if( tokens[i].pre )
				tokens[i].pre( param1, &iparam1, param2, &iparam2, param3, &iparam3, *indice );
			else {
				pre_generic( param1, &iparam1, param2, &iparam2, param3, &iparam3 );
			}

#ifdef WITH_MMIX_FULL_COMPATIBILITY
			pPcb->p_codeseg[(*indice)++] = tokens[i].bytecode;
#else
			pPcb->p_codeseg[(*indice)++] = i;
#endif

			if( tokens[i].params == 1 )  {
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x00ff0000);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x0000ff00);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x000000ff);
			}

			else if( tokens[i].params == 2 ) {
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x000000ff);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam2 & 0x0000ff00);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam2 & 0x000000ff);
			}

			else if( tokens[i].params == 3 ) {
				pPcb->p_codeseg[(*indice)++] = (char)(iparam1 & 0x000000ff);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam2 & 0x000000ff);
				pPcb->p_codeseg[(*indice)++] = (char)(iparam3 & 0x000000ff);
			}

			else return STATUS_UNEXPECTED;

			return STATUS_SUCCESS;
		}
	}
	console_out("Error en %s: Token '%s' no encontrado\n",pPcb->p_nombre,token);
	return STATUS_ERROR;
}

/**
 * \fn STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
 * \brief Funcion que se encarga de convertir a 'bytecodes' un programa
 * \author Ricardo Quesada
 */
STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
{
	int indice=0;

	PDEBUG("tokenize_to_mem()");


	label_init();
loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	/* es algun valor del interprete */
	switch( get_specific( fp )) {
		case STATUS_END:
			label_deinit();
			return STATUS_SUCCESS;
		case STATUS_FILEEOF:
			console_out("Error: EOF\n");
			label_deinit();
			return STATUS_FILEEOF;
		case STATUS_LABEL:
			label_add( fp, indice );
			break;
		case STATUS_COMMAND:
			if( convert_2_bytecode( pPcb, fp, &indice ) != STATUS_SUCCESS ){
				label_deinit();
				return STATUS_ERROR;
			}
			break;
		default:
			break;
	}
	goto loop;
}

/**
 * \fn STATUS run_proc( PPCB pPcb )
 * \brief Hace 'andar' los procesos
 * \author Ricardo Quesada
 */
STATUS run_proc( PPCB pPcb )
{
	char t = pPcb->p_codeseg[pPcb->p_ip];

	pPcb->p_cpu_usada++;

	if( t >= NRTOKENS ) {
		console_out("Instruccion invalida en: %s\n",pPcb->p_nombre);
		return STATUS_INVALID;
	}

	return (tokens[(int)t].func)(pPcb);
}
