/*
 * Trabajo Practico Numero 5.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file main.c
 * Archivo que tiene el main loop y la funcion main.
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "metric.h"
#include "interfaz.h"
#include "load.h"
#include "events.h"
#include "mem.h"
#include "sem.h"

#ifdef WITH_GNOME
#include <gnome.h>
#endif


int speed = 200000;
int seguir = 1;

/**
 * \fn void Sleep( int usec )
 * \brief Un 'sleep' mas fino y portable
 * \param microsegundos a esperar
 * \author Ricardo Quesada
 */
void Sleep( int usec )
{
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = usec;
	select(0,NULL,NULL,NULL,&tv);
}

/**
 * \fn STATUS main_loop(void)
 * \brief Main loop (que mas)
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS main_loop(void)
{
	PCB old_pcb;
	PPCB pPcb;

	while( seguir ) {
#ifdef DEBUG
		printf("--> ciclo %d <--\n",(unsigned)jiffies);
#endif
		console_input();

		memset( &old_pcb, 0, sizeof(PCB));
		while( archivo_read( &old_pcb ) == STATUS_SUCCESS ) {
			/* Este PCB creado aca, es el que se va a encolar! */
			pPcb = malloc( sizeof(PCB));
			if( pPcb ) {
				*pPcb = old_pcb;
				if( load( pPcb ) == STATUS_SUCCESS )
					queue_proc( pPcb );
				else
					free(pPcb);
			}
		}

		run();				/* scheduling */

		procesar_events();		/* I/O */

		metric_display_all();		/* mostrar en pantalla */

#ifdef WITH_GNOME
		while( gtk_events_pending() )
			gtk_main_iteration();
#endif

		Sleep(speed);
	}

	return STATUS_SUCCESS;
}

/**
 * \fn help
 * \brief Muestra el modo de uso
 */
void help( char *nom)
{
	printf("Modo de uso:\n");
	printf("\t%s [archivo_de_conf]\n",nom);
	printf("\tSi se omite el archivo_de_conf, buscara el archivo 'tp.ini' por default.\n");
	printf("\tY si el archivo 'tp.ini' no se encuentra, mostrara este mensaje.\n");
	printf("\n\nEjemplos:\n");
	printf("\t%s filosofos.ini\n",nom);
	printf("\t%s barbero.ini\n",nom);
	printf("\t%s general.ini\n",nom);
}

/**
 * \fn int main( void )
 * \brief su nombre lo dice todo
 */
int main( int argc, char **argv )
{
#ifdef WITH_GNOME
	gnome_init( "TP5", "4.1", argc, argv );
#endif
	printf("\n\nTP5 - Sistemas Operativos - Grupo MRM\n");
	if( argc == 1 ) {
		if( archivo_init("tp.ini") != STATUS_SUCCESS ) {
			help(argv[0]),exit(1);
		}
	} else if (argc == 2 ) {
		if( archivo_init(argv[1]) != STATUS_SUCCESS ) {
			printf("Error al abrir el archivo '%s'\n",argv[1]);
			help(argv[0]),exit(1);
		}
	} else
		help(argv[0]),exit(1);

	sched_init();
	init_events();
	metric_init();
	interfaz_init();
	init_mem();
	sem_init();

	main_loop();

	interfaz_deinit();
	archivo_close();
	metric_flush();
	deinit_mem();

	return 0;
}
