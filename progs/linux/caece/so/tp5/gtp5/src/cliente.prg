!
! Problema del barbero 
! Grupo MRM
!
! Modulo: Customer
! 
! .param	code_len	data_len	stack_len
!
.version	2
.param	160	0	0
.sem	MAX_CAPACITY	20
.sem	SOFA		4
.sem	COORD		3
.sem	BARBER_CHAIR	3
.sem	CUST_READY	0
.sem	FINISHED	0
.sem	LEAVE_B_CHAIR	0
.sem	PAYMENT		0
.sem	RECEIPT		0


	trap	WAIT ,MAX_CAPACITY ,0

	! Enter shop
	setl	$0 ,5
@loop1
	incl	$0 ,-1
	bnz	$0 ,@loop1

	trap	WAIT ,SOFA ,0

	! Sit on sofa
	setl	$0 ,3
@loop2
	incl	$0 ,-1
	bnz	$0 ,@loop2

	trap	WAIT ,BARBER_CHAIR ,0
	
	! Get up from sofa
	setl	$0 ,2
@loop3
	incl	$0 ,-1
	bnz	$0 ,@loop3

	trap	SIGNAL ,SOFA ,0

	! Sit in barber chair
	setl	$0 ,4
@loop4
	incl	$0 ,-1
	bnz	$0 ,@loop4

	trap 	SIGNAL ,CUST_READY ,0
	trap	WAIT ,FINISHED ,0

	trap	SIGNAL ,COORD ,0
	trap	SIGNAL ,RECEIPT ,0

	! leave barber chair
	setl	$0 ,5
@loop5
	incl	$0 ,-1
	bnz	$0 ,@loop5

	trap	SIGNAL ,LEAVE_B_CHAIR ,0

	! pay
	setl	$0 ,8
@loop6
	incl	$0 ,-1
	bnz	$0 ,@loop6

	trap	SIGNAL ,PAYMENT ,0

	trap	WAIT ,RECEIPT ,0

	! exit shop
	setl	$0 ,8
@loop7
	incl	$0 ,-1
	bnz	$0 ,@loop7

	trap 	SIGNAL ,MAX_CAPACITY ,0

	trap	EXIT ,0 ,0
.end
