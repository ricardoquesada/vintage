#ifdef WITH_GNOME
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include "adm_common.h"
#include "adm_graph_gnome_metrica.h"




#include "interfaz.h"

static GnomeCanvasGroup *group = NULL;
static GtkWidget *canvas1;
static void create_metric( void );


/**
 * Grafica las metricas.
 * Se encarga de realizar los graficos de las metricas, mostrando m�ximo,
 * m�nimo, promedio y actual de la m�trica que se le pasa.
 * @param pTspant Las variables que conforman al programa, no puede ser \c NULL.
 * @param pTMetric Las variables que hacen a la m�trica.
 * \author Picorelli Marcelo.
 * \bug En este momento no dibuja la escala, ni cambia la misma con lo que si
 * algun parametro excede el tama�o disponible en pantalla, simplemente
 * desaparece. Adem�s ciertas m�tricas no proveen un valor para actual, con lo
 * que el mismo siempre figura como 0.
 **/
void adm_graph_gnome_metrica( TSPant *pTspant, PMETRIC pTMetric )
{
	GnomeCanvasItem *item;
	GnomeCanvasPoints *points;

	static int iXLastPos = 0;  /* Ultima posicion dibujada. */
	static int iYLastPos = 0;

	if( !group )
	{
		create_metric();
	}

	if( pTspant->pTMetric != pTMetric )
		return;

	points = gnome_canvas_points_new( 2 );

	points->coords[0] = iXLastPos++;
	points->coords[1] = 100 - iYLastPos;

	points->coords[2] = iXLastPos;
	points->coords[3] = 100 - pTMetric->cur;

	console_out( "Current = %d", pTMetric->cur );

	if( iXLastPos > 150 )
		iXLastPos = 0;

	item = gnome_canvas_item_new( group, gnome_canvas_line_get_type(),
		"points", points,
		"fill_color", "black",
		NULL );

	gnome_canvas_points_unref( points );

	iYLastPos = pTMetric->cur;
}


static void create_metric( void )
{
	GtkWidget *window;
	GtkWidget *scrolledwindow;


  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize( window, 200, 150 );
  gtk_object_set_data (GTK_OBJECT (window), "Metrica", window);
  gtk_window_set_title (GTK_WINDOW (window), _("window"));

  scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( scrolledwindow ),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );
 // gtk_widget_show (scrolledwindow);
  gtk_container_add (GTK_CONTAINER (window), scrolledwindow);

  gtk_widget_push_visual (gdk_imlib_get_visual ());
  gtk_widget_push_colormap (gdk_imlib_get_colormap ());
  canvas1 = gnome_canvas_new ();
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();
//  gtk_widget_show (canvas1);
  gtk_container_add (GTK_CONTAINER (scrolledwindow), canvas1);
  gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas1), 0, 0, 200, 150);

  group = gnome_canvas_root( GNOME_CANVAS( canvas1 ) );

  gtk_widget_show_all( window );
  //return window;
}
#endif
