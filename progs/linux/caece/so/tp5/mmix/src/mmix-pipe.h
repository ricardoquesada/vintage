/*5:*/
#line 150 "mmix-pipe.w"

#define Extern extern
/*6:*/
#line 163 "mmix-pipe.w"

#ifdef __STDC__
#define ARGS(list) list
#else
#define ARGS(list) ()
#endif

/*:6*//*7:*/
#line 175 "mmix-pipe.w"

#define random my_random
#define fsqrt my_fsqrt
#define div my_div

/*:7*//*8:*/
#line 182 "mmix-pipe.w"

#define issue_bit (1<<0)

#define pipe_bit (1<<1)

#define coroutine_bit (1<<2)

#define schedule_bit (1<<3)

#define uninit_mem_bit (1<<4)

#define interactive_read_bit (1<<5)

#define show_spec_bit (1<<6)

#define show_pred_bit (1<<7)

#define show_wholecache_bit (1<<8)


/*:8*//*52:*/
#line 1109 "mmix-pipe.w"

#define rA 21 
#define rB 0  
#define rC 8  
#define rD 1  
#define rE 2  
#define rF 22 
#define rG 19 
#define rH 3  
#define rI 12 
#define rJ 4  
#define rK 15 
#define rL 20 
#define rM 5  
#define rN 9  
#define rO 10 
#define rP 23 
#define rQ 16 
#define rR 6  
#define rS 11 
#define rT 13 
#define rU 17 
#define rV 18 
#define rW 24 
#define rX 25 
#define rY 26 
#define rZ 27 
#define rBB 7  
#define rTT 14 
#define rWW 28 
#define rXX 29 
#define rYY 30 
#define rZZ 31 

/*:52*//*57:*/
#line 1190 "mmix-pipe.w"

#define POWER_FAILURE (1<<0) 
#define PARITY_ERROR (1<<1) 
#define NONEXISTENT_MEMORY (1<<2) 
#define REBOOT_SIGNAL (1<<4) 
#define INTERVAL_TIMEOUT (1<<7) 

/*:57*//*87:*/
#line 1826 "mmix-pipe.w"

#define ticks g[rC].o 

/*:87*//*129:*/
#line 2405 "mmix-pipe.w"

#define max_stage 99 
#define vanish 98 
#define flush_to_mem 97 
#define flush_to_S 96 
#define fill_from_mem 95 
#define fill_from_S 94 
#define fill_from_virt 93 
#define write_from_wbuf 92 
#define cleanup 91 

/*:129*//*166:*/
#line 3109 "mmix-pipe.w"

#define WRITE_BACK 1 
#define WRITE_ALLOC 2 

/*:166*/
#line 152 "mmix-pipe.w"

/*11:*/
#line 242 "mmix-pipe.w"

typedef enum{false,true,wow}bool;

/*:11*//*17:*/
#line 314 "mmix-pipe.w"

typedef unsigned int tetra;

typedef struct{tetra h,l;}octa;

/*:17*//*23:*/
#line 432 "mmix-pipe.w"

typedef struct coroutine_struct{
char*name;
int stage;
struct coroutine_struct*next;
struct coroutine_struct**lockloc;
struct control_struct*ctl;
}coroutine;

/*:23*//*37:*/
#line 602 "mmix-pipe.w"

typedef coroutine*lockvar;

/*:37*//*40:*/
#line 645 "mmix-pipe.w"

typedef struct{
octa o;
struct specnode_struct*p;
}spec;

typedef struct specnode_struct{
octa o;
bool known;
octa addr;
struct specnode_struct*up,*down;
}specnode;

/*:40*//*44:*/
#line 727 "mmix-pipe.w"

/*47:*/
#line 793 "mmix-pipe.w"

typedef enum{
TRAP,FCMP,FUN,FEQL,FADD,FIX,FSUB,FIXU,
FLOT,FLOTI,FLOTU,FLOTUI,SFLOT,SFLOTI,SFLOTU,SFLOTUI,
FMUL,FCMPE,FUNE,FEQLE,FDIV,FSQRT,FREM,FINT,
MUL,MULI,MULU,MULUI,DIV,DIVI,DIVU,DIVUI,
ADD,ADDI,ADDU,ADDUI,SUB,SUBI,SUBU,SUBUI,
IIADDU,IIADDUI,IVADDU,IVADDUI,VIIIADDU,VIIIADDUI,XVIADDU,XVIADDUI,
CMP,CMPI,CMPU,CMPUI,NEG,NEGI,NEGU,NEGUI,
SL,SLI,SLU,SLUI,SR,SRI,SRU,SRUI,
BN,BNB,BZ,BZB,BP,BPB,BOD,BODB,
BNN,BNNB,BNZ,BNZB,BNP,BNPB,BEV,BEVB,
PBN,PBNB,PBZ,PBZB,PBP,PBPB,PBOD,PBODB,
PBNN,PBNNB,PBNZ,PBNZB,PBNP,PBNPB,PBEV,PBEVB,
CSN,CSNI,CSZ,CSZI,CSP,CSPI,CSOD,CSODI,
CSNN,CSNNI,CSNZ,CSNZI,CSNP,CSNPI,CSEV,CSEVI,
ZSN,ZSNI,ZSZ,ZSZI,ZSP,ZSPI,ZSOD,ZSODI,
ZSNN,ZSNNI,ZSNZ,ZSNZI,ZSNP,ZSNPI,ZSEV,ZSEVI,
LDB,LDBI,LDBU,LDBUI,LDW,LDWI,LDWU,LDWUI,
LDT,LDTI,LDTU,LDTUI,LDO,LDOI,LDOU,LDOUI,
LDSF,LDSFI,LDHT,LDHTI,CSWAP,CSWAPI,LDUNC,LDUNCI,
LDVTS,LDVTSI,PRELD,PRELDI,PREGO,PREGOI,GO,GOI,
STB,STBI,STBU,STBUI,STW,STWI,STWU,STWUI,
STT,STTI,STTU,STTUI,STO,STOI,STOU,STOUI,
STSF,STSFI,STHT,STHTI,STCO,STCOI,STUNC,STUNCI,
SYNCD,SYNCDI,PREST,PRESTI,SYNCID,SYNCIDI,PUSHGO,PUSHGOI,
OR,ORI,ORN,ORNI,NOR,NORI,XOR,XORI,
AND,ANDI,ANDN,ANDNI,NAND,NANDI,NXOR,NXORI,
BDIF,BDIFI,WDIF,WDIFI,TDIF,TDIFI,ODIF,ODIFI,
MUX,MUXI,SADD,SADDI,MOR,MORI,MXOR,MXORI,
SETH,SETMH,SETML,SETL,INCH,INCMH,INCML,INCL,
ORH,ORMH,ORML,ORL,ANDNH,ANDNMH,ANDNML,ANDNL,
JMP,JMPB,PUSHJ,PUSHJB,GETA,GETAB,PUT,PUTI,
POP,RESUME,SAVE,UNSAVE,SYNC,SWYM,GET,TRIP}mmix_opcode;

/*:47*//*49:*/
#line 872 "mmix-pipe.w"

#define max_pipe_op feps
#define max_real_command trip

typedef enum{
mul0,
mul1,
mul2,
mul3,
mul4,
mul5,
mul6,
mul7,
mul8,
div,
sh,
mux,
sadd,
mor,
fadd,
fmul,
fdiv,
fsqrt,
fint,
fix,
flot,
feps,
fcmp,
funeq,
fsub,
frem,
mul,
mulu,
divu,
add,
addu,
sub,
subu,
set,
or,
orn,
nor,
and,
andn,
nand,
xor,
nxor,
shlu,
shru,
shl,
shr,
cmp,
cmpu,
bdif,
wdif,
tdif,
odif,
zset,
cset,
get,
put,
ld,
ldptp,
ldpte,
ldunc,
ldvts,
preld,
prest,
st,
syncd,
syncid,
pst,
stunc,
cswap,
br,
pbr,
pushj,
go,
prego,
pushgo,
pop,
resume,
save,
unsave,
sync,
jmp,
noop,
trap,
trip,
incgamma,
decgamma,
incrl,
sav,
unsav,
resum
}internal_opcode;

/*:49*/
#line 728 "mmix-pipe.w"

typedef struct control_struct{
octa loc;
mmix_opcode op;unsigned char xx,yy,zz;
spec y,z,b,ra;
specnode x,a,go,rl;
coroutine*owner;
internal_opcode i;
int state;
bool usage;
bool need_b;
bool need_ra;
bool ren_x;
bool mem_x;
bool ren_a;
bool set_l;
bool interim;
unsigned int arith_exc;
unsigned int hist;
int denin,denout;
octa cur_O,cur_S;
unsigned int interrupt;
void*ptr_a,*ptr_b,*ptr_c;
}control;

/*:44*//*68:*/
#line 1490 "mmix-pipe.w"

typedef struct{
octa loc;
tetra inst;
unsigned int interrupt;
bool noted;
unsigned int hist;
}fetch;

/*:68*//*76:*/
#line 1626 "mmix-pipe.w"

typedef struct func_struct{
char name[16];
tetra ops[8];
int k;
coroutine*co;
}func;

/*:76*//*164:*/
#line 3076 "mmix-pipe.w"

typedef enum{random,serial,pseudo_lru,lru}replace_policy;

/*:164*//*167:*/
#line 3120 "mmix-pipe.w"

typedef struct{
octa tag;
char*dirty;
octa*data;
int rank;
}cacheblock;

typedef cacheblock*cacheset;

typedef struct{
int a,b,c,g,v;

int aa,bb,cc,gg,vv;

int tagmask;
replace_policy repl,vrepl;
int mode;
int access_time;
int copy_in_time;
int copy_out_time;
cacheset*set;
cacheset victim;
coroutine filler;
control filler_ctl;
coroutine flusher;

control flusher_ctl;
cacheblock inbuf;
cacheblock outbuf;
lockvar lock;
lockvar fill_lock;
int ports;
coroutine*reader;

char*name;
}cache;

/*:167*//*206:*/
#line 3641 "mmix-pipe.w"

typedef struct{
tetra tag;
octa*chunk;
}chunknode;

/*:206*//*246:*/
#line 4378 "mmix-pipe.w"

typedef struct{
octa o;
octa addr;
tetra stamp;
internal_opcode i;
}write_node;

/*:246*//*371:*/
#line 6441 "mmix-pipe.w"

typedef enum{
Halt,Fopen,Fclose,Fread,Fgets,Fgetws,
Fwrite,Fputs,Fputws,Fseek,Ftell}sys_call;

/*:371*/
#line 153 "mmix-pipe.w"

/*4:*/
#line 145 "mmix-pipe.w"

Extern int verbose;

/*:4*//*29:*/
#line 511 "mmix-pipe.w"

Extern int ring_size;
Extern coroutine*ring;
Extern int cur_time;

/*:29*//*59:*/
#line 1361 "mmix-pipe.w"

Extern int fetch_max,dispatch_max,peekahead,commit_max;


/*:59*//*60:*/
#line 1387 "mmix-pipe.w"

Extern control*reorder_bot,*reorder_top;

Extern control*hot,*cool;
Extern control*old_hot;
Extern int deissues;

/*:60*//*66:*/
#line 1446 "mmix-pipe.w"

Extern int*dispatch_stat;

Extern bool security_disabled;

/*:66*//*69:*/
#line 1509 "mmix-pipe.w"

Extern fetch*fetch_bot,*fetch_top;

Extern fetch*head,*tail;

/*:69*//*77:*/
#line 1634 "mmix-pipe.w"

Extern func*funit;
Extern int funit_count;

/*:77*//*86:*/
#line 1818 "mmix-pipe.w"

Extern specnode g[256];
Extern specnode*l;
Extern int lring_size;

Extern int max_rename_regs,max_mem_slots;
Extern int rename_regs,mem_slots;

/*:86*//*98:*/
#line 1944 "mmix-pipe.w"

Extern octa cool_O,cool_S;

/*:98*//*115:*/
#line 2168 "mmix-pipe.w"

Extern specnode mem;

/*:115*//*136:*/
#line 2534 "mmix-pipe.w"

#define pipe_limit 90
Extern unsigned char pipe_seq[max_pipe_op+1][pipe_limit+1];

/*:136*//*150:*/
#line 2817 "mmix-pipe.w"

Extern int bp_a,bp_b,bp_c,bp_n;
Extern char*bp_table;

/*:150*//*168:*/
#line 3158 "mmix-pipe.w"

Extern cache*Icache,*Dcache,*Scache,*ITcache,*DTcache;

/*:168*//*207:*/
#line 3653 "mmix-pipe.w"

Extern int mem_chunks;
Extern int mem_chunks_max;
Extern int hash_prime;
Extern chunknode*mem_hash;

/*:207*//*211:*/
#line 3703 "mmix-pipe.w"

Extern int last_h;

/*:211*//*214:*/
#line 3746 "mmix-pipe.w"

Extern int mem_addr_time;
Extern int bus_words;
Extern int mem_read_time;
Extern int mem_write_time;
Extern lockvar mem_lock;

/*:214*//*242:*/
#line 4312 "mmix-pipe.w"

Extern bool no_hardware_PT;

/*:242*//*247:*/
#line 4393 "mmix-pipe.w"

Extern write_node*wbuf_bot,*wbuf_top;

Extern write_node*write_head,*write_tail;

Extern lockvar wbuf_lock;
Extern int holding_time;
Extern lockvar speed_lock;

/*:247*//*284:*/
#line 5092 "mmix-pipe.w"

Extern spec inst_ptr;
Extern octa*fetched;

/*:284*//*349:*/
#line 6102 "mmix-pipe.w"

Extern int frem_max;
Extern int denin_penalty,denout_penalty;

/*:349*/
#line 154 "mmix-pipe.w"

/*9:*/
#line 208 "mmix-pipe.w"

Extern void MMIX_init ARGS((void));
Extern void MMIX_run ARGS((int cycs,octa breakpoint));

/*:9*//*38:*/
#line 605 "mmix-pipe.w"

Extern void print_locks ARGS((void));

/*:38*//*161:*/
#line 2968 "mmix-pipe.w"

Extern void print_stats ARGS((void));

/*:161*//*175:*/
#line 3214 "mmix-pipe.w"

Extern void print_cache ARGS((cache*,bool));

/*:175*//*178:*/
#line 3258 "mmix-pipe.w"

Extern void clean_block ARGS((cache*,cacheblock*));

/*:178*//*180:*/
#line 3275 "mmix-pipe.w"

Extern void zap_cache ARGS((cache*));

/*:180*//*209:*/
#line 3678 "mmix-pipe.w"

Extern octa mem_read ARGS((octa addr));

/*:209*//*212:*/
#line 3706 "mmix-pipe.w"

Extern void mem_write ARGS((octa addr,octa val));

/*:212*//*252:*/
#line 4439 "mmix-pipe.w"

Extern void print_pipe ARGS((void));

/*:252*/
#line 155 "mmix-pipe.w"


/*:5*/
