/*1:*/
#line 19 "mmix-mem.w"

#include <stdio.h> 
#include "mmix-pipe.h" 
extern octa read_hex();
static char buf[20];

/*:1*//*2:*/
#line 29 "mmix-mem.w"

octa spec_read ARGS((octa));
octa spec_read(addr)
octa addr;
{
octa val;
if(verbose&interactive_read_bit){
printf("** Read from loc %08x%08x: ",addr.h,addr.l);
fgets(buf,20,stdin);
val= read_hex(buf);
}else val.l= val.h= 0;
if(verbose&show_spec_bit)
printf("   (spec_read %08x%08x from %08x%08x at time %d)\n",
val.h,val.l,addr.h,addr.l,ticks.l);
return val;
}

/*:2*//*3:*/
#line 49 "mmix-mem.w"

void spec_write ARGS((octa,octa));
void spec_write(addr,val)
octa addr,val;
{
if(verbose&show_spec_bit)
printf("   (spec_write %08x%08x to %08x%08x at time %d)\n",
val.h,val.l,addr.h,addr.l,ticks.l);
}

/*:3*/
