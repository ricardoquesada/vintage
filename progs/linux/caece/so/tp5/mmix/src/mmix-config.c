#define errprint0(f) fprintf(stderr,f) 
#define errprint1(f,a) fprintf(stderr,f,a) 
#define errprint2(f,a,b) fprintf(stderr,f,a,b) 
#define errprint3(f,a,b,c) fprintf(stderr,f,a,b,c) 
#define panic(x) {x;errprint0("!\n") ;exit(-1) ;} \

#define BUF_SIZE 100 \

#define sign_bit 0x80000000 \

/*38:*/
#line 1013 "mmix-config.w"

#include <stdio.h>  
#include <stdlib.h>  
#include <ctype.h>  
#include <string.h>  
#include <limits.h>  
#include "mmix-pipe.h"
/*12:*/
#line 408 "mmix-config.w"

typedef struct{
char name[20];
int*v;
int defval;
int minval,maxval;
bool power_of_two;
}pv_spec;

/*:12*//*13:*/
#line 419 "mmix-config.w"

typedef enum{assoc,blksz,setsz,gran,vctsz,
wrb,wra,acctm,citm,cotm,prts}c_param;

typedef struct{
char name[20];
c_param v;
int defval;
int minval,maxval;
bool power_of_two;
}cpv_spec;

/*:13*//*14:*/
#line 433 "mmix-config.w"

typedef struct{
char name[8];
internal_opcode v;
int defval;
}op_spec;

/*:14*/
#line 1020 "mmix-config.w"

/*9:*/
#line 358 "mmix-config.w"

FILE*config_file;
char buffer[BUF_SIZE];
char token[BUF_SIZE];
char*buf_pointer= buffer;
bool token_prescanned;

/*:9*//*15:*/
#line 444 "mmix-config.w"

int fetch_buf_size,write_buf_size,reorder_buf_size,mem_bus_bytes,hardware_PT;
int max_cycs= 60;
pv_spec PV[]= {
{"fetchbuffer",&fetch_buf_size,4,1,INT_MAX,false},
{"writebuffer",&write_buf_size,2,1,INT_MAX,false},
{"reorderbuffer",&reorder_buf_size,5,1,INT_MAX,false},
{"renameregs",&max_rename_regs,5,1,INT_MAX,false},
{"memslots",&max_mem_slots,2,1,INT_MAX,false},
{"localregs",&lring_size,256,256,1024,true},
{"fetchmax",&fetch_max,2,1,INT_MAX,false},
{"dispatchmax",&dispatch_max,1,1,INT_MAX,false},
{"peekahead",&peekahead,1,0,INT_MAX,false},
{"commitmax",&commit_max,1,1,INT_MAX,false},
{"fremmax",&frem_max,1,1,INT_MAX,false},
{"denin",&denin_penalty,1,0,INT_MAX,false},
{"denout",&denout_penalty,1,0,INT_MAX,false},
{"writeholdingtime",&holding_time,0,0,INT_MAX,false},
{"memaddresstime",&mem_addr_time,20,1,INT_MAX,false},
{"memreadtime",&mem_read_time,20,1,INT_MAX,false},
{"memwritetime",&mem_write_time,20,1,INT_MAX,false},
{"membusbytes",&mem_bus_bytes,8,8,INT_MAX,true},
{"branchpredictbits",&bp_n,0,0,8,false},
{"branchaddressbits",&bp_a,0,0,32,false},
{"branchhistorybits",&bp_b,0,0,32,false},
{"branchdualbits",&bp_c,0,0,32,false},
{"hardwarepagetable",&hardware_PT,1,0,1,false},
{"disablesecurity",(int*)&security_disabled,0,0,1,false},
{"memchunksmax",&mem_chunks_max,1000,1,INT_MAX,false},
{"hashprime",&hash_prime,2009,2,INT_MAX,false}};

cpv_spec CPV[]= {
{"associativity",assoc,1,1,INT_MAX,true},
{"blocksize",blksz,8,8,8192,true},
{"setsize",setsz,1,1,INT_MAX,true},
{"granularity",gran,8,8,8192,true},
{"victimsize",vctsz,0,0,INT_MAX,true},
{"writeback",wrb,0,0,1,false},
{"writeallocate",wra,0,0,1,false},
{"accesstime",acctm,1,1,INT_MAX,false},
{"copyintime",citm,1,1,INT_MAX,false},
{"copyouttime",cotm,1,1,INT_MAX,false},
{"ports",prts,1,1,INT_MAX,false}};

op_spec OP[]= {
{"mul0",mul0,10},
{"mul1",mul1,10},
{"mul2",mul2,10},
{"mul3",mul3,10},
{"mul4",mul4,10},
{"mul5",mul5,10},
{"mul6",mul6,10},
{"mul7",mul7,10},
{"mul8",mul8,10},
{"div",div,60},
{"sh",sh,1},
{"mux",mux,1},
{"sadd",sadd,1},
{"mor",mor,1},
{"fadd",fadd,4},
{"fmul",fmul,4},
{"fdiv",fdiv,40},
{"fsqrt",fsqrt,40},
{"fint",fint,4},
{"fix",fix,2},
{"flot",flot,2},
{"feps",feps,4}};
int PV_size,CPV_size,OP_size;

/*:15*//*28:*/
#line 762 "mmix-config.w"

internal_opcode int_op[256]= {
trap,fcmp,funeq,funeq,fadd,fix,fadd,fix,
flot,flot,flot,flot,flot,flot,flot,flot,
fmul,feps,feps,feps,fdiv,fsqrt,frem,fint,
mul,mul,mul,mul,div,div,div,div,
add,add,addu,addu,sub,sub,subu,subu,
addu,addu,addu,addu,addu,addu,addu,addu,
cmp,cmp,cmpu,cmpu,sub,sub,subu,subu,
sh,sh,sh,sh,sh,sh,sh,sh,
br,br,br,br,br,br,br,br,
br,br,br,br,br,br,br,br,
pbr,pbr,pbr,pbr,pbr,pbr,pbr,pbr,
pbr,pbr,pbr,pbr,pbr,pbr,pbr,pbr,
cset,cset,cset,cset,cset,cset,cset,cset,
cset,cset,cset,cset,cset,cset,cset,cset,
zset,zset,zset,zset,zset,zset,zset,zset,
zset,zset,zset,zset,zset,zset,zset,zset,
ld,ld,ld,ld,ld,ld,ld,ld,
ld,ld,ld,ld,ld,ld,ld,ld,
ld,ld,ld,ld,ld,ld,ld,ld,
ld,ld,ld,ld,prego,prego,go,go,
st,st,st,st,st,st,st,st,
st,st,st,st,st,st,st,st,
st,st,st,st,st,st,st,st,
st,st,st,st,st,st,pushgo,pushgo,
or,or,orn,orn,nor,nor,xor,xor,
and,and,andn,andn,nand,nand,nxor,nxor,
bdif,bdif,wdif,wdif,tdif,tdif,odif,odif,
mux,mux,sadd,sadd,mor,mor,mor,mor,
set,set,set,set,addu,addu,addu,addu,
or,or,or,or,andn,andn,andn,andn,
noop,noop,pushj,pushj,set,set,put,put,
pop,resume,save,unsave,sync,noop,get,trip};
int int_stages[max_real_command+1];

int stages[256];

/*:28*/
#line 1021 "mmix-config.w"

/*10:*/
#line 368 "mmix-config.w"

static void get_token ARGS((void));
static void get_token()
{
register char*p,*q;
if(token_prescanned){
token_prescanned= false;return;
}
while(1){
if(*buf_pointer=='\0'||*buf_pointer=='\n'||*buf_pointer=='%'){
if(!fgets(buffer,BUF_SIZE,config_file)){
strcpy(token,"end");return;
}
if(strlen(buffer)==BUF_SIZE-1&&buffer[BUF_SIZE-2]!='\n')
panic(errprint1("config file line too long: `%s...'",buffer));

buf_pointer= buffer;
}else if(!isspace(*buf_pointer))break;
else buf_pointer++;
}
for(p= buf_pointer,q= token;!isspace(*p)&&*p!='%';p++,q++)*q= *p;
buf_pointer= p;*q= '\0';
return;
}

/*:10*//*11:*/
#line 396 "mmix-config.w"

static int get_int ARGS((void));
static int get_int()
{int v;
get_token();
if(sscanf(token,"%d",&v)!=1)return-1;
return v;
}

/*:11*//*16:*/
#line 517 "mmix-config.w"

static cache*new_cache ARGS((char*));
static cache*new_cache(name)
char*name;
{register cache*c= (cache*)calloc(1,sizeof(cache));
if(!c)panic(errprint1("Can't allocate %s",name));

c->aa= 1;
c->bb= 8;
c->cc= 1;
c->gg= 8;
c->vv= 0;
c->repl= random;
c->vrepl= random;
c->mode= 0;
c->access_time= c->copy_in_time= c->copy_out_time= 1;
c->filler.ctl= &(c->filler_ctl);
c->filler_ctl.ptr_a= (void*)c;
c->filler_ctl.go.o.l= 4;
c->flusher.ctl= &(c->flusher_ctl);
c->flusher_ctl.ptr_a= (void*)c;
c->flusher_ctl.go.o.l= 4;
c->ports= 1;
c->name= name;
return c;
}

/*:16*//*22:*/
#line 636 "mmix-config.w"

static void ppol ARGS((replace_policy*));
static void ppol(rr)
replace_policy*rr;
{
get_token();
if(strcmp(token,"random")==0)*rr= random;
else if(strcmp(token,"serial")==0)*rr= serial;
else if(strcmp(token,"pseudolru")==0)*rr= pseudo_lru;
else if(strcmp(token,"lru")==0)*rr= lru;
else token_prescanned= true;
}

/*:22*//*23:*/
#line 649 "mmix-config.w"

static void pcs ARGS((cache*));
static void pcs(c)
cache*c;
{
register int j,n;
get_token();
for(j= 0;j<CPV_size;j++)if(strcmp(token,CPV[j].name)==0)break;
if(j==CPV_size)panic(errprint1(
"Configuration syntax error: `%s' isn't a cache parameter name",token));

n= get_int();
if(n<CPV[j].minval)panic(errprint2(
"Configuration error: %s must be >= %d",CPV[j].name,CPV[j].minval));

if(n> CPV[j].maxval)panic(errprint2(
"Configuration error: %s must be <= %d",CPV[j].name,CPV[j].maxval));
if(CPV[j].power_of_two&&(n&(n-1)))panic(errprint1(
"Configuration error: %s must be power of 2",CPV[j].name));
switch(CPV[j].v){
case assoc:c->aa= n;ppol(&(c->repl));break;
case blksz:c->bb= n;break;
case setsz:c->cc= n;break;
case gran:c->gg= n;break;
case vctsz:c->vv= n;ppol(&(c->vrepl));break;
case wrb:c->mode= (c->mode&~WRITE_BACK)+n*WRITE_BACK;break;
case wra:c->mode= (c->mode&~WRITE_ALLOC)+n*WRITE_ALLOC;break;
case acctm:if(n> max_cycs)max_cycs= n;
c->access_time= n;break;
case citm:if(n> max_cycs)max_cycs= n;
c->copy_in_time= n;break;
case cotm:if(n> max_cycs)max_cycs= n;
c->copy_out_time= n;break;
case prts:c->ports= n;break;
}
}

/*:23*//*30:*/
#line 814 "mmix-config.w"

static int lg ARGS((int));
static int lg(n)
int n;
{register int j,l;
for(j= n,l= 0;j;j>>= 1)l++;
return l-1;
}

/*:30*//*31:*/
#line 823 "mmix-config.w"

static void alloc_cache ARGS((cache*,char*));
static void alloc_cache(c,name)
cache*c;
char*name;
{register int j,k;
if(c->bb<c->gg)panic(errprint1(
"Configuration error: blocksize of %s is less than granularity",name));

if(name[1]=='T'&&c->bb!=8)panic(errprint1(
"Configuration error: blocksize of %s must be 8",name));
c->a= lg(c->aa);
c->b= lg(c->bb);
c->c= lg(c->cc);
c->g= lg(c->gg);
c->v= lg(c->vv);
c->tagmask= -(1<<(c->b+c->c));
if(c->a+c->b+c->c>=32)panic(errprint1(
"Configuration error: %s has >= 4 gigabytes of data",name));
if(c->gg!=8&&!(c->mode&WRITE_ALLOC))panic(errprint2(
"Configuration error: %s does write-around with granularity %d",
name,c->gg));
/*32:*/
#line 865 "mmix-config.w"

c->set= (cacheset*)calloc(c->cc,sizeof(cacheset));
if(!c->set)panic(errprint1(
"Can't allocate cache sets for %s",name));

for(j= 0;j<c->cc;j++){
c->set[j]= (cacheblock*)calloc(c->aa,sizeof(cacheblock));
if(!c->set[j])panic(errprint2(
"Can't allocate cache blocks for set %d of %s",j,name));
for(k= 0;k<c->aa;k++){
c->set[j][k].tag.h= sign_bit;
c->set[j][k].dirty= (char*)calloc(c->bb>>c->g,sizeof(char));
if(!c->set[j][k].dirty)panic(errprint3(
"Can't allocate dirty bits for block %d of set %d of %s",k,j,name));
c->set[j][k].data= (octa*)calloc(c->bb>>3,sizeof(octa));
if(!c->set[j][k].data)panic(errprint3(
"Can't allocate data for block %d of set %d of %s",k,j,name));
}
}

/*:32*/
#line 845 "mmix-config.w"
;
if(c->vv)/*33:*/
#line 885 "mmix-config.w"

{
c->victim= (cacheblock*)calloc(c->vv,sizeof(cacheblock));
if(!c->victim)panic(errprint1(
"Can't allocate blocks for victim cache of %s",name));
for(k= 0;k<c->vv;k++){
c->victim[k].tag.h= sign_bit;
c->victim[k].dirty= (char*)calloc(c->bb>>c->g,sizeof(char));
if(!c->victim[k].dirty)panic(errprint2(
"Can't allocate dirty bits for block %d of victim cache of %s",
k,name));

c->victim[k].data= (octa*)calloc(c->bb>>3,sizeof(octa));
if(!c->victim[k].data)panic(errprint2(
"Can't allocate data for block %d of victim cache of %s",k,name));
}
}

/*:33*/
#line 846 "mmix-config.w"
;
c->inbuf.dirty= (char*)calloc(c->bb>>c->g,sizeof(char));
if(!c->inbuf.dirty)panic(errprint1(
"Can't allocate dirty bits for inbuffer of %s",name));

c->inbuf.data= (octa*)calloc(c->bb>>3,sizeof(octa));
if(!c->inbuf.data)panic(errprint1(
"Can't allocate data for inbuffer of %s",name));
c->outbuf.dirty= (char*)calloc(c->bb>>c->g,sizeof(char));
if(!c->outbuf.dirty)panic(errprint1(
"Can't allocate dirty bits for outbuffer of %s",name));
c->outbuf.data= (octa*)calloc(c->bb>>3,sizeof(octa));
if(!c->outbuf.data)panic(errprint1(
"Can't allocate data for outbuffer of %s",name));
if(name[0]!='S')/*34:*/
#line 903 "mmix-config.w"

{
c->reader= (coroutine*)calloc(c->ports,sizeof(coroutine));
if(!c->reader)panic(errprint1(

"Can't allocate readers for %s",name));
for(j= 0;j<c->ports;j++){
c->reader[j].stage= vanish;
c->reader[j].name= (name[0]=='D'?(name[1]=='T'?"DTreader":"Dreader"):
(name[1]=='T'?"ITreader":"Ireader"));
}
}

/*:34*/
#line 860 "mmix-config.w"
;
}

/*:31*/
#line 1022 "mmix-config.w"

void MMIX_config(filename)
char*filename;
{register int i,j,n;
config_file= fopen(filename,"r");
if(!config_file)
panic(errprint1("Can't open configuration file %s",filename));

/*17:*/
#line 544 "mmix-config.w"

PV_size= (sizeof PV)/sizeof(pv_spec);
CPV_size= (sizeof CPV)/sizeof(cpv_spec);
OP_size= (sizeof OP)/sizeof(op_spec);
ITcache= new_cache("ITcache");
DTcache= new_cache("DTcache");
Icache= Dcache= Scache= NULL;
for(j= 0;j<PV_size;j++)*(PV[j].v)= PV[j].defval;
for(j= 0;j<OP_size;j++){
pipe_seq[OP[j].v][0]= OP[j].defval;
pipe_seq[OP[j].v][1]= 0;
}

/*:17*/
#line 1030 "mmix-config.w"
;
/*18:*/
#line 564 "mmix-config.w"

funit_count= 0;
while(strcmp(token,"end")!=0){
get_token();
if(strcmp(token,"unit")==0){
funit_count++;
get_token();get_token();
}
}
funit= (func*)calloc(funit_count+1,sizeof(func));
if(!funit)panic(errprint0("Can't allocate the functional units"));

strcpy(funit[funit_count].name,"%%");

funit[funit_count].ops[0]= 0x80000000;
funit[funit_count].ops[7]= 0x1;

/*:18*/
#line 1031 "mmix-config.w"
;
/*19:*/
#line 587 "mmix-config.w"

rewind(config_file);
funit_count= 0;
token[0]= '\0';
while(strcmp(token,"end")!=0){
get_token();
if(strcmp(token,"end")==0)break;
/*20:*/
#line 603 "mmix-config.w"

for(j= 0;j<PV_size;j++)if(strcmp(token,PV[j].name)==0){
n= get_int();
if(n<PV[j].minval)panic(errprint2(

"Configuration error: %s must be >= %d",PV[j].name,PV[j].minval));
if(n> PV[j].maxval)panic(errprint2(
"Configuration error: %s must be <= %d",PV[j].name,PV[j].maxval));
if(PV[j].power_of_two&&(n&(n-1)))panic(errprint1(
"Configuration error: %s must be a power of 2",PV[j].name));
*(PV[j].v)= n;
break;
}
if(j<PV_size)continue;

/*:20*/
#line 594 "mmix-config.w"
;
/*21:*/
#line 618 "mmix-config.w"

if(strcmp(token,"ITcache")==0){
pcs(ITcache);continue;
}else if(strcmp(token,"DTcache")==0){
pcs(DTcache);continue;
}else if(strcmp(token,"Icache")==0){
if(!Icache)Icache= new_cache("Icache");
pcs(Icache);continue;
}else if(strcmp(token,"Dcache")==0){
if(!Dcache)Dcache= new_cache("Dcache");
pcs(Dcache);continue;
}else if(strcmp(token,"Scache")==0){
if(!Icache)Icache= new_cache("Icache");
if(!Dcache)Dcache= new_cache("Dcache");
if(!Scache)Scache= new_cache("Scache");
pcs(Scache);continue;
}

/*:21*/
#line 595 "mmix-config.w"
;
/*24:*/
#line 686 "mmix-config.w"

for(j= 0;j<OP_size;j++)if(strcmp(token,OP[j].name)==0){
for(i= 0;;i++){
n= get_int();
if(n<0)break;
if(n==0)panic(errprint0(
"Configuration error: Pipeline cycles must be positive"));

if(n> 255)panic(errprint0(
"Configuration error: Pipeline cycles must be <= 255"));
if(n> max_cycs)max_cycs= n;
if(i>=pipe_limit)panic(errprint1(
"Configuration error: More than %d pipeline stages",pipe_limit));
pipe_seq[OP[j].v][i]= n;
}
token_prescanned= true;
break;
}
if(j<OP_size)continue;

/*:24*/
#line 596 "mmix-config.w"
;
if(strcmp(token,"unit")==0)/*25:*/
#line 706 "mmix-config.w"

{
get_token();
if(strlen(token)> 15)panic(errprint1(
"Configuration error: `%s' is more than 15 characters long",token));

strcpy(funit[funit_count].name,token);
get_token();
if(strlen(token)!=64)panic(errprint1(
"Configuration error: unit %s doesn't have 64 hex digit specs",
funit[funit_count].name));
for(i= j= n= 0;j<64;j++){
if(token[j]>='0'&&token[j]<='9')n= (n<<4)+(token[j]-'0');
else if(token[j]>='a'&&token[j]<='f')n= (n<<4)+(token[j]-'a'+10);
else if(token[j]>='A'&&token[j]<='F')n= (n<<4)+(token[j]-'A'+10);
else panic(errprint1(
"Configuration error: `%c' is not a hex digit",token[j]));
if((j&0x7)==0x7)funit[funit_count].ops[i++]= n,n= 0;
}
funit_count++;
continue;
}

/*:25*/
#line 597 "mmix-config.w"
;
panic(errprint1(
"Configuration syntax error: Specification can't start with `%s'",token));

}

/*:19*/
#line 1032 "mmix-config.w"
;
/*26:*/
#line 737 "mmix-config.w"

/*27:*/
#line 749 "mmix-config.w"

for(j= div;j<=max_pipe_op;j++)int_stages[j]= strlen(pipe_seq[j]);
for(;j<=max_real_command;j++)int_stages[j]= 1;
for(j= mul0,n= 0;j<=mul8;j++)
if(strlen(pipe_seq[j])> n)n= strlen(pipe_seq[j]);
int_stages[mul]= n;
int_stages[ld]= int_stages[st]= int_stages[frem]= 2;
for(j= 0;j<256;j++)stages[j]= int_stages[int_op[j]];

/*:27*/
#line 738 "mmix-config.w"
;
for(j= 0;j<=funit_count;j++){
/*29:*/
#line 800 "mmix-config.w"

for(i= n= 0;i<256;i++)
if(((funit[j].ops[i>>5]<<(i&0x1f))&0x80000000)&&stages[i]> n)
n= stages[i];
if(n==0)panic(errprint1(
"Configuration error: unit %s doesn't do anything",funit[j].name));


/*:29*/
#line 740 "mmix-config.w"
;
funit[j].k= n;
funit[j].co= (coroutine*)calloc(n,sizeof(coroutine));
for(i= 0;i<n;i++){
funit[j].co[i].name= funit[j].name;
funit[j].co[i].stage= i+1;
}
}

/*:26*/
#line 1033 "mmix-config.w"
;
/*35:*/
#line 916 "mmix-config.w"

alloc_cache(ITcache,"ITcache");
ITcache->filler.name= "ITfiller";ITcache->filler.stage= fill_from_virt;
alloc_cache(DTcache,"DTcache");
DTcache->filler.name= "DTfiller";DTcache->filler.stage= fill_from_virt;
if(Icache){
alloc_cache(Icache,"Icache");
Icache->filler.name= "Ifiller";Icache->filler.stage= fill_from_mem;
}
if(Dcache){
alloc_cache(Dcache,"Dcache");
Dcache->filler.name= "Dfiller";Dcache->filler.stage= fill_from_mem;
Dcache->flusher.name= "Dflusher";Dcache->flusher.stage= flush_to_mem;
}
if(Scache){
alloc_cache(Scache,"Scache");
if(Scache->bb<Icache->bb)panic(errprint0(
"Configuration error: Scache blocks smaller than Icache blocks"));

if(Scache->bb<Dcache->bb)panic(errprint0(
"Configuration error: Scache blocks smaller than Dcache blocks"));
if(Scache->gg!=Dcache->gg)panic(errprint0(
"Configuration error: Scache granularity differs from the Dcache"));
Icache->filler.stage= fill_from_S;
Dcache->filler.stage= fill_from_S;Dcache->flusher.stage= flush_to_S;
Scache->filler.name= "Sfiller";Scache->filler.stage= fill_from_mem;
Scache->flusher.name= "Sflusher";Scache->flusher.stage= flush_to_mem;
}

/*:35*/
#line 1034 "mmix-config.w"
;
/*36:*/
#line 950 "mmix-config.w"

bus_words= mem_bus_bytes>>3;
j= (mem_read_time<mem_write_time?mem_write_time:mem_read_time);
n= 1;
if(Scache&&Scache->bb> n)n= Scache->bb;
if(Icache&&Icache->bb> n)n= Icache->bb;
if(Dcache&&Dcache->bb> n)n= Dcache->bb;
n= mem_addr_time+((int)(n+bus_words-1)/bus_words)*j;
if(n> max_cycs)max_cycs= n;
ring_size= max_cycs+1;
ring= (coroutine*)calloc(ring_size,sizeof(coroutine));
if(!ring)panic(errprint0("Can't allocate the scheduling ring"));

{register coroutine*p;
for(p= ring;p<ring+ring_size;p++){
p->name= "";
p->stage= max_stage;
}
}

/*:36*/
#line 1035 "mmix-config.w"
;
/*37:*/
#line 972 "mmix-config.w"

if(hash_prime<=mem_chunks_max)panic(errprint0(
"Configuration error: hashprime must exceed memchunksmax"));

mem_hash= (chunknode*)calloc(hash_prime+1,sizeof(chunknode));
if(!mem_hash)panic(errprint0("Can't allocate the hash table"));

mem_hash[0].chunk= (octa*)calloc(1<<13,sizeof(octa));
if(!mem_hash[0].chunk)panic(errprint0("Can't allocate chunk 0"));
mem_hash[hash_prime].chunk= (octa*)calloc(1<<13,sizeof(octa));
if(!mem_hash[hash_prime].chunk)panic(errprint0("Can't allocate 0 chunk"));
mem_chunks= 1;
fetch_bot= (fetch*)calloc(fetch_buf_size+1,sizeof(fetch));
if(!fetch_bot)panic(errprint0("Can't allocate the fetch buffer"));
fetch_top= fetch_bot+fetch_buf_size;
reorder_bot= (control*)calloc(reorder_buf_size+1,sizeof(control));
if(!reorder_bot)panic(errprint0("Can't allocate the reorder buffer"));
reorder_top= reorder_bot+reorder_buf_size;
wbuf_bot= (write_node*)calloc(write_buf_size+1,sizeof(write_node));
if(!wbuf_bot)panic(errprint0("Can't allocate the write buffer"));
wbuf_top= wbuf_bot+write_buf_size;
if(bp_n==0)bp_table= NULL;
else{
if(bp_a+bp_b+bp_c>=32)panic(errprint0(
"Configuration error: Branch table has >= 4 gigabytes of data"));
bp_table= (char*)calloc(1<<(bp_a+bp_b+bp_c),sizeof(char));
if(!bp_table)panic(errprint0("Can't allocate the branch table"));
}
l= (specnode*)calloc(lring_size,sizeof(specnode));
if(!l)panic(errprint0("Can't allocate local registers"));
j= bus_words;
if(Icache&&Icache->bb> j)j= Icache->bb;
fetched= (octa*)calloc(j,sizeof(octa));
if(!fetched)panic(errprint0("Can't allocate prefetch buffer"));
dispatch_stat= (int*)calloc(dispatch_max+1,sizeof(int));
if(!dispatch_stat)panic(errprint0("Can't allocate dispatch counts"));
no_hardware_PT= 1-hardware_PT;

/*:37*/
#line 1036 "mmix-config.w"
;
}

/*:38*/
