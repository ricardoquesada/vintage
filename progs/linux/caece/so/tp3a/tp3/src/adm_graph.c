/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <assert.h>
#include <curses.h>

#include "tipos.h"
#include "sched.h"
#include "adm_graph.h"
#include "adm_common.h"
#include "mem.h"

/**
 * \file adm_graph.c
 * Graficador de funciones
 */

static void int_scroll( WINDOW *win );

/**
 * Muestra el estado de los procesos.
 * @param pTspant Las variables que conforman al programa, no puede ser \c
 * NULL.
 * \author Picorelli Marcelo.
 */


int adm_graph_mem( TSPant *pTspant )
{
	PLIST_ENTRY  pMemCode = NULL;
	PLIST_ENTRY  pList = NULL;
	PMEM_NODO	pNodo = NULL;

	int iXMaxCoord;
	int iYMaxCoord;
	int iFila = 0;
	int iColumna = 0;

	char *cpBeginMem = NULL;

	chtype	cChar;

	assert( pTspant != NULL );


	if( !( pMemCode = ptr_mem() )	)
		return -1;

	/* Tamanio de pantalla y otras yerbas...*/
	getmaxyx( pTspant->pGPad, iYMaxCoord, iXMaxCoord );

	pList = pMemCode->next;

	pNodo = (PMEM_NODO)pList;
	cpBeginMem = pNodo->ptr;


	while( !IsListEmpty( pMemCode ) && (pList != pMemCode) )
	{
		pNodo = (PMEM_NODO) pList;

		cChar = pNodo->status == MEM_BUSY ? ACS_BLOCK | COLOR_PAIR( PANT_CYAN) : ACS_BOARD;

	/* Direccion de comienzo */
		iFila = (int)( pNodo->ptr - cpBeginMem ) / ( iXMaxCoord - 2 );
		iColumna = (int)( pNodo->ptr - cpBeginMem ) % ( iXMaxCoord - 2 ) ;

		{
			int k;

			for( k = 0; k < pNodo->tot; k++ ) 
			{
				mvwaddch( pTspant->pGPad, iFila + 1, iColumna + 1, cChar );
				iColumna++;

				if( iColumna == iXMaxCoord - 2)
				{
					iColumna = 0;
					iFila++;
				}
			}
		}

		pList = LIST_NEXT( pList );
	}
	wnoutrefresh( pTspant->pGPad );

	return 1;
}

int adm_graph_ps( TSPant *pTspant )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;

	int i = 0;
	int iYMaxCoord;
	int iXMaxCoord;


	char *cpEstado[] = { "CREADO", "CORRIENDO", "DORMIDO  ", "PARADO  ", "ZOMBIE", "TERMINADO" };

	assert( pTspant != NULL );

	getmaxyx( pTspant->pGPad, iYMaxCoord, iXMaxCoord );

	//wclear( pTspant->pGPad );
	mvwaddstr( pTspant->pGPad, 0,0, "Nombre\tEstado\t\tStart\tEnd\tWasted\n" );

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) 
			&& i < iYMaxCoord ) {

		pPcb = proclist2pcb( pList );

		wprintw( pTspant->pGPad, "%s\t%s\t%d\t%d\t%d", pPcb->p_nombre,
			cpEstado[pPcb->p_estado], pPcb->p_t_run_start, pPcb->p_t_run_end,pPcb->p_cpu_usada );
		if( pPcb->p_estado == PROC_TERMINADO ) {
			int tardo = pPcb->p_t_run_end - pPcb->p_t_run_start;
			wprintw( pTspant->pGPad," (%d%%)", (tardo * 100 )/ pPcb->p_cpu_usada );
		}
		wprintw( pTspant->pGPad,"\n");

		pList = LIST_NEXT( pList );
		i++;
	}
	wnoutrefresh( pTspant->pGPad );

	return 1;
}

/**
 * Grafica las metricas.
 * Se encarga de realizar los graficos de las metricas, mostrando m�ximo,
 * m�nimo, promedio y actual de la m�trica que se le pasa.
 * @param pTspant Las variables que conforman al programa, no puede ser \c
 * NULL.
 * @param pTMetric Las variables que hacen a la m�trica.
 * \author Picorelli Marcelo.
 * \bug En este momento no dibuja la escala, ni cambia la misma con lo que si
 * algun parametro excede el tama�o disponible en pantalla, simplemente
 * desaparece. Adem�s ciertas m�tricas no proveen un valor para actual, con lo
 * que el mismo siempre figura como 0.
 */
int adm_graph_metric( TSPant *pTspant, PMETRIC pTMetric  )
{
	static int iXLastPos = 0;	/* Ultima posicion dibujada. */
	static int iXMaxCoord = 0;	/* Ancho de la ventana. */
	static int iYMaxCoord = 0;	/* Altura de la ventana. */
	static int iLastMax = 0;	/* Ultimo valor m�ximo. */

	float scale = pTMetric->scale_y;
	int i;

	assert( pTspant != NULL );
	assert( pTMetric != NULL );

	if( pTspant->pTMetric != pTMetric )
		return 0;

	if( !iXMaxCoord )
		getmaxyx( pTspant->pGPad, iYMaxCoord, iXMaxCoord );

	scrollok( pTspant->pGPad, FALSE );

	if( iLastMax < pTMetric->max )
	{
		mvwaddch( pTspant->pGPad, iYMaxCoord - iLastMax - 1, 0, ' ' );
		mvwaddch( pTspant->pGPad, iYMaxCoord - iLastMax - 1,iXMaxCoord - 1, ' ' );
		iLastMax = pTMetric->max;
	}

	/* Alcanzamos el final de la pantalla */
	if( iXLastPos == iXMaxCoord - 1 )
	{
		/* Scroll */
		int_scroll( pTspant->pGPad );
		iXLastPos--;
	}

	/* Dibujamos la escala */
	for( i = 0; i < iYMaxCoord; i++ )
	{
		if( !(i % 3) )
			mvwprintw( pTspant->pGPad, iYMaxCoord - i - 1, 0, "%3d",
				(int)((float)i/scale ) );

		if( ( (int)((float)i/scale) == pTMetric->min ) ||
			 ( (int)((float)i/scale) == pTMetric->max ) ||
			 ( (int)((float)i/scale) == pTMetric->prom ) )
		{
			wattron( pTspant->pGPad, A_BOLD );
			mvwprintw( pTspant->pGPad, iYMaxCoord - i - 1, 0, "%3d",
				(int)((float)i/scale ) );
			wattroff( pTspant->pGPad, A_BOLD );
		}
	}

	/* Marcas de m�ximo valor alcanzado */
	wattron( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_RED ) );
	mvwaddch( pTspant->pGPad, ( iYMaxCoord - iLastMax * scale ) - 1, 0,
		ACS_RARROW );

	/* Marcas de m�nimo valor alcanzado */
	mvwaddch( pTspant->pGPad, ( iYMaxCoord - pTMetric->min * scale )-1,
		0, ACS_RARROW );
	wattroff( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_RED ) );

	/* Marca Promedio */
	wattron( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_GREEN ) );
	mvwaddch( pTspant->pGPad, (iYMaxCoord - pTMetric->prom * scale ) - 1,
		iXLastPos, ACS_BULLET );
	wattroff( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_GREEN ) );

	/* Marca actual */
	wattron( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_CYAN ) | A_BOLD );
	mvwaddch( pTspant->pGPad, ( iYMaxCoord - pTMetric->cur * scale ) - 1,
		iXLastPos++, ACS_DIAMOND );
	wattroff( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_CYAN ) | A_BOLD );

	/* Que estamos dibujando ? */
	wattron( pTspant->pGMetrics, A_BOLD );
	mvwprintw( pTspant->pGMetrics, 14, 10, "%s", pTMetric->nombre);
	wclrtoeol( pTspant->pGMetrics );
	mvwprintw( pTspant->pGMetrics, 15, 9, "%3d", pTMetric->max);
	wclrtoeol( pTspant->pGMetrics );
	mvwprintw( pTspant->pGMetrics, 16, 9, "%3d", pTMetric->min);
	wclrtoeol( pTspant->pGMetrics );
	mvwprintw( pTspant->pGMetrics, 17, 9, "%3d", pTMetric->prom);
	wclrtoeol( pTspant->pGMetrics );
	mvwprintw( pTspant->pGMetrics, 18, 9, "%3d", pTMetric->cur);
	wclrtoeol( pTspant->pGMetrics );
	wattroff( pTspant->pGMetrics, A_BOLD );

	wnoutrefresh( pTspant->pGPad );

	scrollok( pTspant->pGPad, TRUE );
	return 1;
}

/* Scroll */
static void int_scroll( WINDOW *win )
{
	int iYMaxCoord;
	int iXMaxCoord;
	int i;
	int j;
	chtype chr;

	getmaxyx( win, iYMaxCoord, iXMaxCoord );

	for( i = 0; i < iYMaxCoord; i++ )
		for( j = 1; j < iXMaxCoord; j++ )
		{
			chr = mvwinch( win, i, j );
			mvwaddch( win, i, j - 1, chr );
		}

		wnoutrefresh( win );
}
