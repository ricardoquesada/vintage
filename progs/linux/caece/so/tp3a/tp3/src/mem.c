/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file mem.c
 * Se encarga de hacer todo el manejo de memoria.
 * Divide la memoria en NODOs. Cada NODO puede estar lleno
 * o vacio. Tiene un puntero al duenio de ese NODO y algunas
 * otras cosas. Cuando se libera un proceso, se busca el NODO
 * y simplemente se lo pone como MEM_FREE. Cuando uno pide memoria,
 * busca algun NODO MEM_FREE que lo pueda albergar. Si no entra,
 * hace el siguiente truco:
 *	Saca los NODOs FREE y los guarda en un lugar temporal.
 *	Pega todos los NODOS BUSY (ya que los NODOS siempre estan
 *	ordenados, resulta muy facil).
 *	y luego agrega un NODO FREE al final de todos del tamanio
 *	de la suma de los antiguos NODOS FREE.
 *
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "mem.h"
#include "interfaz.h"


LIST_ENTRY qMemcode;			/**< Lista que contine los bloques de codemem */
LIST_ENTRY qMemdata;			/**< Lista que contine los bloques de datamem */

char	*mem_code = NULL;		/**< Segmento de codigo */
int	mem_code_tot;

char	*mem_data = NULL;		/**< Segmento de data */
int	mem_data_tot;


/**
 * \fn void static compat_mem()
 * \brief Funcion que se encarga de reorganizar la memoria, poniendo
 * los FREE juntos y los BUSY juntos.
 * \author Ricardo Quesada
 */
void static compat_mem()
{
	LIST_ENTRY  lTemp;
	PLIST_ENTRY pList;
	PLIST_ENTRY pLtmp;
	PMEM_NODO pN,pNprev;
	MEM_NODO Ntmp;
	int suma;

	/*
	 * 1er paso:
	 *	Sacar los los MEM_FREE y moverlos a una lista aparte
	 */
	InitializeListHead( &lTemp );
	pList = qMemcode.next;
	suma=0;
	while( !IsListEmpty( &qMemcode) && ( pList != &qMemcode ) ) {
		PLIST_ENTRY tmp;

		pN = (PMEM_NODO) pList;

		tmp = LIST_NEXT(pList);

		if( pN->status == MEM_FREE ) {
			pLtmp = RemoveHeadList( pList->prev );
			InsertTailList( &lTemp, pLtmp );
			suma++;
		}
		pList = tmp;
	}
#ifdef DEBUG_MEM
	console_out("1er paso: Se movieron %d free nodes\n",suma);
#endif

	/*
	 * 2do paso:
	 *	Juntar los nodos restantes (los que tienen cosas);
	 */
	pNprev = &Ntmp;
	pNprev->ptr = mem_code-1;
	pNprev->tot = 0;

	pList = qMemcode.next;
	while( !IsListEmpty( &qMemcode) && ( pList != &qMemcode ) ) {
		char *ant_ptr;

		pN = (PMEM_NODO) pList;

		ant_ptr = pNprev->ptr + pNprev->tot + 1;

		if( pN->status == MEM_FREE )
			console_out("Abnormal Error\n");

		if( pN->ptr > ant_ptr ) {
			/* a mover se ha dicho */
			console_out("Moviendo el proceso %s de 0x%8x a 0x%8x\n",pN->pPcb->p_nombre,pN->ptr,ant_ptr);
			memmove( ant_ptr , pN->ptr, pN->tot );
			pN->ptr = ant_ptr;
			pN->pPcb->p_codeseg = ant_ptr;
		}

		pNprev = pN;

		pList = LIST_NEXT(pList);
	}
	/*
	 * 3er paso:
	 *	La memoria libre que quedo hago que sea un solo NODO
	 *	En pNprev que la info de ultimo nodo utilizado
	 */
	suma=0;
	pList = lTemp.next;
	while( !IsListEmpty( &lTemp) && ( pList != &lTemp ) ) {
		PLIST_ENTRY tmp;

		pN = (PMEM_NODO) pList;

		suma += pN->tot;

		tmp = RemoveHeadList( &lTemp );
		pList = LIST_NEXT(pList);
		free( tmp );
	}

	/* 4to paso:
	 *	crear ese nodo grande :)
	 */

	if( suma > 0 ){
		console_out("Memoria recuperada: %d\n",suma);
		pN = malloc( sizeof(MEM_NODO));
		pN->ptr = pNprev->ptr + pNprev->tot + 1;
		pN->tot = suma;
		pN->status = MEM_FREE;
		InsertTailList( &qMemcode, (PLIST_ENTRY) pN );
	}
}

/**
 * \fn static void dar_memoria( PPCB pPcb, int tipo )
 * \brief Da memoria a un PCB. Usada internamente por get_mem()
 * \param pPcb PCB que va recibir la mem
 * \param tipo Si es memoria de data o codigo
 * \author Ricardo Quesada
 */
static void dar_memoria( PPCB pPcb, int tipo )
{
	int success;
	PLIST_ENTRY pList;
	PMEM_NODO pN;

#ifdef DEBUG_MEM
	pList = qMemcode.next;
	while( !IsListEmpty( &qMemcode) && ( pList != &qMemcode ) ) {
		pN = (PMEM_NODO) pList;
		console_out("nodo: %d 0x%8x '%s' %d\n"
				,pN->status
				,pN->ptr
				,((pN->pPcb)?pN->pPcb->p_nombre:"NULL")
				,pN->tot);
		pList = LIST_NEXT(pList);
	}
#endif

again:
	success = 0;
	pList = qMemcode.next;

	while( !IsListEmpty( &qMemcode) && ( pList != &qMemcode ) ) {

		pN = (PMEM_NODO) pList;

		if( pN->status == MEM_FREE && pN->tot >= pPcb->p_memcode ) {
			int dif;

			dif = pN->tot - pPcb->p_memcode;	/* memoria libre que me va a quedar en ese segmento */

			pPcb->p_codeseg = pN->ptr;		/* le asigno la memoria */
			pN->status = MEM_BUSY;			/* y la marco como ocupada */
			pN->tot = pPcb->p_memcode;		/* y con este nuevo tamanio */
			pN->pPcb = pPcb;			/* PCB duenio de la mem */

			if( dif ) {				/* si sobro lugar, creo un nuevo nodo */
				PMEM_NODO pNtmp;
				pNtmp = malloc( sizeof(MEM_NODO));
				memset( pNtmp,0,sizeof(MEM_NODO));
				pNtmp->tot = dif;
				pNtmp->status = MEM_FREE;
				pNtmp->ptr = pN->ptr + pN->tot;

#ifdef DEBUG_MEM
				console_out("Generando un FREE NODE de %d en 0x%8x\n",pNtmp->tot,pNtmp->ptr);
#endif

				InsertTailList( (PLIST_ENTRY) pN->next.next, (PLIST_ENTRY) pNtmp );
			}
			success = 1;
			break;
		}
		pList = LIST_NEXT(pList);
	}
	if( success == 0 ) {
		compat_mem();
		goto again;
	}
}

/**
 * \fn STATUS get_mem( PPCB pPcb )
 * \brief Aloca memoria para un proceso
 * \author Ricardo Quesada
 */
STATUS get_mem( PPCB pPcb )
{
	if( pPcb->p_memcode <= mem_code_tot ) {

		dar_memoria( pPcb, MEM_CODE );
		mem_code_tot -= pPcb->p_memcode;
		console_out("proceso '%s' con %d en 0x%8x\n",pPcb->p_nombre,pPcb->p_memcode,pPcb->p_codeseg);

		return STATUS_SUCCESS;
	} else {
		return STATUS_NOMEM;
	}
}

/**
 * \fn STATUS free_mem( PPCB pPcb )
 * \brief Aloca memoria para un proceso
 * \author Ricardo Quesada
 */
STATUS free_mem( PPCB pPcb )
{
	PLIST_ENTRY pList;
	PMEM_NODO pN;
	int success=0;

	console_out("Liberando %d bytes(%s)\n",pPcb->p_memcode, pPcb->p_nombre );
	if( pPcb->p_codeseg ) {
		pList = qMemcode.next;
#ifdef DEBUG_MEM
		console_out("Buscando nodo 0x%8x\n",pPcb->p_codeseg);
#endif
		while( !IsListEmpty( &qMemcode) && ( pList != &qMemcode ) ) {
			pN = (PMEM_NODO) pList;

			if( (pN->status == MEM_BUSY) && (pN->ptr == pPcb->p_codeseg )) {
				pN->status = MEM_FREE;
				pN->pPcb = NULL;
				mem_code_tot += pN->tot;
				success=1;
				break;
			}
			if( (pN->status == MEM_FREE) && (pN->ptr == pPcb->p_codeseg ))
				console_out("What!!!\n");

			pList = LIST_NEXT(pList);
		}
	}
	if(success==0)
		console_out("Problemas :(((\n");
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS init_mem()
 * \brief Inicializa la memoria
 * \author Ricardo Quesada
 */
STATUS init_mem()
{
	PMEM_NODO pN;

	mem_code = malloc( 1000 );		/* 1k */
	mem_code_tot = 1000;

	mem_data = malloc( 1000 );		/* 1k */
	mem_data_tot = 1000;

	InitializeListHead( &qMemcode );
	InitializeListHead( &qMemdata );

	pN = malloc( sizeof(MEM_NODO));
	memset( pN, 0, sizeof(MEM_NODO));
	pN->ptr = mem_code ;
	pN->tot = mem_code_tot;
	pN->status = MEM_FREE;
	InsertTailList( &qMemcode, (PLIST_ENTRY) pN );

	pN = malloc( sizeof(MEM_NODO));
	memset( pN, 0, sizeof(MEM_NODO));
	pN->ptr = mem_data ;
	pN->tot = mem_data_tot;
	pN->status = MEM_FREE;
	InsertTailList( &qMemdata, (PLIST_ENTRY) pN );

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS deinit_mem()
 * \brief Libera la memoria
 * \author Ricardo Quesada
 */
STATUS deinit_mem()
{
	if(mem_code)
		free(mem_code), mem_code_tot = 0;

	if(mem_data)
		free(mem_data), mem_data_tot = 0;

	return STATUS_SUCCESS;
}

/*
 *
 */
PLIST_ENTRY ptr_mem( void )
{
/*	PLIST_ENTRY pList;
	PMEM_NODO pNodo;

	pList = qMemcode.next;
	pNodo = (PMEM_NODO)pList;

	return (char *) pNodo->ptr;
*/
	return &qMemcode;
}
