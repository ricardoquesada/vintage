/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#ifndef _ADM_COMMS_
#define _ADM_COMMS_

#include <curses.h>
#include "adm_common.h"

/**
 * \file adm_comms.h
 */

/** Mensaje de error que se muestra en lugar de la ayuda si la misma no
 * existe.*/
#define ADM_COMMS_NOHELP	"No hay ayuda disponible para este comando."
#define ADM_COMMS_NOCOMMAND "No existe este comando."

/**
 * Soporte para la implementaci�n de comandos en \c spant.
 * Esta estructura tiene como prop�sito facilitar el manejo del pasaje de
 * par�metros a las rutinas de comandos, debido a que las rutinas de comandos
 * s�lo soportan el pasaje de un parametro adicional a las funciones que
 * soportan, se puede volver engorroso el estar creando estructuras
 * auxiliares, por lo que se crea esta estructura que usa un conjunto de
 * variables comunes dentro del �mbito de la aplicaci�n que estamos creando.
 * Adem�s al saber el tipo de variables que esperamos, podemos realizar
 * algunos chequeos extra.
 */
typedef struct TpantComms
{
	TSPant *pTspant;	/**< Las "propiedades" de spant.*/
	WINDOW *pTWindow;	/**< La ventana que se va a utilizar como salida del
							comando a ejecutar.*/
}TSPantComms;

void adm_comms_add_help( TSPant *pTspant, WINDOW *pTDisplayWin );
int adm_comms_clear( char *cpArg, WINDOW *pTClearWin );
int adm_comms_clock_reset( char *cpArg, TSPant *pTspant );
int adm_comms_clock_set_speed( char *cpArg, TSPant *pTspant );
int adm_comms_list( char *cpArg, TSPant *pTspant );
int adm_comms_mem( char *cpArg, TSPant *pTspant );
int adm_comms_ps( char *cpArg, TSPant *pTspant );
int adm_comms_graph( char *cpArg, TSPant *pTspant );


#endif
