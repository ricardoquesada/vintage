#ifndef _ADM_GRAPH_
#define _ADM_GRAPH_

#include "metric.h"
#include "adm_common.h"

int adm_graph_mem( TSPant *pTspant );
int adm_graph_ps( TSPant *pTspant );
int adm_graph_gqueued( TSPant *pTspant );
int adm_graph_metric( TSPant *pTspant, PMETRIC pTMetric );
#endif
