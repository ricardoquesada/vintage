/*
 * Trabajo Practico Numero 2. Scheduling.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file sched.c
 * Archivo que contiene las funciones de scheduling.
 * Si un proceso usa la politica SCHED_RR (RoundRobin), es  un proceso que
 * deja la CPU luego de consumir su TIME_SLICE. Si en cambio es SCHED_FIFO,
 * una vez que tome la CPU, no la larga hasta * que termine de ejecutarse.
 * Los procesos son encolados en las qRun (colas de prioridades) salvo que
 * necesiten usar una CPU en particular, y en ese caso, no se le calcula
 * la prioridad y son encolados en la cola particular de esa CPU.
 * Existe una cola de procesos en PROC_SLEEP, que no salen de esa cola,
 * hasta que ocurra el evento que estan esperando.
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#ifdef HAVE_UNIX
#include <unistd.h>
#endif

#include "sched.h"
#include "tipos.h"
#include "interfaz.h"
#include "token.h"
#include "mem.h"

LIST_ENTRY g_proc_list;		/**< Lista global que contiene a todos los procesos */

LIST_ENTRY qRun[ NRQ ];		/**< Array de lista de prioridades */
int qRunCant[ NRQ ];


CPU cpu[NRCPU];			/**< Array de CPUs */

unsigned long jiffies;		/**< Contador de ciclos */
static unsigned long g_pid;	/**< ultimo pid */

/**
 * \fn resched( PPCB pPcb )
 * \brief Recalcula la prioridad del proceso, y lo encola donde debe.
 * \param pPcb puntero al proceso que se le quiero recalcular la prioridad.
 * \author Ricardo Quesada
 */ 
void resched( PPCB pPcb )
{
	int p_estcpu;
	int temp;

	assert( pPcb );

	PDEBUG("resched()\n");

	/* Esta en estado 'runneable' xq llego aca */
	pPcb->p_estado = PROC_RUN;

	/*
	 * Si el proceso requiere especificamente una CPU, va directamente a la
	 * cola de esa CPU salteandose las colas de prioridades. Ese proceso tiene
	 * prioridad sobre todos los otros procesos
	 */
	if( pPcb->p_cpu >= 0 && pPcb->p_cpu < NRCPU ) {
		InsertTailList( &cpu[pPcb->p_cpu].qRun, (PLIST_ENTRY) pPcb );
		cpu[pPcb->p_cpu].qRunCant++;
		return;
	}

	/*
	 * De lo contrario usa las colas de prioridades.
	 * Para simplificar su lectura, p_estcpu es solamente lo que
	 * fue utilizado por el proceso.
	 * TODO: Calcular 'bien' el p_estcpu, utilizando un decay filter.
	 */
	p_estcpu = pPcb->p_cpu_usada;

	temp = pPcb->p_basepriority + p_estcpu /4 + pPcb->p_nice * 2 ;

	if( temp < pPcb->p_basepriority )
		temp = pPcb->p_basepriority;
	else if( temp > 127 )
		temp = 127;

	pPcb->p_priority = temp;

	/* Veo en que cola lo encolo */
	temp = temp / (128/NRQ);

	InsertTailList( &qRun[temp], (PLIST_ENTRY) pPcb );
	qRunCant[temp]++;
}

/**
 * \fn void sched_cpu()
 * \brief Funcion que hace el scheduling sobre los procesos que esten en las CPUs
 * \author Ricardo Quesada
 */ 
void sched_cpu( void )
{
	int i;

	PDEBUG("sched_cpu()\n");

	for(i=0;i<NRCPU;i++) {
		if(cpu[i].c_estado == CPU_RUNNING &&  \
			cpu[i].c_proceso->p_politica == SCHED_RR && \
			cpu[i].c_proceso->p_cpu_usada % TIME_SLICE == 0) {

			resched( cpu[i].c_proceso );

			cpu[i].c_estado = CPU_IDLE;
			cpu[i].c_proceso = NULL;
		}
	}
}

/**
 * \fn PPCB next_proc( int i )
 * \brief devuelve el siguiente proceso encolado
 * \param i CPU que pide el proceso
 * \return PPCB el proceso que esta encola o NULL
 * \author Ricardo Quesada
 */
PPCB next_proc( int i )
{
	int j=0;
	PPCB pPcb=NULL;

	PDEBUG("next_proc()\n");
	/* Si hay algun proceso encola en su cola, uso ese */
	if( cpu[i].qRunCant ) {
		pPcb = (PPCB) RemoveHeadList( &cpu[i].qRun );
		cpu[i].qRunCant--;
		return pPcb;
	}

	while( j < NRQ && qRunCant[j] == 0 ){
		j++;
	}
	if( j < NRQ ) {
		pPcb = (PPCB) RemoveHeadList( &qRun[j] );
		qRunCant[j]--;
	}
	return pPcb;
}


/** 
 * \fn void run(void)
 * \brief Funcion que hace andar los procesos
 * \author Ricardo Quesada
 */
void run(void)
{
	int i;
	PPCB pPcb;

	/* Funcion dividida en 2 partes para que sea mas entendible */

	PDEBUG("run()\n");

#ifdef DEBUG
	for(i=0;i<NRCPU;i++) {
		printf("CPU[%i]\n",i);
		printf("\tc_uso=%d\n",cpu[i].c_uso);
		printf("\tc_estado=%d\n",cpu[i].c_estado);
		if(cpu[i].c_proceso)
			printf("\tc_proceso=%s\n",cpu[i].c_proceso->p_nombre);
	}
#endif

	/* 
	 * 1ra parte:
	 * Corre los procesos que estan en las CPU
	 */
	for(i=0;i<NRCPU;i++) {
		if( cpu[i].c_estado == CPU_RUNNING ) {
			STATUS s;
			cpu[i].c_uso++;

			s = run_proc( cpu[i].c_proceso );
			switch( s ) {
			case STATUS_END:
			case STATUS_INVALID:
				free_mem( cpu[i].c_proceso );
				cpu[i].c_proceso->p_estado = PROC_TERMINADO;
				cpu[i].c_proceso->p_t_run_end = jiffies;
				cpu[i].c_proceso = NULL;
				cpu[i].c_estado = CPU_IDLE;
				break;
			case STATUS_EVENT:
				cpu[i].c_proceso->p_estado = PROC_SLEEP;
				cpu[i].c_proceso = NULL;
				cpu[i].c_estado = CPU_IDLE;
				break;
			default:
				break;
			}
		}
	}

	sched_cpu();

	/*
	 * 2da parte:
	 * Se fija las CPU libres y les asigna un proceso
	 */
	for(i=0;i<NRCPU;i++) {

		if( cpu[i].c_estado == CPU_IDLE ) {
			if( (pPcb = next_proc(i)) ) {
				PDEBUG(("next_proc=%s\n",pPcb->p_nombre));
				cpu[i].c_proceso =  pPcb;
				cpu[i].c_estado = CPU_RUNNING;
			} 
			/* como puede haber procesos, para otras cpu, no salgo del for */
		}
	}

	jiffies++;
}


/** 
 * \fn STATUS sched_init()
 * \brief Inicializa todo lo que tiene que ver con el scheduling y run
 * \author Ricardo Quesada
 */
STATUS sched_init()
{
	int i;

	/* 
	 * Inicializa las colas
	 */
	for(i=0;i<NRQ;i++) {
		InitializeListHead( &qRun[i] );
		qRunCant[i]=0;
	}
	InitializeListHead( &g_proc_list );


	/*
	 * Inicializa las CPU
	 */
	for(i=0;i<NRCPU;i++) {
		cpu[i].c_proceso = NULL;
		cpu[i].c_estado = CPU_IDLE;
		cpu[i].c_uso = 0;
		InitializeListHead( &cpu[i].qRun );
		cpu[i].qRunCant=0;
	}

	jiffies = 0;
	g_pid = 0;

	return STATUS_SUCCESS;
}

/**
 * \fn STATUS queue_proc( PPCB pPcb )
 * \brief encola el proceso recien creado
 * \param pPcb puntero al proceso a insertar
 * \return Devuelve la copia que se creo, o NULL si hubo error
 */
PPCB queue_proc( PPCB pPcb )
{
	assert( pPcb );

	PDEBUG("queue_proc()\n");

	pPcb->p_pid = g_pid++;
	pPcb->p_estado = PROC_IDL;
	pPcb->p_t_run_start = jiffies;
	pPcb->p_cpu_usada = 0;
	resched( pPcb );	

	InsertHeadList( &g_proc_list, (PLIST_ENTRY) &pPcb->p_next );

	return pPcb;
}

/**
 * \fn PPCB proclist2pcb( PLIST_ENTRY pList )
 * \brief dado un proc list devuelve el PPCB correspondiente
 * \param pList, puntero al proc list
 * \return Devuelve un ptr al PCB
 */
PPCB proclist2pcb( PLIST_ENTRY pList )
{
	char *temp = (char*) pList;
	temp -= sizeof(LIST_ENTRY);
	return (PPCB) temp;
}
