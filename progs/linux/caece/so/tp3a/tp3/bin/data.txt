! Formato del archivo:
! todos los valores estan separados por un 'tab' o sea \t en C
! y estos son los valores:
! nombre	Nombre generico del proceso
! time_rel	Tiempo relativo de cuando se debe encolar
! prioridad	Prioridad Base del archivo( entre 0 y 50. ver sched.h)
! cpu		Cpu preferida o -1
! policy	0 para Round Robin o 1 para FIFO (Si usa FIFO, no hay prioridades )
! nice		entre -20 y 19

backup	0	50	-1	0	0
ls	0	50	-1	0	0
lpr	1	50	-1	0	0
dd	52	30	-1	0	0
vi	40	50	-1	1	0
cpio	0	20	-1	0	0
splay	28	10	-1	1	0
cat	80	10	-1	0	0
cc	5	10	-1	0	0
g++	10	10	-1	0	0
ee	70	10	-1	1	0
nvi	2	10	-1	0	0
gv	150	50	-1	0	0
df	0	50	-1	0	0
gzip	3	0	-1	1	0
ls	3	50	-1	0	0
ll	4000	50	-1	0	0
