! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	87	0	0
	burst	25
	read	1	2
	burst	30
	write	1	5
	burst	2

	read	2	3
	write	2	3

	lda	40
	burst	37
	deca
	jnz	-3

	read	2	3
	write	2	3

	burst	100

	exit	0
.end
