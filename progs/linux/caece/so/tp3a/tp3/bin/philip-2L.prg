!
! philip-2.prg
! Segundo acercamiento al problema de los filosofos, Stallings.
! No produce Deadlock, pero puede producir Starvation, es del tipo 'Diestro'.
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	?	0	0
	lda	1
	burst 30

! Esto permite variables, o hacemos 5 programitas con sus respectivos SEMs??
	wait SEM_A 0	// wait_room

	wait SEM 0
	wait SEM 1	

	burst 20

	signal SEM 1
	signal SEM 0

	signal SEM_A 0	// wait_room

	jnz -22

	exit	0
.end
