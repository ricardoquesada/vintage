!
! philip-1R.prg
! Primer acercamiento al problema de los filosofos, Stallings.
! Produce Deadlock, es del tipo 'Zurdo'.
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	?	0	0
	lda	1
	burst 30

! Esto permite variables, o hacemos 5 programitas con sus respectivos SEMs??
	wait SEM 1
	wait SEM 0	

	burst 20

	signal SEM 0
	signal SEM 1

	jnz -16

	exit	0
.end
