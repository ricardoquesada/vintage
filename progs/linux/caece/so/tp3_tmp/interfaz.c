/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file interfaz.c
 * Funciones que tienen que ver con la interfaz
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdarg.h>

#include "tipos.h"
#include "sched.h"
#include "metric.h"
#include "interfaz.h"
#include "adm_common.h"

static TSPant pantalla;

/**
 * \fn STATUS interfaz_gfx( PMETRIC pM )
 * \brief Muestra el estado de cada cola
 */
STATUS interfaz_gfx( PMETRIC pM )
{
#ifdef DEBUG_INTERFAZ
	printf("nombre:%s\n",pM->nombre);
	printf("min:%d\n",pM->min);
	printf("max:%d\n",pM->max);
	printf("promedio:%d\n",pM->prom);
	printf("current:%d\n",pM->cur);
#endif
	pantalla.ipGraph( &pantalla, pM );
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS interfaz_init()
 * \brief Inicializa la interfaz
 */
STATUS interfaz_init()
{
	pantalla_init( &pantalla );
	return STATUS_ERROR;
}

/**
 * \fn STATUS interfaz_deinit()
 * \brief Desinicializa la interfaz
 */
STATUS interfaz_deinit()
{
	pantalla_end( &pantalla );
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS interfaz_refresh();
 * \brief Refresca la pantalla
 */
STATUS interfaz_refresh()
{
	adm_status_refresh( &pantalla );
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_out();
 * \brief Saca por consola un string
 */
STATUS console_out( char *str, ...)
{
        va_list args;
	char buf[2000];

	va_start(args, str);
	vsprintf(buf, str, args);
	va_end(args);

	wprintw( pantalla.pGConsole, "%s", buf );
	wrefresh( pantalla.pGConsole );

	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_input();
 * \brief Espera una tecla por consola
 */
STATUS console_input()
{
	int iKey;

	if( ( ( iKey = wgetch( pantalla.pGConsole )  ) != ERR ) && ( iKey == KEY_IC ) )	{
		adm_console( &pantalla );
	}
	return STATUS_SUCCESS;
}
