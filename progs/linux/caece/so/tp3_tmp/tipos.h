/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file tipos.h
 * Definicion de tipos generales.
 * \author Ricardo Quesada
 */

#ifndef __TIPOS_H
#define __TIPOS_H


/** \typedef struct _LIST_ENTRY
 * \brief Definicion de list doble (ademas es circular)
 */
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY *next;	/**< next LIST_ENTRY */
	struct _LIST_ENTRY *prev;	/**< prev LIST_ENTRY */
} LIST_ENTRY, *PLIST_ENTRY;

/**
 * \typedef enum STATUS
 * \brief Posibles valores para STATUS
 */
typedef enum {
	STATUS_SUCCESS,			/**< Todo bien */
	STATUS_ERROR,			/**< Error general */
	STATUS_OVERFLOW,		/**< Hubo un overflow */
	STATUS_UNDERFLOW,		/**< Hubo un underflow */
	STATUS_FILEERROR,		/**< Error con el archivo */
	STATUS_FILEEOF,			/**< El archivo se termino */
	STATUS_NOTREADY,		/**< No esta listo */
	STATUS_NOMEM,			/**< Sin memoria */
} STATUS, *PSTATUS;


/**
 * Colores.
 */
#define PANT_GREEN 	1
#define PANT_RED	2
#define PANT_CYAN	3
#define PANT_WHITE	4


#define LENTRY_NULL {NULL,NULL}

#define LIST_NEXT(Entry) (((PLIST_ENTRY)Entry)->next)
#define LIST_PREV(Entry) (((PLIST_ENTRY)Entry)->prev)

//
//  Doubly-linked list manipulation routines.  Implemented as macros
//  but logically these are procedures.
//

//
//  VOID
//  InitializeListHead(
//      PLIST_ENTRY ListHead
//      );
//

#define InitializeListHead(ListHead) (\
    (ListHead)->next = (ListHead)->prev = (ListHead))

//
//  BOOLEAN
//  IsListEmpty(
//      PLIST_ENTRY ListHead
//      );
//

#define IsListEmpty(ListHead) \
    ((ListHead)->next == (ListHead))

//
//  PLIST_ENTRY
//  RemoveHeadList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveHeadList(ListHead) \
    (ListHead)->next;\
    {RemoveEntryList((ListHead)->next)}

//
//  PLIST_ENTRY
//  RemoveTailList(
//      PLIST_ENTRY ListHead
//      );
//

#define RemoveTailList(ListHead) \
    (ListHead)->prev;\
    {RemoveEntryList((ListHead)->prev)}

//
//  VOID
//  RemoveEntryList(
//      PLIST_ENTRY Entry
//      );
//

#define RemoveEntryList(Entry) {\
    PLIST_ENTRY _EX_prev;\
    PLIST_ENTRY _EX_next;\
    _EX_next = (Entry)->next;\
    _EX_prev = (Entry)->prev;\
    _EX_prev->next = _EX_next;\
    _EX_next->prev = _EX_prev;\
    }

//
//  VOID
//  InsertTailList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertTailList(ListHead,Entry) {\
    PLIST_ENTRY _EX_prev;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_prev = _EX_ListHead->prev;\
    (Entry)->next = _EX_ListHead;\
    (Entry)->prev = _EX_prev;\
    _EX_prev->next = (Entry);\
    _EX_ListHead->prev = (Entry);\
    }

//
//  VOID
//  InsertHeadList(
//      PLIST_ENTRY ListHead,
//      PLIST_ENTRY Entry
//      );
//

#define InsertHeadList(ListHead,Entry) {\
    PLIST_ENTRY _EX_next;\
    PLIST_ENTRY _EX_ListHead;\
    _EX_ListHead = (ListHead);\
    _EX_next = _EX_ListHead->next;\
    (Entry)->next = _EX_next;\
    (Entry)->prev = _EX_ListHead;\
    _EX_next->prev = (Entry);\
    _EX_ListHead->next = (Entry);\
    }


#endif /* __TIPOS_H */
