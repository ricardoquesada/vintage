/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */
#ifndef _NCOLA_COMMS_
#define _NCOLA_COMMS_

#include <curses.h>
#include "ncola_common.h"

/**
 * \file ncola_comms.h
 */

/** Mensaje de error que se muestra en lugar de la ayuda si la misma no
 * existe.*/
#define NCOLA_COMMS_NOHELP	"No hay ayuda disponible para este comando."
#define NCOLA_COMMS_NOCOMMAND "No existe este comando."

/**
 * Soporte para la implementaci�n de comandos en \c ncola.
 * Esta estructura tiene como prop�sito facilitar el manejo del pasaje de
 * par�metros a las rutinas de comandos, debido a que las rutinas de comandos
 * s�lo soportan el pasaje de un parametro adicional a las funciones que
 * soportan, se puede volver engorroso el estar creando estructuras
 * auxiliares, por lo que se crea esta estructura que usa un conjunto de
 * variables comunes dentro del �mbito de la aplicaci�n que estamos creando.
 * Adem�s al saber el tipo de variables que esperamos, podemos realizar
 * algunos chequeos extra.
 */
typedef struct NcolaComms
{
	TNCola *pTncola;	/**< Las "propiedades" de ncola.*/
	WINDOW *pTWindow;	/**< La ventana que se va a utilizar como salida del
							comando a ejecutar.*/
}TNColaComms;

void ncola_comms_add_help( TNCola *pTncola, WINDOW *pTDisplayWin );
int ncola_comms_clear( char *cpArg, WINDOW *pTClearWin );
int ncola_comms_clock_reset( char *cpArg, TNCola *pTncola );
int ncola_comms_clock_set_speed( char *cpArg, TNCola *pTncola );
int ncola_comms_list( char *cpArg, TNCola *pTncola );
int ncola_comms_ps( char *cpArg, TNCola *pTncola );
int ncola_comms_graph( char *cpArg, TNCola *pTncola );


#endif
