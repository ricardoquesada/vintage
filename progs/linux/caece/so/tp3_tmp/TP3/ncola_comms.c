/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <curses.h>
#include <assert.h>
#include <stdlib.h>

#include "cons.h"
#include "tipos.h"
#include "archivo.h"
#include "ncola_graph.h"
#include "ncola_comms.h"
#include "ncola_common.h"

/**
 * \file ncola_comms.c
 * Funciones para el manejo de pantalla
 */

static int ncola_comms_help( char *cpArg, TNColaComms *data );

/**
 * Wrapper para el comando de Help.
 * Se utiliza un wrapper, ya que se necesitan pasar varios par�metros a la
 * rutina real de \c help. Entonces se crea una estructura \e est�tica
 * conteniendo la info, no hay mayor problema en que esta estructura sea 
 * est�tica ya que no se supone que el \e agregado del comando se haga m�s de
 * una vez.
 * @param pTncola La estructura que guarda todas las variables comunes del
 * programa, no puede ser \c NULL.
 * @param pDisplayWin La ventana en la cual se mostrar� el resultado del help,
 * no puede ser \c NULL.
 * \author Picorelli Marcelo.
 */
void ncola_comms_add_help( TNCola *pTncola, WINDOW *pDisplayWin )
{
	static TNColaComms TData;

	assert( pTncola != NULL );
	assert( pDisplayWin != NULL );

	TData.pTncola = pTncola;
	TData.pTWindow = pDisplayWin;

	cons_command_add( &pTncola->pTCommands, "help",
		"Brinda ayuda sobre el comando que se le pasa como argumento.",
		ncola_comms_help, &TData );
}

/**
 * Limpia la pantalla que se le indica.
 * En este momento solo es capaz de limpiar una pantalla a la vez, no acepta
 * par�metros, y no se puede modificar la ventana que limpia.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTClearWin La ventana a limpiar, no puede ser \c NULL.
 */
int ncola_comms_clear( char *cpArg, WINDOW *pTClearWin )
{
	assert( pTClearWin != NULL );

	wclear( pTClearWin );
	wrefresh( pTClearWin );

	return 1;
}

/**
 * Brinda ayuda sobre el comando que se le pide.
 * Busca el comando que se le pasa y de encontrarlo, busca la descripci�n del
 * mismo mostr�ndolo en la ventana que se le indica. De no encotrar el
 * comando, muestra un mensaje de error predefinido.
 * @param Tncola Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * @param cpArg Es el nombre del comando, y es proporcionado por la
 * aplicaci�n, no por el programador.
 * @param pTWindow Ventana en la cual se desplegar� el resultado del comando,
 *		no puede ser \c NULL.
 * @returns 1 En caso de �xito, 0 en caso de falla.
 */
static int ncola_comms_help( char *cpArg, TNColaComms *pTData )
{
	TConsoleBind *pTCommand = NULL;

	assert( pTData != NULL );

	if( cpArg )
	{
		if( ( pTCommand = cons_command_get( pTData->pTncola->pTCommands,
			cpArg ) ) )
		{
			if( pTCommand->cpHelp )
			{
				wprintw( pTData->pTWindow, "%s: %s\n", cpArg, pTCommand->cpHelp );
			}
			else
			{
				wprintw( pTData->pTWindow, "%s: %s\n", cpArg, NCOLA_COMMS_NOHELP );
			}
		}
		else
		{
			wprintw( pTData->pTWindow, "%s: %s\n", cpArg, NCOLA_COMMS_NOCOMMAND );
			wrefresh( pTData->pTWindow );
			return 0;
		}
	}
	wrefresh( pTData->pTWindow );

	return 1;
}

/**
 *
 */
int ncola_comms_file_open( char *cpArg, WINDOW *pDisplayWindow )
{
	assert( pDisplayWindow != NULL );

	/* Seria interesante poder testear el status del archivo, para saber si 
	* se esta trabajando con uno o no, en caso de estar trabajando debiera
	* devolver un mensaje de error, y debieramos crear el comando reopen, o algo
	* asi.*/

	if( !cpArg )
	{
		waddstr( pDisplayWindow, "open: Falta especificar el nombre del archivo.");
		return 0;
	}
/*
	if( !( archivo_init( cpArg ) ) )
	{
		wprintw( pDisplayWindow, "Error: El archivo %s no pudo ser abierto\n",
			cpArg );
		return 0;
	} 
	else
	{
		wprintw( pDisplayWindow, "OK: Tomando entrada del archivo %s\n", cpArg );
	}
*/

	return 1;
}

/**
 * Coloca al clock interno en cero.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param ipClock Direccion del clock, no puede ser \c NULL.
 * \author Picorelli Marcelo.
 */
int ncola_comms_clock_reset( char *cpArg, TNCola *pTncola )
{
	assert( pTncola != NULL );

	pTncola->iClock = 0;
	waddstr( pTncola->pGConsole, "Clock reset.\n" );
	wrefresh( pTncola->pGConsole );
	return 1;
}

/**
 * Cambia la velocidad del clock interno.
 * @param cpArg La nueva velocidad.
 * @param pTncola Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * \bug No estoy revisando bien el parametro, capaz que falle la conversi�n.
 * \author Picorelli Marcelo. 
 */
int ncola_comms_clock_set_speed( char *cpArg, TNCola *pTncola )
{
	assert( pTncola != NULL );

	if( strlen( cpArg ) )
	{
		pTncola->iSpeed = atoi( cpArg );
		wprintw( pTncola->pGConsole, "Nueva velocidad del clock = %d\n",
			pTncola->iSpeed );
	}
	else
	{
		wprintw( pTncola->pGConsole,"Velocidad del clock = %d\n", pTncola->iSpeed);
	}
	wrefresh( pTncola->pGConsole );
	return 1;
}

/**
 * Muestra todos los comandos disponibles.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTncola Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * \author Picorelli Marcelo.
 */
int ncola_comms_list( char *cpArg, TNCola *pTncola )
{
	int i;
	TConsoleBind *pTCommand;
	TConsoleBind **pComms = pTncola->pTCommands;
	
	assert( pTncola != NULL );

	wclear( pTncola->pGPad );
	for( i = 0; pComms[i]; i++ )
	{
		pTCommand = cons_command_get( pComms, pComms[i]->cpCommand );	
		wprintw( pTncola->pGPad, "%s: %s\n", pComms[i]->cpCommand, 
			pTCommand->cpHelp ? pTCommand->cpHelp : "No hay ayuda disponible." );
	}
	wrefresh( pTncola->pGPad );

	return 1;
}

/** 
 * Wrapper para la funci�on ncola_graph_ps.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTncola Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * \author Picorelli Marcelo.
 * 
 */
int ncola_comms_ps( char *cpArg, TNCola *pTncola )
{
	assert( pTncola != NULL );
	ncola_graph_ps( pTncola );

	return 1;
}

/**
 * Permite seleccionar la m�trica a graphicar.
 * @param cpArg Par�metro en com�n de todos los \e callbacks, ignorado.
 * @param pTncola Es la estructura que mantiene todos los datos del programa,
 * provista por la aplicaci�n, no por el programador.
 * @returns 1 En caso de �xito, -1 en caso de error;
 * \author Picorelli Marcelo.
 * 
 */
int ncola_comms_graph( char *cpArg, TNCola *pTncola )
{
	int iMetr;
	PMETRIC pTMetric = NULL;
	
	assert( pTncola != NULL );

	if( !cpArg )
		return -1;

	/* Tratamos de interpretar el argumento como un n�mero */
	/* El problema es que si queremos ver la metrica cero no funciona */
	if( atoi( cpArg ) ) 
	{
		if( ( pTMetric = metric_get( atoi( cpArg ) ) ) != NULL ) 
		{
			pTncola->pTMetric = pTMetric;
			wprintw( pTncola->pGConsole, "OK: Nueva m�trica %s\n", cpArg );
			wrefresh( pTncola->pGConsole );
			return 1;
		}
	}

	/* Si pidieron la m�trica como un nombre, pasamos por ac�. */
	if( ( iMetr = metric_id_get( cpArg ) ) != -1 )
	{
		pTncola->pTMetric = metric_get( iMetr );
		wprintw( pTncola->pGConsole, "OK: Nueva m�trica %s\n", cpArg );
		wrefresh( pTncola->pGConsole );
		return 1;
	}

	wprintw( pTncola->pGConsole, "Error: La m�trica %s no existe\n", cpArg );
	return -1;
}
