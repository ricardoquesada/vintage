/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file main.c
 * Archivo que tiene el main loop y la funcion main.
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "metric.h"
#include "interfaz.h"


/*
 * Esta funcion esta en este archivo ya que no es ninguna
 * primitiva de sched, ni nada de eso. Lo unico que hace es 
 * simular que ocurren ciertos eventos con los procesos que estan corriendo
 * en la CPU, y esas cosas
 */
/**
 * \fn void demo_events(void)
 * \brief Funcion 'demo' de como generar eventos
 * \author Ricardo Quesada
 */
void demo_events(void)
{
	static unsigned int last_tick=0;
	int i,evt;
	PPCB pPcb;

	/* Genera un evento al azar una vez por ciclo */
	evt = (int) (((float)NREVENTS)*rand()/(RAND_MAX+1.0));
	generate_event( evt );

	/*
	 * Duerme algun proceso que este corriendo en alguna CPU, haciendo
	 * esperar un evento generado al azar.
	 * Esto ocurre cada vez que jiffies >= last_tick
	 */
	if( last_tick < jiffies ) {
		/* give me an event */
		evt = (int) (((float)NREVENTS)*rand()/(RAND_MAX+1.0));

		/* find a real running process */
		for(i=0;i<NRCPU;i++) {
			if( cpu[i].c_estado == CPU_RUNNING ) {
				pPcb = cpu[i].c_proceso;
				cpu[i].c_proceso = NULL;
				cpu[i].c_estado = CPU_IDLE;

				wait_event( pPcb, evt );
				break;
			}
		}


		/* generate new last_tick */
		i = 1 + (int) (10.0*rand()/(RAND_MAX+1.0));

		last_tick = jiffies + i;
	}
}


/**
 * \fn STATUS main_loop(void)
 * \brief Main loop (que mas)
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS main_loop(void)
{ 
	PCB pcb;

	while(1) {
#ifdef DEBUG
		printf("--> ciclo %d <--\n",(unsigned)jiffies);
#endif

		console_input();

		memset( &pcb, 0, sizeof(PCB));
		while( archivo_read( &pcb ) == STATUS_SUCCESS ) 
			queue_proc( (void *) &pcb );
	
		demo_events();

		run();

		metric_display_all();

	}
}

/**
 * \fn int main( void )
 * \brief su nombre lo dice todo
 */
int main( void )
{
	sched_init();
	archivo_init("data.txt");
	metric_init();
	interfaz_init();

	main_loop();

	interfaz_deinit();
	archivo_close();
	metric_flush();

	return 0;
}
