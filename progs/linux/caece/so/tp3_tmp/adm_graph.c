/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <assert.h>
#include <curses.h>

#include "tipos.h"
#include "sched.h"
#include "adm_graph.h"
#include "est_pantalla.h"

/**
 * \file adm_graph.c
 * Graficador de funciones
 */

/**
 * Muestra el estado de los procesos.
 * @param pTspant Las variables que conforman al programa, no puede ser \c
 * NULL.
 * \author Picorelli Marcelo.
 */
int adm_graph_ps( TSPant *pTspant )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;

	char *cpEstado[] = { "CREADO", "CORRIENDO", "DORMIDO  ", "PARADO  ", "ZOMBIE", "TERMINADO" };

	assert( pTspant != NULL );

	wclear( pTspant->pGPad );
	mvwaddstr( pTspant->pGPad, 0,0, "Nombre\tEstado\t\tStart\tEnd\n" );

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		wprintw( pTspant->pGPad, "%s\t%s\t%d\t%d\n", pPcb->p_nombre,
			cpEstado[pPcb->p_estado], pPcb->p_t_in, pPcb->p_t_run_start );
		pList = LIST_NEXT( pList );
	}

	waddstr( pTspant->pGPad, "Presione una tecla." );
	wrefresh( pTspant->pGPad );

	nodelay( pTspant->pGPad, FALSE );
	wgetch( pTspant->pGPad );
	nodelay( pTspant->pGPad, TRUE );
	wclear( pTspant->pGPad );

	return 1;
}

/**
 *
 */
int adm_graph_gqueued( TSPant *pTspant )
{
	static int iMaxQueued = 0;/* TODO: colocar los datos en sus propias estructs*/
	static int iXLastPos = 0;
	static int iXMaxCoord = 0;
	static int iYMaxCoord = 0;
	int iQueued = 0;

	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;

	assert( pTspant != NULL );

	if( !iXMaxCoord )
		getmaxyx( pTspant->pGPad, iYMaxCoord, iXMaxCoord );

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		if( pPcb->p_estado == PROC_RUN )
			iQueued++;
		pList = LIST_NEXT( pList );
	}

	if( !iXLastPos )
		wclear( pTspant->pGPad );

	if( iQueued > iMaxQueued )
	{
		mvwaddch( pTspant->pGPad, iYMaxCoord - iMaxQueued - 1, 0, ' ' );
		mvwaddch( pTspant->pGPad, iYMaxCoord - iMaxQueued - 1,iXMaxCoord - 2, ' ' );
		iMaxQueued = iQueued;
	}

	mvwaddch( pTspant->pGPad, iYMaxCoord - iMaxQueued - 1, 0, '>' );
	mvwaddch( pTspant->pGPad, iYMaxCoord - iMaxQueued - 1, iXMaxCoord - 2, '<' );
	mvwaddch( pTspant->pGPad, iYMaxCoord - iQueued - 1, iXLastPos++, '+' );

	wnoutrefresh( pTspant->pGPad );

	if( iXLastPos == iXMaxCoord - 1)
		iXLastPos = 0;

	return 1;
}

/**
 * Grafica las metricas.
 * Se encarga de realizar los graficos de las metricas, mostrando m�ximo,
 * m�nimo, promedio y actual de la m�trica que se le pasa.
 * @param pTspant Las variables que conforman al programa, no puede ser \c
 * NULL.
 * @param pTMetric Las variables que hacen a la m�trica.
 * \author Picorelli Marcelo.
 * \bug En este momento no dibuja la escala, ni cambia la misma con lo que si
 * algun parametro excede el tama�o disponible en pantalla, simplemente
 * desaparece. Adem�s ciertas m�tricas no proveen un valor para actual, con lo
 * que el mismo siempre figura como 0.
 */
int adm_graph_metric( TSPant *pTspant, PMETRIC pTMetric  )
{
	static int iXLastPos = 0;		/* Ultima posicion dibujada. */
	static int iXMaxCoord = 0;	/* Ancho de la ventana. */
	static int iYMaxCoord = 0;	/* Altura de la ventana. */
	static int iLastMax = 0;		/* Ultimo valor m�ximo. */

	assert( pTspant != NULL );
	assert( pTMetric != NULL );

	if( !iXMaxCoord )
		getmaxyx( pTspant->pGPad, iYMaxCoord, iXMaxCoord );

	/* Alcanzamos el final de la pantalla. */
	if( !iXLastPos )
		wclear( pTspant->pGPad );

	if( iLastMax < pTMetric->max )
	{
		mvwaddch( pTspant->pGPad, iYMaxCoord - iLastMax - 1, 0, ' ' );
		mvwaddch( pTspant->pGPad, iYMaxCoord - iLastMax - 1,iXMaxCoord - 2, ' ' );
		iLastMax = pTMetric->max;
	}


	/* Marcas de m�ximo valor alcanzado */
	wattron( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_RED ) );
	mvwaddch( pTspant->pGPad, iYMaxCoord - iLastMax - 1, 0, '>' );
	mvwaddch( pTspant->pGPad, iYMaxCoord - iLastMax - 1, iXMaxCoord - 2, '<' );

	/* Marcas de m�nimo valor alcanzado */
	mvwaddch( pTspant->pGPad, iYMaxCoord - pTMetric->min- 1, 0, '>' );
	mvwaddch( pTspant->pGPad, iYMaxCoord - pTMetric->min- 1, iXMaxCoord - 2, '<');
	wattroff( pTspant->pGPad, (attr_t)COLOR_PAIR( PANT_RED ) );

	/* Marca Promedio */
	wattron( pTspant->pGPad, (attr_t)COLOR_PAIR( pTMetric->attrib ) );
	mvwaddch( pTspant->pGPad, iYMaxCoord - pTMetric->prom - 1,iXLastPos, '+' );
	wattroff( pTspant->pGPad, (attr_t)COLOR_PAIR( pTMetric->attrib ) );

	/* Marca actual */
	mvwaddch( pTspant->pGPad, iYMaxCoord - pTMetric->cur - 1, iXLastPos++, '*' );

	/* Que estamos dibujando ? */
	wattron( pTspant->pGMetrics, (attr_t)COLOR_PAIR( pTMetric->attrib ) );
	mvwaddstr( pTspant->pGMetrics, 18, 0, pTMetric->nombre );
	wattroff( pTspant->pGMetrics, (attr_t)COLOR_PAIR( pTMetric->attrib ) );

	wnoutrefresh( pTspant->pGPad );

	if( iXLastPos == iXMaxCoord - 1)
		iXLastPos = 0;

	return 1;
}
