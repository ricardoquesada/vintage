#ifndef __TOKEN_H
#define __TOKEN_H

#define TOKEN_NILL		"nop"
#define TOKEN_EXIT		"exit"
#define TOKEN_FORK		"fork"
#define TOKEN_READ		"read"
#define TOKEN_WRITE		"write"
#define TOKEN_OPEN		"open"
#define TOKEN_CLOSE		"close"
#define TOKEN_WAIT		"wait"
#define TOKEN_BURST		"burst"
#define TOKEN_ADD		"add"
#define TOKEN_MUL		"mul"
#define TOKEN_IF		"if"
#define TOKEN_JUMP		"jump"

STATUS tokenize_to_mem( PPCB pPcb, FILE *fp );
STATUS run_proc( PPCB pPcb );

#endif /* __TOKEN_H */
