/*
 * Trabajo Practico Numero 2. Scheduling.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file sched.h
 * Contiene la deficion de tipos y estructuras usadas en sched.c
 */
#ifndef __SCHED_H
#define __SCHED_H

#include "tipos.h"

#define NRQ		32	/* 32 colas de prioridad (sizeof(long)) */
#define NRCPU		4	/* Hay 4 CPUs */
#define TIME_SLICE	4	/* cada proceso corre 4 ticks antes dejar la CPU */

extern unsigned long jiffies;
extern LIST_ENTRY g_proc_list;

typedef unsigned long PID_T;

typedef enum {
	/* Los 5 estados reales de 4.4BSD */
	PROC_IDL,			/**< Process being created by fork. */
	PROC_RUN,			/**< Currently runnable. */
	PROC_SLEEP,			/**< Sleeping on an address. */
	PROC_STOP,			/**< Process debugging or suspension. */
	PROC_ZOMB,			/**< Awaiting collection by parent. */

	/* estados agregados para este tp */
	PROC_TERMINADO,			/**< proceso ya terminado */

	PROC_LAST
} PROC, *PPROC;


/**
 * \typedef typedef struct _pcb
 * Contiene los datos de un proceso
 */
typedef struct _pcb {
/* IMPORTANTE!!! No modificar el orden de estas 2 1ros entries!!! */
	LIST_ENTRY p_queue;		/**< cola en la que esta */
	LIST_ENTRY p_next;		/**< siguiente proceso */
/* agregar a partir de aca, en caso de ser necesario */

	PID_T p_pid;			/**< identificador del proceso */
	char p_nombre[100];		/**< nombre de proceso */

	PROC p_estado;			/**< estado actual del proceso */

	int p_priority;			/**< prioridad del momento */
	int p_basepriority;		/**< prioridad base USER,SWAP,SOCKET,etc*/
	int p_politica;			/**< politica SCHED_RR, SCHED_FIFO */
	int p_rtpriority;		/**< realtime priority (for future use)*/
	int p_nice;			/**< Process "nice" value. */
	int p_cpu;			/**< CPU en la que prefiere corren */

	unsigned long p_t_run_start;	/**< tiempo en que se empezo a ejecutar (cuando se encolo) */
	unsigned long p_t_run_end;	/**< tiempo en que se termino el prg */

	unsigned long p_cpu_usada;	/**<  tiempo de CPU consumido */

	/* propio de la CPU */
	unsigned long	p_eax;		/**< registro general eax */
	unsigned long	p_ebx;		/**< registro general ebx */
	unsigned long	p_ip;		/**< instruccion pointer */
	unsigned int	p_boolean;	/**< estado de ultima evaluacion */

	/* Segmentos y sus tamanios */
	char *p_codeseg;		/**< segmento de codigo */
	char *p_dataseg;		/**< segmento de datos */
	char *p_stackseg;		/**< segmento de pila */
	unsigned long	p_memcode;	/**< Size necesario para el 'codigo' */
	unsigned long	p_memdata;	/**< Size necesario para la 'data' */
	unsigned long	p_memstack;	/**< Size necesario para el 'stack' */

	/* relacionado cuando esta es 'sleep' */
	int p_queue_tot;		/**< Total de unidades que espera en 'SLEEP' */
} PCB, *PPCB;

/**
 * \typedef struct _cpu
 * Contiene los datos de cada CPU
 */
typedef struct _cpu {
	int c_estado;			/**< estado de la cpu */
	int c_uso;			/**< cantidad de tick consumidos por la CPU */
	PPCB c_proceso;			/**< ptr al proceso que esta corriendo */
	LIST_ENTRY qRun;		/**< procesos destinados para este procesador */
	int qRunCant;			/**< cantidad de procesos encolados */
} CPU, *PCPU;
extern CPU cpu[NRCPU];

/**
 * \enum Estados de una CPU
 */
enum {
	CPU_IDLE,			/**< sin proceso */
	CPU_RUNNING,			/**< con proceso siendo ejecutado */
};

/**
 * \enum Tipos de politicas
 */
enum {
	SCHED_RR=0,			/**< round robin */
	SCHED_FIFO=1,			/**< first in, first out */
	SCHED_OTHER=2,			/**< (for future use) */
};


/* tipos de prioridad base */
#define PSWP	0
#define PVM	4
#define PINOD	8
#define PRIBIO	16
#define PVFS	20
#define PZERO	22
#define PSOCK	24
#define PWAIT	32
#define PLOCK	36
#define PPAUSE	40
#define PUSER	50



/* prototipos */
STATUS sched_init();
void run(void);
PPCB queue_proc( PPCB pPcb_copy );
PPCB next_proc( int i );
void sched_cpu( void );
PPCB proclist2pcb( PLIST_ENTRY pList );
void resched( PPCB pPcb );

#endif /* __SCHED_H */
