/*
 * Trabajo Practico Numero 3. Multiprocessing
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file net.h
 * Archivo que contiene las declaraciones de 'net'
 * \author Ricardo Quesada
 */

#ifndef __NET_H
#define __NET_H

#include <sys/types.h>

ssize_t writen(int fd, const void *vptr, size_t n );
int net_connect_unix(char *path );
int net_connect_tcp(char *host, int port );
ssize_t net_readline(int sock, void *gs,size_t maxlen );
int net_printf(int sock, char *format, ...);

#endif /* __NET_H */
