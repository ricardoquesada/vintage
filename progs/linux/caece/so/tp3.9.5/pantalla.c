/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <curses.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cons.h"
#include "tipos.h"
#include "metric.h"
#include "archivo.h"
#include "adm_comms.h"
#include "adm_graph.h"
#include "adm_common.h"

/**
 * \file pantalla.c
 * Manejo de pantalla
 */

/**
 * Info sobre el programa, el grupo (autores) y cualquier otra yerba \b ACA.
 */

#define PANT_FILAS	20			/**< El ancho del pGPad (ver TSPant). */
#define PANT_COLUMNAS	60	/**< El alto del pGPad (ver TSPant). */
#define PANT_CONSOLE_BUFFER 80 /**< Tama�o del buffer de entrada para la
	consola.*/

static void pant_init_screen( TSPant *pTspant );
static void pant_comms_init( TSPant *pTspant );

/**
 * Inicia las rutinas del programa.
 * Actualmente esto es:
 *	- Incializar el ncurses.
 *	- Inicializar la estructura de comandos y agregar los mismos.
 *	- Inicializar la cola de procesos.
 *	- Inicializar la lista de estados.
 *	- Inicializar la lista de m�tricas.
 *	- Dibujar la pantalla (inicializando las estructuras internas).
 *
 * El \c ncurses est� siendo utilizado para la representaci�n de los datos de la
 * cola en pantalla, los gr�ficos y las m�tricas; tambi�n se lo utiliza para
 * el manejo del teclado.
 * @param pTspant La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
void pantalla_init( TSPant *pTspant )
{
	assert( pTspant != NULL );

	initscr();
	start_color();

	init_pair( PANT_GREEN, COLOR_GREEN, COLOR_BLACK );
	init_pair( PANT_RED, COLOR_RED, COLOR_BLACK );
	init_pair( PANT_CYAN, COLOR_CYAN, COLOR_BLACK );
	init_pair( PANT_WHITE, COLOR_WHITE, COLOR_BLACK );

	cbreak();
	noecho();

	pTspant->iClock = 0;

	pant_init_screen( pTspant );
	pant_comms_init( pTspant );

	pTspant->pTMetric = metric_get( 0 );	/* Metrica 'default' */
	pTspant->ipGraph = adm_graph_metric;
}


/**
 * Inicia la consola.
 * Incicia las estructuras de datos pertinentes y agrega los comandos
 * definidos.
 * @param pTspant La estructura con las propiedades del programa, no puede ser
 * \c NULL.
 * \author Picorelli Marcelo.
 */
static void pant_comms_init( TSPant *pTspant )
{
	assert( pTspant != NULL );

	pTspant->pTCommands = cons_new();
	assert( pTspant->pTCommands != NULL );

	adm_comms_add_help( pTspant, pTspant->pGConsole );

	cons_command_add( &pTspant->pTCommands, "clear", "Limpia la pantalla",
		adm_comms_clear, pTspant->pGPad );

	cons_command_add( &pTspant->pTCommands, "reset",
		"Pone en cero al clock interno.", adm_comms_clock_reset, pTspant );

	cons_command_add( &pTspant->pTCommands, "speed",
		"speed [vel] - Modifica la velocidad del clock", adm_comms_clock_set_speed,
		pTspant );

	cons_command_add( &pTspant->pTCommands, "ps",
		"Muestra el estado actual de todos los procesos", adm_comms_ps, pTspant );

	cons_command_add( &pTspant->pTCommands, "list",
		"Muestra la lista de comandos disponibles", adm_comms_list, pTspant );

	cons_command_add( &pTspant->pTCommands, "graph",
		"Cambia la m�trica que se desea ver", adm_comms_graph, pTspant );

}

/**
 * Termina el programa.
 * Actualmente esto es:
 *	- Eliminar la estructura de comandos.
 *	- Terminar el ncurses.
 *	- Eliminar las estructuras creadas y liberar la memoria.
 */
void pantalla_end( TSPant *pTspant )
{
	assert( pTspant != NULL );

	nocbreak();
	cons_destroy( pTspant->pTCommands );

	delwin( pTspant->pGPad );
	delwin( pTspant->pGMetrics );
	delwin( pTspant->pGConsole );
	endwin();
}

/**
 * Dibuja la pantalla inicial.
 * Crea las estructuras necesarias, como ser las diversas ventanas.
 */
static void pant_init_screen( TSPant *pTspant )
{
	assert( pTspant != NULL );

	/* Algunas l�neas para mejorar la apariencia */
	mvhline( 0, 1, ACS_HLINE, COLS - 2 );
	mvhline( LINES - 1, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, 0, ACS_VLINE, LINES - 2 );
	mvvline( 1, COLS - 1, ACS_VLINE, LINES - 2 );

	mvhline( PANT_FILAS, 1, ACS_HLINE, COLS - 2 );
	mvvline( 1, PANT_COLUMNAS - 1, ACS_VLINE, PANT_FILAS - 1 );

	/* Ahora las esquinas e intersecciones */
	mvaddch( 0, 0, ACS_ULCORNER );
	mvaddch( 0, COLS - 1, ACS_URCORNER );
	mvaddch( LINES - 1, 0, ACS_LLCORNER );
	mvaddch( LINES - 1, COLS - 1, ACS_LRCORNER );

	mvaddch( PANT_FILAS, PANT_COLUMNAS - 1, ACS_BTEE );
	mvaddch( 0, PANT_COLUMNAS - 1, ACS_TTEE );
	mvaddch( PANT_FILAS, 0, ACS_LTEE );
	mvaddch( PANT_FILAS, COLS - 1, ACS_RTEE );
	refresh();

	/* Graficos */
	pTspant->pGPad = newwin( PANT_FILAS - 1, PANT_COLUMNAS - 2, 1, 1 );
	scrollok( pTspant->pGPad, TRUE );
	wnoutrefresh( pTspant->pGPad );

	/* Metricas */
	pTspant->pGMetrics = newwin( PANT_FILAS - 1, COLS -
		PANT_COLUMNAS - 2, 1, PANT_COLUMNAS );
	scrollok( pTspant->pGMetrics, TRUE );
	wnoutrefresh( pTspant->pGMetrics );

	/* Consola */
	pTspant->pGConsole = newwin( LINES - PANT_FILAS - 2, COLS - 2,
		PANT_FILAS + 1, 1 );
	scrollok( pTspant->pGConsole, TRUE );
	keypad( pTspant->pGConsole, TRUE );
	nodelay( pTspant->pGConsole, TRUE );
	wnoutrefresh( pTspant->pGConsole );

	doupdate();
}

/**
 * Muestra el estado actual de los procesos.
 * \author Picorelli Marcelo.
 */
void pantalla_status_refresh( TSPant *pTspant )
{
	PLIST_ENTRY pList = g_proc_list.next;
	PPCB pPcb;
	int ivEstado[PROC_LAST];
	int i;

	assert( pTspant != NULL );

	mvwprintw( pTspant->pGMetrics, 0, 0, "Clock: %d", jiffies );
	wclrtoeol( pTspant->pGMetrics );


	memset( &ivEstado, 0, sizeof( ivEstado ));

	while( !IsListEmpty( &g_proc_list) && ( pList != &g_proc_list ) ) {

		pPcb = proclist2pcb( pList );

		ivEstado[ pPcb->p_estado ]++;
		pList = LIST_NEXT( pList );
	}

	mvwprintw( pTspant->pGMetrics, 3, 0, "Parados  : %d", ivEstado[ PROC_STOP ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics, 4, 0, "Dormidos  : %d", ivEstado[ PROC_SLEEP ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics, 5, 0, "Ejecutando : %d", ivEstado[ PROC_RUN ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics, 6, 0, "Terminados : %d", ivEstado[ PROC_TERMINADO ] );
	wclrtoeol( pTspant->pGMetrics );

	mvwprintw( pTspant->pGMetrics, 8, 0, "CPUS");
	wclrtoeol( pTspant->pGMetrics );

	for(i=0;i<NRCPU;i++) {
		mvwprintw( pTspant->pGMetrics, 9+i, 0, "%i:%s",i,
				(cpu[i].c_estado==CPU_RUNNING ? cpu[i].c_proceso->p_nombre : "IDLE" )
				);
		wclrtoeol( pTspant->pGMetrics );
	}

	wrefresh( pTspant->pGMetrics );

}

int pantalla_console( TSPant *pTspant )
{
	char cpBuffer[ PANT_CONSOLE_BUFFER ];
	char tokens[2][80];
	char *cpTemp;

	assert( pTspant != NULL );

	echo();
	nodelay( pTspant->pGConsole, FALSE );
	waddch( pTspant->pGConsole, '$' );
	wgetnstr( pTspant->pGConsole, cpBuffer, PANT_CONSOLE_BUFFER );
	if( strlen( cpBuffer ) )
	{
		/* Leo la linea de comando, por ahora a lo sumo 2 palabras.*/
		if( ( cpTemp = strtok( cpBuffer, " " ) ) )
		{
			strcpy( tokens[0], cpTemp );
			if( ( cpTemp = strtok( NULL, " " ) ) )
				strcpy( tokens[1], cpTemp );
			else
				tokens[1][0] = 0;

			cons_command_execute( pTspant->pTCommands, tokens[0],
				tokens[1] );
		}
	}

	noecho();
	nodelay( pTspant->pGConsole, TRUE );

	return 1;
}
