/*
 * Grupo MRM
 * TP3
 */
/**
 * \file events.c
 * Encarga de manejar los eventos (internos o externos) que puedan
 * ocurrir, como I/O a dispositivos y esas cosas.
 * Cada Dispositivo cuanto con 2 colas. Una para lectura y otra para
 * escritura. Son colas FIFO, y cada cola tiene su propio tiempo de
 * acceso (Depende del dispositivo y si es 'read' o 'write'
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#ifdef HAVE_UNIX
#include <unistd.h>
#endif

#include "sched.h"
#include "events.h"
#include "tipos.h"
#include "interfaz.h"

EVENTOS eventos[NREVENTS];

/**
 * \fn void wait_event( PPCB pPcb, int event, int tot )
 * \brief Funcion que pone un proceso en SLEEP esperando un evento
 * \param pPcb proceso que se pone a 'sleep'
 * \param event Evento que espera que ocurra
 * \param tot Cantidad de unidades o eventos que espera que ocurra
 * \author Ricardo Quesada
 */
void wait_event( PPCB pPcb, int event, int tot )
{
	int i;

	assert( pPcb );

	i = event % NREVENTS;		/* safe */

	console_out("Durmiendo proceso '%s' en cola %d (tiempo de acceso %d)\n",pPcb->p_nombre,event,eventos[event].e_tiempo);

	pPcb->p_queue_tot = tot;

	pPcb->p_estado = PROC_SLEEP;
	InsertTailList( &eventos[i].e_qSleep, (PLIST_ENTRY) pPcb );
	eventos[i].e_qSleepCant++;
}

/**
 * \fn void generate_event( int event )
 * \brief Genera un evento, despertando al 1ero de la lista
 * \param evento Evento que se generara
 * \author Ricardo Quesada
 */
void generate_event( int event )
{
	int i;
	PPCB pPcb; 

	i = event % NREVENTS;		/* safe */
	pPcb = (PPCB) eventos[i].e_qSleep.next;

	if( eventos[i].e_qSleepCant ) {

		if( !pPcb->p_queue_tot-- ) {

			pPcb = (PPCB) RemoveHeadList( &eventos[i].e_qSleep );
			eventos[i].e_qSleepCant--;

			console_out("Despertando proceso: %s\n",pPcb->p_nombre);

			/* Esto no se si es lo mas 'justo', pero esta 'bien' */
			resched( pPcb );
		}
	}
}

/**
 * \fn void procesar_events()
 * \brief Recorre las colas de eventos y hace otras cosas
 * \author Ricardo Quesada
 */
void procesar_events()
{
	int i;

#if 0
	/* Genera un evento al azar una vez por ciclo */
	i = (int) (((float)NREVENTS)*rand()/(RAND_MAX+1.0));
	generate_event( i );
#endif

	/* Recorre la lista de eventos, y si hay uno listo lo dispara */
	for(i=0;i<NREVENTS;i++) {
		if( eventos[i].e_qSleepCant ) {
			if( jiffies%eventos[i].e_tiempo == 0 ) {
				generate_event( i );
			}
		}
	}
}
/**
 * \fn void init_events()
 * \brief Inicializa todo lo relacionado con los eventos
 * \author Ricardo Quesada
 */
void init_events()
{
	int i;

	/*
	 * Colas pares (read)
	 * 	entre 0 y 10 
	 * Colas impares (write)
	 * 	entre 15 y 25
	 */
	for(i=0;i<NREVENTS;i++) {
		InitializeListHead( &eventos[i].e_qSleep );
		eventos[i].e_qSleepCant=0;
		eventos[i].e_tiempo=  (i%2 == 0 ? 0 : 15 ) + (int) ((10.0)*rand()/(RAND_MAX+1.0));
	}
}
