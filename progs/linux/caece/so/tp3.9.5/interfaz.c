/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file interfaz.c
 * Funciones que tienen que ver con la interfaz
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdarg.h>

#include "tipos.h"
#include "sched.h"
#include "metric.h"
#include "interfaz.h"
#include "adm_common.h"

#ifdef WITH_NCURSES
static TSPant pantalla;
#endif /* WITH_NCURSES */

/**
 * \fn STATUS interfaz_gfx( PMETRIC pM )
 * \brief Muestra el estado de cada cola
 */
STATUS interfaz_gfx( PMETRIC pM )
{
#ifdef DEBUG_INTERFAZ
	printf("nombre:%s\n",pM->nombre);
	printf("min:%d\n",pM->min);
	printf("max:%d\n",pM->max);
	printf("promedio:%d\n",pM->prom);
	printf("current:%d\n",pM->cur);
#endif
#ifdef WITH_NCURSES
	pantalla.ipGraph( &pantalla, pM );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS interfaz_init()
 * \brief Inicializa la interfaz
 */
STATUS interfaz_init()
{
#ifdef WITH_NCURSES
	pantalla_init( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_ERROR;
}

/**
 * \fn STATUS interfaz_deinit()
 * \brief Desinicializa la interfaz
 */
STATUS interfaz_deinit()
{
#ifdef WITH_NCURSES
	pantalla_end( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS interfaz_refresh();
 * \brief Refresca la pantalla
 */
STATUS interfaz_refresh()
{
#ifdef WITH_NCURSES
	pantalla_status_refresh( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_out();
 * \brief Saca por consola un string
 */
STATUS console_out( char *str, ...)
{
        va_list args;
	char buf[2000];

	va_start(args, str);
	vsprintf(buf, str, args);
	va_end(args);

#ifdef WITH_NCURSES
	wprintw( pantalla.pGConsole, "%s", buf );
	wrefresh( pantalla.pGConsole );
#else
	printf( buf );
#endif /* WITH_NCURSES */

	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_input();
 * \brief Espera una tecla por consola
 */
STATUS console_input()
{
	int iKey;

#ifdef WITH_NCURSES
	if( ( ( iKey = wgetch( pantalla.pGConsole )  ) != ERR ) && ( iKey == KEY_IC ) )	{
		pantalla_console( &pantalla );
	}
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}
