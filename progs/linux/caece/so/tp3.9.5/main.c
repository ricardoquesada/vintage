/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file main.c
 * Archivo que tiene el main loop y la funcion main.
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "metric.h"
#include "interfaz.h"
#include "load.h"
#include "events.h"
#include "mem.h"

#define INITIAL_MEMORY	10240	/* 10K */	
int speed = 200000;

/**
 * \fn void Sleep( int usec )
 * \brief Un 'sleep' mas fino y portable
 * \param microsegundos a esperar
 * \author Ricardo Quesada
 */
void Sleep( int usec )
{
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = usec;
	select(0,NULL,NULL,NULL,&tv);
}

/**
 * \fn STATUS main_loop(void)
 * \brief Main loop (que mas)
 * \return STATUS
 * \author Ricardo Quesada
 */
STATUS main_loop(void)
{ 
	PCB pcb;

	while(1) {
#ifdef DEBUG
		printf("--> ciclo %d <--\n",(unsigned)jiffies);
#endif
		console_input();

		memset( &pcb, 0, sizeof(PCB));
		while( archivo_read( &pcb ) == STATUS_SUCCESS ) {
			if( load( &pcb ) == STATUS_SUCCESS )
				queue_proc( &pcb );
		}

		run();				/* scheduling */

		procesar_events();		/* I/O */

		metric_display_all();		/* mostrar en pantalla */

		Sleep(speed);
	}
}

/**
 * \fn help
 * \brief Muestra el modo de uso
 */
void help()
{
	printf("Modo de uso:\n");
	printf("\t\ttp3 [archivo_de_conf]\n");
	printf("\t\tSi se omite el archivo_de_conf, buscara el archivo 'data.txt'\n");
}

/**
 * \fn int main( void )
 * \brief su nombre lo dice todo
 */
int main( int argc, char **argv )
{
	printf("\n\nTP3 - Sistemas Operativos - Grupo MRM\n");
	if( argc == 1 ) {
		if( archivo_init("data.txt") != STATUS_SUCCESS ) {
			printf("Error al abrir el archivo 'data.txt'\n");
			exit(1);
		}
	} else if (argc == 2 ) {
		if( archivo_init(argv[1]) != STATUS_SUCCESS ) {
			printf("Error al abrir el archivo '%s'\n",argv[1]);
			exit(1);
		}
	} else
		help(),exit(1);

	if( mem_init( INITIAL_MEMORY ) == STATUS_NOMEM )  
                return STATUS_NOMEM;

	sched_init();
	init_events();
	metric_init();
	interfaz_init();

	main_loop();

	interfaz_deinit();
	archivo_close();
	metric_flush();
	mem_deinit();

	return 0;
}
