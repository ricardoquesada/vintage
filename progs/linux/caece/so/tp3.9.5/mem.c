/*
 * Trabajo Practico Numero 1.
 * Sistemas Operativos.
 * Grupo MRM
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "mem.h"

static int int_mem_search_chunk( int iMemNeeded, int *iFirstBlock, int *Frags);
static void int_mem_assign( PPCB pPcb, int iBeginBlock );
static void int_mem_defrag( void );

static char *cpMemPool = NULL;/**< Puntero a la Cantidad de memoria disponible*/
static int   iMemPoolSize;		/**< Cantidad de memoria total */
static int	 iMemPoolFree;		/**< Cantidad de memoria disponible */

/**
 * Ubica un segmento contiguo de memoria.
 * Intenta ubicar la memoria, en caso de no conseguirla, devuelve error.
 * @param iMemNeeded Cuanta memoria se necesita.
 * @param iFirstBlock Un puntero en el cual almacenar el comienzo del bloque
 * de memoria que satisfaga el requerimiento.
 * @param iFrags Un puntero en el cual almacenar la cantidad de memoria
 * existente en forma fragmentada.
 */
static int int_mem_search_chunk(int iMemNeeded, int *iFirstBlock, int *iFrags )
{
	int iMemFragments = 0;	/* Cuento la cantidad de memoria disponible en
										fragmentos que no son del tama�o necesario */
	int iBeginBlock = 0;
	int i = 0;
	int j = 0;

	assert( iFirstBlock != NULL );
	assert( iFrags != NULL );

	/* Busco un lugar suficientemente grande para colocar el bloque de mem. */
	/* En este momento es del tipo First Fit */
	while( i < iMemPoolSize )
	{
		/* Si estoy en un bloque ocupado, salteo hasta llegar a uno libre */
		/* Creo que mejor es hacer una lista con los espacios disponibles */
		while( (char)cpMemPool[ i ] != -1 && i < iMemPoolSize )
			i++;

		iBeginBlock = i; 
		/* Ahora cuento la cantidad de bloques libres */
		while( (char)cpMemPool[i] == -1 && i < iMemPoolSize && j < iMemNeeded )
		{
			i++; j++;
		}

		/* Si la cantidad de bloques libres es la que necesito...*/
		if( j == iMemNeeded )
		{
			*iFirstBlock = iBeginBlock;
			*iFrags = iMemFragments;	
			return STATUS_SUCCESS;
		}
		else
		{
			iMemFragments += j;	/* Incremento la cuenta de mem. libre */
			j = 0;
		}
	}

	*iFrags = iMemFragments;	
	return STATUS_NOMEM;
}

/**
 * Asigna un bloque de memoria.
 * @param pPcb El PCB del proceso al que se le asignara la memoria.
 * @param iBeginBlock El comienzo del bloque de memoria.
 * \author Picorelli Marcelo.
 */
static void int_mem_assign( PPCB pPcb, int iBeginBlock )
{
	int iMemNeeded = pPcb->p_memdata + pPcb->p_memcode + pPcb->p_memstack;

	pPcb->p_dataseg = cpMemPool + iBeginBlock; 
	pPcb->p_codeseg = cpMemPool + iBeginBlock + pPcb->p_memdata; 
	pPcb->p_stackseg = cpMemPool + iBeginBlock + pPcb->p_memdata +
		pPcb->p_memcode; 

	iMemPoolFree -= iMemNeeded;
	memset( cpMemPool + iBeginBlock, 0, iMemNeeded );

	//PDEBUG( "get_mem(): free mem : %d\n", iMemPoolFree );
}
/**
 * Defragmenta la memoria.
 * \author Picorelli Marcelo.
 */
static void int_mem_defrag( void )
{
	int i = 0;
	int iFirstFree = 0;
	int iBegin = 0;

/********
		{
			int k;
			fprintf( stderr, "PRE compacted\n" );
			for( k = 0; k < iMemPoolSize; k++ )
			{
				fprintf( stderr, "%c", cpMemPool[k] == -1 ? '0':'X' );
			}
			fprintf( stderr, "\n" );
		}
*********/

	while( i < iMemPoolSize )
	{
		/* Si estoy en un bloque ocupado, salteo hasta llegar a uno libre */
		while( (char)cpMemPool[ i ] != -1 && i < iMemPoolSize ) 
			i++;

		iFirstFree = i;
		while( (char)cpMemPool[ i ] == -1 && i < iMemPoolSize )
			i++;

		if( i != iMemPoolSize )	/* Ya no hay nada hacia el fondo */
		{//		return;	
			/* Muevo al primer lugar libre el bloque de memoria */
			memmove( &cpMemPool[ iFirstFree ], &cpMemPool[ i ], 
				iMemPoolSize - i );	/* TODO: Arreglar esto!!! */
			memset( &cpMemPool[ iMemPoolSize - (i - iFirstFree) ], -1, 
				i - iFirstFree );
		}
	}
/***********
		{
			int k;
			fprintf( stderr, "POST compacted\n" );
			for( k = 0; k < iMemPoolSize; k++ )
			{
				fprintf( stderr, "%c", cpMemPool[k] == -1 ? '0':'X' );
			}
			fprintf( stderr, "\n" );
		}
**************/
}

/**
 * Inicializa la memoria interna.
 * @param iMemNeeded La cantidad de memoria inicial que se va a asignar.
 * @returns STATUS_SUCCESS si hay memoria, o STATUS_NOMEM en caso contrario.
 * \author Picorelli Marcelo.
 * 
 */
STATUS mem_init( int iMemNeeded )
{
	assert( iMemNeeded >= 0 );
	assert( cpMemPool == NULL ); /* Si != NULL, ya fuimos invocados...*/

	if( ( cpMemPool = (char *)malloc( sizeof( char ) * iMemNeeded ) ) )
	{
		iMemPoolSize = iMemPoolFree = iMemNeeded;
		memset( cpMemPool, -1, iMemNeeded );

		return STATUS_SUCCESS;
	}

	return STATUS_NOMEM;
}

/**
 * Libera la memoria utilizada.
 * \author Picorelli Marcelo.
 */
void mem_deinit( void )
{
	assert( cpMemPool != NULL );

	iMemPoolSize = iMemPoolFree = 0;
	free( cpMemPool );
	cpMemPool = 0;
}

/**
 * Ubica memoria para el proceso que la requiere.
 * Intenta ubicar la memoria, en caso de no conseguirla, mira si una 
 * defragmentacion seria util, de ser asi defragmenta y devuelve la memoria.
 * @param pPcb Un puntero al PCB del proceso que requiere la memoria.
 * @returns STATUS_SUCCESS si logra encontrar lugar, STATUS_NOMEM en caso
 *	contrario.
 * \author Picorelli Marcelo.
 */
STATUS get_mem( PPCB pPcb )
{
	int iMemNeeded = pPcb->p_memdata + pPcb->p_memcode + pPcb->p_memstack;
	int iMemFragments;
	int iBeginBlock;

	assert( pPcb != NULL );
	//PDEBUG( "get_mem(): Needed (%d)\n", iMemNeeded );

	/* Bueno, vemos si obtuve la memoria */
	if( int_mem_search_chunk( iMemNeeded, &iBeginBlock, &iMemFragments ) == 
		STATUS_SUCCESS )
	{	
		int_mem_assign( pPcb, iBeginBlock );
/*********
		{
			int k;
			fprintf( stderr, "malloc (%d)\n", iMemNeeded );
			for( k = 0; k < iMemPoolSize; k++ )
			{
				fprintf( stderr, "%c", cpMemPool[k] == -1 ? '0':'X' );
			}
			fprintf( stderr, "\n" );
		}
*********/
		return STATUS_SUCCESS;
	}
	else
	{
		/* NO funciona bien, asi que mejor lo comento y luego lo arreglo 
		if( iMemFragments >= iMemNeeded )
		{
			int_mem_defrag();
			int_mem_search_chunk( iMemNeeded, &iBeginBlock, &iMemFragments ); 
			int_mem_assign( pPcb, iBeginBlock );
			
			return STATUS_SUCCESS;
		}
		*/
			
		//PDEBUG( "get_mem(): Mem.Left (%d)\n", iMemFragments );
		return STATUS_NOMEM;
	}
}

/**
 * Libera la memoria utilizada.
 * Cuando menos por ahora, me valgo del PCB para saber cual es la region de
 * memoria ocupada, al igual que su longitud, no se si debiera crearme mi
 * propia estructura, pero tendriamos dos estructuras con la misma info, en fin
 * preguntar o averiguar.
 * @param El puntero al PCB.
 * \author Picorelli Marcelo.
 */
STATUS free_mem( PPCB pPcb )
{
	char *iBeginBlock = pPcb->p_dataseg;
	int iBlockSize = pPcb->p_memdata + pPcb->p_memcode + pPcb->p_memstack;
	assert( pPcb != NULL );

	/* Averiguo cual es el comienzo del bloque */
	if( pPcb->p_codeseg < iBeginBlock )
		iBeginBlock = pPcb->p_codeseg;
	else
	if( pPcb->p_stackseg < iBeginBlock )
		iBeginBlock = pPcb->p_stackseg;

	assert( iBeginBlock >= cpMemPool );
	assert( iBeginBlock + iBlockSize <= iBeginBlock + iMemPoolSize );
	memset( iBeginBlock, -1, iBlockSize );
/**********
	{
		int k;
		fprintf( stderr, "freed(%d)\n", iBlockSize );
		for( k = 0; k < iMemPoolSize; k++ )
		{
			fprintf( stderr, "%c", cpMemPool[k] == -1 ? '0':'X' );
		}
		fprintf( stderr, "\n" );
	}
************/
	iMemPoolFree += pPcb->p_memdata + pPcb->p_memcode + pPcb->p_memstack;
	//PDEBUG( "get_mem(): free mem : %d\n", iMemPoolFree );
	
	return STATUS_SUCCESS;
}
