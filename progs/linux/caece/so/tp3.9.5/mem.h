#ifndef __MEM_H
#define __MEM_H


/**/
void mem_deinit( void );
STATUS mem_init( int iMemNeeded );
/**/
STATUS get_mem( PPCB pPcb );
STATUS free_mem( PPCB pPcb );

#endif /* __MEM_H */
