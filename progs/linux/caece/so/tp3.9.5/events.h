/**
 * \file events.h
 * Declaraciones y esas cosas para el events.h
 */
#ifndef __EVENTS_H
#define __EVENTS_H

#define NREVENTS	10	/* posibles eventos que pueden ocurrir */

typedef struct _eventos {
	LIST_ENTRY e_qSleep;		/**< Array de lista de eventos */
	int e_qSleepCant;		/**< Cuantos hay encolados */
	int e_tiempo;			/**< tiempo de acceso que tiene esta cola */
} EVENTOS, *PEVENTOS;

void wait_event( PPCB pPcb, int event, int tot );
void procesar_events();
void generate_event( int event );
void init_events();


#endif /* __EVENTS_H */
