!
! Problema del barbero 
! Grupo MRM
!
! Modulo: Cashier
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	20	0	0
.sem	MAX_CAPACITY	20
.sem	SOFA		4
.sem	COORD		3
.sem	BARBER_CHAIR	3
.sem	CUST_READY	0
.sem	FINISHED	0
.sem	LEAVE_B_CHAIR	0
.sem	PAYMENT		0
.sem	RECEIPT		0


	wait	PAYMENT
	wait	COORD

!	Accept Pay
	burst	12

	signal	COORD
	signal	RECEIPT

!	Forever
	jump	-10

	exit	0
.end
