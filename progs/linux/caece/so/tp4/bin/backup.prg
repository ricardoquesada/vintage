! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	135	0	0

	burst	100
	read	4	1
	burst	10
	read	4	2
	burst	20
	read	4	10
	burst	10

	lda	8
	burst	11
	read	1	0
	deca
	jnz	-6

	lda	7
	write	4	3
	burst	37
	write	4	1
	read	4	1
	deca
	jnz 	-12

	exit	0
.end
