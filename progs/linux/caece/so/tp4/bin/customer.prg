!
! Problema del barbero 
! Grupo MRM
!
! Modulo: Customer
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	50	0	0
.sem	MAX_CAPACITY	20
.sem	SOFA		4
.sem	COORD		3
.sem	BARBER_CHAIR	3
.sem	CUST_READY	0
.sem	FINISHED	0
.sem	LEAVE_B_CHAIR	0
.sem	PAYMENT		0
.sem	RECEIPT		0


	wait	MAX_CAPACITY

! Enter shop
	burst	5

	wait	SOFA

! Sit on sofa
	burst	3

	wait	BARBER_CHAIR
	
! Get up from sofa
	burst	2

	signal	SOFA

! Sit in barber chair
	burst	2

	signal	CUST_READY
	wait	FINISHED

	signal	COORD
	signal	RECEIPT

! leave barber chair
	burst 4

	signal	LEAVE_B_CHAIR

! pay
	burst 8

	signal PAYMENT

	wait	RECEIPT

! exit shop
	burst	6

	signal	MAX_CAPACITY

	exit	0
.end
