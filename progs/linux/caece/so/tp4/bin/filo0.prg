!
! filosofo 0
! Primer acercamiento al problema de los filosofos, Stallings.
! Produce Deadlock, es del tipo 'Zurdo'.
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	100	0	0
.sem	SEM_0	1
.sem	SEM_1	1

	lda	15

	wait	SEM_0
	wait	SEM_1

	burst	100

	signal	SEM_1
	signal	SEM_0

	burst	20

	deca
	jnz	-13

	exit	0
.end
