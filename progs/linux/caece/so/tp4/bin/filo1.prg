!
! filosofo 1
! Primer acercamiento al problema de los filosofos, Stallings.
! Produce Deadlock, es del tipo 'Zurdo'.
! 
! .param	code_len	data_len	stack_len
!
.version	1
.param	100	0	0
.sem	SEM_1	1
.sem	SEM_2	1

	lda	15

	wait	SEM_1
	wait	SEM_2

	burst	100

	signal	SEM_2
	signal	SEM_1

	burst	20

	deca
	jnz	-13

	exit	0
.end
