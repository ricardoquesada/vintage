/**
 * \file token.h
 */
#ifndef __TOKEN_H
#define __TOKEN_H

#define TOKEN_NILL		"nop"
#define TOKEN_EXIT		"exit"
#define TOKEN_FORK		"fork"
#define TOKEN_READ		"read"
#define TOKEN_WRITE		"write"
#define TOKEN_OPEN		"open"
#define TOKEN_CLOSE		"close"
#define TOKEN_WAIT		"wait"
#define TOKEN_BURST		"burst"
#define TOKEN_LDA		"lda"
#define TOKEN_JZ		"jz"
#define TOKEN_JNZ		"jnz"
#define TOKEN_INCA		"inca"
#define TOKEN_DECA		"deca"
#define TOKEN_JUMP		"jump"
#define TOKEN_SIGNAL		"signal"
#define TOKEN_KILL		"kill"

STATUS tokenize_to_mem( PPCB pPcb, FILE *fp );
STATUS run_proc( PPCB pPcb );

#endif /* __TOKEN_H */
