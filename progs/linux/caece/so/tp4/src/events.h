/**
 * \file events.h
 * Declaraciones y esas cosas para el events.h
 */
#ifndef __EVENTS_H
#define __EVENTS_H

#define NREVENTS	(5*2)	/* posibles eventos que pueden ocurrir */

void wait_event( PPCB pPcb, int event, int tot );
void procesar_events();
void generate_event( int event );
void init_events();
STATUS evt_free( PPCB pPcb );

#endif /* __EVENTS_H */
