/** \mainpage Introducci�n
 *
 * \section carga Ejecuci�n del programa
 *
 * El programa puede recibir como par�metro el archivo de
 * configuraci�n. Este archivo es el que dice a que hora,
 * con que prioridades y de que modo se deben cargar los 
 * programas. Por default Busca el archivo 'tp.ini'. Si no
 * lo encuentra el programa mostrar� un pantalla de ayuda.
 *
 *
 * \section interfaz Interf�z gr�fica
 *
 * El programa tiene una interf�z gr�fica basada en "ncurses".
 * A trav�z de esta interf�z uno puedo ejecutar comandos, ver
 * distintas m�tricas, ver el estado de los procesos, de la memoria,
 * de los sem�foros, etc.
 *
 * \subsection command Entrada de comandos
 *
 * Para ingresar un comando, uno debe apretar la tecla "INSERT".
 * De esa manera se pausa la ejecuci�n hasta que se ingrese el comando.
 * Para ver la lista de comandos escribir: 'help' (sin las comillas).
 * Estos son los comandos:
 *
 * help:	Muestra ayuda sobre los comandos.
 *
 * clear:	Borra la pantalla.
 *
 * speed:	Cambia la velocidad de la CPU.
 *
 * graph:	Cambia de metrica.
 *
 * mem:		Muestra el estado de la memoria.
 *
 * ps:		Muestra el estado de los procesos.
 *
 * sem:		Muestra el estado de los semaforos.
 *
 * kill:	Mata a un proceso.
 *
 * exit:	Salir.
 *
 *
 * \subsection metricas Cambiando las m�tricas
 *
 * Las m�tricas se pueden cambiar usando el comando 'graph nombre_de_m�trica'
 * � usando los cursores arriba y abajo.
 *
 * 
 * \subsection speed Cambiando la velocidad
 * 
 * La velocidad de ejecuci�n se puede cambiar con el comando 'speed' o con
 * los cursores izquierda y derecha
 *
 * \subsection zoom Cambiando la escala
 *
 * Si uno esta viendo una m�trica y algunos de sus valores no se ven, uno
 * puede hacer ZOOM IN o ZOOM OUT con las teclas 'PAGE UP' y 'PAGE DOWN'.
 *
 * \section sched Scheduler
 
 * El scheduling fue basado (casi en su totalidad) en el que es usado por
 * 4.4BSD. Sin embargo, algunos features como las politicas SCHED_FIFO,SCHED_RR
 * fueron basados en el scheduling del Linux 2.2
 * Basicamente estos son los atributos del proceso (ver 'struct PCB' en
 * 'sched.h') que se usan en el scheduling.
 *
 * int p_priority;
 *
 * int p_basepriority;
 *
 * int p_politica;
 *
 * int p_nice;
 *
 * int p_cpu;
 *
 *
 * Esos valores son asignados por 1ra vez en el archivo 'data.txt'. Luego
 * algunos de esos valores van siendo modificados a travez de la vida del
 * proceso.
 *
 * El motor tiene:
 *
 *    32 colas de prioridades.
 *
 *    4 CPUs.
 *
 *    1 cola adicional por cada CPU.
 *
 *
 * \section dispatch Ejecuci�n de programas
 *
 * Se eligio usar una especie de JIT (Just In Time) compiler.
 *
 * . El proceso se carga (a su debido tiempo)
 *
 * . Se le chequea la sintaxis
 *
 * . Si es correcta, se pide memoria para su almacenamiento.
 *
 * . Si hay mem, se lo compila a bytecode en esa memoria.
 *
 *
 * Entonces cuando se ejecuta el programa, no se interpreta, ya que
 * fue previamente compilado, resultando asi una ejecuci�n m�s r�pida
 * y simple.
 * 
 *
 * Set de instruccion disponibles:
 *
 *    nop         No OPeration
 *
 *    exit        Llamada para terminar el programa
 *
 *    read a b    Lee del dispositivo 'a' 'b' unidades
 *
 *    write a b   Escribe al dispositivo 'a' 'b' unidades
 *
 *    wait  a     Espera el semaforo 'a'
 *
 *    signal a    Signalea el semaforo 'a'
 *
 *    burst a b   Ejecuta 'a' veces algo
 *
 *    lda a       Carga el acumulador con 'a'
 *
 *    jz a        Si el acumulador esta en 0, salta 'a' posiciones
 *
 *    jnz a       Si el acumulador no esta en 0, salta 'a' posiciones
 *
 *    inca        Incrementa el acumulador en 1      
 *    
 *    deca        Decrementa el acumulador en 1
 *
 *    jump a      Salta 'a' posiciones.
 *
 *    kill id     Mata al proceso id.
 *
 *
 *
 * Cabe recalcar que la ejecuci�n es verdadera. Por ejemplo, el siguiente
 * programa:
 *
 *       lda	13       ; ocupa 2 bytes
 *
 *       burst	80       ; ocupa 2 bytes
 *
 *       deca            ; ocupa 1 byte
 *
 *       jnz	-3       ; ocupa 2 bytes
 *
 *
 * hace esto:
 *     Ejecuta 'burst 80' 13 veces.
 *     
 * Pero si en cambio, se ejecutase:
 *
 *       lda	13   
 *
 *       burst	80 
 *
 *       deca 
 *
 *       jnz	-2       ; <-- PUSE '-2' en vez de '-3'
 *
 *
 *  Va a intentar de ejecutar la instruccion 80, luego del 'jnz -2', generando
 *  asi un excepcion del tipo 'Instruccion invalida' terminando el proceso.
 *
 */
