/*
 * Trabajo Practico Numero 4.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "interfaz.h"
#include "load.h"
#include "token.h"
#include "tokens.h"
#include "sem.h"

#undef TOKEN_DEBUG
/**
 * \file token.c
 * pseudo 'JIT' Just In Time compiler.
 * Cuando se carga el programa, se lo compila a 'bytecode', 
 * y eso es lo que se guarda en memoria, y se ejecuta.
 * \author Ricardo Quesada
 */

struct _tokens {
	char *nombre;		/**< nombre del token */
	int params;		/**< parametros que recibe */
	char bytecode;		/**< codigo del bytecode */
	STATUS (*func)();	/**< funcion a llamar */
	STATUS (*pre)();	/**< funcion que hace algo antes de la compilacion */
} tokens[] = {
	/* Para que la ejecucion sea 'optimizada' hacer que el bytecode '0'
	 * este en el indice 0, el '1' en el 1, etc */
	{TOKEN_NILL	,0, 0, token_nill, NULL },
	{TOKEN_EXIT	,1, 1, token_exit, NULL },
	{TOKEN_FORK	,0, 2, token_fork, NULL },
	{TOKEN_READ	,2, 3, token_read, NULL },
	{TOKEN_WRITE	,2, 4, token_write, NULL },
	{TOKEN_OPEN	,1, 5, token_open, NULL },
	{TOKEN_CLOSE	,1, 6, token_close, NULL },
	{TOKEN_WAIT	,1, 7, token_wait, sem_a2i },
	{TOKEN_BURST	,1, 8, token_burst, NULL },
	{TOKEN_LDA	,1, 9, token_lda, NULL },
	{TOKEN_JZ	,1,10, token_jz, NULL },
	{TOKEN_JNZ	,1,11, token_jnz, NULL },
	{TOKEN_INCA	,0,12, token_inca, NULL },
	{TOKEN_DECA	,0,13, token_deca, NULL },
	{TOKEN_JUMP	,1,14, token_jump, NULL },
	{TOKEN_SIGNAL	,1,15, token_signal, sem_a2i },
	{TOKEN_KILL	,1,16, token_kill, NULL },
};
#define	NRTOKENS  (sizeof(tokens)/sizeof(tokens[0]))

/**
 * \fn static STATUS get_specific( FILE *fp )
 * \brief Evalua codigo especifico del interprete
 * \author Ricardo Quesada
 */
static STATUS get_specific( FILE *fp )
{
	int r;
	char c;
	char quesoy[1000];

	PDEBUG("get_specific()");

read_loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	r=fread(  &c, 1 ,1, fp);
	if(r!=1 || c=='\n')
		goto read_loop;

	if( c=='.') {
		fscanf( fp,"%s\n", quesoy );
		if(strcmp( quesoy,"end")==0)
			return STATUS_END;
		else
			return STATUS_ERROR;
	}
	fseek( fp, -1 ,SEEK_CUR);
	return STATUS_SUCCESS;
}

/**
 * \fn static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
 * \brief Convierte un string a un bytecode
 * \author Ricardo Quesada
 */
static STATUS convert_2_bytecode( PPCB pPcb, FILE *fp, int *indice )
{
	char buffer[1000];
	char token[100];
	char param1[100];
	int  iparam1;
	int  iparam2;
	int i;
	int j;

	PDEBUG("convert_2_bytecode()");

	/*
	 * Dado que hay un bug horrible en libc hago esto para evitarlo
	 * 1ro un readline y luego un sscanf en vez de un fscanf
	 */

	memset(buffer,0,sizeof(buffer));
	{
		char c;
		int ii=0;
		
		do {
			fread(  &c, 1 ,1, fp);
			buffer[ii++]=c;
		} while( c !='\n');
	}


	j = sscanf(buffer, "%s %s %d\n",token,param1,&iparam2);

#ifdef TOKEN_DEBUG
	fprintf(stderr,"Bytecompiling '%s' '%d'\n",token,j);
#endif
	for(i = 0; i < NRTOKENS; i++) {
		if(strcmp( token, tokens[i].nombre )==0 ){

			pPcb->p_codeseg[(*indice)++] = tokens[i].bytecode;

			if( tokens[i].params >= 1 )  {
				if( tokens[i].pre )
					tokens[i].pre( param1, &iparam1 );
				else
					iparam1 = atoi( param1 );
				pPcb->p_codeseg[(*indice)++] = (char) iparam1;
			} else return STATUS_SUCCESS;

			if( tokens[i].params >= 2 ) {
				pPcb->p_codeseg[(*indice)++] = (char) iparam2; 
			}

			return STATUS_SUCCESS;
		}
	}
	console_out("Error en %s: Token '%s' no encontrado\n",pPcb->p_nombre,token);
	return STATUS_ERROR;
}

/**
 * \fn STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
 * \brief Funcion que se encarga de convertir a 'bytecodes' un programa
 * \author Ricardo Quesada
 */
STATUS tokenize_to_mem( PPCB pPcb, FILE *fp ) 
{
	int indice=0;

	PDEBUG("tokenize_to_mem()");

loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	/* es un REM */
	if( get_rem( fp ) == STATUS_FILEEOF )
		return STATUS_FILEEOF;

	/* es algun valor del interprete */
	switch( get_specific( fp )) {
		case STATUS_END:
			return STATUS_SUCCESS;
		case STATUS_FILEEOF:
			console_out("Error: EOF\n");
			return STATUS_FILEEOF;
		case STATUS_SUCCESS:
			if( convert_2_bytecode( pPcb, fp, &indice ) != STATUS_SUCCESS )
				return STATUS_ERROR;
			break;
		default:
			break;
	}
	goto loop;
}

/**
 * \fn STATUS run_proc( PPCB pPcb )
 * \brief Hace 'andar' los procesos
 * \author Ricardo Quesada
 */
STATUS run_proc( PPCB pPcb )
{
	char t = pPcb->p_codeseg[pPcb->p_ip];

	pPcb->p_cpu_usada++;

	if( t >= NRTOKENS ) {
		console_out("Instruccion invalida en: %s\n",pPcb->p_nombre);
		return STATUS_INVALID;
	}

	return (tokens[(int)t].func)(pPcb);
}
