/*
 * Trabajo Practico Numero 2.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file archivo.h
 */
#ifndef __ARCHIVO_H
#define __ARCHIVO_H

#include "tipos.h"
#include "sched.h"

STATUS archivo_init(char *nombre);
STATUS archivo_read( PPCB ppcb );
STATUS archivo_close(void);

#endif  /* __ARCHIVO_H */
