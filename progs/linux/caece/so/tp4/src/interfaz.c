/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file interfaz.c
 * Funciones que tienen que ver con la interfaz
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdarg.h>

#include "tipos.h"
#include "sched.h"
#include "metric.h"
#include "interfaz.h"
#include "adm_comms.h"
#include "adm_graph.h"
#include "adm_common.h"

#ifdef WITH_NCURSES
static TSPant pantalla;
#endif /* WITH_NCURSES */

/**
 * \fn STATUS interfaz_gfx( PMETRIC pM )
 * \brief Muestra el estado de cada cola
 */
STATUS interfaz_gfx( PMETRIC pM )
{
#ifdef DEBUG_INTERFAZ
	printf("nombre:%s\n",pM->nombre);
	printf("min:%d\n",pM->min);
	printf("max:%d\n",pM->max);
	printf("promedio:%d\n",pM->prom);
	printf("current:%d\n",pM->cur);
#endif
#ifdef WITH_NCURSES
	pantalla.ipGraph( &pantalla, pM );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS interfaz_init()
 * \brief Inicializa la interfaz
 */
STATUS interfaz_init()
{
#ifdef WITH_NCURSES
	pantalla_init( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_ERROR;
}

/**
 * \fn STATUS interfaz_deinit()
 * \brief Desinicializa la interfaz
 */
STATUS interfaz_deinit()
{
#ifdef WITH_NCURSES
	pantalla_end( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS interfaz_refresh();
 * \brief Refresca la pantalla
 */
STATUS interfaz_refresh()
{
#ifdef WITH_NCURSES
	pantalla_status_refresh( &pantalla );
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_out();
 * \brief Saca por consola un string
 */
STATUS console_out( char *str, ...)
{
        va_list args;
	char buf[2000];

	va_start(args, str);
	vsprintf(buf, str, args);
	va_end(args);

#ifdef WITH_NCURSES
	wprintw( pantalla.pGConsole, "%s", buf );
	wrefresh( pantalla.pGConsole );
#else
	printf( buf );
#endif /* WITH_NCURSES */

	return STATUS_SUCCESS;
}


/**
 * \fn STATUS console_input();
 * \brief Espera una tecla por consola
 */
STATUS console_input()
{
	int iKey;
	int iGraph = 0;
	PMETRIC pM = NULL;

#ifdef WITH_NCURSES
	if( ( iKey = wgetch( pantalla.pGConsole )  ) != ERR )
	{
		switch( iKey )
		{
			case KEY_IC:
				pantalla_console( &pantalla );
				break;

			/* Metrica previa */
			case KEY_UP:
				iGraph = pantalla_metric_draw_menu( &pantalla, -1 );
				pM = metric_get( iGraph);
				adm_comms_graph( pM->nombre, &pantalla);
				/**/pantalla.ipGraph = adm_graph_metric;
				break;

			/* Metrica siguiente */
			case KEY_DOWN:
				iGraph = pantalla_metric_draw_menu( &pantalla, 1);
				pM = metric_get( iGraph);
				adm_comms_graph( pM->nombre, &pantalla);
				/**/pantalla.ipGraph = adm_graph_metric;
				break;

			/* Aumentar la velocidad (10%) */
			case KEY_RIGHT:
				if( ( speed - ( speed *.1 ) ) >= 100 )
					speed -= speed *.1;
				else
					speed = 100;
/*TODO agregar el debug, o como se llame */
				wprintw( pantalla.pGConsole, "Speed (%d)\n", speed );
				break;

			/* Disminuir la velocidad (10%) */
			case KEY_LEFT:
				speed += speed *.1;
				wprintw( pantalla.pGConsole, "Speed (%d)\n", speed );
				break;

			/* Aumentar la escala (25%) */
			case KEY_NPAGE:
				pantalla.pTMetric->scale_y *= 1.25;
				pantalla.iXLastPos =0;
				wclear( pantalla.pGPad );
				wrefresh( pantalla.pGPad );
				wprintw( pantalla.pGConsole, "Zoom In 25% (zoom factor %.3f)\n",
					pantalla.pTMetric->scale_y );
				break;

			case KEY_PPAGE:
				pantalla.pTMetric->scale_y *= .75;
				pantalla.iXLastPos =0;
				wclear( pantalla.pGPad );
				wrefresh( pantalla.pGPad );
				wprintw( pantalla.pGConsole, "Zoom Out 25% (zoom factor %.3f)\n",
					pantalla.pTMetric->scale_y );
				break;
		}
	}
#endif /* WITH_NCURSES */
	return STATUS_SUCCESS;
}
