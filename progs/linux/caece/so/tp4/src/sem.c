/*
 * Trabajo Practico Numero 4.
 * Sistemas Operativos.
 * Grupo MRM
 */

/**
 * \file sem.c
 * Tiene la implementacion de semaforos
 *
 * \author Ricardo Quesada
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "sem.h"
#include "interfaz.h"

#undef SEM_DEBUG

LIST_ENTRY g_sem_list;		/**< Lista de semaforos definidos */

static int sem_id=0;

/**
 * \fn PSEM sem_get_from_id( int id )
 * \brief Dado un id, devuelve el PSEM
 */
PSEM sem_get_from_id( int id )
{
	PSEM pSem;
	PLIST_ENTRY pList = g_sem_list.next;

	while( !IsListEmpty( &g_sem_list ) && ( pList != &g_sem_list ) ) {

		pSem = (PSEM) pList;

		if( id == pSem->id ) {
			return pSem;
		}
		pList = LIST_NEXT(pList);
	}
	console_out("Fuck!\n");
	return NULL;
}

/**
 * \fn
 * \brief Libera del PCB tal semaforo
 */
static STATUS free_sem_from_pcb( int id, PLIST_ENTRY pL )
{
	PLIST_ENTRY pList = pL->next;
	PNODO pN,pTmp;

	while( !IsListEmpty( pL ) && ( pList != pL ) ) {

		pN = (PNODO) pList;

		if( id == pN->id ) {
			pTmp = (PNODO) RemoveHeadList( pList->prev );
#ifdef SEM_DEBUG
			console_out("Freeing '%d'\n",pN->id);
#endif
			free( pTmp );
			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}
	return STATUS_ERROR;
}

/**
 * \fn static STATUS sem_wakeup( char *nombre )
 * \brief Despierta los PCB que esperan en el semaforo
 * \param pSem Semaforo al que se le pide que depierte a los pcb que espera sus recursos
 * \author Ricardo Quesada
 */
static STATUS sem_wakeup( PSEM pSem )
{
	PPCB pPcb;

	while( pSem->max > 0 ) {
		if( !IsListEmpty( &pSem->pcbs )) {
			pSem->max--;
			pPcb = (PPCB) RemoveHeadList( &pSem->pcbs );

#ifdef SEM_DEBUG
			console_out(" Waking up '%s'\n",pPcb->p_nombre);
#endif
			/* Ya no se usa este recurso... sacarlo */
			free_sem_from_pcb( pSem->id, &pPcb->p_sem_l_wait );
			
			resched( pPcb );
		} else
			break;
	}
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS sem_create( char *nombre, int max )
 * \brief Inicializa un semaforo
 * \param nombre Nombre del semaforo (ASCII)
 * \param max Cantidad de recursos del semaforo
 * \author Ricardo Quesada
 */
STATUS sem_create( char *nombre, int max )
{
	PLIST_ENTRY pList = g_sem_list.next;
	PSEM pSem;

	while( !IsListEmpty( &g_sem_list) && ( pList != &g_sem_list ) ) {

		pSem = (PSEM) pList;

		if( strncmp( nombre, pSem->nombre, strlen( pSem->nombre ))==0) {
			/*
			 * No lo creo porque ya existe. Esto no es un
			 * bug, si un 'feature' que se usa para la implementacion
			 * de los semaforos
			 */
			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}

	/* Ok, como no existe lo creo */
	pSem = malloc ( sizeof( SEM));

	if(pSem==NULL)
		return STATUS_NOMEM;

	pSem->id = sem_id++;
	strncpy(pSem->nombre, nombre, SEM_MAX_NAME );
	pSem->max = max;
	InitializeListHead( &pSem->pcbs );

	InsertTailList( &g_sem_list, (PLIST_ENTRY) pSem );

	return STATUS_SUCCESS;
}


/**
 * \fn STATUS sem_wait( int id, PPCB pPcb )
 * \brief Espera que el recurso de tal semaforo este disponible
 * \param nombre Nombre del semaforo que se espera
 * \author Ricardo Quesada
 */
STATUS sem_wait( int id, PPCB pPcb )
{
	PLIST_ENTRY pList = g_sem_list.next;
	PSEM pSem;

	while( !IsListEmpty( &g_sem_list) && ( pList != &g_sem_list ) ) {

		pSem = (PSEM) pList;

		if( id == pSem->id ) {
			PNODO pN;

#ifndef SEM_DEBUG
			console_out("sem_wait '%s'->'%s'\n",pPcb->p_nombre,pSem->nombre);
#endif
			pN = (PNODO) malloc( sizeof(NODO) );
			pN->id = id;

			if( pSem->max == 0 ) {

				InsertTailList( &pSem->pcbs, (PLIST_ENTRY) pPcb );

				InsertTailList( &pPcb->p_sem_l_wait, (PLIST_ENTRY) pN );
				pPcb->p_estado = PROC_SLEEP;
				return STATUS_EVENT;
			} else {
				pSem->max--;
				InsertTailList( &pPcb->p_sem_list, (PLIST_ENTRY) pN );
			}

			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}
	return STATUS_ERROR;
}

/**
 * \fn STATUS sem_signal( int id )
 * \brief Avisa que un recurso del semaforo esta disponible
 * \param id Id del semaforo
 * \author Ricardo Quesada
 */
STATUS sem_signal( int id, PPCB pPcb )
{
	PLIST_ENTRY pList = g_sem_list.next;
	PSEM pSem;


	while( !IsListEmpty( &g_sem_list) && ( pList != &g_sem_list ) ) {

		pSem = (PSEM) pList;

		if( id == pSem->id ) {
#ifndef SEM_DEBUG
			console_out("sem_signal '%s'->'%s'\n",pPcb->p_nombre,pSem->nombre);
#endif
			pSem->max++;
			sem_wakeup( pSem );
			free_sem_from_pcb( pSem->id, &pPcb->p_sem_list );
			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}
	return STATUS_ERROR;
}

/**
 * \fn STATUS sem_a2i( char *nombre, &id )
 * \brief Dado un nombre, devulve el id del semaforo
 * \author Ricardo Quesada
 */
STATUS sem_a2i( char *nombre, int *id )
{
	PLIST_ENTRY pList = g_sem_list.next;
	PSEM pSem;

	while( !IsListEmpty( &g_sem_list) && ( pList != &g_sem_list ) ) {

		pSem = (PSEM) pList;

		if( strncmp( nombre, pSem->nombre, strlen( pSem->nombre)) == 0) {
			*id = pSem->id;
			return STATUS_SUCCESS;
		}
		pList = LIST_NEXT(pList);
	}
	return STATUS_ERROR;
}


/**
 * \fn STATUS sem_init()
 * \brief Inicializa las funciones de semaforos
 * \author Ricardo Quesada
 */
STATUS sem_init()
{
	InitializeListHead( &g_sem_list );
	sem_id = 0;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS sem_free( PPCB pPcb )
 * \brief libera los semaforos alocados por el proceso
 * \param pPcb PCB que tiene los semaforos
 */
STATUS sem_free( PPCB pPcb )
{
	PNODO pN;
	PPCB pP;
	PSEM pSem;
	PLIST_ENTRY pList;

	/* libera los recursos que esta esperando */
	while( !IsListEmpty( &pPcb->p_sem_l_wait )) {
		pN = (PNODO) RemoveHeadList( &pPcb->p_sem_l_wait );

		pSem = sem_get_from_id( pN->id );

		pList = pSem->pcbs.next;
		while( !IsListEmpty( &pSem->pcbs ) && ( pList != &pSem->pcbs) ) {

			pP = (PPCB ) pList;

			if(pP->p_pid == pPcb->p_pid ) {
				pP = (PPCB) RemoveHeadList( pList->prev );
			}
			pList = LIST_NEXT(pList);
		}
		free( pN );
	}

	/* libera los recursos que tiene tomados */
	while( !IsListEmpty( &pPcb->p_sem_list )) {
		pN = (PNODO) RemoveHeadList( &pPcb->p_sem_list );

		sem_signal( pN->id, pPcb );
		free( pN );
	}
	return STATUS_SUCCESS;
}
