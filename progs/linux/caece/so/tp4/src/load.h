/*
 */

/**
 * \file load.h
 */
#ifndef __LOAD_H
#define __LOAD_H

#define FILE_VERSION	1
#define LOAD_VERSION		"version"
#define LOAD_PARAM		"param"

STATUS load( PPCB pPcb );
STATUS get_rem( FILE *fp );

#endif /* __LOAD_H */
