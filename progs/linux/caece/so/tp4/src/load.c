/*
 * Trabajo Practico Numero 3.
 * Sistemas Operativos.
 * Grupo MRM
 */
#include <stdio.h>
#include <string.h>

#include "tipos.h"
#include "sched.h"
#include "archivo.h"
#include "interfaz.h"
#include "load.h"
#include "token.h"
#include "mem.h"
#include "sem.h"

/**
 * \file load.c
 * Funciones que manejan la entrada de procesos a traves de
 * un archivo de datos
 * \author Ricardo Quesada
 */

/**
 * \fn static STATUS get_version( FILE *fp )
 * \brief chequea que la version del archivo sea correcta
 */
static STATUS get_version( FILE *fp )
{
	char c_version[200];
	int d_version;

	fscanf( fp, ".%s\t%d\n", c_version, &d_version);
	if( strcmp( c_version, LOAD_VERSION )!=0 || d_version != FILE_VERSION ) {
		return STATUS_ERROR;
	}
	return STATUS_SUCCESS;
}

/**
 * \fn static STATUS get_vars( PPCB pPcb, FILE *fp )
 * \brief chequea que la version del archivo sea correcta
 */
static STATUS get_options( PPCB pPcb, FILE *fp )
{
	int r;
	char c;
	char quesoy[1000];
	char param1[1000];
	int  param2;

	PDEBUG("get_specific()");

read_loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	r=fread( &c, 1 ,1, fp);
	if(r!=1 || c=='\n')
		goto read_loop;

	if( c=='.') {
		fscanf( fp,"%s %s %d\n", quesoy, param1, &param2 );
		if(strcmp( quesoy,"sem")==0) {
			sem_create( param1, param2 );
		} else if(strcmp( quesoy,"var")==0) {
			sem_create( param1, param2 );
		}
		goto read_loop;
	}
	fseek( fp, -1 ,SEEK_CUR);
	return STATUS_SUCCESS;
}

/**
 * \fn static STATUS get_param( PPCB pPcb, FILE *fp )
 * \brief Carga el size del dataseg, textseg y stackseg
 */
static STATUS get_param( PPCB pPcb, FILE *fp )
{
	char c_param[200];
	unsigned long	memcode=0;
	unsigned long	memdata=0;
	unsigned long	memstack=0;

	fscanf( fp, ".%s\t%d\t%d\t%d\n", c_param, (unsigned*) &memcode, (unsigned*)&memdata, (unsigned*)&memstack );

	if( strcmp( c_param, LOAD_PARAM) !=0 || !memcode ) {
		return STATUS_ERROR;
	}

	pPcb->p_memcode = memcode; 
	pPcb->p_memdata = memdata;
	pPcb->p_memstack = memstack;
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS get_rem( FILE *fp )
 * \brief Skipea los rems
 */
STATUS get_rem( FILE *fp )
{
	int r;
	char c;

	PDEBUG("get_rem()");

read_loop:
	if( feof( fp ))
		return STATUS_FILEEOF;

	r=fread(  &c, 1 ,1, fp);
	if(r!=1 || c=='\n')
		goto read_loop;

	if( c=='!') {
		while(c!='\n')
			fread(  &c, 1 ,1, fp);
		goto read_loop;
	}
	while( c==' ' || c=='\t' ) {
		fread( &c, 1, 1, fp );
		if( feof( fp ))
			return STATUS_FILEEOF;
	}
	if( c=='\n' )
		goto read_loop;
	fseek( fp, -1 ,SEEK_CUR);
	return STATUS_SUCCESS;
}

/**
 * \fn STATUS load( PPCB pPcb )
 * \brief Encargada de cargar el archivo pPcb->p_nombre en mem
 */
STATUS load( PPCB pPcb )
{
	FILE *fp=NULL;
	char nombre[200];
	STATUS s;

	PDEBUG("load()\n");

	strcpy( nombre, pPcb->p_nombre );
	strcat( nombre, ".prg" );

	if( (fp = fopen( nombre, "rt" )) == NULL) {
		console_out("Error al abrir el archivo: '%s'\n",nombre);
		goto error;
	}

	if( get_rem( fp ) == STATUS_FILEEOF )
		return STATUS_FILEEOF;

	if( get_version( fp ) != STATUS_SUCCESS ) {
		console_out("Formato desconocido en el archivo: '%s'\n",nombre);
		goto error;
	}

	if( get_param( pPcb, fp ) != STATUS_SUCCESS ) {
		console_out("Formato desconocido en el archivo: '%s'\n",nombre);
		goto error;
	}

	if( get_mem( pPcb ) != STATUS_SUCCESS ) {
		console_out("No hay memoria para cargar el archivo: '%s'\n",nombre);
		goto error;
	}

	if( (s=get_options( pPcb, fp )) != STATUS_SUCCESS )
		return s;

	if( tokenize_to_mem( pPcb, fp ) != STATUS_SUCCESS ) {
		console_out("Syntax error in: '%s'\n",nombre);
		goto error;
	}

	console_out("Encolando '%s' ",nombre);
	if( pPcb->p_politica == SCHED_RR )
		console_out("pol=RR,");
	else
		console_out("pol=FIFO,");
	console_out("nice=%d,pri=%d,",pPcb->p_nice,pPcb->p_basepriority);
	if( pPcb->p_cpu == -1)
		console_out("cpu=ANY");
	else
		console_out("cpu=%d",pPcb->p_cpu);
	console_out("\n");

	if(fp)
		fclose( fp );
	return STATUS_SUCCESS;

error:
	if(fp)
		fclose( fp );
	return STATUS_ERROR;
}
