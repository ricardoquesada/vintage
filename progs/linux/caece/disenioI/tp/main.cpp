//	$Id$


#include "instrucciones.h"
#include "estado.h"

#include <string>

using namespace std;

int main()
{
	Instrucciones m_inst;
	string nombre;
	int nivel;

	cout << (*m_inst.show()) << endl;

	cout << "Ingrese nombre: ";
	cin >> nombre;
	cout << "Ingrese nivel: ";
	cin >> nivel;

	Camara cam1("camara 1"), cam2("camara 2"), cam3("camara 3");
	cam1.add(&cam2);
	cam1.add(&cam3);

	Camara cam4("camara 4"), cam5("camara 5");
	cam2.add(&cam4);
	cam2.add(&cam5);

	Camara cam6("camara 6");
	cam3.add(&cam5);
	cam3.add(&cam6);

	Camara cam7("camara 7"), cam8("camara 8"), cam9("camara 9");
	cam4.add(&cam7);
	cam5.add(&cam8);
	cam6.add(&cam9);

	Camara cam10("camara final",true);
	cam7.add(&cam10);
	cam8.add(&cam10);
	cam9.add(&cam10);

	Preguntas pregs("preguntas.txt");

	Estado m_estado(nombre,nivel,&cam1,&pregs);

	m_estado.run();

	return 0;
}
