//	$Id$

#include "pregunta.h"
#include <string>

using namespace std;

void Pregunta::setPregunta( const std::string& pregunta )
{
	m_pregunta = pregunta;
}

void Pregunta::addRespuesta(const std::string &respuesta)
{
	m_respuestas[m_indice++] = respuesta;
}

void Pregunta::setCorrecta( int correcta )
{
	m_correcta = correcta;
}


void Pregunta::preguntar()
{
	m_preguntada = true;
	cout << m_pregunta << endl;

	for(int i=0;i<m_indice;i++) {
		cout << i+1 << " - " << m_respuestas[i] << endl;
	}
}

bool Pregunta::esCorrecta(int i )
{
	return (m_correcta == i);
}
