//	$Id$

#include "camara.h"

#include <string>

using namespace std;

Camara::Camara( const string& nombre, bool final )
: m_nombre(nombre), m_final(final)
{
}

Camara::Camara( const Camara &cam )
: m_nombre(cam.m_nombre), m_final(cam.m_final), m_camaras(cam.m_camaras)
{
}

void Camara::add( Camara *camara )
{
	m_camaras[camara->nombre()] = camara;
}

Camara* Camara::find( string &nombre )
{
	camara_map::iterator my_it;

	my_it = m_camaras.find(nombre);

	if(my_it == m_camaras.end() )
		throw "Camara no encontrada";
	
	return  my_it->second;
}

void Camara::alternativas()
{
	string posibles;

	camara_map::iterator my_it;

	my_it = m_camaras.begin();

	for( my_it = m_camaras.begin(); my_it != m_camaras.end(); my_it++ )
		cout << (*my_it).first << " " << (*my_it).second << endl;
}

//Camara::~Camara()
//{
//	camara_map::iterator my_it;
//	m_camaras.erase(1);
//}
