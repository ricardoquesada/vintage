//	$Id$

#ifndef __PREGUNTAS_H
#define __PREGUNTAS_H

#include "pregunta.h"
#include "temas.h"

#include <map>
#include <string>

class Preguntas {
public:
	enum niveles {
		NIVEL_FACIL,
		NIVEL_MEDIO,
		NIVEL_DIFICIL,

		NIVEL_LAST
	};

	Preguntas( const std::string& filename );
	bool preguntar( const std::string &tema, int nivel);
	void temas( void ) { return m_temas.enum_temas(); }
	bool tema_valido( int i ) { return m_temas.tema_valido(i); }
	string& i_to_tema( int i ) { return m_temas.i_to_tema(i); }

private:
	bool hacerPregunta(Pregunta *p);

	typedef multimap <const std::string,Pregunta *> preguntas_multimap; 
	preguntas_multimap m_preguntas;
	Temas m_temas;
};

#endif // __PREGUNTAS_H
