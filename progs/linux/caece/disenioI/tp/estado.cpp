//	$Id$

#include "estado.h"
#include "camara.h"
#include "preguntas.h"

#include <string>

using namespace std;

Estado::Estado( string &nombre, int nivel, Camara *camara, Preguntas *pregs )
{
	m_nombre = nombre;
	m_nivel = nivel;
	m_camara = camara;
	m_preguntas_bien = 0;
	m_preguntas_mal = 0;
	m_pregs = pregs;
}

void Estado::run()
{
	int i;
	bool bien=false;

	cout << "Elija tema:" << endl;
	m_pregs->temas();
	cin >> i;

	while(!bien && m_preguntas_mal < PREG_MAL) {
		cout << "Usted esta en la camara: " << m_camara->nombre() << endl;
		cout << "Para pasar de camara tiene que contestar: " << PREG_BIEN - m_preguntas_bien << endl;
		cout << "Vidas: " << PREG_MAL - m_preguntas_mal << endl;

		if( m_pregs->tema_valido(i) ) {
			bien = m_pregs->preguntar( m_pregs->i_to_tema(i), m_nivel );
			if( !bien ) {
				m_preguntas_mal++;
				cout << endl << "*** Respuesta incorrecta ***" << endl;
			}
		}
	}
	if( m_preguntas_mal == PREG_MAL )
		cout << "Ud. perdio" << endl;
	else {
		cout << "Ud. paso de camara." << endl;
		cout << "Elija nueva camara." << endl;
		m_camara->alternativas();
	}
}
