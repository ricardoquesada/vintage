//	$Id$

#ifndef __TEMAS_H
#define __TEMAS_H

#include <map>
#include <string>

class Temas
{
public:
	bool add_tema( const std::string & tema );
	bool tema_valido( int i);
	string& i_to_tema( int i);
	void enum_temas( void );

private:
	typedef map <std::string, int> temas_map;
	typedef map <int, std::string> temas_map_i;

	temas_map m_temas;
	temas_map_i m_temas_i;
};

#endif // __TEMAS_H
