//	$Id$

#ifndef __PREGUNTA_H
#define __PREGUNTA_H

#include <string>
#include <map>

class Pregunta {
public:
	Pregunta() : m_nivel(0), m_indice(0), m_preguntada(false) {};
	void setPregunta( const std::string &pregunta );
	void addRespuesta(const std::string &respuesta);
	void setCorrecta( int correcta );
	void setTema( const std::string &tema ) { m_tema = tema; }
	void setNivel( int nivel ) { m_nivel = nivel; }
	int nivel() { return m_nivel; }

	void preguntar();
	bool esCorrecta( int i );
	const std::string& tema() { return m_tema; }
	bool fueHecha() { return m_preguntada; }

private:
	typedef map <int, std::string> pregunta_map; 

	int m_nivel;
	std::string m_tema;
	std::string m_pregunta;
	pregunta_map m_respuestas;
	int m_correcta;
	int m_indice;
	bool m_preguntada;		// Ya fue hecha esta pregunta
};
#endif // __PREGUNTA_H

