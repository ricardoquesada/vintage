//	$Id$

#ifndef __CAMARA_H
#define __CAMARA_H

#include <map>
#include <string>

class Camara {
public:
	Camara( const std::string &nombre, bool final = true );
	Camara( const Camara&);
	void add( Camara * );
	Camara *find (std::string& nombre);
	void alternativas();
	std::string nombre() const { return m_nombre; };
private:
	typedef std::map<std::string, Camara *> camara_map;
	std::string m_nombre;
	camara_map m_camaras;
	bool m_final;
};
#endif // __CAMARA_H

