//	$Id$

#include <stdlib.h>
#include <fstream.h>
#include <string.h>
#include <string>

#include "preguntas.h"
#include "pregunta.h"

using namespace std;

bool Preguntas::hacerPregunta(Pregunta *p)
{
	p->preguntar();
	int i;

	cin >> i;

	return p->esCorrecta(i);
}

bool Preguntas::preguntar( const string &tema, int nivel )
{
	preguntas_multimap::iterator lastElement, my_it = m_preguntas.find(tema);

	if( my_it == m_preguntas.end() ) {
		cerr << "Tema no encontrado. Por favor elija otro tema";
		return false;
	}

	int j=(int) ((float)(m_preguntas.count(tema))*rand()/(RAND_MAX+1.0));
	int i=0;

	lastElement = m_preguntas.upper_bound(tema);

	cout << "Haciendo pregunta: " << j << " del tema: " << tema << endl;

	// de j al final
	for ( ; my_it != lastElement; ++my_it) {
		Pregunta *p = my_it->second;

		if( !p->fueHecha() && i >= j )
			return hacerPregunta(p);
		i++;
	}

	// del principio hasta j-1
	my_it = m_preguntas.find(tema);
	for ( ; my_it != lastElement; ++my_it) {
		Pregunta *p = my_it->second;

		if( !p->fueHecha() && i < j )
			return hacerPregunta(p);
		i++;
	}

	return false;
}

Preguntas::Preguntas( const string & filename )
{
	ifstream infile(filename.c_str(), ios::in, 0664);
	Pregunta *p = NULL;

	if ( !infile ) {
		cerr << "No se pudo abrir el archivo: " << filename << endl;
		throw "No se encontro el archivo";
	}
  
	while ( !infile.eof() ) {
		char line[500];

		memset(line,0,sizeof(line));
		infile.getline(line,sizeof(line),'\n');

		// Tema
		if( line[0]=='t') {
			if( !p )
				p = new Pregunta();

			if( p ) {
				p->setTema( &line[2] );
				m_temas.add_tema( &line[2] );
			} else 
				throw "Error al crear pregunta";
		}

		// Pregunta
		else if( line[0]=='p' ) {
			if( p ) {
				p->setPregunta(&line[2]);
			}
		}

		// Respuesta
		else if( line[0]=='r' ) {
			if( p ) {
				p->addRespuesta(&line[2]);
			}
		}

		// Nivel
		else if( line[0]=='n' ) {
			int nivel = atoi(&line[2]);
			if( p ) {
				p->setNivel(nivel);
			}
		}

		// Solucion
		else if( line[0]=='s' ) {
			int correcta = atoi( &line[2] );
			if( p ) {
				p->setCorrecta(correcta);
				int nivel = p->nivel();
				if( nivel >= 0 && nivel < NIVEL_LAST ) {
					m_preguntas.insert( make_pair(p->tema(), p) );
					p = NULL;
				}
			}
		}
	}

	infile.close();
}
