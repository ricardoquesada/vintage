//	$Id$

#include "temas.h"

#include <string>

using namespace std;

bool Temas::add_tema( const string &tema )
{
	static int indice = 0;

	temas_map::iterator my_it = m_temas.find(tema);

	if(my_it != m_temas.end() )
		return false;

	m_temas_i[indice] = tema;
	m_temas[tema] = indice++;
	return true;
}

void Temas::enum_temas( void )
{
#if 0
	temas_map::iterator my_it;

	my_it = m_temas.begin();

	for( my_it = m_temas.begin(); my_it != m_temas.end(); my_it++ ) {
		cout << (*my_it).second << " - " << (*my_it).first << endl;
	}
#else
	unsigned int i;
	for(i=0;i<m_temas.size();i++) {
		cout << i << " - " << m_temas_i[i] << endl;
	}
#endif
}

bool Temas::tema_valido( int i )
{
	if( i < 0 || static_cast<unsigned int>(i) >= m_temas.size() )
		return false;
	return true;
}

string& Temas::i_to_tema( int i )
{
	return m_temas_i[i];
}
