//	$Id$

#ifndef __INSTRUCCIONES_H
#define __INSTRUCCIONES_H

#include <string>

class Instrucciones {
public:
	std::string * show();
	void set( std::string texto ) { m_texto = texto; };
private:
	std::string m_texto;
};

#endif // __INSTRUCCIONES_H
