//	$Id$

#ifndef __ESTADO_H
#define __ESTADO_H

#include <string>
#include "camara.h"
#include "preguntas.h"

class Estado {
public:
	Estado( string &nombre, int nivel, Camara *camara, Preguntas *pregs );
	void run();

	enum {
		PREG_BIEN=3,
		PREG_MAL=3
	};
private:
	std::string m_nombre;
	int m_nivel;
	Camara *m_camara;
	int m_preguntas_bien;
	int m_preguntas_mal;
	Preguntas *m_pregs;
};

#endif // __ESTADO_H
