#include <asm/io.h>

#include "video.h"
#include "ints.h"
#include "asm.h"

unsigned long timer=0;
unsigned char lastkey=0;

#define IRQ_NONE	0x00
#define IRQ_TIMER	0x01
#define IRQ_KEYBOARD	0x02
#define IRQ_CASCADE	0x04
#define IRQ_3		0x08
#define IRQ_4		0x10
#define IRQ_5		0x20
#define IRQ_6		0x40
#define IRQ_7		0x80

void initInts() {
	static uint32 IDT[0x100*2]={0};
	int i;

	for (i=0x20;i<0x2f;i++)
		setIDT(IDT,i,nullIRQ);
//	for (i=0x10;i<0x100;i++)
//		setIDT(IDT,i,nullISR);

	setIDT(IDT,0x20,clockISR);
	setIDT(IDT,0x21,keyboardISR);

	lidt((uint32)IDT,sizeof(IDT));

//	outb_p(~(IRQ_TIMER|IRQ_CASCADE|IRQ_KEYBOARD),0x21);		// 
	outb_p(~(IRQ_CASCADE|IRQ_KEYBOARD),0x21);		// 
	outb_p(~IRQ_NONE,0xA1);

	sti();
}

void setIDT(uint32 *IDT,unsigned int intNum,void *ISR) {
	IDT[2*intNum]=0x00100000 | ((uint32)ISR & 0x0000ffff);
	IDT[2*intNum+1]=((uint32)ISR & 0xffff0000) | 0x00008E00;
}

void keyboardISR() {
	int i;
//	asm("bb:jmp bb");
	asm("
		push	%%ax
		inb	$0x60,%%al
		movb	%%al,%0" : "=a" (lastkey));
	asm("
		inb	$0x61,%al
		movb	%al,%ah
		orb	$0x80,%al
		outb	%al,$0x61
		movb	%ah,%al
		outb	%al,$0x61
		movb	$0x20,%al
		outb	%al,$0x20");

	for(i=0;i<1000;i++) {
		vidmem[i]=lastkey;
		vidmem[i+1]=(char) i;
		i++;
	}

	vidmem[158]=lastkey;
	vidmem[159]=0xf;

	asm("
		pop	%ax
		addl	$4,%esp
		iret");
}


void clockISR() {
	asm("
		push	%ax");
	timer++;
	asm("
		movb	$0x20,%al
		outb	%al,$0x20
		pop	%ax
		addl	$4,%esp
		iret");
}

void nullIRQ() {
	asm("
		push	%ax
		movb	$0x20,%al
		out	%al,$0x20
		pop	%ax
		addl	$4,%esp
		iret");
}

void  nullISR() {
	asm("iret");
}

