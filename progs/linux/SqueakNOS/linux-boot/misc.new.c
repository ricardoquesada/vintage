/*	$Id: misc.c,v 1.4 2000/07/17 04:20:07 gera Exp $
 * misc.c
 */

#include <asm/segment.h>
#include <asm/io.h>

#define __KERNEL__
#include <linux/tty.h>

#include "video.h"

#define OF(args)  args
#define STATIC static

#undef memset
#undef memcpy
#define memzero(s, n)     memset ((s), 0, (n))

#define SCREEN_INFO (*(struct screen_info *)0x90000)

extern char input_data[];
extern int input_len;

static void error(char *m);
  
void* memset(void* s, int c, size_t n)
{
	int i;
	char *ss = (char*)s;

	for (i=0;i<n;i++) ss[i] = c;
}

void* memcpy(void* __dest, __const void* __src,
			    size_t __n)
{
	int i;
	char *d = (char *)__dest, *s = (char *)__src;

	for (i=0;i<__n;i++) d[i] = s[i];
}

static void error(char *x)
{
	puts("\n\n");
	puts(x);
	puts("\n\n -- System halted");

	while(1);	/* Halt */
}

#define STACK_SIZE (64*1024)

long user_stack [STACK_SIZE];

struct {
	long * a;
	short b;
	} stack_start = { & user_stack [STACK_SIZE] , __KERNEL_DS };

// This is the main entry point after bootloader
#define main decompress_kernel
int main(void *mv)
#undef main
{
	initVideo();
	sqMain(input_data);
}

void exit() {
	puts("Exiting...\n");
	while (1);
}
int timer=0;
