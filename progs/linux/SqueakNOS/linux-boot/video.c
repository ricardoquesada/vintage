#include <asm/segment.h>
#include <asm/io.h>

#include "video.h"

char *vidmem = (char *)0xb8000;
int vidport;
int lines, cols;

int initVideo() {
	SCREEN_INFO.orig_video_mode = 0x13;
	SCREEN_INFO.orig_video_lines = 24;
	SCREEN_INFO.orig_video_cols = 80;
	SCREEN_INFO.orig_x = 0;
	SCREEN_INFO.orig_y = 0;

	if (SCREEN_INFO.orig_video_mode == 7) {
		vidmem = (char *) 0xb0000;
		vidport = 0x3b4;
	} else {
		vidmem = (char *) 0xb8000;
		vidport = 0x3d4;
	}

	lines = SCREEN_INFO.orig_video_lines;
	cols = SCREEN_INFO.orig_video_cols;

	return 1;
}

void scroll() {
    int i;

    memcpy ( vidmem, vidmem + cols * 2, ( lines - 1 ) * cols * 2 );
    for ( i = ( lines - 1 ) * cols * 2; i < lines * cols * 2; i += 2 )
        vidmem[i] = ' ';
}

void puts(char *s) {
    int x,y,pos;
    char c;

    x = SCREEN_INFO.orig_x;
    y = SCREEN_INFO.orig_y;

    while ( ( c = *s++ ) != '\0' ) {
        if ( c == '\n' ) {
            x = 0;
            if ( ++y >= lines ) {
                scroll();
                y--;
            }
        } else {
            vidmem [ ( x + cols * y ) * 2 ] = c;
            if ( ++x >= cols ) {
                x = 0;
                if ( ++y >= lines ) {
                    scroll();
                    y--;
                }
            }
        }
    }
	SCREEN_INFO.orig_x = x;
	SCREEN_INFO.orig_y = y;

	pos = (x + cols * y) * 2;   // Update cursor position 
	outb_p(14, vidport);
	outb_p(0xff & (pos >> 9), vidport+1);
	outb_p(15, vidport);
	outb_p(0xff & (pos >> 1), vidport+1);
}

void putx(int n) {
	const char digits[]="0123456789abcdef";
	char str[9];
	int i;
	for (i=7;i>=0;i--) {
		str[i]=digits[n&0xf];
		n>>=4;
	}
	str[8]=0;
	puts(str);
}

void putc(const char c) {
	char str[2];
	str[0]=c;
	str[1]=0;
	puts(str);
}

int waitChar() {
	return 0;
}
