#include "types.h"

struct {
	unsigned short limit;
	unsigned long addr __attribute__((packed));
} idt_descriptor;


void inline lidt(uint32 offset, unsigned short size) {

	idt_descriptor.limit = size -1;
	idt_descriptor.addr = offset;

/*	asm("lidt (%0)"::"p" (((char *)idtp)+2)); */

	__asm__ __volatile__ ("lidt %0" : "=m" (idt_descriptor));  /* riq */

}

void inline lgdt(uint32 offset, uint32 size) {
	uint32 gdtp[2];

	gdtp[0]=(size-1) << 0x10;
	gdtp[1]=offset;

/*	asm("lgdt (%0)"::"p" (((char *)gdtp)+2)); */
	__asm__ __volatile__ ("lgdt %0" : : "m" (((char *)gdtp)+2)); /* riq */
}
