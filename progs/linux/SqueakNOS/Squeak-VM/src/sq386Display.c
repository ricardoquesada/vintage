#define main sqMain
#include "sq.h"

#ifdef NEED_SELECT
# include <sys/select.h>
#endif

/*** Variables -- Imported from Virtual Machine ***/
extern unsigned char *memory;
extern int interruptKeycode;
extern int interruptPending;
extern int interruptCheckCounter;
#ifdef CCACHE
extern int contextCacheEntries;
extern int stackCacheEntries;
#endif

int initialHeapSize;			/* 5 megabytes by default */

int    initialArgc;
char **initialArgv;

int		 sleepWhenUnmapped= 0;
int		 noTitle= 0;
int		 fullScreen= 0;
unsigned long 	startUpTime;

/* maximum input polling frequency */
#define	MAXPOLLSPERSEC	33


/*** Functions ***/

#ifdef ioMSecs
# undef ioMSecs
#endif

int  HandleEvents(void);
void RecordFullPathForImageName(char *localImageName);
void SetUpTimers(void);
void segv(int ignored);

extern void aioPollForIO(int, int);	/* see sqUnixNetwork.c */


/*** VM Home Directory Path ***/

int vmPathSize(void)
{
}

int vmPathGetLength(int sqVMPathIndex, int length)
{
}


int HandleEvents(void)
{
  /* quick check for asynchronous socket i/o */
  aioPollForIO(0, 0);
  return 0;
}


/*** I/O Primitives ***/

int ioFormPrint(int bitsAddr, int width, int height, int depth,
		double hScale, double vScale, int landscapeFlag)
{
  puts(
	  "Sorry, a headless VM cannot print Forms.  If you\n"
	  "*really* need this then let me know, since there\n"
	  "is a (rather painful to implement) solution.\n");
  return false;
}


int ioBeep(void)
{
  return 0;
}


int ioGetButtonState(void)
{
  ioProcessEvents();  /* process all pending events */
  return 0;
}

int ioGetKeystroke(void)
{
  int keystate;

  ioProcessEvents();  /* process all pending events */
  return -1;  /* keystroke buffer is empty */
}

int ioMSecs(void)
{
/*
  struct timeval now;
  gettimeofday(&now, 0);
  if ((now.tv_usec -= startUpTime.tv_usec) < 0) {
    now.tv_usec += 1000000;
    now.tv_sec -= 1;
  }
  now.tv_sec -= startUpTime.tv_sec;
  return (now.tv_usec / 1000 + now.tv_sec * 1000) & 0x1fffffff;
*/
}

int ioMicroMSecs()
{
  /* already have millisecond resolution */
  return ioMSecs();
}

int ioRelinquishProcessorForMicroseconds(int microSeconds)
{
  /* sleep in select() for immediate response to socket i/o */
  aioPollForIO(microSeconds, 0);
  return microSeconds;
}

int ioMousePoint(void)
{
  int x= 320, y= 240;
  ioProcessEvents();  /* process all pending events */
  return (x << 16) | (y & 0xFFFF);  /* x is high 16 bits; y is low 16 bits */
}

int ioPeekKeystroke(void)
{
  ioProcessEvents();  /* process all pending events */
  return -1;  /* keystroke buffer is empty */
}

/* this should be rewritten to use SIGIO and/or the interval timers */
int ioProcessEvents(void)
{
  static unsigned long nextPollTick= 0;

  if ((unsigned long)ioMSecs() > nextPollTick)
    {
      /* time to process events! */
      while (HandleEvents())
	{
	  /* process all pending events */
	}
    /* wait a while before trying again */
      nextPollTick= ioMSecs() + (1000 / MAXPOLLSPERSEC);
    }
  return 0;
}

/* returns the size of the Squeak window */
int ioScreenSize(void)
{
  int w= 640, h= 480;
  return (w << 16) | (h & 0xFFFF);  /* w is high 16 bits; h is low 16 bits */
}

/* returns the local wall clock time */
int ioSeconds(void)
{
/*
  extern int convertToSqueakTime(int unixTime);
  struct timeval tv;

  gettimeofday(&tv, 0);
#ifdef HAS_TIMEZONE
  return convertToSqueakTime(tv.tv_sec - timezone);
#else
  return convertToSqueakTime(tv.tv_sec + localtime(&tv.tv_sec)->tm_gmtoff);
#endif
*/
}

int ioSetCursor(int cursorBitsIndex, int offsetX, int offsetY)
{
  unsigned char data[32], mask[32];     /* cursors are always 16x16 */
  unsigned char *screen=(unsigned char*)0xa0000;
	int i;

  for (i= 0; i < 16; i++) {
		data[i*2+0]= ((unsigned)checkedLongAt(cursorBitsIndex + (4 * i)) >> 24) & 0xFF;
//		screen[(640/8)*offsetY+offsetX/8+i*2+1]=data[i*2+0];
		screen[(640/8)*offsetY+offsetX/8+i*2+1]=0xff;
		data[i*2+1]= ((unsigned)checkedLongAt(cursorBitsIndex + (4 * i)) >> 16) & 0xFF;
		screen[(640/8)*offsetY+offsetX/8+i*2+0]=0xff;
//		screen[(640/8)*offsetY+offsetX/8+i*2+0]=data[i*2+1];
		offsetY++;
//		mask[i*2+0]= ((unsigned)checkedLongAt(cursorMaskIndex + (4 * i)) >> 24) & 0xFF;
//		mask[i*2+1]= ((unsigned)checkedLongAt(cursorMaskIndex + (4 * i)) >> 16) & 0xFF;
	}

  return 0;
}


int ioSetFullScreen(int fullScreen)
{
  return 0;
}


int ioForceDisplayUpdate(void)
{
  return 0;
}

int ioShowDisplay(int dispBitsIndex, int width, int height, int depth,
		  int affectedL, int affectedR, int affectedT, int affectedB)
{
  unsigned char *screen=(unsigned char*)0xa0000;

	copyReverseImageBytes(
				 dispBitsIndex,screen, 1, width, height,
			   affectedL, affectedT, affectedR, affectedB);

  return 0;
}


/*** Image File Naming ***/

void RecordFullPathForImageName(char *localImageName)
{
}

int imageNameSize(void)
{
}

int imageNameGetLength(int sqImageNameIndex, int length)
{
}

int imageNamePutLength(int sqImageNameIndex, int length)
{
}

/*** Timing support ***/

void SetUpTimers(void)
{
  startUpTime=timer;
}

int clipboardSize(void)
{
  return 0;
}

/* claim ownership of the X selection, providing the given string to requestors */
int clipboardWriteFromAt(int count, int byteArrayIndex, int startIndex)
{
  return 0;
}

/* transfer the X selection into the given byte array; optimise local requests */
int clipboardReadIntoAt(int count, int byteArrayIndex, int startIndex)
{
  return 0;
}

/*** Profiling ***/

int clearProfile(void) { return 0; }
int dumpProfile(void) { return 0; }
int startProfiling(void) { return 0; }
int stopProfiling(void) { return 0; }

/*** Access to system attributes and command-line arguments ***/

static char *getSpecialAttribute(int id)
{
  static char pid[]="0";
  return pid;
}

int attributeSize(int id)
{
  if (id + 1 >= initialArgc) return 0;
  if (id < 0)
    return strlen(getSpecialAttribute(-id));
  else
    return strlen(initialArgv[id + 1]);
}

int getAttributeIntoLength(int id, int byteArrayIndex, int length)
{
  char *attrIndex= (char *)byteArrayIndex;
  char *arg;
  if (id + 1>= initialArgc) return 0;
  if (id < 0)
    arg= getSpecialAttribute(-id);
  else
    arg= initialArgv[id + 1];
  while (length--)
    *attrIndex++= *arg++;
  return 0;
}

/*** Command line ***/

static char *progName;

/*** Segmentation fault handler ***/

#ifdef __alpha__
/* headers for setsysinfo (see below) */
# include <sys/sysinfo.h>
# include <sys/proc.h>
#endif

void main(void *image)
{

  /* initialisation */

	puts("setting up internal variables.\n");

  SetUpTimers();		// Assign StartUpTime

  sqFileInit();			// Assign thisSession (Session ID)
  joystickInit();		// void now

#ifdef NEED_TZSET
  //tzset();	/* should _not_ be necessary! */
#endif

  RecordFullPathForImageName(0);

  /* check the interpreter's size assumptions for basic data types */
  if (sizeof(int) != 4)    error("This C compiler's integers are not 32 bits.");
  if (sizeof(double) != 8) error("This C compiler's floats are not 64 bits.");
  // if (sizeof(time_t) != 4) error("This C compiler's time_t's are not 32 bits.");

  /* read the image file and allocate memory for Squeak heap */
  {
	static SQFile imgFile;
    sqImageFile f=&imgFile;
		f->file=image;
		f->offset=0;
    readImageFromFileHeapSize(f, initialHeapSize);
  }

  /* run Squeak */
	puts("starting to interpret loaded image.\n");
//	waitChar();
  interpret();

}


int ioExit(void)
{
  exit(0);
}
