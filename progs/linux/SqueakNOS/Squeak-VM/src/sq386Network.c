#include "sq.h"

/*** TCP Socket states ***/

#define Unconnected		0x00
#define WaitingForConnection	0x01
#define Connected		0x02
#define OtherEndClosed		0x03
#define ThisEndClosed		0x04

static int thisNetSession= 0;
static int one= 1;

/*** Resolver state ***/

static int  resolverSema;


/*** asynchronous i/o support ***/

/* poll for io activity and call the appropriate handler(s) *
 *
 * Note: this can be called from ioProcessEvents with a zero timeout
 *       and from ioRelinquishProcessor with a non-zero timeout.
 *
 *	 "extraFd" is a file descriptor that is polled for reading but
 *	 never handled -- this allows a relinquished CPU to return
 *	 early if there is mouse or keyboard input activity.  Essential
 *	 for (e.g.) handling keyboard interrupts during i/o wait.
 */
void aioPollForIO(int microSeconds, int extraFd)
{
}

/* initialise asynchronous i/o handlers */
static void aioInit()
{
}

/* disable handlers and close all sockets */
static void aioShutdown()
{
}

/*** Squeak network functions ***/

/* start a new network session */
int sqNetworkInit(int resolverSemaIndex)
{
  return 0;
}

/* terminate the current network session (invalidates all open sockets) */
void sqNetworkShutdown(void)
{
}


/*** Squeak Generic Socket Functions ***/

/* create a new socket */
void sqSocketCreateNetTypeSocketTypeRecvBytesSendBytesSemaID(
        SocketPtr s, int netType, int socketType,
        int recvBufSize, int sendBufSize, int semaIndex)
{
	success(false);
}

/* return the state of a socket */
int sqSocketConnectionStatus(SocketPtr s)
{
	success(false);
	return -1;
}

/* TCP => start listening for incoming connection
 * UDP => associate the local port number with the socket.
 */
void sqSocketListenOnPort(SocketPtr s, int port)
{
	success(false);
}

/* TCP => open a connection.
 * UDP => set remote address.
 */
void sqSocketConnectToPort(SocketPtr s, int addr, int port)
{
	success(false);
}

/* close the socket */
void sqSocketCloseConnection(SocketPtr s)
{
	success(false);
}

/* close the socket without lingering */
void sqSocketAbortConnection(SocketPtr s)
{
  sqSocketCloseConnection(s);
}

/* Release the resources associated with this socket. 
   If a connection is open, abort it.*/
void sqSocketDestroy(SocketPtr s)
{
	success(false);
}

/* answer the OS error code for the last socket operation */
int sqSocketError(SocketPtr s)
{
	return success(false);
}

/* return the local IP address bound to a socket */
int sqSocketLocalAddress(SocketPtr s)
{
	return success(false);
}

/* return the peer's IP address */
int sqSocketRemoteAddress(SocketPtr s)
{
	return success(false);
}

/* return the local port number of a socket */
int sqSocketLocalPort(SocketPtr s)
{
	return success(false);
}

/* return the peer's port number */
int sqSocketRemotePort(SocketPtr s)
{
	return success(false);
}

/* answer whether the socket has data available for reading */
int sqSocketReceiveDataAvailable(SocketPtr s)
{
	return success(false);
}

/* answer whether the socket has space to receive more data */
int sqSocketSendDone(SocketPtr s)
{
	return success(false);
}

/* read data from the socket s into buf for at most bufSize bytes.
   answer the number actually read.  For UDP, fill in the peer's address
   with the approriate value. */
int sqSocketReceiveDataBufCount(SocketPtr s, int buf, int bufSize)
{
	return success(false);
}

/* write data to the socket s from buf for at most bufSize bytes.
   answer the number of bytes actually written. */ 
int sqSocketSendDataBufCount(SocketPtr s, int buf, int bufSize)
{
	return success(false);
}



/*** Resolver functions ***/

/* Note: the Mac and Win32 implementations implement asynchronous lookups
 * in the DNS.  I can't think of an easy way to do this in Unix without
 * going totally ott with threads or somesuch.  If anyone knows differently,
 * please tell me about it. - Ian
 */

/*** irrelevancies ***/

void sqResolverAbort(void) {}

void sqResolverStartAddrLookup(int address)
{
	success(false);
}

int sqResolverStatus(void)
{
	return success(false);
}

/*** trivialities ***/

int sqResolverAddrLookupResultSize(void)	{ return 0; }
int sqResolverError(void)			{ return 0; }
int sqResolverLocalAddress(void)		{ return 0; }
int sqResolverNameLookupResult(void)		{ return 0; }

void sqResolverAddrLookupResult(char *nameForAddress, int nameSize)
{
	success(false);
}

/*** name resolution ***/

void sqResolverStartNameLookup(char *hostName, int nameSize)
{
	success(false);
}
