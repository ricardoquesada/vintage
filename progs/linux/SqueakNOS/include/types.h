/*	$Id: types.h,v 1.1 2000/03/24 20:39:39 gera Exp $	*/
#ifndef __TYPES_H__
#define __TYPES_H__

typedef unsigned long uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;

#endif
