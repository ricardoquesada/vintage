/*	$Id: asm.h,v 1.2 2000/07/17 20:07:59 gera Exp $	*/
#include "types.h"

#ifndef __ASM_H__
#define __ASM_H__

// #define cli()	asm("cli")
// #define sti()	asm("sti")

void inline lgdt(uint32 offset,uint32 size);
void inline lidt(uint32 offset,uint32 size);

#endif
