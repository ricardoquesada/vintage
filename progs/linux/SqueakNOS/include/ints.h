/*	$Id: ints.h,v 1.2 2000/07/17 20:07:59 gera Exp $	*/
#include "types.h"

extern unsigned long timer;
extern unsigned char lastkey;

void InitInts();
void setIDT(uint32 *IDT,unsigned int intNum,void *ISR);
void nullIRQ();
void nullISR();
void clockISR();
void keyboardISR();
