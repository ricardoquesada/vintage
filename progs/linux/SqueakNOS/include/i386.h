/*	$Id: i386.h,v 1.1 2000/03/24 20:39:39 gera Exp $	*/
struct _DescriptorTablePtr {
	u_int16_t	size;		// Descriptor table size
	u_int32_t	address;	// Descriptor table 32bit address
};

struct _Descriptor {
	u_int16_t	size;		// Size of segment (fragmentation)
	u_int16_t	addr0_15;	// Address' bits
	u_int8_t	addr16_23;	// Address' bits
	u_int8_t	access;		// Access
	u_int8_t	granularity;	// Segment's size granularity
	u_int8_t	addr24_31;	// Address' bits
};

struct _TSS { // Task
	u_int32_t	BackLink;
	u_int32_t	ESP0;
	u_int32_t	SS0;
	u_int32_t	misc[4];
	u_int32_t	CR3;
	u_int32_t	EIP;
	u_int32_t	EFL;
	u_int32_t	EAX;
	u_int32_t	ECX;
	u_int32_t	EDX;
	u_int32_t	EBX;
	u_int32_t	ESP;
	u_int32_t	EBP;
	u_int32_t	ESI;
	u_int32_t	EDI;
	u_int32_t	ES;
	u_int32_t	CS;
	u_int32_t	SS;
	u_int32_t	DS;
	u_int32_t	FS;
	u_int32_t	GS;
	u_int32_t	LDT;
	u_int16_t	DebugTrap;
	u_int16_t	MapBase;
};
