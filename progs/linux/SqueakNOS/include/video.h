/*	$Id: video.h,v 1.3 2000/07/18 03:08:36 riq Exp $	*/
#ifndef __VIDEO_H__
#define __VIDEO_H__

#ifdef LINUX_BOOT
#define __KERNEL__
#include <linux/tty.h>
#endif

#define SCREEN_INFO (*(struct screen_info *)0x90000)

extern char* vidmem;
extern int vidport;
extern int lines, cols;

#define putchar putc
int initVideo();

void scroll();
void puts(char *s);
void putx(int n);
void putc(const char c);
#endif
