/*	$Id: memory.h,v 1.1 2000/03/24 20:39:39 gera Exp $	*/

void* memset(void* s, int c, int n);
void* memcpy(void* __dest, __const void* __src, int __n);
void *malloc(int size);
void free(void *where);
