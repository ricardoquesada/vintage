/*
 * set16_xy v0.2
 * riq.
 * 
 * Esto es una implementacion de 'setxy( x, y, c)' para fbcon_vga16
 * para swain y aweil.
 * 
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <linux/fb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/io.h>

#include "logo.h"
#include "f.h"

/* set_xy es un wrapper, se puede obviar */
void set_xy( int x, int y, unsigned char c)
{
	set16_xy(x,y,c);
}

void draw_logo( void )
{

	int y,x;
	unsigned char c=0;
	int i=0;

	for(y=0;y<80;y++) {
		for(x=0;x<40;x++) {
			c = linux_logo16[i++];
			c >>= 3;
			set_xy(x+100,y+100,c);
		}
	}
}

void draw_diag( void )
{
	int x;
	for(x=0;x<480;x++) {
		set_xy(x,x,1);
		set_xy(x,480-x,2);
	}
}

void draw_rectxy( int x, int y, int xl, int yl, int c )
{
	int i;

	for(i=x;i<=x+xl;i++) {
		set_xy(i,y,c);
		set_xy(i,y+yl,c);
	}
	for(i=y;i<=y+yl;i++) {
		set_xy(x,i,c);
		set_xy(x+xl,i,c);
	}
}
void draw_rect( void )
{
	int i;

	for(i=0;i<40;i++) {
		draw_rectxy( 400,200,50-i,50-i,i);
		usleep(1);
	}
}


int main( int argc, char **argv )
{
	int fd;

	fd = init_set16();
	if(fd==-1) {
		printf("Error!\n");
		exit(-1);
	}

	draw_logo();
	draw_diag();
	draw_rect();

	close_set16( fd );
	return 0;
}
