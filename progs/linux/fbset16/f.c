/*
 * set16_xy v0.2
 * riq.
 * 
 * Esto es una implementacion de 'setxy( x, y, c)' para fbcon_vga16
 * para swain y aweil.
 * 
 */

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <asm/system.h>
#include <asm/io.h>
#include <sys/ioctl.h>
#include <sys/io.h>

#include "o.h"

#define GRAPHICS_ADDR_REG 0x3ce		/* Graphics address register. */
#define GRAPHICS_DATA_REG 0x3cf		/* Graphics data register. */

#define SET_RESET_INDEX 0		/* Set/Reset Register index. */
#define ENABLE_SET_RESET_INDEX 1	/* Enable Set/Reset Register index. */
#define DATA_ROTATE_INDEX 3		/* Data Rotate Register index. */
#define GRAPHICS_MODE_INDEX 5		/* Graphics Mode Register index. */
#define BIT_MASK_INDEX 8		/* Bit Mask Register index. */


struct fb_fix_screeninfo fb_fix;
struct fb_var_screeninfo fb_var;
unsigned char* fb_mem;
volatile unsigned char *m_mmioaddr;

/* Set the Graphics Mode Register.  Bits 0-1 are write mode, bit 3 is
   read mode. */
static inline void setmode(int mode)
{
	outb(GRAPHICS_MODE_INDEX, GRAPHICS_ADDR_REG);
	outb(mode, GRAPHICS_DATA_REG);
}

/* Set the value of the Bit Mask Register.  It must already have been
   selected with selectmask(). */
static inline void setmask(int mask)
{
	outb(BIT_MASK_INDEX, GRAPHICS_ADDR_REG);
	outb(mask, GRAPHICS_DATA_REG);
}

/* Set the Data Rotate Register.  Bits 0-2 are rotate count, bits 3-4
   are logical operation (0=NOP, 1=AND, 2=OR, 3=XOR). */
static inline void setop(int op)
{
	outb(DATA_ROTATE_INDEX, GRAPHICS_ADDR_REG);
	outb(op, GRAPHICS_DATA_REG);
}

/* Set the Enable Set/Reset Register.  The code here always uses value
   0xf for this register.  */
static inline void setsr(int sr)
{
	outb(ENABLE_SET_RESET_INDEX, GRAPHICS_ADDR_REG);
	outb(sr, GRAPHICS_DATA_REG);
}

/* Set the Set/Reset Register. */
static inline void setcolor(int color)
{
	outb(SET_RESET_INDEX, GRAPHICS_ADDR_REG);
	outb(color, GRAPHICS_DATA_REG);
}

/* Set the value in the Graphics Address Register. */
static inline void setindex(int index)
{
	outb(index, GRAPHICS_ADDR_REG);
}

void set16_xy( int xx, int yy , int c )
{
	unsigned char *m;
	unsigned char bitmask=1;
	unsigned char t=0;

	t = (xx&7);
	t ^= 7;

	bitmask <<= t;

	m = fb_mem + (xx/8) + yy * (fb_var.xres/8);

	t = *m ;		/* load latch register */
	setmask(bitmask);
	setmode(2);
	setop(0);
	*m = c;
}

int init_ports(void)
{
	return (ioperm(0x3b4, 0x3df - 0x3b4 + 1 , 1 ));
}

int init_fb( void )
{
	int fd;

	fd = open("/dev/fb0", O_RDWR);
	if (fd<0) 
		return -1;

	// fetch initial settings
	ioctl( fd, FBIOGET_FSCREENINFO, &fb_fix );
	ioctl( fd, FBIOGET_VSCREENINFO, &fb_var );

	if(fb_fix.type != FB_TYPE_VGA_PLANES)
		return -1;

	/* map framebuffer and get the address */
	if ( MAP_FAILED == (fb_mem = (unsigned char *) mmap(	NULL, 
						fb_fix.smem_len, 
						PROT_READ | PROT_WRITE, 
						MAP_SHARED,
						fd, 0 )) )
	{
		return -1;
	}
	
	m_mmioaddr = (unsigned char *) mmap(NULL, fb_fix.mmio_len,
					PROT_READ | PROT_WRITE, MAP_SHARED,
					fd, fb_fix.smem_len);
	m_mmioaddr = NULL;

	return fd;
}

int init_set16( void )
{
	int fd;

	fd = init_ports();

	if(fd==-1)
		return fd;

	fd = init_fb();

	return fd;
}

void close_set16( int fd)
{
	close( fd );
}

