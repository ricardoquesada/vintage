// ==== Standard POV-Ray Includes ====
#include "colors.inc"	// Standard Color definitions
#include "textures.inc"	// Standard Texture definitions
#version 3

camera
{
  location  <0.0 , 3.0 ,-6.0>
look_at   <0.0 , 0.0 , 0.0>
}

light_source{<-100,200,-100> color White}
//plane{<0,1,0>,-10 pigment{color rgb<.3,.2,.3>} finish{Dull}}
 
#declare C = union{
cylinder{<3,0,0>,<-3,0,0>,.5 pigment {Blue} finish{Metal}}

torus
{  0.55,  0.1
 texture{Gold_Metal} rotate<0,0,90> translate<-3,0,0>}

torus
{  0.2,  0.1
 texture{Gold_Metal} rotate<0,0,90> translate<-3.81,0,0>}


disc{
  <-3.82, 0, 0> // center position
  x,         // normal vector
  0.2,       // outer radius
  0.0        // optional hole radius
}

cone{<-3,0,0>,.45,<-3.8,0,0>,.25 texture{Gold_Metal}}
cylinder{<-2,0,0>,<-3,0,0>,.6 texture{Gold_Metal}}
//finishes

torus
{  0.6,  0.02
 texture{Gold_Metal} rotate<0,0,90> translate<-2,0,0>}

torus
{  0.6,  0.02
 texture{Gold_Metal} rotate<0,0,90> translate<-2.9,0,0>}

torus
{  0.2,  0.5
 texture{Gold_Metal} rotate<0,0,90> translate<-2.5,0,0>}

// cone
quartic {
  < 1.0,  0.0,  0.0,   0.0,    0.0,  0.0,  0.0,  0.0,  0.0,   0.0,
    0.0,  0.0,  0.0,   0.0,    0.0,  0.0,  0.0,  0.0,  0.0,   0.0,
    1.0,  0.0,  0.0,   0.0,    0.0,  0.0,  0.0,  0.0,  0.0,   0.0,
    1.0,  0.0,  0.0,   0.0, -1000.0 >

scale .12
   texture {Gold_Metal}
   translate <3.5,0,0>}

torus
{  0.5,  0.15
 texture{Gold_Metal} rotate<0,0,0> translate<3.5,.62,0>}


torus
{  0.25,  0.4
 texture{Gold_Metal} rotate<0,0,0> translate<3.5,1,0>}

cone{<3.5,1,0>,.1,<3.5,2.4,0>,.8 open  texture{Gold_Metal}}


torus
{  0.81,  0.1
 texture{Gold_Metal} rotate<0,0,0> translate<3.5,2.4,0>}
 sphere { 0, 1
    pigment { color rgbt <1, 1, 1, 1> }
    /*halo {           //This halo only works with Pov V3
      emitting
      spherical_mapping
      linear
      
      color_map {
        [ 0 color rgbt <1, 0, 0, 1> ]
        [ 1 color rgbt <1, 1, 0, -1> ]
      }
      samples 10
      frequency 2
      samples 20
      
 }    */
    hollow 
  translate<3.5,2.5,0>} 

} 
 

#declare ME =


texture{
  pigment  {
    marble // some pattern
    color_map {[0.1 color red 1] [0.7 color rgb 1]}
    turbulence 1
    scale <1,1,1> rotate<0,90,0>// transformations
  }
normal  {
    marble // some pattern
    turbulence 0.5
    scale <1,3,1>// transformations
  }
 finish
  {
    ambient 0.2
    specular 0.6 // shiny
  }
}
#declare Eye = union{
sphere{<0,0,0>,.3  texture{ME}}
sphere{<0,0,-.105>,.2  pigment{Black} finish{Glossy}}}

#declare Me = union{
sphere{<0,0,0>,1 pigment{Yellow} finish{phong 1}normal {bumps .4 scale .1}}
object{Eye translate<-.2,1.2,0>}
object{Eye rotate<0,0,180> translate<.2,1.2,0>}

// legs + arms
cylinder{<0,0,0>,<1.75,1,0>,.1 pigment{Red}}
cylinder{<0,0,0>,<-1.75,1,0>,.1 pigment{Red}}
cylinder{<0,0,0>,<.75,-2,0>,.1 pigment{Red}}
cylinder{<0,0,0>,<-.75,-2,0>,.1 pigment{Red}}

sphere{<1.75,1,0>,.2 pigment{Yellow} finish{phong 1}}
sphere{<-1.75,1,0>,.2 pigment{Yellow} finish{phong 1}}
sphere{<.75,-2,0>,.2 pigment{Yellow} finish{phong 1}}
sphere{<-.75,-2,0>,.2 pigment{Yellow} finish{phong 1}}

}

//background
sphere{<0,0,0>,1000 	pigment { bozo

			color_map{
			[.1 color Red]
			[.4 color Blue]
		   [.6 color Green]
			[.9 color Red] } scale 300} finish{ambient 1}}

union{
cylinder{<0,-2.2,0>,<0,-2.5,0>,1.5 texture{Polished_Chrome}}

object{Me rotate<0,90,0>}

object{C scale .5 rotate<0,180,0> translate<-3,0,0>}

rotate<0,-30,0> translate<2,0,0>}

