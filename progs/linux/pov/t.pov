// Persistence Of Vision raytracer version 3.0 sample file.
// Used by pigment/normal pattern example scenes

#version 3.0
global_settings { assumed_gamma 2.2 }

#include "colors.inc"

camera {
  location <0,1,-14>
  direction 3*z
}

plane {
  y, -1.01
  pigment {checker White, Magenta}
}

plane {
  z, 3.01
  pigment {checker White, Magenta}
}

light_source { <300, 500, -500> color Gray65}
light_source { <-50,  10, -500> color Gray65}

sphere {0,1
   pigment{
     bozo color_map{[0.0 Red][1.0 Green]}
     scale 0.12
   }
  translate <-2, 2, 0>
}

sphere {0,1
   pigment{
     bozo color_map{[0.2 Yellow][0.3 Blue][0.5 White]}
     scale 0.35
   }
  translate -2*x
}

cylinder{
  -z,z,1 
  scale .75
  pigment{Blue}
  normal{
     bozo 0.3
     scale 0.25
   }
   finish{phong 0.8 phong_size 50}

  rotate <-30,30,0>
  translate 2*y
}

cylinder{
  -z,z,1 
  scale .35
  pigment {Red}
  rotate <-30,30,0>
}

box{<-1,-1,-1>,<1,1,1>
  scale .75
   pigment{Orange}
   normal{
     bozo 0.1
     scale 0.5
   }
//   finish{
//     phong 0.8 phong_size 200
//   }
  rotate <-30,30,0>
  translate <2,2,0>
}

box{<-1,-1,-1>,<1,1,1>
  scale .35
  pigment {White}
  rotate <-30,30,0>
  translate 2*x
}

