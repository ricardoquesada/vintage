#include "colors.inc"

light_source { <1000,1000,-1000>, White}
  
#default
{
   finish{ambient 0.4} 
}

camera { location <3,5,-15> direction 2*z look_at <0,0,0>}
   
   
  #declare R1 = seed(154);
  #declare R2 = seed(2745);
  #declare R3 = seed(3745) ;

 
  plane{<0,1,0>,-2 pigment{SkyBlue} finish{reflection .4}  }
   
#declare Z=0;

union{
#while (Z<=360)  
  

#declare X= rand(R1) ;
#declare Y= rand(R2)  ;
#declare A= rand(R3)  ; 
  
  
sphere{<0,1,0>,.2  
    rotate<Z*12,0,Z*12>
translate<0,0,2>
rotate<0,Z,0> pigment{rgb<A,Y,X>}finish {phong 1}
    
     rotate<45,0,180>}
      
      
      
      
#declare Z=Z+1;
#end   
}

