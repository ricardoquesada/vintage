#include "colors.inc"
#include "shapes.inc"
#include "textures.inc"
#include "stones.inc"

#version 3.0

global_settings {
  assumed_gamma 2.2 // for most PC monitors
  max_trace_level 5
}

sphere { <1, 0, -6>, 0.5
  finish {
    ambient 0.1
    diffuse 0.6
  }
  pigment { NeonPink }
}

box { <-1, -1, -1>, < 1,  1,  1>
  rotate <0, -20, 0>
  finish {
    ambient 0.1
    diffuse 0.6
  }
//  texture { T_Stone25 scale 4 }
  pigment { Green }
}

cone {
  <-2, -1, -2>, 2.0    // Center and radius of one end
  <-2, 1, -2>, 0.4    // Center and radius of other end

  pigment { Orange }

//  texture { T_Stone25 scale 4 }
}


cylinder { <-6, 6, 30>, <-6, -1, 30>, 3
  finish {
    ambient 0.1
    diffuse 0.6
  }
  pigment {NeonBlue}
}

plane { <0, 1, 0>, -1
  pigment {
    checker color Red, color Blue
  }
}




light_source { <5, 30, -30> color White }

light_source { <-5, 30, -30> color White }


camera {
  location <0.0, 1.0, -10.0>
  look_at  <0.0, 1.0,  0.0>

//  focal_point <-6, 1, 30>    // blue cylinder in focus
//  focal_point < 0, 1,  0>    // green box in focus
  focal_point < 1, 1, -6>    // pink sphere in focus

  aperture 0.4     // a nice compromise
//  aperture 0.05    // almost everything is in focus
//  aperture 1.5     // much blurring

//  blur_samples 4       // fewer samples, faster to render
  blur_samples 20      // more samples, higher quality image
}
