#include "colors.inc"

camera {
  angle 15
  location <0,2,-10>
  look_at <0,0,0>
}

light_source { <10, 20, -10> color White }

blob {
  threshold .65
  sphere { <.5,0,0>, .8, 1 pigment {Blue} }
  sphere { <-.5,0,0>,.8, 0.7 pigment {Pink} }

  sphere { <0,.5,0>,.8, 0.5 pigment {Green} }
  sphere { <0,-.5,0>,.8, 0.3 pigment {Yellow} }

  finish { phong 1 }
}
//  sphere { <.5,0,0>, .8 pigment { Yellow transmit .75 } }
//  sphere { <-.5,0,0>, .8 pigment { Green transmit .75 } }
