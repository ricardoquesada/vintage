//
// Mi primer dibujo en con POV
// Una cara
//
#include "colors.inc"
#include "shapes.inc"
#include "textures.inc"
#include "stones.inc"

#version 3.0

global_settings {
  assumed_gamma 2.2 // for most PC monitors
  max_trace_level 5
}

#declare Ojo = union{
sphere{<0,0,0>,.3  pigment{White} scale x*1.0}
sphere{<0,0,-.105>,.2  pigment{Black} finish{Glossy}}}


union {
   	sphere { <0, 0, 0>, 2.0  pigment {Flesh} }	// cara
   	sphere { <0, -2, 0>, 1  pigment {Flesh} }	// cara

   	sphere { <0, 0, -2.0>, 0.3 pigment {Flesh} } 	// nariz
	object{Ojo translate<0.6,0.5,-1.6>}
	object{Ojo translate<-0.6,0.5,-1.6>}
} // cara



light_source { <-5, 30, -30> color White }
light_source { <5, 30, -30> color White }


camera {
  location <0.0, 0.0, -10.0>
  look_at  <0.0, 0.0,  0.0>
}
