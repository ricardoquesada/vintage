/*
 * Original Author: Luke Stras <stras@ecf.toronto.edu>
 *
 * Revision History
 * 1999/09/22 stras Initial version
 * 1999/09/25 JHS   re-use for PPPOE discovery 
 * 1999/09/26 JHS   retransmission for PADI and PADR
 * 1999/10/11 ireid@sympatico.ca   -I fix
 * 1999/10/22  jhs AC cookie echo, -F, -R, -P and help extensions
 * 1999/10/23  small fix by Josef Drexler to create_padr(); 
 *              doesnt make sense and doesnt work if removed 
 *              (will figure it out later). Looks like alignm issue
 *              Fixed: Stupid mistake on my part (much cleaner now).
 * 1999/10/27   Woody Suwalski <woody@rebel.com> ARM requires packed
 *              structs.
 * 1999/10/29   Andi Kleen <ak@suse.de> Port to new ioctl interface,
 *              Fix Endian bugs.
 *
 * 1999/11/20  jhs: -P option
 * 1999/11/30  Marc Boucher <marc@mbsi.ca> sigchild and misc cleanup, typos etc
 * 1999/12/04  jhs: AC and service name reflection + overriding (-A, -E)
 *              major rewrite of the PADR creation. Much cleaner now.
 * 1999/12/09  jhs: PADI override of Sname;
 * 1999/12/13  Marc Boucher <marc@mbsi.ca>: support for daemonization, syslog,
 *             runtime debugging; include cleanup, ARM fixes, etc.
 * 2000/01/10  Perry Wintner <perryw@3no.com>, Service Name Tag miscopy
 *             
 */

#include "pppoed.h"



void
sigproc (int src)
{
  int i;
/*
  info ("Received signal %d", src);
*/
  for (i = 0; i < 16; i++) {
    if (ses_table[i].pid > 0) {
      poe_dbglog (&ses_table[i],"Sending signal %d to pid %d", src, ses_table[i].pid);
      kill (ses_table[i].pid, src);
    }
  }
  poe_die (1);
}


void
sigchild (int src)
{
  pid_t pid;
  int status;
  int i;
  pid = waitpid (-1, &status, WNOHANG);
/*
  poe_info ("Child received signal %d PID %d, status %d", src, pid, status);
*/
  if (pid < 1) {
    return;
  }
/* scan */
  for (i = 0; i < 16; i++) {
    if (ses_table[i].pid == pid) {
      poe_info (&ses_table[i],"Line id which died is: %d", i);
      ses_table[i].state = DISCONNECTED;
      break;
    }

  }

}


void
print_help ()
{
  
  fprintf (stdout,"pppoe version %d.%d build %d", VERSION_MAJOR, VERSION_MINOR,
	  VERSION_DATE);
  fprintf (stdout,"recognized options are:");
  fprintf (stdout," -I <interface> : overrides the default interface of eth0");
  fprintf (stdout," -L <pppox_num> : overrides the default lineid of pppox0. Used to");
  fprintf (stdout,"                  have multiple sessions");
  fprintf (stdout," -S : starts pppoed in server mode");
  fprintf (stdout," -R <num_retries>: forces pppoed to be restarted num_retries");
  fprintf (stdout,"                   should the other end be detected to be dead.");
  fprintf (stdout,"                   Needs lcp_echo. Read the INSTALL file instructions");
  fprintf (stdout," -F <filename> : specifies additional ppp options file");
  fprintf (stdout," -C <filename> : ppp options file in /etc/ppp/peers/");
  fprintf (stdout," -d <level> : sets debug level");
  fprintf (stdout," -D : prevents pppoed from detaching itself and running in the background");
  fprintf (stdout," -P <path to pppd> : selects a different pppd. Defaults to " _PATH_PPPD);
  fprintf (stdout," -A <AC name> to select a specific AC by name");
  fprintf (stdout," -E <AC service name> to select a specific AC service by name");
}


int 
get_args (int argc, char **argv,struct session *sess)
{
  struct filter *filt;
  struct host_tag *tg;
  int opt;
  char htag[256];
/* the number of PADI and PADR retransmits.
   We should be able to use the command line options to pass these instead
 */
  sess->PADR_ret = 5;
  sess->PADI_ret = 5;

  sess->np=0;
  sess->opt_debug = 0;
  DEB_DISC=0;
  DEB_DISC2=0;
  sess->opt_daemonize = 1;
/*
   log_to_fd = -1;
 */

  sess->log_to_fd = fileno (stdout);

/* defaults to eth0 */
  strcpy (sess->name, "eth0");


  if ((sess->filt=malloc(sizeof(struct filter))) == NULL) {
        poe_error (sess,"failed to malloc for Filter ");
        poe_die (-1);
      }

  filt=sess->filt;  /* makes the code more readable */

/* set default filters; move this to routine */
  /* parse options */

  while ((opt = getopt (argc, argv, "A:C:E:d:DR:I:F:L:V:P:S:N:G")) != -1)

    switch (opt) {
    case 'R':			/* sets number of retries */
      filt->num_restart = strtol (optarg, (char **) NULL, 10);
      filt->num_restart += 1;
      break;
    case 'I':			/* sets interface */
      if (strlen (optarg) >= IFNAMSIZ) {
	poe_error (sess,"interface name cannot exceed %d characters", IFNAMSIZ - 1);
	return (-1);
      }
      strncpy (sess->name, optarg, sizeof(sess->name));
      break;
    case 'C':			/* name of the file in /etc/ppp/peers */
       if (strlen(filt->fname)) {
       poe_error (sess,"-F can not be used with -C");
       return (-1);
      }
      if (strlen(optarg) > MAX_FNAME) {
        poe_error (sess,"file name cannot exceed %d characters", MAX_FNAME - 1);
        return (-1);
      }
      strncpy (filt->fname, optarg, sizeof(filt->fname));
      filt->peermode=1;
      break;
    case 'F':			/* sets the options file */
       if (strlen(filt->fname)) {
       poe_error (sess,"-F can not be used with -C");
       return (-1);
      }

      if (strlen(optarg) > MAX_FNAME) {
        poe_error (sess,"file name cannot exceed %d characters", MAX_FNAME - 1);
        return (-1);
      }
      strncpy (filt->fname, optarg, sizeof(filt->fname));
      break;
    case 'D':			/* don't daemonize */
      sess->opt_daemonize = 0;
      break;
    case 'd':			/* debug level */
      sess->opt_debug = strtol (optarg, (char **) NULL, 10);
      if (sess->opt_debug & 0x0002)
	DEB_DISC=1;
      if (sess->opt_debug & 0x0004)
	DEB_DISC2=1;
      break;
    case 'P':			/* sets the pppd binary */
      if (strlen(optarg) > MAX_FNAME) {
        poe_error (sess,"pppd binary cant exceed  %d characters", MAX_FNAME - 1);
        return (-1);
      }
      strncpy (filt->pppd, optarg, sizeof(filt->pppd));
      break;
    case 'G':			/* lineid */
      sess->np = 1;
      break;
    case 'L':			/* lineid */
      sess->line = strtol (optarg, (char **) NULL, 10);
      poe_info (sess,"lineid set to %s %d", optarg, sess->line);
      break;
    case 'V':			/* version */
      fprintf (stdout,"pppoe version %d.%d build %d", VERSION_MAJOR,
	      VERSION_MINOR, VERSION_DATE);
      return (0);
    case 'S':			/* server mode */
      poe_error (sess,"pppoe server mode not supported in this version");
      return (0);
    case 'A':			/* server mode */
      poe_info (sess,"AC name override to %s", optarg);
      if ((filt->ntag = malloc (sizeof (struct pppoe_tag) + strlen (optarg)))
	  == NULL) {
	poe_error (sess,"failed to malloc for AC name: %m");
	return (-1);
      }
      filt->ntag->length = htons (strlen (optarg));
      filt->ntag->type = htons (TAG_AC_NAME);
      strcpy ((char *) filt->ntag + sizeof (struct pppoe_tag), optarg);
      break;
    case 'E':			/* server mode */
      poe_info (sess,"AC service name override to %s", optarg);
      if ((filt->stag = malloc (sizeof (struct pppoe_tag) + strlen (optarg)))
	  == NULL) {
	poe_error (sess,"failed to malloc for service name: %m");
	return (-1);
      }
      filt->stag->length = htons (strlen (optarg));
      filt->stag->type = htons (TAG_SERVICE_NAME);
      strcpy ((char *) (filt->stag + sizeof (struct pppoe_tag)), optarg);
      break;
    default:
      poe_error (sess,"Unknown option '%c'", optopt);
      print_help ();
      return (-1);
    }

/* for this release make hostuniq mandatory to catch buggy ACs out
there */
     if ((filt->htag = malloc (sizeof (struct pppoe_tag) + strlen (htag)))
	  == NULL) {
        poe_error (sess,"failed to malloc for hostuniq tag: %m");
	return (-1);
      }
       tg=(struct host_tag *)filt->htag;
       filt->htag->length = htons (sizeof (unsigned int));
       filt->htag->type = htons (TAG_HOST_UNIQ);
       tg->id=getpid();

  return (1);

}

int
main (int argc, char **argv)
{

/* 
only worry about a single ppp_connection in this version
Multi-pppoe is achieved by starting multiple sessions.
Please dont send me any patches on this. Its part of
the restructuring plans later. It works just fine.
*/
  struct session *sess = &ses_table[0];
  memset(sess,0,sizeof(struct session));

  if ((get_args (argc,(char **) argv,sess)) <1)
	poe_die(-1);

  if (!sess->np) {
	poe_create_pidfile (sess);
  }

  openlog ("pppoed", LOG_PID | LOG_NDELAY, LOG_PPPOE);
  setlogmask (LOG_UPTO (sess->opt_debug ? LOG_DEBUG : LOG_INFO));

  if (!sess->np && sess->opt_daemonize) {
    poe_detach (sess);
    if (!sess->detached)
	poe_error(sess,"failed to detach!");
  }

  if (!sess->np) {
    signal (SIGINT, &sigproc);
    signal (SIGTERM, &sigproc);
    signal (SIGCHLD, &sigchild);
  }

/* ppp_connect never comes back */
  ppp_connect (sess);

  poe_info(sess,"ppp_connect came back!");

 exit(0);
}
