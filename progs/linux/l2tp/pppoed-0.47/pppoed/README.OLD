27/sep/99

So why another PPPOE solution? This is what open source is all
about! Look at the variants of CD players out there ;->

I was motivated (mostly by Matthew Darwin) to work on this code
because the available solution then was a user space program which
consumed a lot of CPU cycles. However, the major driver for me 
was that the available code was not GPL. Now, if only the original
version was GPLed (but that's another discussion).

I wrote the first few lines back in August, but as in most free
software projects my spare time was pre-empted by other higher 
priorities. A while later a solution appeared from Michal Ostrowski
<mostrows@styx.uwaterloo.ca> 
Initially i was glad that someone has finally released a GPLed
solution and i didnt have to be _the_ fireman; however, a closer 
examination of the code got me a little concerned. The code is 
very well written and of really worthy quality, *BUT* i dont like 
the architectural point of view it takes.

- I dont think sockets should have been used for something like this:

	* Dedicating a whole socket family for only _one app_
	(in this case PPPOE) is really against the philosophy of
	re-use. A good use of the socket interface is when a lot of apps
	use it. Look at all those client server apps that use 
	TCP or UDP sockets. Look at the potential of all ATM apps
	using the ATMPVC sockets (one of these days,of course ;->). 
	Thats the way it should be, IMHO.

	* Cluttering and fattening the kernel with all that unnecessary
	code (which results from the sockets solution) should be a NO_NO.
	I have taken a minimalist approach instead. I tried very hard not
	touch the PPP code. In the future when the PPP over X driver
	(which currently only implements PPP over Ethernet)
	becomes more generic parts of it should be moved into the PPP code
        so other protocols (L2TP, etc) can use them.
	
	* really, PPP is a link layer thing; integrating it using a 
	higher layer interface (socket) is probably not a good way(tm).
	There are other protocols which use PPP framing: ATM, SONET,
	Frame Relay etc. They are mostly Link/physical layer. I try instead
	to provide room for these protocols to re-use the code (at least
	thats the plan). Going through an extra one or two stack layers
	unneccessarily adds latency.

- Discovery should not be in the kernel.
	Even ARP should be moved out of the kernel, literally. All protocol
	control, unless very heavily used should be done in user space.
	In the case of PPPOE, discovery is only used for connection
	setup and teardown. In the current view, there might only be
	one ppp session being set. I have left it open to have many 
	(which off hand i think is also what the other two solutions did).
	Control aspects (such as discovery in PPPOE) tend to be very 
        feature rich and dynamically changing (when someone comes up with
	smarter ways of doing things: For example, i could easily see
	PPPOE discovery being extended to get details on gateways, DNS,
	etc just like DHCP does). As a result: putting control protocols in
	the kernel ends up cluttering it with a lot of code which needs 
	updates as newer features are added.
	You can keep adding features in user space instead of waiting
	for new kernels to come out.
	Morever, discovery in PPPOE is really a client-server application.
	These kind of apps should sit in user space.


I decided to start with kernel 2.2 ( i include untested code for 2.0.38
and 2.3.18. Iam pretty sure that the 2.3.18 works out of the box, but the
2.0.38 might require some changes to PPP) because this is where the gap 
seems to be at the moment. My really tested version on the sympatico
setup was on a 2.2.10 kernel but the 2.2.12 is essentially the same.

UPDATE: October 16/99 -- Includes a tested version for 2.3.21
I have no plans of getting the 2.0 working (although it is doable)
mostly for lack of time and feel that not many people are still using
2.0 kernels. At some point i'll hack on it. I might prioritize it if
someone will give me a very good reason.

This code should be portable to other unixes (such as the BSDs).
If you port it please drop me a note.

I have cannibalized another GPL'ed user space PPPOE implementation 
from Luke Stras <stras@ecf.toronto.edu>. This is where the user space 
PPPOEd program included originates from (and in particular the discovery 
protocol code). I actually like Luke's solution, but it runs on user space.

Acknowledgements
----------------

1) Matthew Darwin <matthew@davin.ottawa.on.ca> for the primary motivation
and for access to his sympatico setup for my first test.
2) Don Bennet for access to his sympatico setup for my second test.
3) Luke Stras <stras@ecf.toronto.edu> for the discovery code.

Hopefully, i should be able to get my connection from sympatico
by the 6th of october so i can fix any bugs discovered etc.

Jamal Hadi Salim,
hadi@cyberus.ca
