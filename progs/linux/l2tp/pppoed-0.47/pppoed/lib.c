#include "pppoed.h"

int
create_raw_socket (struct session *ses,unsigned short type)
{
  int optval = 1, rv;

  if ((rv = socket (PF_INET, SOCK_PACKET, htons (type))) < 0) {
    poe_error (ses,"socket: %m");
    return -1;
  }

  if (setsockopt (rv, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) < 0) {
    poe_error (ses,"setsockopt: %m");
    return -1;
  }

  return rv;
}

int
get_hw_addr (struct session *ses,int s) 
{
  struct ifreq ifr;

  strncpy (ifr.ifr_name, ses->name, sizeof (ifr.ifr_name));

  if (ioctl (s, SIOCGIFHWADDR, &ifr) < 0) {
    poe_error (ses,"ioctl(SIOCGIFHWADDR): %m");
    return -1;
  }

  if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
    poe_error (ses,"interface %s is not Ethernet!", ses->name);
    return -1;
  }

  memcpy (ses->smac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

/*
   poe_dbglog("<%s> we received hware address %E",ses->name,hw_addr);

 */
  return 0;
}


int
open_device (struct session *ses)
{
  int fd;
  char fn[30];
  sprintf (fn, "/dev/pppox%d", ses->line);
  fd = open (fn, O_RDWR);
  if (fd < 0) {
    poe_error (ses,"cannot open %s: %m", fn);
    poe_die (1);
  }
  return fd;
}

#define err(x)  error(x),die(1)

int inet_sk = -1;

void
open_inet_sk (struct session *ses)
{
  if (inet_sk != -1)
    return;
  inet_sk = socket (PF_INET, SOCK_DGRAM, 0);
  if (inet_sk < 0)
    poe_error (ses,"inet socket: %m");
}

int
ifindex (struct session *ses)
{
  struct ifreq ifr;
  memset (&ifr, 0, sizeof ifr);
  strncpy (ifr.ifr_name, ses->name, sizeof (ifr.ifr_name));
  if (ioctl (inet_sk, SIOCGIFINDEX, &ifr) < 0) {
    poe_error (ses,"error getting device %s: %m", ses->name);
    poe_die (1);
  }
  return ifr.ifr_ifindex;
}

int
cntrl (struct session *ses)
{
  struct poe_arg poe;

  open_inet_sk (ses);

  memset (&poe, 0, sizeof poe);
  poe.poe_version = POE_VERSION;

  /* Set session ID */
  poe_dbglog (ses,"cntrl: SessionID (%x)", ses->sid);
  /* sid is already in network order; we didnt bother converting 
     earlier */
  poe.poe_session_id = ses->sid;
  if (ioctl (ctrl_fd, PPPOEIOCSID, &poe) < 0)
    poe_error (ses,"PPPOECSID: %m");

  /* Set device */
  poe.poe_addr.sll_ifindex = ifindex (ses);
  poe.poe_addr.sll_hatype = ARPHRD_ETHER;
  poe.poe_addr.sll_halen = 6;
  poe.poe_addr.sll_family = 0;	/* XXX */
  /* should set protocol */
  if (ioctl (ctrl_fd, PPPOEIOCSDEV, &poe) < 0)
    poe_error (ses,"PPPOECSDEV: %m");

  /* Destination MAC */
  memcpy (poe.poe_addr.sll_addr, ses->dmac, 6);
  if (ioctl (ctrl_fd, PPPOEIOCDMAC, &poe) < 0)
    poe_error (ses,"PPPOEIODID: %m");

  return 0;
}


int
init_ses (struct session *ses)
{
/* do error checks here; session name etc are valid */
  if ((disc_sock = create_raw_socket (ses,ETH_P_PPPOE_DISC)) < 0) {
    poe_error (ses,"unable to create raw socket");
    return 1;
  }

  if (get_hw_addr (ses,disc_sock) != 0) {
    poe_error (ses,"unable to get hardware address");
    return -1;
  }
  return 1;
}

void
cleanup_ses (struct session *ses)
{
  close (disc_sock);
}

/*  TODO: cleanup the filtering */
int filter_tags (struct session *ses)
{
/* the filter values are always overriding if we have them */
  if (NULL == ses->filt->stag) {
     ses->filt->stag=find_tag (ses,ses->tags_len,ses->tags,TAG_SERVICE_NAME); 
  }
  if (NULL == ses->filt->ntag) {
     ses->filt->ntag=find_tag (ses,ses->tags_len,ses->tags, TAG_AC_NAME); 
  }
  if (NULL == ses->filt->ctag) {
     ses->filt->ctag=find_tag (ses,ses->tags_len,ses->tags, TAG_AC_COOKIE); 
  }
  /* validate the host tag if one is there; 
  TODO: Move this validation elsewhere*/

  /* NOTE: a screw up of the AC on the uniq host is not a problem
  at the moment for us -- but maybe in the future; so all we do is
  whine
  */
  if (NULL != ses->filt->htag) {
     struct pppoe_tag *ac_htag;
     ac_htag=find_tag (ses,ses->tags_len,ses->tags, TAG_HOST_UNIQ); 
     if (NULL == ac_htag) {
	    poe_error (ses,"Buggy AC: Contact your ISP please,
	    		we sent a host uniq tag but got none back: %m");
     } else { 
       struct host_tag *tgs,*tgr;
       tgs=(struct host_tag *)ses->filt->htag;
       tgr=(struct host_tag *)ac_htag;
	    /* compare if it looks like ours */
       if (tgr->id !=tgs->id) {
	    poe_error (ses,"Buggy AC: Contact your ISP please,
		we sent a host-uniqid %d and received %d",tgs->id,tgr->id);

       }
     	
     }
  }

return 0;
}


/*
* TODO:Look into collapsing some of the common code in the create_*
*/
int
create_padi (struct pppoe_packet **packet, struct session *ses)
{
  int size;
  int size2=0,size4=0;
  char *p;

  if (packet == NULL)
    return 0;

  if (*packet != NULL)
    free (*packet);

  size = sizeof (struct pppoe_packet) + sizeof (struct pppoe_tag);
  if (ses->filt->stag)
    size += ntohs (ses->filt->stag->length);
  if (ses->filt->htag){
    size += sizeof (struct pppoe_tag) + ntohs (ses->filt->htag->length);
  }
  if ((*packet = malloc (size)) == NULL) {
    poe_error (ses,"create_padi: malloc failed: %m");
    return 0;
  }

  memcpy ((*packet)->ethhdr.h_dest, MAC_BCAST_ADDR, ETH_ALEN);
  memcpy ((*packet)->ethhdr.h_source, ses->smac, ETH_ALEN);
  (*packet)->ethhdr.h_proto = htons (ETH_P_PPPOE_DISC);
  (*packet)->ver = 1;
  (*packet)->type = 1;
  (*packet)->code = CODE_PADI;
  (*packet)->session = 0;
  (*packet)->length = htons (size - sizeof (struct pppoe_packet));

  if (!ses->filt->stag) {
    /* fill out a blank service-name tag */
    (*(struct pppoe_tag *) (*packet + 1)).type = htons (TAG_SERVICE_NAME);
    (*(struct pppoe_tag *) (*packet + 1)).length = 0;
    
    size2 = sizeof (struct pppoe_tag);
    
  } else {
    memcpy ((char *) (*packet + 1), ses->filt->stag, ntohs (ses->filt->stag->length));
    size2 = sizeof (struct pppoe_tag)+ntohs (ses->filt->stag->length);
  }

  p = (char *) (*packet + 1);
  if (ses->filt->htag){
    size4 = sizeof (struct pppoe_tag) + ntohs (ses->filt->htag->length);
    p +=size2;
    memcpy ((char *) p, ses->filt->htag, size4);
  }
  return size;
}


/*
*
*/
int
create_padr (struct pppoe_packet **packet, struct session *ses)
{
  int size;
  int size1 = 0, size2 = 0, size3 = 0,size4 = 0;
  char *p;

  if (packet == NULL)
    return 0;

  if (*packet != NULL)
    free (*packet);

  if (0> filter_tags(ses)) {
    poe_error (ses,"create_padr: filter_tags failed: %m");
    return 0;
  }

  if (ses->filt->stag)
    size1 = sizeof (struct pppoe_tag) + ntohs (ses->filt->stag->length);
  if (ses->filt->ntag)
    size2 = sizeof (struct pppoe_tag) + ntohs (ses->filt->ntag->length);
  if (ses->filt->ctag)
    size3 = sizeof (struct pppoe_tag) + ntohs (ses->filt->ctag->length);
  /*
  if a host_uniq tag was requested send it
  */
  if (ses->filt->htag)
    size4 = sizeof (struct pppoe_tag) + ntohs (ses->filt->htag->length);

  size = size1 + size2 + size3;
  if ((*packet = malloc (size + sizeof (struct pppoe_packet))) == NULL) {
    poe_error (ses,"create_padr: malloc failed: %m");
    return 0;
  }

  memcpy ((*packet)->ethhdr.h_dest, ses->dmac, ETH_ALEN);
  memcpy ((*packet)->ethhdr.h_source, ses->smac, ETH_ALEN);
  (*packet)->ethhdr.h_proto = htons (ETH_P_PPPOE_DISC);
  (*packet)->ver = 1;
  (*packet)->type = 1;
  (*packet)->code = CODE_PADR;
  (*packet)->session = 0;
  (*packet)->length = htons (size);

  p = (char *) (*packet + 1);
  if (ses->filt->stag) {
    memcpy ((char *) p, ses->filt->stag, size1);
  }

  if (ses->filt->ntag) {
    p += size1;
    memcpy ((char *) p, ses->filt->ntag, size2);
  }


  if (ses->filt->ctag) {
    p += size2;
    memcpy ((char *) p, ses->filt->ctag, size3);
  }

#if 0
  if (ses->filt->htag) {
    p += size3;
    memcpy ((char *) p, ses->filt->htag, size4);
  }
#endif
  
  return size + sizeof (struct pppoe_packet);
}

int
send_packet (struct session *ses ,int sock, struct pppoe_packet *packet, int len, const char *ifn)
{
  struct sockaddr addr;
  int c;

  memset (&addr, 0, sizeof (addr));
  poe_dbglog (ses,"send_packet: ifn %s %P", ifn, packet, len);
  strcpy (addr.sa_data, ifn);

  if ((c = sendto (sock, packet, len, 0, &addr, sizeof (addr))) < 0)
    poe_error (ses,"sendto (send_packet): %m");

  return c;
}

int
wait_for_packet (struct session *ses ,int *sock, int ns, struct pppoe_packet **packet, int *len,int num_ret, int ret_pkt_size,struct pppoe_packet  *ret_pkt)
{
  fd_set fdset;
  int i, maxs = -1;
  struct sockaddr_in from;
  socklen_t fromlen;
  struct timeval timeout;
  int retransmits = 0;
  int reason = -1;

/* 
   a num_ret == 0 implies block and wait for the packet
 */

  FD_ZERO (&fdset);
  *len = 0;

  while (1) {
    retransmits++;
    if (num_ret && retransmits > num_ret)
      return -1;
    if (retransmits <= num_ret) {
      timeout.tv_sec = 1 << retransmits;
      timeout.tv_usec = 0;
    }

    for (i = 0; i < ns; i++) {
      FD_SET (sock[i], &fdset);
      if (sock[i] > maxs)
	maxs = sock[i];
    }

    if (num_ret == 0)
      reason = select (maxs + 1, &fdset, NULL, NULL, NULL);
    else
      reason = select (maxs + 1, &fdset, NULL, NULL, &timeout);

    if (reason < 0) {
      poe_error (ses,"select (wait_for_packet): %m");
      return -1;
    }
    if (reason == 0) {		/* timeout */
      if (send_packet (ses,*sock, ret_pkt, ret_pkt_size, ses->name) < 0) {
	poe_warn (ses,"unable to send PADI packet");
	return -1;
      }
      continue;			/* go back to the top */
    }

    /* reason has the number of ready descriptors */


    for (i = 0; i < ns; i++) {
      if (FD_ISSET (sock[i], &fdset)) {
	if (sock[i] < 3)	/* ie, it's an fd */
	  return sock[i];

	if (*packet != NULL)
	  free (*packet);

	if ((*packet = malloc (MAX_PACKET)) == NULL) {
	  poe_error (ses,"wait_for_packet: malloc failed: %m");
	  return -1;
	}

	fromlen = sizeof (from);
	if ( (*len = recvfrom (sock[i], *packet, MAX_PACKET, 0,
		      (struct sockaddr *) &from, &fromlen)) < 0) {
	  poe_error (ses,"wait_for_packet: recv failed: %m");
  	  *len = 0;
	  return -1;
	}


	return sock[i];
      }
    }
  }
}

struct pppoe_tag *
find_tag (struct session *ses, int length,struct pppoe_tag *start_tag, unsigned short type)
{

  struct pppoe_tag *tag ;

  for(tag = start_tag;
	(tag < (struct pppoe_tag *) ((char *) start_tag + length)) &&
	ntohs (tag->type) != TAG_END_OF_LIST;
	tag = (struct pppoe_tag *) ((char *) (tag + 1) + ntohs (tag->length))) {

    if (DEB_DISC2) {
      poe_dbglog (ses,"find_tag: tag %p length %d tag->type %x data: %.*B", tag,
	  ntohs (tag->length), ntohs (tag->type), ntohs (tag->length),
							(char *) (tag + 1));
    }
    if (ntohs (tag->type) == type) {	/* got it */

      if (DEB_DISC2)
	poe_dbglog (ses,"Found it!");
      return tag;
    }
    if (DEB_DISC2)
      poe_dbglog (ses,"find_tag: not matched! ");
  }

  return NULL;
}


int
serv_discover (struct session *ses)
{

  int pkt_size=0;
  int ret_pkt_size=0;
  struct pppoe_tag *tags = NULL;
  struct pppoe_tag *t = NULL;
  struct pppoe_packet *packet = NULL;
  struct pppoe_packet *ret_pkt = NULL;

  int state = CODE_PADI;


  /* main discovery loop */


  while (1) {

    switch (state) {
    case CODE_PADI:		/* initiate ppp_connection */
    {
      if (DEB_DISC)
	poe_dbglog (ses,"Sending PADI");
      /* start the PPPoE session */
      if ((pkt_size = create_padi (&packet, ses)) == 0) {
	poe_error (ses,"unable to create PADI packet");
	return (-1);
      }
      /* send the PADI packet */
      if (send_packet (ses,disc_sock, packet, pkt_size, ses->name) < 0) {
	poe_warn (ses,"unable to send PADI packet");
	return (-1);
      }

      ret_pkt = packet;
      ret_pkt_size = pkt_size;

      state = CODE_PADO;
      break;
    }

    case CODE_PADO:		/* wait for PADO */
    {
      if (DEB_DISC)
	  poe_dbglog (ses,"waiting for PADO");
	if (wait_for_packet (ses,&disc_sock, 1, &packet, &pkt_size, ses->PADI_ret, ret_pkt_size,ret_pkt)
	    != disc_sock || (packet->code != CODE_PADO &&
			     packet->code != CODE_PADT)) {
	  if (packet->code == CODE_PADI)	/* just an echo */
	    continue;
	  poe_warn (ses,"PADO:unexpected packet %x", packet->code);
	  continue;
	}
	if (packet->code == CODE_PADT) {	/* early termination */
	  state = CODE_PADT;
	  continue;
	}
	if (DEB_DISC2) {
	  poe_dbglog (ses,"got: %d bytes: %.*B", pkt_size, pkt_size, packet);
	  poe_dbglog (ses,"PADO received: %P", packet, pkt_size);
	}
	memcpy (ses->dmac, packet->ethhdr.h_source, ETH_ALEN);
	poe_dbglog (ses,"AC at: %E\n", ses->dmac);
	if (ntohs (packet->length) > 0) {
	  ses->tags_len = ntohs (packet->length);
	  tags = (struct pppoe_tag *) (packet + 1);
	  if (tags) {
	    ses->tags = malloc (ntohs (packet->length));
	    if (NULL == ses->tags) {
	      poe_error (ses,"failed to get mem for tags");
	      return -1;
	    }
	    memcpy (ses->tags, tags, ntohs (packet->length));
/* 
   scan for all types of errors;
   return error code
*/
	   if (NULL != (t = find_tag (ses,ntohs (packet->length),
                    (struct pppoe_tag *) (packet + 1), 
		    TAG_SERVICE_NAME_ERROR))) {
	
	      poe_error (ses,"TAG SERVICE NAME ERROR!");
	      return -1;
	   }
	   if (NULL != (t = find_tag (ses,ntohs (packet->length),
                    (struct pppoe_tag *) (packet + 1), 
                    TAG_AC_SYSTEM_ERROR))) {
	
	      poe_error (ses,"TAG AC SYSTEM ERROR!");
	      return -1;
	   }
	   if (NULL != (t = find_tag (ses,ntohs (packet->length),
                    (struct pppoe_tag *) (packet + 1), 
                    TAG_GENERIC_ERROR))) {
	
	      poe_error (ses,"TAG GENERIC ERROR!");
	      return -1;
	   }

	  }  else  { /* no tags is an error */
		return -1;
	}


	return 1;
      } /* packet > 0 */
     } /* case */

      if (packet->code == CODE_PADT) {	/* NOT expected early termination */
	return -1;
      }

    default:
      poe_error (ses,"invalid state %x", state);
      return (-1);
    }
  } /* while */
  return (0);
}


int
serv_request (struct session *ses)
{

  int pkt_size=0;
  int ret_pkt_size=0;
  struct pppoe_packet *ret_pkt = NULL;
  struct pppoe_packet *packet = NULL;

  int state = CODE_PADR;


  while (1) {
    if (state == STATE_RUN)
      break;

    switch (state) {

    case CODE_PADR:		/* send PADR */
      if (DEB_DISC)
	poe_dbglog (ses,"Sending PADR");
      if ((pkt_size = create_padr (&packet, ses)) == 0) {
	poe_error (ses,"unable to create PADR packet");
	return (-1);
      }

/*
   probably free all the tags sent here; who cares really ... when
   the prog exists the system would reclaim the memory anyway ..
 */
      if (send_packet (ses,disc_sock, packet, pkt_size, ses->name) < 0) {
	poe_error (ses,"unable to send PADR packet");
	return (-1);
      }
      state = CODE_PADS;
      ret_pkt = packet;
      ret_pkt_size = pkt_size;
      break;

    case CODE_PADS:		/* wait for PADS */
      if (DEB_DISC)
	poe_dbglog (ses,"Waiting for PADS");
      if (wait_for_packet (ses,&disc_sock, 1, &packet, &pkt_size, ses->PADR_ret,ret_pkt_size,ret_pkt)
	  != disc_sock || (packet->code != CODE_PADS &&
			   packet->code != CODE_PADT)) {
	if (packet->code == CODE_PADR)	/* just an echo */
	  continue;
	poe_warn (ses,"PADS:unexpected packet %x", packet->code);
	continue;
      }
      if (memcmp (packet->ethhdr.h_source, ses->dmac, ETH_ALEN)
	  != 0)
	continue;		/* discard packets not from AC */
      if (packet->code == CODE_PADT) {	/* early termination */
	state = CODE_PADT;
	continue;
      }

      socks[0] = disc_sock;

/* it is in network order; dont convert */
      ses->sid = packet->session;

      poe_dbglog (ses,"\nRequest completed successfully \n(session ID=%d AC MAC=%E)"
              , ntohs (ses->sid), ses->dmac);

      state = STATE_RUN;

/* should start a thread here which just waits for a PADT
   packet 
 */
      break;

    case STATE_RUN:		/* running */

      break;

    case CODE_PADT:		/* received a PADT */
      return -1;
      break;

    default:
      poe_error (ses,"invalid state %x", state);
      return (-1);
    }
  }
  return 1;
}

/*
*/
void 
print_session_tags (struct session *ses)
{
  struct pppoe_tag *t;

/*
    poe_info(ses," ---- session tags ----");
*/
  for(t = ses->tags;
	(t < (struct pppoe_tag *) ((char *) ses->tags + ses->tags_len)) &&
	ntohs (t->type) != TAG_END_OF_LIST;
	t = (struct pppoe_tag *) ((char *) (t + 1) + ntohs (t->length))) {
    poe_info(ses,"%T", t);
  }
}

/* 
   **********************************************
*/

int
ppp_connect (struct session *ses)
{

int num_restart =1;

if (ses->filt->num_restart)
	num_restart=ses->filt->num_restart;
	/*
  poe_info (ses,"ppp_connect: total retries to go %d", num_restart);
  */

  if (init_ses (ses) < 0)
    return -1;

restart:
  num_restart--;

/* 
   we block until we establish a ppp_connection
   in this version
*/

  if (-1 == serv_discover (ses)) {
    if (!num_restart) {
      cleanup_ses (ses);
      poe_error (ses,"Failed to discover server!\n");
      return (-1);
    }
    goto restart;
  }

  poe_dbglog (ses,"discovered Server!\n");

  if (serv_request (ses) < 0) {
    cleanup_ses (ses);
    poe_error (ses,"Failed to Connect to server!\n");
    poe_die (-1);
  }

  poe_dbglog (ses,"discovered SessionID!\n");

  if (ses_connect (ses) > 0)
    if (num_restart)
      goto restart;

  return 1;
}

int
ses_connect (struct session *ses)
{
  int pid;
  char pppox[32];
  char fname[MAX_FNAME];
  char pppd[MAX_FNAME];
/* at this point we know the destination MAC address,
   the session ID,
   the device,
*/

if (strlen(ses->filt->pppd)) {
	strcpy (pppd,ses->filt->pppd);
}
else
        strcpy (pppd,_PATH_PPPD);


if (strlen(ses->filt->fname) ) {
        strcpy (fname,ses->filt->fname);
}
else
	fname[0]='\0';
	

  ctrl_fd = open_device (ses);

/* download the cntrl info for this lineid */
  cntrl (ses);

  close (ctrl_fd);

  if (ses->np)
	return 1;
/*
  signal (SIGCHLD, &sigchild);
*/

/* 
   now lets fire the PPP session
   TODO: use pthreads 
*/

  pid = fork ();
  if (pid < 0) {
    poe_error (ses,"unable to fork() for pppd: %m");
    poe_die (-1);
  }

  ses->pid = pid;
  sprintf (pppox, "/dev/pppox%d", ses->line);

  if (!pid) {
    /* the speed is meaningless really, but pppd insists */
    if (strlen(ses->filt->fname) ) {
      poe_info (ses,"starting %s to interface %s using options file: %s",
	    pppd, pppox, fname);

      if (!ses->filt->peermode) {
         if (execl (pppd, "pppd", pppox, "38400", "file", fname, NULL) < 0) {
	   poe_error (ses,"cannot execl %s: %m", pppd);
	   poe_die (-1);
         }
      } else {
          if (execl (pppd, "pppd", "call", fname, NULL) < 0) {
            poe_error (ses,"cannot execl %s: %m", pppd);
            poe_die(-1);
          }

      }
    } else {
      poe_info (ses,"starting %s to interface %s", pppd, pppox);

      if (execl (pppd, "pppd", pppox, "38400", NULL) < 0) {
	poe_error (ses,"cannot execl %s: %m", pppd);
	poe_die (-1);
      }
    }
  }

  poe_dbglog (ses,"PPP started pid is: %d", ses->pid);

/* 
   sit here and asynchronously wait for a PADT
   or issue a PADT should the user decide to terminate the ppp_connection
   Hmmmm ... maybe we should have an option to restart the ppp_connection
   if it dies
 */

/* move to ppp_connected state only after pppd has been started */
  ses->state = CONNECTED;

  pause ();

  ses->state = DISCONNECTED;
  poe_info (ses,"OK we got killed");

/*
  if (ses->filt->num_restart < 2 )
    return (0);
*/

  return 1;
}

