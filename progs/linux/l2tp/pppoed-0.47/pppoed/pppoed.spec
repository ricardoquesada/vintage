Summary:	PPP-over-ethernet control daemon
Name:		pppoed
Version:	%%VERSION%%
Release:	%%RELEASE%%
Group:		Daemons
Source:		pppoed.tar.gz
Copyright:	GPL
Requires:	ppp >= 2.3.7
Buildroot:	/tmp/pppoed-root

%description
This package provides mixed kernel/user mode PPP encapsulation for xDSL and
similar internet links which use PPP protocol over the IP layer.  It is
necessary for Sympatico service in Ontario and probably elsewhere.  To use
this package successfully, your kernel must include support for the PPP
encapsulation.  A patch for various kernels is included in this archive.

%changelog
* Sun Jan 30 2000 Ralph Siemsen <ralphs@netwinder.org>
- Updated for version 0.43 with new directory structure

* Tue Jan 18 2000 Ralph Siemsen <ralphs@netwinder.org>
- Updated to version 0.42
- Put the conf.modules stuff into postinstall/postuninstall

* Tue Dec 21 1999 Ralph Siemsen <ralphs@netwinder.org>
- Packaged version 0.41 into RPM format

%prep
%setup -c

%build
cd pppoed
make dep
make

%install
cd pppoed
rm -rf $RPM_BUILD_ROOT
export PREFIX=$RPM_BUILD_ROOT
mkdir -p $PREFIX/usr/sbin
make install
mkdir -p $PREFIX/dev
make dev
mkdir -p $PREFIX/usr/doc/pppoed
DOCS='README* ../*README* ../docs/*'
install -o root -g root -m 644 $DOCS $PREFIX/usr/doc/pppoed
mkdir -p $PREFIX/etc/rc.d/init.d
install -o root -g root -m 755 init.d_pppoed $PREFIX/etc/rc.d/init.d/pppoed

%clean
rm -rf $RPM_BUILD_ROOT

%files
/usr/sbin/*
/usr/doc/*
/dev/*
#/usr/man/man8/*
/etc/rc.d/init.d/*

%post
chkconfig --add pppoed
# Add entry to conf.modules or modules.conf
FILE=/etc/conf.modules
if [ ! -e $FILE ]; then FILE=/etc/modules.conf; fi
if [ ! -e $FILE ]; then touch $FILE; fi
cp -ax $FILE $FILE~
grep -a -v pppox $FILE~ > $FILE
echo "alias char-major-144 pppox" >> $FILE
rm $FILE~

%preun
chkconfig --del pppoed
# Remove entry from conf.modules or modules.conf
FILE=/etc/conf.modules
if [ ! -e $FILE ]; then FILE=/etc/modules.conf; fi
if [ ! -e $FILE ]; then touch $FILE; fi
cp -ax $FILE $FILE~
grep -a -v pppox $FILE~ > $FILE
rm $FILE~
