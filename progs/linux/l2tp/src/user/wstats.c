/*	$Id$	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <asm/ioctl.h>
#include <unistd.h>
#include <assert.h>

#include "wiasd.h"
#include "wias.h"

struct session ses_table[N_SES];

/**
 * @fn int create_raw_socket (unsigned short type)
 */
int create_raw_socket (unsigned short type)
{
	int optval = 1, rv;

	if ((rv = socket (PF_INET, SOCK_PACKET, htons (type))) < 0) {
		printf("socket: %m");
		return -1;
	}

	if (setsockopt (rv, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) < 0) {
		printf("setsockopt: %m");
		return -1;
	}

	return rv;
}

/**
 * @fn int get_hw_addr (struct session *ses) 
 */
int get_hw_addr (struct session *ses) 
{
	struct ifreq ifr;

	assert(ses);

	strncpy (ifr.ifr_name, ses->dev, sizeof (ifr.ifr_name));

	if (ioctl (ses->sock, SIOCGIFHWADDR, &ifr) < 0) {
		printf("ioctl(SIOCGIFHWADDR): %m\n");
		return -1;
	}

	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
		printf("interface %s is not Ethernet!\n", ses->dev);
		return -1;
	}

	memcpy (ses->smac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

	PDEBUG("we have hware address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n"
			,(unsigned char)ses->smac[0]
			,(unsigned char)ses->smac[1]
			,(unsigned char)ses->smac[2]
			,(unsigned char)ses->smac[3]
			,(unsigned char)ses->smac[4]
			,(unsigned char)ses->smac[5] );

	return 0;
}

/**
 * @fn void ses_cleanup (struct session *ses)
 */
void ses_cleanup (struct session *ses)
{
	assert(ses);
	close (ses->sock);
}

/**
 * @fn int send_packet (struct session *ses, void *packet, int len )
 */
int send_packet (struct session *ses, void *packet, int len )
{
	struct sockaddr addr;
	int c;

	assert(ses);
	assert(packet);

	memset (&addr, 0, sizeof (addr));
	strcpy (addr.sa_data, ses->dev);

	if ((c = sendto (ses->sock, packet, len, 0, &addr, sizeof (addr))) < 0) {
		PDEBUG("sendto (send_packet): %m");
	}

	return c;
}

/**
 * @fn int ses_init (struct session *ses)
 */
WIAS_STATUS ses_init (struct session *ses)
{
	if ((ses->sock = create_raw_socket (ETH_P_WIAS_DISC)) < 0) {
		printf("unable to create raw socket\n");
		return WIAS_STATUS_NOPERM;
	}

	if (get_hw_addr (ses) != 0) {
		printf("No pude encontrar la mac address :-(\n");
		return WIAS_STATUS_ERROR;
	}

	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn void ses_default( struct session *ses )
 */
WIAS_STATUS ses_default( struct session *ses )
{
	assert(ses);

	ses->sock = -1;
	ses->opt_debug = 0;
	ses->opt_daemonize = 1;
	ses->log_to_fd = fileno (stdout);
	memcpy( ses->dmac, MAC_BCAST_ADDR, ETH_ALEN );
	memcpy( ses->smac, MAC_BCAST_ADDR, ETH_ALEN );

	if (options_from_file(ses,WIAS_OPTIONS_FILE) != WIAS_STATUS_SUCCESS ) {
		printf("Error while trying to read file %s\n",WIAS_OPTIONS_FILE);
		return WIAS_STATUS_FILEERROR;
	}
	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn int OpenSap()
 */
int OpenSap( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		printf("\nOpenning SAP...");
		wias_send_open_sap_request( ses ,"\x00\x00\x00\x00\xae\xe0\xa3\x91");
		s = wias_get_open_sap_confirm( ses );
	} while(s!= WIAS_STATUS_SUCCESS);
	printf("OK (SAP=%d)\n",ses->SAP);
	return 0;
}

/**
 * @fn int Connect( struct session *ses )
 */
int Connect( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		printf("\nConnecting...");
		wias_send_connect_request( ses );
		s = wias_get_connect_confirm( ses );
		if( s == WIAS_STATUS_BADLOGIN ) {
			printf("\nInvalid username or passwrd\n");
			exit(1);
		} else if ( s == WIAS_STATUS_BADVPN ) {
			printf("\nInvalid VPN number\n");
			exit(2);
		}
	} while (s!=WIAS_STATUS_SUCCESS);
	printf("OK\n");

	return 0;
}


int main (int argc, char **argv)
{
	char buf[ MAX_PACKET];
	int len=sizeof(buf);
	struct wias_link_status_confirm *wdr = (struct wias_link_status_confirm*)buf;

	struct session *ses = &ses_table[0];

	printf("wias statistics v"WIAS_VERSION"\n\n");

	memset(ses,0,sizeof(struct session));

	if( ses_default( ses ) != WIAS_STATUS_SUCCESS )
		return -1;

	if( ses_init( ses ) != WIAS_STATUS_SUCCESS )
		return -2;

	wias_send_discover_request( ses );
	wias_get_packet( ses, buf, &len );

	do {
		wias_send_link_status_request( ses );
		len=sizeof(buf);
	} while( wias_get_link_status_confirm( ses, buf, &len ) != WIAS_STATUS_SUCCESS );

//	printf("MAC: ");	print_MAC();
	printf("ESSID: 0x%hX\n",ntohs(wdr->le.ESSID));
	printf("Sucessful Tx Packets (QPSK): %u\n",ntohl(wdr->le.txrx_c.success_tx_QPSK));
	printf("Failed Tx Packets (QPSK): %u\n",ntohl(wdr->le.txrx_c.fail_tx_QPSK));
	printf("Number of Associate Requests: %u\n",ntohl(wdr->le.associate_requests));
	printf("Number of Associate Responses: %u\n",ntohl(wdr->le.associate_responses));
	printf("Number of Beacons: %u\n",ntohl(wdr->le.beacons));
	printf("Number of Beacons Missed: %u\n",ntohl(wdr->le.beacons_missed));
	printf("System Uptime: %u0 ms\n",ntohl(wdr->le.system_uptime));
	printf("Link Uptime: %u0 ms\n",ntohl(wdr->le.link_uptime));
	printf("Signal Strength Above Threshold at AP: %u dB\n",wdr->le.signal_strength_AP);
	printf("Signal Strength Above Threshold at WM: %u dB\n",wdr->le.signal_strength_WM);
	printf("Quality at AP: %u\n",wdr->le.quality_AP);
	printf("Quality at WM: %u\n",wdr->le.quality_WM);

	exit(0);
}


/* requested items by Velocom */

// MAC
// Vinuculo
// ESSID
// Tx/Rx
// succesfull Tx QPSK
// filed Tx QPSK
//
// Number of succedd requests
// responese
//
// number of beacons
// missed
//
// system uptime
// link uptime
//
// singal stregth above AP
// WM
//
// Quality at AP
// at WM


