/*	$Id$	*/
/*
 *	WaveAccess 3500 driver
 *	by Ricardo Quesada & Gerardo Richarte
 *	based on pppoe by Jamal Hadi Salim (hadi@cyberus.ca)	
 *
 */

#ifndef __KERNEL__
#define __KERNEL__
#endif
#ifndef MODULE
#define MODULE
#endif

#include <linux/config.h>
#include <linux/module.h>	

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/fcntl.h>
#include <linux/string.h>
#include <linux/major.h>
#include <linux/mm.h>
#include <linux/init.h>
#include <linux/notifier.h>

#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/bitops.h>

#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/if_ether.h>

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include <linux/devpts_fs.h>
#include <linux/if.h>		/* ifreq	*/

#include <linux/ppp_defs.h>	

#include "wias.h"

#define WIASOE_MAJOR 	(145)
#define WIASOE_MINOR_START (64)

#define MIN(a,b)	((a) < (b) ? (a) : (b))

static struct tty_struct *wiasoe_table[N_SES];
static struct termios *wiasoe_termios[N_SES];
static struct termios *wiasoe_termios_locked[N_SES];

static struct tty_driver wiasoe_driver;
static int wiasoe_refcount=0;
static struct wiasoe_struct wiasoe_state[N_SES];
static int debug = 0;

/*
 * RX
 * receive routine
 */
/**
 * @fn int wiasoe_rcv(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
 */
int wiasoe_rcv(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
{
	struct tty_struct *tty=NULL;
	struct wiasoe_hdr  *wiasoehdr;
	int len=0;
	int full=0;

	PDEBUG("wiasoe_rcv\n");

	if( wiasoe_state[0].dev == NULL ) {
		PDEBUG("wiasoe_rcv: No device\n");
		goto error;
	}

	wiasoehdr=(struct wiasoe_hdr  *)skb->nh.raw;

	len = htons(wiasoehdr->msg_len) & WIAS_LEN_MASK;

	switch (htons(wiasoehdr->msg_len) & WIAS_COMMAND_MASK) {
		case WIAS_MSG_ECHO_REQUEST:
			full=1;
			break;
		case WIAS_MSG_ECHO_REPLY:
			full=1;
			break;
		case WIAS_MSG_MODEM_REQUEST:
			full=0;
			break;
		default:
			full=0;
			printk("Unknow command %2x\n", htons(wiasoehdr->msg_len) & WIAS_COMMAND_MASK );
			break;
	}

	tty=wiasoe_state[0].tty;

	/* sesion no initialized! */
	if (!tty)
		goto error;

	/* remove the WIASOE headers */
	if(full==0)
		skb->data=skb_pull(skb,WIASOE_HDR_LEN);

	/* send it to the discipline */
	tty->ldisc.receive_buf(tty, skb->data, 0, len);

error:
	kfree_skb(skb);
	return 0;
}

static struct packet_type wiasoe_packet_type =
{
	__constant_htons(ETH_P_WIAS_SESS),
	NULL,		/* struct net_device *dev  (all devices) */
	wiasoe_rcv,	/* int (*func)(sdb, dev, pt ) */
	NULL,		/* void * data */
	NULL		/* next */
};

/**
 * @fn static void wiasoe_close(struct tty_struct * tty, struct file * filp)
 */
static void wiasoe_close(struct tty_struct * tty, struct file * filp)
{
	int line;

	if(debug)
		printk("wiasoe_close...\n");

	if (!tty)
		return;

	if (tty->count == 1)
		tty->driver_data = 0;

	line = MINOR(tty->device) - tty->driver.minor_start;
	if(debug)
		printk("wiasoe_close line:%d\n",line);
	
	if ((unsigned) line > N_SES) {
		printk("wiasoe_close: bogus line %d\n", line); 
		return;
	}	

	if (--wiasoe_state[line].counter > 0) { 
		if (debug) 
			printk("wiasoe_close: not really closing: counter=%d\n",wiasoe_state[line].counter);
		MOD_DEC_USE_COUNT; 
		return; 
	} else if (wiasoe_state[line].counter < 0) { 
		printk("wiasoe_close: bogus counter %d\n",
				wiasoe_state[line].counter);
		return; 
	}

	if (wiasoe_state[line].tty == NULL) { 
		printk("wiasoe_close: device already closed\n"); 
		return;
	}		
	
	start_bh_atomic();
	wiasoe_state[line].tty=NULL;
	wiasoe_state[line].state=CLOSED;
	end_bh_atomic();

	if (debug)
		printk("wiasoe_close called\n"); 

	MOD_DEC_USE_COUNT;
}

/**
 * @fn static void wiasoe_unthrottle(struct tty_struct * tty)
 */
static void wiasoe_unthrottle(struct tty_struct * tty)
{ 
	if( debug )
		printk("wiasoe_unthorttle...\n");

	if (!tty)
		return;

	if ((tty->flags & (1 << TTY_DO_WRITE_WAKEUP)) &&
	    tty->ldisc.write_wakeup)
		(tty->ldisc.write_wakeup)(tty);
	wake_up_interruptible(&tty->write_wait);
	set_bit(TTY_THROTTLED, &tty->flags);
}

/**
 * @fn static int wiasoe_write(struct tty_struct * tty, int from_user,
 */
static int wiasoe_write(struct tty_struct * tty, int from_user,
		       const unsigned char *buf, int count)
{
	struct device *etherdev;
	struct sk_buff *skb;
	int line,c=0,min_len;
	int i;
	int start=0;

	if (debug) printk("wiasoe_write...\n");

/* is 0 the best value to return here ? */
	if (!tty || tty->stopped)
		return 0;

	line = MINOR(tty->device) - tty->driver.minor_start;

/* check for inconsistencies */
	if ((wiasoe_state[line].tty != tty) || (CLOSED == wiasoe_state[line].state)) {
		if (debug) 
			printk("wiasoe_write: inconsistent tty or closed tty\n");
		return -EIO;
	}

	if (NULL == wiasoe_state[line].dev) {
		if (debug)
			printk("wiasoe_write: NULL device\n");
		return -EIO;
	}

	c = MIN(count, tty->ldisc.receive_room(tty));

	if (debug>1) printk("c = %d\n",c);

	if (c < PPP_HDRLEN) {
		printk("\n BAD PPP data! length %d\n",c);
		goto printerror;
	}

	if (debug>1) {
		printk("\nWrite START: %d bytes of data \n",c);
		for (i=0;i<c;i++) {
			if (0 == i%8)
				printk ("\n %d:",i);
			printk(" %x ",*(buf+i));
		}
		printk("\nDump END: all data .....\n");
	}

	/* grab the etherdev from the table */
	etherdev=wiasoe_state[line].dev;

	if( c-start < 0x2a ) {
		min_len = 0x2a;
	} else {
		min_len = c-start+1;
	}

	skb=alloc_skb(WIASOE_HDR_LEN+min_len+etherdev->hard_header_len+2,GFP_ATOMIC);
						// +2 for WIAS trailer
	if (skb == NULL) {
		printk("%s: Memory squeeze, dropping packet.\n", etherdev->name);
		return c;	
	}

	skb->dev = etherdev;
	/* for now */

	skb->mac.raw=skb->data;
	skb_reserve(skb,WIASOE_HDR_LEN+etherdev->hard_header_len);

	/* copia data */
	memcpy(skb_put(skb, c-start),buf+start,c-start);
	memset(skb_put(skb,2),1,2);		 // WIAS trailer
	if (min_len>c-start)
		*(char*)memset(skb_put(skb, min_len-c+start),0,min_len-c+start)=0x82;	// Magic!
	
/* fix these two below at some point they are wrong */
	skb->priority = 1;
	skb->nh.raw=skb->mac.raw+etherdev->hard_header_len;

	skb->protocol=htons(ETH_P_WIAS_SESS);

	wiasoe_state[line].wiasoe_hdr.wiasoe.msg_len=htons((c-start+2) | WIAS_MSG_MODEM_REPLY);
						// +2 for WIAS trailer
	wiasoe_state[line].wiasoe_hdr.wiasoe.seq =
		htons(wiasoe_state[line].seq++ & 0x3fff );


	/* copia header ether + wias header */
	memcpy(
		skb_push(skb,sizeof(struct wiasoe_ether)),
		&wiasoe_state[line].wiasoe_hdr,
		sizeof(struct wiasoe_ether));

	/* y lo manda nomas... */
	dev_queue_xmit (skb);

	return c;
printerror:

	if (debug > 1) { 
		printk("\nDump START: all data .....\n");
		for (i=0;i<c;i++) {
			if (0 == i%8) 
				printk ("\n %d:",i);
			printk(" %2x ",*(buf+i));
		}
		printk("\nDump END: all data .....\n");
		return c-1;
	}
	return -1;
}

/**
 * @fn static int wiasoe_write_room(struct tty_struct *tty)
 */
static int wiasoe_write_room(struct tty_struct *tty)
{
	if( debug )
		printk("wiasoe_writeroom...\n");

	if (!tty || tty->stopped)
		return 0;

	return tty->ldisc.receive_room(tty);
}

/**
 * @fn static int wiasoe_chars_in_buffer(struct tty_struct *tty)
 */
static int wiasoe_chars_in_buffer(struct tty_struct *tty)
{
	int count;

	if( debug )
		printk("wiasoe_chars_in_buffer...\n");

	if (!tty || !tty->ldisc.chars_in_buffer)
		return 0;

	/* The ldisc must report 0 if no characters available to be read */
	count = tty->ldisc.chars_in_buffer(tty);

	return count;
}

/**
 * @fn static void wiasoe_flush_buffer(struct tty_struct *tty)
 */
static void wiasoe_flush_buffer(struct tty_struct *tty)
{
	if( debug )
		printk("wiasoe_flush_buffer...\n");

	if (!tty)
		return;
	
	if (tty->ldisc.flush_buffer)
		tty->ldisc.flush_buffer(tty);
	
	if (tty->packet) {
		tty->ctrl_status |= TIOCPKT_FLUSHWRITE;
		wake_up_interruptible(&tty->read_wait);
	}
}

/**
 * @fn int init_WIASOE(int line)
 */
int init_WIASOE(int line)
{
	wiasoe_state[line].counter = 1;
	wiasoe_state[line].seq = 1;
	wiasoe_state[line].magic=WIASOE_TYPE;
	wiasoe_state[line].wiasoe_hdr.ether.h_proto=htons(ETH_P_WIAS_SESS);

	return 0;
}

/**
 * @fn static int wiasoe_open(struct tty_struct *tty, struct file * filp)
 */
static int wiasoe_open(struct tty_struct *tty, struct file * filp)
{
	struct wiasoe_struct * wiasoe;
	int	line;


	line = MINOR(tty->device) - tty->driver.minor_start;
	if(debug) printk("wiasoe_opening line:%d \n",line);

	if ((line < 0) || (line >= N_SES)) {
		return -ENODEV;
	}

	MOD_INC_USE_COUNT;

	if (wiasoe_state[line].tty) {
		wiasoe_state[line].counter++; 
		if (debug > 1) 
			printk("wiasoe_open: line already occupied!\n");
		return 0;	
	}

	if (init_WIASOE(line) < 0) {
		printk("wiasoe_open: error 1!\n");
		MOD_DEC_USE_COUNT;
		return -EIO; 
	}

	wiasoe = (struct wiasoe_struct *)(tty->driver.driver_state) + line;
	tty->driver_data = wiasoe;

	if (tty->count != 1) { 
		printk("wiasoe_open: error 2!\n");
		MOD_DEC_USE_COUNT;
		return -EIO;
	}

	clear_bit(TTY_OTHER_CLOSED, &tty->flags);

	wiasoe_state[line].tty=tty;
	wiasoe_state[line].state=OPENED;
	set_bit(TTY_THROTTLED, &tty->flags);

	return 0;
}

/**
 * @fn static void wiasoe_set_termios(struct tty_struct *tty, struct termios *old_termios)
 */
static void wiasoe_set_termios(struct tty_struct *tty, struct termios *old_termios)
{
	if(debug)
		printk("wiasoe_set_termios...\n");

	tty->termios->c_cflag &= ~(CSIZE | PARENB);
	tty->termios->c_cflag |= (CS8 | CREAD);
}

/**
 * @fn static int wias_netdev_notifier(struct notifier_block *this, 
 */
static int wias_netdev_notifier(struct notifier_block *this, 
			unsigned long event, void *ptr)
{
	struct device *dev = ptr;
	int i;

	/* XXX Probably races. */ 
	if (event != NETDEV_DOWN && event != NETDEV_REBOOT)
		return NOTIFY_DONE;
	for (i = 0; i < N_SES; i++) { 
		if (wiasoe_state[i].dev == dev) {
			printk("wiasoe:%d: device went away from under us\n",
				   i);
			wiasoe_state[i].dev = NULL;
		}
	}
	return NOTIFY_DONE;
}

static struct notifier_block wiasoe_netdev_notifier = { 
	wias_netdev_notifier,
	NULL,
	0
};

/**
 * @fn int wiasoe_ioctl (struct tty_struct *tty, struct file *file,
 */
int wiasoe_ioctl (struct tty_struct *tty, struct file *file,
			 unsigned int cmd, unsigned long arg)
{
	char mac[ETH_ALEN];
	int i;
	struct device *them; /* physical device */
#ifdef CONFIG_IP_ALIAS
	char *colon;
	char realphysname[IFNAMSIZ];
#endif /* CONFIG_IP_ALIAS */

	if(debug)
		printk("wiasoe_ioctl... 0x%.8x, 0x%.8x\n",cmd,(int)arg);

	if (!tty) {
		printk("wiasoe_ioctl called with NULL tty!\n");
		return -EIO;
	}


	switch (cmd) { 
	/*
	case TCGETS:
	case TCSETSF:
	case TCFLSH:
	case TIOCMBIS:
		return -ENOIOCTLCMD;
	*/
	case TIOCMBIC:
		return 0;
	case WIAS_ADDMACDST:
		PDEBUG("WIAS_ADDMACDST\n");
		if (copy_from_user(mac,(void*)arg,sizeof(mac))) {
			printk("error en WIAS_ADDMACDST\n");
		}
		for(i=0;i< N_SES;i++) {
			memcpy( wiasoe_state[i].wiasoe_hdr.ether.h_dest,mac,sizeof(mac) );
		}
		break;
		
	case WIAS_ADDMACSRC:
		PDEBUG("WIAS_ADDMACSRC\n");
		if (copy_from_user(mac,(void*)arg,sizeof(mac))) {
			printk("error en WIAS_ADDMACDST\n");
		}
		for(i=0;i< N_SES;i++) {
			memcpy( wiasoe_state[i].wiasoe_hdr.ether.h_source,mac,sizeof(mac) );
		}
		break;

	case WIAS_SETDEV:
		PDEBUG("WIAS_SETDEV\n");
#ifdef CONFIG_IP_ALIAS
		/* If this is an IP alias interface, get its real physical name */
		copy_from_user(realphysname, (char*)arg, IFNAMSIZ);
		realphysname[IFNAMSIZ-1] = 0;
		colon = strchr(realphysname, ':');
		if (colon) *colon = 0;
		them = dev_get(realphysname);
#else /* CONFIG_IP_ALIAS */
		them = dev_get(cf->cf_name);
#endif /* CONFIG_IP_ALIAS */

		if (them == NULL) {
			printk( KERN_INFO
				"wiasoe_ioctl: "
				"physical device requested is null\n");

			return -ENXIO;
		}
	
		if (wiasoe_state[0].dev)
			return -EBUSY;

		wiasoe_state[0].dev = them;
		return 0;

	case WIAS_DELDEV:
		if (! wiasoe_state[0].dev)
			return -ENODEV;
		wiasoe_state[0].dev = NULL;
		return 0;

	default:
		return -ENOIOCTLCMD;
	}
	return 0;
}


int __init wiasoe_init(void)
{
	int i;

	memset(&wiasoe_driver, 0, sizeof(struct tty_driver));
	wiasoe_driver.magic = TTY_DRIVER_MAGIC;
	wiasoe_driver.driver_name = "wiasoe";
	wiasoe_driver.name = "wiasS";
	wiasoe_driver.major = WIASOE_MAJOR;
	wiasoe_driver.minor_start = WIASOE_MINOR_START;
	wiasoe_driver.num = N_SES;

/* just like the ttyS* */

	wiasoe_driver.type = TTY_DRIVER_TYPE_SERIAL;
	wiasoe_driver.subtype = SERIAL_TYPE_NORMAL;
	wiasoe_driver.init_termios = tty_std_termios;
	wiasoe_driver.init_termios.c_cflag =
		B9600 | CS8 | CREAD | HUPCL | CLOCAL;


	wiasoe_driver.flags =  TTY_DRIVER_REAL_RAW;
	wiasoe_driver.refcount = &wiasoe_refcount;
	wiasoe_driver.table = wiasoe_table;
	wiasoe_driver.termios = wiasoe_termios;
	wiasoe_driver.termios_locked = wiasoe_termios_locked;
	wiasoe_driver.driver_state = wiasoe_state;

	wiasoe_driver.open = wiasoe_open;
	wiasoe_driver.close = wiasoe_close;
	wiasoe_driver.write = wiasoe_write;
	wiasoe_driver.write_room = wiasoe_write_room;
	wiasoe_driver.flush_buffer = wiasoe_flush_buffer;
	wiasoe_driver.chars_in_buffer = wiasoe_chars_in_buffer;
	wiasoe_driver.unthrottle = wiasoe_unthrottle;
	wiasoe_driver.set_termios = wiasoe_set_termios;
	wiasoe_driver.ioctl = wiasoe_ioctl;

	if (tty_register_driver(&wiasoe_driver)) { 
		printk("Couldn't register wiasoe driver");
		return -EINVAL;
	}

	/* also register the SESSION Ether protocol */
	dev_add_pack(&wiasoe_packet_type);

	for (i=0;i<N_SES;i++) {
		wiasoe_state[i].tty=NULL;
	}

	printk("Registering WiasoE driver\n"); 

	register_netdevice_notifier(&wiasoe_netdev_notifier); 

	return 0;
}


MODULE_AUTHOR("Ricardo Quesada & Gerardo Richiarte"); 
MODULE_DESCRIPTION("'PPP over WIAS over Ethernet' driver"); 
MODULE_PARM(debug, "i"); 

#ifdef MODULE

int init_module(void) 
{
	return wiasoe_init();
}

void cleanup_module(void)
{
	int err;

	printk("Removing WIASoE protocol\n"); 

	dev_remove_pack(&wiasoe_packet_type);

	printk("Removing WIASoE tty driver\n");

	err = tty_unregister_driver(&wiasoe_driver);
	if (err < 0)
		printk("wiasoe: tty_unregister_driver failed: %d\n", err); 

	unregister_netdevice_notifier(&wiasoe_netdev_notifier); 

	synchronize_bh(); 
}
#endif
