/*
 * SA grouping
 * Copyright (C) 1996  John Ioannidis.
 * Copyright (C) 1998, 1999  Richard Guy Briggs.
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

char spigrp_c_version[] = "RCSID $Id: spigrp.c,v 1.27 2000/01/25 14:38:52 rgb Exp $";


#include <sys/types.h>
#include <linux/types.h> /* new */
#include <string.h>
#include <errno.h>
#include <sys/stat.h> /* open() */
#include <fcntl.h> /* open() */
#include <stdlib.h> /* system(), strtoul() */

#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
/* #include <linux/ip.h> */

#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <freeswan.h>
#include <linux/autoconf.h>	/* CONFIG_IPSEC_PFKEYv2 */
#ifdef CONFIG_IPSEC_PFKEYv2
     #include <signal.h>
     #include <pfkeyv2.h>
     #include <pfkey.h>
#endif /* CONFIG_IPSEC_PFKEYv2 */
#include "radij.h"
#include "ipsec_encap.h"
#include "ipsec_netlink.h"
#include "ipsec_ah.h"


__u32 bigbuf[1024];
char *program_name;

#ifdef CONFIG_IPSEC_PFKEYv2
extern unsigned int pfkey_lib_debug; /* used by libfreeswan/pfkey_v2_build */
int pfkey_sock;
fd_set pfkey_socks;
uint32_t pfkey_seq = 0;
#endif /* CONFIG_IPSEC_PFKEYv2 */

#if 0
static int
resolve_ip(char *arg, struct in_addr *ip)
{
	struct hostent *hostp;
	struct netent *netp;
	
	if (!strcmp(arg, "default")) {/* default is really 0.0.0.0 */
		ip->s_addr = htonl(INADDR_ANY);
		return(1);
	}
	if (!(strcmp(arg, "255.255.255.255"))) {
		/* inet_addr() returns -1 for this, as it should, but that means an error */
		ip->s_addr = htonl(INADDR_BROADCAST);
		return (1);
	}
	if ((netp = getnetbyname(arg)) != (struct netent *)NULL) {
		ip->s_addr = htonl(netp->n_net);
		return(1);
	}
	if ((hostp = gethostbyname(arg)) == (struct hostent *)NULL) {
		errno = h_errno;
		return(-1);
	}
	memcpy((char *) ip, (char *) hostp->h_addr_list[0], (unsigned int)hostp->h_length);
	return(0);
}
#endif

static void
usage(char *s)
{
	fprintf(stdout, "usage: %s [ --label <label> ] [ --debug ] dst1 spi1 proto1 [ dst2 spi2 proto2 [ dst3 spi3 proto3 [ dst4 spi4 proto4 ] ] ]\n", s);
	fprintf(stdout, "usage: %s [ --label <label> ] [ --debug ] --said <SA1> [ <SA2> [ <SA3> [ <SA4> ] ] ]\n", s);
	fprintf(stdout, "usage: %s --help\n", s);
	fprintf(stdout, "usage: %s --version\n", s);
	exit(1);
}

	
int
main(int argc, char **argv)
{
/*	int fd; */
/*	int ret; */
	int i, nspis;
	char *endptr;
	int said_opt = 0;

	struct encap_msghdr *em = (struct encap_msghdr *)bigbuf;
	const char* error_s;
	char ipaddr_txt[ADDRTOA_BUF];                
	int debug = 0;
	int j;

#ifdef CONFIG_IPSEC_PFKEYv2
	int error = 0;

	struct sadb_ext *extensions[SADB_EXT_MAX + 1];
	struct sadb_msg *pfkey_msg;
#if 0
	struct sockaddr_in pfkey_address_s_ska;
#endif
	struct sockaddr_in pfkey_address_d_ska;
#else /* CONFIG_IPSEC_PFKEYv2 */
	int fd = 0;
#endif /* CONFIG_IPSEC_PFKEYv2 */
	
	program_name = argv[0];

        if(strcmp(argv[1], "--debug") == 0) {
		debug = 1;
		if(debug) {
			fprintf(stdout, "\"--debug\" option requested.\n");
		}
		argv += 1;
		argc -= 1;
#ifdef CONFIG_IPSEC_PFKEYv2
		pfkey_lib_debug = 1;
#endif /* CONFIG_IPSEC_PFKEYv2 */
        }

	if(debug) {
		fprintf(stdout, "argc=%d.\n", argc);
	}

	if(argc == 1) {
		system("cat /proc/net/ipsec_spigrp");
		exit(0);
	}

        if(strcmp(argv[1], "--help") == 0) {
		if(debug) {
			fprintf(stdout, "\"--help\" option requested.\n");
		}
                usage(program_name);
        }

        if(strcmp(argv[1], "--version") == 0) {
                fprintf(stderr, "%s, %s\n", program_name, spigrp_c_version);
                exit(1);
        }

        if(strcmp(argv[1], "--label") == 0) {
		if(argc > 2) {
			program_name = argv[2];
			if(debug) {
				fprintf(stdout, "using \"%s\" as a label.\n", program_name);
			}
			argv += 2;
			argc -= 2;
		} else {
			fprintf(stderr, "%s: --label option requires an argument.\n",
				program_name);
			exit(1);
		}
        }

        if(strcmp(argv[1], "--said") == 0) {
		if(debug) {
			fprintf(stdout, "processing %d args with --said flag.\n", argc);
		}
		said_opt = 1;
        }
	
	if(said_opt) {
		if (argc < 3 /*|| argc > 5*/) {
			fprintf(stderr, "expecting 3 or more args with --said, got %d.\n", argc);
			usage(program_name);
		}
		nspis = argc - 2;
	} else {
		if ((argc < 4) || (argc > 13) || ((argc % 3) != 1)) {
			fprintf(stderr, "expecting 4 or more args without --said, got %d.\n", argc);
			usage(program_name);
		}
		nspis = argc / 3;
	}

	if(debug) {
		fprintf(stdout, "processing %d nspis.\n", nspis);
	}

	em->em_magic = EM_MAGIC;
	em->em_version = 0;
	if(nspis == 1)
		em->em_type = EMT_UNGRPSPIS;
	else
		em->em_type = EMT_GRPSPIS;
	em->em_msglen = EMT_GRPSPIS_FLEN +
		nspis *
		(sizeof(em->em_rel[0]));
	for(i = 0; i < nspis; i++) {
		if(debug) {
			fprintf(stdout, "processing spi #%d.\n", i);
		}

		if(said_opt) {
			if((error_s = atosa(argv[i+2], 0, &(em->em_rel[i].emr_said))) != NULL) {
				fprintf(stderr, "%s: Error, %s converting --sa argument:%s\n",
					program_name, error_s, argv[i+2]);
				exit (1);
			}
			if(debug) {
				addrtoa(em->em_rel[i].emr_said.dst, 0, ipaddr_txt, sizeof(ipaddr_txt));
				fprintf(stdout, "said[%d].dst=%s.\n", i, ipaddr_txt);
			}
		} else {
			if(!strcmp(argv[i*3+3], "ah")) {
				em->em_rel[i].emr_proto = SA_AH;
			}
			if(!strcmp(argv[i*3+3], "esp")) {
				em->em_rel[i].emr_proto = SA_ESP;
			}
			if(!strcmp(argv[i*3+3], "tun")) {
				em->em_rel[i].emr_proto = SA_IPIP;
			}
			if(em->em_rel[i].emr_proto == 0) {
				fprintf(stderr, "%s: Badly formed proto: %s\n",
					program_name, argv[i*3+3]);
				exit(1);
			}
			em->em_rel[i].emr_spi = htonl(strtoul(argv[i*3+2], &endptr, 0));
			if(!(endptr == argv[i*3+2] + strlen(argv[i*3+2]))) {
				fprintf(stderr, "%s: Badly formed spi: %s\n",
					program_name, argv[i*3+2]);
				exit(1);
			}
			if((error_s = atoaddr(argv[i*3+1], 0, &(em->em_rel[i].emr_dst))) != 0) {
				if(error_s) {
					fprintf(stderr, "%s: Error, %s converting %dth address argument:%s\n",
						program_name, error_s, i, argv[i*3+1]);
					exit (1);
				}
			}
#if 0
			if((ret = resolve_ip(argv[i*3+1] /*optarg*/, &(em->em_rel[i].emr_dst))) != 0) {
				if(ret == 1) {
					fprintf(stderr, "%s: Destination address cannot be a network: %s\n",
						program_name, argv[i*3+1]/*optarg*/);
					exit (1);
				} else {
					fprintf(stderr, "%s: Invalid formation of destination address: %s\n",
						program_name, argv[i*3+1]/*optarg*/);
					exit (1);
				}
			}
#endif
		}
		if(debug) {
			fprintf(stdout, "SA (size:%d) %d contains: ",
				(sizeof(struct sa_id) + sizeof(struct tdb*)), i+1);
			for(j = 0; j < sizeof(struct sa_id); j++) {
				fprintf(stdout, " %02x",
					((char*)(em))
					[8 + i * (sizeof(struct sa_id) + sizeof(struct tdb*)) + j]);
			}
			fprintf(stdout, "\n");
			fprintf(stdout, "proto = %d\n", em->em_rel[i].emr_said.proto);
			fprintf(stdout, "spi = %08x\n", em->em_rel[i].emr_said.spi);
			fprintf(stdout, "edst = %08x\n", em->em_rel[i].emr_said.dst.s_addr);
		}
	}	

#ifdef CONFIG_IPSEC_PFKEYv2
	if(debug) {
		fprintf(stdout, "Openning pfkey socket.\n");
	}

	if((pfkey_sock = socket(PF_KEY, SOCK_RAW, PF_KEY_V2) ) < 0) {
		fprintf(stderr, "%s: Trouble openning PF_KEY family socket with error: ",
			program_name);
		switch(errno) {
		case ENOENT:
			fprintf(stderr, "device does not exist.  See FreeS/WAN installation procedure.\n");
			break;
		case EACCES:
			fprintf(stderr, "access denied.  ");
			if(getuid() == 0) {
				fprintf(stderr, "Check permissions.  Should be 600.\n");
			} else {
				fprintf(stderr, "You must be root to open this file.\n");
			}
			break;
		case EUNATCH:
			fprintf(stderr, "Netlink not enabled OR KLIPS not loaded.\n");
			break;
		case ENODEV:
			fprintf(stderr, "KLIPS not loaded or enabled.\n");
			break;
		case EBUSY:
			fprintf(stderr, "KLIPS is busy.  Most likely a serious internal error occured in a previous command.  Please report as much detail as possible to development team.\n");
			break;
		case EINVAL:
			fprintf(stderr, "Invalid argument, KLIPS not loaded or check kernel log messages for specifics.\n");
			break;
		case ENOBUFS:
			fprintf(stderr, "No kernel memory to allocate SA.\n");
			break;
		case ESOCKTNOSUPPORT:
			fprintf(stderr, "Algorithm support not available in the kernel.  Please compile in support.\n");
			break;
		case EEXIST:
			fprintf(stderr, "SA already in use.  Delete old one first.\n");
			break;
		case ENXIO:
			fprintf(stderr, "SA does not exist.  Cannot delete.\n");
			break;
		default:
			fprintf(stderr, "Unknown file open error %d.  Please report as much detail as possible to development team.\n", errno);
		}
		exit(1);
	}

	for(i = 0; i < (((nspis - 1) < 2) ? 1 : (nspis - 1)); i++) {
		if(debug) {
			fprintf(stdout, "processing %dth pfkey message.\n", i);
		}

		pfkey_extensions_init(extensions);
		for(j = 0; j < ((nspis == 1) ? 1 : 2); j++) {
			if(debug) {
				fprintf(stdout, "processing %dth said of %dth pfkey message.\n", j, i);
			}

			/* Build an SADB_X_GRPSA message to send down. */
			/* It needs <base, SA, SA2, address(S,D,D2) > minimum. */
			if(!j) {
				if((error = pfkey_msg_hdr_build(&extensions[0],
								SADB_X_GRPSA,
								proto2satype(em->em_rel[i].emr_said.proto),
#if 0
								(em->em_rel[i].emr_said.proto == SA_ESP ? SADB_SATYPE_ESP :
								 (em->em_rel[i].emr_said.proto == SA_AH ? SADB_SATYPE_AH :
								  (em->em_rel[i].emr_said.proto == SA_IPIP ? SADB_X_SATYPE_IPIP :
								   SADB_SATYPE_UNSPEC))),
#endif
								0,
								++pfkey_seq,
								getpid()))) {
					fprintf(stderr, "%s: Trouble building message header, error=%d.\n",
						program_name, error);
					pfkey_extensions_free(extensions);
					exit(1);
				}
			} else {
				if(debug) {
					fprintf(stdout, "setting x_satype proto=%d satype=%d\n",
						em->em_rel[i+j].emr_said.proto,
						proto2satype(em->em_rel[i+j].emr_said.proto)
#if 0
						(em->em_rel[i+j].emr_said.proto == SA_ESP ? SADB_SATYPE_ESP :
						 (em->em_rel[i+j].emr_said.proto == SA_AH ? SADB_SATYPE_AH :
						  (em->em_rel[i+j].emr_said.proto == SA_IPIP ? SADB_X_SATYPE_IPIP :
						   SADB_SATYPE_UNSPEC)))
#endif
						);
				}

				if((error = pfkey_x_satype_build(&extensions[SADB_X_EXT_SATYPE2],
								 proto2satype(em->em_rel[i+j].emr_said.proto)
#if 0
								 (em->em_rel[i+j].emr_said.proto == SA_ESP ? SADB_SATYPE_ESP :
								  (em->em_rel[i+j].emr_said.proto == SA_AH ? SADB_SATYPE_AH :
								   (em->em_rel[i+j].emr_said.proto == SA_IPIP ? SADB_X_SATYPE_IPIP :
								    SADB_SATYPE_UNSPEC)))
#endif
					))) {
					fprintf(stderr, "%s: Trouble building message header, error=%d.\n",
						program_name, error);
					pfkey_extensions_free(extensions);
					exit(1);
				}
			}

			if((error = pfkey_sa_build(&extensions[!j ? SADB_EXT_SA : SADB_X_EXT_SA2],
						   !j ? SADB_EXT_SA : SADB_X_EXT_SA2,
						   em->em_rel[i+j].emr_said.spi, /* in network order */
						   0,
						   0,
						   0,
						   0,
						   0))) {
				fprintf(stderr, "%s: Trouble building sa extension, error=%d.\n",
					program_name, error);
				pfkey_extensions_free(extensions);
				exit(1);
			}
			
#if 0
			if(!j) {
				pfkey_address_s_ska.sin_addr.s_addr = 0;
				pfkey_address_s_ska.sin_family = AF_INET;
				pfkey_address_s_ska.sin_port = 0;
				{
					int k;
					
					for(k = 0;
					    k < sizeof(struct sockaddr_in) -
						    offsetof(struct sockaddr_in, sin_zero);
					    k++) {
						pfkey_address_s_ska.sin_zero[k] = 0;
					}
				}
				if((error = pfkey_address_build(&extensions[SADB_EXT_ADDRESS_SRC],
								SADB_EXT_ADDRESS_SRC,
								0,
								0,
								(struct sockaddr*)&pfkey_address_s_ska))) {
					addrtoa(pfkey_address_s_ska.sin_addr,
						0, ipaddr_txt, sizeof(ipaddr_txt));
					fprintf(stderr, "%s: Trouble building address_s extension (%s), error=%d.\n",
						program_name, ipaddr_txt, error);
					pfkey_extensions_free(extensions);
					exit(1);
				}
			}
#endif			
			pfkey_address_d_ska.sin_addr = em->em_rel[i+j].emr_said.dst; /* dst */
			pfkey_address_d_ska.sin_family = AF_INET;
			pfkey_address_d_ska.sin_port = 0;
			{
				int k;
				for(k = 0;
				    k < sizeof(struct sockaddr_in) -
					    offsetof(struct sockaddr_in, sin_zero);
				    k++) {
					pfkey_address_d_ska.sin_zero[k] = 0;
				}
			}
			if((error = pfkey_address_build(&extensions[!j ? SADB_EXT_ADDRESS_DST : SADB_X_EXT_ADDRESS_DST2],
							!j ? SADB_EXT_ADDRESS_DST : SADB_X_EXT_ADDRESS_DST2,
							0,
							0,
							(struct sockaddr*)&pfkey_address_d_ska))) {
				addrtoa(pfkey_address_d_ska.sin_addr,
					0, ipaddr_txt, sizeof(ipaddr_txt));
				fprintf(stderr, "%s: Trouble building address_d extension (%s), error=%d.\n",
					program_name, ipaddr_txt, error);
				pfkey_extensions_free(extensions);
				exit(1);
			}
			
		}

		if((error = pfkey_msg_build(&pfkey_msg, extensions, EXT_BITS_IN))) {
			fprintf(stderr, "%s: Trouble building pfkey message, error=%d.\n",
				program_name, error);
			pfkey_extensions_free(extensions);
			pfkey_msg_free(&pfkey_msg);
			exit(1);
		}
		
		if((error = write(pfkey_sock,
				  pfkey_msg,
				  pfkey_msg->sadb_msg_len * IPSEC_PFKEYv2_ALIGN)) !=
		   pfkey_msg->sadb_msg_len * IPSEC_PFKEYv2_ALIGN) {
			fprintf(stderr, "%s: pfkey write failed, returning %d with errno=%d.\n",
				program_name, error, errno);
			pfkey_extensions_free(extensions);
			pfkey_msg_free(&pfkey_msg);
			switch(errno) {
			case EACCES:
				fprintf(stderr, "access denied.  ");
				if(getuid() == 0) {
					fprintf(stderr, "Check permissions.  Should be 600.\n");
				} else {
					fprintf(stderr, "You must be root to open this file.\n");
				}
				break;
			case EUNATCH:
				fprintf(stderr, "Netlink not enabled OR KLIPS not loaded.\n");
				break;
			case EBUSY:
				fprintf(stderr, "KLIPS is busy.  Most likely a serious internal error occured in a previous command.  Please report as much detail as possible to development team.\n");
				break;
			case EINVAL:
				fprintf(stderr, "Invalid argument, check kernel log messages for specifics.\n");
				break;
			case ENODEV:
				fprintf(stderr, "KLIPS not loaded or enabled.\n");
				fprintf(stderr, "No device?!?\n");
				break;
			case ENOBUFS:
				fprintf(stderr, "No kernel memory to allocate SA.\n");
				break;
			case ESOCKTNOSUPPORT:
				fprintf(stderr, "Algorithm support not available in the kernel.  Please compile in support.\n");
				break;
			case EEXIST:
				fprintf(stderr, "SA already in use.  Delete old one first.\n");
				break;
			case ENOENT:
				fprintf(stderr, "device does not exist.  See FreeS/WAN installation procedure.\n");
				break;
			case ENXIO:
				fprintf(stderr, "SA does not exist.  Cannot delete.\n");
				break;
			default:
				fprintf(stderr, "Unknown socket write error %d.  Please report as much detail as possible to development team.\n", errno);
			}
			exit(1);
		}
		if(pfkey_msg) {
			pfkey_extensions_free(extensions);
			pfkey_msg_free(&pfkey_msg);
		}
	}

	(void) close(pfkey_sock);  /* close the socket */
#else /* CONFIG_IPSEC_PFKEYv2 */

	fd = open("/dev/ipsec", 2);
	if (fd < 0) {
		fprintf(stderr, "%s: Could not open /dev/ipsec -- ", program_name);
		switch(errno) {
		case ENOENT:
			fprintf(stderr, "device does not exist.  See FreeS/WAN installation procedure.\n");
		case EACCES:
			fprintf(stderr, "access denied.  ");
			if(getuid() == 0) {
				fprintf(stderr, "Check permissions.  Should be 600.\n");
			} else {
				fprintf(stderr, "You must be root to open this file.\n");
			}
			break;
		case EUNATCH:
			fprintf(stderr, "Netlink not enabled OR KLIPS not loaded.\n");
			break;
		case ENODEV:
			fprintf(stderr, "KLIPS not loaded or enabled.\n");
			break;
		case EBUSY:
			fprintf(stderr, "KLIPS is busy.  Most likely a serious internal error occured in a previous command.  Please report as much detail as possible to development team.\n");
		default:
			fprintf(stderr, "Unknown file open error %d\n", errno);
		}
		exit(1);
	}
	if (write(fd, (caddr_t)em, em->em_msglen) != em->em_msglen) {
		close(fd);
		fprintf(stderr, "%s: Had trouble writing to /dev/ipsec -- ", program_name);
		switch(errno) {
		case EINVAL:
			fprintf(stderr, "Invalid argument, check kernel log messages for specifics.\n");
			break;
		case ENXIO:
			fprintf(stderr, "SA not found.  Check kernel log messages for specifics.\n");
			break;
		case EBUSY:
			fprintf(stderr, "SA in use.  Check kernel log messages for specifics.\n");
			break;
		default:
			fprintf(stderr, "Unknown file write error %d\n", errno);
		}
		close(fd);
		exit(1);
	}
	close(fd);
#endif /* CONFIG_IPSEC_PFKEYv2 */
	exit(0);
}
/*
 * $Log: spigrp.c,v $
 * Revision 1.27  2000/01/25 14:38:52  rgb
 * Fixed variable declaration bug so it will compile with pfkey off.
 *
 * Revision 1.26  2000/01/22 23:22:47  rgb
 * Use new function proto2satype().
 *
 * Revision 1.25  2000/01/21 09:42:32  rgb
 * Replace resolve_ip() with atoaddr() from freeswanlib.
 *
 * Revision 1.24  2000/01/21 06:25:51  rgb
 * Added pfkeyv2 support to completely avoid netlink.
 * Added --debug switch to command line.
 * Added --said processing to command line.
 *
 * Revision 1.23  1999/12/07 18:30:26  rgb
 * Added headers to silence fussy compilers.
 * Converted local functions to static to limit scope.
 * Removed unused cruft.
 * Converted main() to prototyped declaration.
 *
 * Revision 1.22  1999/11/25 09:09:43  rgb
 * Comment out unused variables.
 * Clarified assignment in conditional with parens.
 *
 * Revision 1.21  1999/11/23 23:06:27  rgb
 * Sort out pfkey and freeswan headers, putting them in a library path.
 *
 * Revision 1.20  1999/10/16 00:27:14  rgb
 * Removed cruft.
 *
 * Revision 1.19  1999/04/15 15:37:28  rgb
 * Forward check changes from POST1_00 branch.
 *
 * Revision 1.15.2.2  1999/04/13 20:58:10  rgb
 * Add argc==1 --> /proc/net/ipsec_*.
 *
 * Revision 1.15.2.1  1999/03/30 17:01:36  rgb
 * Make main() return type explicit.
 *
 * Revision 1.18  1999/04/11 00:12:09  henry
 * GPL boilerplate
 *
 * Revision 1.17  1999/04/06 04:54:39  rgb
 * Fix/Add RCSID Id: and Log: bits to make PHMDs happy.  This includes
 * patch shell fixes.
 *
 * Revision 1.16  1999/03/17 15:40:54  rgb
 * Make explicit main() return type of int.
 *
 * Revision 1.15  1999/01/28 23:20:49  rgb
 * Replace hard-coded numbers in macros and code with meaningful values
 * automatically generated from sizeof() and offsetof() to further the
 * goal of platform independance.
 *
 * Revision 1.14  1999/01/22 06:36:46  rgb
 * 64-bit clean-up.
 *
 * Revision 1.13  1998/11/12 21:08:04  rgb
 * Add --label option to identify caller from scripts.
 *
 * Revision 1.12  1998/10/26 01:28:38  henry
 * use SA_* protocol names, not IPPROTO_*, to avoid compile problems
 *
 * Revision 1.11  1998/10/25 02:47:09  rgb
 * Fix bug in size of stucture passed in from user space for grpspi command.
 * Added debugging code to find spigrp stucture size mismatch bug.
 * Convert switch to loop for more efficient coding and redundant code elimination.
 *
 * Revision 1.10  1998/10/19 18:58:56  rgb
 * Added inclusion of freeswan.h.
 * a_id structure implemented and used: now includes protocol.
 *
 * Revision 1.9  1998/10/09 04:36:32  rgb
 * Changed help output from stderr to stdout.
 * Avoid use of argv[0] after first use.
 *
 * Revision 1.8  1998/08/05 22:24:45  rgb
 * Change includes to accomodate RH5.x
 *
 * Revision 1.7  1998/07/29 21:43:17  rgb
 * Convert to 0x-prefixed spis.
 * Support dns lookups for hostnames.
 *
 * Revision 1.6  1998/07/14 18:24:05  rgb
 * Remove unused skbuff header.
 *
 * Revision 1.5  1998/07/09 18:14:11  rgb
 * Added error checking to IP's and keys.
 * Made most error messages more specific rather than spamming usage text.
 * Added more descriptive kernel error return codes and messages.
 * Converted all spi translations to unsigned.
 * Removed all invocations of perror.
 *
 * Revision 1.4  1998/06/30 18:04:32  rgb
 * Fix compiler warning: couldn't find 'struct option' prototype.
 *
 * Revision 1.3  1998/05/27 18:48:20  rgb
 * Adding --help and --version directives.
 *
 * Revision 1.2  1998/05/18 21:14:16  rgb
 * Modifications to be able to ungroup spi's.
 *
 * Revision 1.1.1.1  1998/04/08 05:35:09  henry
 * RGB's ipsec-0.8pre2.tar.gz ipsec-0.8
 *
 * Revision 0.3  1996/11/20 14:51:32  ji
 * Fixed problems with #include paths.
 * Changed (incorrect) references to ipsp into ipsec.
 *
 * Revision 0.2  1996/11/08 15:46:29  ji
 * First limited release.
 *
 *
 */
