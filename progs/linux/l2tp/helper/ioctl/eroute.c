/*
 * manipulate eroutes
 * Copyright (C) 1996  John Ioannidis.
 * Copyright (C) 1998, 1999  Richard Guy Briggs.
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

char eroute_c_version[] = "RCSID $Id: eroute.c,v 1.30 2000/01/22 23:22:46 rgb Exp $";


#include <sys/types.h>
#include <linux/types.h> /* new */
#include <string.h>
#include <errno.h>
#include <stdlib.h> /* system(), strtoul() */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netdb.h>


#include <unistd.h>
#include <freeswan.h>
#include <linux/autoconf.h>	/* CONFIG_IPSEC_PFKEYv2 */
#ifdef CONFIG_IPSEC_PFKEYv2
     #include <signal.h>
     #include <pfkeyv2.h>
     #include <pfkey.h>
#endif /* CONFIG_IPSEC_PFKEYv2 */
#include "radij.h"
#include "ipsec_encap.h"
#include "ipsec_netlink.h"


#include <stdio.h>
#include <getopt.h>

char *program_name;
extern char *optarg;
extern int optind, opterr, optopt;
char *edst_opt, *spi_opt, *proto_opt, *said_opt, *dst_opt, *src_opt;
		
#ifdef CONFIG_IPSEC_PFKEYv2
extern unsigned int pfkey_lib_debug; /* used by libfreeswan/pfkey_v2_build */
int pfkey_sock;
fd_set pfkey_socks;
uint32_t pfkey_seq = 0;
#endif /* CONFIG_IPSEC_PFKEYv2 */

static void
usage(char* arg)
{
	fprintf(stdout, "usage: %s --{add,replace} --src <src>/<srcmaskbits>|<srcmask> --dst <dst>/<dstmaskbits>|<dstmask> <SA>\n", arg);
	fprintf(stdout, "            where <SA> is '--edst <edst> --spi <spi> --proto <proto>' OR '--said <said>' OR '--said %%passthrough'.\n");
	fprintf(stdout, "       %s --del --src <src>/<srcmaskbits>|<srcmask> --dst <dst>/<dstmaskbits>|<dstmask>\n", arg);
	fprintf(stdout, "       %s --clear\n", arg);
	fprintf(stdout, "       %s --help\n", arg);
	fprintf(stdout, "       %s --version\n", arg);
	fprintf(stdout, "        [ --debug ] is optional to any %s command.\n", arg);
exit(1);
}

#if 0
static int
resolve_ip(char *arg, struct in_addr *ip)
{
	struct hostent *hostp;
	struct netent *netp;
	
	if (!strcmp(arg, "default")) {/* default is really 0.0.0.0 */
		ip->s_addr = htonl(INADDR_ANY);
		return(1);
	}
	if (!(strcmp(arg, "255.255.255.255"))) {
		/* inet_addr() returns -1 for this, as it should, but that means an error */
		ip->s_addr = htonl(INADDR_BROADCAST);
		return (1);
	}
	if ((netp = getnetbyname(arg)) != (struct netent *)NULL) {
		ip->s_addr = htonl(netp->n_net);
		return(1);
	}
	if ((hostp = gethostbyname(arg)) == (struct hostent *)NULL) {
		errno = h_errno;
		return(-1);
	}
	memcpy((char *) ip, (char *) hostp->h_addr_list[0], hostp->h_length);
	return(0);
}
#endif

static struct option const longopts[] =
{
	{"dst", 1, 0, 'D'},
	{"src", 1, 0, 'S'},
	{"add", 0, 0, 'a'},
	{"replace", 0, 0, 'r'},
	{"clear", 0, 0, 'c'},
	{"del", 0, 0, 'd'},
	{"edst", 1, 0, 'e'},
	{"proto", 1, 0, 'p'},
	{"help", 0, 0, 'h'},
	{"spi", 1, 0, 's'},
	{"said", 1, 0, 'I'},
	{"version", 0, 0, 'v'},
	{"label", 1, 0, 'l'},
	{"optionsfrom", 1, 0, '+'},
	{"debug", 0, 0, 'g'},
	{0, 0, 0, 0}
};

int
main(int argc, char **argv)
{
/*	int fd; */
	struct encap_msghdr emh;
	char *endptr;
/*	int ret; */
	int c, previous = -1;
	const char* error_s;
	int debug = 0;

#ifdef CONFIG_IPSEC_PFKEYv2
	int error = 0;
	int i;

	char ipaddr_txt[ADDRTOA_BUF];                
	struct sadb_ext *extensions[SADB_EXT_MAX + 1];
	struct sadb_msg *pfkey_msg;
	struct sockaddr_in pfkey_address_s_ska;
	struct sockaddr_in pfkey_address_d_ska;
	struct sockaddr_in pfkey_address_sflow_ska;
	struct sockaddr_in pfkey_address_dflow_ska;
	struct sockaddr_in pfkey_address_smask_ska;
	struct sockaddr_in pfkey_address_dmask_ska;
#else /* CONFIG_IPSEC_PFKEYv2 */
	int fd = 0;
#endif /* CONFIG_IPSEC_PFKEYv2 */
	
	if(argc == 1) {
		system("cat /proc/net/ipsec_eroute");
		exit(0);
	}

	program_name = argv[0];
	memset(&emh, 0, sizeof(emh));

	while((c = getopt_long(argc, argv, "acdD:e:hprs:S:vl:+:g", longopts, 0)) != EOF)
	{
		switch(c) {
		case 'g':
			debug = 1;
#ifdef CONFIG_IPSEC_PFKEYv2
			pfkey_lib_debug = 1;
#endif /* CONFIG_IPSEC_PFKEYv2 */
			break;
		case 'a':
			if(emh.em_type) {
				fprintf(stderr, "%s: Only one of '--add', '--replace', '--clear', or '--del' options permitted.\n",
					program_name);
				exit(1);
			}
			emh.em_type = EMT_SETEROUTE;
			break;
		case 'r':
			if(emh.em_type) {
				fprintf(stderr, "%s: Only one of '--add', '--replace', '--clear', or '--del' options permitted.\n",
					program_name);
				exit(1);
			}
			emh.em_type = EMT_RPLACEROUTE;
			break;
		case 'c':
			if(emh.em_type) {
				fprintf(stderr, "%s: Only one of '--add', '--replace', '--clear', or '--del' options permitted.\n",
					program_name);
				exit(1);
			}
			emh.em_type = EMT_CLREROUTE;
			break;
		case 'd':
			if(emh.em_type) {
				fprintf(stderr, "%s: Only one of '--add', '--replace', '--clear', or '--del' options permitted.\n",
					program_name);
				exit(1);
			}
			emh.em_type = EMT_DELEROUTE;
			break;
		case 'e':
			if(said_opt) {
				fprintf(stderr, "%s: Error, EDST parameter redefined:%s, already defined in SA:%s\n",
					program_name, optarg, said_opt);
				exit (1);
			}				
			if(edst_opt) {
				fprintf(stderr, "%s: Error, EDST parameter redefined:%s, already defined as:%s\n",
					program_name, optarg, edst_opt);
				exit (1);
			}				
#if 0
			if((ret = resolve_ip(optarg, &(emh.em_erdst))) != 0) {
				if(ret == 1) {
					fprintf(stderr, "%s: Destination address cannot be a network: %s\n",
						program_name, optarg);
					exit (1);
				} else {
					fprintf(stderr, "%s: Invalid formation of destination address: %s\n",
						program_name, optarg);
					exit (1);
				}
			}
#endif
			if((error_s = atoaddr(optarg, 0, &(emh.em_erdst))) != 0) {
				if(error_s) {
					fprintf(stderr, "%s: Error, %s converting --edst argument:%s\n",
						program_name, error_s, optarg);
					exit (1);
				}
			}
			edst_opt = optarg;
			break;
		case 'h':
		case '?':
			usage(program_name);
			exit(1);
		case 's':
			if(said_opt) {
				fprintf(stderr, "%s: Error, SPI parameter redefined:%s, already defined in SA:%s\n",
					program_name, optarg, said_opt);
				exit (1);
			}				
			if(spi_opt) {
				fprintf(stderr, "%s: Error, SPI parameter redefined:%s, already defined as:%s\n",
					program_name, optarg, spi_opt);
				exit (1);
			}				
			emh.em_erspi = htonl(strtoul(optarg, &endptr, 0));
			if(!(endptr == optarg + strlen(optarg))) {
				fprintf(stderr, "%s: Invalid character in SPI parameter: %s\n",
					program_name, optarg);
				exit (1);
			}
			if(emh.em_erspi < 0x100) {
				fprintf(stderr, "%s: Illegal reserved spi: %s => 0x%x Must be larger than 0x100.\n",
					program_name, optarg, emh.em_erspi);
				exit(1);
			}
			spi_opt = optarg;
			break;
		case 'p':
			if(said_opt) {
				fprintf(stderr, "%s: Error, PROTO parameter redefined:%s, already defined in SA:%s\n",
					program_name, optarg, said_opt);
				exit (1);
			}				
			if(proto_opt) {
				fprintf(stderr, "%s: Error, PROTO parameter redefined:%s, already defined as:%s\n",
					program_name, optarg, proto_opt);
				exit (1);
			}
#if 0
			if(emh.em_erproto) {
				fprintf(stderr, "%s: Warning, PROTO parameter redefined:%s\n",
					program_name, optarg);
				exit (1);
			}
#endif
			if(!strcmp(optarg, "ah"))
				emh.em_erproto = SA_AH;
			if(!strcmp(optarg, "esp"))
				emh.em_erproto = SA_ESP;
			if(!strcmp(optarg, "tun"))
				emh.em_erproto = SA_IPIP;
			if(emh.em_erproto == 0) {
				fprintf(stderr, "%s: Invalid PROTO parameter: %s\n",
					program_name, optarg);
				exit (1);
			}
			proto_opt = optarg;
			break;
		case 'I':
			if(said_opt) {
				fprintf(stderr, "%s: Error, SAID parameter redefined:%s, already defined in SA:%s\n",
					program_name, optarg, said_opt);
				exit (1);
			}				
			if(proto_opt) {
				fprintf(stderr, "%s: Error, PROTO parameter redefined in SA:%s, already defined as:%s\n",
					program_name, optarg, proto_opt);
				exit (1);
			}
			if(edst_opt) {
				fprintf(stderr, "%s: Error, EDST parameter redefined in SA:%s, already defined as:%s\n",
					program_name, optarg, edst_opt);
				exit (1);
			}
			if(proto_opt) {
				fprintf(stderr, "%s: Error, SPI parameter redefined in SA:%s, already defined as:%s\n",
					program_name, optarg, spi_opt);
				exit (1);
			}
			if((error_s = atosa(optarg, 0, &emh.em_ersaid)) != NULL) {
				fprintf(stderr, "%s: Error, %s converting --sa argument:%s\n",
					program_name, error_s, optarg);
				exit (1);
			} else if((emh.em_erspi < 0x100) && (emh.em_erspi > 0x0)){
				fprintf(stderr, "%s: Illegal reserved spi: %s => 0x%x Must be larger than or equal to 0x100.\n",
					program_name, optarg, emh.em_erspi);
				exit(1);
			}
			said_opt = optarg;
			break;
		case 'v':
			printf("%s, %s\n", program_name, eroute_c_version);
			exit(1);
		case 'D':
			if(dst_opt) {
				fprintf(stderr, "%s: Error, EDST parameter redefined:%s, already defined as:%s\n",
					program_name, optarg, dst_opt);
				exit (1);
			}				
			if (atosubnet(optarg, 0, &emh.em_eaddr.sen_ip_dst, &emh.em_emask.sen_ip_dst)) {
				fprintf(stderr, "%s: Invalid formation of destination network/mask: %s\n",
					program_name, optarg);
				exit (1);
			}
			dst_opt = optarg;
			break;
		case 'S':
			if(src_opt) {
				fprintf(stderr, "%s: Error, EDST parameter redefined:%s, already defined as:%s\n",
					program_name, optarg, src_opt);
				exit (1);
			}				
			if (atosubnet(optarg, 0, &emh.em_eaddr.sen_ip_src, &emh.em_emask.sen_ip_src)) {
				fprintf(stderr, "%s: Invalid formation of source network/mask: %s\n",
					program_name, optarg);
				exit (1);
			}
			src_opt = optarg;
			break;
		case 'l':
			program_name = optarg;
			break;
		case '+': /* optionsfrom */
			optionsfrom(optarg, &argc, &argv, optind, stderr);
			/* no return on error */
			break;
		default:
		}
		previous = c;
	}

	if(debug) {
		printf("%s: DEBUG: argc=%d\n", program_name, argc);
	}

	/* Sanity checks */

	switch(emh.em_type) {
	case EMT_SETEROUTE:
	case EMT_RPLACEROUTE:
		if(!(edst_opt && spi_opt && proto_opt) && !(said_opt)) {
			fprintf(stderr, "%s: add option must have SA specified.\n",
				program_name);
			exit(1);
		}
	case EMT_DELEROUTE:
		if(!src_opt) {
			fprintf(stderr, "%s: Error -- %s option '--src' is required.\n",
				program_name, (emh.em_type == EMT_SETEROUTE) ? "add" : "del");
			exit(1);
		}
		if(!emh.em_eaddr.sen_ip_src.s_addr || !emh.em_emask.sen_ip_src.s_addr) {
			fprintf(stderr, "%s: warning -- %s option '--src' is 0.0.0.0.  If the <default address> was not intentional, then a name lookup failed.\n",
				program_name, (emh.em_type == EMT_SETEROUTE) ? "add" : "del");
/*			exit(1); */
		}
		if(!dst_opt) {
			fprintf(stderr, "%s: Error -- %s option '--dst' is required.\n",
				program_name, (emh.em_type == EMT_SETEROUTE) ? "add" : "del");
			exit(1);
		}
		if(!emh.em_eaddr.sen_ip_dst.s_addr || !emh.em_emask.sen_ip_dst.s_addr) {
			fprintf(stderr, "%s: warning -- %s option '--dst' is 0.0.0.0.  If the <default address> was not intentional, then a name lookup failed.\n",
				program_name, (emh.em_type == EMT_SETEROUTE) ? "add" : "del");
/*			exit(1); */
		}
	case EMT_CLREROUTE:
		emh.em_magic = EM_MAGIC;
		emh.em_msglen= sizeof emh;
		emh.em_version = 0;
		emh.em_eaddr.sen_len = emh.em_emask.sen_len = sizeof (struct sockaddr_encap);
		emh.em_eaddr.sen_family = emh.em_emask.sen_family = AF_ENCAP;
		emh.em_eaddr.sen_type = SENT_IP4;
		emh.em_emask.sen_type = 255;
		break;
	default:
		fprintf(stderr, "%s: exactly one of '--add', '--replace', '--del' or '--clear' options must be specified.\n"
			"Try %s --help' for usage information.\n",
			program_name, program_name);
		exit(1);
	}

#ifdef CONFIG_IPSEC_PFKEYv2
	if((pfkey_sock = socket(PF_KEY, SOCK_RAW, PF_KEY_V2) ) < 0) {
		fprintf(stderr, "%s: Trouble openning PF_KEY family socket with error: ",
			program_name);
		switch(errno) {
		case ENOENT:
			fprintf(stderr, "device does not exist.  See FreeS/WAN installation procedure.\n");
			break;
		case EACCES:
			fprintf(stderr, "access denied.  ");
			if(getuid() == 0) {
				fprintf(stderr, "Check permissions.  Should be 600.\n");
			} else {
				fprintf(stderr, "You must be root to open this file.\n");
			}
			break;
		case EUNATCH:
			fprintf(stderr, "Netlink not enabled OR KLIPS not loaded.\n");
			break;
		case ENODEV:
			fprintf(stderr, "KLIPS not loaded or enabled.\n");
			break;
		case EBUSY:
			fprintf(stderr, "KLIPS is busy.  Most likely a serious internal error occured in a previous command.  Please report as much detail as possible to development team.\n");
			break;
		case EINVAL:
			fprintf(stderr, "Invalid argument, KLIPS not loaded or check kernel log messages for specifics.\n");
			break;
		case ENOBUFS:
			fprintf(stderr, "No kernel memory to allocate SA.\n");
			break;
		case ESOCKTNOSUPPORT:
			fprintf(stderr, "Algorithm support not available in the kernel.  Please compile in support.\n");
			break;
		case EEXIST:
			fprintf(stderr, "SA already in use.  Delete old one first.\n");
			break;
		case ENXIO:
			fprintf(stderr, "SA does not exist.  Cannot delete.\n");
			break;
		default:
			fprintf(stderr, "Unknown file open error %d.  Please report as much detail as possible to development team.\n", errno);
		}
		exit(1);
	}

	/* Build an SADB_X_ADDFLOW or SADB_X_DELFLOW message to send down. */
	/* It needs <base, SA, address(SD), flow(SD), mask(SD)> minimum. */
	pfkey_extensions_init(extensions);
	if((error = pfkey_msg_hdr_build(&extensions[0],
					(emh.em_type == EMT_SETEROUTE ||
					 emh.em_type == EMT_RPLACEROUTE) ?
					SADB_X_ADDFLOW : SADB_X_DELFLOW,
					proto2satype(emh.em_erproto),
#if 0
					(emh.em_erproto == SA_ESP ? SADB_SATYPE_ESP :
					 (emh.em_erproto == SA_AH ? SADB_SATYPE_AH :
					  (emh.em_erproto == SA_IPIP ? SADB_X_SATYPE_IPIP :
					   SADB_SATYPE_UNSPEC))),
#endif
					0,
					++pfkey_seq,
					getpid()))) {
		fprintf(stderr, "%s: Trouble building message header, error=%d.\n",
			program_name, error);
		pfkey_extensions_free(extensions);
		exit(1);
	}

	switch(emh.em_type) {
	case EMT_SETEROUTE:
	case EMT_RPLACEROUTE:
	case EMT_CLREROUTE:
		if((error = pfkey_sa_build(&extensions[SADB_EXT_SA],
					   SADB_EXT_SA,
					   emh.em_erspi, /* in network order */
					   0,
					   0,
					   0,
					   0,
					   (emh.em_type == EMT_CLREROUTE) ? SADB_X_SAFLAGS_CLEARFLOW : 0))) {
			fprintf(stderr, "%s: Trouble building sa extension, error=%d.\n",
				program_name, error);
			pfkey_extensions_free(extensions);
			exit(1);
		}
	default:
	}

	switch(emh.em_type) {
	case EMT_SETEROUTE:
	case EMT_RPLACEROUTE:
		pfkey_address_s_ska.sin_addr.s_addr = 0; /* esrc */
		pfkey_address_s_ska.sin_family = AF_INET;
		pfkey_address_s_ska.sin_port = 0;
		for(i = 0; i < sizeof(struct sockaddr_in) - offsetof(struct sockaddr_in, sin_zero); i++) {
			pfkey_address_s_ska.sin_zero[i] = 0;
		}
		if((error = pfkey_address_build(&extensions[SADB_EXT_ADDRESS_SRC],
						SADB_EXT_ADDRESS_SRC,
						0,
						0,
						(struct sockaddr*)&pfkey_address_s_ska))) {
			addrtoa(pfkey_address_s_ska.sin_addr,
				0, ipaddr_txt, sizeof(ipaddr_txt));
			fprintf(stderr, "%s: Trouble building address_s extension (%s), error=%d.\n",
				program_name, ipaddr_txt, error);
			pfkey_extensions_free(extensions);
			exit(1);
		}
		
		pfkey_address_d_ska.sin_addr = emh.em_erdst; /* edst */
		pfkey_address_d_ska.sin_family = AF_INET;
		pfkey_address_d_ska.sin_port = 0;
		for(i = 0; i < sizeof(struct sockaddr_in) - offsetof(struct sockaddr_in, sin_zero); i++) {
			pfkey_address_d_ska.sin_zero[i] = 0;
		}
		if((error = pfkey_address_build(&extensions[SADB_EXT_ADDRESS_DST],
						SADB_EXT_ADDRESS_DST,
						0,
						0,
						(struct sockaddr*)&pfkey_address_d_ska))) {
			addrtoa(pfkey_address_s_ska.sin_addr,
				0, ipaddr_txt, sizeof(ipaddr_txt));
			fprintf(stderr, "%s: Trouble building address_d extension (%s), error=%d.\n",
				program_name, ipaddr_txt, error);
			pfkey_extensions_free(extensions);
			exit(1);
		}
	default:
	}
	
	pfkey_address_sflow_ska.sin_addr/*.s_addr*/ = emh.em_eaddr.sen_ip_src; /* src flow */
	pfkey_address_sflow_ska.sin_family = AF_INET;
	pfkey_address_sflow_ska.sin_port = 0;
	for(i = 0; i < sizeof(struct sockaddr_in) - offsetof(struct sockaddr_in, sin_zero); i++) {
		pfkey_address_sflow_ska.sin_zero[i] = 0;
	}
	if((error = pfkey_address_build(&extensions[SADB_X_EXT_ADDRESS_SRC_FLOW],
					SADB_X_EXT_ADDRESS_SRC_FLOW,
					0,
					0,
					(struct sockaddr*)&pfkey_address_sflow_ska))) {
		addrtoa(pfkey_address_sflow_ska.sin_addr,
			0, ipaddr_txt, sizeof(ipaddr_txt));
		fprintf(stderr, "%s: Trouble building address_sflow extension (%s), error=%d.\n",
			program_name, ipaddr_txt, error);
		pfkey_extensions_free(extensions);
		exit(1);
	}
	
	pfkey_address_dflow_ska.sin_addr = emh.em_eaddr.sen_ip_dst; /* dst flow */
	pfkey_address_dflow_ska.sin_family = AF_INET;
	pfkey_address_dflow_ska.sin_port = 0;
	for(i = 0; i < sizeof(struct sockaddr_in) - offsetof(struct sockaddr_in, sin_zero); i++) {
		pfkey_address_dflow_ska.sin_zero[i] = 0;
	}
	if((error = pfkey_address_build(&extensions[SADB_X_EXT_ADDRESS_DST_FLOW],
					SADB_X_EXT_ADDRESS_DST_FLOW,
					0,
					0,
					(struct sockaddr*)&pfkey_address_dflow_ska))) {
		addrtoa(pfkey_address_sflow_ska.sin_addr,
			0, ipaddr_txt, sizeof(ipaddr_txt));
		fprintf(stderr, "%s: Trouble building address_dflow extension (%s), error=%d.\n",
			program_name, ipaddr_txt, error);
		pfkey_extensions_free(extensions);
		exit(1);
	}
	
	pfkey_address_smask_ska.sin_addr/*.s_addr*/ = emh.em_emask.sen_ip_src; /* src mask */
	pfkey_address_smask_ska.sin_family = AF_INET;
	pfkey_address_smask_ska.sin_port = 0;
	for(i = 0; i < sizeof(struct sockaddr_in) - offsetof(struct sockaddr_in, sin_zero); i++) {
		pfkey_address_smask_ska.sin_zero[i] = 0;
	}
	if((error = pfkey_address_build(&extensions[SADB_X_EXT_ADDRESS_SRC_MASK],
					SADB_X_EXT_ADDRESS_SRC_MASK,
					0,
					0,
					(struct sockaddr*)&pfkey_address_smask_ska))) {
		addrtoa(pfkey_address_smask_ska.sin_addr,
			0, ipaddr_txt, sizeof(ipaddr_txt));
		fprintf(stderr, "%s: Trouble building address_smask extension (%s), error=%d.\n",
			program_name, ipaddr_txt, error);
		pfkey_extensions_free(extensions);
		exit(1);
	}
	
	pfkey_address_dmask_ska.sin_addr = emh.em_emask.sen_ip_dst; /* dst mask */
	pfkey_address_dmask_ska.sin_family = AF_INET;
	pfkey_address_dmask_ska.sin_port = 0;
	for(i = 0; i < sizeof(struct sockaddr_in) - offsetof(struct sockaddr_in, sin_zero); i++) {
		pfkey_address_dmask_ska.sin_zero[i] = 0;
	}
	if((error = pfkey_address_build(&extensions[SADB_X_EXT_ADDRESS_DST_MASK],
					SADB_X_EXT_ADDRESS_DST_MASK,
					0,
					0,
					(struct sockaddr*)&pfkey_address_dmask_ska))) {
		addrtoa(pfkey_address_smask_ska.sin_addr,
			0, ipaddr_txt, sizeof(ipaddr_txt));
		fprintf(stderr, "%s: Trouble building address_dmask extension (%s), error=%d.\n",
			program_name, ipaddr_txt, error);
		pfkey_extensions_free(extensions);
		exit(1);
	}
	
	if((error = pfkey_msg_build(&pfkey_msg, extensions, EXT_BITS_IN))) {
		fprintf(stderr, "%s: Trouble building pfkey message, error=%d.\n",
			program_name, error);
		pfkey_extensions_free(extensions);
		pfkey_msg_free(&pfkey_msg);
		exit(1);
	}

	if((error = write(pfkey_sock,
			  pfkey_msg,
			  pfkey_msg->sadb_msg_len * IPSEC_PFKEYv2_ALIGN)) !=
	   pfkey_msg->sadb_msg_len * IPSEC_PFKEYv2_ALIGN) {
		fprintf(stderr, "%s: pfkey write failed, returning %d with errno=%d.\n",
			program_name, error, errno);
		pfkey_extensions_free(extensions);
		pfkey_msg_free(&pfkey_msg);
		switch(errno) {
		case EINVAL:
			fprintf(stderr, "Invalid argument, check kernel log messages for specifics.\n");
			break;
		case ENXIO:
			if((emh.em_type == EMT_SETEROUTE) ||
			   (emh.em_type == EMT_RPLACEROUTE)) {
				fprintf(stderr, "Invalid mask.\n");
				break;
			}
			if(emh.em_type == EMT_DELEROUTE) {
				fprintf(stderr, "Mask not found.\n");
				break;
			}
		case EFAULT:
			if((emh.em_type == EMT_SETEROUTE) ||
			   (emh.em_type == EMT_RPLACEROUTE)) {
				fprintf(stderr, "Invalid address.\n");
				break;
			}
			if(emh.em_type == EMT_DELEROUTE) {
				fprintf(stderr, "Address not found.\n");
				break;
			}
		case EACCES:
			fprintf(stderr, "access denied.  ");
			if(getuid() == 0) {
				fprintf(stderr, "Check permissions.  Should be 600.\n");
			} else {
				fprintf(stderr, "You must be root to open this file.\n");
			}
			break;
		case EUNATCH:
			fprintf(stderr, "Netlink not enabled OR KLIPS not loaded.\n");
			break;
		case EBUSY:
			fprintf(stderr, "KLIPS is busy.  Most likely a serious internal error occured in a previous command.  Please report as much detail as possible to development team.\n");
			break;
		case ENODEV:
			fprintf(stderr, "KLIPS not loaded or enabled.\n");
			fprintf(stderr, "No device?!?\n");
			break;
		case ENOBUFS:
			fprintf(stderr, "No kernel memory to allocate SA.\n");
			break;
		case ESOCKTNOSUPPORT:
			fprintf(stderr, "Algorithm support not available in the kernel.  Please compile in support.\n");
			break;
		case EEXIST:
			fprintf(stderr, "SA already in use.  Delete old one first.\n");
			break;
		case ENOENT:
			fprintf(stderr, "device does not exist.  See FreeS/WAN installation procedure.\n");
			break;
		default:
			fprintf(stderr, "Unknown socket write error %d.  Please report as much detail as possible to development team.\n", errno);
		}
/*		fprintf(stderr, "%s: socket write returned errno %d\n",
		program_name, errno);*/
		exit(1);
	}

	if(pfkey_msg) {
		pfkey_extensions_free(extensions);
		pfkey_msg_free(&pfkey_msg);
	}

	(void) close(pfkey_sock);  /* close the socket */
#else /* CONFIG_IPSEC_PFKEYv2 */

	fd = open("/dev/ipsec", O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "%s: Could not open /dev/ipsec -- ", program_name);
		switch(errno) {
		case ENOENT:
			fprintf(stderr, "device does not exist.  See FreeS/WAN installation procedure.\n");
		case EACCES:
			fprintf(stderr, "access denied.  ");
			if(getuid() == 0) {
				fprintf(stderr, "Check permissions.  Should be 600.\n");
			} else {
				fprintf(stderr, "You must be root to open this file.\n");
			}
			break;
		case EUNATCH:
			fprintf(stderr, "Netlink not enabled OR KLIPS not loaded.\n");
			break;
		case ENODEV:
			fprintf(stderr, "KLIPS not loaded or enabled.\n");
			break;
		case EBUSY:
			fprintf(stderr, "KLIPS is busy.  Most likely a serious internal error occured in a previous command.  Please report as much detail as possible to development team.\n");
			break;
		default:
			fprintf(stderr, "Unknown file open error %d.  Please report as much detail as possible to development team.\n", errno);
		}
		exit(1);
	}

	if(debug) {
		printf("%s: DEBUG: open ok\n", program_name);
	}

	if (write(fd, &emh, sizeof emh) != sizeof emh) {
		fprintf(stderr, "%s: Failed -- ", program_name);
		switch(errno) {
		case EINVAL:
			fprintf(stderr, "Invalid argument, check kernel log messages for specifics.\n");
			break;
		case ENXIO:
			if((emh.em_type == EMT_SETEROUTE) ||
			   (emh.em_type == EMT_RPLACEROUTE)) {
				fprintf(stderr, "Invalid mask.\n");
				break;
			}
			if(emh.em_type == EMT_DELEROUTE) {
				fprintf(stderr, "Mask not found.\n");
				break;
			}
		case EFAULT:
			if((emh.em_type == EMT_SETEROUTE) ||
			   (emh.em_type == EMT_RPLACEROUTE)) {
				fprintf(stderr, "Invalid address.\n");
				break;
			}
			if(emh.em_type == EMT_DELEROUTE) {
				fprintf(stderr, "Address not found.\n");
				break;
			}
		case ECONNREFUSED:
			fprintf(stderr, "Connection refused, Is KLIPS, the ipsec module, loaded?\n");
			break;
		default:
			fprintf(stderr, "Unknown file write error %d.  Please report as much detail as possible to development team.\n", errno);
		}
		close(fd);
		exit(1);
	}
	close(fd);
#endif /* CONFIG_IPSEC_PFKEYv2 */

	if(debug) {
		printf("%s: DEBUG: write ok\n", program_name);
	}

	exit(0);
}
/*
 * $Log: eroute.c,v $
 * Revision 1.30  2000/01/22 23:22:46  rgb
 * Use new function proto2satype().
 *
 * Revision 1.29  2000/01/21 09:42:32  rgb
 * Replace resolve_ip() with atoaddr() from freeswanlib.
 *
 * Revision 1.28  2000/01/21 06:22:28  rgb
 * Changed to AF_ENCAP macro.
 * Added --debug switch to command line.
 * Added pfkeyv2 support to completely avoid netlink.
 *
 * Revision 1.27  1999/12/07 18:27:10  rgb
 * Added headers to silence fussy compilers.
 * Converted local functions to static to limit scope.
 *
 * Revision 1.26  1999/11/25 09:07:44  rgb
 * Fixed printf % escape bug.
 * Clarified assignment in conditional with parens.
 *
 * Revision 1.25  1999/11/23 23:06:26  rgb
 * Sort out pfkey and freeswan headers, putting them in a library path.
 *
 * Revision 1.24  1999/06/10 15:55:14  rgb
 * Add error return code.
 *
 * Revision 1.23  1999/04/15 15:37:27  rgb
 * Forward check changes from POST1_00 branch.
 *
 * Revision 1.19.2.2  1999/04/13 20:58:10  rgb
 * Add argc==1 --> /proc/net/ipsec_*.
 *
 * Revision 1.19.2.1  1999/03/30 17:01:36  rgb
 * Make main() return type explicit.
 *
 * Revision 1.22  1999/04/11 00:12:08  henry
 * GPL boilerplate
 *
 * Revision 1.21  1999/04/06 04:54:37  rgb
 * Fix/Add RCSID Id: and Log: bits to make PHMDs happy.  This includes
 * patch shell fixes.
 *
 * Revision 1.20  1999/03/17 15:40:54  rgb
 * Make explicit main() return type of int.
 *
 * Revision 1.19  1999/01/26 05:51:01  rgb
 * Updated to use %passthrough instead of bypass.
 *
 * Revision 1.18  1999/01/22 06:34:52  rgb
 * Update to include SAID command line parameter.
 * Add IPSEC 'bypass' switch.
 * Add error-checking.
 * Cruft clean-out.
 *
 * Revision 1.17  1998/11/29 00:52:26  rgb
 * Add explanation to warning about default source or destination.
 *
 * Revision 1.16  1998/11/12 21:08:03  rgb
 * Add --label option to identify caller from scripts.
 *
 * Revision 1.15  1998/10/27 00:33:27  rgb
 * Make output error text more fatal-sounding.
 *
 * Revision 1.14  1998/10/26 01:28:38  henry
 * use SA_* protocol names, not IPPROTO_*, to avoid compile problems
 *
 * Revision 1.13  1998/10/25 02:44:56  rgb
 * Institute more precise error return codes from eroute commands.
 *
 * Revision 1.12  1998/10/19 18:58:55  rgb
 * Added inclusion of freeswan.h.
 * a_id structure implemented and used: now includes protocol.
 *
 * Revision 1.11  1998/10/09 18:47:29  rgb
 * Add 'optionfrom' to get more options from a named file.
 *
 * Revision 1.10  1998/10/09 04:34:58  rgb
 * Changed help output from stderr to stdout.
 * Changed error messages from stdout to stderr.
 * Added '--replace' option.
 * Deleted old commented out cruft.
 *
 * Revision 1.9  1998/08/18 17:18:13  rgb
 * Delete old commented out cruft.
 * Reduce destination and source default subnet to warning, not fatal.
 *
 * Revision 1.8  1998/08/05 22:24:45  rgb
 * Change includes to accomodate RH5.x
 *
 * Revision 1.7  1998/07/29 20:49:08  rgb
 * Change to use 0x-prefixed hexadecimal for spi's.
 *
 * Revision 1.6  1998/07/28 00:14:24  rgb
 * Convert from positional parameters to long options.
 * Add --clean option.
 * Add hostname lookup support.
 *
 * Revision 1.5  1998/07/14 18:13:28  rgb
 * Restructured for better argument checking.
 * Added command to clear the eroute table.
 *
 * Revision 1.4  1998/07/09 18:14:10  rgb
 * Added error checking to IP's and keys.
 * Made most error messages more specific rather than spamming usage text.
 * Added more descriptive kernel error return codes and messages.
 * Converted all spi translations to unsigned.
 * Removed all invocations of perror.
 *
 * Revision 1.3  1998/05/27 18:48:19  rgb
 * Adding --help and --version directives.
 *
 * Revision 1.2  1998/04/13 03:15:29  rgb
 * Commands are now distinguishable from arguments when invoking usage.
 *
 * Revision 1.1.1.1  1998/04/08 05:35:10  henry
 * RGB's ipsec-0.8pre2.tar.gz ipsec-0.8
 *
 * Revision 0.3  1996/11/20 14:51:32  ji
 * Fixed problems with #include paths.
 * Changed (incorrect) references to ipsp into ipsec.
 *
 * Revision 0.2  1996/11/08 15:45:24  ji
 * First limited release.
 *
 *
 */
