/* 
 * message.h
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: message.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 *
 *  Commands
 *  --------
 *  
 *  Each command message may have a cmd_* and rsp_* structure which
 *  define the unique charactistics of the messages message or response
 *  body. Commands are of two types...
 *  
 *  synchronous: msg_* wait for the response to the command and return
 *               the response message. These commands block and therefore
 *               cannot be used atomically or when blocking is not 
 *               permitted. If the response is not receive within one
 *               second, the command returns NULL.
 *				
 *  asynchronous: amsg_* do not wait for a response (none is returned)
 *               and their responses are handled by the interrupt handler.
 */

#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <linux/types.h>
#include "adapter.h"

/* Dimensions of the command and response ring buffers (in messages) */
#define NR_WRITE_BUFS	512
#define NR_READ_BUFS	NR_WRITE_BUFS

/* Pre-defined processor values */
#define PROC_NULL	0x0000
#define PROC_CPU	0x1000
#define PROC_TDM	0xffff

/* Time to wait in secs. for a command response */
#define SR_TIMEOUT	1

/*
   Get the adapter diagnotic information (planned)
*/
#define MSG_DIAG	0xff
struct rsp_diag {
	__u8 diag_code __attribute__((packed));
	__u8 reserved __attribute__((packed));
	__u16 p1_stat __attribute__((packed));
	__u16 p2_stat __attribute__((packed));
	__u16 p3_stat __attribute__((packed));
	__u16 p4_stat __attribute__((packed));
};

#define msg_diag(x)			(sendrcv_message(x, PROC_CPU, 0, MSG_DIAG, NULL, \
								0, SR_TIMEOUT))
#define amsg_diag(x)		(send_message(x, PROC_CPU, 0, 0xff, NULL, 0))

/*
   Get the product model information (planned)
*/
#define MSG_PRODUCTNO	0xfe
struct rsp_productno {
	char model_no[4] __attribute__((packed));
	char nr_ports[3] __attribute__((packed));
	char hw_rev[3] __attribute__((packed));
	char modem_cap __attribute__((packed));
	char isdn_cap __attribute__((packed));
	char fax_cap __attribute__((packed));
	char voice_cap __attribute__((packed));
};

#define msg_productno(x)	(sendrcv_message(x, PROC_CPU, 0, 0xfe, NULL, \
								0, SR_TIMEOUT))
#define amsg_productno(x)	(send_message(x, PROC_CPU, 0, 0xfe, NULL, 0))

/*
   Get the serial number (planned)
*/
#define MSG_SERIALNO	0xfd
struct rsp_serialno {
	char serialno[10] __attribute__((packed));
};

#define msg_serialno(x)		(sendrcv_message(x, PROC_CPU, 0, 0xfd, NULL, \
								0, SR_TIMEOUT))
#define amsg_serialno(x)	(send_message(x, PROC_CPU, 0, 0xfd, NULL, 0))

/*
   Get the current OS software version and date
*/
#define MSG_OSVERSION	0x19
struct rsp_osversion {
	__u8 mod_type __attribute__((packed));	/* for modversion only */
	__u8 reserved __attribute__((packed));
	__u32 version __attribute__((packed));
	__u16 year __attribute__((packed));
	__u8 month __attribute__((packed));
	__u8 day __attribute__((packed));
};

#define msg_osversion(x)	(sendrcv_message(x, PROC_CPU, 0, 0x19, NULL, \
								0, SR_TIMEOUT))
#define amsg_osversion(x)	(send_message(x, PROC_CPU, 0, 0x19, NULL, 0))

/*
   Get the module software version and date
*/
#define MSG_MODVERSION		0x17
#define MODTYPE_MODEM		0
#define MODTYPE_ISDN		1
#define MODTYPE_FAX			2
#define MODTYPE_VOICE		3

struct cmd_modversion {
	__u8 mod_type __attribute__((packed));
};
#define rsp_modversion	rsp_osversion

#define msg_modversion(x,y)	(sendrcv_message(x, PROC_CPU, 0, 0x17, y, \
								sizeof(struct cmd_modversion), SR_TIMEOUT))
#define amsg_modversion(x,y)	(send_message(x, PROC_CPU, 0, 0x17, y, \
								sizeof(struct cmd_modversion)))

/*
   Set the call type for the UPP
*/
#define MSG_CALLTYPE		0x24
#define CALLTYPE_MODEM		0
#define CALLTYPE_ISDN		1
#define CALLTYPE_FAX		2
#define CALLTYPE_VOICE		3

#define CALLTYPE_SUCCESS	0x28
#define CALLTYPE_FAILURE	0xff

struct cmd_calltype {
	__u8 module __attribute__((packed));
};

struct rsp_calltype {
	__u8 module __attribute__((packed));
	__u8 diag_code __attribute__((packed));
};

#define msg_calltype(x,y,z)	(sendrcv_message(x, y, 0, 0x24, z, \
								sizeof(struct cmd_calltype), SR_TIMEOUT))
#define amsg_calltype(x,y,z)	(send_message(x, y, 0, 0x24, z, \
								sizeof(struct cmd_calltype)))

/*
   Shut down a UPP when not in use
*/
#define MSG_POWERDOWN	0x18
#define msg_powerdown(x,y)	(sendrcv_message(x, y, 0, 0x18, NULL, 0, \
								SR_TIMEOUT))
#define amsg_powerdown(x,y)	(send_message(x, y, 0, 0x18, NULL, 0))

/*
   A block of code to be uploaded, used by all upload commands
*/
#define UPLOAD_CONTINUE		1
#define UPLOAD_END			0

struct cmd_upload {
	__u16 size __attribute__((packed));
	__u32 addr __attribute__((packed));
	__u8  cont __attribute__((packed));
	__u8  type __attribute__((packed));
	__u16 reserved __attribute__((packed));
	__u32 mod_size __attribute__((packed));
};

struct rsp_upload {
	__u8 checksum __attribute((packed));
};

/*
   Upload flash code (planned)
*/
#define MSG_FLASH	0xee
#define msg_flash(x,y)		(sendrcv_message(x, PROC_CPU, 0, 0xee, \
								y, sizeof(struct cmd_upload), SR_TIMEOUT))
#define amsg_flash(x,y)		(send_message(x, PROC_CPU, 0, 0xee, \
								y, sizeof(struct cmd_upload)))

/*
   Upload OS code
*/
#define MSG_OSLOAD	0x20
#define msg_osload(x,y)		(sendrcv_message(x, PROC_CPU, 0, 0x20, \
								y, sizeof(struct cmd_upload), SR_TIMEOUT))
#define amsg_osload(x,y)	(send_message(x, PROC_CPU, 0, 0x20, \
								y, sizeof(struct cmd_upload)))

/*
   Upload UPP module code (planned)
*/
#define MSG_UPPLOAD		0x16
#define msg_uppload(x,y)	(sendrcv_message(x, PROC_CPU, 0, 0x16, \
								y, sizeof(struct cmd_upload), SR_TIMEOUT))
#define amsg_uppload(x,y)	(send_message(x, PROC_CPU, 0, 0x16, \
								y, sizeof(struct cmd_upload)))

/*
	Restart the board -> revert to power-on
*/
#define MSG_RESTART		0x15
#define amsg_restart(x)		(send_message(x, PROC_CPU, 0, MSG_RESTART, NULL, 0))

/*
   Boot the loaded OS software
*/
#define MSG_BOOT	0x22
struct rsp_boot {
	char diag_code __attribute__((packed));
};

#define msg_boot(x)			(sendrcv_message(x, PROC_CPU, 0, 0x22, NULL, 0, \
								SR_TIMEOUT))
#define amsg_boot(x)		(send_message(x, PROC_CPU, 0, 0x22, NULL, 0))

/*
   Connect a UPP to a local TDM bus and timeslot
*/
#define MSG_LBATTACH	0xf4
#define LBATTACH_CONNECT	1
#define LBATTACH_DISCONNECT	0

#define LBATTACH_ULAW		0
#define LBATTACH_ALAW		1

struct cmd_lbattach {
	__u8	bank __attribute__((packed));
	__u8	slot __attribute__((packed));
	__u8	mode __attribute__((packed));
	__u8	coding __attribute__((packed));
};

#define rsp_lbattach		cmd_lbattach

#define msg_lbattach(x,y,z,q)	(sendrcv_message(x, y, z, 0xf4, q, \
									sizeof(struct cmd_lbattach), SR_TIMEOUT))
#define amsg_lbattach(x,y,z,q)	(send_message(x, y, z, 0xf4, q, \
									sizeof(struct cmd_lbattach)))

/*
   Connect a local TDM timeslot with an external one
*/
#define MSG_TDMSWITCH	0x04
#define TDMSWITCH_DISABLED	0x0
#define TDMSWITCH_SENDONLY	0x0100
#define TDMSWITCH_RECVONLY	0x0001
#define TDMSWITCH_NORMAL	0x0101

struct cmd_tdmswitch {
	__u8	bank __attribute__((packed));
	__u8	slot __attribute__((packed));
	__u8	ext_src __attribute__((packed));
	__u8	ext_dst __attribute__((packed));
	__u8	ext_src_stream __attribute__((packed));
	__u8	ext_dst_stream __attribute__((packed));
	__u16	enable __attribute__((packed));
};

#define rsp_tdmswitch	cmd_tdmswitch

#define msg_tdmswitch(x,y)	(sendrcv_message(x, PROC_TDM, 0, 0x04, y, \
								sizeof(struct cmd_tdmswitch), SR_TIMEOUT))
#define amsg_tdmswitch(x,y)	(send_message(x, PROC_TDM, 0, 0x04, y, \
								sizeof(struct cmd_tdmswitch)))

/*
   Set the modem speed and modulation type
*/
#define MSG_SPEED		0x05
#define SPEED_V34		0x00
#define SPEED_V32		0x01
#define SPEED_V22B		0x02
#define SPEED_V21		0x03
#define SPEED_BELL212	0x04
#define SPEED_BELL103	0x05
#define SPEED_V23		0x06
#define SPEED_V22		0x07
#define SPEED_AUTO		0xff

#define SPEED_2p4		0x02
#define SPEED_4p8		0x03
#define SPEED_7p2		0x04
#define SPEED_9p6		0x05
#define SPEED_12p0		0x06
#define SPEED_14p4		0x07
#define SPEED_16p8		0x08
#define SPEED_19p2		0x09
#define SPEED_21p6		0x0a
#define SPEED_24p0		0x0b
#define SPEED_26p4		0x0c
#define SPEED_28p8		0x0d
#define SPEED_31p2		0x0e
#define SPEED_33p6		0x0f
#define SPEED_UNKNOWN	0xff

struct cmd_speed {
	__u8 max_speed __attribute__((packed));
	__u8 min_speed __attribute__((packed));
	__u8 modulation __attribute__((packed));
};
#define rsp_speed	cmd_speed

#define msg_speed(x,y,z,q)		(sendrcv_message(x, y, z, 0x05, q, \
									sizeof(struct cmd_speed), SR_TIMEOUT))
#define amsg_speed(x,y,z,q)		(send_message(x, y, z, 0x05, q, \
									sizeof(struct cmd_speed)))

/*
   Set the modem compression option
*/
#define MSG_COMPRESS		0x0b
#define COMPRESS_NONE		0
#define COMPRESS_AUTO		1

struct cmd_compress {
	__u8	type __attribute__((packed));
};
#define rsp_compress	cmd_compress

#define msg_compress(x,y,z,q)	(sendrcv_message(x, y, z, 0x0b, q, \
									sizeof(struct cmd_compress), SR_TIMEOUT))
#define amsg_compress(x,y,z,q)	(send_message(x, y, z, 0x0b, q, \
									sizeof(struct cmd_compress)))

/*
   Set the port framing mode
*/
#define MSG_FRAMING		0x0c
#define FRM_ASYNC_RAW	0x00
#define FRM_SYNC_RAW	0x01
#define FRM_SYNC_PPP	0x02
#define FRM_ASYNC_PPP	0x03

struct cmd_framing {
	__u8 type __attribute__((packed));
};
#define rsp_framing	cmd_framing

#define msg_framing(x,y,z,q)	(sendrcv_message(x, y, z, 0x0c, q, \
									sizeof(struct cmd_framing), SR_TIMEOUT))
#define amsg_framing(x,y,z,q)	(send_message(x, y, z, 0x0c, q, \
									sizeof(struct cmd_framing)))

/*
   Set the ACCM 
*/
#define MSG_TXACCM	0x06
#define MSG_RXACCM	0x07
struct cmd_accm {
	__u32	accm __attribute((packed));
};
#define rsp_accm	cmd_accm

#define msg_txaccm(x,y,z,q)		(sendrcv_message(x, y, z, 0x06, q, \
									sizeof(struct cmd_accm), SR_TIMEOUT))
#define amsg_txaccm(x,y,z,q)	(send_message(x, y, z, 0x06, q, \
									sizeof(struct cmd_accm)))
#define msg_rxaccm(x,y,z,q)		(sendrcv_message(x, y, z, 0x07, q, \
									sizeof(struct cmd_accm), SR_TIMEOUT))
#define amsg_rxaccm(x,y,z,q)	(send_message(x, y, z, 0x07, q, \
									sizeof(struct cmd_accm)))

/*
   Begin negotiating on an open timeslot
*/
#define MSG_CONNECT	0x08
#define ORIGINATE	0
#define ANSWER		1

struct cmd_connect {
	char mode __attribute__((packed));
};
#define rsp_connect	cmd_connect

#define msg_connect(x,y,z,q)	(sendrcv_message(x, y, z, 0x08, q, \
									sizeof(struct cmd_connect), SR_TIMEOUT))
#define amsg_connect(x,y,z,q)	(send_message(x, y, z, 0x08, q, \
									sizeof(struct cmd_connect)))

/*
   Begin a digital ISDN call
*/
#define MSG_ISDN	0x38
#define ISDN_64		0
#define ISDN_56		1

struct cmd_isdn {
	char mode __attribute__((packed));
};
#define rsp_isdn	cmd_isdn

#define msg_isdn(x,y,z,q)		(sendrcv_message(x, y, z, 0x38, q, \
									sizeof(struct cmd_isdn), SR_TIMEOUT))
#define amsg_isdn(x,y,z,q)		(send_message(x, y, z, 0x38, q, \
									sizeof(struct cmd_isdn)))

/*
   Disconnect a port after all TX buffers are sent
*/
#define MSG_DISCONNECT	0x09
struct rsp_disconnect {
	char tx_flushed __attribute__((packed));	/* drop only */
	char rx_flushed __attribute__((packed));
};

#define msg_disconnect(x,y,z)	(sendrcv_message(x, y, z, 0x09, NULL, 0, \
									SR_TIMEOUT))
#define amsg_disconnect(x,y,z)	(send_message(x, y, z, 0x09, NULL, 0))

/*
   Disconnect a port NOW!
*/
#define MSG_DROP	0x0d
#define msg_drop(x,y,z)			(sendrcv_message(x, y, z, 0x0d, NULL, 0, \
									SR_TIMEOUT))
#define amsg_drop(x,y,z)		(send_message(x, y, z, 0x0d, NULL, 0))

/*
   Set the AHM message mask
*/
#define MSG_AHMMASK		 	0x1a
#define AHMMASK_DROP		1
#define AHMMASK_CONNECT		2
#define AHMMASK_LINK		4
#define AHMMASK_RETRAIN		8

struct cmd_ahmmask {
	__u16 reserved __attribute__((packed));
	__u16 mask __attribute__((packed));
};
#define rsp_ahmmask	cmd_ahmmask

#define msg_ahmmask(x,y,z,q)	(sendrcv_message(x, y, z, 0x1a, q, \
									sizeof(struct cmd_ahmmask), SR_TIMEOUT))
#define amsg_ahmmask(x,y,z,q)	(send_message(x, y, z, 0x1a, q, \
									sizeof(struct cmd_ahmmask)))
	
/*
   Send data
*/
#define MSG_SEND		0x0f
#define FRIND_BEGIN		0x0001
#define FRIND_CONTINUE	0x0000
#define FRIND_END		0xf100

struct cmd_send {
	__u16 size __attribute__((packed));
	__u16 frame_ind __attribute__((packed));
	__u16 reserved __attribute__((packed));
	__u32 addr __attribute__((packed));
};

struct rsp_send {
	__u16 size __attribute__((packed));
	__u16 frame_ind __attribute__((packed));
	__u16 reserved __attribute__((packed));
	__u32 free_space __attribute__((packed));
};

#define msg_send(x,y,z,q)		(sendrcv_message(x, y, z, 0x0f, q, \
									sizeof(struct cmd_send), SR_TIMEOUT))
#define amsg_send(x,y,z,q)		(send_message(x, y, z, 0x0f, q, \
									sizeof(struct cmd_send)))

/*
   Receive data
*/
#define MSG_RECEIVE		0x10
#define FRIND_ENDCRC	0xf301
#define FRIND_ENDCRCERR	0xf311

struct cmd_receive {
	__u16 size __attribute__((packed));
	__u32 addr __attribute__((packed));
};

struct rsp_receive {
	__u16 size __attribute__((packed));
	__u16 frame_ind __attribute__((packed));
	__u16 reserved __attribute__((packed));
	__u16 pending __attribute__((packed));
};

#define msg_receive(x,y,z,q)	(sendrcv_message(x, y, z, 0x10, q, \
									sizeof(struct cmd_receive), SR_TIMEOUT))
#define amsg_receive(x,y,z,q)	(send_message(x, y, z, 0x10, q, \
									sizeof(struct cmd_receive)))

/*
	Cancel buffers pending on a UPP
*/
#define MSG_CANCEL	0x25
#define CANCEL_ALL	0
#define CANCEL_TX	1
#define CANCEL_RX	2

struct cmd_cancel {
	char mode __attribute__((packed));
};

struct rsp_cancel {
	char nr_tx __attribute__((packed));
	char nr_rx __attribute__((packed));
	char mode __attribute__((packed));
};

#define msg_cancel(x,y,z,q)		(sendrcv_message(x, y, z, 0x25, q, \
									sizeof(struct cmd_cancel), SR_TIMEOUT))
#define amsg_cancel(x,y,z,q)	(send_message(x, y, z, 0x25, q, \
									sizeof(struct cmd_cancel)))

/*
   Get Modem line state
*/
#define MSG_LINESTAT		0x02
#define LINE_IDLE			0
#define LINE_HANDSHK		1
#define LINE_CONNECTED		2
#define LINE_DISCONNECTING	3
#define LINE_RETRAINING		4

struct rsp_linestat {
	__u8 status __attribute__((packed));
};

#define msg_linestat(x,y,z)		(sendrcv_message(x, y, z, 0x02, NULL, 0, \
									SR_TIMEOUT))
#define amsg_linestat(x,y,z)	(send_message(x, y, z, 0x02, NULL, 0))

/*
   Get modem status
*/
#define MSG_MODEMSTAT	0x01
struct rsp_modemstat {
	__u8 mod_type __attribute__((packed));
	__u8 rx_speed __attribute__((packed));
	__u8 tx_speed __attribute__((packed));
	__u8 quality __attribute__((packed));
	__u8 nr_renegs __attribute__((packed));
	__u8 nr_retrains __attribute__((packed));
};

#define msg_modemstat(x, y, z)	(sendrcv_message(x, y, z, 0x01, NULL, 0, \
									SR_TIMEOUT))
#define amsg_modemstat(x, y, z)	(send_message(x, y, z, 0x01, NULL, 0))

/*
   Get error correction and compression stats
*/
#define MSG_ECDCSTAT		0x03
#define ECDCTYPE_UNKNOWN	0
#define ECDCTYPE_NONE		1
#define ECDCTYPE_V42		2
#define ECDCTYPE_V42BIS		3
#define ECDCTYPE_ERROR		6

struct rsp_ecdcstat {
	__u16 ber __attribute__((packed));
	__u8 ratio __attribute__((packed));
	__u8 link_stat __attribute__((packed));
};

#define msg_ecdcstat(x,y,z)		(sendrcv_message(x, y, z, 0x03, NULL, 0, \
									SR_TIMEOUT))
#define amsg_ecdcstat(x,y,z)	(sendrcv_message(x, y, z, 0x03, NULL, 0))

/*
   Error message
*/
#define MSG_ERROR		0x0e
#define ERR_NOBUF		33
#define ERR_INCAPABLE	34
#define ERR_SUCCESS		40
#define ERR_IMAGESIZE	41
#define ERR_IMAGEEND	42

struct rsp_error {
	__u8 diag_code __attribute__((packed));
	__u8 command __attribute__((packed));
};

/*
   General Autonomous Host Message types and constants
*/
#define MSG_AHM			0x0a
#define AHM_DROP		1
#define AHM_CONNECT		2
#define AHM_LINK		3
#define AHM_RETRAIN		4

struct rsp_ahm {
	char type __attribute__((packed));
	struct rsp_modemstat modem_stats __attribute__((packed));
	struct rsp_ecdcstat ecdc_stats __attribute__((packed));
};

/*
   The basic message structure
*/
typedef struct message {
	__u16	seq_no __attribute__((packed));
	__u16	reserved1 __attribute__((packed));
	__u16	proc_id __attribute__((packed));
	__u8	process __attribute__((packed));
	__u8	reserved2 __attribute__((packed));
	__u8	count __attribute__((packed));
	__u8	type __attribute__((packed));
	union {
		unsigned char raw[22] __attribute__((packed));
		struct cmd_modversion modversion __attribute__((packed));
		struct cmd_calltype calltype __attribute__((packed));
		struct cmd_upload upload __attribute__((packed));
		struct cmd_lbattach lbattach __attribute__((packed));
		struct cmd_tdmswitch tdmswitch __attribute__((packed));
		struct cmd_speed speed __attribute__((packed));
		struct cmd_compress compress __attribute__((packed));
		struct cmd_framing framing __attribute__((packed));
		struct cmd_accm accm __attribute__((packed));
		struct cmd_connect connect __attribute__((packed));
		struct cmd_isdn isdn __attribute__((packed));
		struct cmd_ahmmask ahmmask __attribute__((packed));
		struct cmd_send send __attribute__((packed));
		struct cmd_receive receive __attribute__((packed));
		struct cmd_cancel cancel __attribute__((packed));
	} u __attribute__((packed));
} message_t;

typedef struct response {
	__u16	seq_no __attribute__((packed));
	__u16	seq_echo __attribute__((packed));
	__u16	proc_id __attribute__((packed));
	__u8	process __attribute__((packed));
	__u8	reserved __attribute__((packed));
	__u8	count __attribute__((packed));
	__u8	type __attribute__((packed));
	union {
		unsigned char raw[22] __attribute__((packed));
		struct rsp_diag diag __attribute__((packed));
		struct rsp_productno productno __attribute__((packed));
		struct rsp_serialno serialno __attribute__((packed));
		struct rsp_osversion osversion __attribute__((packed));
		struct rsp_modversion modversion __attribute__((packed));
		struct rsp_calltype calltype __attribute__((packed));
		struct rsp_upload upload __attribute__((packed));
		struct rsp_boot boot __attribute__((packed));
		struct rsp_lbattach lbattach __attribute__((packed));
		struct rsp_tdmswitch tdmswitch __attribute__((packed));
		struct rsp_speed speed __attribute__((packed));
		struct rsp_compress compress __attribute__((packed));
		struct rsp_framing framing __attribute__((packed));
		struct rsp_accm accm __attribute__((packed));
		struct rsp_connect connect __attribute__((packed));
		struct rsp_isdn isdn __attribute__((packed));
		struct rsp_disconnect disconnect __attribute__((packed));
		struct rsp_ahm ahm __attribute__((packed));
		struct rsp_ahmmask ahmmask __attribute__((packed));
		struct rsp_send send __attribute__((packed));
		struct rsp_receive receive __attribute__((packed));
		struct rsp_cancel cancel __attribute__((packed));
		struct rsp_linestat linestat __attribute__((packed));
		struct rsp_modemstat modemstat __attribute__((packed));
		struct rsp_ecdcstat ecdcstat __attribute__((packed));
		struct rsp_error error __attribute__((packed));
	} u __attribute__((packed));
} response_t;

/*
   Message send/receive functions implemented in message.c
*/
extern unsigned int send_message(adapter_t *, __u16, __u8, __u8, void *, __u8);
extern response_t *receive_message(adapter_t *);
extern response_t *sendrcv_message(adapter_t *, __u16, __u8, __u8, void *, 
	__u8, int);

#endif
