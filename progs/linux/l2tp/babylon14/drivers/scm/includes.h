/*
 * includes.h
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: includes.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#ifdef MODULE
	#include <linux/module.h>
#endif
#include <linux/version.h>
#include <linux/errno.h>
#include <asm/segment.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/malloc.h>
#include <linux/mm.h>
#include <linux/string.h>
#include <linux/ioport.h>
#include <linux/timer.h>
#include <linux/wait.h>
#include <linux/types.h>
#include <linux/config.h>
#include <linux/pci.h>
#include <linux/bios32.h>

#if !defined(bool) && !defined(HAVE_BOOL)
	#define bool	char
	#ifndef true
		#define false	(0)
		#define true	(!false)
	#endif
#endif

#ifndef MIN
#define MIN(x,y)		((x>y) ? y : x)
#endif

#ifndef MAX
#define MAX(x,y)		((x<y) ? y : x)
#endif

#endif
