# Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
# $Id: scmd.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
# Released under the GNU Public License. See LICENSE file for details.


#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>

#include <stdio.h>
#include <string.h>

#include "../scb/scioc.h"
#include "../scb/message.h"

enum Reqs { ReqGetChan, ReqConnect, ReqConnecting, ReqDisconnect, ReqDisconnected };

struct Req {
	enum Reqs req;
	void *ch;
	unsigned long data;
	char num[52];
};

int scmfd;

void sendReq ( int req, void *ch, unsigned long data )
{
	struct Req r;
	r.req = req;
	r.ch = ch;
	r.data = data;
	memset(r.num, 0, sizeof(r.num));

	if (sizeof(r) != write(scmfd, &r, sizeof(r))) {
		perror("write(req)");
		exit(1);
	}
}

void getReq ( struct Req *req )
{
	if (sizeof(*req) != read(scmfd, req, sizeof(*req))) {
		perror("read(req)");
		exit(1);
	}
}

void dumpBuf ( unsigned char *buf, int len )
{
	int i;
	for (i=0; i<len; i++) {
		if (!(i & 0xf))
			printf("\n%04x:", i);
		printf(" %02x", buf[i]);
	}
	puts("\n");
}

#define NUM_CH	23

int main ( void )
{
	scs_ioctl ioc;
	int fd, len;
	unsigned char buf[4096];
	unsigned callTypes;
	struct Req req;
	void *channels[NUM_CH];
	int chan_connected[NUM_CH];
	int i;
	fd_set rfds;

	memset(channels, 0, sizeof(channels));
	memset(chan_connected, 0, sizeof(chan_connected));

	scmfd = open("/proc/scmctrl", O_RDWR);
	if (scmfd < 0) {
		perror("open(/proc/scmctrl)");
		return 1;
	}

	fd = open("/dev/apsisdn", O_RDWR);
	if (fd < 0) {
		perror("open(/dev/apsisdn)");
		return 1;
	}

	for (i=0; i<NUM_CH; i++) {
		callTypes = 1U << CALLTYPE_SPEECH;

		ioc.device = 0;
		ioc.channel = i;
		ioc.command = SCIOCLISTENCALL;
		ioc.dataptr = (void *)callTypes;
		if (0 != ioctl(fd, SCIOCLISTENCALL, &ioc)) {
			perror("ioctl(SCIOCLISTENCALL)");
			return 1;
		}
	}

	FD_ZERO(&rfds);
	for (;;) {
		FD_SET(scmfd, &rfds);
		FD_SET(fd, &rfds);
		select(fd+1, &rfds, NULL, NULL, NULL);

		if (FD_ISSET(scmfd, &rfds)) {
			getReq(&req);
			fprintf(stderr, "got req: %d\n", req.req);
			switch(req.req) {
			case ReqConnect:
				for (i=0; i<NUM_CH; i++) {
					if (!channels[i] && !chan_connected[i]) {
						channels[i] = req.ch;
						fprintf(stderr, "would dial channel %d\n", i);
						break;
					}
				}
				if (NUM_CH == i)
					sendReq(ReqDisconnected, req.ch, 0);
				break;
			case ReqDisconnected:
				for (i=0; i<NUM_CH; i++) {
					if (channels[i] == req.ch) {
						fprintf(stderr, "would hangup channel %d\n", i);
						channels[i] = NULL;
					}
				}
				break;
			case ReqGetChan:
				if (!req.ch) {
					fprintf(stderr, "getReq: (%d,%p) --- argh!\n", req.req, req.ch);
					break;
				}
				for (i=0; i<NUM_CH; i++) {
					if (chan_connected[i] && !channels[i]) {
						channels[i] = req.ch;
						sendReq(ReqConnecting, channels[i], i << 8);
						break;
					}
				}
				if (NUM_CH == i)
					sendReq(ReqDisconnected, req.ch, 0);
			}
		}

		if (FD_ISSET(fd, &rfds) && (len=read(fd, &buf, sizeof(buf))) > 0) {
		printf("read()=%d", len);
		dumpBuf(buf, len);

		if (1==buf[8] && 1==buf[9] && 1==buf[10]) {
			int i = buf[11] - 1;
			printf("got PhyConnect on %d: setting Mvip stream,slot to 0,%d\n", i, i);
			if (i<0 || i >= NUM_CH)
				printf("ooops... channel %d out of range", i);
			ioc.device = 0;
			ioc.channel = i;
			ioc.command = SCIOCSETMVIP;
			ioc.dataptr = (void *)(i);
			if (ioctl(fd, SCIOCSETMVIP, &ioc))
				perror("ioctl(SCIOCSETMVIP");
			else
				printf("ok!\n");
			if (chan_connected[i])
				fprintf(stderr, "Argh!  channel %d already connected!\n", i);
			chan_connected[i] = 1;
			sendReq(ReqGetChan, 0, 0);
		} else if (1==buf[8] && 1==buf[9] && 2==buf[10]) {
			int i = buf[11] - 1;
			printf("got PhyDisconnect on %d\n", i);
			if (i<0 || i >= NUM_CH)
				printf("ooops... channel %d out of range", i);
			ioc.device = 0;
			ioc.channel = i;
			ioc.command = SCIOCRELMVIP;
			ioc.dataptr = NULL;
			if (ioctl(fd, SCIOCRELMVIP, &ioc))
				perror("ioctl(SCIOCRELMVIP)");
			else
				printf("ok!\n");
			if (!chan_connected[i])
				fprintf(stderr, "Argh!  channel %d not connected!\n", i);
			if (channels[i]) {
				sendReq(ReqDisconnect, channels[i], 0);
				channels[i] = NULL;
			}
			chan_connected[i] = 0;
		}
		fflush(stdout);
		}
	}
	printf("read=%d\n", len);
	if (len < 0)
		perror("read");
	close(fd);
	return 0;
}

