/*
 * io.c
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: io.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
#define __KERNEL__
#include <linux/mm.h>

#include "includes.h"
#include "adapter.h"
#include "message.h"

/*
   Send data on a port
*/
int scm_sndpkt(channel_t *ch, __u8 *data, int len)
{
	port_t *p = ch->dev;
	struct cmd_send cs;
	unsigned long flags;
	int ret = 0;

	save_flags(flags);
	cli();
	if (p->tx_buf) {
		ret = -EBUSY;
		goto out;
	}

	p->tx_buf = kmalloc(len, GFP_ATOMIC);
	if (!p->tx_buf) {
		ret = -ENOMEM;
		goto out;
	}
	memcpy(p->tx_buf, data, len);

	cs.size = len;
	cs.frame_ind = FRIND_END;
	cs.addr = (__u32) p->tx_buf;
	if (0 == amsg_send(p->card, p->index, 0, &cs)) {
		ret = -EIO;
		kfree(p->tx_buf);
		p->tx_buf = NULL;
	}
out:
	restore_flags(flags);
	return ret;
}

/*
   Set the send and recieve ACCM's
*/
void scm_setaccm(channel_t *ch, __u32 rx, __u32 tx)
{
	struct cmd_accm accm;
	adapter_t *a = ((port_t *)ch->dev)->card;
	__u8 prt_index = ((port_t *)ch->dev)->index;
	
	/* Set the RX ACCM */
	accm.accm = rx;
	amsg_rxaccm(a, prt_index, 0, &accm);
	
	/* Set the TX ACCM */
	accm.accm = tx;
	amsg_txaccm(a, prt_index, 0, &accm);
}
