/*
 * call.c
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: call.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#define __KERNEL__
#include "includes.h"
#include "adapter.h"
#include "message.h"
#include "mvip_if.h"

/*
   Babylon wants to hangup, so drop carrier and tell the MVIP layer
*/
int scm_hangup(channel_t *ch)
{
	port_t *port = (port_t *) ch->dev;
	adapter_t *a = port->card;

	amsg_drop(a, port->index, 0);
	call_hangup(ch, 0x10);
	return 0;
}

/*
   Carrier has been detected, attach to the bus and begin
   negotiating
*/
void call_connecting(struct channel *ch, int incoming, unsigned long data)
{
	port_t *port = (port_t *) ch->dev;
	adapter_t *a = port->card;
	struct cmd_tdmswitch ts;
	struct cmd_connect cm;
	struct cmd_lbattach cl;
	struct cmd_speed ms = { SPEED_33p6, SPEED_2p4, SPEED_AUTO };
	struct cmd_calltype ct = { CALLTYPE_MODEM };
	struct cmd_framing ft = { FRM_ASYNC_PPP };
	struct cmd_compress cp = { COMPRESS_AUTO };

	printk("call_connecting\n");

	/* set parameters for call... */
	amsg_calltype(a, port->index, &ct);
	amsg_speed(a, port->index, 0, &ms);
	amsg_compress(a, port->index, 0, &cp);
	amsg_framing(a, port->index, 0, &ft);

	/* 
	   Attach the local TDM slot for the port to the
	   external bus

	   This is pending a final draft of the mvip_if
	*/
	port->mvip_rxstream = (data & 0xff) + 8;
	port->mvip_rxslot = (data >> 8) & 0xff;
	port->mvip_txstream = (data & 0xff) + 0;
	port->mvip_txslot = port->mvip_rxslot;
	ts.bank = port->lb_bank;
	ts.slot = port->lb_slot;
	ts.ext_src = port->mvip_rxslot;
	ts.ext_dst = port->mvip_txslot;
	ts.ext_src_stream = port->mvip_rxstream;
	ts.ext_dst_stream = port->mvip_txstream;
	ts.enable = TDMSWITCH_NORMAL;
	printk("port: %d bank: %d slot: %d ext_src: %d/%d ext_dst: %d/%d \n",
			port->index,
			ts.bank, ts.slot,
			ts.ext_src, ts.ext_src_stream,
			ts.ext_dst, ts.ext_dst_stream
			);
	amsg_tdmswitch(a, &ts);

	cl.bank = port->lb_bank;
	cl.slot = port->lb_slot;
	cl.mode = LBATTACH_CONNECT;
	cl.coding = LBATTACH_ULAW;

	printk("bank: %d slot: %d\n", cl.bank, cl.slot);
	amsg_lbattach(a, port->index, 0, &cl);

	/*
	   Begin negotiations
	*/
	cm.mode = incoming ? ANSWER : ORIGINATE;
	printk("mode: %d\n", cm.mode);
	amsg_connect(a, port->index, 0, &cm);
}

/*
   The call has been physically disconnected
*/
void call_disconnect(struct channel *ch)
{
	port_t *port = (port_t *) ch->dev;
	adapter_t *a = port->card;
	struct cmd_tdmswitch ts;

	printk("call_disconnect: ch=%p\n", ch);
	/* Detach the port from the TDM bus */
	ts.bank = port->lb_bank;
	ts.slot = port->lb_slot;
	ts.ext_src = port->mvip_rxslot;
	ts.ext_dst = port->mvip_txslot;
	ts.ext_src_stream = port->mvip_rxstream;
	ts.ext_dst_stream = port->mvip_txstream;
	ts.enable = TDMSWITCH_DISABLED;
	amsg_tdmswitch(a, &ts);

	amsg_drop(a, port->index, 0);
	/* Inform the MVIP layer that the bus has been released */
	call_disconnected(ch);
}
