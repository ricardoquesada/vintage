/*
 * cdev.h
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: cdev.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _CDEV_H_
#define _CDEV_H_

#ifdef __KERNEL__
#include <linux/ioctl.h>
#else
#include <sys/ioctl.h>
#endif

#define BLK_SIZE		2048		/* Size of code upload block (bytes) */

#define SCMIOC_RESET	_IO('B', 0x01)
#define SCMIOC_OSLOAD	_IO('B', 0x02)
#define SCMIOC_ABORT	_IO('B', 0x03)
#define SCMIOC_OSVER	_IO('B', 0x04)
#define SCMIOC_OSSTART	_IO('B', 0x05)
#define SCMIOC_OSSTOP	_IO('B', 0x06)
#define SCMIOC_SPEED	_IO('B', 0x07)

/* structure passed with SCMIOC_OSVER */
struct ioc_version {
	unsigned long	version;
	unsigned char	century;
	unsigned char	year;
	unsigned char	month;
	unsigned char	day;
};

/* structure passed with SCMIOC_SPEED */
struct ioc_speed {
	int port;						/* -1 if global change */
	unsigned char max_speed;
	unsigned char min_speed;
};

/* Modem speed codes for struct ioc_speed */
#define SP_24	0x02
#define SP_48	0x03
#define SP_72	0x04
#define SP_96	0x05
#define SP_120	0x06
#define SP_144	0x07
#define SP_168	0x08
#define SP_192	0x09
#define SP_216	0x0A
#define SP_240	0x0B
#define SP_264	0x0C
#define SP_288	0x0D
#define SP_312	0x0E
#define SP_336	0x0F

#endif
