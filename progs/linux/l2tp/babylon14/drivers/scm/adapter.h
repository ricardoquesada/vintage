/* 
 * adapter.h
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: adapter.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _ADAPTER_H_
#define _ADAPTER_H_

#include <linux/types.h>
#include <linux/wait.h>
#include <aps_if.h>

/*
   Aribitrary and insane limitations
*/
#define MAX_CARDS	8
#define MAX_PORTS	60

#define RCV_BUF_SIZE	2048
#define SND_BUF_SIZE	2048

#define PCI_VENDOR_PHY	0x1343
#define PCI_DEVICE_CHAM	0xc001

/*
   Map of the PCI registers at the base of shared RAM
*/
struct pci_regs {
	__u32	cmd_write_ptr;
	__u32	rsp_read_ptr;
	__u32	cmd_read_ptr;
	__u32	rsp_write_ptr;
	__u32	sft_reset;
	char	r2[0x0C];
	__u32	irq_status;
	__u32	irq_mask;
	char	r3[0x18];
	__u8	signature[8];
	__u8	state[4];
};

/*
   The glue for each port to Babylon and it's adapter
*/
struct adapter;
typedef struct port {
	struct adapter	*card;
	channel_t		ch;
	unsigned int	disc_cause;
	int				index;			/* UPP # */
	__u8			lb_bank;		/* Local TDM bus bank */
	__u8			lb_slot;		/* Local TDM bus time slot */
	__u8			mvip_txstream;
	__u8			mvip_rxstream;
	__u8			mvip_txslot;	
	__u8			mvip_rxslot;
	__u8			reserved[2];
	bool			rx_buf_trash;	/* Buffer is garbage, discard */
	__u8			*rx_buf;		/* data receive host buffer */
	__u32			rx_buf_len;		/* Amount of data in the rx_buf */
	
	__u8			*tx_buf;
} port_t;

/*
   The state and information structure for each adapter
*/
#define AST_STOPPED		0
#define AST_RUNNING		1

typedef struct adapter {
	int					index;				/* Index in the array */
	int					state;
	__u16				tx_seqno;			/* next message sequence # */
	unsigned long		rambase;			/* physical ram base addr */
	volatile __u8		*vrambase;			/* virtual ram base addr */
	char				devicename[5];	
	__u8				irq;				
	__u8				model[5];			
	__u8				hw_version[4];
	int					nr_ports;			/* Number of ports onboard */
	unsigned int		async_seq;			/* msq seq # ot match on */
											/* sync message lookout */
	void				*async_rsp;			/* the response to sync */
	struct wait_queue 	*async_queue;		/* sync msg timeout timer */
	volatile struct pci_regs 	*r;					/* pointer to PCI regs */
	port_t				*ports[MAX_PORTS+1];	
	
	struct tty_driver	*tty_drv;			/* TTY interface */
} adapter_t;

#endif
