/*
 * proc.c
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: proc.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
*/

#define __KERNEL__
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include "includes.h"
#include "message.h"
#include "adapter.h"

extern int cinst;
extern adapter_t *cards[];

int proc_scm_getinfo(char *, char **, off_t, int, int);

struct proc_dir_entry proc_scm = {
	0, 8, "scm", S_IFREG | S_IRUGO, 1, 0, 0, 
	0, &proc_net_inode_operations, proc_scm_getinfo,
	0, 0, 0, 0
};

int proc_init(void)
{
	proc_register_dynamic(&proc_root, &proc_scm);
	return 0;
}

void proc_cleanup()
{
	proc_unregister(&proc_root, proc_scm.low_ino);
}

int proc_scm_getinfo(char *buf, char **start, off_t offset, int len, int u)
{
	off_t pos = 0, begin = 0;
	int l = 0, i = 0;
	unsigned long flags;

	save_flags(flags);
	cli();

	for(i = 0 ; i < cinst ; i++) {
		l += sprintf(buf + l, "%4s cmd_write: %#x cmd_read: %#x rsp_write: %#x rsp_read: %#x\n",
			cards[i]->devicename,
			cards[i]->r->cmd_write_ptr, cards[i]->r->cmd_read_ptr,
			cards[i]->r->rsp_write_ptr, cards[i]->r->rsp_read_ptr);

		pos = begin + l;
		if(pos < offset) {
			l = 0;
			begin = pos;
		}
		if(pos > offset + len)
			break;
	}

	restore_flags(flags);

	*start = buf + (offset - begin);
	l -= offset - begin;
	if(l > len)
		l = len;
	
	return l;
}
