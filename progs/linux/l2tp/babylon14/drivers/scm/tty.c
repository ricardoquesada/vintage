/*
 * tty.c - Implements standard TTY driver interface to the SCM
 * adapter
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: tty.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#include "includes.h"
#include "adapter.h"
#include "message.h"
#include <linux/tty_driver.h>

extern adapter_t *cards[MAX_CARDS];
extern int cinst;

/* TTY driver callbacks */
static int tty_open(struct tty_struct *, struct file *);
static void tty_close(struct tty_struct *, struct file *);
static int tty_write(struct tty_struct *, int, const unsigned char *, int); 
static void tty_put_char(struct tty_struct *, unsigned char);
static void tty_flush_chars(struct tty_struct *);
static int tty_write_room(struct tty_struct *);
static int tty_chars_in_buffer(struct tty_struct *);
static int tty_ioctl(struct tty_struct *, struct file *, unsigned int, 
		unsigned long);
static void tty_set_termios(struct tty_struct *, struct termios *);
static void tty_throttle(struct tty_struct *);
static void tty_unthrottle(struct tty_struct *);
static void tty_stop(struct tty_struct *);
static void tty_start(struct tty_struct *);
static void tty_hangup(struct tty_struct *);
static void tty_flush_buffer(struct tty_struct *);
static void tty_set_ldisc(struct tty_struct *);

/* tty_driver.h */
static struct tty_driver scm_driver = {
	TTY_DRIVER_MAGIC,
	"scm",
	0,
	SCMTTY_MAJOR,
	SCMTTY_MINOR_START,
	60,
	TTY_DRIVER_TYPE_SERIAL,
	SYSTEM_TYPE_TTY,
	init_termios,
	TTY_DRIVER_REAL_RAW | TTY_DRIVER_INSTALLED,
	0,
	0,
	0,
	0, 
	0, 
	tty_open,
	tty_close,
	tty_write,
	tty_put_char,
	tty_flush_chars,
	tty_write_room,
	tty_chars_in_buffer,
	tty_ioctl,
	tty_set_termios,
	tty_throttle,
	tty_unthrottle,
	tty_stop,
	tty_start,
	tty_hangup,
	tty_flush_buffer,
	tty_set_ldisc
};

int tty_open(struct tty_struct *, struct file *)
{

}

void tty_close(struct tty_struct *, struct file *)
{

}

int tty_write(struct tty_struct *, int, const unsigned char *, int)
{

}

void tty_put_char(struct tty_struct *, unsigned char)
{

}

void tty_flush_chars(struct tty_struct *) 
{

}

int tty_write_room(struct tty_struct *)
{

}

int tty_chars_in_buffer(struct tty_struct *)
{

}

int tty_ioctl(struct tty_struct *, struct file *, unsigned int, 
		unsigned long)
{

}

void tty_set_termios(struct tty_struct *, struct termios *)
{

}

void tty_throttle(struct tty_struct *)
{

}

void tty_unthrottle(struct tty_struct *)
{

}

void tty_stop(struct tty_struct *)
{

}

void tty_start(struct tty_struct *)
{

}

void tty_hangup(struct tty_struct *)
{

}

void tty_flush_buffer(struct tty_struct *)
{

}

void tty_set_ldisc(struct tty_struct *)
{

}
