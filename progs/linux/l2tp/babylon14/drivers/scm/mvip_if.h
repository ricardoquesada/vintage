/* 
 * mvip_if.h * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: mvip_if.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _MVIP_IF_H_
#define _MVIP_IF_H_

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <sys/types.h>
#endif
#include <aps_if.h>

/*************************************************
   Functions exported by the driver
*/

/*
   Called by mviplib to indicate that a call is physically connected on 
   the given MVIP slot and stream. You must call call_connected when
   the link-layer connection is ready. Must not block, safe.
   data is (stream <<< 8) | slot
*/
void call_connecting(channel_t *ch, int incoming, unsigned long data );

/*
   Called by mviplib to tell the driver to disconnect itself from the
   MVIP bus. Must not block, safe.
*/
void call_disconnect(channel_t *ch);

/*************************************************
   Functions exported by mviplib
*/

/*
   Setup a call request and get an MVIP timeslot. Will call device_connect
   when the call has been placed and an MVIP slot assigned. Will not block,
   safe. **Note pass this as the channel_t->Connect handler.
*/
void call_connect(struct channel *ch, char *num, __u32 data);

/*
   Disconnect the call on the given channel for the given
   reason. Will not block, safe. Will call call_disconnect.
*/
void call_hangup(struct channel *ch, int cause);

/*
   Indicate a successful call completion and connection. Will not block,
   safe.
*/
void call_connected(struct channel *ch);

/*
   Tell the mviplib that the device has been detached
   from the MVIP bus. Will not block, is safe.
*/
void call_disconnected(struct channel *ch);

#endif
