/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: dcplus.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef __DCPLUS_H__
#define __DCPLUS_H__

#define __pack__	__attribute__((packed))

/* Limits */
#define MAXPKTLEN		1536
#define MAXBUFLEN		MAXPKTLEN + 16
#define MAXSPIDLEN		20
#define MAXDNLEN		20
#define SHMEM_SIZE		(4 * 1024)
#define MAXADAPTERS		4
#define LOGSIZE			128	/* shorts */
#define HDRSSIZE		9

#define DCPLUS_SIG		0xaa553174
#define MEM_MASK		0x00001FFF

/* Lookup table for IRQ codes */
struct irq_struct {
	__u8 irq_num;
	__u8 irq_code;
};
#define NUM_IRQS	8

/* Lookup table for RAM address codes */
struct addr_struct {
	__u32 base_addr;
	__u8 base_code;
};
#define NUM_ADDRS	4

/*
	Mailbox message structure
*/
typedef struct l4_ce_l3_msg {
	__u32 	reserved __pack__;
	__u32	home_exch __pack__;
	__u8  	primcode __pack__;
	__u8  	receipt __pack__;
	__u8	d_attrib __pack__;
	__u8	connid __pack__;
	__u16	datalen __pack__;
	__u16	refnum __pack__;
	__u8	cause[2] __pack__;
	__u16	lci_chantype __pack__;
	__u32	dataptr __pack__;
} l4_ce_l3_msg_t __pack__;

/* Possible values for cmd_code above */
#define N_CONN_RQ	0x00		/* Connect request */
#define N_CONN_RS	0x01		/* Connect response */
#define N_DISC_RQ	0x02		/* Disconnect request */
#define N_REL_RQ	0x03		/* Release request */
#define N_DATA_RQ	0x04		/* Data request */
#define N_DACK_RQ	0x05		/* Data Ack request */
#define N_EXP_RQ	0x06		/* Expedited request */
#define N_STAT_RQ	0x07		/* Status request */
#define N_RES_RQ	0x08		/* Reset request */
#define N_RES_RS	0x09		/* Reset response */
#define N_START_RQ	0x0A		/* Start request */
#define N_RELOAD_RQ	0x0B		/* Reload request */
#define N_KEYPAD_RQ	0x0C		/* Keypad request */
#define N_SS_RQ         0x10
#define N_DV_RQ         0x11
#define N_VOL_RQ        0x12
#define N_MAP_RQ        0x13
#define N_MIC_RQ        0x14
#define N_BCHAN_LOOP    0x15
#define N_DCHAN_LOOP    0x16
#define N_SUS_RQ        0x20
#define N_REM_RQ        0x21
#define N_PDL_RQ	0x30

#define N_CONN_IN       0x00
#define N_CONN_CF       0x01
#define N_DISC_IN       0x02
#define N_DISC_CF       0x03
#define N_DATA_IN       0x04
#define N_DACK_IN       0x05
#define N_EXP_IN        0x06
#define N_STAT_IN       0x07
#define N_RES_IN        0x08
#define N_RES_CF        0x09
#define N_REL_IN        0x0A
#define N_SS_IN         0x10
#define N_SUS_CF        0x20
#define N_REM_CF        0x21
#define N_PDL_CF        0x30
#define N_PDL_END       0x31
#define N_CONN_ACK_IN   0x32

/* Possible values for cause[0] with cmd_code N_STAT_RQ above */
#define ST_RQ_RESTART    0x00
#define LAP_FLOW_OFF     0x01
#define LAP_FLOW_ON      0x02
#define DUMMY_DATA       0x03
#define RELEASE_REQ      0x04
#define S3_REL_REQ       0x05
#define BEG_RLOOP        0x06
#define END_RLOOP        0x07
#define USART_RCVRDY_ON  0x08
#define USART_RCVRDY_OFF 0x09
#define USART_MODE_CHG   0x0A
#define FLOW_OFF_LNK     0x0B
#define FLOW_ON_LNK      0x0C
#define TEI_ASS_REQ      0x0D
#define L2_EST_REQ       0x0E
#define B_CH_OOS         0x0F
#define B_CH_INS         0x10
#define CUP_PARMS_LOAD   0x11
#define GET_LINE_STATUS  0x12
#define SERIAL_LOAD	 0x13
#define ESS_EPSM_PRIM    0x80
#define I_DP_CONFIG_REQ  0x90

/* Possible values for cause[0] with cmd_code N_STAT_IN above */
#define STAT_ONHOOK     0x00
#define STAT_OFFHOOK    0x01
#define FATAL_ERROR     0x02
#define START_ALERT     0x03
#define STOP_ALERT      0x04
#define CAUSE_IND       0x05   /* Cause IE indication        */
#define DISPLAY_INFO    0x06   /* Display information        */
#define SIGNAL_IND      0x07   /* Signal IE indicator        */
#define PROGRESS_IND    0x08   /* Progress IE indicator      */
#define FEATURE_IND     0x09   /* Feature Indicator IE       */
#define LINE_NOT_ACTIVE 0x0A   /* Line not currently active  */
#define LINE_ACTIVE     0x0B   /* Line currently active      */
#define TEI_REMOVED     0x0C   /* TEI Removed                */
#define DISC_RCV        0x0D   /* Q.931 DISC pkt received    */
#define UPDATE_PCHAN    0x0E   /* Update PCHAN for a connid  */
#define SPID_ERR        0x0F   /* err in term init rq        */
#define NO_CHAN_ERR     0x10   /* no channel avail           */
#define NOTIFY_IND      0x12   /* Notify Indic IE indication */
#define INFO_RQ_IND     0x13   /* Info Request IE indication */
#define TEI_REM_INFO    0x14   /* TEI Removed information    */
#define SCREEN_UPDATE   0x15   /* Update Screen after PDL    */
#define MISC_INFO       0x16   /* Miscellaneous information  */
#define ISDN_MSG        0x17   /* ISDN MSG HEX DUMP          */
#define PPP_ACTIVE      0x18   /* PPP Active                 */
#define PPP_UPDATE_PROF 0x19   /* Update the PPP Profile     */
#define LINE_NOT_READY  0x1A   /* Line not ready for calls   */
#define LINE_READY      0x1B   /* Line now ready for calls   */
#define TEI_OBTAINED    0x1C   /* TEI accepted   */

/* Possible values for cause[1] with N_SS_RQ above */
#define SS_HOLD_RQ       0
#define SS_TRANS_RQ      1
#define SS_CONF_RQ       2
#define SS_RECONN_RQ     3
#define SS_DROP_RQ       4
#define SS_FEAT_BTN_RQ   5
#define SS_SWH_RQ        6

/* Possible values for cause[1] with N_SS_IN above */
#define SS_IN_DISPLAY    0
#define SS_IN_FI         1
#define SS_IN_BLANK      0xF0
#define SS_IN_UPDATE_SCA 0xF1

/* Possible values for rec_code above */
#define NOACK       (unsigned char) 0xFF /* prim (if any) not acked yet    */
#define PRIMACK     (unsigned char) 0    /* acknowledge prim               */
#define CMDACK      (unsigned char) 0
#define CODEACK     (unsigned char) 0
#define EVTACK      (unsigned char) 0
#define BADPRIM     (unsigned char) 1    /* problem executing prim         */
#define BADCMD      (unsigned char) 1    /* invalid Command Code code      */
#define BADPARM     (unsigned char) 2    /* invalid Command Parameter code */
#define XMITBSY     (unsigned char) 3    /*  Transmitter Busy              */
#define XMTINACTIVE (unsigned char) 4    /*  Transmitter Inactive          */
#define READREJ     (unsigned char) 5    /*  READ_REQ Buffer Rejected      */
#define MBOX_IDLE   (unsigned char) 0x10 /*  Mailbox avail for next prim   */
#define EXECERR     (unsigned char) 0x11 /*  Buffer Request Denied         */

/* Possible values for attrib above */
#define D_BIT_DATA	1		/* Want a N_DACK_IN message */
#define M_BIT_DATA	2		/* Data is multi-buffer */


/*
	Information block for *_CONN_IN, *_CONN_RQ
*/
#define CONN_PARAM_HDRLEN	26
typedef struct connparms {
	__u16	lci_chantype __pack__;
	__u8	voice_data __pack__;
	__u8	svctype __pack__;
	__u8	pvcselect __pack__;
	__u8	profile __pack__;
	__u8	i_called_addr_len __pack__;
	__u8	i_called_addr_offset __pack__;
	__u8	i_calling_addr_len __pack__;
	__u8	i_calling_addr_offset __pack__;
	__u8	o_called_addr_len __pack__;
	__u8	o_called_addr_offset __pack__;
	__u8	o_calling_addr_len __pack__;
	__u8	o_calling_addr_offset __pack__;
	__u8	call_id_len __pack__;
	__u8	call_id_offset __pack__;
	__u8	receipt_cf_indic __pack__;
	__u8	exped_data_indic __pack__;
	__u8	user_data_len1 __pack__;
	__u8	user_data_offset1 __pack__;
	__u8	user_data_len2 __pack__;
	__u8	user_data_offset2 __pack__;
	__u8	o_called_sub_len __pack__;
	__u8	o_called_sub_offset __pack__;
	__u8	o_calling_sub_len __pack__;
	__u8	o_calling_sub_offset __pack__;
	__u8	var_data[MAXBUFLEN - CONN_PARAM_HDRLEN] __pack__;
} connparms_t __pack__;

/* Voice/Data Indicator types for voice_data above */
#define CT_DATA64		0	/* 64K data call */
#define CT_VOICE		1	/* Voice Call */
#define CT_DSP			2	/* DSP data call */
#define CT_PDL			3	/* NI2 parameter download */
#define CT_DATA56		4	/* 56K data call */
#define CT_DATA56_VOICE		5	/* 56K over voice */
#define CT_DATA64_VOICE		6	/* 64K over voice */

/* Possible values for svc_type above */
#define SVC_S_DATA		2	/* Switched clear channel */
#define SVC_S_V120		5	/* Switched V.120 */
#define SVC_S_V110		6	/* Switched V.110 */
#define SVC_S_RAWHDLC		7	/* Switched HDLC data */


/*
	Board status information
*/
typedef struct isdn_line_status {
	__u8	max_b_ch __pack__;
	__u8	active __pack__;
	__u8	tei_ok[2] __pack__;
	__u8	spid_ok[2] __pack__;
} isdn_line_status_t;

typedef struct phy_stat {
	wait_queue_head_t wait;
	isdn_line_status_t linestat;
	__u8 rsprecv;
} phy_stat_t;


/*
	Information block for N_STAT_I(MISC_INFO)
*/
#define STAT_PARAM_HDRLEN	54
typedef struct stat_param {
	__u8	usr_usr_len __pack__;
	__u8	usr_usr_offset __pack__;
	__u8	facil_info_len __pack__;
	__u8	facil_info_offset __pack__;
	__u8	nets_facil_info_len __pack__;
	__u8	nets_facil_info_offset __pack__;
	__u8	keypad_len __pack__;
	__u8	keypad_offset __pack__;
	__u8	called_sub_len __pack__;
	__u8	called_sub_offset __pack__;
	__u8	calling_sub_len __pack__;
	__u8	calling_sub_offset __pack__;
	__u8	llc_len __pack__;
	__u8	llc_offset __pack__;
	__u8	fi_len[20] __pack__; 
	__u8	fi_offset[20] __pack__;
	__u8	var_data[MAXBUFLEN - STAT_PARAM_HDRLEN] __pack__;
} stat_param_t __pack__;


/*

*/
typedef struct cup_params {
	/* Line parameters */
	__u8		switch_type __pack__;
	__u8		multipoint __pack__;
	__u8		spid[2][MAXSPIDLEN + 1] __pack__;
	__u8		ss __pack__;
	__u8		olap_send __pack__;
	__u8		dn[2][MAXDNLEN] __pack__;

	/* The current firmware doesn't have X.25 */
	__u8		unused_x25_params[24] __pack__;

	/* TEIs */
	__u8		sigtei[2] __pack__;
	__u8		dx25tei __pack__;

	/* B channel data type */
	__u8		data_svc[2] __pack__;

	/* Supplementary services */
	__u8		ss_max_ca_ces0 __pack__;
	__u8		ss_max_feat_button __pack__;
	__u8		ss_max_ca_button __pack__;

	/* X.25 PVCs */
	__u16		pvc_count __pack__;
	struct {
		__u16	lci __pack__;
		__u16	type __pack__;
	} pvc[2] __pack__;
} cup_params_t __pack__;

/* Possible values for switch_type above */
#define SWT_5ESS		0 	/* NA */
#define SWT_DMS100		1 	/* NA */
#define SWT_NET3		2 	/* ETSI */
#define SWT_1TR6		3 	/* Germany */
#define SWT_NTT			4 	/* Japan */
#define SWT_NI1			5	/* NA */
#define SWT_VN			6	/* France */
#define SWT_NI2			7	/* NA */
#define SWT_AUS			8	/* Australia */

/* Possible values for multipoint above */
#define MP_PTP			0	/* Point to Point */
#define MP_EIMP			1	/* Endpoint Initialing MP */
#define MP_MLHG			2	/* MP + NI-1 MLHG */

/* Possible values for ss above */
#define SS_NONE			0
#define SS_5ESS_A		1
#define SS_5ESS_B		2
#define SS_5ESS_C		3
#define SS_5ESS_D		4
#define SS_EKTS_BASIC		5
#define SS_EKTS_CACH		6

/* Possible values for olap_send above */
#define SEND_ENBLOC		0
#define SEND_OVERLAP		1

/* Possible values for data_svc above */
#define DSVC_HDLC		SVC_S_RAWHDLC

/*
	Boilerplate for the shared RAM segment (4k)
*/
typedef struct shmem {
	/* Hardware config parameters */
	__u8		signal[2] __pack__;

	/*
		active is used on startup to initialize the board's
		irq and rambase. You must write the base address and
		irq masks to the first byte of BASE + SIG_LOC
		and set active to 2. This will save the new base address
		of the board to EEPROM, set the IRQ and rambase and then
		wait for active to be set to 1. 
	*/
	__u8		active __pack__;
	__u8		USART_active __pack__;
	__u8		flow __pack__;
	__u8		dtedce __pack__;
	__u8		hardware __pack__;
	__u8		pseudo_switch __pack__;

	/* Mailbox message buffers */
	l4_ce_l3_msg_t 	out_box __pack__;
	l4_ce_l3_msg_t	in_box __pack__;

	/* Parameter block */
	cup_params_t	cup_params __pack__;

	/* Logging */
	__u16		log_ctr __pack__;
	__u16		log_wrap_ctr __pack__;
	__u16		log[LOGSIZE] __pack__;

	/* Buffers */
	__u8		out_buf[MAXBUFLEN] __pack__;
	__u8		in_buf[MAXBUFLEN] __pack__;

	__u8		pad1[291] __pack__;

	__u8		log_active __pack__;
	__u8		tx_count[3] __pack__;	/* 0 is the D channel, 1 & 2 are B1 & B2 respectively */
	__u8		pad2[231] __pack__;
	__u8		serial_no[10] __pack__;	/* 0xfea */
	__u32		ver __pack__;
	union {
		__u32	l __pack__;
		__u8	b[4] __pack__;
	}		sig __pack__;
	__u8		pad4[2] __pack__;
	__u8		hst_intr __pack__;
	__u8		brd_intr __pack__;
#if 1
/* Location of things in shared RAM */
#define LOC_BRD_INTR		0x0fff
#define LOC_HST_INTR		0x0ffe
#define LOC_SIG			0x0ff8
#define LOC_VER			0x0ff4
#define LOC_OUT_BUF		0x01bc
#define LOC_IN_BUF		0x07cc
#endif
} shmem_t __pack__;

struct adapter;

typedef struct bchannel {
	struct adapter 	*adapter;
	int 		index;
	char		name[16];
	int 		connid;
	void 		*priv;
	__u8		tx_outstanding;
	__u8		last_tx_count;
	__u8		stalled;
	__u8		cause;
} bchannel_t;

typedef struct dchannel {
	struct adapter 	*adapter;
	char		name[16];
} dchannel_t;

typedef struct adapter {
	int 			index;
	int 			phys_index;
	char			name[16];
	__u8			*v_rambase;
	shmem_t 		*sram;
	unsigned short		version;
	unsigned int 		irq;
	bchannel_t 		b[2];
	void			*ch[2];
	dchannel_t 		d;
	struct tq_struct 	bh_task;
	int			initialized;
	int			line_active;
	struct sk_buff_head	rx_msg_queue;
	struct sk_buff_head	msg_tx_queue;
	long			msg_tx_time;
	int			msg_tx_lock;
	void			*msg_tx_callback_data;
	struct timer_list	msg_tx_timer;
	struct timer_list	flow_timer;
	struct timer_list	dial_timers[2];
} adapter_t;

extern int dcplus_identify(shmem_t *sram);
extern u32 dcplus_version(shmem_t *sram);

extern void dcplus_init(adapter_t *);
extern void dcplus_relocate(__u8 *, unsigned int, unsigned int);
extern int dcplus_checkirq(adapter_t *a, int irq);
extern int dcplus_probeirq(adapter_t *a);
extern int dcplus_sndmsg(adapter_t *a, l4_ce_l3_msg_t *, void *, void *);
extern void dcplus_cancelmsg(adapter_t *a, l4_ce_l3_msg_t *, void *, void *);
extern struct sk_buff *dcplus_rcvmsg(adapter_t *a);
extern void dcplus_procmsg(adapter_t *a, l4_ce_l3_msg_t *, struct sk_buff *);
extern int dcplus_start(adapter_t *a);
extern void dcplus_stop(adapter_t *a);
extern int dcplus_load_cup_params(adapter_t *a, cup_params_t *);
extern int dcplus_setswitch(adapter_t *a, unsigned char, unsigned char);
extern int dcplus_setspid(adapter_t *, int, char *);
extern int dcplus_setdn(adapter_t *, int, char *);
extern int dcplus_setserial(adapter_t *, char *);
extern int dcplus_connect(struct adapter *a, connparms_t *, int, void *);
extern int dcplus_hangup(struct adapter *a, int, int);
extern int dcplus_sndpkt(struct adapter *a, int, const __u8 *, int);
#ifdef DEBUG
extern void dcplus_dumpmsg(struct adapter *a, l4_ce_l3_msg_t *);
#endif

/* provided by user of lib */
extern void nconnrq_compl(adapter_t *a, int ch_idx, long connid, void *);
extern void getstatus_compl(__u8 *buf, void *);

#endif /* __DCPLUS_H__ */
