/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: l2tp.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */


#ifndef L2TP_H
#define L2TP_H

#define L2TP_MAX_WINDOW	8U		/* Tunable: maximum window size, must be a power of 2 */

#define L2TP_T		0x0001
#define L2TP_L		0x0002
#define L2TP_S		0x0010
#define L2TP_D		0x0020	/* new Data Sequence bit */
#define L2TP_O		0x0040
#define L2TP_P		0x0080
#define L2TP_VER_MASK	0xf000

#define L2TP_VER	0x2000

#define L2TP_MT_SCCRQ	1
#define L2TP_MT_SCCRP	2
#define L2TP_MT_SCCCN	3
#define L2TP_MT_StopCCN	4
#define L2TP_MT_Hello	6
#define L2TP_MT_OCRQ	7
#define L2TP_MT_OCRP	8
#define L2TP_MT_OCCN	9
#define L2TP_MT_ICRQ	10
#define L2TP_MT_ICRP	11
#define L2TP_MT_ICCN	12
#define L2TP_MT_CDN	14
#define L2TP_MT_WEN	15
#define L2TP_MT_SLI	16

struct sk_buff;

struct l2tp_tunnel {
	struct l2tp_tunnel	*next;

	struct sockaddr_in	remaddr;
	int			sock;
	int			devfd;

	u16			Tunnel,
				Session;

	u16			Ns,
				Nr,
				data_Ns,
				data_Nr,
				data_Nw,
				data_Nv;

	u16			data_win;	/* data window we are offering */

	int			ack_pending;

	struct sk_buff		*inPkts[L2TP_MAX_WINDOW];
	struct sk_buff		*outPkts[L2TP_MAX_WINDOW];
				/* we're clever here and don't release the packet until it's acknowledged */
#if 0
	timer_t			timer;		/* retransmit timer */
#endif
};

#if 0
extern int l2tp_data_xmit(struct l2tp_tunnel *s, struct sk_buff *skb);
extern int l2tp_ctrl_xmit(struct l2tp_tunnel *s, struct sk_buff *skb);

extern void l2tp_data_recv(struct l2tp_tunnel *s, struct sk_buff *skb);
extern void l2tp_ctrl_recv(struct l2tp_tunnel *s, struct sk_buff *skb);

extern void l2tp_udp_recv(struct sk_buff *skb);
extern int l2tp_udp_xmit(struct l2tp_tunnel *s, struct sk_buff *skb);
#endif

#endif
