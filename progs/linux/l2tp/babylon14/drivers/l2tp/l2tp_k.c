/*
 * l2tp_k.c - A Layer 2 Tunnelling Protocol driver for Babylon 1.4
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: l2tp_k.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
 
#define __KERNEL__
#define MODULE

#include <linux/version.h>
#include <linux/module.h>
#include <linux/net.h>
#include <linux/in.h>
#include <linux/fs.h>
#if LINUX_VERSION_CODE >= 0x20100
#include <linux/poll.h>
#endif
#include <linux/sched.h>
#include <linux/unistd.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/smp_lock.h>
#include <net/sock.h>

#include "vercomp.h"
#include "aps_if.h"

#include "l2tp.h"

#define L2TP_MAJOR	63
#define NDEV		4

static char devName[] = "l2tph";
static short l2tp_port = 1701;
static struct socket *sock;

static wait_queue_head_t waitq;

static struct l2tp_ch {
	channel_t		ch;
	wait_queue_head_t	wait;
	struct sk_buff		*skb;
	char			dialed[48];
	u32			call_info;
} l2tp_chans[NDEV];

static void l2tp_data_recv(struct l2tp_tunnel *tunnel, struct sk_buff *skb)
{
	printk("l2tp_data_recv: got skb at %p len=%d\n", skb, skb->len);
	b_kfree_skb(skb);
}

#include "l2tp_p.c"


static int l2tpf_open(struct inode *ino, struct file *file)
{
	if (MINOR(ino->i_rdev) >= NDEV)
		return -ENODEV;

	file->private_data = &l2tp_chans[MINOR(ino->i_rdev)];
	MOD_INC_USE_COUNT;
	return 0;
}

static int l2tpf_release(struct inode *ino, struct file *file)
{
	file->private_data = NULL;
	MOD_DEC_USE_COUNT;
	return 0;
}

#if LINUX_VERSION_CODE >= 0x20100
static unsigned int l2tpf_poll(struct file *file, poll_table *wait)
{
	struct l2tp_ch *ch = file->private_data;
	unsigned int mask = 0;

	poll_wait(file, &ch->wait, wait);
	if (ch->skb || (CS_DISCONNECTING == ch->ch.state))
		mask |= POLLIN | POLLRDNORM;

	if ((CS_CONNECTING == ch->ch.state) ||
	    (CS_CONNECTED == ch->ch.state))
		mask |= POLLOUT | POLLWRNORM;
	return mask;
}
#endif

static int l2tpf_read(struct file *file, char *buf, size_t len, loff_t *off)
{
	struct l2tp_ch *ch = file->private_data;
	int ret;

	if (len == 0)
		return 0;

	if (ch->skb) {
		if (ch->skb->len < len)
			len = ch->skb->len;

		ret = copy_to_user(buf, ch->skb->data, len);
		if (!ret) {
			b_dev_kfree_skb(ch->skb);
			ch->skb = NULL;
			if (ch->ch.state == CS_CONNECTED)
				clear_tbusy(&ch->ch);
			ch->ch.OutputComplete(&ch->ch);
			return len;
		}
		return -EFAULT;
	}

	if (CS_CONNECTED == ch->ch.state)
		return -EAGAIN;
	return 0;
}

static int l2tpf_write(struct file *file, const char *buf, size_t len, loff_t *off)
{
	struct l2tp_ch *ch = file->private_data;
	struct sk_buff *skb;

	if (len < 0 || len > 1500)
		return -EINVAL;

	if (ch->ch.state != CS_CONNECTED)
		return 0;

	skb = dev_alloc_skb(len);
	if (!skb)
		return -ENOMEM;

	if (!copy_from_user(skb_put(skb, len), buf, len)) {
		ch_Input(&ch->ch, skb);
		return len;
	}

	b_dev_kfree_skb(skb);
	return -EFAULT;
}

static int l2tpf_ioctl(struct inode *ino, struct file *file, unsigned int cmd, unsigned long data)
{
	return -ENOSYS;
}

static struct file_operations l2tp_fops = {
	NULL,	/* lseek */
	l2tpf_read,
	l2tpf_write,
	NULL,	/* readdir */
	l2tpf_poll,
	l2tpf_ioctl,
	NULL,	/* mmap */
	l2tpf_open,
	NULL,	/* l2tp_flush */
	l2tpf_release,
	NULL,	/* fsync */
	NULL,	/* fasync */
	NULL,	/* check_media_change */
	NULL	/* revalidate */
};

static void l2tp_use(channel_t *ch)
{
	MOD_INC_USE_COUNT;
}

static void l2tp_unuse(channel_t *ch)
{
	MOD_DEC_USE_COUNT;
}

static int l2tp_ioctl(channel_t *ch, unsigned int cmd, unsigned long arg)
{
	return -ENOSYS;
}

static int l2tp_output(channel_t *ch, struct sk_buff *skb)
{
	struct l2tp_ch *lch = (void *)ch;
	if (tas_tbusy(ch))
		return -EBUSY;

	if (CS_CONNECTED == ch->state) {
		lch->skb = skb;
		wake_up_interruptible(&lch->wait);
		return 0;
	}
	b_dev_kfree_skb(skb);
	return 0;
}

static int l2tp_hangup(channel_t *ch)
{
	struct l2tp_ch *lch = (void *)ch;

	set_tbusy(ch);
	ch->state = CS_DISCONNECTED;
	wake_up_interruptible(&lch->wait);
	return 0;
}

static int l2tp_connect(channel_t *ch, const char *number, u32 info)
{
	struct l2tp_ch *lch = (void *)ch;
printk("l2tp_connect: ch = %p\n", ch);

	strncpy(lch->dialed, number, sizeof(lch->dialed));
	lch->call_info = info;
	ch->state = CS_CONNECTING;
	wake_up_interruptible(&lch->wait);
#if 0
	ch->Open(ch);
	ch->Up(ch);
	ch->ConnectComplete(ch, 0);
#endif
	return 0;
}

static void l2tp_data_ready(struct sock *sk, int bytes)
{
	printk("l2tp_data_ready: %d bytes\n", bytes);
}

static int l2tp_thread(void *arg)
{
	struct sk_buff *skb;
	int err;

	lock_kernel();

	exit_files(current);
	exit_mm(current);

	printk("in l2tp_thread...\n");
	strcpy(current->comm, "kl2tpd");

	for (;;) {
		skb = skb_recv_datagram(sock->sk, 0, 0, &err);
		if (!skb)
			break;

		printk("ohh, we have a packet(%p)... len=%d, src=%u dst=%u len=%u check=%u\n",
			skb, skb->len,
			ntohs(skb->h.uh->source),
			ntohs(skb->h.uh->dest),
			ntohs(skb->h.uh->len),
			ntohs(skb->h.uh->check)
			);

		/* latency is worse than dropping packets as there may be many connections that we can't
		 * afford to delay.
		 */
		skb = skb_unshare(skb, GFP_ATOMIC);
		if (!skb)
			continue;

		printk("it's now unshared at %p\n", skb);

		l2tp_udp_recv(skb);
	}

	printk("err set to %d\n", err);

	MOD_DEC_USE_COUNT;
	return 0;
}


int init_module(void)
{
	struct l2tp_ch *ch;
	unsigned i;
	int ret = 0;
	struct sockaddr_in sin;

	init_waitqueue_head(&waitq);

	ret = sock_create(PF_INET, SOCK_DGRAM, IPPROTO_UDP, &sock);
	if (ret) {
		printk("l2tp: unable to create udp socket\n");
		goto out;
	}

	sin.sin_family = AF_INET;
	sin.sin_port = htons(l2tp_port);
	sin.sin_addr.s_addr = INADDR_ANY;

	ret = sock->ops->bind(sock, (struct sockaddr *)&sin, sizeof(sin));
	if (ret) {
		printk("l2tp: unable to bind udp socket\n");
		sock_release(sock);
		goto out;
	}

	for (ch=l2tp_chans,i=0; i<NDEV; ch++,i++) {
		init_waitqueue_head(&ch->wait);
		ch->ch.unit = 0;
		ch->ch.index = i;
		ch->ch.use = l2tp_use;
		ch->ch.unuse = l2tp_unuse;
		ch->ch.ioctl = l2tp_ioctl;
		ch->ch.Output = l2tp_output;
		ch->ch.Hangup = l2tp_hangup;
		ch->ch.Connect = l2tp_connect;

		strcpy(ch->ch.dev_class, "l2tp");
		sprintf(ch->ch.device_name, "l2tp%u", i);
		RegisterChannel(&ch->ch);
	}

	register_chrdev(L2TP_MAJOR, devName, &l2tp_fops);

	MOD_INC_USE_COUNT;
	ret = kernel_thread(l2tp_thread, NULL, 0);
	printk("kernel_thread returned %d\n", ret);
	if (ret < 0) {
		MOD_DEC_USE_COUNT;
	} else
		ret = 0;

	printk("Babylon L2TP Driver loaded.\n");

out:
	return ret;
}

void cleanup_module(void)
{
	struct l2tp_ch * ch;
	unsigned i;

	for (ch=l2tp_chans,i=0; i<NDEV; ch++,i++) {
		UnregisterChannel(&ch->ch);
		if (ch->skb)
			b_kfree_skb(ch->skb);
	}

	unregister_chrdev(L2TP_MAJOR, devName);

	printk("L2TP Module unloaded.\n");
	sock_release(sock);
}

