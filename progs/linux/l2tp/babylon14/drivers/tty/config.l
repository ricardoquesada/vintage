%{
/*
 * config.l - lexer for bttyd config file
 *
 * (C) Spellcaster Telecommunications, Inc.
 *
 * $Id: config.l,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 */

#include "y.tab.h"
#include <string.h>

int lineno=0;
%}

ws	[ \t]+
comment	#.*
qstring	\"[^\"\n]*[\"\n]
id	[a-zA-Z][a-zA-Z0-9]*
nl	\n
num	[0-9]+

%%

{nl}		{ lineno++; }
{ws}		;	/* Skip whitespace */
{comment}	;	/* Skip comments */
{qstring}	{	/* Quoted String */
			yylval.string=strdup(yytext+1);
			if (yylval.string[yyleng-2]!='"') {
				warning("Unterminated character string");
			} else {
				/* remove the closing quote */	
				yylval.string[yyleng-2]=' ';
				return QSTRING;
			}
		}
300		{ return BAUD300; }
1200		{ return BAUD1200; }
2400		{ return BAUD2400; }
9600		{ return BAUD9600; }
19200		{ return BAUD19200; }
38400		{ return BAUD38400; }
57600		{ return BAUD57600; }
115200		{ return BAUD115200; }
230400		{ return BAUD230400; }
460800		{ return BAUD460800; }

line		{ return LINE; }
type		{ return TYPE; }
flow		{ return FLOW; }
connect		{ return CONNECT; }
speed		{ return SPEED; }
local		{ return LOCAL; }
modem		{ return MODEM; }
xonxoff		{ return XONXOFF; }
rtscts		{ return RTSCTS; }
chatprog	{ return CHATPROG; }
chatparams	{ return CHATPARAMS; }

{id}		{
			yylval.string=strdup(yytext);
			return ID;
		}

%%

