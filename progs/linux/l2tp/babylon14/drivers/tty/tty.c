/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: tty.c,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#define MODULE

#include <linux/module.h>

#include <linux/fs.h>
#include <linux/tty.h>
#include <linux/tty_ldisc.h>
#include <linux/tqueue.h>

#include "../include/aps_if.h"

#include "port.h"	/* port stuff */
#include "msg.h"	/* Deamon messaging */
#include "ldisc.h"

#define CDEV_MAJOR 64

#define DEVCLASS_NAME	"btty"
int devclass_id;
static char version[] = "0.02";

/* Daemon character device callbacks */
int d_open(struct inode *i, struct file *f);
void d_close(struct inode *i, struct file *f);

/* Babylon channel callbacks */
void send_message(struct bmsg *m);
extern int b_output(channel_t *, unsigned char *, int);

/* TTY line discipline callbacks */
extern int l_open(struct tty_struct *tty);
extern void l_close(struct tty_struct *tty);
extern void l_receive_buf(struct tty_struct *tty, const unsigned char *cp,
	char *fp, int count);
extern int l_receive_room(struct tty_struct *tty);
extern void l_write_wakeup(struct tty_struct *tty);

/*
 * Ports
 */
port_t *register_port(char *name);
int unregister_port(port_t *port);
port_t *port_list = NULL;

/*
 * the daemon message queue
 */
struct bmsg *m_queue = NULL;
struct wait_queue *wait_list;

struct tty_ldisc adc_ldisc = {
	TTY_LDISC_MAGIC,
	0,
	0,
	l_open,
	l_close,
	0,	/* void flush_buffer */
	0,	/* int chars_in_buffer */
	0,	/* int read(tty, file, unsigned char buf, unsigned int nr */
	0,	/* int write(...) */
	0,	/* int ioctl */
	0,	/* void set_termios(tty, struct termios *old) */
	0,	/* int select(...) */
	l_receive_buf,
	l_receive_room,
	l_write_wakeup
};

/*
 * This device class is alive!
 */
int init_module()
{
	pr_info("SpellCaster Babylon ISDN Device Class v%s Loaded\n", version);
	pr_info("Copyright (C) 1996, 1997 SpellCaster Telecommunications Inc.\n");

	devclass_id = RegisterDeviceClass(DEVCLASS_NAME);
	if(devclass_id < 0) {
		pr_info("ISDN Device class already registered!\n");
		return -1;
	}

	return tty_register_ldisc(N_BAB, &adc_ldisc);
}

/*
 * This device class is toast
 */
void cleanup_module()
{
	tty_register_ldisc(N_BAB, NULL);
	UnregisterDeviceClass(devclass_id);
}

/*
 * Babylon wants a port connected
 */
static void b_connect(channel_t *ch, char *phone, __u32 type)
{
	printk("b_connect\n");
	ch->ConnectComplete(ch, 0x100);
}

/*
 * Babylon wants a port hungup
 */
static void b_hangup(channel_t *ch)
{
	printk("b_hangup\n");
}

/*
 * Put a port in-service
 */
port_t *register_port(char *name)
{
	port_t *p;
	MOD_INC_USE_COUNT;
	printk("register_port(%s)\n", name);

	p = kmalloc(sizeof(port_t), GFP_KERNEL);
	if(!p) {
		printk("Couldn't allocate port '%s'\n", name);
		MOD_DEC_USE_COUNT;
		return 0;
	}

	p->magic = PORT_MAGIC;
	strncpy(p->channel.device_name, name, sizeof(p->channel.device_name));
	strcpy(p->channel.dev_class, "bppp");
	p->channel.dev = p;
	p->channel.Output = b_output;
	p->channel.Connect = b_connect;
	p->channel.Hangup = b_hangup;
	p->channel.Open = b_open;
	p->channel.Close = b_close;
	p->channel.state = CS_UNAVAIL;
	
	p->tty = NULL;

	if(RegisterChannel(&p->channel)) {
		printk("%s: Channel registration failed\n", name);
		kfree(p);
		MOD_DEC_USE_COUNT;
		return 0;
	}

	return p;
}

/*
 * Take a port out-of-service
 */
int unregister_port(port_t *port)
{
	printk("unregister_port\n");
	if(!port_is_valid(port)) {
		printk("Passed an unvalid port pointer !!!\n");
		return 0;
	}

	UnregisterChannel(&port->channel);
	kfree(port);
	MOD_DEC_USE_COUNT;
	return 1;
}

