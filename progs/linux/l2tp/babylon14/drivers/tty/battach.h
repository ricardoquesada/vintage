/*
 *   battach.h
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: battach.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _BATTACH_H
#define _BATTACH_H

/* Babylon Line decipline number which is gathered from the
 * kernel tty.h and is equal to NR_DISCS-1. We undef it first to
 * get rid of warnings
 */
#undef N_BAB
#define N_BAB 15
 
enum ERRMSGS {
	ERR_SETDISC,
	ERR_RESETDISC,
	ERR_ATTACH,
};
char *errmsg[] = {
	"Can't set line discpline",
	"Can't reset line discpline",
	"Attempt to attached to babylon failed",
};
	

void error(int errnum);
void handler(int signal);

#endif
