/*
 * Header file for the modem and bttyd.conf file parser
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: ttyd.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _TTYD_H_
#define _TTYD_H_

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define CONFIGFILE "/etc/bttyd.conf"
#define LOCK_PREFIX "/var/lock/LCK.."
#define PID_FILE "/var/run/bttyd.pid"

/* Define our maximums */
#define MAXCONFIG 10
#define MAXMODEMS 50
/* 
 * This is a bad place for this, but it's only really available
 * in kernel code otherwise
 */
#define N_BAB 15

enum TYPES {TYPE_LOCAL, TYPE_MODEM };
enum FLOWCTRL {FLOW_XON,FLOW_RTS };

typedef struct optiondef {
	char chatprog[80];
	char chatparams[80];
} optionstype;

typedef struct linedef {
	char linename[80];
	int type;
	int flow;
	char connect[256];
	int speed;
} linetype;

typedef struct slinedef {
	int confignum;
	int fd;
} slinetype;

typedef struct processdef {
	int pid;
	int confignum;
	int fd;
} processtype;

int lock_port(struct slinedef *port);
void unlock_port(struct slinedef *port);
int open_port(struct slinedef *port);
void close_port(struct slinedef *port);
int init_port(struct slinedef *port);
int do_chat(struct slinedef *port,char *connect);
int findserial(int portnum);
void hangup(int port);
void dial(int portnum,char *num);
int readconfig();
int loop();

#endif
