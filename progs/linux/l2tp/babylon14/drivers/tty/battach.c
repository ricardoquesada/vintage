/*
 * battach.c
 *
 * Program to attach a tty device to babylon. Once attached, Babylon initiates
 * communications with the device for ppp, authentication, etc. battach sleeps
 * until a SIGHUP is recieved.
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: battach.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include "ldisc.h"
#include <errno.h>
#include <sys/syslog.h>
#include <fcntl.h>
#include <signal.h>
#include "ttyd.h"
#include "version.h"

/*
 * Out signal handler
 */
void handler(int signal) {	
	syslog(LOG_NOTICE,"Caught signal %d!",signal);
}

/*
 * Routine to check if bttyd is currently running
 * Returns 1 if yes and 0 if not.
 */
int check_bttyd() {
	char pid_str[12];
	pid_t pid;
	int pid_fd,n;

	memset(pid_str, '\0', 12);
	/* Check for a stale lock */
	if (!(pid_fd = open(PID_FILE, O_RDONLY, 0))) {
		return(0);
	}

	n = read(pid_fd, &pid_str, 11);
	close(pid_fd);
	if(n < 0) {
		return(0);
	}

	/* See if the process still exists */
	sscanf(pid_str, " %d", &pid);
	if(pid == 0 || (kill(pid, 0) == -1 && errno == ESRCH)) {
		/* bttyd not running */
		return(0);
	} else {
		/* bttyd running */
		return(1);
	}
}
 
int main ( int argc, char *argv[] ) {
	int arg,i,ctrl_fd,portnum,pid_fd;
	Req msg;

	/* Process command line options */
	for (i=0;i<argc;i++) {
		if (argv[i][0]=='-') {
			switch(argv[i][1]) {
				case 'f':
					/* don't do anything */
					break;
				case 'h':
					printf("%s [-hpf]\n",argv[0]);
					puts("   -h  this info");
					puts("   -f  faxgetty mode (dummy argument)");
					puts("   -v  print version");
					exit(1);
					break;
				case 'v':
					printf("battach Version %s\n",version);
					exit(1);
				default:
					puts("Bad command arguments. Use -h for help");
					exit(1);
			}
		}
	}
				
	/* Open syslog */
	openlog("battach",LOG_PID|LOG_CONS,LOG_DAEMON);

	/* Catch the HUP and TERM signals */
	signal(SIGHUP,handler);
	signal(SIGTERM,handler);
	
	/* Check if bttyd is running */
	if (!check_bttyd()) {
		syslog(LOG_NOTICE,"bttyd not running. battach aborted");
		exit(2);
	}
		
	/* Open up our control file */
	if ((ctrl_fd = open("/proc/bttyctrl", O_WRONLY)) < 0) {
		syslog(LOG_NOTICE,"Cannot open /proc/bttyctrl: %s",strerror(errno));
		exit(2);
	}
	/* Get a port number to work with */
	if ((portnum=ioctl(ctrl_fd,BIOCF_TTY_FIND_PORT,0))<0) {
		syslog(LOG_NOTICE,"Can't get port number to use (bttyd running?): %s",strerror(errno));
		exit(2);
	}
	/* Tell bttyd the pid we are using */
	msg.pid=getpid();
	msg.port=portnum;
	msg.phone_num[0]=(char)0;
	msg.cmd=REQ_INPORT;
	write(ctrl_fd,&msg,sizeof(Req));
	/* Build our message so we are ready to quit */
	msg.pid=0;

	arg=N_BAB;
	/* Set the line discipline to Babylon */
	if (ioctl(STDIN_FILENO, TIOCSETD, &arg)<0) {
		syslog(LOG_NOTICE,"Can't set line discpline: %s",strerror(errno));
		write(ctrl_fd,&msg,sizeof(Req));
		close(ctrl_fd);
		exit(1);
	}
	/* Tell the tty driver to attach babylon to the serial port */
	if (ioctl(STDIN_FILENO, BIOC_TTY_ATTACH, portnum) != 0) {
		syslog(LOG_NOTICE,"Attach to Babylon failed: %s",strerror(errno));
		/* Force the line discipline back to normal */
		arg=0;	
		ioctl(STDIN_FILENO, TIOCSETD, &arg);
		/* Tell bttyd that we cleared the line in case a user drops the link */
		write(ctrl_fd,&msg,sizeof(Req));
		close(ctrl_fd);
		exit(1);
	}
	syslog(LOG_NOTICE,"Connected on port btty%d",portnum);

	/* Wait around until we get a signal to quit */
	pause();
	
	/* Report we're done and exit normally */
	syslog(LOG_NOTICE,"Disconnected on port btty%d",portnum);

	/* Set the line discipline to normal */
	arg=0;	
	if (ioctl(STDIN_FILENO, TIOCSETD, &arg)<0) {
		syslog(LOG_NOTICE,"Failed to reset line discipline on btty%d: %s",portnum,strerror(errno));
	}
	/* Tell bttyd that we cleared the line in case a user drops the link */
	write(ctrl_fd,&msg,sizeof(Req));
	close(ctrl_fd);

	closelog();
	exit(0);
}

