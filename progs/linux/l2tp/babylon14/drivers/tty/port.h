/*
 * Header file for defining the port information structs
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: port.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

typedef struct {
	unsigned char *base;
	unsigned char *head;
	unsigned int count;
	unsigned int size;
	unsigned int lock;
	unsigned short fcs;
} buf_t;

typedef struct port {
	unsigned int magic;
	struct port *next;
	unsigned long id;
	struct tty_struct *tty;
	channel_t channel;

	buf_t rbuf;
	unsigned int rx_packets;
	unsigned int rx_bytes;
	unsigned int rx_errors;
	unsigned int rx_fifo_errors;
	unsigned int rx_frame_errors;
	unsigned int rx_length_errors;
	unsigned int rx_crc_errors;

	struct sk_buff *xskb;
	buf_t xbuf;
	unsigned int tx_packets;
	unsigned int tx_bytes;
	unsigned int tx_errors;
	unsigned int tx_fifo_errors;
	unsigned int tx_frame_errors;
	unsigned int tx_length_errors;
	unsigned int tx_crc_errors;
	
	unsigned char error;	/* this frame is in error */
	unsigned char escape;	/* last rx was escape char */
	unsigned char in_tx;

	unsigned long callId;

	struct tq_struct	bh;
	unsigned long disc_cause;
} port_t;

#define PORT_MAGIC	0xabcd1234

#define port_is_valid(x)	(*((unsigned int *)x) == PORT_MAGIC ? 1 : 0)
