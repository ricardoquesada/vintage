/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: version.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _VERSION_H_
#define _VERSION_H_
char version[] = "1.0.1";

#endif
