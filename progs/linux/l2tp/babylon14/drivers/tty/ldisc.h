/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: ldisc.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */


#ifndef _LDISC_H
#define _LDISC_H_

#include <linux/tty.h>
/*#include <linux/skbuff.h>*/
#include <linux/if_ether.h>
#include <linux/ioctl.h>
#include "battach.h"
#include "aps_if.h"

/* Special Characters and masks */
#define PPP_FLAG        0x7e
#define PPP_ESCAPE      0x7d
#define PPP_TRANS       0x20
#define PPP_INITFCS     0xffff
#define PPP_GOODFCS	0xf0b8

#define PPP_MINLEN		6

/* Line discipline ID */
#define BAB_MAGIC		0xdeadbeef

#define BIOC_TTY_ATTACH	_IO('I', 0x67)
#define BIOC_TTY_UP	_IO('I', 0x68)
#define BIOC_TTY_SETSTATUS	_IO('I', 0x69)
#define BIOC_TTY_GETSTATUS	_IO('I', 0x6a)

/* proc file ioctl id's */
#define BIOCF_TTY_STAT_IDLE		_IO('T', 0x01)
#define BIOCF_TTY_STAT_DIALING		_IO('T', 0x02)
#define BIOCF_TTY_STAT_RINGING		_IO('T', 0x03)
#define BIOCF_TTY_STAT_CONNECTING	_IO('T', 0x04)
#define BIOCF_TTY_STAT_CONNECTED	_IO('T', 0x05)
#define BIOCF_TTY_STAT_DISCONNECTING	_IO('T', 0x06)
#define BIOCF_TTY_STAT_DISCONNECTED	_IO('T', 0x07)
#define BIOCF_TTY_STAT_STALLED		_IO('T', 0x08)
#define BIOCF_TTY_STAT_UNAVAIL		_IO('T', 0x09)
#define BIOCF_TTY_REGISTER_PORT		_IO('T', 0x0a)
#define BIOCF_TTY_RESET			_IO('T', 0x0b)
#define BIOCF_TTY_CONN_FAILED		_IO('T', 0x0c)
#define BIOCF_TTY_UNREGISTER_PORT	_IO('T', 0x0d)
#define BIOCF_TTY_FIND_PORT		_IO('T', 0x0e)
#define BIOCF_TTY_KILL			_IO('T', 0x0f)

/* Max port number permitted */
#define MAXPORTNUM 1024

/* ACCM mapping macros */
#define in_xmap(ad, c)  (ad->channel.x_accm[(c) >> 5] & (1 << ((c) & 0x1f)))
#define in_rmap(ad, c)  ((c) < 32 && ((ad->channel.r_accm >> (c)) & 0x1))

#define l_ins_char(buf,ch)		(buf.base[buf.count++] = (__u8)ch)
#define l_stuff_char(ax, buf, cx)	if(in_xmap(ax, cx)) { \
						cx ^= PPP_TRANS;\
						l_ins_char(buf, PPP_ESCAPE); \
					} \
					l_ins_char(buf, cx)
					

typedef struct Reqdef {
	unsigned int cmd;
	unsigned int port;
	char phone_num[80];
	pid_t pid;
} Req;

enum Reqs {
	REQ_DIAL,
	REQ_HANGUP,
	REQ_INPORT
};

#endif /* _LDISC_H_ */
