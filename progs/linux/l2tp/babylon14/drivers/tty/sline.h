/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: sline.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _SLINE_H_
#define _SLINE_H_

#include <pthread.h>
#include "msg.h"

#define MODEM		1
#define DIRECT		0
#define HWFLOWCTRL	0
#define SWFLOWCTRL	1
#define FAILURE		1
#define SUCCESS		0

typedef struct sline {
	unsigned int magic;
	unsigned int id;
	unsigned int call_id;
	struct sline *next;
	char *name;
	int fd;
	int init_disc;

	/* Thread access control to the sline data (not queue) */
	int valid;
	pthread_t ctrl_thread;
	pthread_mutex_t sline_mutex;
	pthread_cond_t sline_cond;

	/* Line parameters */
	unsigned int speed;
	unsigned char flow_ctrl;	/* Flow control 0-hw, 1-sw */
	unsigned char modem_ctrl;	/* Modem control 0-no, 1-yes */

	/* Chat strings */
	char *init_str;
	char *connect_str;
	char *disconn_str;
	char *reset_str;

	/* Chat parameters */
	char *prog;
	char *log;

	/* sline Message Queue and thread access control to it */
	struct bmsg *m_queue;
	pthread_mutex_t queue_mutex;
	pthread_cond_t queue_cond;
	int queue_has_messages;
} sline_t;

#endif /* _SLINE_H_ */
