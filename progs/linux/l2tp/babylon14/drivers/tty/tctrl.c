/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: tctrl.c,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthreads.h>
#include "msg.h"
#include "sline.h"

extern void send_reply(sline_t *, struct bmsg *, unsigned int);
extern void indicate_status(sline_t *, struct bmsg *, unsigned int);

static char lock_prefix[] = "/var/lock/LCK..";

/*
 * This is the entry point for the serial line control thread
 * which reads messages from it's private message queue and acts
 * on them. It opens a serial port device and when a connection
 * has been established, swithes the line discipline of the port
 * to the Babylon TTY line discipline. It then waits for the carrier
 * to drop or for Babylon to request a hangup, then closes the port
 * and exits. It returns NULL as it runs detached.
 */
void *ctrl_main(void *arg)
{
	sline_t *s = (sline_t *)arg;

	/* Wait for and process messages */
	while(!finished) {
		int status;

		/* Lock the message queue */
		status = pthread_mutex_lock(&s->queue_mutex);
		if(status)
			continue;	/* Try again */

		/* No need to wait, we've got messages! */
		if(s->queue_has_messages) {
			process_messages(s);
			continue;
		}

		/* Wait for messages to arrive */
		status = pthread_cond_wait(&s->queue_cond, &s->queue_mutex);
		if(status != 0) {
		}

		/* We got a message */
		if(s->queue_has_messages) {
			process_messages(s);
		}
		
	}
	return NULL;
}

void process_messages(sline_t *s)
{
	struct bmsg *mbuf;

	/* Get the next message from the queue */
	mbuf = mq_pop(s->m_queue);
	if(mbuf == NULL) {
		syslog(LOG_DEBUG, "%s: Message queue emptied", s->name);
		s->queue_has_messages = 0;
		pthread_mutex_unlock(&s->queue_mutex);
		return;
	}

	/* 
	 * We dont need the queue again. Unlock it so new messages can
	 * arrive 
	 */
	pthread_mutex_unlock(&s->queue_mutex);

	/* confirm the id */
	if(mbuf->id != s->id) {
		syslog(LOG_WARNING, "%s: Message received with wrong ID (%ld != %ld)",
			s->name, s->id, mbuf->id);
		goto clear_message;
	}

	/* switch on the function */
	switch(mbuf->function) {
		case M_CONNECT:
			r = connect_port(s, mbuf->call_id, mbuf->u.phone_num);
			indicate_state(s, mbuf, r);
			break;
			break;
			
		case M_HANGUP:
			hangup_modem(s);
			break;
			
		case M_OPEN:
			if(open_port(s))
				send_reply(s, mbuf, FAILURE);
			else
				send_reply(s, mbuf, SUCCESS);
			break;
			
		case M_CLOSE:
			close_port(s);
			break;
			
		default:
			syslog(LOG_WARNING, "%s: Unknown message type",
				s->name);
			break;
	}

	free(mbuf);
}

int lock_port(sline_t *s)
{
	char pid_str[12];
	char lck_file[1024];
	int lck_fd;
	pid_t pid;

	sprintf(lck_file, "%s%s", lock_prefix, s->name);
	while(1) {
		memset(pid_str, '\0', 12);
		lck_fd = open(lck_file, O_RDWR | O_EXCL | O_CREAT, 0644);
		if(!lck_fd) {
			if(errno == EEXIST) {
				/* Check for a stale lock */
				lck_fd = open(lck_file, O_RDONLY, 0);
				if(!lck_fd) {
					syslog(LOG_WARNING, "%s: unable to lock port: %m", 
						s->name);
					return -1;
				}

				n = read(lck_fd, &pid_str, 11);
				close(lck_fd);
				if(n < 0) {
					syslog(LOG_ERR, "%s: Unable to read lock pid", s->name);
					return -1;
				}

				/* See if the process still exists */
				sscanf(pid_str, " %d", &pid);
				if(pid == 0 || (kill(pid, 0) == -1 && errno == ESRCH)) {
					/* Unlink the old lock */
					unlink(lck_file);
					syslog(LOG_NOTICE, "%s: Removed stale lock, pid %d",
						s->name, pid);
					continue;
				}
				else {
					syslog(LOG_ERR, "%s: Port locked by pid %d", s->name,
						pid);
					return -1;
				}
			}
			else {
				syslog(LOG_ERR, "%s: Failed to lock port: %m", s->name);
				return -1;
			}
		}
		
		/* Write our lock info */
		pid = getpid();
		sprintfipid_str, "%010d\n", pid);
		write(lck_fd, pid_str, 11);

		close(lck_fd);
		return 0;
	}
}

void unlock_port(sline_t *s)
{
	char lck_file[1024];
	
	sprintf(lck_file, "%s%s", lock_prefix, s->name);
	unlink(lck_file);
}

int open_port(sline_t *s)
{
	char port_file[1024];
	int fl;

	if(lock_port(s)) {
		return -1;
	}

	/* open the tty non-blocking in case TIOCLOCAL isn't set -- we'll correct that shortly */
	sprintf(port_file, "/dev/%s", s->name);
	s->fd = open(port_file, O_RDWR | O_NONBLOCK, 0);
	if(!s->fd) {
		syslog(LOG_ERR, "Error opening serial port %s: %m", port_file);
		return -1;
	}

	/* put the fd back into blocking mode */
	fl = fcntl(s->fd, F_GETFL, 0);
	fl &= ~O_NONBLOCK;
	fcntl(s->fd, F_SETFL, fl);

	/* Make the port our's exclusively */
	if(ioctl(s->fd, TIOCEXCL, 0) < 0) {
		syslog(LOG_ERR, "%s: ioctl(TIOCEXCL): %m", s->name);
		return -1;
	}

	/* Initialize the port & modem */
	if(init_modem(s)) {
		syslog(LOG_ERR, "%s: Modem initialization failed", s->name);
		return -1;
	}

	return 0;
}

void close_port(sline_t *s)
{
	reset_modem(s);
	close(s->fd);
	unlock_port(s);
	s->fd = 0;
}

int connect_port(sline_t *s, unsigned int call_id, char *phone_num)
{
	if(s->call_id) {
		/* Call already in progress */
		syslog(LOG_WARNING, "%s: Connect with call already in progress", s->name);
		return S_ERROR;
	}

	if(!s->fd) {
		/* Port not open */
		syslog(LOG_ERR, "%s: Connect with port closed!", s->name);
		return S_ERROR;
	}
	
	s->call_id = call_id;
	syslog(LOG_INFO, "%s: Connecting with call_id %ld", s->name, s->call_id);

	switch(dial_modem(s, phone_num)) {
	case 0:
		/* Set the line disc to Babylon */
		ioctl(s->fd, TIOCGETD, &s->init_disc);
		ioctl(s->fd, TIOCSETD, B);

		return S_CONNECT;

	case 3:		/* Timeout */
		return S_NOANSWER;

	case 4:		/* NO CARRIER */
		return S_NOCARRIER;

	case 5:		/* BUSY */
		return S_BUSY;

	case 6:		/* NO DIALTONE */
		return S_NODIALTONE;

	default:	/* User define */
		return S_ERROR;
	}
	
	return S_ERROR;
}

int init_modem(sline_t *s)
{
	struct termios tios;
	FILE *tmp;
	char tmpfile[1024];
	int status = 0;
	int efd;

	/* Get the current line settings */
	if(tcgetattr(s->fd, &tios)) {
		syslog(LOG_ERR, "%s: tcgetattr: %m", s->name);
		return -errno;
	}

	/* Modify for PPP */
	tios.c_cflag &= ~(CSIZE | CSTOPB | PARENB | CLOCAL);
	tios.c_cflag |= CS8 | CREAD | HUPCL;
	tios.c_iflag = IGNBRK | IGNPAR;
	tios.c_oflag = 0;
	tios.c_lflag = 0;
	tios.c_cc[VMIN] = 1;
	tios.c_cc[VTIME] = 0;

	/* Remove modem control if direct */
	if(s->modem_ctrl == DIRECT)
		tios.c_cflag ^= (CLOCAL | HUPCL);

	/* Set flow control */
	if(s->flow_ctrl == HWFLOWCTRL)
		tios.c_cflag |= CRTSCTS;
	else {
		tios.c_iflag |= IXOFF;
		tios.c_cc[VSTOP]  = 0x13;
		tios.c_cc[VSTART] = 0x11;
	}

	/* Set the speed */
	cfsetospeed (&tios, to_speed(s->speed));
	cfsetispeed (&tios, 0);		/* same as output */

	/* Set the attributes */
	if(tcsetattr(s->fd, TCSAFLUSH, &tios) < 0) {
		syslog(LOG_ERR, "%s: tcsetattr: %m", s->name);
		return -errno;
	}

	if(s->init_str != NULL) {
		/* Prepare the modem init string */
		sprintf(tmpfile, "/var/tmp/chat.%s", s->name);
		tmp = fopen(tmp_file, "w");
		fputs("ABORT ERROR ", tmp);
		fputs(s->init_str, tmp);
		fclose(tmp);

		status = do_chat(s->fd, s->fd, tmpfile, s->prog, s->log);
		unlink(tmpfile);
		return status;
	}

	return 0;
}

int do_chat(int in, int out, char *cfile, char *prog, char *log)
{
	int status;
	int efd;
	pid_t pid;

	/* fork and chat */
	pid = fork();
	if(pid < 0) {
		syslog(LOG_ERR, "%s: fork: %m", s->name);
		return -errno;
	}

	if(pid == 0) {
		/* Make the serial port stdin and stdout */
		dup2(in, 0);
		dup2(out, 1);

		/* Make stderr the log file */
		efd = open(log, O_WRONLY | O_APPEND | O_CREAT, 0644);
		dup2(efd, 2);

		/* Don't know what this is for, pppd does it */
		setuid(getuid());
		setgid(getgid());

		/* run chat */
		execlp(prog, "-V", "-f", tmpfile, (char *)0);
		/* We only get here on error */
		_exit(-errno);
	}

	while(waitpid(pid, &status, 0) < 0) {
		if(errno == EINTR)
			continue;
		syslog(LOG_ERR, "%s: waitpid: %m", s->name);
	}

	return status;
}

int reset_modem(sline_t *s)
{

}

int dial_modem(sline_t *s, char *dial_str)
{

}

int hangup_modem(sline_t *s)
{

}
