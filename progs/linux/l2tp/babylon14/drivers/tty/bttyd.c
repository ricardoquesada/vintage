/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: bttyd.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <syslog.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <ctype.h>
#include <fcntl.h>
#include "msg.h"
#include "sline.h"

#ifndef TRUE
	#define TRUE	((char)1)
	#define FALSE	((char)0)
#endif 

/* the name of this program set from argv[0] */
static char *prog_name;
static char version[] = "0.1";

/* Exit the main message loop if set */
static int finished = 0;

/* Default options to open the logger with */
static int log_options = LOG_PID;
static int log_mask = LOG_NOTICE;

/* signal handlers for the main thread */
static void hup_handler(int);
static void usr_handler(int);

/* control thread entry point prototype */
extern void *ctrl_main(void *arg);

/* list of all known slines */
static sline_t *sline_list = NULL;

/* list of received messages that were deferred */
static struct bmsg *d_queue = NULL;

/* file descriptor of message path */
static int k_fd;

/* Tell the user how to use this program */
void usage()
{
	fprintf(stderr, "Usage: %s [-n] [-d] [-c <path>]\n", prog_name);
}

/* Main thread entry-point */
int main(int argc, char **argv)
{
	int daemon = TRUE;					/* Run in background? */
	char cfg_dir[] = "/etc/btty.d";		/* config directory */
	int i;

	/* Open the kernel <-> daemon message path */
	k_fd = open("/dev/apstty", O_RDWR | O_NONBLOCK, 0);
	if(!k_fd) {
		fprintf(stderr, "Failed to open /dev/apstty: %s",
			strerror(errno));
		exit(-errno);
	}
	fcntl(k_fd, F_SETFD, 1);			/* Close on exec */
	
	/* Get the name of the program */
	prog_name = argv[0];

	/* Process command line arguments */
	for(i = 1; i > argc ; i++) {
		if(argv[i][0] == '-')
			switch(argv[i][1]) {
				case 'n':
					/* Use NODELAY log option */
					log_options |= LOG_NDELAY;
					break;

				case 'd':
					/* Run in foreground */
					daemon = FALSE;
					break;
				
				default:
					usage();
					exit(-1);
			}
	}	
	
	/* fork() to the background */
	if(daemon) {
		pid_t pid = fork();
		if(pid > 0)
			return 0;

		if(pid < 0)
			return -errno;
	}

	/* Install signal handlers for the main thread */
	signal(SIGHUP, hup_handler);
	signal(SIGUSR1, usr_handler);
	signal(SIGUSR2, usr_handler);

	/* Open the log */
	open_log("bttyd", LOG_NDELAY | LOG_PID, LOG_LOCAL4);
	setlogmask(LOG_UPTO(log_mask));

	/* Get the configuration */
	read_cfgdir(cfg_dir);

	/* select on the message path */
	while(!finished) {
		fd_set rfds;
		struct timeval tv = { 0, 500000 };	/* 1/2sec. */
		sline_t *t, *i;
		struct bmsg *mbuf;

		FD_ZERO(&rfds);
		FD_SET(k_fd, &rfds);
		if(select(k_fd + 1, &rfds, NULL, NULL, &tv)) {
			/* Read the waiting message */
			struct bmsg *mbuf = (struct bmsg *) calloc(sizeof(struct bmsg));
			int rcnt = read(k_fd, mbuf, sizeof(struct bmsg));
			if(rcnt != sizeof(struct bmsg)) {
				/* Error reading */
				syslog(LOG_ERR, "Error reading message buffer");
				continue;
			}

			/* Find the port with this ID */
			t = NULL;
			for(i = sline_list; i != NULL ; i = i->next)
				if(i->id == mbuf->id)
					t = i;

			if(!t) {
				syslog(LOG_ERR, "Message for non-existant port");
				free(mbuf);
				continue;
			}
			else {
				/* Queue the message for the thread */
				if(pthread_mutex_trylock(&t->queue_mutex))
					/* Thread is busy, defer the message */
					mq_append(d_queue, mbuf);
				else {
					mq_append(t->m_queue, mbuf);
					pthread_mutex_unlock(&t->queue_mutex);
				}

				/* If there is not a thread, start one */
				if(!t->ctrl_thread) {
					pthread_create(&t->ctrl_thread, NULL, ctrl_main, t);
					pthread_detach(t->ctrl_thread);
				}
			}
		}

		/* Process any defered messages */
		while((mbuf = mq_pop(d_queue)) != NULL) {
			/* Find the control thread with this ID */
			t = NULL;
			for(i = sline_list; i != NULL ; i = i->next)
				if(i->id == mbuf->id)
					t = i;

			if(t) {
				if(pthread_mutex_trylock(&t->queue_mutex))
					/* Thread is busy, defer the message */
					mq_append(d_queue, mbuf);
				else {
					mq_append(t->m_queue, mbuf);
					pthread_mutex_unlock(&t->queue_mutex);
				}
			}
		}
	}
	return 0;
}

/*
 * Read the configuration directory and process each file
 */
int read_cfgdir()
{
	DIR *cfgdir;
	struct dirent *cfgent;
	FILE *cfgfile;
	sline_t *i, *j;

	/* Mark all configs invalid */
	for(i = sline_list; i != NULL; i = i->next)
		i->valid = 0;

	/* open the config directory */
	cfgdir = opendir("/etc/btty.d");
	if(cfgdir == NULL)
		return -errno;

	/* for each file listed ... */
	while((cfgent = readdir(cfgdir)) != NULL) {
		struct stat sbuf;
		char fpath[256];
		sline_t tline;
		
		if(cfgent->d_name[0] == '.')
			continue;

		/* 
		 * The file name must match the name of a character special
		 * file in /dev
		 */
		sprintf(fpath, "/dev/%s", cfgent->d_name);
		if(!stat(fpath, &sbuf))
			if(!S_ISCHR(sbuf.st_mode)) {
				syslog(LOG_WARNING, "%s is not the name of a device",
					cfgent->d_name);
				continue;
			}
		else {
			syslog(LOG_WARNING, "%s is not the name of a device",
				cfgent->d_name);
			continue;
		}
	
		sprintf(fpath, "%s/%s", "/etc/btty.d", cfgent->d_name);
		if(!stat(fpath, &sbuf))
			if(!S_ISREG(sbuf.st_mode)) {
				syslog(LOG_WARNING, "%s is not a regular file", fpath);
				continue;
			}
		else {
			syslog(LOG_WARNING, "%s Can't stat file", fpath);
			continue;
		}
					
		/* See if the port already exists */
		for(i = sline_list; i != NULL; i = i->next)
			if(!strcmp(i->name, cfgent->d_name)) {
				read_cfgfile(fpath, i);
				break;
			}

		if(!i) {
			/* Create a new port entry */
			i = (sline_t *) calloc(sizeof(sline_t));
			if(!i) {
				syslog(LOG_ERR, "Out of memory registering port");
				continue;
			}

			/* Initialize */
			i->name = (char *) strdup(cfgent->d_name);

			/* Register the port */
			i->id = (unsigned) ioctl(k_fd, IOCREGISTER, cfgent->d_name);
			if(!i->id) {
				syslog(LOG_ERR, "Error registering port %s",
					cfgent->d_name);
				free(i);
				continue;
			}
			
			/* Add the port to the list */
			i->next = sline_list;
			sline_list = i;
			read_cfgfile(fpath, i);
		}
	}

	/***** This needs work!!! *****/
	/* unregister and delete all invalid ports */
	for(j = NULL, i = sline_list; i != NULL; j = i, i = i->next) {
		if(!i->valid) {
			ioctl(k_fd, IOCUNREGISTER, i->id);
			j->next = i->next;
			i = j;
			free(i);
		}
	}
}

/*
 * Read a configuration file
 */
int read_cfgfile(char *fpath, sline_t *ent)
{
	FILE *cfgfile;
	char *c;
	char *arg;

	cfgfile = fopen(fpath, "r");
	if(cfgfile == NULL) {
		syslog(LOG_WARNING, "Failed to open port config %s: %m",
			ent->name);
		return -errno;
	}

	c = (char *) calloc(1024);
	while((c = fgets(c, 1024, cfgfile)) != NULL) {
		int len;
		int inspace = 0;

		/* Non alpha marks comment line */
		if(!isalpha(*c))
			continue;

		/* Chop trailing whitespace */
		len = strlen(c); 	
		while(len && isspace(*(c + (len - 1)))) {
			*(c + (len - 1)) = '\0';
			len--;
		}
		len--;			/* make len an index */

		/* space or tab mark the argument */
		for(arg = c ; arg <= (c + len) ; arg++) {
			if(isspace(*arg)) {
				*arg = '\0';
				inspace = 1;
			}
			if(!isspace(*arg) && inspace)
				break;
		}

		/* match the keyword */
		if(!strcasecmp(c, "modem")) {
			ent->modem_ctrl = MODEM;
			continue;
		}
		if(!strcasecmp(c, "direct")) {
			ent->modem_ctrl = DIRECT;
			continue;
		}
		if(!strcasecmp(c, "crtscts")) {
			ent->flow_ctrl = HWFLOWCTRL;
			continue;
		}
		if(!strcasecmp(c, "xonxoff")) {
			ent->flow_ctrl = SWFLOWCTRL;
			continue;
		}
		if(!strcasecmp(c, "init")) {
			ent->init_str = (char *)strdup(arg);
			continue;
		}
		if(!strcasecmp(c, "reset")) {
			ent->reset_str = (char *) strdup(arg);
			continue;
		}
		if(!strcasecmp(c, "dial")) {
			ent->connect_str = (char *) strdup(arg);
			continue;
		}
		if(!strcasecmp(c, "hangup")) {
			ent->disconn_str = (char *) strdup(arg);
			continue;
		}
		if(!strcasecmp(c, "speed")) {
			ent->speed = atoi(arg);
			continue;
		}
		if(!strcasecmp(c, "program")) {
			ent->prog = (char *) strdup(arg);
			continue;
		}
		if(!strcasecmp(c, "log")) {
			ent->log = (char *) strdup(arg);
			continue;
		}

		/* Invalid keyword */
		syslog(LOG_NOTICE, "Unrecognized config keyword %s in %s ignored",
			c, fpath);
 	}

	ent->valid = TRUE;
	fclose(cfgfile);
	return 0;
}

/*
 * Handle a HUP signal in the main thread. Re-read the configuration
 */
void hup_handler(int sig)
{
	syslog(LOG_INFO, "Caught HUP signal");
	read_cfgdir();
}

/*
 * Handle USR1 and USR2 signals
 */
void usr_handler(int sig)
{
	switch(sig) {
		case SIGUSR1:
			if(log_mask < 7)
				log_mask++;
			else
				syslog(LOG_INFO, "Log level as high as it will go");
			break;
			
		case SIGUSR2:
			if(log_mask > 0)
				log_mask--;
			else
				syslog(LOG_INFO, "Log level as low as it will go");
			break;
			
		default:
			syslog(LOG_ERR, "Caught unknown signal??");
			return;
	}
	
	setlogmask(LOG_UPTO(log_mask));
}
