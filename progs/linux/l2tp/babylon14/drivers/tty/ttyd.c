/* 
 * ttyd.c - Daemon for dialing and hanging up on btty devices
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: ttyd.c,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include "ldisc.h"
#include "ttyd.h"
#include "version.h"

int ctrl_fd;
extern struct linedef ttyconfig[MAXCONFIG];
extern struct optiondef options;
struct processdef processes[MAXPORTNUM];
char *arg0;
int chatpid;
int GotSignal;

/*
 * Our signal handlers
 */
void dial_handler(int signal) {	
	int status;

	if (signal==SIGCHLD) {
		/* Child process died. Wait for funeral */
		wait(&status);
	} else {
		if (chatpid>0) {
			kill(chatpid,signal);
			chatpid=0;
		}
		syslog(LOG_NOTICE,"Caught signal %d!",signal);
		GotSignal=1;
	}
}

/*
 * Loop signal handler - rereads config file
 */
void loop_handler(int signal) {
	if (signal==SIGHUP) {
		if (read_config()==0) {
			syslog(LOG_NOTICE,"Re-read config file");
		} else {
			syslog(LOG_NOTICE,"Error re-reading config file");
		}
	}
}

/*
 * Routine to hangup a port - we do this by sending a SIGHUP to the process in
 * charge of the port which is generally a child of bttyd or is battach
 */
void hangup (int port) {
	pid_t pid;
	
	if (processes[port].pid>0) {
		kill(processes[port].pid,SIGHUP);
		pid=processes[port].pid;
		processes[port].pid=0;
		syslog(LOG_NOTICE,"Hangup complete port %d (pid %d)\n", port,pid);
	} else {
		syslog(LOG_NOTICE,"Hangup request possible idle port (%d). Forced idle.\n", port);
	}
	/* Force the port to idle - we ignore any failures*/
	ioctl(ctrl_fd, BIOCF_TTY_STAT_IDLE, port);
}

/*
 * Routine to dial. It configures the serial port, and then forks a process to dial
 * and camp on the line
 */
void dial(int portnum,char *num) {
	int ret,ldisc,i,failed;
	struct slinedef port;
	char process[80];
	char connect[256],tmp[256],*p;

	failed=0;
	chatpid=0;
	/* Find a port to use */
	if ((port.confignum=findserial(portnum))==-1) {
		/* We failed. Tell bdial*/
		ioctl(ctrl_fd, BIOCF_TTY_CONN_FAILED, portnum);
		syslog(LOG_NOTICE,"Dial attempt failed. No serial ports available.");
		return;
	}
	/* Find the phone number and build up a string with it in it */
	strcpy(tmp,ttyconfig[port.confignum].connect);
	p=strstr(tmp,"%n");
	*p=(char)0;
	p+=2;
	sprintf(connect,"%s%s%s",tmp,num,p);
	/* Open a port */
	if (open_port(&port)>0) {
		return;
	}
	processes[portnum].fd=port.fd;
	/* Setup the port */
	if (init_port(&port)>0) {
		close(port.fd);
		return;
	}

	signal(SIGCHLD,dial_handler);
	/* 
	 * Fork off a process to chat to the modem, make a connection,
	 * and camp on the line until we need to hangup
	 */
#ifndef NOFORK
	ret = fork();
	if (0 == ret) {
#endif
		
		sprintf(arg0,"bttyd [dialing port btty%d]",portnum);
		/* Catch the HUP and TERM signals */
		signal(SIGHUP,dial_handler);
		signal(SIGTERM,dial_handler);
		signal(SIGCHLD,dial_handler);
		if (do_chat(&port,connect)==0) {
			ldisc=N_BAB;	
			/* Set the line discipline to Babylon */
			sprintf(arg0,"bttyd [btty%d: Setting line disc]",portnum);
			if (ioctl(port.fd, TIOCSETD, &ldisc)==0) {
				sprintf(arg0,"bttyd [btty%d: Attaching port to babylon]",portnum);
				/* Tell babylon to attach to the tty */
				if (ioctl(port.fd, BIOC_TTY_ATTACH, portnum)==0) {
					sprintf(arg0,"bttyd [connected port btty%d]",portnum);
					syslog(LOG_NOTICE,"Line connected (btty%d)",portnum);
					GotSignal=0;
					/* Loop until we get told to hangup or the port status */
					/* changes to idle indicating we got hungup on */
					while (!GotSignal) {
						/* Check the port status - if this returns CS_IDLE or an error */
						/* the line got dropped without us! */
						if (ioctl(port.fd, BIOC_TTY_GETSTATUS, portnum)<1) {
							syslog(LOG_NOTICE,"Down by remote? (btty%d) (result:%d)",portnum);
							break;
						}
						sleep(1); /* Sleep for one second */
					}
					syslog(LOG_NOTICE,"Line dropped (btty%d)",portnum);
				} else {
					syslog(LOG_NOTICE,"Can't attached to Babylon on line %s.",ttyconfig[port.confignum].linename);
					failed=1;
				}
			} else {
				syslog(LOG_NOTICE,"Can't set line disc on line %s.",ttyconfig[port.confignum].linename);
				failed=1;
			}
		} else {
			failed=1;
		}
		
		/* Set the line discipline to normal */
		ldisc=0;	
		if (ioctl(port.fd, TIOCSETD, &ldisc)<0) {
			syslog(LOG_NOTICE,"Can't reset line disc on line %s.",ttyconfig[port.confignum].linename);
		}

		close(port.fd);
		/* If we failed, tell the user */
		if (failed) {
			if (ioctl(ctrl_fd, BIOCF_TTY_CONN_FAILED, portnum) != 0) {
				syslog(LOG_NOTICE,"Failed to indicated connection failed on %s.",ttyconfig[port.confignum].linename);
			}
		}
		unlock_port(&port);
		port.fd = 0;

#ifndef NOFORK
		exit(0);
	} else if (ret < 0) {
		syslog(LOG_NOTICE,"Dial attempt failed. Unable to fork process to dial.");

		/* Tell babylon this port is idle */
		if (ioctl(ctrl_fd, BIOCF_TTY_CONN_FAILED, portnum) != 0) {
			syslog(LOG_NOTICE,"Failed to indicated connection failed on %s.",ttyconfig[port.confignum].linename);
		}
		close(port.fd);
		unlock_port(&port);
		port.fd = 0;
		exit(1);
	} else {
		/* Successful fork - save the PID and file handle for hangup */
		processes[portnum].pid=ret;
		/* Close the port in the parent process */
		close(port.fd);
		port.fd = 0;
	}
#endif
}

/*
 * Routine to grab the port num and pid from battach and record it for hangup
 */
void inport(int portnum,pid_t pid) {
	if (pid>0) {
#ifdef DEBUG
		syslog(LOG_NOTICE,"inport: Set port btty%d with pid %d",portnum,pid);
#endif
		if (processes[portnum].pid==0) {
			processes[portnum].pid=pid;
			processes[portnum].confignum=0;
			processes[portnum].fd=0;
		} else {
			syslog(LOG_NOTICE,"Inport error: Port %d already in use",portnum);
		}
	} else {
#ifdef DEBUG
		syslog(LOG_NOTICE,"inport: Cleared port btty%d",portnum,pid);
#endif
		processes[portnum].pid=0;
		processes[portnum].confignum=0;
		processes[portnum].fd=0;
	}
}
	
/*
 * Routine to read the config file
 * NEEDS WORK: WON'T DO A REREAD ON SIGHUP
 */
int read_config() {
	extern FILE *yyin;
	int ret;
	
	/* Initialize configuration space */
	memset(&ttyconfig,sizeof(ttyconfig),(char)0);
	memset(&options,sizeof(options),(char)0);
	/* Set our global filename item */
	if ((yyin=fopen(CONFIGFILE,"r"))==NULL) {
		syslog(LOG_NOTICE,"Cannot open %s: %s\n",CONFIGFILE,strerror(errno));
		exit(1);
	}
	/* Parse the config file */
	if (yyparse()>0) {
		ret=1;	
	} else {
		ret=0;
	}
	fclose(yyin);
	return(ret);
}

/*
 * Routine to do the main processing. It's forks to a separate process by main
 */
int loop() {
	char buf[1024], *cmd, *num;
	int ret, portnum,i;
	Req msg;
	fd_set rfds;

	signal(SIGHUP,loop_handler);
	/* Open up our control file */
	if ((ctrl_fd = open("/proc/bttyctrl", O_RDONLY)) < 0) {
		syslog(LOG_NOTICE,"Cannot open /proc/bttyctrl: %s",strerror(errno));
		return(2);
	}
#if 0
	if (ioctl(ctrl_fd,BIOCF_TTY_RESET,0)!=0) {
		syslog(LOG_NOTICE,"Driver reset failed: %s",strerror(errno));
		return(2);
	}
#endif
	/* Create ports for each serial line defined in the config file */
	for (i=0;((i<MAXCONFIG) && (ttyconfig[i].linename[0]!=(char)0));i++) {
		if (ioctl(ctrl_fd,BIOCF_TTY_REGISTER_PORT,0)<0) {
			syslog(LOG_NOTICE,"Port registration failed for %s: %s",ttyconfig[i].linename,strerror(errno));
			return(2);
		}
	}
	FD_ZERO(&rfds);
	/* Loop forever */
	for(;;) {
		/* Camp on the device until we get some data */
		FD_SET(ctrl_fd, &rfds);
		select(ctrl_fd+1, &rfds, NULL, NULL, NULL);
		if (!FD_ISSET(ctrl_fd, &rfds)) {
			continue;
		}
		if ((ret = read(ctrl_fd, &msg, sizeof(Req))) > 0) {
			switch(msg.cmd) {
				case REQ_DIAL:
					dial(msg.port,msg.phone_num);
					break;
				case REQ_HANGUP:
					hangup(msg.port);
					break;
				case REQ_INPORT:
					inport(msg.port,msg.pid);
					break;
			}
		}
	}
	return(0);
}

/*
 * Routine to create our pid file in /var/run and barf if we are already
 * running. Returns 0 on success and 1 one error
 */
int pid_file() {
	int pid_fd,n;
	pid_t pid;
	char pid_str[12];

	memset(pid_str, '\0', 12);
	while(1) {
		if ((pid_fd = open(PID_FILE, O_RDWR | O_EXCL | O_CREAT, 0644))<0) {
			if(errno == EEXIST) {
				if (check_lock(PID_FILE,&pid)) {
					printf("\nbttyd already running as process %d\n\n",pid);
					return(1);
				}
				else {
					/* Unlink the old lock */
					unlink(PID_FILE);
					continue;
				}
			}
			else {
				perror(PID_FILE);
				return(1);
			}
		}
		
		/* Write our lock info */
		pid = getpid();
		sprintf(pid_str, "%010d\n", pid);
		write(pid_fd, pid_str, 11);
		close(pid_fd);
		return(0);
	}
}
int main(int argc,char *argv[]) {
	int ret;

	if ((argc>1) && (strcmp(argv[1],"-v")==0)) {
		printf("bttyd Version %s\n",version);
		exit(1);
	}
		
	if (getuid()!=0) {
		puts("\nThis application must be run as root\n");
		exit(2);
	}
	/* Wipe out our processes table */
	memset(&processes,sizeof(processes),(char)0);
	arg0=argv[0];
	chatpid=0;
#ifndef NOFORK
	/* fork out as a daemon */
	ret = fork();
	if (0 == ret) {
#endif
		/* Create our pid file and abort if we are already running */
		if (pid_file()==1) {
			exit(2);
		}
		strcpy(arg0,"bttyd [parent process]");
		/* Open syslog */
		openlog("bttyd",LOG_PID|LOG_CONS,LOG_DAEMON);
		/* Read the config file */
		if (read_config()==0) {
			syslog(LOG_NOTICE,"bttyd %s loaded",version);
			if ((ret=loop())==0) {
				syslog(LOG_NOTICE,"bttyd exited normally");
			} else {
				syslog(LOG_NOTICE,"bttyd exited on error");
			}
			closelog();
			exit(ret);
		} else {
			closelog();
			exit(1);
		}
#ifndef NOFORK
	} else if (ret<0) {
		syslog(LOG_NOTICE,"ttyd daemon fork failed: %s",strerror(errno));
		perror("ttyd daemon fork failed");
	}
#endif
	exit(0);
}

