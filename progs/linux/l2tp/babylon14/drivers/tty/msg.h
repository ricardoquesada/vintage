/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: msg.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _MSG_H_
#define _MSG_H_

/* IOCTL's */
#include <linux/ioctl.h>
#define IOCREGISTER		_IO('B', 100)
#define IOCUNREGISTER	_IO('B', 200)

/* Dialing states */
#define S_CONNECT		0x00
#define S_NOCARRIER		0x10
#define S_BUSY			0x11
#define S_NOANSWER		0x12
#define S_NODIALTONE	0x26
#define S_ERROR			0x100

/* Daemon messages */
#define M_CONNECT		0x03
#define M_HANGUP		0x04
#define M_STATE			0x05
#define M_OPEN			0x06
#define M_CLOSE			0x07

/* Daemon message structure */
struct bmsg {
	struct bmsg 	*next;
	unsigned int	id;
	unsigned int	function;
	unsigned int	call_id;
	union {
		char port_name[256];
		unsigned char state;
		char phone_num[32];
	} u;
};

/* Put a message at the end of the queue */
inline void mq_append(struct bmsg *q, struct bmsg *d)
{
	struct bmsg *a, *b;

	for(a=NULL, b=q ; b!=NULL ; a=b, b=b->next);
	if(a) 
		a->next = d;
	else
		q = d;
}

/* Take a message from the front of the queue */
inline struct bmsg *mq_pop(struct bmsg *q)
{
	struct bmsg *a;

	a = q;
	if(a) {
		q = a->next;
		a->next = NULL;
	}
	return a;
}

/* Put a message at the head of the queue */
inline void mq_push(struct bmsg *q, struct bmsg *m)
{
	m->next = q;
	q = m;
}

#endif /* _MSG_H_ */
