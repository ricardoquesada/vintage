/*
 * TTY Driver control program
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: ttyctrl.c,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include "ldisc.h"
#include "ttyd.h"
#include "version.h"

enum FLAGS {
	FLAG_RESET,
	FLAG_FORCE,
	FLAG_NEW,
	FLAG_CHECK,
	FLAG_KILL
};

void help();
char *arg0;

extern FILE *yyin;
extern struct linedef ttyconfig[MAXCONFIG];
extern struct optiondef options;

int main ( int argc, char *argv[] ) {
	int i,port,flag,ctrl_fd;
	
	arg0=argv[0];
	port=-1;
	if (argc<2) {
		help();
	}
	/* Process command line options */
	for (i=0;i<argc;i++) {
		if (argv[i][0]=='-') {
			switch(argv[i][1]) {
				case 'v':
					printf("ttyctrl Version %s\n",version);
					exit(0);
				case 'p':
					port=atoi(argv[i+1]);
					if ((port<0) || (port>MAXPORTNUM)) {
						printf("Invalid port number %d (max %d)",port,MAXPORTNUM);
						help();
					}
					i++;
					break;
				case 'r':
					if ((flag==FLAG_FORCE) || (flag==FLAG_NEW)) {
						puts("Can't use other command with force port idle");
						help();
					}
					flag=FLAG_RESET;
					break;
				case 'f':
					if ((flag==FLAG_RESET) || (flag==FLAG_NEW)) {
						puts("Can't use other command with force port idle");
						help();
					}
					flag=FLAG_FORCE;
					break;
				case 'n':
					if ((flag==FLAG_RESET) || (flag==FLAG_FORCE)) {
						puts("Can't use other command with new port");
						help();
					}
					flag=FLAG_NEW;
					break;
				case 'c':
					/* Initialize configuration space */
					memset(&ttyconfig,sizeof(ttyconfig),(char)0);
					memset(&options,sizeof(options),(char)0);
					/* Set our global filename item */
					if ((yyin=fopen(CONFIGFILE,"r"))==NULL) {
						printf("Cannot open %s: %s\n",CONFIGFILE,strerror(errno));
						exit(1);
					}
					/* Parse the config file */
					if (yyparse()>0) {
						puts("Error on config file");
					} else {
						puts("Config file OK");
					}
					fclose(yyin);
					exit(0);
#ifdef DEBUG
				case 'k':
					flag=FLAG_KILL;
					break;
#endif
				case 'h':
					help();
					break;
				default:
					puts("Bad command argument. Use -h for help");
					exit(1);
			}
		}
	}
	if ((ctrl_fd = open("/proc/bttyctrl", O_RDONLY)) < 0) {
		perror("TTY: Cannot open /proc/bttyctrl");
		exit(1);
	}
	switch (flag) {
		case FLAG_RESET:
			if (ioctl(ctrl_fd,BIOCF_TTY_RESET,0)!=0) {
				perror("TTY: Driver reset failed");
				exit(1);
			} else {
				puts("TTY: Driver reset.");
			}
			break;
		case FLAG_FORCE:
			if (port==-1) {
				puts("TTY: Force command requires a port number");
				help();
			}
			if (ioctl(ctrl_fd,BIOCF_TTY_STAT_IDLE,port)!=0) {
				perror("TTY: Set port idle failed");
				exit(1);
			} else {
				printf("TTY: Port %d set to status idle.\n",port);
			}
			break;
		case FLAG_NEW:
			if (ioctl(ctrl_fd,BIOCF_TTY_REGISTER_PORT,0)!=0) {
				perror("TTY: New port create failed");
				help();
			} else {
				puts("TTY: Created new port");
			}
			break;
#ifdef DEBUG
		case FLAG_KILL:
			if (ioctl(ctrl_fd,BIOCF_TTY_KILL,0)!=0) {
				perror("TTY: Kill driver failed");
				help();
			} else {
				puts("TTY: Command successful");
			}
			break;
			
#endif
	}
	close(ctrl_fd);
}
			

/*
 * Routine to print help info
 */
void help() {
	printf("%s [-hpf]\n",arg0);
	puts("   -h  this info");
	puts("   -p  port number");
	puts("   -r  reset driver");
	puts("   -f  force port idle (USE WITH CAUTION)");
	puts("   -n  create a new port");
	puts("   -v  version");
	exit(1);
}
			
