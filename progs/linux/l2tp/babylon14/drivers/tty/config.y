%{
/*
 * config.y - parser for bttyd config file
 *
 * (C) Spellcaster Telecommunications, Inc.
 *
 * $Id: config.y,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/file.h>
#include <syslog.h>
#include <termios.h>
#include "ttyd.h"

extern int lineno;
extern char *yytext;

struct linedef ttyconfig[MAXCONFIG];
struct optiondef options;

char filename[80];
int ttynum=-1;
int modemnum=-1;
int i,foundit;

%}

%union {
	char	*string;
	unsigned int	num;
}

%token	<string>	QSTRING ID LINE TYPE LOCAL MODEM FLOW XONXOFF RTSCTS CONNECT
%token	<string>	CHATPROG CHATPARAMS
%token	<num>		SPEED BAUD300 BAUD1200 BAUD2400 BAUD9600 BAUD19200 BAUD38400 BAUD115200
%token	<num>		BAUD230400 BAUD460800 BAUD57600

%type <num>		baud

%start start

%%

start:	options lines
	;

/* Options processing */
options:	option
	|	options option
	;

option:		chatprog
	|	chatparams
	;
	
chatprog:	CHATPROG QSTRING { strcpy(options.chatprog,$2); }
	;	

chatparams:	CHATPARAMS QSTRING { strcpy(options.chatparams,$2); }
	;	

/* Lines processing */	
lines:	line
	| lines line
	;

line:	linename linecontents   /* bttyd.conf stuff */
	| linename
	;

linename:	LINE ID	{
			char s[80];
			
			ttynum++;
			strcpy(ttyconfig[ttynum].linename,$2);
			sprintf(s,"/dev/%s",$2);
			if (access(s,F_OK)!=0) {
				sprintf(s,"Line /dev/%s does not exist",$2);
				yyerror(s);
				exit(1);
			}
	}
	;

linecontents:	linecontent
	|	linecontents linecontent
	;

linecontent:	speed
	|	 type
	|	flow
	|	connect
	;

type:	TYPE LOCAL { ttyconfig[ttynum].type=TYPE_LOCAL; }
	| TYPE MODEM { ttyconfig[ttynum].type=TYPE_MODEM; }
	;

flow:	FLOW XONXOFF { ttyconfig[ttynum].flow=FLOW_XON; }
	| FLOW RTSCTS { ttyconfig[ttynum].flow=FLOW_RTS; }
	;

connect:	CONNECT QSTRING { strcpy(ttyconfig[ttynum].connect,$2); }
		;

speed:	SPEED baud { ttyconfig[ttynum].speed=$2; }
	;

baud:	BAUD300		{ $$=B300; }
	| BAUD1200	{ $$=B1200; }
	| BAUD2400	{ $$=B2400; }
	| BAUD9600	{ $$=B9600; }
	| BAUD19200	{ $$=B19200; }
	| BAUD38400	{ $$=B38400; }
	| BAUD57600	{ $$=B57600;	}
	| BAUD115200	{ $$=B115200; }
	| BAUD230400	{ $$=B230400; }
	| BAUD460800	{ $$=B460800; }
	;

%%


/*
 * yyerror - routine to report an error and abort
 */
int yyerror(const char *msg) {
#ifdef TTYCTRL
	printf("ERROR in %s line %d: %s at '%s'\n",CONFIGFILE,lineno,msg,yytext);
#else
	syslog(LOG_ERR,"ERROR in %s line %d: %s at '%s'\n",CONFIGFILE,lineno,msg,yytext);
#endif
}

/*
 * yywrap - returns 1 to say we's all done folks
 */
int yywrap() {
	return(1);
}

/*
 * warning - warns the user of weirdness
 */
void warning(char *msg) {
	syslog(LOG_NOTICE,"WARNING: line %d: %s\n",lineno,msg);
}
