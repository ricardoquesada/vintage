/*
 * scbctrl.h - Adapter control utility
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: scbctrl.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
#define DCB_CMD_LOAD	1
#define DCB_CMD_RESET	2
#define DCB_CMD_START	3
#define DCB_CMD_DEBUG	4
#define DCB_CMD_SWITCH	5
#define DCB_CMD_SPID	6
#define DCB_CMD_DN	7
#define DCB_CMD_TRACE	8
#define DCB_CMD_STAT	9
#define DCB_CMD_SPEED	10

typedef struct {
	int cmd;
	char arg[255];
	int adapter;
	int channel;
} command;

void pushcmd(command *);
command *popcmd(void);

#define DEVFILE		"/dev/apsisdn"
