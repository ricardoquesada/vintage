/*
 * scbctrl.c - Adapter control utility
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: scbctrl.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <limits.h>

typedef u_int32_t	u32;
typedef u_int16_t	u16;
typedef u_int8_t	u8;

#include "message.h"
#include "scioc.h"
#include "scbctrl.h"
#include "hardware.h"

/* The device name template for the dcb */
static char *devfile, *cmdname;
static char devicename[256];
static unsigned devnumidx;

/* Options we support (see the man page) */
static char shrt_options[] = "D:l:rRM::d:c:a:vgw::n::s::up::E::S::F::C::L::J::Y::UtT:K::";
static struct option long_options[] = {
	{ "load", required_argument, NULL, 'l' },
	{ "trace", required_argument, NULL, 'T' },
	{ "reset", no_argument, NULL, 'r' },
	{ "factory", no_argument, NULL, 'R' },
	{ "mode", optional_argument, NULL, 'M' },
	{ "adapter", required_argument, NULL, 'd' },
	{ "channel", required_argument, NULL, 'c' },
	{ "span", required_argument, NULL, 'a' },
	{ "verbose", no_argument, NULL, 'v' },
	{ "restart", no_argument, NULL, 'g' },
	{ "switch", optional_argument, NULL, 'w' },
	{ "dn", optional_argument, NULL, 'n' },
	{ "spid", optional_argument, NULL, 's' },
	{ "status", no_argument, NULL, 'u' },
	{ "linkstats", no_argument, NULL, 'U' },
	{ "speed", optional_argument, NULL, 'p' },
	{ "signal", optional_argument, NULL, 'S' },
	{ "frame", optional_argument, NULL, 'F' },
	{ "crc6", optional_argument, NULL, 'C' },
	{ "line-len", optional_argument, NULL, 'L' },
	{ "jitter", optional_argument, NULL, 'J' },
	{ "csu", no_argument, NULL, 't' },
	{ "carrier", required_argument, NULL, 'K' },
	};

static int verbose = 0;

int do_ioctl(int adapter, int command, scs_ioctl *data);
int upload_firmware(unsigned int, char *);
void print_usage();
void print_status(unsigned int);

const char *boardname[] = {
	"DataCommute/BRI",
	"TeleCommute/BRI", 
	"DataCommute/PRI",
	"TeleCommute/Plus",
	"DataCommute/BRIe",
	"DataCommute/PRI Plus",
	"DataCommute/Plus",
	"DataCommute/PRIe"
};

const char *briswitches[] = {
	"National ISDN-1",
	"AT&T 5ESS MultiPoint", 
	"NorTel DMS100",
	"AT&T 5ESS PointToPoint",
	"ETSI Multipoint",
	"ETSI Point-to-Point"
};

const char *priswitches[] = {
	"AT&T 4ESS",
	"AT&T 5ESS",
	"NorTel DMS100",
	"National ISDN-2",
	"ETSI"
};

const char *states[] = {
	"Unavailable",
	"Ready",
	"Connecting...",
	"Connected",
	"Disconnecting..."
};

const char *switch_states[] = {
	"DOWN",
	"UP"
};

const char *l2_states[] = {
	"",
	"TEI Unassigned",
	"Assign TEI", 
	"Establish TEI",
	"TEI Assigned",
	"Establishing",
	"Releasing", 
	"UP",
	"Timer Recovery"
};

const char *pri_states[] = { "--", "RD", "CG", "CN", "DG" };
const char *call_type[] = { "56", "64"};
const char *params_signal[] = { "AMI", "B8ZS"};
const char *params_frame[] = { "F12", "F4", "ESF", "F72" };
const char *params_crc6[] = { "off", "on" };

const char *params_line_len[] = {
	"NA",
	"0-50",
	"20-80",
	"60-130",
	"110-200",
	"140-230",
	"210-290",
	"270-320"
};

const char *params_jitter[] = { "off", "on" };

int status = 0;

void print_linkstats(unsigned int card)
{
	scs_ioctl	theCommand;
	boardInfo	bi;
	int	i, j;

	memset(&theCommand, 0, sizeof(theCommand));
	memset(&bi, 0, sizeof(boardInfo));

	/*
	 * Get the adapter resource info
	 */
	theCommand.device = card;
	theCommand.command = SCIOCGETCARDINFO;
	theCommand.dataptr = &bi;
	if(do_ioctl(card, theCommand.command, &theCommand)) {
		printf("Error getting card info\n");
		return;
	}

	printf("%-9s | %-10s %-10s %-10s | %-10s %-10s %-10s\n", "Channel", "Rx Good", "Rx Bad", "Rx Dropped", "Tx Good", "Tx Bad", "Tx Dropped");
	puts("----------|----------------------------------|----------------------------------");

	for (j=0; j<bi.num_spans; j++) {
	for (i=0; i<=bi.num_channels; i++) {
		LnkStats	stats;

		theCommand.device = card;
		theCommand.channel = i;
		theCommand.span = j;
		theCommand.command = SCIOCGETLNKSTAT;
		theCommand.dataptr = &stats;
		if (do_ioctl(card, theCommand.command, &theCommand)) {
			perror("Error getting status information");
			return;
		}

		if (0 == i)
			printf("  D       |");
		else
			printf("  B%-2d     |", i);

		printf(" %10ld %10ld %10ld | %10ld %10ld %10ld\n",
			stats.rx_good, stats.rx_bad, stats.rx_dropped,
			stats.tx_good, stats.tx_bad, stats.tx_dropped);
	}
	}
}

int main(int argc, char **argv)
{
	char opt;
	unsigned int opt_indx;
	unsigned int adapter = 0;
	unsigned int span = 0;
	scs_ioctl theCommand;
	unsigned int channel = 0;
	int options = 0;

	memset(&theCommand, 0, sizeof(theCommand));

	cmdname = rindex(argv[0], '/');
	if (!cmdname)
		cmdname = argv[0];
	else
		cmdname++;

	if (!strcmp(cmdname, "dcplusc")) {
		devfile = "/dev/bab/0";
		strcpy(devicename, "dcplus0");
		devnumidx = 6;
	} else if (!strcmp(cmdname, "scbctrl")) {
		devfile = "/dev/bab/0";
		strcpy(devicename, "scb0");
		devnumidx = 3;
	} else {
		fprintf(stderr, "%s: Not called as either dcplusc or scbctrl!\n", cmdname);
		exit(2);
	}

	/* Suck in the command line options and push to the command queue */
	while((opt = getopt_long(argc, argv, shrt_options, long_options, &opt_indx)) != EOF) {
		switch(opt) {
		case '?':     /* unknown option */
			print_usage();
			return -1;

		case 'D':	/* device file to use */
			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			devfile = optarg;
			break;

		case 'd':	/* device ID if multiple adapters */
			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if ((adapter = atoi(optarg)) < 0 || adapter > 9) {
				fprintf(stderr, "Invalid device ID\n");
				return -1;
			}
			devicename[devnumidx] = '0' + adapter;
			break;

		case 'c':	/* channel number */
			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			channel = atoi(optarg) + 1;
			break;

		case 'a':	/* span number */
			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			span = atoi(optarg);
			break;

		case 'l':	/* load firmware code */
			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			return upload_firmware(adapter, optarg);

		case 'T':	/* set trace flag */
		{
			unsigned long	ul;
			u32	trace;

			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if (!optarg) {
				fprintf(stderr, "Missing trace value.\n");
				return -1;
			}

			ul = strtoul(optarg, NULL, 0x10);
			if (ULONG_MAX == ul && 0 != errno) {
				fprintf(stderr, "Trace value invalid: %s\n", strerror(errno));
				return -1;
			}

			trace = ul;
			theCommand.command = SCIOCTRACE;
			theCommand.device = adapter;
			theCommand.dataptr = &trace;
			if (do_ioctl(adapter, theCommand.command, &theCommand)) {
				fprintf(stderr, "%s: set trace failed\n",devicename);
				return -1;
			} else {
				if (verbose)
					printf("%s: Trace value set to: %08lx.\n", devicename, (long)trace);
				return 0;
			}
		}

		case 'r':	/* reset with no restart */
			theCommand.command = SCIOCRESET;
			theCommand.device = adapter;
			if (do_ioctl(adapter, theCommand.command, &theCommand)) {
				printf("%s: Reset Failed\n",devicename);
				return -1;
			} else {
				if(verbose)
					printf("%s: Adapter Reset.\n", devicename);
				return 0;
			}

		case 'R':	/* revert to factory default settings */
			theCommand.command = SCIOCSETFACTDFLT;
			theCommand.device = adapter;
			if (do_ioctl(adapter, theCommand.command, &theCommand)) {
				printf("%s: setting factory defaults failed.\n", devicename);
				return -1;
			} else {
				if(verbose)
					printf("%s: factory defaults set.\n", devicename);
				return 0;
			}

		case 'g':	/* start firmware */
			theCommand.command = SCIOCSTART;
			theCommand.device = adapter;
			if(do_ioctl(adapter, theCommand.command, &theCommand)) {
				printf("%s: Firmware start failed\n", devicename);
				return -1;
			} else {
				if(verbose)
					printf("%s: Firmware started.\n", devicename);
				return 0;
			}

		case 'v':	/* to be or not to be verbose that is */
			verbose = !verbose;
			break;

		case 'w':	/* set or return the switch type */
		{
			unsigned char switchtype = 0xff;

			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if(optarg) {
				/*
				 * Set the switch type
				 */
				theCommand.command = SCIOCSETSWITCH;
				theCommand.device = adapter;

				/*
				 * Convert the Mnemonic to a char code
				 */
				if (!strcmp(optarg, "NI1") || !strcmp(optarg, "4ESS"))
					switchtype = 0;
				else if (!strcmp(optarg, "5ESSMP"))
					switchtype = 1;
				else if (!strcmp(optarg, "5ESSPTP"))
					switchtype = 3;
				else if (!strcmp(optarg, "DMS100"))
					switchtype = 2;
				else if (!strcmp(optarg, "NI2"))
					switchtype = 3;
				else if (!strcmp(optarg, "ETSI"))
					switchtype = 4;
				else {
					fprintf(stderr, "Unrecognized switch type '%s'\n", optarg);
					return -1;
				}
				theCommand.dataptr = (unsigned char *) &switchtype;
				return do_ioctl(adapter, theCommand.command, &theCommand);
			}
			else {
				theCommand.command = SCIOCGETSWITCH;
				theCommand.device = adapter;
				theCommand.dataptr = &switchtype;
				do_ioctl(adapter, theCommand.command, &theCommand);

				switch(switchtype) {
					case 0:
						printf("National ISDN 1(BRI)/AT&T 4ESS(PRI)\n");
						break;
					case 1:
						printf("AT&T 5ESS Multipoint\n");
						break;
					case 2:
						printf("Northern Telecomm DMS100\n");
						break;
					case 3:
						printf("AT&T 5ESS PtP(BRI)/National ISDN 2(PRI)\n");
						break;
					case 4:
						printf("ETSI\n");
						break;
					default:
						printf("Error getting switch type.\n");
						return -1;
				}
			}	
			return status;
		}

		case 's':	/* set or return the spid */
		{
			char spid[SCIOC_SPIDSIZE];

			/*
			 * We need a channel for this one
			 */
			if(!channel) {
				if(verbose)
					printf("%s: Channel required for get/set SPID\n", devicename);
				return -1;
			}

			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if(optarg) {
				theCommand.command = SCIOCSETSPID;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) optarg;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error setting SPID for channel %d\n", devicename, channel);
					return -1;
				} else {
					if(verbose)
						printf("%s: SPID for channel %d set to %s\n", devicename, channel, optarg);
					return 0;
				}
			}
			else {
				theCommand.command = SCIOCGETSPID;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) spid;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error getting SPID for channel %d\n", devicename, channel);
					return -1;
				} else {
					if(verbose)
						printf("%s: SPID for channel %d is ", devicename, channel);
					printf("%s\n", spid);
					return 0;
				}
			}
		}

		case 'n':	/* set or return the dn */
		{
			char dn[SCIOC_DNSIZE];

			/*
			 * We need a channel
			 */
			if(!channel) {
				if(verbose)
					printf("%s: Channel required for get/set DN\n", devicename);
				return -1;
			}

			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if(optarg) {
				theCommand.command = SCIOCSETDN;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) optarg;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error setting DN for channel %d\n", devicename, channel);
					return -1;
				}
				else {
					if(verbose)
						printf("%s: DN for channel %d set to %s\n", devicename, channel, optarg);
					return 0;
				}
			}
			else {
				theCommand.command = SCIOCGETDN;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) dn;
				if (do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error getting DN for channel %d\n", devicename, channel);
					return -1;
				} else {
					if(verbose)
						printf("%s: DN for channel %d is ", devicename, channel);
					printf("%s\n", dn);
					return 0;
				}
			}
		}

		case 'u':	/* print channel status */
			print_status(adapter);
			return 0;
			break;

		case 'U':	/* print link statistics */
			print_linkstats(adapter);
			return 0;
			break;

		case 'M':	/* set/get the card's mode */
		{
			u8 mode;

			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			theCommand.device = adapter;
			theCommand.channel = channel;
			theCommand.span = span;
			theCommand.dataptr = (char *)&mode;

			if (optarg) {
				theCommand.command = SCIOCSETCARDMODE;
				mode = atoi(optarg);

				if (do_ioctl(adapter, theCommand.command, &theCommand)) {
					fputs(devicename, stderr);
					perror("Error setting mode for card");
					return -1;
				}
			}

			theCommand.command = SCIOCGETCARDMODE;
			if (do_ioctl(adapter, theCommand.command, &theCommand)) {
				fputs(devicename, stderr);
				perror("Error getting mode for card");
				return -1;
			}

			printf("%s: mode is %d\n", devicename, mode);
			return 0;
		}

		case 'K':	/* set or return the carrier system (PRI only) */
		{
			char carrier[SCIOC_CARRIERSIZE];


			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if(optarg) {
				if(!strcasecmp(optarg, "CEPT"))
					carrier[0] = CEPT;
				else if (!strcasecmp(optarg, "T1"))
					carrier[0] = T1; 

				/*
				 * We need a channel for this one
				 */
				if(!channel) {
					if (carrier[0] == CEPT)
						carrier[1] = 30;
					if (carrier[0] == T1)
						carrier[1] = 23;
				} else
					carrier[1] = channel - 1;

				if(verbose) {
					printf("%s: Active channels set to %d\n", devicename, carrier[1]);
					return -1;
				}

				theCommand.command = SCIOCSETCARRIER;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) carrier;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: error setting carrier\n", devicename);
					return -1;
				} else {
					if(verbose)
						printf("%s: carrier set to %s with %d active channels.\n", devicename, carrier[0] == CEPT ? "CEPT" : "T1", carrier[1]);
					return 0;
				}
			}
			else {
				theCommand.command = SCIOCGETCARRIER;
				theCommand.device = adapter;
				theCommand.channel = 0;
				theCommand.span = 0;
				theCommand.dataptr = (char *) carrier;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					printf("%s: Error getting carrier.\n", devicename);
					return -1;
				} else {
					printf("%s: Carrier is %s with %d active channels.\n", devicename, carrier[0] == CEPT ? "CEPT" : "T1", carrier[1]);
					return 0;
				}
			}
		}

		case 'p':	/* set the call speed */
		{
			char speed = -1;
			/*
			 * We need a channel
			 */
			if(!channel) {
				if(verbose)
					printf("%s: Channel required for get/set call speed\n", devicename);
				return -1;
			}

			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if(optarg) {
				theCommand.command = SCIOCSETSPEED;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;

				if (!strcmp(optarg,"64"))
					speed = 0;
				else if (!strcmp(optarg,"56"))
					speed = 1;
				else {
					print_usage();
					return -1;
				}
				theCommand.dataptr = (char *) &speed;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error setting speed for channel %d\n", 
						       devicename, channel);
					return -1;
				}
				else {
					if(verbose)
						printf("%s: Speed for channel %d set to %s\n", 
						       devicename, channel, optarg);
					return 0;
				}
			} else {
				theCommand.command = SCIOCGETSPEED;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) &speed;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error getting speed for channel %d\n", 
						       devicename, channel);
					return -1;
				} else {
					if(verbose)
						printf("%s: Speed for channel %d is ", devicename, channel);
					switch(speed) {
					case 0:
						printf("64Kbps unrestricted\n");
						break;
					case 1:
						printf("56Kbps rate adapted\n");
						break;
					default:
						printf("Error getting speed\n");
						return -1;
					}

					return 0;
				}
			}
		}

		case 'E':
		{
			if (!optarg && optind < argc && argv[optind][0] != '-')
				optarg = argv[optind++];

			if(optarg) {
				theCommand.command = SCIOCSETSERIAL;
				theCommand.device = adapter;
				theCommand.channel = channel;
				theCommand.span = 0;
				theCommand.dataptr = (char *) optarg;
				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					if(verbose)
						printf("%s: Error setting serial number\n", devicename);
					return -1;
				} else {
					if(verbose)
						printf("%s: Serial number set to %s\n", devicename, optarg);
					return 0;
				}
			}
		}
		case 't':	/* toggle CSU enable */
		case 'S':	/* Set PRI signal coding */
		case 'F':	/* Set PRI frame mode */
		case 'C':	/* Toggle PRI CRC6 support */
		case 'L':	/* Set T1 buildout */
		case 'J':	/* Toggle PRI jitter attenuation */
		{
			/* 
			 * Retreive current parameter settings to 
			 * delta against 
			 */
			PRIparameters pi;
			boardInfo bi;

			memset(&bi, 0, sizeof(bi));

			theCommand.device = adapter;
			theCommand.command = SCIOCGETCARDINFO;
			theCommand.dataptr = &bi;
			if(do_ioctl(adapter, theCommand.command, &theCommand)) {
				printf("Error getting card info\n");
				return -1;
			}

			theCommand.command = SCIOCGETPARAMS;
			theCommand.device = adapter;
			theCommand.dataptr = &pi;
			if(do_ioctl(adapter, theCommand.command, &theCommand)) {
				if(verbose)
					printf("%s: Error getting PRI device parameters\n", devicename);
				return -1;
			} else {
				switch(opt) {
				case 'S':
					if (!optarg && optind < argc && argv[optind][0] != '-')
						optarg = argv[optind++];
					if(!strcasecmp(optarg, "AMI"))
						pi.signal_code = SIGCODE_AMI;
					else if(!strcasecmp(optarg, "B8ZS"))
						pi.signal_code = SIGCODE_B8ZS;
					else {
						print_usage();
						return -1;
					}
					break;

				case 'F':
					if (!optarg && optind < argc && argv[optind][0] != '-')
						optarg = argv[optind++];
					if(!strcasecmp(optarg, "F12"))
						pi.frame_mode = FRAMODE_F12;
					else if (!strcasecmp(optarg, "F4"))
						pi.frame_mode = FRAMODE_F4;
					else if (!strcasecmp(optarg, "F72"))
						pi.frame_mode = FRAMODE_F72;
					else if (!strcasecmp(optarg, "ESF"))
						pi.frame_mode = FRAMODE_ESF;
					else {
						print_usage();
						return -1;
					}
					break;

				case 't':
					pi.csu_enable = !pi.csu_enable;
					break;

				case 'C':
					if (pi.crc6_enable)
						pi.crc6_enable = CRC6_DISABLED;
					else
						pi.crc6_enable = CRC6_ENABLED;
					break;

				case 'L':
					if (!optarg && optind < argc && argv[optind][0] != '-')
						optarg = argv[optind++];

					if (!strcasecmp(optarg, "NA"))
						pi.line_length = LLEN_NA;
					else if (!strcasecmp(optarg, "CEPT"))
						pi.line_length = LLEN_NA;
					else if (!strcasecmp(optarg, "0-50"))
						pi.line_length = LLEN_50;
					else if (!strcasecmp(optarg, "20-80"))
						pi.line_length = LLEN_80;
					else if (!strcasecmp(optarg, "60-130"))
						pi.line_length = LLEN_130;
					else if (!strcasecmp(optarg, "110-200"))
						pi.line_length = LLEN_200;
					else if (!strcasecmp(optarg, "140-230"))
						pi.line_length = LLEN_230;
					else if (!strcasecmp(optarg, "210-290"))
						pi.line_length = LLEN_290;
					else if (!strcasecmp(optarg, "270-320"))
						pi.line_length = LLEN_320;
					else {
						print_usage();
						return -1;
					}
					break;

				case 'J':
					if (PRIPLUS_BOARD == bi.modelid) {
						pi.bypass_jitter ++;
						pi.bypass_jitter %= 3;
					} else {
						if (pi.bypass_jitter)
							pi.bypass_jitter = BPJIT_DISABLED;
						else
							pi.bypass_jitter = BPJIT_ENABLED;
					}
					break;
				}

				/*
				 * Write the parameters back to the board
				 */
				theCommand.command = SCIOCSETPARAMS;

				if(do_ioctl(adapter, theCommand.command, &theCommand)) {
					fprintf(stderr, "%s: Error setting params: %s\n", devicename, strerror(errno));
					return -1;
				} else {
					if(verbose)
						printf("%s: Parameters Set\n", devicename);
					return 0;
				}

			}
		}

		default:
			print_usage();
			return -1;

		} /* switch */
	} /* while */

	if (!options) {	/* No options */
		print_usage();
		return -1;
	}

	return 0;
}


int upload_firmware(unsigned int card, char *filename)
{
	FILE *f;
	char srec[SCIOC_SRECSIZE+8];
	int  pcomplete;
	unsigned long fsize, fsent;
	struct stat filestat;
	scs_ioctl theCmd;
	int			fd;
	int last_pcompl=-1;

	theCmd.command = SCIOCLOAD;
	theCmd.device = card;

	fd = open(devfile, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		printf("Failed\n");
		fprintf(stderr, "Error opening %s: %s\n", devfile, strerror(errno));
		return errno;
	}

	f = fopen(filename, "r");
	if (f == NULL) {
		printf("Error opening %s: %s\n", filename, strerror(errno));
		return -1;
	}

	if(stat(filename, &filestat))
		printf("%s\n", strerror(errno));
	fsize = filestat.st_size;
	fsent = 0;

	while (!feof(f)) {
		int i;
		/*
		 * Scan in an input line
		 */
		fgets(srec, sizeof(srec), f);
		srec[sizeof(srec) - 1] = 0;
		i = strlen(srec);
		fsent += i;
		while (i > 0 && isspace(srec[i-1]))
			srec[--i] = '\0';
		theCmd.dataptr = (char *) srec;

		/*
		 * Send it to the adapter
		 */
		pcomplete = (fsent*100UL)/fsize;
		if (ioctl(fd, SCIOCLOAD, &theCmd) < 0) {
			fprintf(stderr, "\nioctl failed: %s\n", 
				strerror(errno));
			return errno;
		}
		/* don't print the message too often as it *really* slows down
		 * flashing of the PRI
		 */
		if (last_pcompl != pcomplete) {
			last_pcompl = pcomplete;
			printf("%3d%% complete %6ld/%6ld\r", 
				pcomplete, fsent, fsize);
			fflush(stdout);
		}
	}
	printf("\nLoading Complete\n");
	fclose(f);
	close(fd);
	return 0;
}

/*
 * do_ioctl send a simple ioctl message to ISDN4Linux's /dev/isdnctrl
 * It requires an adapter id, command number and dcbri_ioctl payload
 */
int do_ioctl(int adapter, int command, scs_ioctl *data)
{
	int fd;

	fd = open(devfile, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		printf("Failed\n");
		fprintf(stderr, "Error opening %s: %s\n", devfile, strerror(errno));
		return errno;
	}

	if (ioctl(fd, command, data) < 0) {
		fprintf(stderr, "ioctl failed: %s\n", strerror(errno));
		return errno;
	}
	close(fd);
	return 0;
}

void print_usage()
{
	fprintf(stderr, "Usage:  %s [options]\n", cmdname);
	fprintf(stderr, "   -l, --load <file>  download firmware (see man page)\n");
	fprintf(stderr, "   -r, --reset        reset the specified card\n");
	fprintf(stderr, "   -R, --factory      revert flash settings to factory defaults\n");
	fprintf(stderr, "   -d, --adapter <n>  refer to adapter n (0 is the first)\n");
	fprintf(stderr, "   -a, --span <n>     refer to span n (DataCommute/PRI Plus only)\n");
	fprintf(stderr, "   -c, --channel <n>  refer to channel n (0 is the first)\n");
	fprintf(stderr, "   -v, --verbose      make more noise\n");
	fprintf(stderr, "   -g, --restart      restart the firmware (a reset must be done prior)\n");
	fprintf(stderr, "   -w, --switch       set the switch type of the adapter (see man page)\n");
	fprintf(stderr, "   -n, --dn <#>       set the channel's Directory Number\n");
	fprintf(stderr, "   -s, --spid <#>     set the channel's SPID\n");
	fprintf(stderr, "   -u, --status       obtain status info from the card\n");
	fprintf(stderr, "   -U, --linkstats    obtain link statistics from the card\n");
	fprintf(stderr, "   -p, --speed        set the speed of the channel (see man page)\n");
	fprintf(stderr, "   -S, --signal       set the adapter's signal encoding scheme (see man page)\n");
	fprintf(stderr, "   -F, --frame        set the framing mode of an adapter (see man page)\n");
	fprintf(stderr, "   -C, --crc6         toggle the crc6 function of a PRI adapter\n");
	fprintf(stderr, "   -J, --jitter       toggle the jitter attenuation on a PRI adapter\n");
	fprintf(stderr, "   -t, --csu          toggle the CSU enable on a DataCommute/PRI Plus\n");
	fprintf(stderr, "   -L, --line-len     set the line length or buildout of a PRI adapter\n");
	fprintf(stderr, "   -K, --carrier <n>  set the card carrier system\n");
	fprintf(stderr, "   -M, --mode <n>     set the card mode (DANGER)\n");
	fprintf(stderr, "        See %s(8) for details.\n", cmdname);
}

void print_status(unsigned int card)
{
	scs_ioctl theCommand;
	boardInfo bi;
	char procVer[16];
	unsigned char switch_type;
	char spid[SCIOC_SPIDSIZE];
	char dn[SCIOC_DNSIZE];
	PRIparameters params;
	PhyStat_bri briphy;
	PhyStat_pri priphy;
	acfaStat acfa;
	int i;

	memset(procVer, 0, sizeof(procVer));
	memset(&bi, 0, sizeof(boardInfo));
	memset(&theCommand, 0, sizeof(theCommand));

	/*
	 * Get the adapter resource info
	 */
	theCommand.device = card;
	theCommand.span = 0;
	theCommand.channel = 0;
	theCommand.command = SCIOCGETCARDINFO;
	theCommand.dataptr = &bi;
	if(do_ioctl(card, theCommand.command, &theCommand)) {
		printf("Error getting card info\n");
		return;
	}

	/*
	 * Get the adapter firmware version
	 */
	theCommand.device = card;
	theCommand.command = SCIOCGETPROCVER;
	theCommand.dataptr = procVer;
	if(do_ioctl(card, theCommand.command, &theCommand)) {
		printf("Error getting firmware version\n");
		return;
	}
	procVer[4] = 0;

	/*
	 * Get the adapter switch type
	 */
	theCommand.device = card;
	theCommand.command = SCIOCGETSWITCH;
	theCommand.dataptr = &switch_type;
	if(do_ioctl(card, theCommand.command, &theCommand)) {
		printf("Error getting switch type\n");
		return;
	}
	/* Print what we got */
	printf("\n--[ SpellCaster ISDN Adapter %s Information ]---------------------------------\n", devicename);
	printf("  Model: %s  Part No.: %s  Serial No.: %s\n", 
		boardname[bi.modelid], bi.part_no, bi.serial_no);
	printf("  Firmware Rev.: %s  Loader Rev.: %s  Hardware Rev.: %s\n", 
		procVer, bi.loadVer, bi.rev_no);
	printf("  I/O Base: %#x  RAM Base: %#lx  Interrupt: %2d  RAM Size: %ldK\n\n", 
		bi.iobase, bi.rambase, bi.irq, bi.ram_size / 1024);

	printf("--[ ISDN Line Information ]-----------------------------------------------------\n");
 	switch(bi.modelid) {
	case BRI_BOARD:
	case DATAPIPER_BOARD:
	case POTS_BOARD:
	case DCPLUS_BOARD:
		theCommand.command = SCIOCGETPHYSTAT;
		theCommand.dataptr = (PhyStat_bri *) &briphy;
		if(do_ioctl(card, theCommand.command, &theCommand)) {
			printf("Error getting status information\n");
			return;
		}
		printf("  Switch Type:  %s  Layer 1: %s\n",  briswitches[switch_type], switch_states[briphy.l1_status]);

		theCommand.device = card;
		theCommand.dataptr = (char *) spid;
		theCommand.channel = 1;
		theCommand.span = 0;
		theCommand.command = SCIOCGETSPID;
		if(do_ioctl(card, theCommand.command, &theCommand)) {
			printf("Error getting status information\n");
			return;
		}

		theCommand.device = card;
		theCommand.dataptr = (char *) dn;
		theCommand.channel = 1;
		theCommand.span = 0;
		theCommand.command = SCIOCGETDN;
		if(do_ioctl(card, theCommand.command, &theCommand)) {
			printf("Error getting status information\n");
			return;
		}
		printf("  Channel 1:    SPID: %s  Number: %s  State: %s\n", spid, dn, states[briphy.b1_status]);

		theCommand.device = card;
		theCommand.command = SCIOCGETSPID;
		theCommand.dataptr = (char *) spid;
		theCommand.channel = 2;
		theCommand.span = 0;
		if(do_ioctl(card, theCommand.command, &theCommand)) {
			printf("Error getting status information\n");
			return;
		}
		theCommand.device = card;
		theCommand.command = SCIOCGETDN;
		theCommand.dataptr = (char *) dn;
		theCommand.channel = 2;
		theCommand.span = 0;
		if(do_ioctl(card, theCommand.command, &theCommand)) {
			printf("Error getting status information\n");
			return;
		}
		printf("  Channel 2:    SPID: %s  Number: %s  State: %s\n\n", spid, dn, states[briphy.b2_status]);
		break;

	case PRI_BOARD:
	case PRIE_BOARD:
	case PRIPLUS_BOARD:
	{
		int span;

		printf("  Switch Type:  %s\n", priswitches[switch_type]);
		theCommand.device = card;
		theCommand.channel = 0;
		theCommand.span = 0;
		theCommand.command = SCIOCGETPARAMS;
		theCommand.dataptr = &params;
		if(do_ioctl(card, theCommand.command, &theCommand)) {
			printf("Error getting status information\n");
			return;
		}
		if(bi.modelid == PRIE_BOARD) {
			printf("  Data Encoding: %s  CRC4 Enabled: %s",
				"HDB3",
				params.crc6_enable ? "Yes" : "No");
		} else {
			printf("  Signalling: %s  Framing: %s  CRC6 Enabled: %s",
				params_signal[params.signal_code], 
				params_frame[params.frame_mode], 
				params.crc6_enable ? "Yes" : "No");
		}
		if (PRIPLUS_BOARD == bi.modelid)
			printf("  CSU Enabled: %s", params.csu_enable ? "Yes" : "No");

		printf("\n  Line Length: %s  Bypass Jitter Attenuation: ", 
			(bi.modelid == PRIE_BOARD && params.line_length == LLEN_NA) ? "CEPT" : params_line_len[params.line_length]);

		if (PRIPLUS_BOARD == bi.modelid) {
			char *jitter[] = {"No", "TX", "RX"};
			printf("%s\n", params.bypass_jitter < 3 ? jitter[params.bypass_jitter] : "Unknown");
		} else
			printf("%s\n", params.bypass_jitter ? "Yes" : "No");

		for (span=0; span<bi.num_spans; span++) {
			if (PRIPLUS_BOARD == bi.modelid)
				printf("\nSpan %d:\n", span);

			theCommand.channel = 0;
			theCommand.span = span;

			theCommand.command = SCIOCGETPHYSTAT;
			theCommand.dataptr = (PhyStat_pri *) &priphy;
			if(do_ioctl(card, theCommand.command, &theCommand)) {
				printf("Error getting status information\n");
				return;
			}

			printf("  Layer 1: %s  Layer 2: %s\n",
				switch_states[priphy.l1_status], l2_states[priphy.l2_status]);

			theCommand.command = SCIOCGETACFASTAT;
			theCommand.dataptr = (acfaStat *) &acfa;
			if(do_ioctl(card, theCommand.command, &theCommand)) {
				printf("Error getting status information\n");
				return;
			}
			printf("  Alarms:  %s%s%s%s%s%s%s\n\n",
				acfa.acfa_nos ? "[NOS]" : "[   ]",
				acfa.acfa_ais ? "[AIS]" : "[   ]",
				acfa.acfa_los ? "[LOS]" : "[   ]",
				acfa.acfa_rra ? "[RRA]" : "[   ]",
				acfa.acfa_slpp ? "[SLPP]" : "[    ]",
				acfa.acfa_slpn ? "[SLPN]" : "[    ]",
				acfa.acfa_fsrf ? "[FSRF]" : "[    ]");

			theCommand.command = SCIOCGETPHYSTAT;
			theCommand.dataptr = (PhyStat_pri *) &priphy;
			if(do_ioctl(card, theCommand.command, &theCommand)) {
				printf("Error getting status information\n");
				return;
			}
			if((bi.modelid == PRI_BOARD) || (bi.modelid == PRIPLUS_BOARD)) { 
			printf("  --[ Channel States ]------------------------------------------------\n");
			printf("   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22\n");
			printf("  --------------------------------------------------------------------\n  ");
			for(i = 0 ; i < 23 ; i++ )
				printf("%s ", pri_states[priphy.b_status[i]]);
				putchar('\n');
			putchar('\n');
			}
			else {
			printf("  --[ Channel States ]------------------------\n");
			printf("   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 \n");
			printf("  --------------------------------------------\n  ");
			for(i = 0 ; i < 15 ; i++ ) 
				printf("%s ", pri_states[priphy.b_status[i]]);
				putchar('\n');
			putchar('\n');
			printf("  --------------------------------------------\n");
			printf("  15 16 17 18 19 20 21 22 23 24 25 26 27 28 29\n");
			printf("  --------------------------------------------\n  ");
			for(i = 15 ; i < 30 ; i++ ) 
				printf("%s ", pri_states[priphy.b_status[i]]);
				putchar('\n');
			putchar('\n');
			}
		}
		break;
        } /* case */
	} /* switch */
}

