/*
 * shmem.c - Shared Memory handling
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: shmem.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
#ifndef SHMEM_H
#define SHMEM_H

#include <linux/string.h>
#include <asm/io.h>

#include "includes.h"		/* This must be first */
#include "hardware.h"
#include "card.h"

/*
 *
 */
extern inline void memcpy_toshmem(board *card, u32 dest, const void *src, size_t n)
{
	unsigned long adj_dest = dest;

	if (card->is_banked) {
		unsigned char ch = dest / SRAM_PAGESIZE;
		adj_dest %= 0x4000;
		outb(card->shmem_magic + ch, card->shmem_pgport);
	}
	memcpy_toio(card->krambase + adj_dest, src, n);

	/* turn off the shared memory */
	if (card->is_banked)
		outb(0x00, card->shmem_pgport);
}

/*
 * 
 */
extern inline void memcpy_fromshmem(board *card, void *dest, u32 src, size_t n)
{
	unsigned long adj_src = src;

	if (card->is_banked) {
		unsigned char ch = src / SRAM_PAGESIZE;
		adj_src %= 0x4000;
		outb(card->shmem_magic + ch, card->shmem_pgport);
	}
	memcpy_fromio(dest,(void *)(card->krambase + adj_src), n);

	/* turn off the shared memory */
	if (card->is_banked)
		outb(0x00, card->shmem_pgport);
}

#endif
