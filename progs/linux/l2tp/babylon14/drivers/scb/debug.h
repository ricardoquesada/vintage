/*
 * debug.h - Self explanitory
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: debug.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef DEBUG_H
#define DEBUG_H
#define pr_error(fmt,arg...)	printk(KERN_ERR fmt,##arg)
#define pr_notice(fmt,arg...)	printk(KERN_NOTICE fmt,##arg)
#define pr_warning(fmt,arg...)	printk(KERN_WARNING fmt,##arg)
#define pr_crit(fmt,arg...)	printk(KERN_CRIT fmt,##arg)
#endif
