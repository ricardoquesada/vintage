/*
 * card.h - 
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: card.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef CARD_H
#define CARD_H

/*
 * We need these if they're not already included
 */
#include <linux/timer.h>
#include <linux/config.h>
#include <linux/skbuff.h>
#include <linux/wait.h>

#include "vercomp.h"

#include "aps_if.h"
#include "message.h"
#include "hardware.h"
#include "plx.h"

/*
 * Name of our Babylon device class
 */
#define DEVCLASS_NAME		"scb"

/*
 * Amount of time to wait for a reset to complete
 */
#define CHECKRESET_TIME		milliseconds(10000)

/*
 * The maximum amount of time to wait for a message response
 * to arrive. Use exclusively by send_and_receive
 */
#define SAR_TIMEOUT		milliseconds(1000)
#define FLASH_TIMEOUT		milliseconds(10000)

struct board_tag;
struct plx_dma_ent;

/*
 * A message waiting to be sent due to FIFO congestion - EP 3/3/98
 */
struct fifo_message {
	struct fifo_message	*next_msg;		/* next queued message */
	struct fifo_message	*prev_msg;		/* previous queued message */
	ReqMessage		msg;			/* the message to send */
	unsigned long		expires;		/* time of expiry (in jiffies) */
};

#define SCB_MAX_TX_BUFS		2

/*
 * Per channel status and configuration
 */
typedef struct scb_chan {
	int			use_count;

	u8			span, link, stopped,
				framing;		/* current framing format: xxx_PROTO in hardware.h */

	struct board_tag	*card;			/* Back pointer to card */
	u32			first_sendbuf;		/* Offset of first send buffer */
	unsigned		num_sendbufs;		/* Number of send buffers */
	unsigned		free_sendbufs;		/* Number of free sendbufs */
	unsigned		next_sendbuf;		/* Next sequential buffer */
	unsigned		done_sendbuf;
	struct sk_buff		*tx_skbs[SCB_MAX_TX_BUFS];

	u32			first_rxbuf;
	unsigned		num_rxbufs;

	channel_t		bch;			/* Babylon's channel control structure */

	struct timer_list	dial_timer;
} bchan;

/*
 * Everything you want to know about the adapter ...
 */
typedef struct board_tag {
	int			model;				/* Adapter model number */
	char			devicename[20];			/* The device name */
	u8			nChannels[MAX_SPANS];		/* Number of channels */
	u8			num_spans;
	u8			tot_chans;

	u8			min_channel[MAX_SPANS];
	u8			mode[MAX_SPANS];

	unsigned int		interrupt;			/* Interrupt number */

	int			iobase;				/* I/O Base address */
	int			ioport[MAX_IO_REGS];		/* Index to I/O ports */
	int			shmem_pgport;			/* port for the exp mem page reg. */
	int			shmem_magic;			/* adapter magic number */

	unsigned int		rambase;			/* Shared RAM base address */
	unsigned int		ramsize;			/* Size of shared memory */
	unsigned int		kramsize;			/* size of mapped shared memory window */

	wait_queue_head_t	async_queue;			/* Queue for async activity */
	bchan			*channel[MAX_SPANS][MAX_CARD_CHANNELS];		/* status of the B channels */
	RspMessage		*async_msgs[MAX_SPANS][MAX_CARD_CHANNELS];	/* Async response messages per channel */

	HWConfig		hwc;				/* Hardware config data */
	u8			seq_no;				/* Next send seq. number */
	u8			sendLock;
	char			loadVer[7];			/* CommManage Version string */
	u8			StartOnReset;			/* Indicates startproc after reset */
	u8			EngineUp;			/* Indicates CommEngine Up */
	u8			resetting;			/* in reset */

	u8			*kiobase;			/*  */
	u8			*krambase;			/* Kernel space point to shared RAM */
	u8			*plx_base;			/*  */
	int			is_banked;			/* memory is linear at krambase */
	int			io_in_mem;			/* ioport[] points into krambase */

	struct timer_list	irq_timer;

	/* FIFO overrun inhibitor EP 3/3/98 */
	struct fifo_message	*fifo_queue_head;		/* messages queue head */
	struct fifo_message	*fifo_queue_tail;		/* messages queue tail*/
	struct timer_list	fifo_write_timer;

#ifdef CONFIG_PCI
	struct plx_dma_ent	*plx_dma_list;			/* packets that are currently being DMA'd on channel 0 */
	struct plx_dma_ent	*plx_build_head;		/* head of list of requests that are queued for DMA */
	struct plx_dma_ent	*plx_build_tail;		/* tail of list of requests that are queued for DMA */

	struct plx_dma_ent	*plx_free_ent;
	unsigned long		plx_ent_page;
#endif

#ifdef DEBUG_LOG
	struct timer_list	log_timer;
	unsigned int		log_ptr;
	int			log_active;
	wait_queue_head_t	log_wait;
#endif

	u32			buffer_base;
	u32			req_queue;
	u32			rsp_queue;
	u8			req_queue_len;
	u8			rsp_queue_len;
	u8			newFifoScheme;
	u8			irq_disabled;
} board;


extern inline void scb_card_disable_irq(board *card)
{
#ifdef CONFIG_PCI
	if (card->plx_base)
		writel(readl(card->plx_base + PLX_INTCSR) & ~(1UL << 11),
			card->plx_base + PLX_INTCSR);
#endif
	card->irq_disabled = 1;
}

extern inline void scb_card_enable_irq(board *card)
{
	card->irq_disabled = 0;
#ifdef CONFIG_PCI
	if (card->plx_base)
		writel(readl(card->plx_base + PLX_INTCSR) | (1UL << 11),
			card->plx_base + PLX_INTCSR);
#endif
}

#endif /* CARD_H */
