/*
 * message.h - Message API declarations
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: message.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

/*
 * Board message macros, defines and structures
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include <asm/byteorder.h>

#define MAX_MESSAGES		32	/* Maximum messages that can be queued */
#define MSG_DATA_LEN		48	/* Maximum size of message payload */
#define MSG_LEN			64	/* Size of a message */
#define CMPID			0	/* Loader message process ID */
#define CEPID			64	/* Firmware message process ID */

/*
 * Macro to determine if a message is a loader message
 */
#define IS_CM_MESSAGE(mesg, tx, cx, dx)		\
		((mesg.type == cmRspType##tx)		\
		&&(mesg.class == cmRspClass##cx)	\
		&&(mesg.code == cmRsp##dx))

#define cmMsg(a,b,c)	__constant_htonl(((u32)(a) << 24) | ((u32)(b) << 16) | ((u32)(c) << 8) | CMPID)
#define ceMsg(a,b,c)	__constant_htonl(((u32)(a) << 24) | ((u32)(b) << 16) | ((u32)(c) << 8) | CEPID)

/*
 * Macro to determine if a message is a firmware message
 */
#define IS_CE_MESSAGE(mesg, tx, cx, dx)		\
		((mesg.type == ceRspType##tx)		\
		&&(mesg.class == ceRspClass##cx)	\
		&&(mesg.code == ceRsp##tx##dx))

/* 
 * Loader Request and Response Messages
 */

/* message codes */
#define cmHWConfig		cmMsg(1,0,1)
#define cmSetX			cmMsg(250,0,1)
#define cmMsgLpbk		cmMsg(1,0,2)
#define cmVersion		cmMsg(1,0,3)
#define cmLoadProc		cmMsg(2,0,1)
#define cmStartProc		cmMsg(2,0,2)
#define cmReadMem		cmMsg(2,0,6)
#define cmInvalid		cmMsg(0,0,0)


/*
 * Firmware Request and Response Messages
 */

#define cePhyProcInfo		ceMsg(1,0,1)
#define cePhyConnect		ceMsg(1,1,1)
#define cePhyDisconnect		ceMsg(1,1,2)
#define cePhySetParams		ceMsg(1,1,3)
#define cePhyGetParams		ceMsg(1,1,4)
#define cePhyStatus		ceMsg(1,2,1)
#define cePhyAcfaStatus		ceMsg(1,2,2)
#define cePhyChCallState	ceMsg(1,2,3)
#define cePhyChServState	ceMsg(1,2,4)
#define cePhyRLoopBack		ceMsg(1,3,1)

#define ceLnkSetParam		ceMsg(2,0,1)
#define ceLnkGetParam		ceMsg(2,0,2)
#define ceLnkGetStats		ceMsg(2,0,3)
#define ceLnkWrite		ceMsg(2,1,1)
#define ceLnkRead		ceMsg(2,1,2)
#define ceLnkFlush		ceMsg(2,1,3)
#define ceLnkWrBufTrc		ceMsg(2,1,4)
#define ceLnkRdBufTrc		ceMsg(2,1,5)

#define ceCallSetSwitchType	ceMsg(3,0,1)
#define ceCallGetSwitchType	ceMsg(3,0,2)
#define ceCallSetFrameFormat	ceMsg(3,0,3)
#define ceCallGetFrameFormat	ceMsg(3,0,4)
#define ceCallSetCallType	ceMsg(3,0,5)
#define ceCallGetCallType	ceMsg(3,0,6)
#define ceCallSetSPID		ceMsg(3,0,7)
#define ceCallGetSPID		ceMsg(3,0,8)
#define ceCallSetMyNumber	ceMsg(3,0,9)
#define ceCallGetMyNumber	ceMsg(3,0,10)
#define ceReqSetCarrierSystem	ceMsg(3,0,13)
#define ceReqGetCarrierSystem	ceMsg(3,0,14)
#define ceCallSetCardMode	ceMsg(3,0,25)
#define ceCallGetCardMode	ceMsg(3,0,26)
#define ceCallSetFrameLen	ceMsg(3,0,27)	/* only for voice mode -- determines latency */
#define ceCallGetFrameLen	ceMsg(3,0,28)

#define ceMiscEngineUp		ceMsg(5,0,1)
#define ceMiscClearTrace	ceMsg(5,0,2)
#define ceMiscMarkTraceEnd	ceMsg(5,0,3)
#define ceMiscSetFactoryDefault	ceMsg(5,0,4)
#define ceMiscSetMsgQueue	ceMsg(5,0,10)

#define ceMvipResetSwitch	ceMsg(10,0,1)
#define ceMvipSetOutput		ceMsg(10,0,2)
#define ceMvipQueryOutput	ceMsg(10,0,3)
#define ceMvipQuerySwitchCaps	ceMsg(10,0,4)
#define ceRspErrError		ceMsg(0,0,0)

/*
 * Call Types
 */
#define CALLTYPE_64K		0
#define CALLTYPE_56K		1
#define CALLTYPE_SPEECH		2
#define CALLTYPE_31KHZ		3

/*
 * Link Level data contains a pointer to and the length of
 * a buffer in shared RAM. Used by LnkRead and LnkWrite message
 * types. Part of RspMsgStruct and ReqMsgStruct.
 */
typedef struct {
	u32	buff_offset	__attribute__((packed));
	u16	msg_len		__attribute__((packed));
} LLData;

/* 
 * 
 */
typedef struct {
	u8	acfa_nos	__attribute__((packed));
	u8	acfa_ais	__attribute__((packed));
	u8	acfa_los	__attribute__((packed));
	u8	acfa_rra	__attribute__((packed));
	u8	acfa_slpp	__attribute__((packed));
	u8	acfa_slpn	__attribute__((packed));
	u8	acfa_fsrf	__attribute__((packed));
} acfaStat;

typedef struct {
	u8	st_u_sense	__attribute__((packed));
	u8	powr_sense	__attribute__((packed));
	u8	sply_sense	__attribute__((packed));
	u8	asic_id		__attribute__((packed));
	u32	ram_size	__attribute__((packed));
	u8	serial_no[13]	__attribute__((packed));
	u8	part_no[10]	__attribute__((packed));
	u8	rev_no[2]	__attribute__((packed));
} HWConfig_bri;

typedef struct {
	u32	ram_size	__attribute__((packed));
	u8	serial_no[13]	__attribute__((packed));
	u8	part_no[10]	__attribute__((packed));
	u8	rev_no[2]	__attribute__((packed));
} HWConfig_pri;

typedef HWConfig_pri HWConfig_pots;

typedef union {
	HWConfig_bri	brihw	__attribute__((packed));
	HWConfig_pri	prihw	__attribute__((packed));
	HWConfig_pots	potshw	__attribute__((packed));
} HWConfig;

typedef struct {
	u8	b1_status	__attribute__((packed));
	u8	b2_status	__attribute__((packed));
	u8	l1_status	__attribute__((packed));
} PhyStat_bri;

typedef struct {
	u8	l1_status	__attribute__((packed));
	u8	l2_status	__attribute__((packed));
	u8	b_status[30]	__attribute__((packed));
} PhyStat_pri;

typedef struct {
	u8	b_status[30]	__attribute__((packed));
} PriStat;

typedef struct {
	struct {
		u32	tx_good	__attribute__((packed));
		u32	tx_bad	__attribute__((packed));
		u32	rx_good	__attribute__((packed));
		u32	rx_bad	__attribute__((packed));
	} channels[3]	__attribute__((packed));
} BRILnkStats;

typedef struct {
	u32	tx_good	__attribute__((packed));
	u32	tx_bad	__attribute__((packed));
	u32	rx_good	__attribute__((packed));
	u32	rx_bad	__attribute__((packed));
	u32	tx_dropped	__attribute__((packed));
	u32	rx_dropped	__attribute__((packed));
} PRILnkStats;

typedef struct {
	u32	req_queue;
	u32	rsp_queue;
	u8	req_queue_len;
	u8	rsp_queue_len;
} PRIMsgQueue;

/*
 * A Message
 */
struct message {
	u8	sequence_no		__attribute__((packed));
	u8	process_id		__attribute__((packed));
	u8	time_stamp		__attribute__((packed));
	u8	cmd_seq_no		__attribute__((packed));	/* Rsp messages only */

	u8	reserved1[3]		__attribute__((packed));
	u8	msg_byte_cnt		__attribute__((packed));

	union {
		struct {
			u8	type		__attribute__((packed));
			u8	class		__attribute__((packed));
			u8	code		__attribute__((packed));
			u8	link		__attribute__((packed));
		}	b	__attribute__((packed));
		u32	l	__attribute__((packed));
	} msgreq;

	u8	rsp_status		__attribute__((packed));	/* Rsp messages only */
	u8	span_no			__attribute__((packed));	/* PRI-2 only */
	u8	call_bearer_type	__attribute__((packed));
	u8	call_channel_type	__attribute__((packed));	/* PRI-2 only */
	union {
		u16		word_array[MSG_DATA_LEN/2]	__attribute__((packed));
		u8		byte_array[MSG_DATA_LEN]	__attribute__((packed));
		LLData		response	__attribute__((packed));
		BRILnkStats	bri_lnkstat	__attribute__((packed));
		PRILnkStats	pri_lnkstat	__attribute__((packed));
		PRIMsgQueue	queue	__attribute__((packed));
	}	msg_data	__attribute__((packed));
};

#define phy_link_no	msgreq.b.link

#define ceMsgType(mesg)		(((mesg)->msgreq.l & htonl(0xffffff00)) | htonl((mesg)->process_id))

#define LINK_MASK	0x7f
#define LINK_SPAN	0x80

typedef struct message ReqMessage;	/* Request message */
typedef struct message RspMessage;	/* Response message */

/*
 * The first 5010 bytes of shared memory contain the message queues,
 * indexes and other data. This structure is its template
 */
typedef struct {
	volatile ReqMessage	req_queue[MAX_MESSAGES]	__attribute__((packed));
	volatile RspMessage	rsp_queue[MAX_MESSAGES]	__attribute__((packed));
	volatile u8		req_head	__attribute__((packed));
	volatile u8		req_tail	__attribute__((packed));
	volatile u8		rsp_head	__attribute__((packed));
	volatile u8		rsp_tail	__attribute__((packed));
	volatile u32		signature	__attribute__((packed));
	volatile u32		trace_enable	__attribute__((packed));
	volatile u8		reserved[4]	__attribute__((packed));
} DualPortMemory;

#endif
