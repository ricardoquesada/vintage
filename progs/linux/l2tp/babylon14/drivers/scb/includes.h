/*
 * includes.h - Self explanitory
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: includes.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#include <linux/version.h>
#include <linux/errno.h>
#include <asm/segment.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/malloc.h>
#include <linux/mm.h>
#include <linux/ioport.h>
#include <linux/timer.h>
#include <linux/wait.h>
#include <linux/types.h>
#include <linux/smp_lock.h>

#include "aps_if.h"
#include "debug.h"
#include "hardware.h"
#include "plx.h"
#include "card.h"
#include "message.h"
#include "scioc.h"
#include "vercomp.h"

struct sk_buff;

#define dpm_activate(x)		do { if (x->is_banked) outb(x->shmem_magic, x->shmem_pgport); } while (0)
#define dpm_deactivate(x)	do { if (x->is_banked) outb(0x00, x->shmem_pgport); } while (0)

#define setup_ports(x)	{ if (x->is_banked)	\
				scb_outb(x, (x->rambase >> 12), EXP_BASE); \
			  scb_outb(x, (x->interrupt | 0x80), IRQ_SELECT); }

