SpellCaster ISDN device class for Babylon                                README
-------------------------------------------------------------------------------
Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
$Id: README,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
Released under the GNU Public License. See LICENSE file for details.

TABLE OF CONTENTS
-----------------

1.0 About Babylon
	1.1 About Babylon Device Classes
	1.2 Adapters supported by this Device Class
	
2.0 Using the SpellCaster ISDN Device Class
	2.1 Making and installing
	2.2 Loading and unloading
	2.3 Troubleshooting
	2.4 Manual configuration

1.0 About Babylon
-----------------
Babylon is device independant multilink PPP software for Linux. It supports
both dial-in and dial-out operations and includes support for popular PPP
features such as PAP, CHAP and IPCP. Babylon is available from:

	SpellCaster Telecommunications Inc.
	73 Laird Drive
	Suite 206
	Toronto, Ontario, Canada,
	M4G 3T4
	(416) 425-0600

1.1 About Babylon Device Classes
--------------------------------
Babylon achieves device independance by defining "device classes". Device
classes are implemented by writing a device class driver specific to the
device you want to support. This module is an example of such a device
class driver which implements support for SpellCaster's ISDN adapters. Each
device class has a name. The name of the device class of this driver is "scb".
Each adapter is numbered from 0..n. The first adapter is labeled scb0, the
second scb1 and so on. Each channel of each adapter is also sub-addressed.
The first channel of the first adapter is labeled scb0.0, the second channel
is labeled scb0.1 and so on.

As of version 1.20 of this driver, channels can also be accessed via the
character special devices /dev/scbXXX where XXX is the minor number of the
channel.  Minor numbers are allocated as follows:

	BRI Cards:
		2 minors for each B channel
		1 minor for the D channel,

	DataCommute/PRI:
		23 minors for B channels
		1 minor for the D channel,

	DataCommute/PRI Plus:
		1 minor for the D channel of span 0
		23 minors for the span 0 B channels
		1 minor for the D channel of span 1
		23 minors for the span 1 B channels

So for a system with a BRI and a PRI installed, you'd get the following
mapping from character special devices to Babylon channels:

	/dev/scb0	scb0.0
	/dev/scb1	scb0.1
	/dev/scb2	D channel of scb0
	/dev/scb3	scb1.0
	/dev/scb4	scb1.1
		...
	/dev/scb25	scb1.22
	/dev/scb26	D channel of scb1

Note that D channels do not provide any data by default.  Instead you must
use scbctrl -D<mode> (only supported on the DataCommute/PRI and /PRI Plus)
to either pass the raw HDLC frames from the D channel to the corresponding
device (which disables the signalling stack on the card, precluding its use
with Babylon for the time being).


1.2 Adapters supported by this Device Class
-------------------------------------------
This device class driver supports the following SpellCaster ISDN adapters:

	DataCommute/BRI 16bit ISA, firmware rev. 1.51
	TeleCommute/BRI 16bit ISA, firmware rev. 1.32
	DataCommute/PRI 16bit ISA, firmware rev. 2.91
	DataCommute/PRI Plus  PCI, firmware rev. 2.88

2.0 Using the SpellCaster ISDN Device Class
-------------------------------------------

2.1 Making and installing
-------------------------
While configuring Babylon, answer Y to "Do you want SpellCaster DataCommute/BRI /PRI
or TeleCommute ISDN adapter support?".

2.2 Loading and unloading
-------------------------
To load the device class, type "insmod scb", to remove it type "rmmod scb"

2.3 Troubleshooting
-------------------
	- for ISA devices, ensure an IRQ is mapped to ISA Legacy Device in your BIOS.
	- the rambase option may be required with certain ethernet drivers (ie SMC Ultra16)

