/*
 * hardware.h - Hardware specifics
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: hardware.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef HARDWARE_H
#define HARDWARE_H

#ifdef __KERNEL__

#include <asm/param.h>			/* For HZ */
#include <asm/io.h>

/* Return the number of jiffies in a given number of msecs */
#define milliseconds(x)	(((x)*HZ)/1000L)

#endif

/*
 * General hardware parameters common to all ISA adapters
 */

#define MAX_CARDS	8		/* The maximum number of cards to
					   control or probe for. If you change
					   this, you must also change the number
					   of elements in io, irq, and ram to
					   match. Initialized in init.c */

#define REQ_QUEUE_OFFSET	0x0000
#define RSP_QUEUE_OFFSET	0x0800

#define REQ_HEAD_OFFSET		0x1000
#define REQ_TAIL_OFFSET		0x1001
#define RSP_HEAD_OFFSET		0x1002
#define RSP_TAIL_OFFSET		0x1003

#define SIG_OFFSET		0x1004		/* Where to find signature in shared RAM */
#define SIGNATURE		0x87654321	/* Board reset signature */

#define TRACE_OFFSET		0x1008		/* Trace enable word offset in shared RAM */
#define BUFFER_OFFSET		0x2000		/* Beginning of buffers */

#define TRBUF_SIZE		0x10000

/* I/O Port parameters */
#define IOBASE_MIN	0x180		/* Lowest I/O port address */
#define IOBASE_MAX	0x3C0		/* Highest I/O port address */
#define IOBASE_OFFSET	0x20		/* Inter-board I/O port gap used during
					   probing */
#define FIFORD_OFFSET	0x0
#define FIFOWR_OFFSET	0x400
#define FIFOSTAT_OFFSET	0x1000
#define RESET_OFFSET	0x2800

#define PG0_OFFSET	0x3000		/* Offset from I/O Base for Page 0 register */
#define PG1_OFFSET	0x3400		/* Offset from I/O Base for Page 1 register */
#define PG2_OFFSET	0x3800		/* Offset from I/O Base for Page 2 register */
#define PG3_OFFSET	0x3C00		/* Offset from I/O Base for Page 3 register */

#define FIFO_READ	0		/* FIFO Read register */
#define FIFO_WRITE	1		/* FIFO Write rgister */
/* #define LO_ADDR_PTR	2	 */	/* Extended RAM Low Addr Pointer */
/* #define HI_ADDR_PTR	3	 */	/* Extended RAM High Addr Pointer */
/* #define NOT_USED_1	4 */
#define FIFO_STATUS	5		/* FIFO Status Register */
/* #define NOT_USED_2	6 */
/* #define MEM_OFFSET	7 */
#define SFT_RESET	10		/* Reset Register */
#define EXP_BASE	11		/* Shared RAM Base address */
#define EXP_PAGE0	12		/* Shared RAM Page0 register */
#define EXP_PAGE1	13		/* Shared RAM Page1 register */
#define EXP_PAGE2	14		/* Shared RAM Page2 register */
#define EXP_PAGE3	15		/* Shared RAM Page3 register */
#define IRQ_SELECT	16		/* IRQ selection register */
#define MAX_IO_REGS	17		/* Total number of I/O ports */

/* FIFO register values */
#define RF_HAS_DATA	0x01		/* fifo has data */
#define RF_QUART_FULL	0x02		/* fifo quarter full */
#define RF_HALF_FULL	0x04		/* fifo half full */
#define RF_NOT_FULL	0x08		/* fifo not full */
#define WF_HAS_DATA	0x10		/* fifo has data */
#define WF_QUART_FULL	0x20		/* fifo quarter full */
#define WF_HALF_FULL	0x40		/* fifo half full */
#define WF_NOT_FULL	0x80		/* fifo not full */

/* Shared RAM parameters */
#define SRAM_MIN	0xD0000         /* Lowest host shared RAM address */
#define SRAM_MAX	0xEFFFF         /* Highest host shared RAM address */

#define SRAM_PAGESIZE	0x4000		/* Size of one RAM page (16K) */

/* Shared RAM buffer parameters */
#define BUFFER_ALIGN	0x800
#define BUFFER_SIZE	0x7FC		/* The size of a buffer in bytes */
#define RX_BUFFERS_MAX	8		/* rx buffers per channel -- must be > 8 for 0 copy and <= 16 (firmware max) */
#define RAW_PROTO	0x00
#define HDLC_PROTO	0x01		/* Frame Format for Layer 2 */

#define BRI_BOARD	0
#define POTS_BOARD	1
#define PRI_BOARD	2
#define HSET_BOARD	3
#define DATAPIPER_BOARD	4
#define PRIPLUS_BOARD	5
#define DCPLUS_BOARD	6
#define PRIE_BOARD	7

#define MAX_CARD_CHANNELS	64	/* don't change without fiddling below too */
#define MAX_SPANS		4

/*
 * Specific hardware parameters for the DataCommute/BRI, TeleCommute/BRI 
 * and DataCommute/Plus
 */
#define BRI_CHANNELS	2		/* Number of B channels */
#define BRI_BASEPG_VAL	0x98
#define BRI_MAGIC	0x60000		/* Magic Number */
#define BRI_MEMSIZE	0x10000		/* Amount of RAM (64K) */
#define BRI_FEATURES	ISDN_FEATURE_L2_HDLC | ISDN_FEATURE_L3_TRANS;

/*
 * Specific hardware parameters for the DataCommute/PRI
 */
#define PRI_CHANNELS	23		/* Number of B channels */
#define PRI_BASEPG_VAL	0x88
#define PRI_MAGIC	0x20000		/* Magic Number */
#define PRI_MEMSIZE	0x100000	/* Amount of RAM (1M) */
#define PRI_FEATURES	ISDN_FEATURE_L2_HDLC | ISDN_FEATURE_L3_TRANS;
#define T1		0		/* T1 Mode */

/*
 * Specific hardware parameters for the DataCommute/PRIe
 */
#define PRIE_CHANNELS	30		/* Number of B channels */
#define CEPT		1		/* CEPT Mode */
/*
 * Some handy macros
 */

/* Determine if a channel number is valid for the adapter */
#define IS_VALID_CHANNEL(y,x)	((x>0) && (x <= adapter[y]->channels))

#ifdef __KERNEL__
#include "card.h"

/* Handy macros that take care of IO */
extern inline void scb_outb(board *card, unsigned char b, int idx)
{
#ifdef CONFIG_PCI
	if (card->io_in_mem)
		writeb(b, card->kiobase + card->ioport[idx]);
	else
#endif
	outb(b, card->ioport[idx]);
}

static inline unsigned int scb_inb (board *card, int idx)
{
#ifdef CONFIG_PCI
	if (card->io_in_mem)
		return readb(card->kiobase + card->ioport[idx]);
#endif
	return inb(card->ioport[idx]);
}
#endif

#endif
