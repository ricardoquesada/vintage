/* plx.h -- stuff for use with the PLX PCI 9080
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: plx.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef PLX_H
#define PLX_H

#define PLX_PCICR		0x04
#define PLX_PCISR		0x06
#define PLX_INTCSR		0x68
#define PLX_CNTRL		0x6c
#define PLX_DMAMODE0		0x80
#define PLX_DMAPADR0		0x84
#define PLX_DMALADR0		0x88
#define PLX_DMASIZ0		0x8c
#define PLX_DMADPR0		0x90
#define PLX_DMACSR0		0xa8
#define PLX_DMAARB		0xac
#define PLX_DMATHR		0xb0
#define PLX_LAS0RR		0x00
#define PLX_LAS0BA		0x04
#define PLX_MARBR		0x08
#define PLX_LBRD0		0x18
#define PLX_LAS1RR		0xf0
#define PLX_LAS1BA		0xf4
#define PLX_LBRD1		0xf8

/* DMA descriptor defines */
#define PLX_DESC_PCI		0x1	/* this bit is set if the descriptor is in PCI address space */
#define PLX_DESC_END		0x2	/* set if this is the last descriptor in a chain */
#define PLX_DESC_IRQ		0x4	/* set to generate an interrupt after terminal count is reached */
#define PLX_DESC_LOCAL_PCI	0x8	/* set if transfer is from local memory to PCI memory */

struct plx_dma_ent {
	u32	pci_addr;
	u32	local_addr;
	u32	size;
	u32	next;			/* pointer to next dma_ent in pci space, bit 0= */

	void	(*cbf)(void *cbdata);	/* callback function */
	void	*cbdata;		/* used as next pointer internally while on free list */
};

#endif /* PLX_H */

