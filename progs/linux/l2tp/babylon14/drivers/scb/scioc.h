/*
 * scioc.h - Adapter ioctl interface
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: scioc.h,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef SCIOC_H
#define SCIOC_H

#ifdef __KERNEL__
#include <linux/ioctl.h>
#else
#include <sys/ioctl.h>
#endif

/*
 * IOCTL Command Codes
 */
#define SCIOCLOAD		_IO('I', 0x01)	/* Load a firmware record */
#define SCIOCRESET		_IO('I', 0x02)	/* Perform hard reset */
#define SCIOCREV		_IO('I', 0x04)	/* Get driver revision */
#define SCIOCSTART		_IO('I', 0x05)	/* Start the firmware */
#define SCIOCGETSWITCH		_IO('I', 0x06)	/* Get switch type */
#define SCIOCSETSWITCH		_IO('I', 0x07)	/* Set switch type */
#define SCIOCGETSPID		_IO('I', 0x08)	/* Get channel SPID */
#define SCIOCSETSPID		_IO('I', 0x09) 	/* Set channel SPID */
#define SCIOCGETDN		_IO('I', 0x0A)	/* Get channel DN */
#define SCIOCSETDN		_IO('I', 0x0B) 	/* Set channel DN */
#define SCIOCTRACE		_IO('I', 0x0C)	/* Get Trace Record */
#define SCIOCGETSPEED		_IO('I', 0x0E)	/* Get channel speed */
#define SCIOCSETSPEED		_IO('I', 0x0F)	/* Set channel speed */
#define SCIOCLOOPTST		_IO('I', 0x10)	/* Perform loopback test */
#define SCIOCSETPARAMS		_IO('I', 0x11)	/* Set PRI Parameters */
#define SCIOCGETPARAMS		_IO('I', 0x12)	/* Get PRI Parameters */
#define SCIOCSETSERIAL		_IO('I', 0x13)	/* Set DcPlus serial number */
#define SCIOCTRCTOG		_IO('I', 0x14)	/* Toggle Trace Mode */
#define SCIOCGETCARDINFO	_IO('I', 0x15)	/* Get card resources */
#define SCIOCGETHWCONFIG	_IO('I', 0x16)	/* Get hardware information */
#define SCIOCGETLOADVER		_IO('I', 0x17)	/* Get loader version string */
#define SCIOCGETPROCVER		_IO('I', 0x18)	/* Get firmware version string */
#define SCIOCGETPHYSTAT		_IO('I', 0x19)	/* Get channel status */
#define SCIOCGETSRVSTAT		_IO('I', 0x1A)	/* Get PRI service states */
#define SCIOCGETCALLSTAT	_IO('I', 0x1B)	/* Get PRI call states */
#define SCIOCGETACFASTAT	_IO('I', 0x1C)	/* Get PRI ACFA status */
#define SCIOCGETLINKSTAT	_IO('I', 0x1D)	/* Get phy. layer status */

#define SCIOCLISTENCALL		_IO('I', 0x1F)	/* Set call type to listen */
#define SCIOCSETMVIP		_IO('I', 0x20)	/* set current call's MVIP slot */
#define SCIOCRELMVIP		_IO('I', 0x21)	/* release current call's MVIP slot */
#define SCIOCNUMDEVICE		_IO('I', 0x22)	/* Get the number devices */
#define SCIOCGETENGSTAT		_IO('I', 0x23)	/* Get the engine status */

#define SCIOCGETLNKSTAT		_IO('I', 0x24)	/* Get the engine status */
#define SCIOCGETCHNAME		_IO('I', 0x25)	/* get the Babylon name of the channel */
#define SCIOCDIAL		_IO('I', 0x26)	/* attach and dial the fd */

#define SCIOCSETFACTDFLT	_IO('I', 0x27)	/* force card to revert to factory default settings */
#define SCIOCGETCARDMODE	_IO('I', 0x28)	/* gets card mode */
#define SCIOCSETCARDMODE	_IO('I', 0x29)	/* change card mode... used to disable isdn stack */

#define SCIOCGETCARRIER		_IO('I', 0x2D)	/* gets carrier system (PRIs only) */
#define SCIOCSETCARRIER		_IO('I', 0x2E)	/* sets carrier system (PRIs only) */


/* 
 * Pass a pointer to one of these when calling ioctl
 */
typedef struct {
	unsigned long command;	/* One of the defines above */
	void *dataptr;		/* Pointer to buffer that will receive result */

	int device;		/* Device ID beginning at 0 */
	int channel;		/* 0 based channel number -- includes D channel@0 */
	int span;		/* 0 based span number */
} scs_ioctl;

/* Size of strings */
#define SCIOC_SPIDSIZE		49
#define SCIOC_DNSIZE		SCIOC_SPIDSIZE
#define SCIOC_REVSIZE		SCIOC_SPIDSIZE
#define SCIOC_SRECSIZE		49
#define SCIOC_SERIALSIZE	10
#define SCIOC_CARRIERSIZE	2	/* carrier system, active channels */

/*
 * PRI Parameters
 */
typedef struct {
	unsigned char signal_code;
	unsigned char frame_mode;
	unsigned char crc6_enable;
	unsigned char line_length;
	unsigned char bypass_jitter;
	unsigned char csu_enable;	/* only on PRIPlus */
} PRIparameters;

#define SIGCODE_AMI	0x0
#define SIGCODE_B8ZS	0x1
#define FRAMODE_F12	0x0
#define FRAMODE_F4	0x1
#define FRAMODE_F72	0x3
#define FRAMODE_ESF	0x2
#define CRC6_DISABLED	0x0
#define CRC6_ENABLED	0xFF
#define LLEN_NA		0x0	/* North American PRI */
#define LLEN_50		0x1	/* T1, 0-50m */
#define LLEN_80		0x2	/* T1, 20-80m */
#define LLEN_130	0x3	/* T1, 60-130m */
#define LLEN_200	0x4	/* T1, 110-200m */
#define LLEN_230	0x5	/* T1, 140-230m */
#define LLEN_290	0x6	/* T1, 210-290m */
#define LLEN_320	0x7	/* T1, 270-320m */
#define BPJIT_DISABLED	0x0	/* Don't bypass jitter attenuation */
#define BPJIT_ENABLED	0xFF	/* Bypass jitter attenuation */

/*
 * Card resource information
 */
typedef struct {
	unsigned long	bi_magic;
	unsigned	bi_length;	/* length of this structure */

	int		num_channels;
	int		num_spans;

	int		modelid;
	unsigned long	trace;
	int		iobase;
	long		rambase;
	char		irq;
	unsigned long	ram_size;
	char		serial_no[14];
	char		part_no[11];
	char		rev_no[3];
	char		loadVer[5];
} boardInfo;

typedef struct {
	unsigned long	tx_good;
	unsigned long	tx_bad;
	unsigned long	rx_good;
	unsigned long	rx_bad;
	unsigned long	tx_dropped;
	unsigned long	rx_dropped;
} LnkStats;

#endif
