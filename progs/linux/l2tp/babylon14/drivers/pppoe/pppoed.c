/* pppoebr.c
 *
 * A program that will accept PPPoE packets on one network interface
 * and forward them to another interface/address.  Allows for both
 * bridging and [limited] routing of PPPoE packets.
 *
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: pppoed.c,v 1.1.1.1 1999/12/30 15:59:23 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 *
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

/* from the packet man page so that we compile under libc5 or glibc */
#include <sys/socket.h>
#include <features.h>    /* for the glibc version number */
#if __GLIBC__ >= 2 && __GLIBC_MINOR >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>     /* the L2 protocols */
#else
#include <asm/types.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>   /* The L2 protocols */
#endif

#include <netinet/in.h>

#define ETH_P_PPPoE_DISC	0x8863
#define ETH_P_PPPoE_SESN	0x8864

enum {
	stPPPOE_DISC_PADI,
	stPPPOE_DISC_PADR,
	stPPPOE_SESN,
} state = stPPPOE_DISC_PADI;

#define PPPoE_CODE_PADI		0x09
#define PPPoE_CODE_PADO		0x07
#define PPPoE_CODE_PADR		0x19
#define PPPoE_CODE_PADS		0x65
#define PPPoE_CODE_PADT		0xa7

unsigned char our_addr[6];
unsigned char dst_addr[6] = "\xff\xff\xff\xff\xff\xff";

#define PACK	__attribute__((packed))

struct pppoehdr {
	unsigned short	ether_type	PACK;
	unsigned char	vertype	PACK;
	unsigned char	code	PACK;
	unsigned short	sesn_id	PACK;
	unsigned short	length	PACK;
};

char *devname = "eth1";
struct sockaddr	sa;

void send_disc_pkt(int sk, unsigned char code, unsigned short sesn, unsigned short length, const char *data)
{
	unsigned char buf[1500];
	struct pppoehdr *h = (struct pppoehdr *)(buf+12);

	memcpy(buf, dst_addr, 6);
	memcpy(buf+6, our_addr, 6);

	h->ether_type = htons(ETH_P_PPPoE_DISC);
	h->vertype = 0x11;
	h->code = code;
	h->sesn_id = htons(sesn);
	h->length = htons(length);

	memcpy(buf, data, length);

	if (0 >= sendto(sk, buf, length + sizeof(*h) + 12, 0, &sa, sizeof(sa)))
		perror("write");
}

void send_disc_padi(int sk)
{
	send_disc_pkt(sk, PPPoE_CODE_PADI, 0x0000, 0, NULL);
}

void send_disc_padr(int sk)
{
	send_disc_pkt(sk, PPPoE_CODE_PADR, 0x0000, 0, NULL);
}

void pppoe_recv(const unsigned char *buf, int len)
{
	struct pppoehdr *h = (struct pppoehdr *)(buf + 12);
	if (memcmp(buf, our_addr, 6)) {
		fprintf(stderr, "not our address.\n");
		return;
	}

	if (h->vertype != 0x11) {
		fprintf(stderr, "wrong version.\n");
		return;
	}

	switch (state) {
	case stPPPOE_DISC_PADI:
		if (h->code == PPPoE_CODE_PADO) {
			memcpy(dst_addr, buf+6, 6);
			state = stPPPOE_DISC_PADR;
			fprintf(stderr, "got pado!\n");
		}
		break;

	case stPPPOE_DISC_PADR:
		if (h->code == PPPoE_CODE_PADS) {
			state = stPPPOE_SESN;
			fprintf(stderr, "got pads!\n");
			sprintf((char *)buf, "%02x%02x%02x%02x%02x%02x%04x%s",
				dst_addr[0], dst_addr[1], dst_addr[2],
				dst_addr[3], dst_addr[4], dst_addr[5],
				ntohs(h->sesn_id),
				devname
				);
			execlp("bdial", "bdial", "hse", buf, NULL);
			exit(1);
		}
		break;

	case stPPPOE_SESN:
		break;
	}
}

void parse_args(int argc, char *argv[])
{
	if (argc > 1) {
		if (strcmp(argv[1], "-d"))
			goto usage;

		if (argc != 3)
			goto usage;

		devname = argv[2];
	}

	return;

usage:
	fprintf(stderr, "usage:  pppoed  <args>\n"
			"where args may be:\n"
			"	-d <devname>\n");
	exit(2);
}

int main(int argc, char *argv[])
{
	unsigned char buf[4096];
	struct ifreq ifreq;
	int sk;

	parse_args(argc, argv);

	sk = socket(PF_INET, SOCK_PACKET, htons(ETH_P_PPPoE_DISC));
	if (sk < 0) {
		perror("socket");
		return 1;
	}

	memset(&ifreq, 0, sizeof(ifreq));
	strncpy(ifreq.ifr_name, devname, sizeof(ifreq.ifr_name));
	if (ioctl(sk, SIOCGIFHWADDR, &ifreq) < 0) {
		perror("SIOCGIFHWADDR:");
		return 1;
	}

	memcpy(our_addr, &ifreq.ifr_ifru.ifru_hwaddr.sa_data, 6);

	sa.sa_family = 0;
	strncpy(sa.sa_data, devname, sizeof(sa.sa_data));

	if (bind(sk, &sa, sizeof(sa)) < 0) {
		perror("bind");
		return 1;
	}

	for (;;) {
		int len, i;

		switch (state) {
		case stPPPOE_DISC_PADI:
			send_disc_padi(sk);
			break;

		case stPPPOE_DISC_PADR:
			send_disc_padr(sk);
			break;

		case stPPPOE_SESN:
			break;
		}

		alarm(2);	/* timeout in 2 seconds */
		len = read(sk, buf, sizeof(buf));

		if (len < 0) {
			perror("read");
			break;
		}
		alarm(0);
		printf("length: %d", len);

		for (i=0; i<len; i++) {
			if (!(i & 15)) {
				printf("    ");
				if (i > 0) {
					int j;
					char *ch = buf+i-16;
					for (j=0; j<16 && (i+j-16) < len; j++, ch++)
						putchar(isprint(*ch) ? *ch : '.'); 
				}
				printf("\n");
			}
			printf(" %02x", buf[i]);
		}
		while (i & 15) {
			i++;
			printf("   ");
		}

		printf("    ");
		if (i > 0) {
			int j;
			char *ch = buf+i-16;
			for (j=0; j<16 && (i+j-16) < len; j++, ch++)
				putchar(isprint(*ch) ? *ch : '.'); 
		}
		printf("\n");

		pppoe_recv(buf, len);
	}
	return 1;
}

