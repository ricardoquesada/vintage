# do this in case subincludes define all after including common.mak
all:

ifeq (,$(VERSION))
VERSION:=1.4
VER_TAG:=v$(shell echo $(VERSION) | sed s/\\./_/g)
endif

INSTALL=/usr/bin/install -c
prefix=/usr
exec_prefix=${prefix}
sbindir=${exec_prefix}/sbin
bindir=${exec_prefix}/bin
mandir=${prefix}/man
transform=s,x,x,

CPPFLAGS:=-I$(TOPDIR)/include
CFLAGS:=-Wall -O2 $(CPPFLAGS)

ARCH:=$(shell uname -m | sed -e s/i[3-6]86/i386/)
TARGET:=$(shell echo "i586-pc-linux-gnu" | sed -e s/i[3-6]86/i386/)

export VERSION PREFIX SBINDIR BINDIR MANDIR INSTALL

subdirs-all:
	for i in $(SUBDIRS) ; do if [ -e $$i/Makefile.in -a -e $$i/Makefile ] ; then ( cd $$i && $(MAKE) all ) || exit 1 ; fi ; done

subdirs-distclean:
	for i in $(SUBDIRS) ; do if [ -e $$i/Makefile.in -a -e $$i/Makefile ] ; then ( cd $$i && $(MAKE) distclean ) || exit 1 ; fi ; done

subdirs-clean:
	for i in $(SUBDIRS) ; do if [ -e $$i/Makefile.in -a -e $$i/Makefile ] ; then ( cd $$i && $(MAKE) clean ) || exit 1 ; fi ; done

subdirs-install:
	for i in $(SUBDIRS) ; do if [ -e $$i/Makefile.in -a -e $$i/Makefile ] ; then ( cd $$i && $(MAKE) install ) || exit 1 ; fi ; done

install-progs: $(PROGS)
	for i in $(PROGS) ; do ${INSTALL} $$i $(PROGS_BINDIR)/`echo $$i | sed '$(transform)'` ; done

install-manpages: $(MANPAGES)
	for i in $(MANPAGES) ; do ${INSTALL} -m 644 $$i $(mandir)/man`echo $$i | sed -e 's/^.*\\.//'`/`echo $$i | sed '$(transform)'` ; done

