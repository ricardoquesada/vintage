/*
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: bab_module.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#define __KERNEL__
#define MODULE

#include <linux/config.h>
#ifdef CONFIG_SMP
#define __SMP__
#endif

#ifdef CONFIG_MODVERSIONS
#define MODVERSIONS
#include <linux/modversions.h>
#endif
