/*
 * aps_if.h - Device Class Driver Interface Header
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: aps_if.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */

#ifndef _APS_IF_H
#define _APS_IF_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __KERNEL__
#include <sys/types.h>
#include <sys/ioctl.h>
#endif

#ifdef __KERNEL__
#include <linux/version.h>
#include <linux/types.h>
#include <linux/ioctl.h>
#include <linux/netdevice.h>
#include <linux/if_ether.h>
#include <linux/skbuff.h>

#include "vercomp.h"

/*
 * Call States
 */
#define CS_IDLE			0
#define CS_DIALING		1
#define CS_RINGING		2
#define CS_CONNECTING		3
#define CS_CONNECTED		4
#define CS_DISCONNECTING	5
#define CS_DISCONNECTED		6
#define CS_STALLED		7
#define CS_UNAVAIL		8

struct wait_queue;
struct sk_buff;

/* 
 * The channel structure 
 * Must be 32-bit aligned!!!!! 
 */
typedef struct channel {
	struct net_device	ndev;

	/* Device Specific */
	unsigned int mru;
	unsigned int device_id;
	unsigned int devclass_id;
	char device_name[16];
	char dev_class[16];
	unsigned int unit;
	unsigned int index;

	/* Pointer to associated device */
	void *dev;
  
	/* Outgoing/Incoming dn's */
	char CalledNumber[32];
	char CallerNumber[32];
	int callType;			/* call type for incoming calls */

	/* Filled in by the device class driver (callbacks) */
	void (*use)(struct channel *);
	void (*unuse)(struct channel *);
	int (*Output)(struct channel *, struct sk_buff *);
	int (*Connect)(struct channel *, const char *, u32);
	int (*Hangup)(struct channel *);
	void (*SetACCM)(struct channel *, u32 rx, u32 tx);
	int (*ioctl)(struct channel *ch, unsigned int cmd, unsigned long arg);

	/* Provided by the protocol driver to the class driver */
	void (*Open)(struct channel *);
	void (*Close)(struct channel *);
	void (*Up)(struct channel *);
	void (*Down)(struct channel *);
	void (*OutputComplete)(struct channel *);
	void (*ConnectComplete)(struct channel *, int);

	/* Next channel in the list */
	struct channel *next;

	/* Link we're attached to */
	void *link;

  	/* The current disposition of the channel */
	unsigned char state;
	unsigned char no_auth;		/* set by driver if link is already authenticated. */
	unsigned char reserved[2];

	unsigned int x_accm[4];		/* Tx Async Control Mask */
	unsigned int r_accm;		/* Rx Async Control Mask */

	struct net_device_stats	stats;

#if LINUX_VERSION_CODE < 0x20100
	unsigned long rx_bytes, tx_bytes;
#define CH_rx_bytes	rx_bytes
#define CH_tx_bytes	tx_bytes
#else
#define CH_rx_bytes	stats.rx_bytes
#define CH_tx_bytes	stats.tx_bytes
#endif
} channel_t;

#define ETH_P_PPP	0x0024

/* Provided by the protocol driver to the class driver */
extern void ch_Input(channel_t *ch, struct sk_buff *skb);
extern void ch_Open(struct channel *);
extern void ch_Close(struct channel *);
extern void ch_Up(struct channel *);
extern void ch_Down(struct channel *);
extern void ch_OutputComplete(struct channel *);
extern void ch_ConnectComplete(struct channel *, int);

#define clear_tbusy(ch)	(clear_bit(0, &(ch)->ndev.tbusy))
#define set_tbusy(ch)	(set_bit(0, &(ch)->ndev.tbusy))
#define tas_tbusy(ch)	(test_and_set_bit(0, &(ch)->ndev.tbusy))

/*
 * The following functions are exported from Babylon
 */
extern unsigned int RegisterDeviceClass(char *);
extern void UnregisterDeviceClass(unsigned int);
extern int RegisterChannel(channel_t *);
extern void UnregisterChannel(channel_t *);

#endif /*def __KERNEL__ */

struct bdev_stats {
	int	dev;
	unsigned long	rx_bytes, tx_bytes;
};

#define BIOCGETCHNAME		_IO('I', 0x25)	/* arg = char name[32] */
#define BIOCDIAL		_IO('I', 0x26)	/* arg = char *number */
#define BIOCANSWER		_IO('I', 0x2a)	/* arg = u32 */
#define BIOCGETCALLID		_IO('I', 0x2b)	/* arg = n/a */
#define BIOC_GETBSTATS		_IO('I', 0x2c)	/* arg = (struct bdev_stats *) */
#define BIOCGETDEVID		_IO('x', 0x14)	/* arg = n/a */
#define BIOCCREATEBUNDLE	_IO('x', 0x15)	/* arg = int * */
#define BIOCDESTROYBUNDLE	_IO('x', 0x16)	/* arg = int */
#define BIOCJOINBUNDLE		_IO('x', 0x17)	/* arg = int */
#define BIOCLEAVEBUNDLE		_IO('x', 0x18)	/* arg = n/a */
#define BIOCHANGUP		_IO('x', 0x19)	/* arg = n/a */
#define BIOC_SETCFL		_IO('x', 0x1a)	/* arg = int */
#define BIOC_GETCFL		_IO('x', 0x1b)	/* arg = int * */

#define AF_BPPP		29		/* protocol family */
#define PF_BPPP		AF_BPPP		/* protocol family */

#define BF_PPP		0x0001		/* channel: actually do ppp processing */
#define BF_ACFC		0x0002
#define BF_PFC		0x0004
#define BF_PASS_ML	0x0008
#define BF_PASS_IP	0x0010
#define BF_PASS_IPX	0x0020
#define BF_SSN		0x0040
#define BF_STAY_UP	0x0080		/* device: stay up when all channels are gone */
#define BF_VJC		0x0100		/* enable Van Jacobson header compression */
#define BF_STRICT_ORDER	0x0200		/* enable strict packet ordering on ML packets.  Required for most compression protocols */

#define BIOC_GETFRAME		_IO('x', 0x1e)	/* arg = (int *) */
#define BIOC_SETFRAME		_IO('x', 0x1f)	/* int */

#define FRAME_HDLC		0x00
#define FRAME_RAW		0x01

#define BIOC_GETMAXFRAMESIZE	_IO('x', 0x20)	/* (int *) */
#define BIOC_SETMAXFRAMESIZE	_IO('x', 0x21)	/* (int) */

#define BIOC_SETBFL		_IO('x', 0x22)	/* arg = (int[2]) */
#define BIOC_GETBFL		_IO('x', 0x23)	/* arg = (int[2]) */

#define BIOC_GETCALLTYPE	_IO('x', 0x24)	/* arg = (int *) */
#define BIOC_SETCALLTYPE	_IO('x', 0x25)	/* arg = (int) */

#define BIOC_GETCAUSECODE	_IO('x', 0x26)	/* arg = (int *) */
#define L2TP_BIND_SOCK		_IO('x', 0x27)	/* arg = int fd */
#define BIOC_GET_CALLING_NUMBER	_IO('x', 0x28)	/* arg = (char[32] *) */
#define BIOC_GET_CALLED_NUMBER	_IO('x', 0x29)	/* arg = (char[32] *) */
#define BIOC_GET_MAX_MRU	_IO('x', 0x2a)	/* arg = (int *) */

#define BIOC_MVIP_ATTACH	_IO('x', 0x2b)	/* arg = (int) = (stream << 8) | slot -> use MVIP_SLOT */
#define MVIP_SLOT_NONE		(~0UL)
#define MVIP_SLOT(stream, slot)	(((unsigned long)(stream) << 8) | (unsigned long)(slot))

#define CALLTYPE_64K		0
#define CALLTYPE_56K		1
#define CALLTYPE_SPEECH		2
#define CALLTYPE_31KHZ		3

#ifdef __cplusplus
};
#endif

#endif // _APS_IF_H

