/* Compatibility Linux-2.0.X <-> Linux-2.1.X
 * Copyright (C) 1997-2000 SpellCaster Telecommunications Inc.
 * $Id: vercomp.h,v 1.1.1.1 1999/12/30 15:59:24 mark Exp $
 * Released under the GNU Public License. See LICENSE file for details.
 */
#ifndef VERCOMP_H
#define VERCOMP_H

#include <linux/types.h>

#ifndef LINUX_VERSION_CODE
#include <linux/version.h>
#endif

#if (LINUX_VERSION_CODE < 0x02030e)
#define net_device	device
#endif

#if (LINUX_VERSION_CODE < 0x020100)
#include <linux/mm.h>
#include <linux/netdevice.h>

static inline unsigned long copy_from_user(void *to, const void *from, unsigned long n)
{
	int i;
	if ((i = verify_area(VERIFY_READ, from, n)) != 0)
		return i;
	memcpy_fromfs(to, from, n);
	return 0;
}

static inline unsigned long copy_to_user(void *to, const void *from, unsigned long n)
{
	int i;
	if ((i = verify_area(VERIFY_WRITE, to, n)) != 0)
		return i;
	memcpy_tofs(to, from, n);
	return 0;
}

static inline void dev_init_buffers(struct net_device *dev)
{
	int i;
	for (i=0; i<DEV_NUMBUFFS; i++)
		skb_queue_head_init(&dev->buffs[i]);
}

#define ioremap(x,y)		vremap(x,y)
#define iounmap(x)		vfree(x)
#define signal_pending(task)	!!((task)->signal & ~(task)->blocked)
#define proc_register(a,b)	proc_register_dynamic(a,b)
#define b_dev_kfree_skb(skb)	dev_kfree_skb(skb,FREE_WRITE)
#define b_kfree_skb(skb)	kfree_skb(skb,FREE_READ)
#define net_device_stats	enet_statistics
#define b_dev_queue_xmit(skb)	dev_queue_xmit(skb, skb->dev, 0)	

#else
#include <asm/uaccess.h>

#define b_kfree_skb(skb)	kfree_skb(skb)
#define b_dev_kfree_skb(skb)	dev_kfree_skb(skb)
#define b_dev_queue_xmit(skb)	dev_queue_xmit(skb)
#endif

#if (LINUX_VERSION_CODE < 0x020125)
#define test_and_clear_bit clear_bit
#define test_and_set_bit set_bit
#endif

#if (LINUX_VERSION_CODE < 0x20301)
struct wait_queue;
typedef struct wait_queue *wait_queue_head_t;
#define DECLARE_WAITQUEUE(wait, task)	struct wait_queue wait = { task, NULL }
extern inline void init_waitqueue_head(wait_queue_head_t *w)
{
	*w = NULL;
}
#endif

#if (LINUX_VERSION_CODE < 0x2030f)
#define dev_get_by_name(foo)	dev_get(foo)
#define dev_put(x)		((void)(x))
#endif

#endif
