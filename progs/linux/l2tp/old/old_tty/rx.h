
#ifndef __WIAS_RX_H
#define __WIAS_RX_H

#define ETH_P_WIAS_SESSION 0x8867

extern int paquetes_rx;

int wias_rx_init();
int wias_rx_cleanup();

#endif /* __WIAS_RX_H */
