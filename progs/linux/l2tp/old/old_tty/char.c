/*
 * wias character driver
 *
 */

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#define __NO_VERSION__ /* don't define kernel_verion in module.h */
#include <linux/module.h>
#include <linux/version.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/malloc.h>	/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/poll.h>
#include <linux/serial.h>
#include <asm/serial.h>

#include <asm/system.h>		/* cli(), *_flags */
#include <asm/segment.h>	/* memcpy and such */

#include "char.h"		/* local definitions */
#include "rx.h"

static int wias_refcount;
struct tty_driver wias_driver;

static struct serial_state wias_table[] = {
	SERIAL_PORT_DFNS	/* Defined in serial.h */
};
#define NR_PORTS	(sizeof(wias_table)/sizeof(struct serial_state))

static struct tty_struct *serial_table[NR_PORTS];
static struct termios *serial_termios[NR_PORTS];
static struct termios *serial_termios_locked[NR_PORTS];

static int get_async_struct(int line, struct async_struct **ret_info)
{
	struct async_struct *info;
	struct serial_state *sstate;

	sstate = wias_table + line;
	sstate->count++;
	if (sstate->info) {
		*ret_info = sstate->info;
		return 0;
	}
	info = kmalloc(sizeof(struct async_struct), GFP_KERNEL);
	if (!info) {
		sstate->count--;
		return -ENOMEM;
	}
	memset(info, 0, sizeof(struct async_struct));
	info->magic = SERIAL_MAGIC;
	info->port = sstate->port;
	info->flags = sstate->flags;
	info->xmit_fifo_size = sstate->xmit_fifo_size;
	info->line = line;
	info->tqueue.routine = NULL;
	info->tqueue.data = info;
	info->state = sstate;
	if (sstate->info) {
		kfree_s(info, sizeof(struct async_struct));
		*ret_info = sstate->info;
		return 0;
	}
	*ret_info = sstate->info = info;
	return 0;
}

/*
 * This routine is called whenever a serial port is opened.  It
 * enables interrupts for a serial port, linking in its async structure into
 * the IRQ chain.   It also performs the serial-specific
 * initialization for the tty structure.
 */
static int wias_open(struct tty_struct *tty, struct file * filp)
{
	struct async_struct	*info;
	int 			retval, line;

	MOD_INC_USE_COUNT;
	line = MINOR(tty->device) - tty->driver.minor_start;
	if ((line < 0) || (line >= NR_PORTS)) {
		MOD_DEC_USE_COUNT;
		return -ENODEV;
	}
	retval = get_async_struct(line, &info);
	if (retval) {
		MOD_DEC_USE_COUNT;
		return retval;
	}
	tty->driver_data = info;
	info->tty = tty;

	printk("wias_open %s%d, count = %d\n", tty->driver.name, info->line,
	       info->state->count);
	info->tty->low_latency = (info->flags & ASYNC_LOW_LATENCY) ? 1 : 0;

	/*
	 * If the port is the middle of closing, bail out now
	 */
	if (tty_hung_up_p(filp) ||
	    (info->flags & ASYNC_CLOSING)) {
		if (info->flags & ASYNC_CLOSING)
			interruptible_sleep_on(&info->close_wait);
		/* MOD_DEC_USE_COUNT; "info->tty" will cause this? */
#ifdef SERIAL_DO_RESTART
		return ((info->flags & ASYNC_HUP_NOTIFY) ?
			-EAGAIN : -ERESTARTSYS);
#else
		return -EAGAIN;
#endif
	}

	/*
	 * Start up serial port
	 */

	if ((info->state->count == 1) &&
	    (info->flags & ASYNC_SPLIT_TERMIOS)) {
		if (tty->driver.subtype == SERIAL_TYPE_NORMAL)
			*tty->termios = info->state->normal_termios;
		else 
			*tty->termios = info->state->callout_termios;
	}

	info->session = current->session;
	info->pgrp = current->pgrp;

	printk("<1>wias_open ttys%d successful...\n", info->line);
	return 0;
}

static void wias_put_char(struct tty_struct *tty, unsigned char ch)
{
	printk("<1>wias_put_char()\n");
}

static void wias_hangup(struct tty_struct *tty)
{
	printk("<1>wias_hangup()\n");
}

static void wias_flush_chars(struct tty_struct *tty)
{
	printk("<1>wias_flush_chars()\n");
}

static void wias_break(struct tty_struct *tty, int break_state)
{
	printk("<1>wias_break()\n");
}

static int wias_ioctl(struct tty_struct *tty, struct file * file, unsigned int cmd, unsigned long arg)
{
	printk("<1>wias_ioctl() cmd:0x%.8x arg:0x%.8x\n",cmd,arg);

	if ((cmd != TIOCGSERIAL) && (cmd != TIOCSSERIAL) &&
	    (cmd != TIOCSERCONFIG) && (cmd != TIOCSERGSTRUCT) &&
	    (cmd != TIOCMIWAIT) && (cmd != TIOCGICOUNT)) {
		if (tty->flags & (1 << TTY_IO_ERROR))
		    return -EIO;
	}
	
	switch (cmd) {
		case TIOCMGET:
			return 0;
		case TIOCMBIS:
		case TIOCMBIC:
		case TIOCMSET:
			return 0;
		case TIOCGSERIAL:
			return 0;
		case TIOCSSERIAL:
			return 0;
		case TIOCSERCONFIG:
			return 0;

		case TIOCSERGETLSR: /* Get line status register */
			return 0;

		case TIOCSERGSTRUCT:
			return 0;
				
		/*
		 * Wait for any of the 4 modem inputs (DCD,RI,DSR,CTS) to change
		 * - mask passed in arg for lines of interest
 		 *   (use |'ed TIOCM_RNG/DSR/CD/CTS for masking)
		 * Caller should use TIOCGICOUNT to see which one it was
		 */
		case TIOCMIWAIT:
			return 0;
		case TIOCGICOUNT:
			return 0;

		case TIOCSERGWILD:
		case TIOCSERSWILD:
			/* "setserial -W" is called in Debian boot */
			printk ("TIOCSER?WILD ioctl obsolete, ignored.\n");
			return 0;

		default:
			return -ENOIOCTLCMD;
		}
	return 0;
}

static void wias_set_termios(struct tty_struct *tty, struct termios *old_termios)
{
	printk("<1>wias_set_termios()\n");
}

/*
 * ------------------------------------------------------------
 * wias_close()
 * 
 * This routine is called when the serial port gets closed.  First, we
 * wait for the last remaining data to be sent.  Then, we unlink its
 * async structure from the interrupt chain if necessary, and we free
 * that IRQ if nothing is left in the chain.
 * ------------------------------------------------------------
 */
static void wias_close(struct tty_struct *tty, struct file * filp)
{
	struct async_struct * info = (struct async_struct *)tty->driver_data;
	struct serial_state *state;
	unsigned long flags;

	printk("<1>wias_close()\n");
	if (!info )
		return;

	state = info->state;
	
	save_flags(flags); cli();
	
	if (tty_hung_up_p(filp)) {
		MOD_DEC_USE_COUNT;
		restore_flags(flags);
		return;
	}
	
	printk("wias_close ttys%d, count = %d\n", info->line, state->count);

	if ((tty->count == 1) && (state->count != 1)) {
		/*
		 * Uh, oh.  tty->count is 1, which means that the tty
		 * structure will be freed.  state->count should always
		 * be one in these conditions.  If it's greater than
		 * one, we've got real problems, since it means the
		 * serial port won't be shutdown.
		 */
		printk("wias_close: bad serial port count; tty->count is 1, "
		       "state->count is %d\n", state->count);
		state->count = 1;
	}
	if (--state->count < 0) {
		printk("wias_close: bad serial port count for ttys%d: %d\n",
		       info->line, state->count);
		state->count = 0;
	}
	if (state->count) {
		MOD_DEC_USE_COUNT;
		restore_flags(flags);
		return;
	}
	info->flags |= ASYNC_CLOSING;
	/*
	 * Save the termios structure, since this port may have
	 * separate termios for callout and dialin.
	 */
	if (info->flags & ASYNC_NORMAL_ACTIVE)
		info->state->normal_termios = *tty->termios;
	if (info->flags & ASYNC_CALLOUT_ACTIVE)
		info->state->callout_termios = *tty->termios;
	/*
	 * Now we wait for the transmit buffer to clear; and we notify 
	 * the line discipline to only process XON/XOFF characters.
	 */
	tty->closing = 1;
	if (info->closing_wait != ASYNC_CLOSING_WAIT_NONE)
		tty_wait_until_sent(tty, info->closing_wait);
	/*
	 * At this point we stop accepting input.  To do this, we
	 * disable the receive line status interrupts, and tell the
	 * interrupt driver to stop checking the data ready bit in the
	 * line status register.
	 */

//	shutdown(info);
	if (tty->driver.flush_buffer)
		tty->driver.flush_buffer(tty);
	if (tty->ldisc.flush_buffer)
		tty->ldisc.flush_buffer(tty);
	tty->closing = 0;
	info->event = 0;
	info->tty = 0;
	if (info->blocked_open) {
		if (info->close_delay) {
			current->state = TASK_INTERRUPTIBLE;
			schedule_timeout(info->close_delay);
		}
		wake_up_interruptible(&info->open_wait);
	}
	info->flags &= ~(ASYNC_NORMAL_ACTIVE|ASYNC_CALLOUT_ACTIVE|
			 ASYNC_CLOSING);
	wake_up_interruptible(&info->close_wait);
	MOD_DEC_USE_COUNT;
	restore_flags(flags);
}

static int wias_write(struct tty_struct * tty, int from_user, const unsigned char *buf, int count)
{
	printk("<1>wias_write()\n");
	return 0;
}

static int wias_write_room(struct tty_struct *tty)
{
	printk("<1>wias_write_room()\n");
	return 500;
}

static void wias_flush_buffer(struct tty_struct *tty)
{
	printk("<1>wias_flush_buffer()\n");
}

/*
 * This function is used to send a high-priority XON/XOFF character to
 * the device
 */
static void wias_send_xchar(struct tty_struct *tty, char ch)
{
	printk("<1>wias_send_xchar()\n");
}

/*
 * ------------------------------------------------------------
 * wias_throttle()
 * 
 * This routine is called by the upper-layer tty layer to signal that
 * incoming characters should be throttled.
 * ------------------------------------------------------------
 */
static void wias_throttle(struct tty_struct * tty)
{
	printk("<1>wias_throttle()\n");
}

static void wias_unthrottle(struct tty_struct * tty)
{
	printk("<1>wias_unthrottle()\n");
}

int wias_read_proc(char *page, char **start, off_t idx, ssize_t count, int *eof, void *data)
{
	printk("<1>wias_read_proc()\n");
	return 0;
}

/*
 * ------------------------------------------------------------
 * wias_stop() and wias_start()
 *
 * This routines are called before setting or resetting tty->stopped.
 * They enable or disable transmitter interrupts, as necessary.
 * ------------------------------------------------------------
 */
static void wias_stop(struct tty_struct *tty)
{
	printk("<1>wias_stop()\n");
}

static void wias_start(struct tty_struct *tty)
{
	printk("<1>wias_start()\n");
}

static void wias_wait_until_sent(struct tty_struct *tty, int timeout)
{
	printk("<1>wias_wait_until_sent()\n");
}

static int wias_chars_in_buffer(struct tty_struct *tty)
{
	printk("<1>wias_chars_in_buffer()\n");
	return 0;
}

/*
 * Operaciones necesarias para un tty device
 */
/**
 * @fn int wias_char_init()
 */
int wias_char_init()
{

	memset(&wias_driver, 0, sizeof(struct tty_driver));
	wias_driver.magic = 'RG';
	wias_driver.driver_name = "pindonga";
	wias_driver.name = "wiasS";
	wias_driver.major = 0;			/* dynamic */
	wias_driver.minor_start = 64;
	wias_driver.num = NR_PORTS;
	wias_driver.type = TTY_DRIVER_TYPE_SERIAL;
	wias_driver.subtype = SERIAL_TYPE_NORMAL;
	wias_driver.init_termios = tty_std_termios;
	wias_driver.init_termios.c_cflag =
		B9600 | CS8 | CREAD | HUPCL | CLOCAL;
	wias_driver.flags = TTY_DRIVER_REAL_RAW;
	wias_driver.refcount = &wias_refcount;
	wias_driver.table = serial_table;
	wias_driver.termios = serial_termios;
	wias_driver.termios_locked = serial_termios_locked;

	wias_driver.open = wias_open;
	wias_driver.close = wias_close;
	wias_driver.write = wias_write;
	wias_driver.put_char = wias_put_char;
	wias_driver.flush_chars = wias_flush_chars;
	wias_driver.write_room = wias_write_room;
	wias_driver.chars_in_buffer = wias_chars_in_buffer;
	wias_driver.flush_buffer = wias_flush_buffer;
	wias_driver.ioctl = wias_ioctl;
	wias_driver.throttle = wias_throttle;
	wias_driver.unthrottle = wias_unthrottle;
	wias_driver.send_xchar = wias_send_xchar;
	wias_driver.set_termios = wias_set_termios;
	wias_driver.stop = wias_stop;
	wias_driver.start = wias_start;
	wias_driver.hangup = wias_hangup;
	wias_driver.break_ctl = wias_break;
	wias_driver.wait_until_sent = wias_wait_until_sent;
	wias_driver.read_proc = wias_read_proc;

	if (tty_register_driver(&wias_driver))
		return -1;
	return 0;
}

/**
 * @fn int wias_char_cleanup()
 */
int wias_char_cleanup()
{
	tty_unregister_driver(&wias_driver);
	return 0;
}
