/* 
 * wias rx kernel driver
 */

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#define __NO_VERSION__ /* don't define kernel_verion in module.h */
#include <linux/module.h>
#include <linux/version.h>

#include <linux/module.h>
#include <linux/config.h>	/* for CONFIG_IP_FORWARD */
#include <linux/version.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/malloc.h>	/* kmalloc() */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/interrupt.h>	/* mark_bh */

#include <linux/netdevice.h>	/* struct device, and other headers */
#include <linux/etherdevice.h>	/* eth_type_trans */
#include <linux/ip.h>		/* struct iphdr */
#include <linux/in.h>		/* struct sockaddr_in */
#include <linux/skbuff.h>
#include <linux/rtnetlink.h>
#include <linux/if_arp.h>
#include <linux/module.h>

#include "rx.h"

/*
 * receive
 */

int paquetes_rx = 0;

/**
 * @fn int wias_rx_sesn(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
 */
int wias_rx_sesn(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
{

	struct tty_struct *tty=NULL;
	struct WiasHdr  *whdr;

	int i,found=0,length=0;

	whdr = (struct WiasHdr *) skb->nh.raw;

	/* XXX: should use a hash table */ 
	for (i=0;i<N_SES;i++) {
		if (wias_state[i].tty)
			if (wias_state[i].x_pppoe_hdr.pppoe.SessionID == pppoehdr->SessionID) {
				found=1; tty=wias_state[i].tty;
				break;
		}
	}


	if (!found || (NULL == tty)) {
		/* if (net_ratelimit())
			printk(KERN_INFO "pppoe: discarding packet for session %d, no ppp device waiting\n", ntohs(pppoehdr->SessionID)); */
		goto error;
	}

	length=htons(whdr->length);

	if (length < WIASOE_HDR_LEN) {
		goto error;  
	}

	/* remove the WIAS headers */
	skb->data=skb_pull(skb,WIAS_HDR_LEN);
	

	tty->ldisc.receive_buf(tty, skb->data, 0, length);


	kfree_skb(skb);
		return 0;

error:
	kfree_skb(skb);
	return 0;
}

/**
 * @fn int wias_rx_cleanup()
 */
int wias_rx_cleanup()
{
	dev_remove_pack(&pt_wias_session);
	return 0;
}
