/*
 *
 * main.c
 *
 */

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#define __NO_VERSION__ /* don't define kernel_verion in module.h */
#include <linux/module.h>
#include <linux/version.h>

char kernel_version [] = UTS_RELEASE;

#include <linux/kernel.h>	/* printk() */
#include <linux/malloc.h>	/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/poll.h>

#include <asm/system.h>		/* cli(), *_flags */
#include <asm/segment.h>	/* memcpy and such */

#include "char.h"		/* local definitions */
#include "rx.h"

int init_module(void)
{
	/*
	 * register rx packet type driver
	 */
	wias_rx_init();

	/*
	 * register char device
	 */
	wias_char_init();
	return 0;
}

void cleanup_module(void)
{
	wias_rx_cleanup();
	wias_char_cleanup();
}
