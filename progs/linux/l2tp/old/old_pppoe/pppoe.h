#ifndef _LINUX_PPPOE_H
#define _LINUX_PPPOE_H 1

#include "pppoe_ioctl.h"
#define ETH_P_PPP_DISC 0x8863          /* PPPoE discovery messages     */
#define ETH_P_PPP_SES  0x8864          /* PPPoE session messages       */

#define ETH_P_WIAS_DISC 0x8866          /* WiasoE discovery messages     */
#define ETH_P_WIAS_SES  0x8867          /* WiasoE session messages       */

#define PPPOE_HDR_LEN  sizeof(struct PPPOEHdr)
#define WIASOE_HDR_LEN  sizeof(struct WIASOEHdr)
//#define N_SES 16
#define N_SES 16


/* the session PPPOE header */

struct PPPOEHdr {
#if defined(__LITTLE_ENDIAN_BITFIELD)
        __u16   version:4,
                type:4,
                code:8;
#elif defined(__BIG_ENDIAN_BITFIELD)
        __u16   code:8,
		type:4,
                version:4;
#endif
        __u16   SessionID;
        __u16   length;
}__attribute__ ((packed));


struct PPPOETHER {
struct ethhdr ether;
struct PPPOEHdr pppoe;
} __attribute__ ((packed));


struct WIASOEHdr {
	__u16	msg:4,
		len:12;
	__u16	seq;
}__attribute__ ((packed));

#define WIAS_MSG_MODEM_REQUEST	(0x8)
#define WIAS_MSG_MODEM_REPLY	(0x4)
#define WIAS_MSG_ECHO_REQUEST	(0x5)
#define WIAS_MSG_ECHO_REPLY	(0x9)

struct WIASOETHER {
	struct ethhdr ether;
	struct WIASOEHdr pppoe;
} __attribute__ ((packed));


#ifdef __KERNEL__

extern int pppoe_rcv(struct sk_buff *skb, struct device *dev, struct packet_type *pt);
extern int wiasoe_rcv(struct sk_buff *skb, struct device *dev, struct packet_type *pt);

#endif



#define PPPOE_TYPE 1
#define PPPOATM_TYPE 2
/* L2TP */
#define PPPOUDP_TYPE 3    

#ifdef __KERNEL__
#define CLOSED 0
#define OPENED 1

struct pppox_struct {
        int magic;
        struct tty_struct *tty;
	struct device *dev;
	union {
		struct PPPOETHER pppoe_hdr;
		struct WIASOETHER wiasoe_hdr;
	} pppox_hdr_ptr;
	int state;
	int counter;
};

#endif

#define x_pppoe_hdr pppox_hdr_ptr.pppoe_hdr 
#define x_wiasoe_hdr pppox_hdr_ptr.wiasoe_hdr 


#endif
