#ifndef __PPPOE_IOCTL_H
#define __PPPOE_IOCTL_H 1

#include <linux/if_packet.h>
#include <asm/ioctl.h>

#ifdef __KERNEL__
extern int pppoe_ioctl(unsigned int cmd, void *arg);
#endif

#define POE_VERSION 1
struct poe_arg { 
	int				   poe_version; 
	int    			   poe_session_id;
	struct sockaddr_ll poe_addr; 
};

/* could probably set most of these in one call */
#define PPPOEIOCSID   _IOW('t',128,struct poe_arg)         /* set session ID */
#define PPPOEIOGSID   _IOW('t',129,struct poe_arg)         /* get session ID */
#define PPPOEIOCDMAC   _IOW('t',130,struct poe_arg)        /* set dst MAC */
/* set the ethernet device index (in poe_addr.sll_ifindex) */ 
#define PPPOEIOCSDEV  _IOW('t',131,struct poe_arg) 

#endif
