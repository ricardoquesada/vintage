/*
 *  linux/drivers/net/pppox.c
 *
 *
 * Author: Jamal Hadi Salim (hadi@cyberus.ca)	
 *
 *	   Initial revision 1999/09/24 (my interpretation of the RFC)
 *	   Partial Rewrite  1999/10/02 (after testing)
 *
 *     Hacked by Andi Kleen <ak@suse.de> in 1999/10/29 for modularization
 *	   and various cleanups.
 *
 * This driver tries to be a generic PPP over X encapsulator/
 * Decapsulator: 
 *
 * X could imply a lot of things (eg Ethernet in PPPOE,
 * ATM in the case PPP over ATM, UDP sockets in the case of L2TP, SONET
 * in case of PPP over Sonet,etc). It seems everyone loves PPP
 * framing. Small overhead, and can encapsulate multiple protocols.
 * 
 * This code assumes Synchronous PPP framing
 *
 * Eventually, split this into two parts: A generic part, which should really
 * be part of PPP and a "specific" part which is mostly the encapsulation,
 * decapsulation and the ioctls. I have tried to comment on the "generic"
 * and PPPOE "specific" split.
 *
 * TODO:
 * Introduce a registration API which binds the "specific" part to the
 * the generic.
 */

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#define __NO_VERSION__ /* don't define kernel_verion in module.h */
#include <linux/config.h>
#include <linux/module.h>	

#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/fcntl.h>
#include <linux/string.h>
#include <linux/major.h>
#include <linux/mm.h>
#include <linux/init.h>
#include <linux/notifier.h>

#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/bitops.h>

#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/if_ether.h>

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include <linux/devpts_fs.h>
#include <linux/if.h>		/* ifreq	*/

#include <linux/ppp_defs.h>	

#include "pppoe.h"

#define PPPOX_MAJOR 144
#define PPPOE_MINOR_START 0	   /* - 63 for PPPoE */ 

#define MIN(a,b)	((a) < (b) ? (a) : (b))

int pox_ioctl (struct tty_struct *tty, struct file *file,
	     unsigned int cmd, unsigned long arg);

static struct packet_type pppoe_packet_type =
{
	__constant_htons(ETH_P_PPP_SES),
	NULL,		/* All devices */
	pppoe_rcv,
	NULL,
	NULL
};

static struct packet_type wiasoe_packet_type =
{
	__constant_htons(ETH_P_WIAS_SES),
	NULL,		/* All devices */
	wiasoe_rcv,
	NULL,
	NULL
};


static struct tty_struct *pppox_table[N_SES];
static struct termios *pppox_termios[N_SES];
static struct termios *pppox_termios_locked[N_SES];

static struct tty_driver pppox_driver;
static int pppox_refcount;
static struct pppox_struct pppox_state[N_SES];

static struct tty_driver wiasox_driver;
static int wiasox_refcount;
static struct pppox_struct wiasox_state[N_SES];

static int debug;

/*
* Generic
*/

static void pppox_close(struct tty_struct * tty, struct file * filp)
{
	int line;

	if (!tty)
		return;

	if (tty->count == 1)
		tty->driver_data = 0;

	line = MINOR(tty->device) - tty->driver.minor_start;
	if ((unsigned) line > N_SES) {
		printk(KERN_DEBUG "pppox_close: bogus line %d\n", line); 
		return;
	}	

	if (--pppox_state[line].counter > 0) { 
		if (debug > 1) 
			printk(KERN_DEBUG "pppox_close: not really closing\n");
		MOD_DEC_USE_COUNT; 
		return; 
	} else if (pppox_state[line].counter < 0) { 
		printk(KERN_DEBUG "pppox_close: bogus counter %d\n",
			   pppox_state[line].counter);
		return; 
	}

	if (pppox_state[line].tty == NULL) { 
		printk(KERN_DEBUG "pppox_close: device already closed\n"); 
		return;
	}		
	
	start_bh_atomic();
	pppox_state[line].tty=NULL;
	pppox_state[line].state=CLOSED;
	end_bh_atomic();

	if (debug>2)
		printk(KERN_DEBUG "pppox_close called\n"); 

	MOD_DEC_USE_COUNT;
}

/*
* Generic
*/
static void pppox_unthrottle(struct tty_struct * tty)
{

	if (!tty)
		return;

	if ((tty->flags & (1 << TTY_DO_WRITE_WAKEUP)) &&
	    tty->ldisc.write_wakeup)
		(tty->ldisc.write_wakeup)(tty);
	wake_up_interruptible(&tty->write_wait);
	set_bit(TTY_THROTTLED, &tty->flags);
}

/*
* Half Generic: 
*
* The generic part just receives the buffer from PPP
* and passes it to the non-generic part (in this case
* PPPOE).
*
*/
static int pppoe_write(struct tty_struct * tty, int from_user,
		       const unsigned char *buf, int count)
{
	struct device *etherdev;
	struct sk_buff *skb;
	int line,c=0;
	int i;
	int start=0;


/* is 0 the best value to return here ? */
	if (!tty || tty->stopped)
		return 0;

	line = MINOR(tty->device) - tty->driver.minor_start;

/* check for inconsistencies */
	if ((pppox_state[line].tty != tty) || (CLOSED == pppox_state[line].state)) {
		if (debug > 0) 
			printk("pppox_write: inconsistent tty or closed tty\n");
		return -EIO;
	}

	if (NULL == pppox_state[line].dev) {
		if (debug > 0)
			printk("pppox_write: NULL  device\n");
		return -EIO;
	}

#if 0
/* this is necessarily evil to allow for pppd invocation of pppoed in user
space. pppd passes its initial control data over user space opens ;-> 
Later add capabilities stuff */
	if (from_user) {
		printk("pppox_write: illegal %d chars from user space\n",count);

		/* swallow them ;-> */
		return count;
	} else {

		c = MIN(count, tty->ldisc.receive_room(tty));


	}
#else
		c = MIN(count, tty->ldisc.receive_room(tty));
#endif

	if (c < PPP_HDRLEN) {
		printk("\n BAD PPP data! length %d\n",c);
		goto printerror;
	}


	if (buf[0] == PPP_FLAG) {
		if (buf[1]!=PPP_ALLSTATIONS) 
			start=1;
		else  
			start=3;
	}

	if (buf[0] == PPP_ALLSTATIONS) {
		if (buf[1]!=PPP_UI) {
			printk("bad data %x\n",buf[1]);
			goto printerror;
		} else
			start=2;
	}

	if (start == 0)
		goto printerror;

#undef PPPOX_DEB
#ifdef PPPOX_DEB
	printk("\nWrite START: %d bytes of data \n",c);
	for (i=0;i<c;i++) {
		if (0 == i%8)
			printk ("\n %d:",i);
		printk(" %x ",*(buf+i));
	}
	printk("\nDump END: all data .....\n");
#endif

/* grab the etherdev from the table */
	etherdev=pppox_state[line].dev;

	skb=alloc_skb(PPPOE_HDR_LEN+c+etherdev->hard_header_len,GFP_ATOMIC);
	if (skb == NULL) {
		printk(KERN_NOTICE "%s: Memory squeeze, dropping packet.\n", etherdev->name);
		return c;	
	}

	skb->dev = etherdev;
	/* for now */

	skb->mac.raw=skb->data;
	skb_reserve(skb,PPPOE_HDR_LEN+etherdev->hard_header_len);

	memcpy(skb_put(skb, c-start),buf+start,c-start);
	
/* fix these two below at some point they are wrong */
	skb->priority = 1;
	skb->nh.raw=skb->mac.raw+etherdev->hard_header_len;

	skb->protocol=htons(ETH_P_PPP_SES);

	pppox_state[line].x_pppoe_hdr.pppoe.length=htons(c-start);
/*
And finally shove the whole pppoe_hdr
*/
	memcpy(skb_push(skb,sizeof(struct PPPOETHER)),&pppox_state[line].x_pppoe_hdr,sizeof(struct PPPOETHER));


	dev_queue_xmit (skb);

	return c;
printerror:

	if (debug > 1) { 
		printk("\nDump START: all data .....\n");
		for (i=0;i<c;i++) {
			if (0 == i%8) 
				printk ("\n %d:",i);
			printk(" %2x ",*(buf+i));
		}
		printk("\nDump END: all data .....\n");
		return c-1;
	}
	return -1;
}

/*
* Generic
*/
static int pppox_write_room(struct tty_struct *tty)
{

	if (!tty || tty->stopped)
		return 0;

	return tty->ldisc.receive_room(tty);
}

/*
* Generic
*/
static int pppox_chars_in_buffer(struct tty_struct *tty)
{
	int count;

	if (!tty || !tty->ldisc.chars_in_buffer)
		return 0;

	/* The ldisc must report 0 if no characters available to be read */
	count = tty->ldisc.chars_in_buffer(tty);

	return count;
}

/*
* Generic
*/

static void pppox_flush_buffer(struct tty_struct *tty)
{
	
	if (!tty)
		return;
	
	if (tty->ldisc.flush_buffer)
		tty->ldisc.flush_buffer(tty);
	
	if (tty->packet) {
		tty->ctrl_status |= TIOCPKT_FLUSHWRITE;
		wake_up_interruptible(&tty->read_wait);
	}
}

/*
* specific
*/
int init_PPPOE(int line)
{

	pppox_state[line].counter = 1;
	pppox_state[line].magic=PPPOE_TYPE;
	pppox_state[line].x_pppoe_hdr.pppoe.version=1;
	pppox_state[line].x_pppoe_hdr.pppoe.type=1;
	pppox_state[line].x_pppoe_hdr.pppoe.code=0;
	pppox_state[line].x_pppoe_hdr.ether.h_proto=htons(ETH_P_PPP_SES);
/* should be reset everytime we receive data from PPP */
	pppox_state[line].x_pppoe_hdr.pppoe.length=0;

	return 0;


}

/*
* Half Generic
*/
static int pppox_open(struct tty_struct *tty, struct file * filp)
{
	int	retval;
	int	line;
	struct	pppox_struct *pppox;

	retval = -ENODEV;
	if (!tty)
		return retval;
	line = MINOR(tty->device) - tty->driver.minor_start;
	if ((line < 0) || (line >= N_SES))
		return retval;

	MOD_INC_USE_COUNT;

	if (pppox_state[line].tty) {
		pppox_state[line].counter++; 
		if (debug > 1) 
			printk(KERN_INFO "pppox_open: line already occupied!\n");
		return 0;	
	}

	if (init_PPPOE(line) < 0) { 
		MOD_DEC_USE_COUNT;
		return -EIO; 
	}

	pppox = (struct pppox_struct *)(tty->driver.driver_state) + line;
	tty->driver_data = pppox;

	if (tty->count != 1) { 
		MOD_DEC_USE_COUNT;
		return -EIO;
	}

	clear_bit(TTY_OTHER_CLOSED, &tty->flags);
		

	pppox_state[line].tty=tty;
	pppox_state[line].state=OPENED;
	set_bit(TTY_THROTTLED, &tty->flags);

	return 0;
}

/*
* Generic
*/
static void pppox_set_termios(struct tty_struct *tty, struct termios *old_termios)
{
	tty->termios->c_cflag &= ~(CSIZE | PARENB);
	tty->termios->c_cflag |= (CS8 | CREAD);
}

static int pox_netdev_notifier(struct notifier_block *this, 
							   unsigned long event,
							   void *ptr)
{
	struct device *dev = ptr;
	int i;

	/* XXX Probably races. */ 
	if (event != NETDEV_DOWN && event != NETDEV_REBOOT)
		return NOTIFY_DONE;
	for (i = 0; i < N_SES; i++) { 
		if (pppox_state[i].dev == dev) {
			printk(KERN_INFO "pppoe:%d: device went away from under us\n",
				   i);
			pppox_state[i].dev = NULL;
		}
	}
	return NOTIFY_DONE;
}

static struct notifier_block pppox_netdev_notifier = { 
	pox_netdev_notifier,
	NULL,
	0
};

int __init pppox_init(void)
{
	int i;

	memset(&pppox_driver, 0, sizeof(struct tty_driver));
	pppox_driver.magic = TTY_DRIVER_MAGIC;
	pppox_driver.driver_name = "pppox";
	pppox_driver.name = "pppox";
	pppox_driver.major = PPPOX_MAJOR;
	pppox_driver.minor_start = PPPOE_MINOR_START;
	pppox_driver.num = N_SES;

/* just like the ttyS* */

	pppox_driver.type = TTY_DRIVER_TYPE_SERIAL;
	pppox_driver.subtype = SERIAL_TYPE_NORMAL;
	pppox_driver.init_termios = tty_std_termios;
	pppox_driver.init_termios.c_iflag = 0;
	pppox_driver.init_termios.c_oflag = 0;
	pppox_driver.init_termios.c_cflag = CS8 | CREAD;
	pppox_driver.init_termios.c_lflag = 0;
	pppox_driver.flags = TTY_DRIVER_RESET_TERMIOS | TTY_DRIVER_REAL_RAW;
	pppox_driver.refcount = &pppox_refcount;
	pppox_driver.table = pppox_table;
	pppox_driver.termios = pppox_termios;
	pppox_driver.termios_locked = pppox_termios_locked;
	pppox_driver.driver_state = pppox_state;

	pppox_driver.open = pppox_open;
	pppox_driver.close = pppox_close;
	pppox_driver.write = pppoe_write;
	pppox_driver.write_room = pppox_write_room;
	pppox_driver.flush_buffer = pppox_flush_buffer;
	pppox_driver.chars_in_buffer = pppox_chars_in_buffer;
	pppox_driver.unthrottle = pppox_unthrottle;
	pppox_driver.set_termios = pppox_set_termios;
	pppox_driver.ioctl = pox_ioctl;

	if (tty_register_driver(&pppox_driver)) { 
		printk(KERN_ERR "Couldn't register pppox driver");
		return -EINVAL;
	}

	/* also register the SESSION Ether protocol */
	dev_add_pack(&pppoe_packet_type);

	for (i=0;i<N_SES;i++) {
		pppox_state[i].tty=NULL;
	}

	printk(KERN_INFO "Registering PPPoE driver\n"); 

	register_netdevice_notifier(&pppox_netdev_notifier); 

	return 0;
}


/*
* Half generic
*/
int wiasoe_rcv(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
{
	struct tty_struct *tty=NULL;
	struct WIASOEHdr  *wiasoehdr;
	int len=0;

	wiasoehdr=(struct WIASOEHdr  *)skb->nh.raw;

	len = htons(wiasoehdr->len);

	switch( wiasoehdr->msg ) {
		case WIAS_MSG_MODEM_REQUEST:
		case WIAS_MSG_ECHO_REQUEST:
		default:
			break;
	}

	tty=wiasox_state[0].tty;

	/* remove the WIASOE headers */
	skb->data=skb_pull(skb,WIASOE_HDR_LEN);
	
	tty->ldisc.receive_buf(tty, skb->data, 0, len);

	kfree_skb(skb);
	return 0;

//error:
	kfree_skb(skb);
	return 0;
}
int pppoe_rcv(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
{
	struct tty_struct *tty=NULL;
	struct PPPOEHdr  *pppoehdr;

	int i,found=0,length=0;

	pppoehdr=(struct PPPOEHdr  *)skb->nh.raw;

	/* XXX: should use a hash table */ 
	for (i=0;i<N_SES;i++) {
		if (pppox_state[i].tty)
			if (pppox_state[i].x_pppoe_hdr.pppoe.SessionID == pppoehdr->SessionID) {
				found=1;
				tty=pppox_state[i].tty;
				break;
		}
	}


	if (!found || (NULL == tty)) {
		/* if (net_ratelimit())
			printk(KERN_INFO "pppoe: discarding packet for session %d, no ppp device waiting\n", ntohs(pppoehdr->SessionID)); */
		goto error;
	}

	length=htons(pppoehdr->length);

	if (length < PPPOE_HDR_LEN) {
		goto error;  
	}

	/* remove the PPPOE headers */
	skb->data=skb_pull(skb,PPPOE_HDR_LEN);
	

	tty->ldisc.receive_buf(tty, skb->data, 0, length);


	kfree_skb(skb);
		return 0;

error:
	kfree_skb(skb);
	return 0;
}

static int pox_getarg(struct poe_arg *poe, unsigned long arg)
{
	if (copy_from_user(poe,(void*)arg,sizeof(struct poe_arg)))
		return -EFAULT;
	if (poe->poe_version != POE_VERSION) { 
		if (debug > 1) 
			printk(KERN_DEBUG "pox_getarg: invalid pppoe version\n"); 
		return -EINVAL;
	}
	return 0;
}

int
pox_ioctl (struct tty_struct *tty, struct file *file,
			 unsigned int cmd, unsigned long arg)
{
	struct poe_arg poe; 
	int err;
	int line;
	struct pppox_struct *ps; 
	
	if (!tty) {
		printk("pox_ioctl called with NULL tty!\n");
		return -EIO;
	}

	switch (cmd) { 
	case PPPOEIOCSDEV:
	case PPPOEIOCSID:
	case PPPOEIOCDMAC:
		break;
	default:
		return -ENOIOCTLCMD;
	}

	line = MINOR(tty->device) - tty->driver.minor_start;
	if (line > N_SES) 
		return -ENODEV;

	ps = &pppox_state[line]; 

	err = pox_getarg(&poe, arg); 
	if (err) 
		return err; 

/* some more IOCTLs to add:
   delete device,  (set state to closed as well)
*/

	err = 0;
	switch (cmd) {
	case PPPOEIOCSDEV: {		/* Set ethernet device */ 
		struct device *dev;

		err = -ENODEV; 
		dev = dev_get_by_index(poe.poe_addr.sll_ifindex); 
		if (NULL == dev) 
			break;
		start_bh_atomic();
		ps->dev = dev;
		memcpy(ps->x_pppoe_hdr.ether.h_source,&dev->dev_addr,ETH_ALEN);
		end_bh_atomic();
		err = 0;
		break; 
	}

	case PPPOEIOCSID:
		start_bh_atomic();
		ps->x_pppoe_hdr.pppoe.SessionID = poe.poe_session_id;
		end_bh_atomic();
		break;

	case PPPOEIOCDMAC:
		start_bh_atomic();
		memcpy(ps->x_pppoe_hdr.ether.h_dest, poe.poe_addr.sll_addr, ETH_ALEN); 
		end_bh_atomic();

		break;
		
	default:
		/* Should not happen */ 
		err = -ENOIOCTLCMD;
		break;
	}

	return err;
}

MODULE_AUTHOR("Jamal Hadi Salim (hadi@cyberus.ca)"); 
MODULE_DESCRIPTION("PPP over Ethernet driver"); 
MODULE_PARM(debug, "i"); 

#ifdef MODULE

int init_module(void) 
{
	return pppox_init();
}

void cleanup_module(void)
{
	int err;

	printk(KERN_INFO "Removing PPPoE protocol\n"); 

	dev_remove_pack(&pppoe_packet_type);

	printk(KERN_INFO "Removing PPPoE tty driver\n");

	err = tty_unregister_driver(&pppox_driver);
	if (err < 0)
		printk(KERN_DEBUG "pppoe: tty_unregister_driver failed: %d\n", err); 

	unregister_netdevice_notifier(&pppox_netdev_notifier); 

	synchronize_bh(); 
}
#endif
