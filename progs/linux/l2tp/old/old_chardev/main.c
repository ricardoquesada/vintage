/*
 *
 * main.c
 *
 */

#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#define __NO_VERSION__ /* don't define kernel_verion in module.h */
#include <linux/module.h>
#include <linux/version.h>

char kernel_version [] = UTS_RELEASE;

#include <linux/kernel.h>	/* printk() */
#include <linux/malloc.h>	/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/poll.h>

#include <asm/system.h>		/* cli(), *_flags */
#include <asm/segment.h>	/* memcpy and such */

#include "scull.h"		/* local definitions */


/*
 * I don't use static symbols here, because register_symtab is called
 */

int scull_major =	SCULL_MAJOR;
int scull_nr_devs =	SCULL_NR_DEVS;	  /* number of bare scull devices */
int scull_quantum =	SCULL_QUANTUM;
int scull_qset =	SCULL_QSET;

Scull_Dev *scull_devices; /* allocated in init_module */

/*
 * Different minors behave differently, so let's use multiple fops
 */

int paquetes_rx = 3;
int paquetes_tx = 3;

static int wias_stats_get_info(char *buffer, char **start, off_t offset, int length, int dummy)
{
	int len = 0;
	off_t begin = 0;

	len += sprintf(buffer + len, "paquetes enviados: %d\npaquetes recividos: %d\n",paquetes_tx,paquetes_rx);

	*start = buffer + (offset - begin);
	len -= (offset - begin);
	if (len > length)
		len = length;
	return len;
}
struct proc_dir_entry scull_proc_entry = {
	0,
	10, "wias_stats",
	S_IFREG | S_IRUGO, 1, 0, 0, 0,
	&proc_net_inode_operations,
	wias_stats_get_info,
	NULL, NULL, NULL, NULL, NULL
};

/*
 * Open and close
 */

int scull_open (struct inode *inode, struct file *filp)
{
	int num = NUM(inode->i_rdev);
	Scull_Dev *dev; /* device information */

	/* type 0, check the device number */
	if (num >= scull_nr_devs) return -ENODEV;
	dev = &scull_devices[num];

	/* and use filp->private_data to point to the device data */
	filp->private_data = dev;

	MOD_INC_USE_COUNT;
	return 0;		   /* success */
}

static int scull_release(struct inode * inode, struct file * filp)
{
	MOD_DEC_USE_COUNT;
	return 0;
}

/*
 * Data management: read and write
 */

static ssize_t scull_read(struct file * file, char * buf, size_t count, loff_t *ppos)
{
	return 0;
}

static ssize_t scull_write(struct file * file, const char * buf, size_t count, loff_t *ppos )
{
	return 0;
}

static long long scull_lseek(struct file * file, long long offset, int orig)
{
	return -ESPIPE;
}

static unsigned int scull_poll(struct file * filp, poll_table * wait)
{
	return 0;
}

static int scull_fasync(int fd, struct file * filp, int on)
{
	return 0;
}

/*
 * The ioctl() implementation
 */

int scull_ioctl (struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	return 0;
}


/*
 * Operaciones necesarias para un tty device
 */
struct file_operations scull_fops = {
	scull_lseek,
	scull_read,
	scull_write,
	NULL,			/* scull_readdir */
	scull_poll,		/* scull_select */
	scull_ioctl,
	NULL,			/* scull_mmap */
	scull_open,
	NULL,			/* flush */
	scull_release,
	NULL,			/* fsync */
	scull_fasync /* nothing more, fill with NULLs */
};


/*
 * Finally, the module stuff
 */

int init_module(void)
{
	int result, i;

	/*
	* Register your major, and accept a dynamic number
	*/
	result = register_chrdev(scull_major, "scull", &scull_fops);
	if (result < 0) {
		printk(KERN_WARNING "scull: can't get major %d\n",scull_major);
		return result;
	}
	if (scull_major == 0) scull_major = result; /* dynamic */

	/* 
	 * allocate the devices -- we can't have them static, as the number
	 * can be specified at load time
	 */
	scull_devices = kmalloc(scull_nr_devs * sizeof (Scull_Dev), GFP_KERNEL);
	if (!scull_devices) {
		result = -ENOMEM;
		goto fail_malloc;
	}
	memset(scull_devices, 0, scull_nr_devs * sizeof (Scull_Dev));
	for (i=0; i < scull_nr_devs; i++) {
		scull_devices[i].quantum = scull_quantum;
		scull_devices[i].qset = scull_qset;
	}

	proc_register(&proc_root, &scull_proc_entry);
	return 0; /* succeed */

fail_malloc:
	unregister_chrdev(scull_major, "scull");
	return result;
}

void cleanup_module(void)
{
	unregister_chrdev(scull_major, "scull");

	proc_unregister(&proc_root, scull_proc_entry.low_ino);

	kfree(scull_devices);
	/* and call the cleanup functions for friend devices */
}
