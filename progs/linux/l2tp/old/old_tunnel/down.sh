#!/bin/sh

ifconfig l2tp0 down
rmmod l2tp

mount  -o remount,rw /
mount  -o remount,rw /home
mount  -o remount,rw /opt
