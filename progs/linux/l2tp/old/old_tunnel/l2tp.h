/* 
 * l2tp kernel driver
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/*
 * Heavily based on freeswan & new_tunnel.c
 */

struct l2tp_header {
	__u8	data[8];
};
#define L2TP_HDRLEN sizeof(struct l2tp_header)

#define ETH_P_L2TPOE_DISCOVERY	0x8866
#define ETH_P_L2TPOE_SESSION	0x8867

struct l2tptunnelconf
{
	__u32	cf_cmd;
	union
	{
		char 	cfu_name[12];
	} cf_u;
#define cf_name cf_u.cfu_name
};

#define L2TP_SET_DEV	(SIOCDEVPRIVATE)
#define L2TP_DEL_DEV	(SIOCDEVPRIVATE + 1)
#define L2TP_CLR_DEV	(SIOCDEVPRIVATE + 2)

#ifdef __KERNEL__
struct l2tppriv {
	struct sk_buff_head sendq;
	struct device *dev;
	struct wait_queue *wait_queue;
	char locked;
	int  (*hard_start_xmit) (struct sk_buff *skb,
		struct device *dev);
	int  (*hard_header) (struct sk_buff *skb,
		struct device *dev,
		unsigned short type,
		void *daddr,
		void *saddr,
		unsigned len);
	int  (*rebuild_header)(struct sk_buff *skb);
	int  (*set_mac_address)(struct device *dev, void *addr);
	void (*header_cache_update)(struct hh_cache *hh, struct device *dev, unsigned char *  haddr);
	struct enet_statistics *(*get_stats)(struct device *dev);
	struct enet_statistics mystats;
	int mtu;	/* What is the desired MTU? */
};
#endif /* __KERNEL__ */

#define L2TP_NUM_IF	1
