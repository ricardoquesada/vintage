/* 
 * l2tp kernel driver
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/*
 * Heavily based on freeswan
 * Other ideas were stolen from babylon pppoe, af_ipx.c, and others
 */

#define MODULE
#ifndef __KERNEL__
#define __KERNEL__
#endif

#include <linux/module.h>
#include <linux/config.h>	/* for CONFIG_IP_FORWARD */
#include <linux/version.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/malloc.h>	/* kmalloc() */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/interrupt.h>	/* mark_bh */

#include <linux/netdevice.h>	/* struct device, and other headers */
#include <linux/etherdevice.h>	/* eth_type_trans */
#include <linux/ip.h>		/* struct iphdr */
#include <linux/in.h>		/* struct sockaddr_in */
#include <linux/skbuff.h>
#include <linux/rtnetlink.h>
#include <linux/if_arp.h>
#include <linux/module.h>
#include <net/arp.h>

#include "l2tp.h"

/*
 * Esta funcion es llamada solo en el 1er paquete.
 * Por que ? Bueno, quizas aca mande el 1er paquete 0x8866 para saber
 * la MAC del modem
 */
static int
l2tp_tunnel_hard_header(struct sk_buff *skb, struct device *dev,
	unsigned short type, void *daddr, void *saddr, unsigned len)
{
	printk( KERN_INFO "l2tp_tunnel_hard_header()\n");

	skb_push(skb,ETH_HLEN + L2TP_HDRLEN );

	memcpy(skb->mac.ethernet->h_source,dev->dev_addr,dev->addr_len);
	memcpy(skb->mac.ethernet->h_dest,dev->dev_addr,dev->addr_len);
	skb->mac.ethernet->h_proto = htons(ETH_P_L2TPOE_DISCOVERY);


	return 0;
}

static int
l2tp_tunnel_rebuild_header(struct sk_buff *skb)
{
	printk( KERN_INFO "l2tp_tunnel_rebuild_header()\n");

	return 0;
}

static int
l2tp_tunnel_set_mac_address(struct device *dev, void *addr)
{
	printk( KERN_INFO "l2tp_tunnel_set_mac_address\n");
	return 0;
}

/*
 * We call the attach routine to attach another device.
 */

static int l2tp_tunnel_open(struct device *dev)
{
	printk("l2tp_tunnel_open\n");
	MOD_INC_USE_COUNT;
	return 0;
}

static int l2tp_tunnel_close(struct device *dev)
{
	printk("l2tp_tunnel_close\n");
	MOD_DEC_USE_COUNT;
	return 0;
}

/*
 *	This function assumes it is being called from dev_queue_xmit()
 *	and that skb is filled properly by that function.
 */

static int l2tp_tunnel_start_xmit(struct sk_buff *skb, struct device *dev)
{
	struct l2tppriv *prv;		/* Our device' private space */
	struct enet_statistics *stats;	/* This device's statistics */
	int ret=0;

	printk( KERN_INFO "l2tp_tunnel_start_xmit\n");

	/*
	 *	Return if there is nothing to do.  (Does this ever happen?) XXX
	 */
	if (skb == NULL) {
		printk( KERN_INFO
			    KERN_INFO "tunnel: "
			    "Nothing to do!\n" );
		goto cleanup;
	}
	if (dev == NULL) {
		printk( KERN_INFO
			    KERN_INFO "tunnel: "
			    "No device associated with skb!\n" );
		goto cleanup;
	}

	prv = dev->priv;
	if (prv == NULL) {
		printk( KERN_INFO
			    KERN_INFO "tunnel: "
			    "Device has no private structure!\n" );
		goto cleanup;
	}

	if ( prv->dev == NULL) {
		printk( KERN_INFO
			    KERN_INFO "tunnel: "
			    "Device is not attached to physical device!\n" );
		goto cleanup;
	}


	/* fill the mac */
	memcpy(skb->mac.ethernet->h_source,dev->dev_addr,dev->addr_len);
	memcpy(skb->mac.ethernet->h_dest,dev->dev_addr,dev->addr_len);
	skb->mac.ethernet->h_proto = htons(ETH_P_L2TPOE_DISCOVERY);

	stats = (struct enet_statistics *) &(prv->mystats);

	stats->tx_bytes += skb->len;
	stats->tx_packets++;

	skb->dev = prv->dev;
	ret = dev_queue_xmit( skb );

 cleanup:
	dev->tbusy = 0;

	if(skb)
		dev_kfree_skb(skb);

	return ret;
}


static int
l2tp_tunnel_attach(struct device *tndev, struct l2tppriv *prv, struct device *dev)
{
        int i;

	printk( KERN_INFO "l2tp_tunnel_attach\n");

	prv->dev = dev;
	prv->hard_start_xmit = dev->hard_start_xmit;
	prv->get_stats = dev->get_stats;

	tndev->hard_header_len = dev->hard_header_len + L2TP_HDRLEN;

	tndev->mtu = dev->mtu - L2TP_HDRLEN;
	prv->mtu = dev->mtu;

	tndev->type = dev->type; /* ARPHRD_TUNNEL */;	/* initially */

	tndev->addr_len = dev->addr_len;
	for (i=0; i<tndev->addr_len; i++) {
		tndev->dev_addr[i] = dev->dev_addr[i];
	}

	printk( "l2tp_tunnel_attach: "
		"physical device %s being attached has HW address: %2x",
	       dev->name, dev->dev_addr[0]);
	for (i=1; i < dev->addr_len; i++) {
		printk( ":%02x", dev->dev_addr[i]);
	}
	printk( KERN_INFO "\n");

	return 0;
}

/*
 * We call the detach routine to detach the l2tp tunnel from another device.
 */

static int l2tp_tunnel_detach(struct device *dev, struct l2tppriv *prv)
{
        int i;

	printk( KERN_INFO "l2tp_tunnel_dettach\n");

	printk( KERN_INFO
		"l2tp_tunnel_detach: "
		"physical device %s being detached from virtual device %s\n",
		prv->dev->name, dev->name);

	prv->dev = NULL;
	prv->hard_start_xmit = NULL;
	prv->get_stats = NULL;

	prv->hard_header = NULL;
	dev->hard_header = NULL;
	
	prv->rebuild_header = NULL;
	dev->rebuild_header = NULL;
	
	prv->set_mac_address = NULL;
	dev->set_mac_address = NULL;
	
	prv->header_cache_update = NULL;
	dev->header_cache_update = NULL;

	dev->neigh_setup        = NULL;
	dev->hard_header_len = 0;
	dev->mtu = 0;
	prv->mtu = 0;
	for (i=0; i<MAX_ADDR_LEN; i++) {
		dev->dev_addr[i] = 0;
	}
	dev->addr_len = 0;
	dev->type = 0;
	return 0;
}

/*
 * We call the clear routine to detach all l2tp tunnels from other devices.
 */
static int
l2tp_tunnel_clear(void)
{
	int i;
	struct device *l2tpdev = NULL, *prvdev;
	struct l2tppriv *prv;
	char name[9];
	int ret;

	printk( KERN_INFO "l2tp_tunnel_clear\n");

	for(i = 0; i < L2TP_NUM_IF; i++) {
		sprintf(name, "l2tp%d", i);
		if((l2tpdev = dev_get(name)) != NULL) {
			if((prv = (struct l2tppriv *)(l2tpdev->priv))) {
				prvdev = (struct device *)(prv->dev);
				if(prvdev) {
					printk( KERN_INFO
						"l2tp_tunnel_clear: "
						"physical device for device %s is %s\n",
						name, prvdev->name);
					if((ret = l2tp_tunnel_detach(l2tpdev, prv))) {
						printk( KERN_INFO
							"l2tp_tunnel_clear: "
							"error %d detatching device %s from device %s.\n",
							ret, name, prvdev->name);
						return ret;
					}
				}
			}
		}
	}
	return 0;
}

static int
l2tp_tunnel_ioctl(struct device *dev, struct ifreq *ifr, int cmd)
{
	struct l2tptunnelconf *cf = (struct l2tptunnelconf *)&ifr->ifr_data;
	struct l2tppriv *prv = dev->priv;
	struct device *them; /* physical device */
#ifdef CONFIG_IP_ALIAS
	char *colon;
	char realphysname[IFNAMSIZ];
#endif /* CONFIG_IP_ALIAS */
	
	printk( KERN_INFO "l2tp_tunnel_ioctl: tncfg service call #%d\n", cmd);

	switch (cmd) {
	/* attach a virtual l2tp? device to a physical device */
	case L2TP_SET_DEV:
#ifdef CONFIG_IP_ALIAS
		/* If this is an IP alias interface, get its real physical name */
		strncpy(realphysname, cf->cf_name, IFNAMSIZ);
		realphysname[IFNAMSIZ-1] = 0;
		colon = strchr(realphysname, ':');
		if (colon) *colon = 0;
		them = dev_get(realphysname);
#else /* CONFIG_IP_ALIAS */
		them = dev_get(cf->cf_name);
#endif /* CONFIG_IP_ALIAS */

		if (them == NULL) {
			printk( KERN_INFO
				"l2tp_tunnel_ioctl: "
				"physical device requested is null\n");

			return -ENXIO;
		}
		
		if (prv->dev)
			return -EBUSY;
		return l2tp_tunnel_attach(dev, dev->priv, them);

	case L2TP_DEL_DEV:
		if (! prv->dev)
			return -ENODEV;
		return l2tp_tunnel_detach(dev, dev->priv);
	       
	case L2TP_CLR_DEV:
		printk( KERN_INFO
			"l2tp_tunnel_ioctl: "
			"calling l2tp_tunnel_clear.\n");
		return l2tp_tunnel_clear();

	default:
		return -EOPNOTSUPP;
	}
}


static struct enet_statistics *
l2tp_tunnel_get_stats(struct device *dev)
{
	return &(((struct l2tppriv *)(dev->priv))->mystats);
}

/*
 *	Called when an l2tp tunnel device is initialized.
 *	The l2tp tunnel device structure is passed to us.
 */
 
int
l2tp_tunnel_init(struct device *dev)
{
	printk( KERN_INFO
		"l2tp_debug:l2tp_tunnel_init: "
		"initialisation of device: %s\n",
		dev->name ? dev->name : "NULL");

	ether_setup( dev );

	/* Add our tunnel functions to the device */
	dev->open		= l2tp_tunnel_open;
	dev->stop		= l2tp_tunnel_close;
	dev->hard_start_xmit	= l2tp_tunnel_start_xmit;
	dev->get_stats		= l2tp_tunnel_get_stats;

	dev->priv = kmalloc(sizeof(struct l2tppriv), GFP_KERNEL);
	if (dev->priv == NULL)
		return -ENOMEM;
	memset(dev->priv, 0, sizeof(struct l2tppriv));

	dev->set_multicast_list = NULL;
	dev->do_ioctl		= l2tp_tunnel_ioctl;
	dev->hard_header	= l2tp_tunnel_hard_header;
//	dev->rebuild_header	= l2tp_tunnel_rebuild_header;
//	dev->set_mac_address	= l2tp_tunnel_set_mac_address;
//	dev->header_cache_update= NULL;

//	dev->neigh_setup        = NULL;
//	dev->hard_header_len 	= 0;
//	dev->mtu		= 0;
//	dev->addr_len		= 0;
//	dev->type		= ARPHRD_ETHER; /* 0 */ /* ARPHRD_ETHER; */ /* initially */
	dev->tx_queue_len	= 10;		/* Small queue */
	memset(dev->broadcast,0xFF, ETH_ALEN);	/* what if this is not attached to ethernet? */

	/* New-style flags. */
	dev->flags		= IFF_NOARP | IFF_POINTOPOINT;
	dev_init_buffers(dev);

	/* We're done.  Have I forgotten anything? */
	return 0;
}

/*
 * receive
 */

int l2tpoe_rx_sesn(struct sk_buff *skb, struct device *dev, struct packet_type *pt)
{
	if (skb->len < 8) {
		printk("l2tpoe: dropping runt packet len=%d\n", skb->len);
		goto drop;
	}

	/* This packet is actually for us, so pass it into the l2tp stack */
	skb_pull(skb, 8);

	netif_rx( skb );
	return 0;

drop:
	dev_kfree_skb(skb);
	return 0;
}

static struct packet_type pt_l2tpoe_session = {
	0,
	NULL,		/* struct net_device *dev */
	l2tpoe_rx_sesn,	/* int (*func)(skb, dev, pt) */
	NULL,		/* void * data */
	NULL		/* next */
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*  Module specific interface                                      */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#ifdef MODULE

static int l2tp_tunnel_probe(struct device *dev)
{
	l2tp_tunnel_init(dev);
	return 0;
}

static struct device dev_l2tp_tunnel = 
{
	"l2tp0\0    ",		/* name */
	0,			/* recv memory end */
	0,			/* recv memory start */
	0,			/* memory end */
	0,			/* memory start */
 	0x0,			/* base I/O address */
	0,			/* IRQ */
	0, 0, 0,		/* flags */
	NULL,			/* next device */
	l2tp_tunnel_probe	/* setup */
};

int init_module(void)
{
	printk( KERN_INFO "l2tpoe v0.1.1\n");
	/* register 'receive' */
	pt_l2tpoe_session.type = htons(ETH_P_L2TPOE_SESSION);
	dev_add_pack(&pt_l2tpoe_session);

	/* register 'transmit' */
	if (register_netdev(&dev_l2tp_tunnel) != 0)
		return -EIO;
	return 0;
}
void cleanup_module(void)
{
	dev_remove_pack(&pt_l2tpoe_session);
	unregister_netdev(&dev_l2tp_tunnel);
	kfree_s(dev_l2tp_tunnel.priv,sizeof(struct l2tppriv));
	dev_l2tp_tunnel.priv=NULL;
}
#endif /* MODULE */

