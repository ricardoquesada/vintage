#ifndef PPPOE_H
#define PPPOE_H	1
#include <stdio.h>		/* stdio               */
#include <stdlib.h>		/* strtoul(), realloc() */
#include <unistd.h>		/* STDIN_FILENO,exec    */
#include <string.h>		/* memcpy()             */
#include <errno.h>		/* errno                */
#include <signal.h>
#include <getopt.h>
#include <stdarg.h>
#include <syslog.h>
#include <paths.h>

#include <sys/types.h>		/* socket types         */
#include <asm/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>		/* ioctl()              */
#include <sys/select.h>
#include <sys/socket.h>		/* socket()             */
#include <net/if.h>		/* ifreq struct         */
#include <net/if_arp.h>
#include <netinet/in.h>

#if __GLIBC__ >= 2 && __GLIBC_MINOR >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>   
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#endif

#ifdef KERNEL2_3
#include <linux/if_pppoe.h>
#if POE_VERSION != 1
#warning "User utils may be outdated"
#endif
#else
#include <linux/pppoe.h>
#if POE_VERSION != 1
#warning "User utils may be outdated"
#endif
#endif


#define CONNECTED 1
#define DISCONNECTED 0

#ifndef _PATH_PPPD
#define _PATH_PPPD "/usr/sbin/pppd"
#endif

#ifndef LOG_PPPOE
#define LOG_PPPOE LOG_DAEMON
#endif


#define VERSION_MAJOR 0
#define VERSION_MINOR 4
#define VERSION_DATE 20000320

/* Bigger than the biggest ethernet packet we'll ever see, in bytes */
#define MAX_PACKET      2000

/* references: RFC 2516 */
/* ETHER_TYPE fields for PPPoE */

#define ETH_P_PPPOE_DISC 0x8863	/* discovery stage */
#define ETH_P_PPPOE_SESS 0x8864

/* ethernet broadcast address */
#define MAC_BCAST_ADDR "\xff\xff\xff\xff\xff\xff"

/* PPPoE packet; includes Ethernet headers and such */
struct pppoe_packet {
  struct ethhdr ethhdr;		/* ethernet header */
  unsigned int ver:4;		/* pppoe version */
  unsigned int type:4;		/* pppoe type */
  unsigned int code:8;		/* pppoe code CODE_* */
  unsigned int session:16;	/* session id */
  unsigned short length;	/* payload length */
  /* payload follows */
} __attribute__ ((packed));

/* maximum payload length */
#define MAX_PAYLOAD (1484 - sizeof(struct pppoe_packet))

/* PPPoE codes */
#define CODE_SESS 0x00		/* PPPoE session */
#define CODE_PADI 0x09		/* PPPoE Active Discovery Initiation */
#define CODE_PADO 0x07		/* PPPoE Active Discovery Offer */
#define CODE_PADR 0x19		/* PPPoE Active Discovery Request */
#define CODE_PADS 0x65		/* PPPoE Active Discovery Session-confirmation */
#define CODE_PADT 0xa7		/* PPPoE Active Discovery Terminate */

/* also need */
#define STATE_RUN (-1)

/* PPPoE tag; the payload is a sequence of these */
struct pppoe_tag {
  unsigned short type;		/* tag type TAG_* */
  unsigned short length;	/* tag length */
  /* payload follows */
} __attribute__ ((packed));

/* PPPoE tag types */
#define TAG_END_OF_LIST        0x0000
#define TAG_SERVICE_NAME       0x0101
#define TAG_AC_NAME            0x0102
#define TAG_HOST_UNIQ          0x0103
#define TAG_AC_COOKIE          0x0104
#define TAG_VENDOR_SPECIFIC    0x0105
#define TAG_RELAY_SESSION_ID   0x0110
#define TAG_SERVICE_NAME_ERROR 0x0201
#define TAG_AC_SYSTEM_ERROR    0x0202
#define TAG_GENERIC_ERROR      0x0203

/* Debug flags */
int DEB_DISC;
int DEB_DISC2;

#define MAX_FNAME 256

struct host_tag {
  struct pppoe_tag *ht;
  unsigned int id;
} __attribute__ ((packed));

struct filter {
  int num_restart;
  char fname[MAX_FNAME];
  char pppd[MAX_FNAME];
  int peermode;

  struct pppoe_tag *stag;
  struct pppoe_tag *ntag;
  struct pppoe_tag *ctag;
  struct pppoe_tag *htag;
} __attribute__ ((packed));

struct session {
  int pid;			/* process ID  */
  int state;			/* Current state */
  int line;			/* PPPox dev number */
  int sid;			/* session ID */
  char name[IFNAMSIZ];		/*dev name */
  char dmac[ETH_ALEN];		/*destination MAC */
  char smac[ETH_ALEN];		/*Source MAC */
  int tags_len;
  int PADR_ret;
  int PADI_ret;
  int opt_daemonize;
  int opt_debug;
  int detached;
  int np;
  struct pppoe_tag *tags;
  int log_to_fd;
  struct filter *filt;
} __attribute__ ((packed));

/*
   retransmit retries for the PADR and PADI packets
   during discovery
 */
int PADR_ret;
int PADI_ret;

int log_to_fd;
int ctrl_fd;
int opt_debug;
int opt_daemonize;

struct session ses_table[16];

int disc_sock, sess_sock, socks[3];	/* raw socket we use */



/* Functions exported from utils.c. */

     void poe_detach (struct session *) ; /* Detach from controlling tty */

     void poe_die (int) __attribute__ ((__noreturn__));	/* Cleanup and exit */
     void poe_create_pidfile (struct session *);

     void poe_log_packet (u_char *, int, char *, int);
				/* Format a packet and log it with syslog */
     void poe_print_string (char *, int, void (*)(void *, char *,...),
			void *);	/* Format a string for output */
     int poe_slprintf (char *, int, char *,...);	/* sprintf++ */
     int vpoe_slprintf (char *, int, char *, va_list);	/* vsprintf++ */
     size_t poe_strlcpy (char *, const char *, size_t);	/* safe strcpy */
     size_t poe_strlcat (char *, const char *, size_t);	/* safe strncpy */
     void poe_dbglog (struct session *,char *,...);	/* log a debug message */
     void poe_info (struct session *,char *,...);	/* log an informational message */
     void poe_notice (int ,char *,...);	/* log a notice-level message */
     void poe_warn (struct session *,char *,...);	/* log a warning message */
     void poe_error (struct session *,char *,...);	/* log an error message */
     void poe_fatal (struct session *,char *,...);	/* log an error message and die(1) */

/* exported by lib.c */
     int serv_discover (struct session *);
     int serv_request (struct session *);
     int create_padi (struct pppoe_packet **, struct session *);
     int send_packet (struct session *,int, struct pppoe_packet *, int, const char *);
     int wait_for_packet (struct session *,int *, int, struct pppoe_packet **, int *, int, int ,struct pppoe_packet  *);
     struct pppoe_tag *find_tag (struct session *,int, struct pppoe_tag *, unsigned short);
     int create_padr (struct pppoe_packet **, struct session *);
     int create_raw_socket (struct session *,unsigned short);
     int get_hw_addr (struct session *,int);
     int open_device (struct session *);
     void open_inet_sk (struct session *);
     int ifindex (struct session *);
     int cntrl (struct session *);
     int create_raw_socket (struct session *,unsigned short);

     int init_ses (struct session *);
     void cleanup_ses (struct session *);
     int ses_connect (struct session *);
     void print_session_tags (struct session *);
     int ppp_connect (struct session *);



#endif
