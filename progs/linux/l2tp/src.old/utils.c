/*	$Id$	*/
/* adapted for l2tpoed by riq@core-sdi.com	(may 2000)*/
/*
 * utils.c - various utility functions used in pppoed.
 *
 * mostly stolen from ppp-2.3.10 by Marc Boucher <marc@mbsi.ca>
 *
 * Copyright (c) 1999 The Australian National University.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright poe_notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the Australian National University.  The name of the University
 * may not be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>		/* stdio */
#include <stdlib.h>		/* strtoul(), realloc() */
#include <string.h>		/* memcpy() */
#include <unistd.h>		/* STDIN_FILENO,exec */
#include <errno.h>		/* errno */

#include <sys/time.h>

#include <net/ethernet.h>
#include <netinet/in.h>

#include <stdarg.h>
#include <ctype.h>
#include <syslog.h>
#include <limits.h>
#include <paths.h>

#include "l2tpoed.h"

static char pidfilename[PATH_MAX];	/* name of pid file */

/*
 * Create a file containing our process ID.
 */
void create_pidfile (struct session *ses)
{
	FILE *pidfile;

	sprintf (pidfilename, "%s%s.pid", _PATH_VARRUN, "l2tpoed");
	if ((pidfile = fopen (pidfilename, "w")) != NULL) {
		fprintf (pidfile, "%d\n", getpid ());
		(void) fclose (pidfile);
	} else {
		PDEBUG("Failed to create pid file %s: %m", pidfilename);
		pidfilename[0] = 0;
	}
}

/*
 * detach - detach us from the controlling terminal.
 */
void
poe_detach (struct session *ses)
{
	if (ses->detached)
		return;

	if ((daemon (0, 0)) < 0) {
		PDEBUG("Couldn't detach (daemon failed: %m)");
#if 0
		poe_die (1);			/* or just return? */
#endif
	}
	ses->detached = 1;
	ses->log_to_fd = -1;
	/* update pid files if they have been written already */
	if (pidfilename[0])
		poe_create_pidfile (ses);
}

/*
 * cleanup - restore anything which needs to be restored before we exit
 */
/* ARGSUSED */
static void cleanup ()
{
	if (pidfilename[0] != 0 && unlink (pidfilename) < 0 && errno != ENOENT)
		syslog (LOG_INFO,"unable to delete pid file ");
	pidfilename[0] = 0;
}

/*
 * poe_die - clean up state and exit with the specified status.
 */
void poe_die (int status)
{
	cleanup ();
	syslog (LOG_INFO, "Exit.");
	exit (status);
}
