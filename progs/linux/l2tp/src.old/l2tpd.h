/*
 * l2tpd header
 */
#ifndef __L2TPOE_H
#define __L2TPOE_H

#include <stdio.h>		/* stdio */
#include <stdlib.h>		/* strtoul(), realloc() */
#include <unistd.h>		/* STDIN_FILENO,exec */
#include <string.h>		/* memcpy() */
#include <errno.h>		/* errno */
#include <signal.h>
#include <getopt.h>
#include <stdarg.h>
#include <syslog.h>
#include <paths.h>

#include <sys/types.h>		/* socket types	*/
#include <asm/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>		/* ioctl() */
#include <sys/select.h>
#include <sys/socket.h>		/* socket() */
#include <net/if.h>		/* ifreq struct	*/
#include <net/if_arp.h>
#include <netinet/in.h>


#ifdef DEBUG_PDEBUG
#define PDEBUG(a...) printf(a)
#else
#define PDEBUG(a...)
#endif

#if __GLIBC__ >= 2 && __GLIBC_MINOR >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>		
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#endif


#define CONNECTED 1
#define DISCONNECTED 0

#ifndef LOG_L2TPOE
#define LOG_L2TPOE LOG_DAEMON
#endif


#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_DATE 20000320

/* Bigger than the biggest ethernet packet we'll ever see, in bytes */
#define MAX_PACKET			2000

/* references: RFC xxxx (si no esisto, lo invetamo nosotro' */
/* ETHER_TYPE fields for L2TPoE */

#define ETH_P_L2TPOE_DISC 0x8866	/* discovery stage */
#define ETH_P_L2TPOE_SESS 0x8867

/* ethernet broadcast address */
#define MAC_BCAST_ADDR "\xff\xff\xff\xff\xff\xff"

/* L2TPoE packet; includes Ethernet headers and such */

struct l2tpoe_packet {
	struct ethhdr ethhdr;		/* ethernet header */
	unsigned int ver:4;		/* pppoe version */
	unsigned int type:4;		/* pppoe type */
	unsigned int code:8;		/* pppoe code CODE_* */
	unsigned int session:16;	/* session id */
	unsigned short length;		/* payload length */
	/* payload follows */
} __attribute__ ((packed));

struct real_l2tpoe_packet {
	struct ethhdr ethhdr;		/* ethernet header */

	unsigned short l_type:1;	/* type bit */
	unsigned short l_len:1;		/* len bit */
	unsigned short l_resA:2;	/* reserved */
	unsigned short l_seq:1;		/* sequence */
	unsigned short l_resB:1;	/* reserved */
	unsigned short l_off:1;		/* offset */
	unsigned short l_pri:1;		/* priority */
	unsigned short l_resC:3;	/* reserved */
	unsigned short l_ver:4;		/* version */

	unsigned short l_lenght;	/* length value */
	unsigned short l_tunid;		/* tunnel id*/
	unsigned short l_sessid;	/* session id*/
	unsigned short l_ns;		/* sent */
	unsigned short l_nr;		/* receive */

	/* payload follows */
} __attribute__ ((packed));

/* maximum payload length */
#define MAX_PAYLOAD (1484 - sizeof(struct l2tpoe_packet))

/* L2TPoE codes */
#define CODE_SESS 0x00		/* L2TPoE session */
#define CODE_PADI 0x09		/* L2TPoE Active Discovery Initiation */
#define CODE_PADO 0x07		/* L2TPoE Active Discovery Offer */
#define CODE_PADR 0x19		/* L2TPoE Active Discovery Request */
#define CODE_PADS 0x65		/* L2TPoE Active Discovery Session-confirmation */
#define CODE_PADT 0xa7		/* L2TPoE Active Discovery Terminate */

/* also need */
#define STATE_RUN (-1)

/* L2TPoE tag; the payload is a sequence of these */
struct l2tpoe_tag {
	unsigned short type;		/* tag type TAG_* */
	unsigned short length;		/* tag length */
	/* payload follows */
} __attribute__ ((packed));

/* L2TPoE tag types */
#define TAG_END_OF_LIST			0x0000
#define TAG_SERVICE_NAME		0x0101
#define TAG_AC_NAME			0x0102
#define TAG_HOST_UNIQ			0x0103
#define TAG_AC_COOKIE			0x0104
#define TAG_VENDOR_SPECIFIC		0x0105
#define TAG_RELAY_SESSION_ID	 	0x0110
#define TAG_SERVICE_NAME_ERROR		0x0201
#define TAG_AC_SYSTEM_ERROR		0x0202
#define TAG_GENERIC_ERROR		0x0203

/* Debug flags */
extern int DEB_DISC;
extern int DEB_DISC2;

#define MAX_FNAME 256
struct host_tag {
	struct l2tpoe_tag *ht;
	unsigned int id;
} __attribute__ ((packed));

struct filter {
	int num_restart;
	char fname[MAX_FNAME];
	char l2ptd[MAX_FNAME];
	int peermode;

	struct l2tpoe_tag *stag;
	struct l2tpoe_tag *ntag;
	struct l2tpoe_tag *ctag;
	struct l2tpoe_tag *htag;
} __attribute__ ((packed));

struct session {
	int pid;			/* process ID  */
	int state;			/* Current state */
	int line;			/* L2TPox dev number */
	int sid;			/* session ID */
	char name[IFNAMSIZ];		/* dev name */
	char dmac[ETH_ALEN];		/* destination MAC */
	char smac[ETH_ALEN];		/* Source MAC */
	int tags_len;
	int PADR_ret;
	int PADI_ret;
	int opt_daemonize;
	int opt_debug;
	int detached;
	int np;
	struct l2tpoe_tag *tags;
	int log_to_fd;
	struct filter *filt;
} __attribute__ ((packed));

/*
 * retransmit retries for the PADR and PADI packets
 * during discovery
 */
int PADR_ret;
int PADI_ret;

int log_to_fd;
int ctrl_fd;
int opt_debug;
int opt_daemonize;

struct session ses_table[16];

int disc_sock, sess_sock, socks[3];	/* raw socket we use */

/*
 * Declaracion de funciones
 */
int filter_tags (struct session *ses);
int ses_connect (struct session *ses);

#endif /* __L2TPOE_H */
