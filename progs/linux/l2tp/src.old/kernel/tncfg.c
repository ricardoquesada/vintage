/*
 * l2tp interface configuration
 * Copyright (C) 2000 Ricardo Quesada
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
/*
 * Heavly based on tncfg from freeswan
 */

char tncfg_c_version[] = "RCSID $Id$";

#include <stdio.h>
#include <string.h>
#include <stdlib.h> /* system(), strtoul() */
#include <unistd.h> /* getuid() */
#include <linux/types.h>
#include <sys/ioctl.h> /* ioctl() */

#include "l2tp.h"

#include <net/if.h>
#include <sys/types.h>
#include <errno.h>
#include <getopt.h>

static void
usage(char *name)
{	
	fprintf(stdout,"%s --attach --virtual <virtual-device> --physical <real-device>\n",
		name);
	fprintf(stdout,"%s --detach --virtual <virtual-device>\n",
		name);
	fprintf(stdout,"%s --clear\n",
		name);
	fprintf(stdout,"%s --help\n",
		name);
	fprintf(stdout,"%s --version\n",
		name);
	exit(1);
}

static struct option const longopts[] =
{
	{"virtual", 1, 0, 'V'},
	{"physical", 1, 0, 'P'},
	{"attach", 0, 0, 'a'},
	{"detach", 0, 0, 'd'},
	{"clear", 0, 0, 'c'},
	{"help", 0, 0, 'h'},
	{"version", 0, 0, 'v'},
	{"label", 1, 0, 'l'},
	{"debug", 0, 0, 'g'},
	{0, 0, 0, 0}
};

int
main(int argc, char *argv[])
{
	struct ifreq ifr;
	struct l2tptunnelconf *shc=(struct l2tptunnelconf *)&ifr.ifr_data;
	int s;
	int c, previous = -1;
	char *program_name;
	int debug = 0;
     
	if(argc == 1) {
		system("cat /proc/net/l2tp_tncfg");
		exit(0);
	}

	memset(&ifr, 0, sizeof(ifr));
	program_name = argv[0];

	while((c = getopt_long_only(argc, argv, "adchvV:P:l:+:", longopts, 0)) != EOF) {
		switch(c) {
		case 'g':
			debug = 1;
			break;
		case 'a':
			if(shc->cf_cmd) {
				fprintf(stderr, "%s: exactly one of '--attach', '--detach' or '--clear' options must be specified.\n",	program_name);
				exit(1);
			}
			shc->cf_cmd = L2TP_SET_DEV;
			break;
		case 'd':
			if(shc->cf_cmd) {
				fprintf(stderr, "%s: exactly one of '--attach', '--detach' or '--clear' options must be specified.\n",	program_name);
				exit(1);
			}
			shc->cf_cmd = L2TP_DEL_DEV;
			break;
		case 'c':
			if(shc->cf_cmd) {
				fprintf(stderr, "%s: exactly one of '--attach', '--detach' or '--clear' options must be specified.\n",	program_name);
				exit(1);
			}
			shc->cf_cmd = L2TP_CLR_DEV;
			break;
		case 'h':
			usage(program_name);
			break;
		case 'v':
			if(optarg) {
				fprintf(stderr, "%s: warning; '-v' and '--version' options don't expect arguments, arg '%s' found, perhaps unintended.\n",
					program_name, optarg);
			}
			printf("%s, %s\n", program_name, tncfg_c_version);
			exit(1);
			break;
		case 'V':
			strcpy(ifr.ifr_name, optarg);
			break;
		case 'P':
			strcpy(shc->cf_name, optarg);
			break;
		case 'l':
			program_name = optarg;
			break;
		default:
			usage(program_name);
			break;
		}
		previous = c;
	}

	switch(shc->cf_cmd) {
	case L2TP_SET_DEV:
		if(!shc->cf_name) {
			fprintf(stderr, "%s: physical I/F parameter missing.\n",
				program_name);
			exit(1);
		}
	case L2TP_DEL_DEV:
		if(!ifr.ifr_name) {
			fprintf(stderr, "%s: virtual I/F parameter missing.\n",
				program_name);
			exit(1);
		}
		break;
	case L2TP_CLR_DEV:
		strcpy(ifr.ifr_name, "l2tp0");
		break;
	default:
		fprintf(stderr, "%s: exactly one of '--attach', '--detach' or '--clear' options must be specified.\n"
			"Try %s --help' for usage information.\n",
			program_name, program_name);
		exit(1);
	}

	s=socket(AF_INET, SOCK_DGRAM,0);
	if(s==-1)
	{
		printf("%s: Socket creation failed -- ", program_name);
		switch(errno)
		{
		case EACCES:
			if(getuid()==0)
				printf("Root denied permission!?!\n");
			else
				printf("Run as root user.\n");
			break;
		case EPROTONOSUPPORT:
			printf("Internet Protocol not enabled");
			break;
		case EMFILE:
		case ENFILE:
		case ENOBUFS:
			printf("Insufficient system resources.\n");
			break;
		case ENODEV:
			printf("No such device.  Is the virtual device valid?  Is the l2tp module linked into the kernel or loaded as a module?\n");
			break;
		default:
			printf("Unknown socket error %d.\n", errno);
		}
		exit(1);
	}
	if(ioctl(s, shc->cf_cmd, &ifr)==-1)
	{
		if(shc->cf_cmd == L2TP_SET_DEV) {
			printf("%s: Socket ioctl failed on attach -- ", program_name);
			switch(errno)
			{
			case EINVAL:
				printf("Invalid argument, check kernel log messages for specifics.\n");
				break;
			case ENODEV:
				printf("No such device.  Is the virtual device valid?  Is the l2tp module linked into the kernel or loaded as a module?\n");
				break;
			case ENXIO:
				printf("No such device.  Is the physical device valid?\n");
				break;
			case EBUSY:
				printf("Device busy.  Virtual device %s is already attached to a physical device -- Use detach first.\n",
				       ifr.ifr_name);
				break;
			default:
				printf("Unknown socket error %d.\n", errno);
			}
			exit(1);
		}
		if(shc->cf_cmd == L2TP_DEL_DEV) {
			printf("%s: Socket ioctl failed on detach -- ", program_name);
			switch(errno)
			{
			case EINVAL:
				printf("Invalid argument, check kernel log messages for specifics.\n");
				break;
			case ENODEV:
				printf("No such device.  Is the virtual device valid?  The l2tp module may not be linked into the kernel or loaded as a module.\n");
				break;
			case ENXIO:
				printf("Device requested is not linked to any physical device.\n");
				break;
			default:
				printf("Unknown socket error %d.\n", errno);
			}
			exit(1);
		}
		if(shc->cf_cmd == L2TP_CLR_DEV) {
			printf("%s: Socket ioctl failed on clear -- ", program_name);
			switch(errno)
			{
			case EINVAL:
				printf("Invalid argument, check kernel log messages for specifics.\n");
				break;
			case ENODEV:
				printf("Failed.  Is the l2tp module linked into the kernel or loaded as a module?.\n");
				break;
			default:
				printf("Unknown socket error %d.\n", errno);
			}
			exit(1);
		}
	}
	exit(0);
}
