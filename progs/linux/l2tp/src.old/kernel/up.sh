#!/bin/sh

mount -o remount,ro /
mount -o remount,ro /home
mount -o remount,ro /opt

ifconfig l2tp0 down
rmmod l2tp
insmod l2tp.o
ifconfig l2tp0 up
./tncfg -a -V l2tp0 -P eth0
ifconfig l2tp0 19.168.66.5 pointopoint 19.168.66.32
ifconfig l2tp0

