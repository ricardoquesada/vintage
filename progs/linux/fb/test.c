/*
 * Test de fb para swain
 * riq.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <linux/fb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <asm/io.h>

#include "logo.h"

struct fb_fix_screeninfo fb_fix;
struct fb_var_screeninfo fb_var_orig;
struct fb_var_screeninfo fb_var;
unsigned char* fb_mem;
volatile unsigned char *m_mmioaddr;
int fd;

int len=0;

void set_xy( int x, int y, unsigned char c)
{
	int j;

	j =  len * y + x;
	memset((void*)fb_mem+j,c,1);
}

int init_fb( void )
{
	
	fd = open("/dev/fb0", O_RDWR);
	if (fd<0) {
		printf("FB dev: Couldn't open /dev/fb0, you must have read-write access!\n");
		return -1;
	}
	printf("OK\n");
	return 0;
}

int close_fb( void )
{
	close(fd);
	return 0;
}


void res_videomode( void )
{
	ioctl(fd,FBIOPUT_VSCREENINFO,&fb_var_orig);
	munmap(fb_mem,fb_fix.smem_len);
}
void set_videomode( int width, int height, int bpp)
{
	// fetch initial settings
	ioctl( fd, FBIOGET_VSCREENINFO, &fb_var_orig );
	ioctl( fd, FBIOGET_FSCREENINFO, &fb_fix );
	fb_var = fb_var_orig;


	// try to set resolution
	fb_var.activate = FB_ACTIVATE_NOW;
	fb_var.accel_flags = 0;
	fb_var.xres = width; 
	fb_var.yres = height;
	fb_var.xres_virtual = width;
	fb_var.yres_virtual = height;
	fb_var.bits_per_pixel = bpp;
	if (-1 == ioctl(fd,FBIOPUT_VSCREENINFO,&fb_var))
	{
		fb_var.yres_virtual = height;
		if (-1 == ioctl(fd,FBIOPUT_VSCREENINFO,&fb_var))
		{
			// Damn: Could not set proper video mode.
			fb_var = fb_var_orig;
			
			// Test if we can use the current one (is it big enough?)
			if ((int)fb_var.xres < width || (int)fb_var.yres < height) {
				printf("FB dev: Couldn't set proper video mode\n");
				return;
			}
		}
	} else {
		printf("Using doublebuffer mode (panning)\n");
	}

	/* move viewport to upper left corner */
	fb_var.xoffset = 0;
	fb_var.yoffset = 0;
	ioctl(fd,FBIOPAN_DISPLAY,&fb_var);

	printf("fix.smem_len=%d 0x%x\n",fb_fix.smem_len,fb_fix.smem_len);

	// map framebuffer and get the address
	if ( MAP_FAILED == (fb_mem = (unsigned char *) mmap(	NULL, 
						fb_fix.smem_len, 
						PROT_READ | PROT_WRITE, 
						MAP_SHARED,
						fd, 0 )) )
	{
		printf("FB dev: Couldn't mmap framebuffer\n");
	}
	

	m_mmioaddr = (unsigned char *) mmap(NULL, fb_fix.mmio_len,
					PROT_READ | PROT_WRITE, MAP_SHARED,
					fd, fb_fix.smem_len);
	printf("io: 0x%x\n",m_mmioaddr);
	m_mmioaddr = NULL;

 	/* clear out the framebuffer */
	{
		int x;
		int y;
		int i;
		unsigned char c;
		
		i=0;
		for(y=0;y<80;y++) {
			for(x=0;x<80;x++) {
				c = linux_logo16[i++];
				set_xy(x,y,c);
			}
		}
	}
}

void scroll_video( void )
{
	int i;

	for(i=0;i<600;i++) {
		fb_var.yoffset = i;
		ioctl(fd,FBIOPAN_DISPLAY,&fb_var);
		usleep(1);
	}
}

int main( int argc, char **argv )
{
	int j;

	if(argc != 3 ) {
		printf("%s: x y\n",argv[0]);
		return -1;
	}

	len = atoi(argv[1]);
	j = atoi(argv[2]);

	init_fb();
	set_videomode(len,j,8);
//	scroll_video();
	sleep(3);
	res_videomode();
	close_fb();
	return 0;
}
