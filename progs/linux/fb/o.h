#ifndef __O__
#define __O__

#include <asm/io.h>

static __inline__ void port_out(int value, int port)
{
    __asm__ volatile ("outb %0,%1"
	      ::"a" ((unsigned char) value), "d"((unsigned short) port));
}

static __inline__ void port_outw(int value, int port)
{
    __asm__ volatile ("outw %0,%1"
	     ::"a" ((unsigned short) value), "d"((unsigned short) port));
}

static __inline__ void port_outl(int value, int port)
{
    __asm__ volatile ("outl %0,%1"
             ::"a" ((unsigned long)value), "d" ((unsigned short) port));
}

static __inline__ int port_in(int port)
{
    unsigned char value;
    __asm__ volatile ("inb %1,%0"
		      :"=a" (value)
		      :"d"((unsigned short) port));
    return value;
}

static __inline__ int port_inw(int port)
{
    unsigned short value;
    __asm__ volatile ("inw %1,%0"
		      :"=a" (value)
		      :"d"((unsigned short) port));
    return value;
}

static __inline__ int port_inl(int port)
{
    unsigned int value;
    __asm__ volatile("inl %1,%0" :
               	     "=a" (value) :
                     "d" ((unsigned short)port));
    return value;
}

#define gr_readb(off)		(((volatile unsigned char *)GM)[(off)])
#define gr_readw(off)		(*(volatile unsigned short*)((GM)+(off)))
#define gr_readl(off)		(*(volatile unsigned long*)((GM)+(off)))
#define gr_writeb(v,off)	(GM[(off)] = (v))
#define gr_writew(v,off)	(*(unsigned short*)((GM)+(off)) = (v))
#define gr_writel(v,off)	(*(unsigned long*)((GM)+(off)) = (v))


/* Note that the arguments of outb/w are reversed compared with the */
/* kernel sources. The XFree86 drivers also use this format. */
#undef inb
#undef inw
#undef inl
#undef outb
#undef outw
#undef outl

#define inb port_in
#define inw port_inw
#define inl port_inl
#define outb(value, port) port_out(value, port)
#define outw(value, port) port_outw(value, port)
#define outl(value, port) port_outl(value, port)

#endif	/* __O__ */
