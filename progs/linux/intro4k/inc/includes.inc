; Copyright (C) 1999-2001 Konstantin Boldyshev <konst@linuxassembly.org>
;
; $Id: includes.inc,v 1.3 2001/01/21 15:18:46 konst Exp $
;
; file          : includes.inc
; created       : 04-Jul-1999
; modified      : 05-Jan-2001
; version       : 0.12
; assembler     : nasm 0.98
; description   : generic constants and structures from header files (libc)
; author        : Konstantin Boldyshev <konst@linuxassembly.org>
; comment       : included internally from system.inc

%ifndef	__SYSTEM_INC
%error "this file must be included internally from system.inc !"
%endif

%ifndef __INCLUDES_INC
%define __INCLUDES_INC

%assign	NULL	0

%assign TRUE	1
%assign FALSE	0

%assign	STDIN	0
%assign	STDOUT	1
%assign	STDERR	2

%assign	EOL	0

%assign	__a	0x07
%assign	__b	0x08
%assign	__e	0x1b
%assign	__f	0x0c
%assign	__n	0x0a
%assign	__r	0x0d
%assign	__t	0x09
%assign	__v	0x0b
%assign	__s	0x20

;
;datatypes
;

%define		INT		resd
%define		UINT		resd
%define		LONG		resd
%define		ULONG		resd
%define 	SHORT		resw
%define		USHORT		resw
%define		CHAR		resb
%define		UCHAR		resb
%define		BYTE		resb
%define		WORD		resw
%define		DWORD		resd
%define		U8		resb
%define		U16		resw
%define		U32		resd

%define		TIME_T		LONG
%define		MODE_T		UINT
%define		OFF_T		ULONG

%define		_INT		dd
%define		_UINT		dd
%define		_LONG		dd
%define		_ULONG		dd
%define 	_SHORT		dw
%define		_USHORT		dw
%define		_CHAR		db
%define		_UCHAR		db
%define		_BYTE		db
%define		_WORD		dw
%define		_DWORD		dd
%define		_U8		db
%define		_U16		dw
%define		_U32		dd

%define		_TIME_T		_LONG
%define		_MODE_T		_UINT
%define		_OFF_T		_ULONG


;
;please provide corresponding include file when adding new entries
;

;
;fcntl.h
;

%assign SEEK_SET	0
%assign SEEK_CUR	1
%assign SEEK_END	2

%assign O_RDONLY	0
%assign O_WRONLY	1
%assign O_RDWR		2
%assign O_ACCMODE	3
%assign O_CREAT		100q
%assign O_EXCL		200q
%assign O_NOCTTY	400q
%assign O_TRUNC		1000q
%assign O_APPEND	2000q
%assign O_NONBLOCK	4000q
%assign O_NDELAY	O_NONBLOCK
%assign O_SYNC		 10000q
%assign FASYNC		 20000q
%assign O_DIRECT	 40000q
%assign O_LARGEFILE	100000q
%assign O_DIRECTORY	200000q
%assign O_NOFOLLOW	400000q

%assign F_DUPFD		0	;Duplicate file descriptor
%assign F_GETFD		1	;Get file descriptor flags
%assign F_SETFD		2	;Set file descriptor flags
%assign F_GETFL		3	;Get file status flags
%assign F_SETFL		4	;Set file status flags
%assign F_GETLK		5	;Get record locking info
%assign F_SETLK		6	;Set record locking info (non-blocking)
%assign F_SETLKW	7	;Set record locking info (blocking)


;
;unistd.h
;

%assign	R_OK	4	; read
%assign W_OK	2	; write
%assign X_OK	1	; execute
%assign F_OK	0	; existence

;
;signal.h
;

%assign SIG_DFL		0
%assign SIG_IGN		1
%assign SIG_ERR		-1

%assign	SIGHUP		1	;Hangup (POSIX)
%assign	SIGINT		2	;Interrupt (ANSI)
%assign	SIGQUIT		3	;Quit (POSIX)
%assign	SIGILL		4	;Illegal instruction (ANSI)
%assign	SIGTRAP		5	;Trace trap (POSIX)
%assign	SIGABRT		6	;Abort (ANSI)
%assign	SIGIOT		6	;IOT trap (4.2 BSD)
%assign	SIGBUS		7	;BUS error (4.2 BSD)
%assign	SIGFPE		8	;Floating-point exception (ANSI)
%assign	SIGKILL		9	;Kill, unblockable (POSIX)
%assign	SIGUSR1		10	;User-defined signal 1 (POSIX)
%assign	SIGSEGV		11	;Segmentation violation (ANSI)
%assign	SIGUSR2		12	;User-defined signal 2 (POSIX)
%assign	SIGPIPE		13	;Broken pipe (POSIX)
%assign	SIGALRM		14	;Alarm clock (POSIX)
%assign	SIGTERM		15	;Termination (ANSI)
%assign	SIGSTKFLT	16	;Stack fault
%assign	SIGCHLD		17	;Child status has changed (POSIX)
%assign	SIGCLD		SIGCHLD	;Same as SIGCHLD (System V)
%assign	SIGCONT		18	;Continue (POSIX)
%assign	SIGSTOP		19	;Stop, unblockable (POSIX)
%assign	SIGTSTP		20	;Keyboard stop (POSIX)
%assign	SIGTTIN		21	;Background read from tty (POSIX)
%assign	SIGTTOU		22	;Background write to tty (POSIX)
%assign	SIGURG		23	;Urgent condition on socket (4.2 BSD)
%assign	SIGXCPU		24	;CPU limit exceeded (4.2 BSD)
%assign	SIGXFSZ		25	;File size limit exceeded (4.2 BSD)
%assign	SIGVTALRM	26	;Virtual alarm clock (4.2 BSD)
%assign	SIGPROF		27	;Profiling alarm clock (4.2 BSD)
%assign	SIGWINCH	28	;Window size change (4.3 BSD, Sun)
%assign	SIGIO		29	;I/O now possible (4.2 BSD)
%assign	SIGPOLL		SIGIO	;Pollable event occurred (System V)
%assign	SIGPWR		30	;Power failure restart (System V)
%assign SIGUNUSED	31
%assign	_NSIG		64	;Biggest signal number + 1

;
;limits.h
;

%assign	NAME_MAX	255
%assign	PATH_MAX	4095

;
;wait.h
;

%assign WNOHANG		1
%assign WUNTRACED	2

;
;sys/mount.h
;

%define MS_RDONLY	1
%define MS_MGC_VAL	0xc0ed0000

;
;sys/socket.h
;

%assign AF_UNSPEC	0
%assign AF_UNIX		1	; Unix domain sockets
%assign AF_LOCAL	1	; POSIX name for AF_UNIX
%assign AF_INET		2	; Internet IP Protocol
%assign AF_AX25		3	; Amateur Radio AX.25
%assign AF_IPX		4	; Novell IPX
%assign AF_APPLETALK	5	; AppleTalk DDP
%assign AF_NETROM	6	; Amateur Radio NET/ROM
%assign AF_BRIDGE	7	; Multiprotocol bridge
%assign AF_ATMPVC	8	; ATM PVCs
%assign AF_X25		9	; Reserved for X.25 project
%assign AF_INET6	10	; IP version 6
%assign AF_ROSE		11	; Amateur Radio X.25 PLP
%assign AF_DECnet	12	; Reserved for DECnet project
%assign AF_NETBEUI	13	; Reserved for 802.2LLC project
%assign AF_SECURITY	14	; Security callback pseudo AF
%assign AF_KEY		15	; PF_KEY key management API
%assign AF_NETLINK	16
%assign AF_ROUTE	AF_NETLINK 	; Alias to emulate 4.4BSD
%assign AF_PACKET	17	; Packet family
%assign AF_ASH		18	; Ash
%assign AF_ECONET	19	; Acorn Econet
%assign AF_ATMSVC	20	; ATM SVCs
%assign AF_SNA		22	; Linux SNA Project (nutters!)
%assign AF_IRDA		23	; IRDA sockets
%assign AF_MAX		32	; For now..

%assign PF_UNSPEC	AF_UNSPEC
%assign PF_UNIX		AF_UNIX
%assign PF_LOCAL	AF_LOCAL
%assign PF_INET		AF_INET
%assign PF_AX25		AF_AX25
%assign PF_IPX		AF_IPX
%assign PF_APPLETALK	AF_APPLETALK
%assign	PF_NETROM	AF_NETROM
%assign PF_BRIDGE	AF_BRIDGE
%assign PF_ATMPVC	AF_ATMPVC
%assign PF_X25		AF_X25
%assign PF_INET6	AF_INET6
%assign PF_ROSE		AF_ROSE
%assign PF_DECnet	AF_DECnet
%assign PF_NETBEUI	AF_NETBEUI
%assign PF_SECURITY	AF_SECURITY
%assign PF_KEY		AF_KEY
%assign PF_NETLINK	AF_NETLINK
%assign PF_ROUTE	AF_ROUTE
%assign PF_PACKET	AF_PACKET
%assign PF_ASH		AF_ASH
%assign PF_ECONET	AF_ECONET
%assign PF_ATMSVC	AF_ATMSVC
%assign PF_SNA		AF_SNA
%assign PF_IRDA		AF_IRDA
%assign PF_MAX		AF_MAX

%assign SOCK_STREAM    1	;stream (connection) socket
%assign SOCK_DGRAM     2	;datagram (conn.less) socket
%assign SOCK_RAW       3	;raw socket
%assign SOCK_RDM       4	;reliably-delivered message
%assign SOCK_SEQPACKET 5	;sequential packet socket
%assign SOCK_PACKET    10	;linux specific way of getting packets at the dev level

%assign IPPROTO_IP		0		; Dummy protocol for TCP
%assign IPPROTO_HOPOPTS		0		; IPv6 Hop-by-Hop options
%assign IPPROTO_ICMP		1		; Internet Control Message Protocol
%assign IPPROTO_IGMP		2		; Internet Group Management Protocol
%assign IPPROTO_IPIP		4		; IPIP tunnels (older KA9Q tunnels use 94)
%assign IPPROTO_TCP		6		; Transmission Control Protocol
%assign IPPROTO_EGP		8		; Exterior Gateway Protocol
%assign IPPROTO_PUP		12		; PUP protocol
%assign IPPROTO_UDP		17		; User Datagram Protocol
%assign IPPROTO_IDP		22		; XNS IDP protocol
%assign IPPROTO_TP		29		; SO Transport Protocol Class 4
%assign IPPROTO_IPV6		41		; IPv6 header
%assign IPPROTO_ROUTING		43		; IPv6 routing header
%assign IPPROTO_FRAGMENT 	44		; IPv6 fragmentation header
%assign IPPROTO_RSVP		46		; Reservation Protocol
%assign IPPROTO_GRE		47		; General Routing Encapsulation
%assign IPPROTO_ESP		50		; encapsulating security payload
%assign IPPROTO_AH		51		; authentication header
%assign IPPROTO_ICMPV6		58		; ICMPv6
%assign IPPROTO_NONE		59		; IPv6 no next header
%assign IPPROTO_DSTOPTS		60		; IPv6 destination options
%assign IPPROTO_MTP		92		; Multicast Transport Protocol
%assign IPPROTO_ENCAP		98		; Encapsulation Header
%assign IPPROTO_PIM		103		; Protocol Independent Multicast
%assign IPPROTO_RAW		255		; Raw IP packets

%ifdef __LIBC__

;
;sys/socket.h
;

%assign	SOL_SOCKET	1

%assign SO_DEBUG	1
%assign SO_REUSEADDR	2
%assign SO_TYPE		3
%assign SO_ERROR	4
%assign SO_DONTROUTE	5
%assign SO_BROADCAST	6
%assign SO_SNDBUF	7
%assign SO_RCVBUF	8
%assign SO_KEEPALIVE	9
%assign SO_OOBINLINE	10
%assign SO_NO_CHECK	11
%assign SO_PRIORITY	12
%assign SO_LINGER	13
%assign SO_BSDCOMPAT	14
%assign SO_REUSEPORT	15
%assign SO_PASSCRED	16
%assign SO_PEERCRED	17
%assign SO_RCVLOWAT	18
%assign SO_SNDLOWAT	19
%assign SO_RCVTIMEO	20
%assign SO_SNDTIMEO	21

%endif

;
;time.h
;

struc timespec
.tv_sec		ULONG	1
.tv_nsec	ULONG	1
endstruc

struc timeval
.tv_sec		ULONG	1
.tv_usec	ULONG	1
endstruc

;
;sys/utsname.h
;

%ifdef __BSD__
%assign SYS_NMLN 32
%assign MAXHOSTNAMELEN 256
%else
%assign SYS_NMLN 65
%assign MAXHOSTNAMELEN SYS_NMLN - 1
%endif

struc utsname
.sysname	CHAR	SYS_NMLN
.nodename	CHAR	SYS_NMLN
.release	CHAR	SYS_NMLN
.version	CHAR	SYS_NMLN
.machine	CHAR	SYS_NMLN
.domainname	CHAR	SYS_NMLN
endstruc

;
;sys/stat.h
;

%assign	S_ISUID	0004000q	;set user id on execution
%assign	S_ISGID	0002000q	;set group id on execution
%assign	S_ISTXT	0001000q	;sticky bit

%assign	S_IRWXU	0000700q	;RWX mask for owner
%assign	S_IRUSR	0000400q	;R for owner
%assign	S_IWUSR	0000200q	;W for owner
%assign	S_IXUSR	0000100q	;X for owner

%assign	S_IREAD		S_IRUSR
%assign	S_IWRITE	S_IWUSR
%assign	S_IEXEC		S_IXUSR

%assign	S_IRWXG	0000070q	;RWX mask for group
%assign	S_IRGRP	0000040q	;R for group
%assign	S_IWGRP	0000020q	;W for group
%assign	S_IXGRP	0000010q	;X for group

%assign	S_IRWXO	0000007q	;RWX mask for other
%assign	S_IROTH	0000004q	;R for other
%assign	S_IWOTH	0000002q	;W for other
%assign	S_IXOTH	0000001q	;X for other

%assign	S_IFMT	 0170000q	;type of file mask
%assign	S_IFIFO	 0010000q	;named pipe (fifo)
%assign	S_IFCHR	 0020000q	;character special
%assign	S_IFDIR	 0040000q	;directory
%assign	S_IFBLK	 0060000q	;block special
%assign	S_IFREG	 0100000q	;regular
%assign	S_IFLNK	 0120000q	;symbolic link
%assign	S_IFSOCK 0140000q	;socket
%assign	S_IFWHT  0160000q	;whiteout
%assign	S_ISVTX	 0001000q	;save swapped text even after use

struc stat
.st_dev		USHORT	1
.__pad1		USHORT	1
.st_ino		ULONG	1
.st_mode	USHORT	1
.st_nlink	USHORT	1
.st_uid		USHORT	1
.st_gid		USHORT	1
.st_rdev	USHORT	1
.__pad2		USHORT	1
%ifdef __BSD__
.st_atime	ULONG	1
.st_atimensec	ULONG	1
.st_mtime	ULONG	1
.st_mtimensec	ULONG	1
.st_ctime	ULONG	1
.st_ctimensec	ULONG	1
.st_size	ULONG	1
.st_blocks	ULONG	1
.__pad3		ULONG	1
.st_blksize	ULONG	1
.st_flags	ULONG	1
.st_gen		ULONG	1
.st_lspare	ULONG	3
%else
.st_size	ULONG	1
.st_blksize	ULONG	1
.st_blocks	ULONG	1
.st_atime	ULONG	1
.__unused1	ULONG	1
.st_mtime	ULONG	1
.__unused2	ULONG	1
.st_ctime	ULONG	1
.__unused3	ULONG	1
%endif
.__unused4	ULONG	1
.__unused5	ULONG	1
endstruc

struc __old_kernel_stat
.st_dev		USHORT	1
.st_ino		USHORT	1
.st_mode	USHORT	1
.st_nlink	USHORT	1
.st_uid		USHORT	1
.st_gid		USHORT	1
.st_rdev	USHORT	1
.st_size	ULONG	1
.st_atime	ULONG	1
.st_mtime	ULONG	1
.st_ctime	ULONG	1
endstruc

;
;sys/resource.h
;

%assign PRIO_PROCESS	0
%assign PRIO_PGRP	1
%assign	PRIO_USER	2

;
;socket.h
;

struc	sockaddr
.sa_family_t	USHORT	1		;address family, AF_xxx
.sa_data	CHAR	14		;14 bytes of protocol address
endstruc

;
;in.h
;

struc in_addr
.s_addr		U32	1
endstruc

%endif	;__INCLUDES_INC
