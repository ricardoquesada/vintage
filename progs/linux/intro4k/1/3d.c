/*
 * 3d engine
 * riq.
 */

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <math.h>

#define MaxX 640
#define MaxY 480
#define VMEM_SIZE MaxX * MaxY

typedef unsigned char byte;

#define colornum 8

byte *p, ColorTable[colornum] = { 0, 0, 2, 0, 0, 2, 10, 2 };

typedef struct
{
	short x,y
} _2D;

typedef struct
{
	float x,y,z
} _3D;

typedef struct
{
	_3D Local;
	_3D Wordl;
	_3D Aligned;
} Vertex_t;

inline void putpixel(int x, int y, byte color)
{
	*(p + y * MaxX + x) = color;
}

int main(void)
{
	int i,j,h;
	int color;
	int size;

	h = open("/dev/fb0", O_RDWR);
	p = mmap(0, VMEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, h, 0);

	for (i = 0; i < VMEM_SIZE; i++) *(p + i) = 0;

	for(size=10;size<200;size+=10)
	{
		for( i=0;i<MaxX; ) {
			for(j=0;j<MaxY;) {
				putbox(i,j,size,ColorTable[(color++)%colornum]);
				j += size;
			}
			i +=size;
		}
		sleep(1);
	}

	munmap(p, VMEM_SIZE);
	close(h);
}
