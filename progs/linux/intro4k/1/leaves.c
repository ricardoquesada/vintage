/*
 leaves.c : C implementation using /dev/fb0

 takes ~2KB
*/

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <math.h>

#define MaxX 640
#define MaxY 480
#define VMEM_SIZE MaxX * MaxY

typedef unsigned char byte;

#define colornum 8

byte *p, ColorTable[colornum] = { 0, 0, 2, 0, 0, 2, 10, 2 };

inline void putpixel(int x, int y, byte color)
{
	*(p + y * MaxX + x) = color;
}

void putbox( int x, int y, int l,  byte color )
{
	int i,j;
	int lx, ly;

	lx = ly = l;
	if( y+l >= MaxY )
		ly = MaxY - y;
	if( x+l >= MaxX )
		lx = MaxX - x;

	for(i=x; i< x+lx; i++) {
		for(j=y;j<y+ly;j++)
			putpixel(i,j,color);
	}
}

int main(void)
{
	int i,j,h;
	int color;
	int size;

	h = open("/dev/fb0", O_RDWR);
	p = mmap(0, VMEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, h, 0);

	for (i = 0; i < VMEM_SIZE; i++) *(p + i) = 0;

	for(size=10;size<200;size+=10)
	{
		for( i=0;i<MaxX; ) {
			for(j=0;j<MaxY;) {
				putbox(i,j,size,ColorTable[(color++)%colornum]);
				j += size;
			}
			i +=size;
		}
		sleep(1);
	}

	munmap(p, VMEM_SIZE);
	close(h);
}
