/*
 * 990614: Jugar con proc y esas cosas
 */
#include <linux/config.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h> /* printk() */
#include <linux/malloc.h> /* kmalloc() */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/interrupt.h> /* mark_bh */

#include <linux/netdevice.h>   /* struct device, and other headers */
#include <linux/etherdevice.h> /* eth_type_trans */
#include <linux/ip.h>          /* struct iphdr */
#include <linux/in.h>          /* struct sockaddr_in */
#include <linux/skbuff.h>
#include <asm/uaccess.h>
#include <linux/in6.h>
#include <asm/checksum.h>
#include <linux/proc_fs.h>
#include <linux/netlink.h>
#include <net/ip.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,1,0)
#error "Para version 2.1 o superior"
#endif

extern int paquetes_rcv;
extern struct inet_protocol riq_protocol;

static int riq_version_get_info(char *buffer, char **start, off_t offset, int length, int dummy)
{
	int len = 0;
	off_t begin = 0;

	len += sprintf(buffer + len, "riq protocol v0.1\n");

	*start = buffer + (offset - begin);
	len -= (offset - begin);
	if (len > length)
		len = length;
	return len;
}

static int riq_paquetes_get_info(char *buffer, char **start, off_t offset, int length, int dummy)
{
	int len = 0;
	off_t begin = 0;

	len += sprintf(buffer + len, "paquetes recividos: %d\n",paquetes_rcv);

	*start = buffer + (offset - begin);
	len -= (offset - begin);
	if (len > length)
		len = length;
	return len;
}

struct proc_dir_entry riq_version =
{
	0,
	7, "wias_tx",
	S_IFREG | S_IRUGO, 1, 0, 0, 0,
	&proc_net_inode_operations,
	riq_paquetes_get_info,
	NULL, NULL, NULL, NULL, NULL
};

struct proc_dir_entry riq_paquetes=
{
	0,
	11, "riq_version",
	S_IFREG | S_IRUGO, 1, 0, 0, 0,
	&proc_net_inode_operations,
	riq_version_get_info,
	NULL, NULL, NULL, NULL, NULL
};

int init_module(void)
{
	proc_register( proc_net, &riq_version);
	proc_register( proc_net, &riq_paquetes);
	inet_add_protocol(&riq_protocol);
	return 0;
}

int cleanup_module(void)
{
	proc_unregister( proc_net, riq_version.low_ino);
	proc_unregister( proc_net, riq_paquetes.low_ino);
	inet_del_protocol(&riq_protocol);
	return 0;
}
