/*
 * receive code
 */

#include <linux/config.h>
#include <linux/version.h>

#define __NO_VERSION__
#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/malloc.h> /* kmalloc() */
#include <linux/errno.h>  /* error codes */
#include <linux/types.h>  /* size_t */
#include <linux/interrupt.h> /* mark_bh */

#include <linux/netdevice.h>   /* struct device, and other headers */
#include <linux/etherdevice.h> /* eth_type_trans */
#include <linux/ip.h>          /* struct iphdr */
#include <linux/skbuff.h>
#include <asm/uaccess.h>
#include <linux/in6.h>
#include <asm/checksum.h>
#include <net/ip.h>

#include "riq_rcv.h"

static int paquetes_rcv=0;

struct riq {
	__u8	valor1;
	__u8	valor2;
	__u8	valor3;
};

int riq_rcv(struct sk_buff *skb, unsigned short xlen)
{
	struct device *dev = skb->dev;
	unsigned char protoc;
	struct iphdr *ipp;
	struct riq *riqp = NULL;

	
	MOD_INC_USE_COUNT;
	
	if (skb == NULL) {
		printk("riq_rcv: skb==null\n");
		goto rcvleave;
	}
	
	protoc = ((struct iphdr *)skb->data)->protocol;
	if(protoc != IPPROTO_RIQ) {
		printk("riq_rcv: proto != %d\n",IPPROTO_RIQ);
		goto rcvleave;
	}

	paquetes_rcv++;

rcvleave:
 	if(skb) 
                kfree_skb(skb);

	MOD_DEC_USE_COUNT;
	return(0);
}

struct inet_protocol riq_protocol = 
{
	riq_rcv,			/* riq handler */
	NULL,				/* riq error handler */
	0,				/* next */
	IPPROTO_RIQ,			/* protocol ID */
	0,				/* copy */
	NULL,				/* data */
	"riq"				/* name */
};
