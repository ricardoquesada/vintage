/*	$Id: net.h,v 1.2 2000/03/17 04:25:21 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __TEG_NET_H
#define __TEG_NET_H

#include <sys/types.h>

ssize_t writen(int fd, const void *vptr, size_t n );
int net_connect_unix(char *path );
int net_connect_tcp(char *host, int port );
ssize_t net_readline(int sock, void *gs,size_t maxlen );
int net_printf(int sock, char *format, ...);

#endif /* __TEG_NET_H */
