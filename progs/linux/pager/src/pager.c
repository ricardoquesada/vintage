#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "all.h"

PAGER_STATUS token_generico( char *str, int i);

struct _nombres tokens[]={
	{"source","Origen",token_generico,40,NULL,0},
	{"title","Titulo",token_generico,140,NULL,0},
	{"date","Fecha",token_generico,40,NULL,0},
	{"time","Hora",token_generico,40,NULL,0},
	{"section","Seccion", token_generico,30,NULL,0},
	{"entry","Id", token_generico,20,NULL,0},
	{"description","Descripcion", token_generico,20,NULL,0},
};
#define	NTOKENS  (sizeof(tokens)/sizeof(tokens[0]))

struct _pager pager = {
	-1,
	"default",
	"www.omnipager.com",
	80,
	"/cgi-bin/pager.pl?id=%s&ver=2&resend=%d",
	"proxy",
	8080,
	0
};

PAGER_STATUS token_generico( char *str, int i)
{
	if( tokens[i].buffsize )
		strncpy( tokens[i].buff, str, tokens[i].buffsize);
	return PAGER_STATUS_SUCCESS;
}

PAGER_STATUS pager_init()
{
//	printf("pager_init()\n");

	pager.fd = -1;
	return PAGER_STATUS_SUCCESS;
}

PAGER_STATUS pager_connect()
{
//	printf("pager_connect()\n");

	if( pager.fd == -1 ) {
		if( !pager.proxyenable)
			pager.fd = net_connect_tcp( pager.host, pager.port );
		else
			pager.fd = net_connect_tcp( pager.proxyhost, pager.proxyport );
	}
	else
		return PAGER_STATUS_UNEXPECTED;

	if( pager.fd < 0 )
		return PAGER_STATUS_NOTCONNECTED;

	return PAGER_STATUS_SUCCESS;
}

PAGER_STATUS pager_close()
{
//	printf("pager_close()\n");

	if( pager.fd == -1)
		return PAGER_STATUS_UNEXPECTED;

	close( pager.fd );
	pager.fd = -1;
	return PAGER_STATUS_SUCCESS;
} 

PAGER_STATUS pager_stripheader()
{
	char buffer[500];
	int i;

//	printf("pager_stripheader()\n");

	if( pager.fd == -1)
		return -1;

	while(1) {
		i = net_readline( pager.fd, buffer, 500);
//		printf("%s",buffer);
		if( i<=2 || buffer[0]=='\n')
			break;
	}
	return 0;
}

PAGER_STATUS pager_readline(char *buffer,int len, int *read)
{
	int a;

	if( pager.fd == -1)
		return PAGER_STATUS_NOTCONNECTED;

	a = net_readline( pager.fd, buffer, len);
	if( a==-1 || a==0)
		return PAGER_STATUS_NOMOREDATA;

//	printf("%s",buffer);

	*read = a;

	return PAGER_STATUS_SUCCESS;
}

static PAGER_STATUS lookup_funcion( PARSER *p )
{
	int i;

	for(i = 0; i < NTOKENS; i++) {
		if(strcmp( p->token, tokens[i].label )==0 ) {
			if (tokens[i].func)
				return( (tokens[i].func)(p->value, i));
			return PAGER_STATUS_TOKENNULL;
		}
	}
//	printf("Token '%s' no encontrado\n",p->token);
	return PAGER_STATUS_TOKENNOTFOUND;
}


PAGER_STATUS pager_readblock()
{
	PAGER_STATUS s;
	char buf[500];
	int a;
	PARSER p;
	DELIM separador={ '\0', '\0', '\0' };
	DELIM igualador={ ':', ':', ':' };

	p.igualador = &igualador;
	p.separador = &separador;
	

//	printf("pager_readblock()\n");

	if( pager.fd == -1)
		return PAGER_STATUS_NOTCONNECTED;

	while(1) {
		s = pager_readline( buf, 500,&a);
		if( s==PAGER_STATUS_NOMOREDATA) {
			return s;
		}
		if( a <= 2) {
			return PAGER_STATUS_BLOCKFINISHED;
		}

		p.data = buf;
		if( parser_call( &p ) ) {
			lookup_funcion(&p);
		} else {
			printf("Error parsing :(\n");
		}
	}

	return PAGER_STATUS_SUCCESS;
}

static PAGER_STATUS pager_request( int r )
{
	char buf_tmp[500];

	if( pager.fd == -1 )
		return PAGER_STATUS_NOTCONNECTED;

	sprintf(buf_tmp,pager.pagerpl,pager.id,r);

	if( !pager.proxyenable)
		net_printf( pager.fd, "GET %s HTTP/1.1\nUser-Agent: GOmnipager %s\nHost: %s\nPragma: No-Cache\n\n",buf_tmp,VERSION,pager.host);
	else
		net_printf(pager.fd,"GET http://%s%s HTTP/1.1\nUser-Agent: GOmnipager %s\nHost: %s\nPragma: No-Cache\n\n",pager.host,buf_tmp,VERSION,pager.host);
	return PAGER_STATUS_SUCCESS;
}

PAGER_STATUS pager_request_all()
{
	return pager_request(1);
}

PAGER_STATUS pager_request_update()
{
	return pager_request(0);
}

PAGER_STATUS pager_receive()
{
	PAGER_STATUS s;

//	printf("pager_receive()\n");
	pager_stripheader();

	do {
		s=pager_readblock();
		CALL_GUI(GUI_BLOCK)();

	} while (s != PAGER_STATUS_NOMOREDATA);
	return PAGER_STATUS_SUCCESS;
}
int pager_cantnombres()
{
	return NTOKENS;
}
