/*	$Id: net.c,v 1.3 2000/03/17 04:25:21 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/*
 * La mayoria de estas funciones fueron basadas en codigo de:
 *       Unix Network Programming vol1 2nd Ed. (W.Richard Stevens)
 *       Linux Application Development (M.K.Johnson, E.K.Troan)
 *       y del archivo gnome-net (que ya no existe mas)
 *
 * v0.91: 10/2/00	. Rename riq_net_* a net_*
 * 			. net_printf usa buffer de tamanio PROT_MAX_LEN
 * v0.9: 31/3/99	. Bug en el INET6 (estable?)
 * v0.8: 24/1/99	. Soporte para INET6
 *			. Rewrite de varias funciones
 *			. Codigo mas inestable :-(
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "all.h"

#ifdef INET6
#include <netdb.h>
#endif

ssize_t
writen(int fd, const void *vptr, size_t n )
{
	size_t nleft;
	ssize_t nwritten;
	const char *ptr;

	ptr = vptr;
	nleft = n;
	while( nleft > 0 ) {
		if( ( nwritten = write(fd, ptr, nleft) ) <= 0 ) {
			if( errno == EINTR )
				nwritten = 0;
			else
				return (-1 );
		}
		nleft -= nwritten ;
		ptr += nwritten;
	}
	return(n);
}

ssize_t
net_readline( int fd, void *vptr, size_t maxlen )
{
	ssize_t n, rc;
	char c, *ptr;

	ptr = vptr ;
	for( n =1 ; n < maxlen; n++ ) {
again:
		if( (rc= read(fd, &c, 1 )) ==1 ) {
			*ptr++ = c;
			if( c=='\n' )
				break;
		} else if( rc== 0 ) {
			return 0;
		} else {
			if( errno == EINTR )
				goto again;
			return(-1);
		}
	}

	*ptr = 0;
	return( n );
}

int
net_connect_tcp(char *thost, int port )
{
	int sock;
#ifdef INET6
	struct sockaddr_in6 address;
#else
	struct sockaddr_in address;
	struct in_addr inaddr;
	struct hostent * host;
#endif /* INET6 */

	bzero(&address,sizeof(address));

#ifdef INET6
	address.sin6_family = AF_INET6;
	address.sin6_port = htons ( port );
	if(( sock = socket(AF_INET6, SOCK_STREAM, 0 )) < 0 ) {
		perror("net_connect_tcp: socket");
		return -1;
	}
	if( inet_pton( AF_INET6, thost, &address.sin6_addr ) <= 0) {
		if(inet_pton(AF_INET, thost, &address.sin6_addr)) {
			perror("inet_pton error:");
			return (-1);
		}
	}
#else
	if( inet_aton( thost, &inaddr ))
		host = gethostbyaddr((char * ) &inaddr, sizeof(inaddr ), AF_INET );
	else
		host = gethostbyname( thost );
	if(!host) {
		perror("net_connect_tcp: looking host");
		return -1;
	}
	if(( sock = socket(AF_INET, SOCK_STREAM, 0 )) < 0 ) {
		perror("net_connect_tcp: socket");
		return -1;
	}
	address.sin_family = AF_INET;
	address.sin_port = htons ( port );
	memcpy( &address.sin_addr, host->h_addr_list[0], sizeof(address.sin_addr));
#endif /* INET6 */

	if( connect( sock, (struct sockaddr * ) &address, sizeof(address)) < 0) {
		perror("net_connect_tcp: connect");
		return -1;
	}
        return sock;
}


int
net_printf(int sock, char *format, ...)
{
        va_list args;
	char buf[PROT_MAX_LEN];

	va_start(args, format);
	vsprintf(buf, format, args);
	va_end(args);

	return writen(sock, buf, strlen(buf));
}

int
net_connect_unix(char *path )
{
	int sock;
	struct sockaddr_un address;
	size_t addrLenght;

	if((sock= socket(AF_UNIX, SOCK_STREAM,0)) < 0 )
		return -1;

	address.sun_family = AF_UNIX ;
	strcpy(address.sun_path,path);

	addrLenght = sizeof( address.sun_family) + strlen(address.sun_path);

	if( connect(sock, (struct sockaddr *) &address, addrLenght ) <0) 
		return -1;

        return sock;
}
