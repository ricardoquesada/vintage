#include <stdarg.h>
#include <stdio.h>

#include "all.h"

PAGER_STATUS textmsg( char *format, ...)
{
        va_list args;
	char buf[PROT_MAX_LEN];

	va_start(args, format);
	vsprintf(buf, format, args);
	va_end(args);

	CALL_GUI(GUI_TEXTMSG)(buf);
	return PAGER_STATUS_SUCCESS;
}
