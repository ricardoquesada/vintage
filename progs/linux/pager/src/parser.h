/*	$Id: parser.h,v 1.3 2000/03/17 04:25:21 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/*
 * Estructura del parser
 */

#ifndef __PARSER_H
#define __PARSER_H

#define PARSER_TOKEN_MAX 400
#define PARSER_VALUE_MAX 400 

typedef struct {
	char a;
	char b;
	char c;
} DELIM, *PDELIM;

typedef struct {
	char *data; 
	int hay_otro;
	char token[PARSER_TOKEN_MAX];
	char value[PARSER_VALUE_MAX];
	PDELIM igualador;
	PDELIM separador;
} PARSER, *PPARSER;

typedef enum {
	PARSER_FIN,
	PARSER_SEPARADOR,
	PARSER_IGUAL,
	PARSER_DATA,
	PARSER_ERROR
} PARSER_VALUE, *PPARSER_VALUE;

/* Unica funcion publica del parser */
int parser_call( PPARSER );


#endif /* __PARSER_H */
