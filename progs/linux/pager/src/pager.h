#ifndef __PAGER_PAGER__H
#define __PAGER_PAGER__H

struct _pager {
	int fd;
	char id[80];
	char host[80];
	int port;
	char pagerpl[100];
	char proxyhost[80];
	int proxyport;
	int proxyenable;
};

struct _nombres {
	char *label;
	char *name;
	PAGER_STATUS (*func) ();
	int size;
	char *buff;
	int buffsize;
}; 

extern struct _nombres tokens[];

extern struct _pager pager;

PAGER_STATUS pager_connect();
PAGER_STATUS pager_close();
PAGER_STATUS pager_stripheader();
PAGER_STATUS pager_readline( char *buffer,int len, int *read);
PAGER_STATUS pager_readblock();
PAGER_STATUS pager_receive();
PAGER_STATUS pager_request_all();
PAGER_STATUS pager_request_update();
int pager_cantnombres();

#endif
