/*	$Id: callbacks.h,v 1.4 2000/03/19 01:07:05 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GUI_GNOME_CALLBACKS_H
#define __GUI_GNOME_CALLBACKS_H

#include <gnome.h>
#include "all.h"

extern GtkWidget *main_win;

void
on_exit_activate                      (GtkWidget     	*widget,
                                        gpointer         user_data);

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_update_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

#endif /* __GUI_GNOME_CALLBACKS_H */
