/*	$Id: callbacks.c,v 1.7 2000/03/19 01:07:05 riq Exp $	*/
/* Tenes Empanadas Graciela
 *
 * Copyright (C) 2000 The Free Software Foundation
 *
 * Author: Ricardo Calixto Quesada <rquesada@core-sdi.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "all.h"
#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "main.h"

GtkWidget *main_win=NULL;

GtkWidget *pref_dialog=NULL;
GtkWidget *conf_entry_proxyhost=NULL;
GtkWidget *conf_spinner_proxyport=NULL;
GtkWidget *conf_cb_proxyenable=NULL;
GtkWidget *conf_entry_id=NULL;

/*
 * funciones auxiliares
 */

/* close a window */
void
destroy_window( GtkWidget * widget, GtkWidget **window )
{
	if( *window != NULL)
		gtk_widget_destroy(*window);

	*window=NULL;
}


/* Brings attention to a window by raising it and giving it focus */
static void
raise_and_focus (GtkWidget *widget)
{
	g_assert (GTK_WIDGET_REALIZED (widget));
	gdk_window_show (widget->window);
	gtk_widget_grab_focus (widget);
}


void
on_exit_activate (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit();
}


static void apply_cb (GtkWidget *widget, gint pagenum, gpointer data)
{
	if (pagenum != -1)
		return;
  
	strncpy(pager.proxyhost,gtk_entry_get_text(GTK_ENTRY(conf_entry_proxyhost)),50);
	strncpy(pager.id,gtk_entry_get_text(GTK_ENTRY(conf_entry_id)),50);

	pager.proxyport = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(conf_spinner_proxyport));
	pager.proxyenable = GTK_TOGGLE_BUTTON(conf_cb_proxyenable)->active;

	gnome_config_set_int   ("/gpager/data/proxyport",  pager.proxyport);
	gnome_config_set_string("/gpager/data/proxyhost",pager.proxyhost);
	gnome_config_set_string("/gpager/data/id",pager.id);
	gnome_config_set_int("/gpager/data/proxyenable",pager.proxyenable);
	gnome_config_sync();
}

void prop_box_changed_callback (GtkWidget *widget, gpointer data)
{
	if(pref_dialog==NULL)
		return;

//	gtk_widget_set_sensitive (conf_entry_proxyhost, pager.proxyenable );
//	gtk_widget_set_sensitive (conf_spinner_proxyport, pager.proxyenable );

	gnome_property_box_changed (GNOME_PROPERTY_BOX (pref_dialog));
}


void
on_update_activate(GtkMenuItem  *menuitem, gpointer         user_data)
{
	if( pager_connect() == PAGER_STATUS_SUCCESS) {
		pager_request_update();
		tag = gdk_input_add( pager.fd, GDK_INPUT_READ,(GdkInputFunction) cb_receive, (gpointer) NULL);
	} else {
		printf("Fuck! No me pude conectar!\n");
	}

}


void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
	GtkWidget *label;
	GtkWidget *table;
	GtkWidget *frame;
	GtkWidget *vbox;
	GtkWidget *hbox;
        GtkAdjustment *adj;


	if( pref_dialog != NULL ) {
		gdk_window_show( pref_dialog->window);
		gdk_window_raise( pref_dialog->window);
		return ;
	}


	table = gtk_table_new (2, 2, FALSE);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD);

	frame = gtk_frame_new (_("Proxy"));
	gtk_container_border_width (GTK_CONTAINER (frame), 0);

	vbox = gtk_vbox_new (TRUE, 0);
	gtk_container_border_width (GTK_CONTAINER (vbox), GNOME_PAD);


	hbox = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox );
	label = gtk_label_new(_("Proxy port:"));
	gtk_box_pack_start( GTK_BOX( hbox), label, TRUE, TRUE, 0);
	adj = (GtkAdjustment *) gtk_adjustment_new( pager.proxyport, 1.0, 65536.0, 1.0, 5.0, 1.0 );
	conf_spinner_proxyport = gtk_spin_button_new( adj, 0.0, 0);
	gtk_box_pack_start( GTK_BOX( hbox), conf_spinner_proxyport, TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed", GTK_SIGNAL_FUNC
			(prop_box_changed_callback), NULL);
//	gtk_widget_set_sensitive (conf_spinner_proxyport, pager.proxyenable );

	hbox = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox );
	label = gtk_label_new(_("Proxy host:"));
	gtk_box_pack_start( GTK_BOX( hbox), label, TRUE, TRUE, 0);
	conf_entry_proxyhost = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( conf_entry_proxyhost ), pager.proxyhost);
	gtk_box_pack_start( GTK_BOX(hbox), conf_entry_proxyhost, TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (conf_entry_proxyhost), "key_press_event",
		GTK_SIGNAL_FUNC (prop_box_changed_callback), NULL);
//	gtk_widget_set_sensitive (conf_entry_proxyhost, pager.proxyenable );


	hbox = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox );
	conf_cb_proxyenable = gtk_check_button_new_with_label(_("Enable Proxy"));
	GTK_TOGGLE_BUTTON(conf_cb_proxyenable)->active=pager.proxyenable;
	gtk_box_pack_start( GTK_BOX( hbox ), conf_cb_proxyenable, TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (conf_cb_proxyenable), "clicked",
		GTK_SIGNAL_FUNC (prop_box_changed_callback), NULL);


	hbox = gtk_hbox_new( FALSE, 0);
	gtk_container_add( GTK_CONTAINER( vbox ), hbox );
	label = gtk_label_new(_("Id:"));
	gtk_box_pack_start( GTK_BOX( hbox), label, TRUE, TRUE, 0);
	conf_entry_id = gtk_entry_new( );
	gtk_entry_set_text( GTK_ENTRY( conf_entry_id ), pager.id);
	gtk_box_pack_start( GTK_BOX(hbox), conf_entry_id, TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (conf_entry_id), "key_press_event",
		GTK_SIGNAL_FUNC (prop_box_changed_callback), NULL);


	gtk_container_add (GTK_CONTAINER (frame), vbox);

	gtk_table_attach (GTK_TABLE (table), frame, 0, 1, 0, 1, GTK_EXPAND |
			GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	pref_dialog = gnome_property_box_new ();
	gnome_dialog_set_parent (GNOME_DIALOG (pref_dialog),
			GTK_WINDOW (main_win));
	gtk_window_set_title (GTK_WINDOW (pref_dialog),
			_("GOmnipager Preferences"));
	gtk_signal_connect (GTK_OBJECT (pref_dialog), "destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed), &pref_dialog);

	label = gtk_label_new (_("GOmnipager"));
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (pref_dialog),
			table, label);

	gtk_signal_connect (GTK_OBJECT (pref_dialog), "apply", GTK_SIGNAL_FUNC
			(apply_cb), NULL);

	if (!GTK_WIDGET_VISIBLE (pref_dialog))
		gtk_widget_show_all (pref_dialog);
	else
		gtk_widget_destroy (pref_dialog);
}

void on_about_activate(GtkMenuItem *menuitem, gpointer user_data)
{
	static GtkWidget *about;
	static const char *authors[] = {
		"Ricardo C. Quesada (rquesada@core-sdi.com)",
		NULL
	};

	if (!about) {
		about = gnome_about_new (
			_("GOmnipager"),
			VERSION,
			_("Copyright (C) 2000 The Free Software Foundation"),
			authors,
			_("A Omnipager for Gnome."),
			NULL);
		gtk_signal_connect (GTK_OBJECT (about), "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &about);
	}

	gtk_widget_show_now (about);
	raise_and_focus (about);
}
