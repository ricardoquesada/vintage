#include <stdio.h>
#include <stdlib.h>

#include "all.h"

int main(int argc, char **argv)
{
	pager_init();

	CALL_GUI(GUI_MAIN)(argc,argv);

#if 0
	if( pager_connect() == PAGER_STATUS_SUCCESS) {
		char a1[100];
		char a2[100];
		char a3[100];
		char a4[100];
		char a5[100];
		char a6[100];
		char a7[100];
		struct _datos datos = {
			a1, a2, a3, a4, a5, a6, a7,
			100,100,100,100,100,100,100
		};

		pager_request_all();
		pager_receive( &datos );
	}
	return 0;
#endif
}
