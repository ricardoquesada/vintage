#ifndef __PAGER_ALL_H
#define __PAGER_ALL_H

#ifndef MAX
#define MAX(a,b)	(((a) > (b)) ? (a): (b))
#endif

#ifndef MIN
#define MIN(a,b)	(((a) < (b)) ? (a): (b))
#endif

#ifndef TRUE
#define TRUE	(1)
#endif
#ifndef FALSE
#define FALSE	(0)
#endif

typedef enum {
	PAGER_STATUS_SUCCESS,
	PAGER_STATUS_ERROR,
	PAGER_STATUS_NOTCONNECTED,
	PAGER_STATUS_UNEXPECTED,
	PAGER_STATUS_NOMEM,
	PAGER_STATUS_NOMOREDATA,
	PAGER_STATUS_BLOCKFINISHED,
	PAGER_STATUS_TOKENNULL,
	PAGER_STATUS_TOKENNOTFOUND,
} PAGER_STATUS, *PPAGER_STATUS;

enum {
	GUI_MAIN,		/* main loop */
	GUI_DISCONNECT,		/* discon */
	GUI_CONNECT,		/*  */
	GUI_TEXTMSG,		/*  */
	GUI_BLOCK,		/* tengo un bloque para mostrar */
	GUI_LAST		/* este no  existe */
};

typedef struct _gui_funcs {
	PAGER_STATUS (*func)();
} GUI_FUNCS;

extern GUI_FUNCS gui_funcs[GUI_LAST];

#define CALL_GUI(a) if( gui_funcs[a].func) gui_funcs[a].func

#define PROT_MAX_LEN 500

PAGER_STATUS textmsg( char *format, ...);

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pager.h"
#include "parser.h"
#include "net.h"

#endif /* __PAGER_ALL_H */
