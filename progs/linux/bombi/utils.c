#include <stdlib.h>
#include <netinet/in.h>

unsigned short get_random_uint()
{
	return( random() );
}
unsigned char  get_random_byte() 
{
	return( random() );
}
void get_random_address( struct in_addr *s )
{
	s->s_addr = (random() &0xff) * 256 * 256 * 256 +
		(random() &0xff) * 256 * 256 +
		(random() &0xff) * 256 +
		(random() &0xff);
}
