# spec file for package wias

%define  ver     0.4.0
%define  RELEASE 1
%define  rel     %{?CUSTOM_RELEASE} %{!?CUSTOM_RELEASE:%RELEASE}
%define  prefix  /usr
%define  sysdocs /usr/doc/packages

Name:      		wias
Summary:   		driver for WaveAccess 3500
Version:		%ver
Release:		%rel
Distribution:	RedHat 6.2 (i686)
Copyright: 	  	GPL
Group: 			Amusements/Games
URL:			http://wias.sourceforge.net/
Packager:     	Ricardo Quesada <riq@core-sdi.com>
Requires:		glibc ppp
Provides:		wias
#Autoreqprov:		On

Docdir:    %{sysdocs}
BuildRoot: /var/tmp/rpm/wias-%{PACKAGE_VERSION}-root

Source:     wias-%{PACKAGE_VERSION}.tar.gz

%description
wias is a driver kernel driver and a user mode program that enables
the user to use the WaveAccess 3500 modem

Authors:
--------
Gerardo Richarte
Ricardo Quesada

%prep
%setup
#CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%{prefix}

%build
make

%install
rm -rf $RPM_BUILD_ROOT
if [ ! -d $RPM_BUILD_ROOT ]; then
	mkdir $RPM_BUILD_ROOT
fi
if [ ! -d $RPM_BUILD_ROOT/usr ]; then
	mkdir $RPM_BUILD_ROOT/usr
fi
if [ ! -d $RPM_BUILD_ROOT/usr/sbin ]; then
	mkdir $RPM_BUILD_ROOT/usr/sbin
fi
make prefix=$RPM_BUILD_ROOT%{prefix} install
#strip $RPM_BUILD_ROOT%{prefix}/sbin/*

%clean
#rm -rf $RPM_BUILD_DIR/wias-%{ver}
#rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%{prefix}/sbin/wias
%{prefix}/sbin/wiasconfig
%{prefix}/sbin/wiasup
%{prefix}/sbin/wiasdown
%{prefix}/sbin/wstats
%{prefix}/sbin/wias.o
%{prefix}/sbin/wreset

%doc ABOUT-NLS AUTHORS COPYING ChangeLog INSTALL NEWS README options.dist

%changelog
