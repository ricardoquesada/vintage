/*	$Id: main1.c,v 1.2 2000/11/06 14:48:16 riq Exp $	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <asm/ioctl.h>
#include <unistd.h>
#include <assert.h>

#include "wiasd.h"
#include "wias.h"
#include "wias-proto.h"

struct session ses_table[N_SES];

/**
 * @fn int create_raw_socket (unsigned short type)
 */
int create_raw_socket (unsigned short type)
{
	int optval = 1, rv;

	if ((rv = socket (PF_INET, SOCK_PACKET, htons (type))) < 0) {
		printf("socket: %m");
		return -1;
	}

	if (setsockopt (rv, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) < 0) {
		printf("setsockopt: %m");
		return -1;
	}

	return rv;
}

/**
 * @fn int get_hw_addr (struct session *ses) 
 */
int get_hw_addr (struct session *ses) 
{
	struct ifreq ifr;

	assert(ses);

	strncpy (ifr.ifr_name, ses->dev, sizeof (ifr.ifr_name));

	if (ioctl (ses->sock, SIOCGIFHWADDR, &ifr) < 0) {
		printf("ioctl(SIOCGIFHWADDR): %m\n");
		return -1;
	}

	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
		printf("interface %s is not Ethernet!\n", ses->dev);
		return -1;
	}

	memcpy (ses->smac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

	PDEBUG("we have hware address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n"
			,(unsigned char)ses->smac[0]
			,(unsigned char)ses->smac[1]
			,(unsigned char)ses->smac[2]
			,(unsigned char)ses->smac[3]
			,(unsigned char)ses->smac[4]
			,(unsigned char)ses->smac[5] );

	return 0;
}

/**
 * @fn void ses_cleanup (struct session *ses)
 */
void ses_cleanup (struct session *ses)
{
	assert(ses);
	close (ses->sock);
}

/**
 * @fn int send_packet (struct session *ses, void *packet, int len )
 */
int send_packet (struct session *ses, void *packet, int len )
{
	struct sockaddr addr;
	int c;

	assert(ses);
	assert(packet);

	memset (&addr, 0, sizeof (addr));
	strcpy (addr.sa_data, ses->dev);

	if ((c = sendto (ses->sock, packet, len, 0, &addr, sizeof (addr))) < 0) {
		PDEBUG("sendto (send_packet): %m");
	}

	return c;
}

/**
 * @fn int ses_init (struct session *ses)
 */
WIAS_STATUS ses_init (struct session *ses)
{
	int fd;

	if ((ses->sock = create_raw_socket (ETH_P_WIAS_CTRL)) < 0) {
		printf("unable to create raw socket\n");
		return WIAS_STATUS_NOPERM;
	}

	if (get_hw_addr (ses) != 0) {
		printf("No pude encontrar la mac address :-(\n");
		return WIAS_STATUS_ERROR;
	}

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		printf("error in ses_init\n");
		return WIAS_STATUS_NODEV;
	}
	ioctl(fd, WIAS_SETDEV, ses->dev);
	close(fd);

	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn void ses_setdestmac( struct session *ses, char *mac )
 */
void ses_setdestmac( struct session *ses, char *mac )
{
	int fd;
	memcpy( ses->dmac, mac, ETH_ALEN );

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		printf("error al setdesmac \n");
		return;
	}
	ioctl(fd, WIAS_ADDMACDST, ses->dmac);
	ioctl(fd, WIAS_ADDMACSRC, ses->smac);
	close(fd);
}

/**
 * @fn ses_setmiscioctl( struct session *ses )
 */
void ses_setmiscioctl( struct session *ses )
{
	int fd;
	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		printf("error al setmiscioctl\n");
		return;
	}
	ioctl(fd, WIAS_ADDTIMER);
	ioctl(fd, WIAS_INITSEQ);
	close(fd);
}

/**
 * @fn void ses_default( struct session *ses )
 */
WIAS_STATUS ses_default( struct session *ses )
{
	assert(ses);

	ses->sock = -1;
	ses->opt_debug = 0;
	ses->opt_daemonize = 1;
	ses->log_to_fd = fileno (stdout);
	memcpy( ses->dmac, MAC_BCAST_ADDR, ETH_ALEN );
	memcpy( ses->smac, MAC_BCAST_ADDR, ETH_ALEN );

	if (options_from_file(ses,WIAS_OPTIONS_FILE) != WIAS_STATUS_SUCCESS ) {
		printf("Error while trying to read file %s\n",WIAS_OPTIONS_FILE);
		return WIAS_STATUS_FILEERROR;
	}
	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn int OpenSap()
 */
int OpenSap( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		printf("\nOpenning SAP...");
		wias_send_open_sap_request( ses ,"\x00\x00\x00\x00\xae\xe0\xa3\x91");
		s = wias_get_open_sap_confirm( ses );
	} while(s!= WIAS_STATUS_SUCCESS);
	printf("OK (SAP=%d)\n",ses->SAP);
	return 0;
}

/**
 * @fn int Connect( struct session *ses )
 */
int Connect( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		printf("\nConnecting...");
		wias_send_connect_request( ses );
		s = wias_get_connect_confirm( ses );
		printf(" (%x)",s);
		if( s == WIAS_STATUS_BADLOGIN ) {
			printf("\nInvalid username or passwrd\n");
			exit(1);
		} else if ( s == WIAS_STATUS_BADVPN ) {
			printf("\nInvalid VPN number\n");
			exit(2);
		}
	} while (s!=WIAS_STATUS_SUCCESS);
	printf(" OK\n");

	return 0;
}


int main (int argc, char **argv)
{
	char buf[MAX_PACKET];
	int len=sizeof(buf);
	fd_set rfds,wfds;
	int want_to_read, want_to_write, proto_state;

	struct session *ses = &ses_table[0];

	printf("wias v"WIAS_VERSION"\n");

	memset(ses,0,sizeof(struct session));

	if( ses_default( ses ) != WIAS_STATUS_SUCCESS )
		return -1;

	if( ses_init( ses ) != WIAS_STATUS_SUCCESS )
		return -2;

	want_to_read=0;
	want_to_write=1;
	proto_state=WIAS_PROTO_START;

#define ONLY_READ()		want_to_read=1;	want_to_write=0
#define ONLY_WRITE()	want_to_read=0;	want_to_write=1

	while (1) {
		FD_ZERO(&rfds);
		FD_ZERO(&wfds);
		if (want_to_read) FD_SET(ses->sock,&rfds);
		if (want_to_write) FD_SET(ses->sock,&wfds);

		if (WIAS_PROTO_START == state) state = WIAS_PROTO_DISCOVER_REQUEST;
		switch (select(ses->sock+1,&rfds,&wfds,NULL,NULL)) {
			case	0:	// time out
				break;
			case	-1:	// error
				break;
			default:	// something set
				if (FD_ISSET(ses->sock,&rfds)) {	// can read
					switch (state) {
						case WIAS_PROTO_DISCOVER_CONFIRM:	// waiting for a discover confirm
							break;
					}
				}
				if (FD_ISSET(ses->sock,&wfds)) {	// can write
					switch (state) {
						case WIAS_PROTO_DISCOVER_REQUEST:	// send a discover request
							wias_send_discover_request( ses );
							state = WIAS_PROTO_DISCOVER_CONFIRM;
							ONLY_READ();
							break;
					}
				}
				break;
		}
	}
	wias_get_packet( ses, buf, &len );
	ses_setdestmac( ses, &buf[6] );

	OpenSap(ses);

	Connect(ses);

	ses_setmiscioctl( ses );

	exit(0);
}
