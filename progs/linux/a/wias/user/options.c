/*
 */

#include <stdio.h>
#include <assert.h>

#include "wiasd.h"
#include "wias.h"

static WIAS_STATUS read_until_eol( FILE *fp )
{
	int c;
	do {
		c = getc( fp );
		if( c == EOF )
			return WIAS_STATUS_FILEERROR;
	} while( c !='\n');

	return WIAS_STATUS_SUCCESS;
}
static WIAS_STATUS get_word( FILE *fp, char *buf, int buflen )
{
	int c;
	int i;
	assert( fp );

	memset(buf,0,buflen);
	/* saca posibles comentarios y whitespaces */
again:
	c = getc( fp );
	if( c == EOF )
		return WIAS_STATUS_FILEERROR;

	if( c == '\n' || c==' ' || c == '\t')
		goto again;

	if( c == '#' ) {
		if( read_until_eol( fp ) != WIAS_STATUS_SUCCESS )
			return WIAS_STATUS_FILEERROR;
		goto again;
	}

	/* empieza el word */
	i=0;
	buf[i++] = (char) c;

	do {
		c = getc( fp );
		buf[i++] = (char) c;
		if(i >= buflen)
			break;
	} while ( c!=EOF && c!='\n' && c!=' ' && c!='\t' );

	buf[i-1]=0;

	PDEBUG("Word leido:%s\n",buf);
	return WIAS_STATUS_SUCCESS;
}

int options_from_file(struct session *ses, char *path )
{
	FILE *fp;
	char username[MAX_USERNAME];
	char userpasswd[MAX_USERPASSWD];
	char uservpn[MAX_USERVPN];
	char ifn[IFNAMSIZ];

	assert(ses);
	assert(path);


	fp = fopen(path,"r");
	if( fp == NULL  )
		return WIAS_STATUS_FILEERROR;

	if (get_word( fp, username, sizeof(username)) != WIAS_STATUS_SUCCESS)
		goto error;
	if (get_word( fp, userpasswd, sizeof(userpasswd)) != WIAS_STATUS_SUCCESS)
		goto error;
	if (get_word( fp, uservpn, sizeof(uservpn)) != WIAS_STATUS_SUCCESS)
		goto error;
	if (get_word( fp, ifn, sizeof(ifn)) != WIAS_STATUS_SUCCESS)
		goto error;

	strncpy (ses->dev, ifn, sizeof(ses->dev));
	strncpy (ses->username, username, sizeof(ses->username));
	strncpy (ses->userpasswd, userpasswd, sizeof(ses->userpasswd));
	strncpy (ses->uservpn, uservpn, sizeof(ses->uservpn) );

	fclose( fp );
	return WIAS_STATUS_SUCCESS;

error:
	fclose( fp );
	return WIAS_STATUS_FILEERROR;
}
