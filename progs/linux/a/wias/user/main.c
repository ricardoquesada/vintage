/*	$Id: main.c,v 1.8 2000/11/08 16:28:46 riq Exp $	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 */
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <asm/ioctl.h>
#include <unistd.h>
#include <assert.h>

#include "wiasd.h"
#include "wias.h"

struct session ses_table[N_SES];

/**
 * @fn int create_raw_socket (unsigned short type)
 */
int create_raw_socket (unsigned short type)
{
	int optval = 1, rv;

	if ((rv = socket (PF_INET, SOCK_PACKET, htons (type))) < 0) {
		fprintf(stderr,"socket: %m");
		return -1;
	}

	if (setsockopt (rv, SOL_SOCKET, SO_BROADCAST, &optval, sizeof (optval)) < 0) {
		fprintf(stderr,"setsockopt: %m");
		return -1;
	}

	return rv;
}

/**
 * @fn int get_hw_addr (struct session *ses) 
 */
int get_hw_addr (struct session *ses) 
{
	struct ifreq ifr;

	assert(ses);

	strncpy (ifr.ifr_name, ses->dev, sizeof (ifr.ifr_name));

	if (ioctl (ses->sock, SIOCGIFHWADDR, &ifr) < 0) {
		fprintf(stderr,"ioctl(SIOCGIFHWADDR): %m\n");
		return -1;
	}

	if (ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) {
		fprintf(stderr,"interface %s is not Ethernet!\n", ses->dev);
		return -1;
	}

	memcpy (ses->smac, ifr.ifr_hwaddr.sa_data, ETH_ALEN);

	PDEBUG("we have hware address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n"
			,(unsigned char)ses->smac[0]
			,(unsigned char)ses->smac[1]
			,(unsigned char)ses->smac[2]
			,(unsigned char)ses->smac[3]
			,(unsigned char)ses->smac[4]
			,(unsigned char)ses->smac[5] );

	return 0;
}

/**
 * @fn void ses_cleanup (struct session *ses)
 */
void ses_cleanup (struct session *ses)
{
	assert(ses);
	close (ses->sock);
}

/**
 * @fn int send_packet (struct session *ses, void *packet, int len )
 */
int send_packet (struct session *ses, void *packet, int len )
{
	struct sockaddr addr;
	int c;

	assert(ses);
	assert(packet);

	memset (&addr, 0, sizeof (addr));
	strcpy (addr.sa_data, ses->dev);

	if ((c = sendto (ses->sock, packet, len, 0, &addr, sizeof (addr))) < 0) {
		PDEBUG("sendto (send_packet): %m");
	}

	return c;
}

/**
 * @fn int ses_init (struct session *ses)
 */
WIAS_STATUS ses_init (struct session *ses)
{
	int fd;

	if ((ses->sock = create_raw_socket (ETH_P_WIAS_CTRL)) < 0) {
		fprintf(stderr,"unable to create raw socket\n");
		return WIAS_STATUS_NOPERM;
	}

	if (get_hw_addr (ses) != 0) {
		fprintf(stderr,"No pude encontrar la mac address :-(\n");
		return WIAS_STATUS_ERROR;
	}

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error in ses_init\n");
		return WIAS_STATUS_NODEV;
	}
	ioctl(fd, WIAS_SETDEV, ses->dev);
	close(fd);

	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn void ses_setdestmac( struct session *ses, char *mac )
 */
void ses_setdestmac( struct session *ses, char *mac )
{
	int fd;
	memcpy( ses->dmac, mac, ETH_ALEN );

	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error al setdesmac\n");
		return;
	}
	ioctl(fd, WIAS_ADDMACDST, ses->dmac);
	ioctl(fd, WIAS_ADDMACSRC, ses->smac);
	close(fd);
}

/**
 * @fn ses_setmiscioctl( struct session *ses )
 */
void ses_setmiscioctl( struct session *ses )
{
	int fd;
	fd = open( "/dev/wiasS0", O_RDONLY );
	if( fd < 0 ) {
		fprintf(stderr,"error al setmiscioctl\n");
		return;
	}
	ioctl(fd, WIAS_ADDTIMER);
	ioctl(fd, WIAS_INITSEQ);
	close(fd);
}

/**
 * @fn void ses_default( struct session *ses )
 */
WIAS_STATUS ses_default( struct session *ses )
{
	assert(ses);

	ses->sock = -1;
	ses->opt_debug = 0;
	ses->opt_daemonize = 1;
	ses->log_to_fd = fileno (stdout);
	memcpy( ses->dmac, MAC_BCAST_ADDR, ETH_ALEN );
	memcpy( ses->smac, MAC_BCAST_ADDR, ETH_ALEN );

	if (options_from_file(ses,WIAS_OPTIONS_FILE) != WIAS_STATUS_SUCCESS ) {
		fprintf(stderr,"Error while trying to read file %s\n",WIAS_OPTIONS_FILE);
		return WIAS_STATUS_FILEERROR;
	}
	return WIAS_STATUS_SUCCESS;
}

/**
 * @fn int OpenSap()
 */
int OpenSap( struct session *ses )
{
	WIAS_STATUS s;

	assert(ses);

	do {
		fprintf(stderr,"\nOpenning SAP...");
		wias_send_open_sap_request( ses ,"\x00\x00\x00\x00\xae\xe0\xa3\x91");
		s = wias_get_open_sap_confirm( ses );
	} while(s!= WIAS_STATUS_SUCCESS);
	fprintf(stderr,"OK (SAP=%d)\n",ses->SAP);
	return 0;
}

/**
 * @fn int Connect( struct session *ses )
 */
int Connect( struct session *ses )
{
	WIAS_STATUS s=WIAS_STATUS_ERROR;
	fd_set rfds;
	struct timeval timeout;
	int retries=8;

	assert(ses);

	do {
		fprintf(stderr,"\nConnecting...");
		wias_send_connect_request( ses );

		FD_ZERO(&rfds);
		FD_SET(ses->sock,&rfds);
		timeout.tv_sec=15;
		timeout.tv_usec=0;

		switch (select(ses->sock+1,&rfds,NULL,NULL,&timeout)) {
			case	0:	// time out
				fprintf(stderr," timeout: %d more retries",--retries);
				if (!retries) return 1;
				break;
			case	-1:	// error
				break;
			default:	// something set
				s = wias_get_connect_confirm( ses );
				fprintf(stderr," (%x)",s);
				switch (s) {
					case	WIAS_STATUS_BADLOGIN:
						fprintf(stderr,"\nInvalid username or passwrd\n");
						exit(1);
					case	WIAS_STATUS_BADVPN:
						fprintf(stderr,"\nInvalid VPN number\n");
						exit(2);
					case	WIAS_STATUS_TIMEOUT:
						fprintf(stderr,"\n timeout received. Forcing a full retry\n");
						return 1;
					case	WIAS_STATUS_BADTYPE:
						fprintf(stderr," packet out of sequence");
						break;
					default:		// evitar warnings en la compilacion
						break;
				}
		}

	} while (s!=WIAS_STATUS_SUCCESS);
	fprintf(stderr," OK\n");

	return 0;
}


int main (int argc, char **argv)
{
	char buf[MAX_PACKET];
	int len=sizeof(buf);

	struct session *ses = &ses_table[0];

	fprintf(stderr,"wias v"WIAS_VERSION"\n");

	memset(ses,0,sizeof(struct session));

	if( ses_default( ses ) != WIAS_STATUS_SUCCESS )
		return -1;

	if( ses_init( ses ) != WIAS_STATUS_SUCCESS )
		return -2;

	ses->SAP=1;
	wias_send_close_sap_request(ses);
	wias_get_packet( ses, buf, &len );
	do {
		wias_send_discover_request( ses );
		wias_get_packet( ses, buf, &len );
		ses_setdestmac( ses, &buf[6] );

		OpenSap(ses);
	} while (0!=Connect(ses));

	ses_setmiscioctl( ses );

	exit(0);
}
