/*	$Id: wiasd.h,v 1.1.1.1 2000/11/05 16:47:57 riq Exp $	*/
/*
 *	WaveAccess 3500 driver
 *	by gerardo richarte, ricardo quesada
 */
#ifndef __WIASD_H
#define __WIASD_H

#include <stdio.h>		/* stdio */
#include <stdlib.h>		/* strtoul(), realloc() */
#include <unistd.h>		/* STDIN_FILENO,exec */
#include <string.h>		/* memcpy() */
#include <errno.h>		/* errno */
#include <signal.h>
#include <getopt.h>
#include <stdarg.h>
#include <syslog.h>
#include <paths.h>

#include <sys/types.h>		/* socket types	*/
#include <asm/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>		/* ioctl() */
#include <sys/select.h>
#include <sys/socket.h>		/* socket() */
#include <net/if.h>		/* ifreq struct	*/
#include <net/if_arp.h>
#include <netinet/in.h>

#include <net/ethernet.h>
#ifdef DEBUG_PDEBUG
#define PDEBUG(a...) printf(a)
#else
#define PDEBUG(a...)
#endif

/* Bigger than the biggest ethernet packet we'll ever see, in bytes */
#define MAX_PACKET			2048

#define MAX_USERNAME 	(100)
#define MAX_USERPASSWD	(100)
#define MAX_USERVPN	(100)

#define WIAS_OPTIONS_FILE	"/etc/wias.conf"
/* ethernet broadcast address */
#define MAC_BCAST_ADDR	"\xff\xff\xff\xff\xff\xff"

struct session {
	int pid;			/**< process ID  */
	int state;			/**< Current state */
	int sock;			/**< raw socket */
	char dev[IFNAMSIZ];		/**< dev name */
	unsigned char dmac[ETH_ALEN];	/**< destination MAC */
	unsigned char smac[ETH_ALEN];	/**< Source MAC */
	char SAP;			/**< SAP */
	int opt_daemonize;
	int opt_debug;
	int detached;
	int log_to_fd;
	char username[MAX_USERNAME];	/**< username */
	char userpasswd[MAX_USERPASSWD];/**< password */
	char uservpn[MAX_USERVPN];	/**< VPN */
} __attribute__ ((packed));


int send_packet (struct session *ses, void *packet, int len );
int options_from_file(struct session *ses, char *path );


#endif /* __WIASD_H */
