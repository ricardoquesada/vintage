/*	$Id$	*/
/****************************************************************************
 	Funcion basada en la del cliente.
	Comportamiento del Robot a nivel BNP
 ****************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#include <config.h>
#include <gnome.h>

#include "robot_cliente.h"
#include "robot_random_board.h"
#include "robot_ai.h"
#include "../gbnserver/bnwrite.h"
#include "../gbnserver/protocol.h"
#include "../gbnserver/parser.h"
#include "../gbnserver/riq-net.h"

/* nombre y skill del robot */

struct { 
	char *nombre;
	robotai_t ai;
} robot_nombres[] = {
	{ "homer", ROBOT_AI_VERYMORON },
	{ "bart", ROBOT_AI_MORON },
	{ "marge", ROBOT_AI_AVERAGE },
	{ "lisa", ROBOT_AI_HI },

	{ "ana" ,ROBOT_AI_AVERAGE },
	{ "javi", ROBOT_AI_AVERAGE },
	{ "emi", ROBOT_AI_AVERAGE },
	{ "nacho", ROBOT_AI_AVERAGE },

	{ "edu", ROBOT_AI_AVERAGE },
	{ "gonza", ROBOT_AI_AVERAGE },
	{ "kato", ROBOT_AI_AVERAGE },
	{ "gera", ROBOT_AI_AVERAGE },

	{ "wari", ROBOT_AI_AVERAGE },
	{ "riq", ROBOT_AI_AVERAGE },
	{ "pipa", ROBOT_AI_AVERAGE },
	{ "beto", ROBOT_AI_AVERAGE },

	{ "raul", ROBOT_AI_AVERAGE },
	{ "max", ROBOT_AI_AVERAGE },
	{ "juan", ROBOT_AI_AVERAGE },
	{ "pipito", ROBOT_AI_AVERAGE },
	
	{ "chichi", ROBOT_AI_AVERAGE },
	{ "pichu", ROBOT_AI_AVERAGE },
	{ "cachi", ROBOT_AI_AVERAGE },
	{ "roque", ROBOT_AI_AVERAGE },

	{ "charly", ROBOT_AI_MORON },
	{ "chupete", ROBOT_AI_MORON },
	{ "tula", ROBOT_AI_AVERAGE },
	{ "loko", ROBOT_AI_MORON },

	{ "cachito", ROBOT_AI_VERYMORON },
	{ "sandro", ROBOT_AI_AVERAGE },
	{ "vince", ROBOT_AI_VERYMORON },
	{ "corcho", ROBOT_AI_AVERAGE },

	{ "bombi", ROBOT_AI_AVERAGE },
	{ "edna", ROBOT_AI_AVERAGE },
	{ "viole", ROBOT_AI_AVERAGE },
	{ "papi", ROBOT_AI_AVERAGE },

	{ "mami", ROBOT_AI_AVERAGE },
	{ "arjona", ROBOT_AI_AVERAGE },
	{ "piojo", ROBOT_AI_MORON },
	{ "valerio", ROBOT_AI_AVERAGE },

	{ "sofi", ROBOT_AI_VERYMORON },
	{ "miko", ROBOT_AI_AVERAGE },
	{ "onda", ROBOT_AI_AVERAGE },
	{ "llave", ROBOT_AI_AVERAGE }
};
static int rnombres = sizeof(robot_nombres) / sizeof(robot_nombres[0] );

/*****************************************************************************
			funiones
			auxiliares
******************************************************************************/
/* convierte un char[10][10] a un char[100] */
static void iwtable( char *dest)
{
	int i,x,y;
	x=0;
	y=0;

	for(i=0;i<100;i++) {
		dest[i]=cliente.mitabla[x][y];
		x++;
		if(x>=10) { 
			x=0;
			y++;
		}
	}
}
/* convierte un char[100] a un char[10][10] */
static void poner_en_board( int d, char *orig)
{
	int i,x,y;
	x=0;
	y=0;

	for(i=0;i<100;i++) {
		cliente.boards[d][x][y]=orig[i];
		x++;
		if(x>=10) { 
			x=0;
			y++;
		}
	}
}

/* Esta funcion tiene algo que ver con el AI */
static void que_usrfrom()
{ 
	int i;
	
	for(i=cliente.numjug+1;i<MAXPLAYER;i++) {
		if( cliente.play[i]>=BOARD && cliente.play[i]<=TURN) {
			cliente.usrfrom=i;
			return;
		}
	}
	for(i=0;i<=cliente.numjug;i++) {
		if( cliente.play[i]>=BOARD && cliente.play[i]<=TURN ) {
			cliente.usrfrom=i;
			return;
		}
	}
	printf("gbnrobot: robot %d: que_usrfrom: Error, aca no tendria que llegar\n",cliente.numjug);
}

static void dime_quien_soy( void )
{
	int tmp;

	tmp = ((cliente.random<<1 ) ^ (cliente.random>>1)) % rnombres;
	if(tmp < 0)
		tmp=-tmp;

	strcpy( cliente.mi_nombre,  robot_nombres[tmp].nombre );
	cliente.ai = robot_nombres[tmp].ai;
	
}

/*****************************************************************************
			funciones
			robot_*
******************************************************************************/
static void robot_fire(char *str)
{
	gfloat new_val;
	GtkAdjustment *adj;
	int i,j,x,y,z;
	PARSER p;
	DELIM igualador={ '=','=','=' };
	DELIM separador={ '/',',','/' };
	
	p.igualador = &igualador;
	p.separador = &separador;
	strcpy(p.sig,str );
	j=0;
	do{
		i=parser_init( &p );
		if(p.status && j==0)		/* X */
			x=atoi(p.token);

		else if(p.status && j==1)	/* Y */
			y=atoi(p.token);	
	
		else if(p.status && j==2)	/* Tocado, hundio, etc */
			z=atoi(p.token);	
		j++;
	} while(i);
	if(j!=3) {
		printf("gbnrobot: Error parsing in '"BN_FIRE"'");
		return;
	}
	if(z==HUNDIDO) {
		adj = GTK_PROGRESS (pbar)->adjustment;

		new_val = adj->value - 1;
		if (new_val > adj->upper)
			new_val = adj->lower;

		gtk_progress_set_value (GTK_PROGRESS (pbar), new_val);
	}
}

static void robot_numjug( char *str)
{
	char buf[101];
	int i;
	
	i=atoi(str);
	cliente.numjug=i;

	generar();

	dime_quien_soy();

	iwtable(buf);
	buf[10*10]=0;
	bnwrite(cliente.sock,BN_NAME"=%s;"BN_CLI_VER"="ROBOTVER";"BN_SER_VER";"BN_PROTOCOL";"BN_SEND"=%s",cliente.mi_nombre,buf);

	sprintf(buf,"-> %s[%i] <-",cliente.mi_nombre,cliente.numjug);
	gtk_label_set_text(GTK_LABEL( label), buf);
}
static void robot_ignore()
{
}
static void robot_killmyself()
{
	bnwrite(cliente.sock,BN_EXIT);
	gdk_input_remove( cliente.tag );
	close( cliente.sock );
	gtk_main_quit();
}
static void robot_board_not_ok()
{
	printf("gbnrobot: Robot: You sent an invalid board. Report this bug!\n");
	robot_killmyself();
}
static void robot_board_ok( void )
{
/*	printf("The board's OK. Press 'Start' and enjoy the game\n"); */
}
static void robot_start( char *str)
{
	int i,k,l,m;
	PARSER p;
	DELIM igualador={ '=','=','=' };
	DELIM separador={ '/',',','/' };

	p.igualador = &igualador;
	p.separador = &separador;

	strcpy(p.sig,str );
	
	m=0;k=0;l=0;
	do{
		i=parser_init( &p );
		
		if(p.status && m==0)		/* number of player */
			l=atoi(p.token);

		else if(p.status && m==1) {	/* estado */
			k=atoi(p.token);
			cliente.play[l]=k;
		}
		m++;
		m=m%3;
	} while(i);
	que_usrfrom();
/*	printf("Robot %d: Starting...usrfrom=%i\n",cliente.numjug,cliente.usrfrom); */
}
static void robot_discon( char *str )
{
	int x;
	
	x=atoi(str);
	if( cliente.numjug==x ) {
/*		printf("Robot %i: Me estan desconectando\n",x); */
		robot_killmyself();
	}
	cliente.play[x]=DISCON;
	que_usrfrom();
	return;
}
static void robot_turn( char *str )
{
	if(atoi(str)==cliente.numjug) {
		bnwrite(cliente.sock,BN_READ"=%i",cliente.usrfrom);
		cliente.play[cliente.numjug]=TURN;
	} else
		cliente.play[cliente.numjug]=PLAY;
}

static void robot_read( char *str )
{
	int i,m,l,x,y;
	char buf[10*10];
	PARSER p;
	DELIM igualador={ '=','=','=' };
	DELIM separador={ '/',',','/' };

	if(cliente.play[cliente.numjug]!=TURN) {
/*		printf("Robot %d: No es mi turno \n",cliente.numjug); */
		return;
	}

	p.igualador = &igualador;
	p.separador = &separador;
	strcpy(p.sig,str );
	m=0;
	do{
		i=parser_init( &p );
		if(p.status && m==0)		/* numero de jugador */
			l=atoi(p.token);

		else if(p.status && m==1)	/* board del jugador */
			strncpy(buf,p.token,10*10);
		m++;
	} while(i);
	
	if(l==cliente.usrfrom ) {
		if(cliente.usrfrom == cliente.numjug ) {
/*			printf("Robot %i : usrfrom = numjug",cliente.numjug); */
		}
		poner_en_board(l,buf);
		robot_ai(&x,&y);	
		bnwrite(cliente.sock,BN_FIRE"=%i,%i",x,y);
	} else
		bnwrite(cliente.sock,BN_READ"=%i",cliente.usrfrom);
}
static void robot_lost( char *str)
{
	int x;
	x=atoi(str);
	cliente.play[x]=DISCON;
	
	if(x==cliente.numjug) {
/*		printf("Robot %i:You lost!\n",cliente.numjug); */
		robot_killmyself();
	}
	que_usrfrom();
	return;
}
static void robot_win( int j, char *str)
{
	int a;
	a=atoi(str);
	cliente.play[a]=DISCON;

	if(a==j) {
		/* FIXME: Truco para evitar colgadas */
		sleep(1);
		robot_killmyself();
	}
	return;
}
static void robot_game_over( void )
{
	robot_killmyself();
}

/**********************************************************************
		lookup token, etc
**********************************************************************/
static int robot_lookup_funcion( PARSER *p )
{
	int i;

	struct {
		char *label;
		void (*func) ();
	} tokens[] = {
		{ BN_BOARD_NOT_OK,	robot_board_not_ok },
		{ BN_BOARD_OK,		robot_board_ok },
		{ BN_DISCON,		robot_discon },
		{ BN_GAME_OVER,		robot_game_over },
		{ BN_LOST,		robot_lost },
		{ BN_MESSAGE,		NULL },
		{ BN_NAME,		NULL },
		{ BN_NUMJUG,		robot_numjug },
		{ BN_PROTOCOL,		NULL },
		{ BN_QUMM,		NULL },
		{ BN_READ,		robot_read },
		{ BN_READY_TO_PLAY,	NULL },
		{ BN_REM,		robot_ignore },
		{ BN_SCORES,		NULL },
		{ BN_SER_FULL,		robot_killmyself },
		{ BN_SER_VER,		NULL },
		{ BN_SOL,		robot_killmyself },
		{ BN_START,		robot_start },
		{ BN_STATUS,		NULL },
		{ BN_TEST,		NULL },
		{ BN_TURN,		robot_turn },
		{ BN_WAIT,		robot_killmyself },
		{ BN_WIN,		robot_win },
		{ BN_FIRE,		robot_fire }
	};
	int ntokens = sizeof (tokens) / sizeof (tokens[0]);

	for (i = 0; i < ntokens; i++) {
		if (strcmp( p->token, tokens[i].label )==0 ){
			if (tokens[i].func)
				(tokens[i].func)( p->value );
			else {
/*				printf("Robot %d: Function '%s' is not implemented yet!\n",cliente.numjug,tokens[i].label); */
			}
			return TRUE;
		}
	}
	return FALSE;
}

static int robot_proceso( gpointer data, int sock, GdkInputCondition GDK_INPUT_READ )
{
	int i,j;
	PARSER p;
	DELIM igualador={ '=', ':', '=' };
	DELIM separador={ ';', ';', ';' };

	char str[PROT_MAX_LEN];

	p.igualador = &igualador;
	p.separador = &separador;
	str[0]=0;

/*	printf("ROBOT: Entrando a robot_proceso %i\n",sock); */
	/* FIXME: uso riq_net_gets. En el futuro reemplazar por gnome_net_gets */
	j=riq_net_readline( sock, str,PROT_MAX_LEN );
	if( j<1 ) {
		robot_killmyself();
		return -1;
	}
	
	strcpy(p.sig,str );
	
	do{
		i=parser_init( &p );
		if(p.status) {
			if( !robot_lookup_funcion( &p ) ) {
				printf("gbnrobot: Token '%s' no encontrado\n",p.token);
			}
		}
	} while(i);
/*	printf("Saliendo de robot_proceso %i\n",sock); */
	return 0;
}

static int robot_init( void )
{
	cliente.sock = riq_net_connect_unix( "/tmp/batnav-socket" );

	if(cliente.sock<0) {
		printf(_("gbnrobot Error: Is the server running?\n"));
		return -1;
	}
	cliente.tag = gdk_input_add( cliente.sock, GDK_INPUT_READ, (GdkInputFunction) robot_proceso, (gpointer) NULL );
    
	return 0;
}

static void message_dlg_clicked(GtkWidget *widget, int button,gpointer data)
{
	if (button == 0) /* Yes */
		robot_killmyself();
	else /* No */
		gnome_dialog_close(GNOME_DIALOG(widget));
}

static gboolean g_do_exit(GtkWidget *widget, gpointer data)
{
	static GtkWidget *box = NULL;

	if (box == NULL) {
		box = gnome_message_box_new (_("Close robot ?"),
			GNOME_MESSAGE_BOX_QUESTION,
			GNOME_STOCK_BUTTON_YES,
			GNOME_STOCK_BUTTON_NO,
			NULL);
		gtk_signal_connect (GTK_OBJECT (box), "clicked",
			GTK_SIGNAL_FUNC (message_dlg_clicked), NULL);

		gtk_window_set_modal (GTK_WINDOW(box),TRUE);
		gnome_dialog_close_hides(GNOME_DIALOG(box), TRUE);
	}
	gtk_widget_show (box);
	return TRUE;
}
static void init_X( )
{
	window = gnome_app_new ("gbnrobot", _("Gnome Batalla Naval robot") );
	gtk_window_set_policy(GTK_WINDOW(window), FALSE, FALSE, TRUE);

	gtk_widget_realize (window);
	gtk_signal_connect ( GTK_OBJECT( window), "destroy",
		GTK_SIGNAL_FUNC( g_do_exit ),
		GTK_OBJECT(window) );
	gtk_signal_connect ( GTK_OBJECT( window), "delete_event",
		GTK_SIGNAL_FUNC( g_do_exit ),
		GTK_OBJECT(window) );

	box = gtk_vbox_new( FALSE, 0);
	gnome_app_set_contents(GNOME_APP(window), box );
   
	gtk_container_border_width ( GTK_CONTAINER(box), 0);
  
  	pix = gnome_pixmap_file("gbnrobot.png");
	if( pix ) {
		imagen = gnome_pixmap_new_from_file( pix );
		gtk_widget_show( imagen );
		g_free( pix );
		gtk_box_pack_start( GTK_BOX(box),imagen,TRUE,TRUE,0);
	}

	adj = (GtkAdjustment *) gtk_adjustment_new (20, 1, 20, 0, 0, 0);

	pbar = gtk_progress_bar_new_with_adjustment (adj);
	
	gtk_progress_set_format_string (GTK_PROGRESS (pbar), "[%v/%u] %p%%");
	gtk_progress_set_show_text (GTK_PROGRESS (pbar), TRUE );

	gtk_box_pack_start (GTK_BOX (box), pbar, FALSE, FALSE, 5);
	
	label = gtk_label_new (_("Connecting...")); 
	gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 5);

	/* Principal */   
	gtk_widget_show_all( window);
}
/****************************************************************************
 *                           MAIN * MAIN * MAIN
 ****************************************************************************/
int main (int argc, char *argv[])
{
	int i;
	GnomeClient *client;

	bindtextdomain( PACKAGE, GNOMELOCALEDIR );
	textdomain( PACKAGE );
  
	
	gnome_init("gbatnav", ROBOTVER, argc, argv );
	
	client = gnome_master_client ();

	init_X();

	if( robot_init() < 0 ) exit(-1);
	bnwrite( cliente.sock,BN_NUMJUG);
	

	gtk_main ();
	gtk_object_unref(GTK_OBJECT(client));
	return 0;
}
