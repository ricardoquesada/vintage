/*	$Id$	*/
/******************************************************************************
 *                        FUNCIONES GNOMES  y/o GTK                           *
 *****************************************************************************/

#include <string.h>

#include <config.h>
#include <gnome.h>

#include "../gbnserver/protocol.h"
#include "gbnclient.h"
#include "cliente.h"
#include "proceso.h"
#include "pantalla.h"
#include "random_board.h"
#include "configure.h"
#include "sendmsg.h"
#include "version.h"

/* pixmaps */
#include "about.xpm"
#include "agua.xpm"
#include "icono.xpm"
#include "fondo.xpm"
#include "barco1.xpm"
#include "barco1_t.xpm"
#include "barco1_h.xpm"
#include "barco2h.xpm"
#include "barco2h_t.xpm"
#include "barco2h_h.xpm"
#include "barco2v.xpm"
#include "barco2v_t.xpm"
#include "barco2v_h.xpm"
#include "barco3h.xpm"
#include "barco3h_t.xpm"
#include "barco3h_h.xpm"
#include "barco3v.xpm"
#include "barco3v_t.xpm"
#include "barco3v_h.xpm"
#include "barco4h.xpm"
#include "barco4h_t.xpm"
#include "barco4h_h.xpm"
#include "barco4v.xpm"
#include "barco4v_t.xpm"
#include "barco4v_h.xpm"
#include "robot.xpm"
#include "random.xpm"

static void message_dlg_clicked(GtkWidget *widget, int button,gpointer data)
{
	if (button == 0) /* Yes */
		gtk_main_quit();
	else /* No */
		gnome_dialog_close(GNOME_DIALOG(widget));
}

gboolean g_do_exit(GtkWidget *widget, gpointer data)
{
	static GtkWidget *box = NULL;

	if (box == NULL) {
		box = gnome_message_box_new (_("gbnclient: Really quit?"),
			GNOME_MESSAGE_BOX_QUESTION,
			GNOME_STOCK_BUTTON_YES,
			GNOME_STOCK_BUTTON_NO,
			NULL);
		gtk_signal_connect (GTK_OBJECT (box), "clicked",
			GTK_SIGNAL_FUNC (message_dlg_clicked), NULL);

		gtk_window_set_modal (GTK_WINDOW(box),TRUE);
		gnome_dialog_close_hides(GNOME_DIALOG(box), TRUE);
	}
	gtk_widget_show (box);
	return TRUE;
}


static void g_cerrar(GtkWidget *widget, int button,gpointer data)
{
	gnome_dialog_close(GNOME_DIALOG(widget));
}

gboolean g_box_lost()
{
	static GtkWidget *box = NULL;

	if (box == NULL ) {
		box = gnome_message_box_new (_("Game Over: You lost!"),
			GNOME_MESSAGE_BOX_INFO,
			GNOME_STOCK_BUTTON_OK,
			NULL);
		
		gtk_signal_connect (GTK_OBJECT (box), "clicked",
			GTK_SIGNAL_FUNC (g_cerrar), NULL);

		gtk_window_set_modal( GTK_WINDOW(box),TRUE);
		gnome_dialog_close_hides(GNOME_DIALOG(box), TRUE);
	}
	gtk_widget_show( box );
	return TRUE;
}

gboolean g_box_win()
{
	static GtkWidget *box = NULL;

	if (box == NULL ) {
		box = gnome_message_box_new (_("Game Over: You are the Winner!"),
			GNOME_MESSAGE_BOX_INFO,
			GNOME_STOCK_BUTTON_OK,
			NULL);
		
		gtk_signal_connect (GTK_OBJECT (box), "clicked",
			GTK_SIGNAL_FUNC (g_cerrar), NULL);

		gtk_window_set_modal( GTK_WINDOW(box),TRUE);
		gnome_dialog_close_hides(GNOME_DIALOG(box), TRUE);
	}
	gtk_widget_show( box );
	return TRUE;
}

void about( GtkWidget *widget, gpointer data )
{
	GtkWidget *about;
	gchar *authors[] = {
		"riq (riq@ciudad.com.ar)",
		NULL
	};
   
	about = gnome_about_new (_("Batalla Naval client"), VERSION,
			"(C) 1998,99 Ricardo C. Quesada",
			(const char**) authors,
			_("A multiplayer, multirobot, networked battleship game."),
			"gnome-gbatnav.png");
	gtk_widget_show (about);
}

GnomeUIInfo gamemenu[] = 
{
	{ GNOME_APP_UI_ITEM, N_("_Generate random board"), NULL, generar, NULL, NULL,
		GNOME_APP_PIXMAP_DATA, random_xpm, 0, 0, NULL },

	{ GNOME_APP_UI_ITEM, N_("_Launch a robot in server"), NULL, init_robot, NULL, NULL, 
		GNOME_APP_PIXMAP_DATA, robot_xpm, 0, 0, NULL }, 

	GNOMEUIINFO_MENU_SCORES_ITEM( NULL, NULL),
	GNOMEUIINFO_SEPARATOR, 
	GNOMEUIINFO_MENU_EXIT_ITEM(g_do_exit,NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo settingsmenu[] = {
	GNOMEUIINFO_MENU_PREFERENCES_ITEM( configure, NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] = 
{
	GNOMEUIINFO_HELP("gbatnav"),
	GNOMEUIINFO_MENU_ABOUT_ITEM(about,NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = 
{
        GNOMEUIINFO_MENU_GAME_TREE(gamemenu),
	GNOMEUIINFO_MENU_SETTINGS_TREE(settingsmenu),
        GNOMEUIINFO_MENU_HELP_TREE(helpmenu),
	GNOMEUIINFO_END
};

GnomeUIInfo main_toolbarinfo[] =
{
	{GNOME_APP_UI_ITEM, N_("Preferences..."), N_("Preferences..."),
	configure, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PROP, 0, 0, NULL},
	
	{GNOME_APP_UI_ITEM, N_("Random"), N_("Generate a random board"),
	generar, NULL, NULL,
	GNOME_APP_PIXMAP_DATA, random_xpm, 0, 0, NULL},

	GNOMEUIINFO_SEPARATOR,

	{GNOME_APP_UI_ITEM, N_("Robot"), N_("Launch a robot in server"),
	init_robot, NULL, NULL,
	GNOME_APP_PIXMAP_DATA, robot_xpm, 0, 0, NULL},
  
	GNOMEUIINFO_SEPARATOR,
	
	{GNOME_APP_UI_ITEM, N_("Exit"), N_("Quit Batalla Naval"),
	g_do_exit, NULL, NULL,
	GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 0, 0, NULL},

	{GNOME_APP_UI_ENDOFINFO}
};

/*****************************************************************************
 *                                   INIT X                                  *
 *****************************************************************************/
void init_X( )
{
	GtkTooltips *tooltips;

#ifdef ENABLE_NLS
# define ELEMENTS(x) (sizeof(x) / sizeof(x[0])) 
	{
		int i;
		for (i = 0; i < ELEMENTS(mainmenu); i++)
			mainmenu[i].label = gettext(mainmenu[i].label);
		for (i = 0; i < ELEMENTS(gamemenu); i++)
			gamemenu[i].label = gettext(gamemenu[i].label);
		for (i = 0; i < ELEMENTS(helpmenu); i++)
			helpmenu[i].label = gettext(helpmenu[i].label);
	}
#endif /* ENABLE_NLS */
   
	window = gnome_app_new ("gbnclient", _("Gnome Batalla Naval client") );
	gtk_window_set_policy(GTK_WINDOW(window), FALSE, FALSE, TRUE);

	gnome_app_create_menus(GNOME_APP(window), mainmenu);
	gnome_app_create_toolbar(GNOME_APP(window), main_toolbarinfo);

	gtk_menu_item_right_justify(GTK_MENU_ITEM(mainmenu[1].widget));
	gtk_widget_realize (window);
   
	gtk_signal_connect ( GTK_OBJECT( window), "destroy",
		GTK_SIGNAL_FUNC( g_do_exit ),
		GTK_OBJECT(window) );
	gtk_signal_connect ( GTK_OBJECT( window), "delete_event",
		GTK_SIGNAL_FUNC( g_do_exit ),
		GTK_OBJECT(window) );
  
	tooltips=gtk_tooltips_new();
	gtk_object_set_data (GTK_OBJECT (window), "tooltips", tooltips);


	/* Box que contiene a box_left, box_button, box_right  */
	vbox = gtk_vbox_new ( FALSE, 0);
	gnome_app_set_contents(GNOME_APP(window), vbox );
   
	gtk_container_border_width ( GTK_CONTAINER(vbox), 0);
	gtk_widget_show ( vbox ); 

	/* box horizontal */
	hbox = gtk_hbox_new ( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(vbox), hbox );
	gtk_container_border_width ( GTK_CONTAINER(hbox), 0);
	gtk_widget_show( hbox );
   
	/******************** LEFT *******************/
	notebook_left = gtk_notebook_new();
	gtk_notebook_set_tab_pos( GTK_NOTEBOOK(notebook_left),GTK_POS_TOP );
	gtk_box_pack_start( GTK_BOX(hbox),notebook_left,TRUE,TRUE,0);
	gtk_widget_show (notebook_left);
   
	/* respecto al drawing_left */
	drawing_left = gtk_drawing_area_new();
	gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_left),200,200);
	gtk_widget_show( drawing_left );
   

	/* se�ales de la drawing area */
	gtk_signal_connect (GTK_OBJECT (drawing_left), "expose_event",
		(GtkSignalFunc) expose_event, NULL);
	gtk_signal_connect (GTK_OBJECT (drawing_left), "button_press_event",
		(GtkSignalFunc) button_press_event, NULL);
	gtk_widget_set_events (drawing_left, GDK_EXPOSURE_MASK
		|GDK_BUTTON_PRESS_MASK);

   
	/* respecto al drawing_about */
	hbox_text_help = gtk_hbox_new ( FALSE, 0);
	gtk_widget_show( hbox_text_help );
   
	text_help = gtk_text_new(NULL,NULL);
	gtk_box_pack_start( GTK_BOX(hbox_text_help), text_help, TRUE,TRUE,0);
	gtk_widget_show(text_help);
   
	vscrollbar_help = gtk_vscrollbar_new (GTK_TEXT (text_help)->vadj);
	gtk_box_pack_start( GTK_BOX(hbox_text_help), vscrollbar_help, FALSE,TRUE,0);
	gtk_widget_show (vscrollbar_help);
   
	label_left = gtk_label_new(_("My board"));
	gtk_notebook_append_page ( GTK_NOTEBOOK(notebook_left),drawing_left,label_left);
	label_left = gtk_label_new(_("Quick Help"));
	gtk_notebook_append_page ( GTK_NOTEBOOK(notebook_left),hbox_text_help,label_left);

	bn_help();
      
	/* center */
   
	vbox_buttons = gtk_vbox_new ( FALSE, 10);
	gtk_container_add ( GTK_CONTAINER (hbox), vbox_buttons );
	gtk_container_border_width( GTK_CONTAINER(vbox_buttons),10);
	gtk_widget_show( vbox_buttons );
   
	button_connect = gtk_button_new();
	button_connect_label = gtk_label_new(_("Connect"));
	gtk_misc_set_alignment( GTK_MISC(button_connect_label),0.5,0.5);
	gtk_container_add( GTK_CONTAINER( button_connect), button_connect_label);
	gtk_widget_show( button_connect_label);

	gtk_box_pack_start( GTK_BOX(vbox_buttons), button_connect, TRUE,TRUE,0);
	gtk_signal_connect_object( GTK_OBJECT (button_connect), "clicked",
		GTK_SIGNAL_FUNC(init_cliente),
		GTK_OBJECT (window) );
	gtk_widget_show(button_connect);
	gtk_tooltips_set_tip (tooltips,button_connect,
		_("Connect to/disconnect from the server"), 
		_("Connect to/disconnect from the server"));
   
   
	button_sendboard = gtk_button_new();
	button_sendboard_label = gtk_label_new(_("Send ships"));
	gtk_misc_set_alignment( GTK_MISC(button_sendboard_label),0.5,0.5);
	gtk_container_add( GTK_CONTAINER( button_sendboard), button_sendboard_label);
	gtk_widget_show( button_sendboard_label);
	gtk_box_pack_start( GTK_BOX(vbox_buttons), button_sendboard,TRUE,TRUE,0);
	gtk_signal_connect_object( GTK_OBJECT (button_sendboard), "clicked",
		GTK_SIGNAL_FUNC(play),
		GTK_OBJECT (window) );
	gtk_widget_set_sensitive(button_sendboard,FALSE);
	gtk_widget_show(button_sendboard);
	gtk_tooltips_set_tip (tooltips,button_sendboard,
		_("Send ships to the server/ Start the game"),
		_("Send ships to the server/ Start the game"));

   
	button_sendmsg = gtk_button_new_with_label ("Send Message");
	gtk_box_pack_start( GTK_BOX(vbox_buttons), button_sendmsg,TRUE,TRUE,0);
	gtk_signal_connect_object( GTK_OBJECT (button_sendmsg), "clicked",
		GTK_SIGNAL_FUNC(bnsendmsg),
		GTK_OBJECT (window) );
	gtk_widget_set_sensitive(button_sendmsg,FALSE);
	gtk_widget_show(button_sendmsg);
	gtk_tooltips_set_tip (tooltips,button_sendmsg,
		_("Send a message"),
		_("Send a message"));
   
	button_status = gtk_button_new_with_label ("Status");
	gtk_box_pack_start( GTK_BOX(vbox_buttons), button_status,TRUE,TRUE,0);
	gtk_signal_connect_object( GTK_OBJECT (button_status), "clicked",
		GTK_SIGNAL_FUNC(status),
		GTK_OBJECT (window) );
	gtk_widget_set_sensitive(button_status,FALSE);
	gtk_widget_show(button_status);		
	gtk_tooltips_set_tip (tooltips,button_status,
		_("Status of the game"),
		_("Status of the game"));
   

	/* right */
	notebook_right = gtk_notebook_new();
	gtk_signal_connect ( GTK_OBJECT ( notebook_right ), "switch_page",
		GTK_SIGNAL_FUNC ( page_switch ), NULL );
	gtk_notebook_set_tab_pos( GTK_NOTEBOOK(notebook_right),GTK_POS_TOP );
	gtk_box_pack_start( GTK_BOX(hbox),notebook_right,TRUE,TRUE,0);
 	gtk_container_border_width( GTK_CONTAINER( notebook_right), 0 );
	gtk_notebook_set_scrollable( GTK_NOTEBOOK(notebook_right) , TRUE );
	gtk_notebook_set_show_tabs( GTK_NOTEBOOK(notebook_right) , TRUE );
	gtk_notebook_popup_enable( GTK_NOTEBOOK(notebook_right) );
	gtk_widget_realize( notebook_right );
	gtk_widget_show (notebook_right);

	drawing_right_about = gtk_drawing_area_new();
	gtk_drawing_area_size(GTK_DRAWING_AREA(drawing_right_about),200,200);
	gtk_signal_connect (GTK_OBJECT (drawing_right_about), "expose_event",
		(GtkSignalFunc) expose_event_about, NULL );
	gtk_widget_set_events (drawing_right_about, GDK_EXPOSURE_MASK );
	gtk_widget_show( drawing_right_about);
	label_right_about = gtk_label_new("Batalla Naval" );
	gtk_widget_show( drawing_right_about);
	gtk_notebook_append_page( GTK_NOTEBOOK( notebook_right), drawing_right_about, label_right_about );
   

	/* Ventana de texto de abajo */
	separator = gtk_hseparator_new ();
	gtk_box_pack_start ( GTK_BOX(vbox), separator, FALSE,TRUE, 0);

	gtk_widget_show(separator);
   
	hbox_text = gtk_hbox_new ( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(vbox), hbox_text );
	gtk_widget_show( hbox_text );
   
	text = gtk_text_new(NULL,NULL);
	gtk_box_pack_start( GTK_BOX(hbox_text), text, TRUE,TRUE,0);
	gtk_widget_show(text);
   
	vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
	gtk_range_set_update_policy( GTK_RANGE( vscrollbar ), GTK_UPDATE_CONTINUOUS );
	gtk_box_pack_start( GTK_BOX(hbox_text), vscrollbar, FALSE,TRUE,0);
	gtk_widget_show (vscrollbar);
   
	gtk_text_freeze(GTK_TEXT(text));
	gtk_widget_realize(text); 
	gtk_text_insert( GTK_TEXT(text),NULL,NULL,NULL,"Gnome Batalla Naval client v"IPVERSION" by riq (c) 1998,99" ,-1);
	gtk_text_thaw(GTK_TEXT(text));

   
	/* StatusBar */
	hbox_status = gtk_hbox_new ( FALSE, 0);
	gtk_container_add ( GTK_CONTAINER(vbox), hbox_status );
	gtk_container_border_width ( GTK_CONTAINER(hbox_status), 0);
	gtk_widget_show ( hbox_status ); 
   
	statusbar_right = gtk_statusbar_new();
	gtk_box_pack_end( GTK_BOX( hbox_status ), statusbar_right, TRUE,TRUE, 0);
	gtk_widget_show( statusbar_right );

	statusbar_left = gtk_statusbar_new();
	gtk_box_pack_end( GTK_BOX( hbox_status ), statusbar_left, TRUE,TRUE, 0);
	gtk_widget_show( statusbar_left );

	foot_left("Batalla Naval");
	foot_right("Gnome client v"VERSION);

	      
	/* Pixmaps */
	barco1 =    gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco1_xpm );
	barco1_t =  gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco1_t_xpm );
	barco1_h =  gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco1_h_xpm );

	barco2h =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2h_xpm );
	barco2h_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2h_t_xpm );
	barco2h_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2h_h_xpm );

	barco2v =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2v_xpm );
	barco2v_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2v_t_xpm );
	barco2v_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco2v_h_xpm );

	barco3h =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3h_xpm );
	barco3h_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3h_t_xpm );
	barco3h_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3h_h_xpm );

	barco3v =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3v_xpm );
	barco3v_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3v_t_xpm );
	barco3v_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco3v_h_xpm );

	barco4h =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4h_xpm );
	barco4h_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4h_t_xpm );
	barco4h_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4h_h_xpm );

	barco4v =   gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4v_xpm );
	barco4v_t = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4v_t_xpm );
	barco4v_h = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],barco4v_h_xpm );

	fondo =     gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],fondo_xpm );
	agua =      gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],agua_xpm );
	about_pix = gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],about_xpm );
	icono =     gdk_pixmap_create_from_xpm_d( window->window, &mask, &window->style->bg[GTK_STATE_NORMAL],icono_xpm );

	gdk_window_set_icon (window->window, NULL, icono , mask );

   
	/* Principal */   
	gtk_widget_show( window);
}
