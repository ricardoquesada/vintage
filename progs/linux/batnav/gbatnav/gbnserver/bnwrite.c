/*	$Id$	*/
/*
 * Funcion bnwrite
 */
#include "../gbnserver/protocol.h"
#include "../gbnserver/server.h"
#include "../gbnserver/riq-net.h"

void
broadcast( char *fmt, ...)
{
	int i;

	char t[1000];
	va_list args;
	va_start( args, fmt );

	vsprintf(t,fmt,args);
	va_end(args);

	for(i=0;i<MAXPLAYER;i++) {
		if( usuario.nro_tot[i]>DISCON )  {
			if(  riq_net_printf(usuario.nro_fd[i],"%s\n",t) < 1 )
				printf("--broadcast error %d\n",i);
		}
	}
	return;
}

int
bnwrite(int fd, char *fmt, ...)
{
	int a;
	char t[1000];
	va_list args;
	va_start( args, fmt );

	vsprintf(t,fmt,args);
	va_end(args);
	
	a = riq_net_printf(fd,"%s\n",t);
	if(a<1) {
		printf("bnwrite error:%d\n",a);
	}
	return a;
}
