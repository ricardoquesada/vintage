/*
	Prueba de parser
 */

/* INCLUDES */
#include <stdio.h>
#include <string.h>
#include "parser.h"

void main(int argc, char *argv[]) 
{
	PARSER p;
	int  j;

	char prueba[]="Quiero leer";

	strcpy(p.sig,prueba);
	j=TRUE;

	while(j) {
		printf("------------\n");
		j=parser_init( &p );
		printf(
			"token=%s\n" \
			"value=%s\n" \
			"next=%s\n"	\
			"bien=%i\n",p.token, p.value, p.sig, p.status 
		);
	}
}
