/*
	Estructura del parser
*/

#ifndef __PARSER__
#define __PARSER__

#define PARSER_TOKEN_MAX 100
#define PARSER_VALUE_MAX 500
#define PARSER_SIG_MAX 5000

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef struct 
{
	char token[PARSER_TOKEN_MAX];
	char value[PARSER_TOKEN_MAX];
	char sig[PARSER_TOKEN_MAX]; 
	int status;
} PARSER, *PARSER_PTR;

enum
{
	PARSER_FIN,
	PARSER_SEPARADOR,
	PARSER_IGUAL,
	PARSER_DATA,
	PARSER_ERROR
};

/* Unica funcion publica del parser */
int
parser_init( PARSER_PTR );

#endif __PARSER__
