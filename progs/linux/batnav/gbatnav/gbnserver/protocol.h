/*	$Id$	*/
/************************************************************************
	Archivo que pertenece al batalla naval 
	Definicion del protocolo
*************************************************************************/

#ifndef __BN_PROTOCOLO_H__
#define __BN_PROTOCOLO_H__

#ifdef __cplusplus
extern "C" {
#endif __cplusplus


#define PROT_VER "2"
#define PROT_PLAY_VER "2"

#define PROT_MAX_LEN 5000
#define MAXNAMELEN 18

/* MAXPLAYERS - Un valor entre 2 y un numero muy grande :) */
#define MAXPLAYER 10

/* NO USAR 'enum' */
#define	DISCON	0 		/* not connected */
#define	CONNEC	1		/* connected */
#define	BOARD	2		/* Board OK */
#define	PLAY	3		/* playing */
#define	TURN	4		/* playing + turn */
#define	PERDIO	5 		/* perdio (sirve para que clientes como
				el btwin sigan monitoreando el juego
				luego de perder */

#define AGUA	'0'
#define NOBARCO	'1'
#define BARCO	'x'
#define TOCADO	'y'
#define HUNDIDO	'z'

#define BN_BOARD_NOT_OK		"board_not_ok"
#define	BN_BOARD_OK		"board_ok"
#define	BN_CANT_MODIFY_BOARD	"cant_modify_board"
#define BN_CLI_VER		"client_version"
#define BN_DISCON		"disconnect"
#define BN_EXIT			"exit"
#define BN_FIRE			"fire"
#define BN_GAME_OVER		"game_over"
#define BN_HELP			"help"
#define BN_LOST			"loser"
#define BN_MESSAGE		"message"
#define BN_NAME			"name"
#define BN_NUMJUG		"number_player"
#define BN_PROTOCOL		"protocol"
#define BN_QUMM			"quiero_un_mundo_mejor"
#define BN_READ			"read"
#define BN_READY_TO_PLAY	"ready_to_play"
#define BN_REM			"*"
#define BN_ROBOT		"quiero_jugar_con_un_robot"
#define BN_SCORES		"scores"
#define BN_SEND			"enviando_mis_barcos"
#define BN_SER_FULL		"el_servidor_esta_lleno"
#define BN_SER_VER		"server_version"
#define BN_SOL			"solo_a_ningun_lado"
#define BN_START		"empezar_a_jugar"
#define BN_STATUS		"status"
#define BN_TEST			"eva_test"
#define	BN_TURN			"es_el_turno_de"
#define	BN_WAIT			"espera_que_hay_gente_jugando"
#define	BN_WIN			"el_ganador_es"


# ifdef __cplusplus
}
# endif __cplusplus

#endif /* __BN_PROTOCOLO__ */
