/*	$Id$	*/
/*
 * Gnome Batalla Naval
 * Servidor
 * 
 * (c) 1998-2000 Ricardo Calixto Quesada
 * mailto: 
 *         riq@ciudad.com.ar
 *         ricardo_quesada@core-sdi.com
 *
 * http://www.pjn.gov.ar/~rquesada
 * http://members.xoom.com/_riq_
 *  
 * Modificaciones para adaptar cliente Win16 y bugfixes por:
 * Horacio Pe�a ( horape@compendium.com.ar )
 */

/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

/* Includes de la interface */
#include <config.h>
#include <gnome.h>

#include "ipv6.h"			/* soporte para IPv6 */
#include "protocol.h"			/* definicion del protocolo */
#include "server.h"
#include "check_board.h"
#include "play.h"
#include "g_interface.h"
#include "parser.h"
#include "version.h"
#include "bnwrite.h"


#ifdef INET6
#include <netdb.h>
#endif /* INET6 */

/* algunos defines lindos del UNP second edition volume 1 */
#define LISTENQ	1024
#define SA struct sockaddr

/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */

/* Estas, antes estaban en main */

struct st_datos usuario;

extern struct { char *nombre; } st_nombres[];

/* This describes all the arguments we understand.  */
static struct poptOption options[] =
{
	{ "port", 'p', POPT_ARG_INT, &usuario.port, 0,  N_("PORT"), N_("Port number. Default is 1995")},
	{ NULL, '\0',  0, NULL  }
};

void init_args( void )
{
	usuario.port = gnome_config_get_int_with_default("/gbnserver/data/port=1995",NULL);

	gnome_config_set_int("/gbnserver/data/port",usuario.port);
	gnome_config_sync();
}
/***************************************************************************
		funciones relacionadas con las tablas 
***************************************************************************/

/* write to board */
gint wtable( gint jug, gchar *str ) 
{
	gint i,x,y;

	if(strlen(str)!=100) 
		return FALSE;

	x=0;y=0;
	for(i=0;i<100;i++) {
		usuario.table[jug].p[x][y]=str[i];
		x++;
		if(x>=10) {
			x=0;
			y++;
		}
	}
	return( algoritmo(jug));
}

/* clean board */
void ctable( int jug) 
{
	int i,x,y;
	x=0;y=0;
	for(i=0;i<100;i++) {
		usuario.table[jug].p[x][y]=NOBARCO;
		x++;
		if(x>=10) {
			x=0;
			y++;
		}
	}
}

void borrar_jugador( int num_jug )
{
	usuario.nro_tot[num_jug]=DISCON;
	say_in_clist( num_jug, C_STATUS, st_nombres[DISCON].nombre );
	say_in_clist( num_jug, C_NAME, "- - -" );
	say_in_clist( num_jug, C_CLIVER, "- - -" );
	say_in_clist( num_jug, C_HOSTNAME, "- - -" );
	say_in_clist( num_jug, C_LASTTOKEN, "- - -" );

	gdk_input_remove( usuario.tag[num_jug] );
	close( usuario.nro_fd[num_jug] );
	usuario.hits[num_jug]=0;
	ctable(num_jug);  
	usuario.nro_fd[num_jug]=0;
	usuario.names[num_jug][0]=0;
	usuario.robot[num_jug]=0;
}

void salir_bien( void )
{
	int i;
	for( i=0;i<MAXPLAYER;i++){
		if( usuario.nro_tot[i]>DISCON) {
			bnwrite(usuario.nro_fd[i],BN_DISCON"=%i",i);
			borrar_jugador( i );
		}
	}
	unlink(BATNAV_UDSOCKET);
}
/*****************************************************************************
		el barco esta hundido ? y todo tipo de funciones
*****************************************************************************/
int quebarco( int i,int x, int y) 
{
	if( (x<0) || (x>9) || (y<0) || (y>9) )
		return NOBARCO;
	return( usuario.table[i].p[x][y]);
}

/* Se fija si el barco es hundido */
int r_eshundido(int i,	/* Numero de Jugador */
		    int x,	/* X del barco */
		    int y,	/* Y del barco */
		    int xx,	/* Direccion X del barco */
		    int yy	/* Direccion Y del barco */
		    )
{
	if(quebarco(i,x,y)<=NOBARCO)
		  return 0;
	if(quebarco(i,x,y)==TOCADO)
		  return r_eshundido(i,x+xx,y+yy,xx,yy);
	return 1;	/* No es hundido */
}

/* Pinta al barco hundido */
void r_pihundido(int i,	/* Numero de Jugador */
		    int x,	/* X del barco */
		    int y,	/* Y del barco */
		    int xx,	/* Direccion X del barco */
		    int yy	/* Direccion Y del barco */
		    )
{
	if(quebarco(i,x,y)<=NOBARCO)
		  return;
	if(quebarco(i,x,y)==TOCADO) {
		usuario.table[i].p[x][y]=HUNDIDO;
		bnwrite(usuario.nro_fd[i],BN_FIRE"=%i,%i,%i",x,y,HUNDIDO);

		return r_pihundido(i,x+xx,y+yy,xx,yy);
	}
	printf("server: gbnserver.c: r_pihundido error\n");
}

int eshundido(int i,int x,int y) 
{
	gint a;

	if(quebarco(i,x,y)<=NOBARCO)
		return FALSE;
	if(quebarco(i,x,y)==HUNDIDO)
		  return TRUE;

	usuario.table[i].p[x][y]=TOCADO;	/* Tocado por ahora */
	a =	r_eshundido(i,x-1,y,-1,0) + 
		r_eshundido(i,x+1,y,+1,0) + 
		r_eshundido(i,x,y-1,0,-1) + 
		r_eshundido(i,x,y+1,0,+1);
	if(a==0) {
		r_pihundido(i,x,y,-1,0);
		r_pihundido(i,x+1,y,+1,0);
		r_pihundido(i,x,y-1,0,-1);
		r_pihundido(i,x,y+1,0,+1);

		usuario.hits[i]++;
		if(usuario.hits[i]==10) { /* con 10 barcos hundidos uno pierde */
			usuario.nro_tot[i]=PERDIO;
			say_in_clist( i, C_STATUS, st_nombres[PERDIO].nombre );
			if(usuario.robot[i]==1) {
				bnwrite(usuario.nro_fd[i],BN_DISCON"=%i",i);
				borrar_jugador( i );
			}
			broadcast(BN_LOST"=%i",i);
		}
		return TRUE;
	}
	return FALSE;
}


/***************************************************************************** 
			funciones control de child y juego
***************************************************************************/
void ex_loop( gpointer data, gint que_sock, GdkInputCondition GDK_INPUT_READ)
{
	int i,j,k,client_len;
#ifdef INET6
	struct sockaddr_in6 client;		/* INET6 */
	char str[INET6_ADDRSTRLEN];
#else /* INET6 */
	struct sockaddr_in client;		/* INET */
#endif /* INET6 */
	struct sockaddr_un uclient;		/* UNIX */
	

	client_len = sizeof(client);
	k = que_sock;
	
	if( que_sock==usuario.sock ) { /* AF_INET */
		if( (que_sock=accept(usuario.sock, (struct sockaddr *) &client,&client_len)) < 0 ) {
			perror("accept error");
			return;
		} 
	} else {	/* La diferencia es que usa aca lo de AF_UNIX */
		if( (que_sock=accept(usuario.usock,(struct sockaddr *)&uclient,&client_len)) < 0 ) {
			perror("accept error");
			return;
		}
	}

	/* El servidor esta lleno ? */
	for(j=0;j<MAXPLAYER;j++) {
		if(usuario.nro_tot[j]==DISCON )
			break;
	}
	if(j>=MAXPLAYER) {
		bnwrite(que_sock,BN_SER_FULL);
		close(que_sock);
		return;
	}

	i = gdk_input_add( que_sock, GDK_INPUT_READ, play_batnav, NULL );
	if(i<0) {
		close(que_sock);
		return;
	}

	usuario.nro_tot[j]=CONNEC;
	say_in_clist( j, C_STATUS, st_nombres[CONNEC].nombre );
	usuario.nro_fd[j]=que_sock;
	usuario.tag[j]=i;
	if(k==usuario.usock)  {
		usuario.robot[j]=1;
		say_in_clist(j,C_HOSTNAME,BATNAV_UDSOCKET);
	} else {
#ifdef INET6
		inet_ntop( AF_INET6, &client.sin6_addr ,str, sizeof(str) );
		say_in_clist(j,C_HOSTNAME,str);
#else /* INET6 */	
		say_in_clist(j,C_HOSTNAME,inet_ntoa( client.sin_addr ) );
#endif /* INET6 */
	}

	return;
}

/*****************
 * main function *
 *****************/
void main(int argc, char *argv[]) 
{
	int i ;
	size_t addrLenght;
#ifdef INET6
	struct sockaddr_in6 server;		/* AF_INET6 */
#else /* INET6 */
	struct sockaddr_in server;		/* AF_INET */
#endif /* INET6 */
	struct sockaddr_un address;		/* AF_UNIX */
	
	
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
	gnome_score_init("gbatnav");

	gnome_init_with_popt_table("gbatnav", BNVERSION, argc, argv, options,0, NULL);

	init_args();

	gethostname(usuario.server_name,PROT_MAX_LEN);        
   
	printf( "\n"
		BNVERSION"\n" 
		"(c) 1995,99 by Ricardo Quesada (riq@ciudad.com.ar)\n"
		"Port Number:%i\n"
		"Unix Domain socket:"BATNAV_UDSOCKET"\n"
		"Server Name:%s\n"
		"Max Players:%i\n"
		,usuario.port,usuario.server_name,MAXPLAYER);
   
	/* Set default parameters */
	for(i=0;i<MAXPLAYER;i++) {
		/* init server */
		usuario.nro_tot[i]=DISCON;
		say_in_clist( i, C_STATUS, st_nombres[DISCON].nombre );
		usuario.nro_fd[i]=0;
		usuario.robot[i]=0;
		ctable(i);
	}
  	
	 /* Fase 1: AF_INET , AF_INET6 Sockets */
	bzero(&server, sizeof(server) );
#ifdef INET6
	usuario.sock=socket(AF_INET6,SOCK_STREAM,0);
	if(usuario.sock <0) {
		perror("gbnserver socket error");
		exit(1);
	}
	server.sin6_family=AF_INET6;
	server.sin6_addr=in6addr_any;
	server.sin6_port=htons(usuario.port);
#else /* INET6 */
	usuario.sock=socket(AF_INET,SOCK_STREAM,0);
	if(usuario.sock <0) {
		perror("gbnserver socket error");
		exit(1);
	}
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=htonl(INADDR_ANY);
	server.sin_port=htons(usuario.port);
#endif /* INET6 */
	if(bind(usuario.sock,(SA *)&server,sizeof(server))< 0) {
		perror("gbnserver bind error");
		exit(2);
	}
	if(listen(usuario.sock,LISTENQ)<0) {
		perror("listen error");
		exit(3);
	}
	
	/* Fase 2: Unix Domain Sockets */
	if((usuario.usock=socket(AF_UNIX, SOCK_STREAM, 0 )) < 0 ) {
		perror("gbnserver unix domain socket error");
		exit(1);
	}
	unlink(BATNAV_UDSOCKET);
	address.sun_family = AF_UNIX;
	strcpy( address.sun_path, BATNAV_UDSOCKET);
	addrLenght = sizeof(address.sun_family) + strlen(address.sun_path);
	if(bind(usuario.usock,(SA *) &address, addrLenght )) {
		perror("gbnserver bind AF_UNIX");
		exit(1);
	}
	if(listen(usuario.usock,LISTENQ)<0) {
		perror("listen AF_UNIX error");
		exit(3);
	}

	init_screen();

	/* AF_UNIX */
	gdk_input_add( usuario.usock, GDK_INPUT_READ, ex_loop, NULL );
	/* AF_INET */
	gdk_input_add( usuario.sock, GDK_INPUT_READ, ex_loop, NULL );

	gtk_main();
}
