/*	$Id$	*/
#ifndef __VERSION_H__
#define __VERSION_H__

#include <gnome.h>
#include <config.h>
#include "../gbnserver/ipv6.h"

#ifdef INET6
	#define BATVER    "Gnome Batalla Naval Server v"VERSION"+IPv6"
#else
	#define BATVER    "Gnome Batalla Naval Server v"VERSION
#endif
#define BNVERSION BATVER

#endif /* __VERSION_H__ */
