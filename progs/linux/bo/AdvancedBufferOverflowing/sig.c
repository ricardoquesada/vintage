#include <signal.h>

void intr(int c) {
	printf("hola:%d\n",c);
}

int main() {
	alarm(2);
	signal(SIGALRM,intr);
	signal(SIGINT,intr);
	while(1);
}
