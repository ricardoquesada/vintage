/* specially crafted to feed you brain by gera@core-sdi.com */

/* jumpy vfprintf, Batman! */

/* I codded it for linux glibc-2.1.3,           *
 * it can't be done in OpenBSD as I thought it, *
 * I think                                      */

int main(int argv,char **argc) {
	printf(argc[1]);		/* Can you do it changing the stack?  */
	while(1);			/* Can you do it without changing it? */
}
