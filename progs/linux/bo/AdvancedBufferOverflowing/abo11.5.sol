#
# solucion problema 11.5
# 
# riq
#
# misma tecnia que la solucion 2 del problema 10
#
# solo que aca el valor que se sobreescribia estaba entre 0x0000 y 0x0100 
# y entre esos valores no podia poner lo que necesitaba y es por eso
# que agrande el buffer un poco, porque yo necesitaba que el 1er valor
# de mi argv[1] sea un nmonico onda mov o algo asi porque si, las 
# instruccion 0xfe iban a ser un segfault.
#
# entonces tenia que buscar una dir de memoria en el stack que, que pueda
# ser ejecutada. (El address tiene que ser ejecutable).
# Por ejemplo:
# la direccion 0xbffefebc es:
#
#	mov    $0xbfbffefe,%esp
#
# (ocupa 5 bytes, y luego del 5to byte hay que poner el shellcode)
#
#
# Algo similar se puede hacer (y creo que es mas facil en este caso) si
# se cambia el retaddr del snprintf en vez del EBP. Para apuntar el codigo
# entre 0 y 0x200 hay que hacer crecer el stack enviando un parametro muy
# grande (igual que aca)
#
# 
perl -e 'print  "\xbc\xfe\xfe\xbf" . "\xbf" . "X"x249 . "Y"x68 . " " . "\x6c\xf8\xff\xbf" . "a"x63580'
