/* specially crafted to feed you brain by gera@core-sdi.com */

/* This is a tricky example to make you think and *
 * give you some help on the next one             */

int main(int argv,char **argc) {
	char buf[256];

	signal(10,main);
	strcpy(buf,argc[1]);
	exit(1);
}
