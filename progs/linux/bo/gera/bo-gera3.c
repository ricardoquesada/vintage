// made to feed your brain by gera@core-sdi.com

-----------------------------------------------------------------------
// Stupid example to let you get introduced... if you think I'm stupid
// you'll eat your words

int main(int argv,char **argc) {
        char buf[256];

        strcpy(buf,argc[1]);
}


-----------------------------------------------------------------------
// This is a tricky example to make you think and then
// give you some help on the next exercise

int main(int argv,char **argc) {
        char buf[256];

        strcpy(buf,argc[1]);
        exit(1);
}


-----------------------------------------------------------------------
// This will prepare your mind for The Next Step
int main(int argv,char **argc) {
        extern system,puts;
        void (*fn)(char*)=&system;
        char buf[256];

        fn=&puts;
        strcpy(buf,argc[1]);
        fn(argc[2]);
        exit(1);
}
// don't forget to run it with 2 arguments
-----------------------------------------------------------------------
// After this, the next is just an Eureka! away.
extern system,puts;
void (*fn)(char*)=&system;

int main(int argv,char **argc) {
        char *pbuf=malloc(strlen(argc[2])+1);
        char buf[256];

        fn=&puts;
        strcpy(buf,argc[1]);
        strcpy(pbuf,argc[2]);
        fn(argc[3]);
        while (1);
}
-----------------------------------------------------------------------
// You take the blue pill, you wake up in your bed,
//     and you believe what you want to believe
// You take the red pill,
//     and I'll show you how deep goes the rabbit hole

int main(int argv,char **argc) {
        char *pbuf=malloc(strlen(argc[2])+1);
        char buf[256];

        strcpy(buf,argc[1]);
        strcpy(pbuf,argc[2]);
        exit(1);
}
