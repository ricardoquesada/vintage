/*
 * Esound driver
 * riq, riq@ciudad.com.ar 
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <esd.h>

static int audio_fd;


int play (void)
{
#define AAA 200000
	int i;
	char buf[AAA];

	for(i=0;i<AAA;i++)
		buf[i]=34;

	write(audio_fd,buf,AAA);
}
int main( int argc, char **argv)
{
	int fd=(int)NULL;
	static esd_format_t esd_format;
	esd_server_info_t *info =NULL;

	esd_format = ESD_STREAM | ESD_PLAY | ESD_STEREO | ESD_BITS16; 

	printf("Esound Driver initialization... 0.0.3\n");

	/* Paso NULL, y ya que es sacado de la variable ESPEAKER */
	fd = esd_open_sound((char*)NULL);
	if(fd < 0) {
		perror("esound");
		goto esound_audio_init_error;
	}

	info = esd_get_server_info(fd);
	if (info) {
		audio_fd = esd_play_stream(esd_format, 22050, NULL, "riq");
		if( audio_fd <= 0)
			goto esound_audio_init_error;
	} else {
		goto esound_audio_init_error;
	}

	esd_print_server_info( info );
	esd_free_server_info(info);
	esd_close(fd);


	play();

        return 0;   

esound_audio_init_error:
	printf("Sound device init failed. Sound disabled\n");
	if(info) esd_free_server_info(info);
	if(fd) esd_close(fd);
        return -1;   
}
