#include <errno.h>
#include <linux/ptrace.h>
#include <linux/user.h>
#include <sys/wait.h>
#include <stdio.h>

int pid = -1;


#define PAGMASK	(~(PAGSIZ - 1))
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
/*
 * move `len' bytes of data from process `pid'
 * at address `addr' to our space at `laddr'
 */
int umoven(long addr, int len, char *laddr)
{
	int n, m;
	int started = 0;
	union {
		long val;
		char x[sizeof(long)];
	} u;

	if (addr & (sizeof(long) - 1)) {
		/* addr not a multiple of sizeof(long) */
		n = addr - (addr & -sizeof(long)); /* residue */
		addr &= -sizeof(long); /* residue */
		errno = 0;
		u.val = ptrace(PTRACE_PEEKDATA, pid, (char *) addr, 0);
		if (errno) {
			if (started && (errno==EPERM || errno==EIO)) {
				/* Ran into 'end of memory' - stupid "printpath" */
				return 0;
			}
			/* But if not started, we had a bogus address. */
//			perror("ptrace: umoven");
			return -1;
		}
		started = 1;
		memcpy(laddr, &u.x[n], m = MIN(sizeof(long) - n, len));
		addr += sizeof(long), laddr += m, len -= m;
	}
	while (len) {
		errno = 0;
		u.val = ptrace(PTRACE_PEEKDATA, pid, (char *) addr, 0);
		if (errno) {
			if (started && (errno==EPERM || errno==EIO)) {
				/* Ran into 'end of memory' - stupid "printpath" */
				return 0;
			}
//			perror("ptrace: umoven");
			return -1;
		}
		started = 1;
		memcpy(laddr, u.x, m = MIN(sizeof(long), len));
		addr += sizeof(long), laddr += m, len -= m;
	}
	return 0;
}


int attach()
{
	int i;
	int j;
	unsigned int base;

	if( pid != -1 ) {
		struct user user;

		/* Attach to the new child */
		if (ptrace(PTRACE_ATTACH, pid, (char *) 1, 0) < 0) {
			perror("PTRACE_ATTACH");
			return -1;
		}
		waitpid( pid, NULL, WUNTRACED );

		if (ptrace(PTRACE_GETREGS, pid, (char *) &user.regs, 0) < 0) {
			perror("PTRACE_GETREGS");
			return -1;
		}

		printf("registers:\n");
		printf("eax: 0x%8x\n",user.regs.eax);
		printf("ebx: 0x%8x\n",user.regs.ebx);
		printf("ecx: 0x%8x\n",user.regs.ecx);
		printf("edx: 0x%8x\n",user.regs.edx);
		printf("esi: 0x%8x\n",user.regs.esi);
		printf("edi: 0x%8x\n",user.regs.edi);
		printf("ebp: 0x%8x\n",user.regs.ebp);
		printf("ds: 0x%4x\n",user.regs.ds);
		printf("es: 0x%4x\n",user.regs.es);
		printf("fs: 0x%4x\n",user.regs.fs);
		printf("gs: 0x%4x\n",user.regs.gs);
		printf("orig eax 0x%8x\n",user.regs.orig_eax);
		printf("cs:eip %4x:%8x\n",user.regs.cs,user.regs.eip);
		printf("ss:esp %4x:%8x\n",user.regs.ss,user.regs.esp);

		j=0;
		base = user.regs.ebp;

		printf("\nstack trace:\n");
		for( i=base; i < base + 128; i = i + sizeof(i) ) {

			unsigned int r;
			unsigned char *f = (char*)&r;

			if( umoven( i, sizeof(r), (char*)&r ) == -1 )
				continue;

			printf("0x%8x: 0x%8x\n",i,r);

		}

		j=0;
		base = 0x8000000;
		for( i=base; i < base +(1024 * 1024); i = i + sizeof(i) ) {

			unsigned int r;
			unsigned char *f = (char*)&r;

			break;
			if( umoven( i, sizeof(r), (char*)&r ) == -1 )
				continue;

			if( j==0 )
				printf("\n%8x: ",i);
			else if( j==2 )
				printf("- ");

			printf("%2x %2x %2x %2x ",f[0],f[1],f[2],f[3]);
			
			j++;
			j = j % 4;
		}

		for(;;) {
			ptrace( PTRACE_SINGLESTEP, pid, 0, 0);
			if (ptrace(PTRACE_GETREGS, pid, (char *) &user.regs, 0) < 0) {
				perror("PTRACE_GETREGS");
				break;
			}
			printf("eip %8x\n",user.regs.eip);
		}

		ptrace( PTRACE_DETACH, pid, (char *) 1, 0 );
	}
}

int main( int argc, char *argv[] )
{
	if( argc > 1 )
		pid = atoi( argv[1] );
	else
		pid = -1;

	attach();
	return 0;
}
