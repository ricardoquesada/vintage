/* Modificado para adaptarlo al VChar por Ricardo Quesada */
/* Internal definitions. */

#ifndef _VCHAR_H
#define _VCHAR_H

/* --------------------- Macro definitions shared by library modules */

/* path de los fonts */
#define fontpath "/usr/lib/kbd/consolefonts/"         /* new on v1.01 */
#define fontsufx ".8x16"                              /* new on v1.01 */

/* colores */
#define Fondo    0x3820
#define BigLleno 0x7fb2
#define BigVacio 0x7fd8
#define Borrar   0x7220
#define Borrar2  0x3420
#define BorrarB  0x0e20
#define SActive  0x7f00
#define MacroFo  0x8920
#define Limpio   0x0720

#define EDIT     0x00
#define CARAC    0x01
#define TEXTEDIT 0x02

#define NOSEQUE  0x8080
#define MOUSE_SI 0x8081
#define KEY_SI   0x8082

#define UPDATEF   0x01
#define NOUPDATEF 0x02
#define BotonOn   0x3f
#define BotonOff  0x7f

/* cantidad total de botones */
#define BOTONES  29
#define BOTLEN   14
#define MAXBOTLEN BOTLEN * 2 + 2
#define BOO      0
#define BUD      1
#define BID      2
#define BCL      3
#define BGR      4
#define BOR      5
#define B01      6
#define B02      7
#define B10      8
#define B11      9
#define B12      10
#define B13      11
#define B14      12
#define B15      13
#define B16      14
#define B17      15
#define BSA      16
#define BSB      17
#define BSH      18
#define BAU      19
#define BAD      20
#define BF1      21
#define BC1      22
#define BL1      23
#define BL2      24
#define BL3      25
#define BL4      26
#define BVC      27
#define BAN      28

/* macros de las macros */
#define MACROSIZE 200
#define MACROPLAY 2
#define MACROREC 1
#define MACROSTOP 0

/* Esto fue sacado de vga.h de ... */

/* SVGAlib, Copyright 1993 Harm Hanemaayer */
/* VGAlib version 1.2 - (c) 1993 Tommy Frandsen */
/* partially copyrighted (C) 1993 by Hartmut Schirmer */

/* type declarations */
typedef unsigned char BYTE;
typedef unsigned short int WORD;
typedef void fn_hand(void);


static inline void port_out(int value, int port)
{
    __asm__ volatile ("outb %0,%1"
	      ::"a" ((unsigned char) value), "d"((unsigned short) port));
}

static inline void port_outw(int value, int port)
{
    __asm__ volatile ("outw %0,%1"
	     ::"a" ((unsigned short) value), "d"((unsigned short) port));
}

static inline int port_in(int port)
{
    unsigned char value;
    __asm__ volatile ("inb %1,%0"
		      :"=a" (value)
		      :"d"((unsigned short) port));
    return value;
}

/*
static inline int port_inw(int port)
{
    unsigned short value;
    __asm__ volatile ("inw %1,%0"
		      :"=a" (value)
		      :"d"((unsigned short) port));
    return value;
}
*/

/* Note that the arguments of outb/w are reversed compared with the */
/* kernel sources. The XFree86 drivers also use this format. */
#define inb port_in
/* #define inw port_inw */
#define outb(port, value) port_out(value, port)
#define outw(port, value) port_outw(value, port)

#endif
