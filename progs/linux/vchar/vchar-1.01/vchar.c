/*************************************************/
/* VChar LI: (c) 1994,96,97 by Ricardo Quesada   */
/* version: 1.01                                 */
/*************************************************/
/* Send bugs and comments to: rquesada@dc.uba.ar */
/*************************************************/
/* ??/9/95 - Investigar svgalib - No sabia como empezar. */
/* 22/8/96 - Segundo intento de hacer el VChar para Linux.Despues de casi 1 ano */
/* 30/8/96 - 0.17 - Algo potable */
/* 31/8/96 - 0.21 - Algo mas potable */
/* 31/8/96 - 0.24 - Empezar con el mouse */
/*  2/9/96 - 0.26 - El mouse ya es una realidad. Las mil una me pasaron */
/*  2/9/96 - 0.27 - Se empezo con la version LI (Linux Interface ) */
/*  2/9/96 - 0.29 - Bastante aceptable */
/*  3/9/96 - 0.32 - Algo very beautiful */
/*  5/9/96 - 0.34 - 8/9 bits Ok! Bug en PC Intern. */
/*  7/9/96 - 0.35 - Todo menos la animacion */
/*  7/9/96 - 0.36 - Espero corregir algun bug y poner la animacion */
/*  8/9/96 - 1.00 - Animacion no creo que sea incluida */
/* 31/8/97 - 1.01 - Incluir soporte para psf y algun bug y Debian alike*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termio.h>
#include <curses.h>
#include <string.h>
#include <signal.h>
#include <gpm.h>                 /* mouse support. New on v0.24 */
#include <sys/vt.h>
#include <sys/kd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/perm.h>
#include "vchar.h"
#include "keyboard.h"
#include "vcharfont.h"

#define VCHARVER "1.01"
#define SELECT_TIME 600
#define SETSIG(sa, sig, fun) {\
	sa.sa_handler = fun; \
	sa.sa_flags = SIG_BLOCK; \
	sigaction(sig, &sa, NULL); \
}
/* Borrado  porque no me compilaba - igual no importa
 * porque uso SIG_BLOCK */
/*	sa.sa_mask = (sigset_t)   */


/* definicion de funciones */
void init_vga(void);
void rest_vga(void);
void savedac(void);
void restdac(void);
void save2c(void);
void save2bin(void);
void viewfont(void);
BYTE getvgaxy(short x,short y);
void getfont(BYTE i);
void cgrid(void);
void crotate90(void);
void vcharfonts(void);
void vcharchar(void);
void c89bits(void);
void canim(void);
void cinvert(void);
void cleftright(void);
void cupdown(void);
void cclean(void);
void nada(void);
void csavechar(void);
void casciiup(void);
void casciidown(void);
void cleartextedi(void);
void asciitable(void);
void copyc(void);
void pastec(void);
void csalir(void);
void xautosave(void);
void cff1(void);
void cfcf1(void);
void macrorec(void);
void macroplay(void);
void scrollup(void);
void scrolldown(void);
void scrollleft(void);
void scrollright(void);
void convertcode(void);
void keycomun(void);
void doubleno(void);
void doublefont(void);
void setxy(BYTE,BYTE);

/* variables globales... */
static WORD srmap,srmem,grmap,grmod,grmis;

static FILE *fh;       /* usadas por openmem */
static BYTE* vid_addr;
static FILE *fh2;      /* usadas por openmem */
static BYTE* vid_addr2;

static BYTE ascii;
static int consolefd;
static int keymode;   /* estado del teclado */
static struct termio  consoleold,consolenew;
static struct vt_sizes newvtsizes;
static struct vt_mode newvtmode,oldvtmode;

BYTE posx,posy;
BYTE posx2,posy2;
BYTE posxt,posyt;
BYTE salir;
BYTE comun;
BYTE mode;
BYTE macroplayrec;
BYTE macroindice;
WORD macrobuf[MACROSIZE];
BYTE dac[256*3];              /* DAC */
BYTE charbuf[6];
BYTE updatef;
BYTE autosavec;
WORD doscode;                 /* the code the keyboard 'returns' */
WORD vcharx1;
WORD vcharx2;
BYTE bit89;
BYTE animflag;
BYTE save2name[100];
BYTE save2nametemp[100];
BYTE save2cctr;
BYTE save2binctr;
BYTE loadfname[300];          /* new on v1.01 */
Gpm_Connect conn;             /* del mouse */
int opt_pointer;              /* del mouse */
BYTE minternal;               /* internal del mouse handler */

static struct caracter
{
   BYTE car[2][32];        /* principal y para futuro uso*/
} caracter;
static struct boton
{
   char nombre[2][MAXBOTLEN];
   BYTE x,y;
   BYTE activo;          /* esta prendido */
   BYTE n;               /* que mensaje mostrar */
   fn_hand *fn;          /* funcion asocioda */
   BYTE l;               /* logitud del boton */
   WORD k;               /* key asociada */
   WORD k2;              /* otra key asociada */
} boton[BOTONES];

/**************************************************** Funciones generales */
/* abre 0xa0000 */
int openmem_char( void ){
   fh=fopen("/dev/mem","r+");
   vid_addr= (BYTE*) mmap(
			  /* where to map, dont mind */
			  NULL,
			  /* how many bytes */
			  0x10000,
			  /* want to read and write */
			  PROT_READ | PROT_WRITE,
			  /* no copy on write */
			  MAP_SHARED,
			  /* handle to /dev/mem */
			  fileno(fh),
			  /* hopefully the Text-buffer :-) */
			  0xa0000);
   
   if(!vid_addr) return 1;
   return 0;
}
/* cierra mem 0xa0000 */
void closemem_char(void){
   munmap( (caddr_t) vid_addr,0x10000);
   fclose(fh);
}

int openmem_vid( void ){
   fh2=fopen("/dev/mem","r+");
   vid_addr2= (BYTE*) mmap(
			   NULL,
			   0x1000,
			   PROT_READ | PROT_WRITE,
			   MAP_SHARED,
			   fileno(fh2),
			   0xb8000 );
   
   if(!vid_addr2) return 1;
   return 0;
}
void closemem_vid(void){
   munmap( (caddr_t) vid_addr2,0x1000);
   fclose(fh2);
}
/* checkconsole sacada de las svgalib */
int checkconsole(void)
{
   struct stat chkbuf;
   int major, minor;
   int fd;
   
   /* get console number we are running in */
   fd = dup(2);		/* stderr */
   fstat(fd, &chkbuf);
   major = chkbuf.st_rdev >> 8;
   minor = chkbuf.st_rdev & 0xff;	/* console number */
   
   if (major != 4 || minor >= 64) 
     return 1;
   close(fd);
   return 0;
}
static void releasevt_signal(int n)
{
   BYTE alto,bajo;
   struct sigaction siga;
   WORD pos;
   outb(0x3d4,0x0f); /* low */
   bajo=inb(0x3d5);
   outb(0x3d4,0x0e); /* high */
   alto=inb(0x3d5);
   pos=alto*256 + bajo;
   posxt=pos%80;
   posyt=pos/80;

   SETSIG(siga, SIGUSR1, releasevt_signal); 
   ioctl(consolefd, VT_RELDISP, 1); 
   doubleno();
   return;
}

static void acquirevt_signal(int n)
{
   struct sigaction siga;
   SETSIG(siga, SIGUSR2, acquirevt_signal);
   ioctl(consolefd, VT_RELDISP, VT_ACKACQ);
   doublefont();
   savedac();
   setxy(posxt,posyt);
   viewfont();
   return;
}

void setxy(BYTE x,BYTE y){
   WORD xy=y*80+x;
   outb(0x3d4,0x0f); /* low */
   outb(0x3d5,(BYTE) xy & 255 );
   outb(0x3d4,0x0e); /* high */
   outb(0x3d5,(BYTE) ((xy >> 8)&255) );
}

void getxy(void){
   BYTE alto,bajo;
   WORD pos;

   outb(0x3d4,0x0f); /* low */
   bajo=inb(0x3d5);
   outb(0x3d4,0x0e); /* high */
   alto=inb(0x3d5);
   pos=alto*256 + bajo;
   posx=pos%80;
   posy=pos/80;
}

   
void showactive(void){
   BYTE i;
   init_vga();
   for(i=0;i<32;i++) vid_addr[0x4000+i] = caracter.car[0][i];
   rest_vga();
}
BYTE getvgaxy(short x,short y){
   return vid_addr2[y*160+x*2];
}
void dirvgaxy(int x,int y,BYTE carac){
   vid_addr2[y*160+x*2]=carac;
}
void dirvgaxya(int x,int y,WORD carcol){
   BYTE alto,bajo;
   bajo=(BYTE) carcol & 255;
   alto=(BYTE) ((carcol >> 8) & 255);
   vid_addr2[y*160+x*2  ]=bajo;
   vid_addr2[y*160+x*2+1]=alto;
}

/*********************************************** LI - Linux Interface */
void init_but(void)
{
   strcpy(boton[BOO].nombre[0],"F5:On/Off     ");
   strcpy(boton[BOO].nombre[1],"F5:Off/On     ");
   boton[BOO].x=11;
   boton[BOO].y=5;
   boton[BOO].activo=0;
   boton[BOO].n=0;
   boton[BOO].fn=cinvert;
   boton[BOO].l=BOTLEN;
   boton[BOO].k=KF5;
   boton[BOO].k2=KUNDEF;
   strcpy(boton[BUD].nombre[0],"F6:Up/Down    ");
   strcpy(boton[BUD].nombre[1],"F6:Down/Up    ");
   boton[BUD].x=11;
   boton[BUD].y=7;
   boton[BUD].activo=0;
   boton[BUD].n=0;
   boton[BUD].fn=cupdown;
   boton[BUD].l=BOTLEN;
   boton[BUD].k=KF6;
   boton[BUD].k2=KUNDEF;
   strcpy(boton[BID].nombre[0],"F7:Left/Right ");
   strcpy(boton[BID].nombre[1],"F7:Right/Left ");
   boton[BID].x=11 ;
   boton[BID].y=9;
   boton[BID].activo=0;
   boton[BID].n=0;
   boton[BID].fn=cleftright;
   boton[BID].l=BOTLEN;
   boton[BID].k=KF7;
   boton[BID].k2=KUNDEF;
   strcpy(boton[BCL].nombre[0],"F8:Clear      ");
   strcpy(boton[BCL].nombre[1],"F8:Clear      ");
   boton[BCL].x=11;
   boton[BCL].y=11;
   boton[BCL].activo=0;
   boton[BCL].n=0;
   boton[BCL].fn=cclean;
   boton[BCL].l=BOTLEN;
   boton[BCL].k=KF8;
   boton[BCL].k2=KUNDEF;
   strcpy(boton[BGR].nombre[0],"@F5:Orig Char ");
   strcpy(boton[BGR].nombre[1],"@F5:VChar Char");
   boton[BGR].x=11 + BOTLEN + 2;
   boton[BGR].y=5;
   boton[BGR].activo=0;
   boton[BGR].n=0;
   boton[BGR].fn=vcharchar;
   boton[BGR].l=BOTLEN;
   boton[BGR].k=KCF5;
   boton[BGR].k2=KUNDEF;
   strcpy(boton[BOR].nombre[0],"@F6:Rotate 90 ");
   strcpy(boton[BOR].nombre[1],"@F6:Rotate 90 ");
   boton[BOR].x=11 + BOTLEN + 2;
   boton[BOR].y=7;
   boton[BOR].activo=0;
   boton[BOR].n=0;
   boton[BOR].fn=crotate90;
   boton[BOR].l=BOTLEN;
   boton[BOR].k=KCF6;
   boton[BOR].k2=KUNDEF;
   strcpy(boton[B01].nombre[0],"@F7:Copy      ");
   strcpy(boton[B01].nombre[1],"@F7:Copy      ");
   boton[B01].x=11 + BOTLEN + 2;
   boton[B01].y=9;
   boton[B01].activo=0;
   boton[B01].n=0;
   boton[B01].fn=copyc;
   boton[B01].l=BOTLEN;
   boton[B01].k=KCF7;
   boton[B01].k2=KUNDEF;
   strcpy(boton[B02].nombre[0],"@F8:Paste     ");
   strcpy(boton[B02].nombre[1],"@F8:Paste     ");
   boton[B02].x=11 + BOTLEN + 2;
   boton[B02].y=11;
   boton[B02].activo=0;
   boton[B02].n=0;
   boton[B02].fn=pastec;
   boton[B02].l=BOTLEN;
   boton[B02].k=KCF8;
   boton[B02].k2=KUNDEF;
   strcpy(boton[BSA].nombre[0],"F4:        Save Char           ");
   strcpy(boton[BSA].nombre[1],"F4:        Save Char           ");
   boton[BSA].x=11;
   boton[BSA].y=13;
   boton[BSA].activo=0;
   boton[BSA].n=0;
   boton[BSA].fn=csavechar;
   boton[BSA].l=BOTLEN * 2 + 2;
   boton[BSA].k=KF4;
   boton[BSA].k2=KUNDEF;
   strcpy(boton[BAU].nombre[0]," \x18 ");
   strcpy(boton[BAU].nombre[1]," \x18 ");
   boton[BAU].x=11+3;
   boton[BAU].y=15;
   boton[BAU].activo=0;
   boton[BAU].n=0;
   boton[BAU].fn=casciidown;
   boton[BAU].l=3;
   boton[BAU].k=KPGUP;
   boton[BAU].k2=KUNDEF;
   strcpy(boton[BAD].nombre[0]," \x19 ");
   strcpy(boton[BAD].nombre[1]," \x19 ");
   boton[BAD].x=11+3;
   boton[BAD].y=17;
   boton[BAD].activo=0;
   boton[BAD].n=0;
   boton[BAD].fn=casciiup;
   boton[BAD].l=3;
   boton[BAD].k=KINSERT;
   boton[BAD].k2=KUNDEF;
   strcpy(boton[BL1].nombre[0]," \x18 ");
   strcpy(boton[BL1].nombre[1]," \x18 ");
   boton[BL1].x=19+6;
   boton[BL1].y=15;
   boton[BL1].activo=0;
   boton[BL1].n=0;
   boton[BL1].fn=scrollup;
   boton[BL1].l=3;
   boton[BL1].k=KHOME;
   boton[BL1].k2=KGUP;
   strcpy(boton[BL2].nombre[0]," \x19 ");
   strcpy(boton[BL2].nombre[1]," \x19 ");
   boton[BL2].x=19+6;
   boton[BL2].y=17;
   boton[BL2].activo=0;
   boton[BL2].n=0;
   boton[BL2].fn=scrolldown;
   boton[BL2].l=3;
   boton[BL2].k=KEND;
   boton[BL2].k2=KGDOWN;
   strcpy(boton[BL3].nombre[0]," \x1b ");
   strcpy(boton[BL3].nombre[1]," \x1b ");
   boton[BL3].x=15+6;
   boton[BL3].y=16;
   boton[BL3].activo=0;
   boton[BL3].n=0;
   boton[BL3].fn=scrollleft;
   boton[BL3].l=3;
   boton[BL3].k=KDELETE;
   boton[BL3].k2=KGLEFT;
   strcpy(boton[BL4].nombre[0]," \x1a ");
   strcpy(boton[BL4].nombre[1]," \x1a ");
   boton[BL4].x=23+6;
   boton[BL4].y=16;
   boton[BL4].activo=0;
   boton[BL4].n=0;
   boton[BL4].fn=scrollright;
   boton[BL4].l=3;
   boton[BL4].k=KPGDOWN;
   boton[BL4].k2=KGRIGHT;

   strcpy(boton[BF1].nombre[0],"F1:Edit Char  ");
   strcpy(boton[BF1].nombre[1],"F1:Edit Char  ");
   boton[BF1].x=81 - ( (BOTLEN + 2) * 2) ;
   boton[BF1].y=5;
   boton[BF1].activo=0;
   boton[BF1].n=0;
   boton[BF1].fn=cff1;
   boton[BF1].l=BOTLEN;
   boton[BF1].k=KF1;
   boton[BF1].k2=KUNDEF;
   strcpy(boton[BC1].nombre[0],"@F1:Edit Text ");
   strcpy(boton[BC1].nombre[1],"@F1:Edit Text ");
   boton[BC1].x=81 - ( (BOTLEN + 2) * 1) ;
   boton[BC1].y=5;
   boton[BC1].activo=0;
   boton[BC1].n=0;
   boton[BC1].fn=cfcf1;
   boton[BC1].l=BOTLEN;
   boton[BC1].k=KCF1;
   boton[BC1].k2=KUNDEF;
   strcpy(boton[B10].nombre[0],"F2:Ascii Table");
   strcpy(boton[B10].nombre[1],"F2:Ascii Table");
   boton[B10].x=81 - ( (BOTLEN + 2) * 2) ;
   boton[B10].y=7;
   boton[B10].activo=0;
   boton[B10].n=0;
   boton[B10].fn=asciitable;
   boton[B10].l=BOTLEN;
   boton[B10].k=KF2;
   boton[B10].k2=KUNDEF;
   strcpy(boton[B14].nombre[0],"@F2:Clear Text");
   strcpy(boton[B14].nombre[1],"@F2:Clear Text");
   boton[B14].x=81 - ( (BOTLEN + 2) * 1) ;
   boton[B14].y=7;
   boton[B14].activo=0;
   boton[B14].n=0;
   boton[B14].fn=cleartextedi;
   boton[B14].l=BOTLEN;
   boton[B14].k=KCF2;
   boton[B14].k2=KUNDEF;
   strcpy(boton[BSB].nombre[0],"F3:Save 2 bin ");
   strcpy(boton[BSB].nombre[1],"F3:Save 2 bin ");
   boton[BSB].x=81 - ( (BOTLEN + 2) * 2);
   boton[BSB].y=9;
   boton[BSB].activo=0;
   boton[BSB].n=0;
   boton[BSB].fn=save2bin;
   boton[BSB].l=BOTLEN;
   boton[BSB].k=KF3;
   boton[BSB].k2=KUNDEF;
   strcpy(boton[BSH].nombre[0],"@F3:Save 2 c  ");
   strcpy(boton[BSH].nombre[1],"@F3:Save 2 c  ");
   boton[BSH].x=81 - ( (BOTLEN + 2) * 1);
   boton[BSH].y=9;
   boton[BSH].activo=0;
   boton[BSH].n=0;
   boton[BSH].fn=save2c;
   boton[BSH].l=BOTLEN;
   boton[BSH].k=KCF3;
   boton[BSH].k2=KUNDEF;
   strcpy(boton[B15].nombre[0],"F9:Autosave On");
   strcpy(boton[B15].nombre[1],"F9:AutosaveOff");
   boton[B15].x=81 - ( (BOTLEN + 2) * 2) ;
   boton[B15].y=11;
   boton[B15].activo=0;
   boton[B15].n=0;
   boton[B15].fn=xautosave;
   boton[B15].l=BOTLEN;
   boton[B15].k=KF9;
   boton[B15].k2=KUNDEF;
   strcpy(boton[BVC].nombre[0],"@F9:VCharFonts");
   strcpy(boton[BVC].nombre[1],"@F9:Orig Fonts");
   boton[BVC].x=81 - ( (BOTLEN + 2) * 1) ;
   boton[BVC].y=11;
   boton[BVC].activo=0;
   boton[BVC].n=0;
   boton[BVC].fn=vcharfonts;
   boton[BVC].l=BOTLEN;
   boton[BVC].k=KCF9;
   boton[BVC].k2=KUNDEF;
   strcpy(boton[B11].nombre[0],"F10:Quit      ");
   strcpy(boton[B11].nombre[1],"Press any key!");
   boton[B11].x=81 - ( (BOTLEN + 2) * 2) ;
   boton[B11].y=13;
   boton[B11].activo=0;
   boton[B11].n=0;
   boton[B11].fn=csalir;
   boton[B11].l=BOTLEN;
   boton[B11].k=KF10;
   boton[B11].k2=KUNDEF;
   strcpy(boton[BAN].nombre[0],"@F10:Animation");
   strcpy(boton[BAN].nombre[1],"Not finished  ");
   boton[BAN].x=81 - ( (BOTLEN + 2) * 1) ;
   boton[BAN].y=13;
   boton[BAN].activo=0;
   boton[BAN].n=0;
   boton[BAN].fn=canim;
   boton[BAN].l=BOTLEN;
   boton[BAN].k=KCF10;
   boton[BAN].k2=KUNDEF;
   strcpy(boton[B12].nombre[0],"F11:Rec Macro ");
   strcpy(boton[B12].nombre[1],"F11:Stop Rec  ");
   boton[B12].x=81 - ( (BOTLEN + 2) * 2) ;
   boton[B12].y=15;
   boton[B12].activo=0;
   boton[B12].n=0;
   boton[B12].fn=macrorec;
   boton[B12].l=BOTLEN;
   boton[B12].k=KF11;
   boton[B12].k2=KUNDEF;
   strcpy(boton[B16].nombre[0],"@F11:8 Bits   ");
   strcpy(boton[B16].nombre[1],"@F11:9 Bits   ");
   boton[B16].x=81 - ( (BOTLEN + 2) * 1) ;
   boton[B16].y=15;
   boton[B16].activo=0;
   boton[B16].n=0;
   boton[B16].fn=c89bits;
   boton[B16].l=BOTLEN;
   boton[B16].k=KCF11;
   boton[B16].k2=KUNDEF;
   strcpy(boton[B13].nombre[0],"F12:Play Macro ");
   strcpy(boton[B13].nombre[1],"F12:Play Macro ");
   boton[B13].x=81 - ( (BOTLEN + 2) * 2) ;
   boton[B13].y=17;
   boton[B13].activo=0;
   boton[B13].n=0;
   boton[B13].fn=macroplay;
   boton[B13].l=BOTLEN;
   boton[B13].k=KF12;
   boton[B13].k2=KUNDEF;
   strcpy(boton[B17].nombre[0],"@F12:Grid On  ");
   strcpy(boton[B17].nombre[1],"@F12:Grid Off ");
   boton[B17].x=81 - ( (BOTLEN + 2) * 1) ;
   boton[B17].y=17;
   boton[B17].activo=0;
   boton[B17].n=0;
   boton[B17].fn=cgrid;
   boton[B17].l=BOTLEN;
   boton[B17].k=KCF12;
   boton[B17].k2=KUNDEF;
}

void redrawbut(void)
{
   BYTE att;
   WORD i,j;
   
   for(i=0;i<BOTONES;i++)
     {
	for(j=0;j<boton[i].l;j++)
	  {
	     att=(boton[i].activo==0) ? BotonOff : BotonOn;
	     vid_addr2[ (boton[i].y-1) * 160 + (boton[i].x-1) * 2 + j*2 + 1] = att;
	     vid_addr2[ (boton[i].y-1) * 160 + (boton[i].x-1) * 2 + j*2 ] = boton[i].nombre[ boton[i].n ][j];
	  }
     }
}

/*********************************************** Bati mouse - keyboard */

/* Esta funcion es una modificacion de Gpm_Getc de gpm-1.10 de liblow.c */
int Riq_Getc(FILE *f)
{
   fd_set selSet;
   int max, flag, result;
   static Gpm_Event ev;
   int fd=fileno(f);
   static int count;

  /* Hmm... I must be sure it is unbuffered */
  if (!(count++))
    setvbuf(f,NULL,_IONBF,0);

  if (!gpm_flag) return NOSEQUE;

  /* If the handler asked to provide more keys, give them back */
   if (gpm_morekeys) 
     {
	(*gpm_handler)(&ev,gpm_data);
	return KEY_SI;
     }
  gpm_hflag=0;
   
   max = (gpm_fd>fd) ? gpm_fd : fd;
   
   /*...................................................................*/
   if (gpm_fd>=0)                                            /* linux */
     while(1)
     {
	if (gpm_visiblepointer) GPM_DRAWPOINTER(&ev);
	do
	  {
	     FD_ZERO(&selSet);
	     FD_SET(fd,&selSet);
	     if (gpm_fd>-1)
	       FD_SET(gpm_fd,&selSet);
	     gpm_timeout.tv_sec=SELECT_TIME;
	     flag=select(max+1,&selSet,(fd_set *)NULL,(fd_set *)NULL,&gpm_timeout);
	  }
	while (!flag);
	
	if (flag==-1)
	  continue;
	
	/************ Aca esta el secreto!!!! **********************/
	if(FD_ISSET(fd,&selSet))
	  {
	     read(fd, &charbuf, sizeof(charbuf));
#ifdef DEBUG
	     printf("0x%2x, ",charbuf[0]);
	     printf("0x%2x, ",charbuf[1]);
	     printf("0x%2x, ",charbuf[2]);
	     printf("0x%2x, ",charbuf[3]);
	     printf("0x%2x, ",charbuf[4]);
	     printf("0x%2x\n\r",charbuf[5]);
	     fflush(stdout);
#endif
	     convertcode();                  /* convierte lo obtenido a potable */
	     return KEY_SI;
	  }
	
	if (Gpm_GetEvent(&ev) && gpm_handler && (result=(*gpm_handler)(&ev,gpm_data)))
	  {
	     gpm_hflag=1;
	     return KEY_SI;
	  }
     }
   return NOSEQUE;                        /* no mouse available */
}
	

/***********************************************  Funciones del mouse */
int mouse_handler(Gpm_Event *event, void *data)
{
   BYTE temp,i;
#ifdef DEBUG
   printf("mouse x,y:%i,%i\n\r",event->x,event->y);
#endif

   GPM_DRAWPOINTER(event);

   if (event->y <=16 && event->x <=8 && event->buttons==GPM_B_LEFT)
     {
	caracter.car[0][event->y-1]|= 1 << (7-(event->x-1));
	viewfont();
	updatef=UPDATEF;
     }
   else if (event->y <=16 && event->x <=8 && event->buttons==GPM_B_RIGHT)
     {
	caracter.car[0][event->y-1]&=255 -( 1 << (7-(event->x-1)));
	viewfont();
	updatef=UPDATEF;
     }
   else if ( event->y >= 20 && event->y <=24 && event->buttons !=0 )
     {
	switch( event->buttons )
	  {
	   case GPM_B_LEFT:
	     ascii=getvgaxy(event->x-1,event->y-1);
	     init_vga();
	     getfont(ascii);
	     rest_vga();
	     viewfont();
	     break;
	   case GPM_B_RIGHT:
	     temp=getvgaxy(event->x-1,event->y-1);
	     init_vga();
	     getfont(temp);
	     rest_vga();
	     viewfont();
	     break;
	  }
     }
   else 
     {
	for(i=0;i<BOTONES;i++)
	  {
	     if(event->y == boton[i].y &&
		event->x >= boton[i].x &&
		event->x <  boton[i].x+ boton[i].l )
	       {
		  if(event->type & GPM_DOWN)
		    {
		       minternal=i;
		       boton[i].activo=1;
		       redrawbut();
		    }
		  else if(event->type & GPM_UP)
		    {
		       if(minternal==i)
			 {
/*			    doscode=boton[i].k; */
			    (boton[i].fn)(); 
			    boton[i].activo=0;
			    boton[i].n^=1;
			    redrawbut();
/*			    comun=1; */
			 }
		    }
	       }
	  }
	if( (event->type & GPM_UP) && minternal!=255 )
	  {
	     for(i=0;i<BOTONES;i++)
	       boton[i].activo=0;
	     redrawbut();
	     minternal=255;
	  }
     }
   
   if( updatef==UPDATEF && ( event->dy!=0 || event->dx!=0))
     {
	viewfont();
	updatef=NOUPDATEF;
	GPM_DRAWPOINTER(event);
     }

   return 0;
}
int init_mouse(void)
{
   gpm_handler=mouse_handler;
   conn.eventMask=~0;
   conn.defaultMask=~GPM_HARD;
   conn.minMod=0;
   conn.maxMod=0;
   gpm_visiblepointer=0;
   
   /* do not force vc number (segun liblow.c) */
   return(Gpm_Open(&conn,0));
}
void rest_mouse(void)
{
   while (Gpm_Close()); /* close all the stack */
}
/***********************************************  Funciones del VChar */
void view_ascii(BYTE asc)
{
   BYTE asc_temp1;
   asc_temp1=asc/100;
   asc=asc%100;
   dirvgaxy(5,16,asc_temp1|0x30);
   asc_temp1=asc/10;
   asc=asc%10;
   dirvgaxy(6,16,asc_temp1|0x30);
   dirvgaxy(7,16,asc|0x30);
}

/* muestra el char que esta en caracter.car[0 o 1] */
void viewfont(void)
{
   short x,y;
   BYTE car,cartemp;
   
   for(y=0;y<16;y++)
     {
	cartemp=caracter.car[0][y];
	for(x=0;x<8;x++)
	  {
	     car=cartemp;
	     if( (cartemp&128)==0)
	       dirvgaxya(x,y,BigVacio);
	     else 
	       dirvgaxya(x,y,BigLleno);
	     cartemp=car<<1;
	  }
     }
   vid_addr2[0xa00]=ascii;
   view_ascii(ascii);
   showactive();
   if(autosavec==1)
     csavechar();
}

/* obtiene los bits del font */
void getfont(BYTE font)
{
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[0][i]=vid_addr[32*font+i];
}
void putfont(BYTE font)
{
   BYTE i;
   for(i=0;i<32;i++)
      vid_addr[32*font+i]=caracter.car[0][i];
}

/* modo ascii del teclado */
void init_key(void)
{
   ioctl(consolefd,KDGKBMODE,&keymode);
   ioctl(consolefd,KDSKBMODE,K_XLATE);
   ioctl(0, TCGETA, &consoleold);
   memcpy(&consolenew, &consoleold, sizeof(struct termio));
   consolenew.c_lflag &=~ (ICANON | ECHO | ISIG | IEXTEN );
   consolenew.c_iflag &=~ (ICRNL | INLCR |IGNCR  | IXON | IGNBRK | BRKINT | PARMRK );
   consolenew.c_oflag &=~ (OPOST);
   consolenew.c_cc[VMIN] =  1;
   consolenew.c_cc[VTIME] = 0;
   ioctl(0, TCSETA, &consolenew);
}
void rest_key(void)
{
   ioctl(0, TCSETA, &consoleold);
   ioctl(consolefd,KDSKBMODE,keymode);
}
void init_oldfonts(void)      /* new on v0.32 */
{
   WORD i;
   init_vga();
   for(i=0;i<256*32;i++)
     vid_addr[0x2000+i]=vid_addr[0x0000+i];
   rest_vga();
}
void init_ioctl(void)
{
   struct sigaction siga;

   newvtsizes.v_rows=25;
   newvtsizes.v_cols=80;
   newvtsizes.v_scrollsize=0;
   ioctl(consolefd,VT_RESIZE,&newvtsizes);

   ioctl(consolefd,VT_GETMODE,&oldvtmode);
   newvtmode=oldvtmode;
   newvtmode.mode=VT_PROCESS;
   newvtmode.relsig=SIGUSR1;
   newvtmode.acqsig=SIGUSR2;
   SETSIG(siga, SIGUSR1, releasevt_signal);
   SETSIG(siga, SIGUSR2, acquirevt_signal);
   ioctl(consolefd, VT_SETMODE, &newvtmode); 
}
void rest_ioctl(void)
{
   ioctl(consolefd,VT_SETMODE,&oldvtmode);
}

void init_screen(void)
{
	WORD z=0;
	int x,y;
	BYTE titulo[]=
	  {
		  "        ���                       �  �  �        � �                            "
		  "        � �                       �  �  �  �  �  � �  ����                      "
		  "        ���                       �� Ricardo Quesada                            "
	  };
	BYTE anim[]=
	  {
		  "������������� � ������������� � ������������� � ������������� � ������������� � "
	  };
	BYTE mensaje[]=
	  {
		  " � Click these letters with the left & right button - Note the differences      "
		  " � Use the Delete/Home/End/PgDown to scroll the character                       "
		  " � Do you like poems ? Read /usr/doc/vchar/DOS/vchar.txt                        "
		  "                                                         ...simplemente �����   "
		  "                                                                        �����   "
	  };
	BYTE copyri[]=
	  {
		  "               Send bugs, comments, etc to rquesada@dc.uba.ar                   "
	  };
	
	savedac();          /* Change dac colors */
	
	/* clear screen */
   for(x=0;x<80;x++)
     {
		 for(y=0;y<3;y++)
			 dirvgaxya(x,y,Fondo);
     }
	for(x=0;x<80;x++)
	  {
		  for(y=3;y<18;y++)
			  dirvgaxya(x,y,BorrarB);
	  }
	for(x=0;x<80;x++)
	  {
		  dirvgaxya(x,18,Borrar2);
	  }
	for(x=0;x<80;x++)
	  {
		  for(y=19;y<24;y++)
			  dirvgaxya(x,y,Borrar);
	  }
	for(x=0;x<80;x++)
	  {
		  dirvgaxya(x,24,Fondo);
	  }
	for(x=8;x<80;x++)
	  {
		  dirvgaxya(x,13,MacroFo);
	  }
	for(z=0;z<80*3;z++)
	  {
		  vid_addr2[z*2]=titulo[z];
	  }
	for(z=0;z<80*1;z++)
	  {
		  vid_addr2[160*18+z*2]=anim[z];
	  }
	for(z=0;z<80*5;z++)
	  {
		  vid_addr2[160*19+z*2]=mensaje[z];
	  }
	for(z=0;z<80*1;z++)
	  {
		  vid_addr2[160*24+z*2]=copyri[z];
	  }
	
	vid_addr2[0xa01]=0x07;
	dirvgaxya(9,1,SActive);          /* font que se ve en el momento */
	
	init_but();                     /* new on VChar LI */
	redrawbut();
	setxy(0,16);                      /* posciciona cursor */
}
void rest_screen(void)
{
   BYTE x,y;
   for(x=0;x<80;x++)
     {
	for(y=0;y<25;y++)
	  dirvgaxya(x,y,Limpio);
     }
   restdac();
}

/* setea la pantalla. En 0xa0000 estan los fonts */
void init_vga( void )
{
   outb(0x3c4,2);           /* addr Sequencer Controller 2nd register */
   srmap=inb(0x3c5);
   outb(0x3c5,4);           /* select bitplane 2 */

   outb(0x3c4,4);           /* Seq Controller 4th register */
   srmem=inb(0x3c5);
   outb(0x3c5,6);           /* 256k & linear processing */

   outb(0x3ce,4);           /* address of Graphic Controller */
   grmap=inb(0x3cf);        /* Select Read Map reg */
   outb(0x3cf,2);           /* Bitplane 2 */
   
   outb(0x3ce,5);           /* Select Graphics mode Register */
   grmod=inb(0x3cf);
   outb(0x3cf,0);           /* Write mode 0, linear addresses, 16 colors */
   
   outb(0x3ce,6);           /* Select miscellaneous register */
   grmis=inb(0x3cf);
   outb(0x3cf,4);           /* Text, linear addressess 64k at 0xa000 */
}
/* Restaura la VGA como debe ser */
void rest_vga( void )
{
   outb(0x3c4,2);           /* addr Sequencer Controller 2nd register */
   outb(0x3c5,srmap); 

   outb(0x3c4,4);           /* Seq Controller 4th register */
   outb(0x3c5,srmem);       /* 256k & linear processing */

   outb(0x3ce,4);           /* address of Graphic Controller */
   outb(0x3cf,grmap);       /* Bitplane 2 */
   
   outb(0x3ce,5);           /* Select Graphics mode Register */
   outb(0x3cf,grmod);       /* Write mode 0, linear addresses, 16 colors */
   
   outb(0x3ce,6);           /* Select miscellaneous register */
   outb(0x3cf,grmis);       /* Text, linear addressess 64k at 0xa000 */
}

/* pone los permisos necesarios para acceder a los ports */
int init_ports(void)
{
	return (ioperm(0x3b4, 0x3df - 0x3b4 + 1 , 1 ));

}
int init_console(void)
{
   return (consolefd=open("/dev/console",O_RDONLY));
}

void doublefont(void)
{
   WORD i,j;
   init_vga();
   for(j=0;j<256;j++)
     {
	for(i=0;i<16;i++)
	  {
	     vid_addr[0x4000+i+j*32]=FONTS[j*16+i];
	  }
     }
   outb(0x3c4,3);
   outb(0x3c5, (inb(0x3c5) & 0xc0) | 1);
   outw(0x3c4,0x0403); /* select 1st and 2nd font */
   rest_vga();
}
void doubleno(void)
{
   outb(0x3c4,3);
   outb(0x3c5,(inb(0x3c5) & 0xc0));
}
void nada(void){}
void casciiup(void)
{
   ascii--;
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();
}
void casciidown(void)
{
   ascii++;
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();
}
void csavechar(void)
{
   init_vga();
   putfont(ascii);
   rest_vga();
}
void csalir(void)
{
   salir=1;
}
void xautosave(void)
{
   autosavec^=1;
}
void cfcf1(void)
{
   if(mode==CARAC){
      mode=TEXTEDIT;
      setxy(posx2,posy2);
   }
   else if(mode==EDIT){
      mode=TEXTEDIT;
      setxy(posx2,posy2);
   }
   else if(mode==TEXTEDIT){
      mode=CARAC;
      setxy(0,16);
   }
}
void cff1(void)
{
   if(mode==CARAC){
      mode=EDIT;
      setxy(posx,posy);
   }
   else if(mode==EDIT){
      mode=CARAC;
      setxy(0,16);
   }
   else if(mode==TEXTEDIT){
      mode=EDIT;
      setxy(posx,posy);
   }
}
void cgrid(void)
{
   BYTE i;
   BYTE xorgrid[16]={
      "\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\xFF"
   };
   init_vga();
   for(i=0;i<16;i++)
     vid_addr[0x4000+216*32+i]^=xorgrid[i];
   rest_vga();
}
void crotate90(void)
{
   BYTE temp[16];
   BYTE i,j,k;

   for(i=0;i<16;i++)
     temp[i]=caracter.car[0][i];
   for(i=0;i<16;i++)
     caracter.car[0][i]=0;
   
   for(k=0;k<2;k++)
     {
	for(i=0;i<8;i++)
	  {
	     for(j=0;j<8;j++)
	       {
		  caracter.car[0][j+8*k] |= (  (temp[i+8*k] & 128) >>  (7 - i) );
		  temp[i+8*k] = temp[i+8*k] << 1;
	       }
	  }
     }
   viewfont();
}
void vcharchar(void)
{
   BYTE i;
   vcharx1^=6;
   init_vga();
   for(i=0;i<16;i++)
     caracter.car[0][i]=vid_addr[ 0x1000*vcharx1 + 32*ascii + i ];
   rest_vga();
   viewfont();
}
void vcharfonts(void)
{
   WORD i;
   vcharx2^=6;
   init_vga();
   for(i=0;i<32*256;i++)
     vid_addr[ 0x0000 + i ]=vid_addr[ 0x1000*vcharx2 + i ];
   rest_vga();
   viewfont();
}
void c89bits(void)
{
   switch(bit89)
     {
      case 0:         /* 8 bits */
	outb(0x3c2,inb(0x3cc) & 0xf3 );
	outw(0x3c4,0x0100);
	outw(0x3c4,0x0101);
	outw(0x3c4,0x0300);
	break;
      case 1:            /* 9 bits */
	outb(0x3c2,(inb(0x3cc) & 0xf3) | 4 );
	outw(0x3c4,0x0100);
	outw(0x3c4,0x0001);
	outw(0x3c4,0x0300);
	break;
     }
   bit89^=1;

/*   outb(0x3c0,0x13);
   outb(0x3c0,0); */
}
void canim(void)
{
   BYTE i;
   init_vga();
   for(i=0;i<16;i++)
     {
	vid_addr[32*254+i]^=vid_addr[32*255+i];
	vid_addr[32*255+i]^=vid_addr[32*254+i];
	vid_addr[32*254+i]^=vid_addr[32*255+i];
     }
   rest_vga();
}

void cinvert(void)
{
   BYTE i;
   for(i=0;i<32;i++)
     {
	caracter.car[0][i]^=255;
     }
   viewfont();
}
void cupdown(void)
{
   BYTE i;
   for(i=0;i<8;i++)
     {
	caracter.car[0][i]^=caracter.car[0][15-i];
	caracter.car[0][15-i]^=caracter.car[0][i];
	caracter.car[0][i]^=caracter.car[0][15-i];
     }
   viewfont();
}
void cleftright(void)
{
   BYTE i,j,k,l;
   for(i=0;i<16;i++)
     {
	j=caracter.car[0][i];
	k=0;
	for(l=0;l<4;l++)
	  {
	     k |= ( (j & (  1<< (7-l) )) >> (7-l*2) );
	     k |= ( (j & (128>> (7-l) )) << (7-l*2) );
	  }
	
	caracter.car[0][i]=k;
     }
   viewfont();
}
void scrollup(void)
{
   BYTE i,temp;
   temp=caracter.car[0][0];
   for(i=0;i<16;i++)
     caracter.car[0][i]=caracter.car[0][i+1];
   caracter.car[0][15]=temp;
   viewfont();
}
void scrolldown(void)
{
   BYTE i,temp;
   temp=caracter.car[0][15];
   for(i=15;i>0;i--)
     caracter.car[0][i]=caracter.car[0][i-1];
   caracter.car[0][0]=temp;
   viewfont();
}
void scrollright(void)
{
   BYTE i,temp;
   for(i=0;i<16;i++)
     {
	temp=caracter.car[0][i] >> 1;
	caracter.car[0][i]=temp | ( (caracter.car[0][i]&1) << 7) ;
     }
   viewfont();
}
void scrollleft(void){
   BYTE i,temp;
   for(i=0;i<16;i++){
      temp=caracter.car[0][i] << 1;
      caracter.car[0][i]=temp | ( (caracter.car[0][i]&128) >> 7) ;
   }
   viewfont();
}
void cclean(void){
   BYTE i;
   for(i=0;i<16;i++)
     caracter.car[0][i]=0;
   viewfont();
}
void macroview(void){
   BYTE asc,z,asc_temp1;
   BYTE macrotit[]={
      "Recording macro...Press F11 to stop. Buffer size[   ]"
   };
   for(z=0;z<53;z++)
     vid_addr2[160*13+z*2+17*2]=macrotit[z];
   asc=MACROSIZE-macroindice;
   asc_temp1=asc/100;
   asc=asc%100;
   dirvgaxy(66,13,asc_temp1|0x30);
   asc_temp1=asc/10;
   asc=asc%10;
   dirvgaxy(67,13,asc_temp1|0x30);
   dirvgaxy(68,13,asc|0x30);
}
void macronoview(void){
   BYTE z;
   for(z=0;z<53;z++)
     vid_addr2[160*13+z*2+17*2]=0x20;
}
void macrorec(void)
{
   WORD i;
   switch(macroplayrec)
     {
      case MACROSTOP: /* start recording */
	macroplayrec=MACROREC;
	macroindice=0;
	for(i=0;i<MACROSIZE;i++)
	  macrobuf[i]=0;
	macroview();
	break;
      case MACROREC: /* stop recording */
	macrobuf[macroindice+1]=0;
	macroplayrec=MACROSTOP;
	macronoview();
	break;
      case MACROPLAY:                  /* evita fucking bug ?*/
	break;
     }
}
void macroplay(void)
{
   if(macroplayrec==MACROSTOP)
     {
	if(macrobuf[0]!=0)
	  {
	     macroindice=0;
	     macroplayrec=MACROPLAY;
	  }
     }
   /*   else if(macroplayrec==MACROREC)
    macroindice--; */
}
void asciitable(void)
{
   WORD z;
   for(z=0;z<256;z++)
     vid_addr2[160*19+z*2]=(BYTE)z;
}
void cleartextedi(void)
{
   WORD z;
   for(z=0;z<5*80;z++)
     vid_addr2[160*19+z*2]=0x20;
}
void copyc(void)
{
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[1][i]=caracter.car[0][i];
}
void pastec(void)
{
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[0][i]=caracter.car[1][i];
   viewfont();
}

/* Teclas que son comunes a todos los modos */
void keycomun(void)
{
   BYTE i;
   for(i=0;i<BOTONES;i++)
     {
	if(doscode==boton[i].k || doscode==boton[i].k2 )
	  {
	     (boton[i].fn)();           /* ejecuta la funcion asociada */
	     boton[i].activo=0;
	     boton[i].n^=1;
	     redrawbut();
	     comun=1;
	  }
     }
}


/* esta rutina la necesito para usar los mismos valores que en DOS */
void convertcode(void)
{
   WORD temp=0,ORI=0;
   int modifiers;
   
   modifiers=6; /* code for the ioctl */
   if (ioctl(0,TIOCLINUX,&modifiers)<0)
     modifiers=0;
   if (modifiers&1) ORI|=KSHIFT;
   if (modifiers&2) ORI|=KRALT;
   if (modifiers&4) ORI|=KCONTROL;
   if (modifiers&8) ORI|=KLALT;
   if (!modifiers)  ORI|=KNONE;

   switch(charbuf[0]){
    case 0x1b:
      switch(charbuf[3]){       /* teclas grises */
       case 0x7e:
	 switch(charbuf[2]){
	  case 0x35:
	    temp=KPGUP;
	    break;
	  case 0x36:
	    temp=KPGDOWN;
	    break;
	  case 0x31:
	    temp=KHOME;
	    break;
	  case 0x34:
	    temp=KEND;
	    break;
	  case 0x32:
	    temp=KINSERT;
	    break;
	  case 0x33:
	    temp=KDELETE;
	    break;
	 }
	 break;
      }
      switch(charbuf[2]){
	  case 0x41:
	    temp=KUP;
	    break;
	  case 0x42:
	    temp=KDOWN;
	    break;
	  case 0x43:
	    temp=KRIGHT;
	    break;
	  case 0x44:
	    temp=KLEFT;
	    break;
	  case 0x78:
	    temp=KGUP;
	    break;
	  case 0x72:
	    temp=KGDOWN;
	    break;
	  case 0x76:
	    temp=KGRIGHT;
	    break;
	  case 0x74:
	    temp=KGLEFT;
	    break;
       case 0x5b:
	 switch(charbuf[3]){
	  case 0x41:
	    temp=KF1;
	    break;
	  case 0x42:
	    temp=KF2;
	    break;
	     case 0x43:
	    temp=KF3;
	    break;
	  case 0x44:
	    temp=KF4;
	    break;
	  case 0x45:
	    temp=KF5;
	    break;
	 }
	 break;
       case 0x31:
	 switch(charbuf[3]){
	  case 0x37:
	    temp=KF6;
	    break;
	  case 0x38:
	    temp=KF7;
	    break;
	  case 0x39:
	    temp=KF8;
	    break;
	 }
	 break;
       case 0x32:
	 switch(charbuf[3]){
	  case 0x30:
	    temp=KF9;
	    break;
	  case 0x31:
	    temp=KF10;
	    break;
	  case 0x33:
	    temp=KF11;
	    break;
	  case 0x34:
	    temp=KF12;
	    break;
	 }
	 break;
      }
      break; 
   }
   temp=(temp==0) ? charbuf[0] & 0xff:temp;
   doscode=( (temp&0x0f00)==0) ? temp : temp|ORI;
#ifdef DEBUG
   printf("doscode 0x%4x     ori 0x4%4x \n\r",doscode,ORI);
#endif
}

void loop(void)
{
   BYTE temp;
   
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();

   temp=0;
   
   do{
      if(macroplayrec!=MACROPLAY)
	{
	   Riq_Getc(stdin);
	   keycomun();
	   if(macroplayrec==MACROREC)
	     {
		macroview();
		if(macroindice>=MACROSIZE)
		  {
		     doscode=KF11;
		     keycomun();          /* simulo que se apreto F11 */
/*		     macroplayrec=MACROSTOP;
		     macrobuf[macroindice]=0;
		     macronoview(); */
		  }
		else 
		  macrobuf[macroindice]=doscode;
		macroindice++;            /* incrementa, total mac[0] no me sirve */
	     }
	   temp=1;
	}
      else if(macroplayrec==MACROPLAY)
	{
	   macroindice++;
	   if( macrobuf[macroindice]==0 || macroindice>=MACROSIZE ){
	      macroplayrec=MACROSTOP;
	      comun=1;                 /* BUG solucionado */
	   }
	   else
	     {
		doscode=macrobuf[macroindice];
		keycomun();
	     }
	   temp=1;
	}
      
      if(temp==1 && comun==0 )                      /* para que consuma menos ? */
	{
	   
	   if(mode==CARAC)
	     {
		switch(doscode)
		  {
		   case KUP:
		     ascii++; break;
		   case KDOWN:
		     ascii--; break;
		   case KRIGHT:
		     ascii=ascii+10;
		     break;
		   case KLEFT:
		     ascii=ascii-10;
		     break;
		   default:
		     if( doscode<0x00ff)
		       ascii=(BYTE) doscode;
		     break;
		  }
		init_vga();
		getfont(ascii);
		rest_vga();
		viewfont();
	     }
	   else if(mode==EDIT)
	     {
		switch(doscode)
		  {
		   case KUP:
		     posy--;
		     posy=(posy>15)?15:posy;
		     setxy(posx,posy); 
		     break;
		   case KDOWN:
		     posy++;
		     posy=(posy>15)?0:posy;
		     setxy(posx,posy); 
		     break;
		   case KRIGHT:
		     posx++;
		     posx=(posx>7)?0:posx;
		     setxy(posx,posy); 
		     break;
		   case KLEFT:
		     posx--;
		     posx=(posx>7)?7:posx;
		     setxy(posx,posy); 
		     break;
		   case KSPACE:
		     caracter.car[0][posy]^= 1 << (7-posx);
		     viewfont();
		     break;
		   case K1:
		     caracter.car[0][posy]|= 1 << (7-posx);
		     viewfont();
		     break;
		   case K2:
		     caracter.car[0][posy]&=255 -( 1 << (7-posx));
		     viewfont();
		     break;
		   default:
		     break;
		  }
	     }
	   else if(mode==TEXTEDIT)
	     {
		switch(doscode)
		  {
		   case KUP:
		     irarriba:
		     posy2--;
		     posy2=(posy2<19)?23:posy2;
		     setxy(posx2,posy2);
		     break;
		   case KDOWN:
		     irabajo:
		     posy2++;
		     posy2=(posy2>23)?19:posy2;
		     setxy(posx2,posy2);
		     break;
		   case KRIGHT:
		     irderecha:
		     posx2++;
		     if(posx2<=79)
		       setxy(posx2,posy2);
		     else
		       {
			  posx2=0;
			  goto irabajo;
		       }
		     break;
		   case KLEFT:
		     irizquierda:
		     posx2--;
		     if(posx2<=79)
		       setxy(posx2,posy2);
		     else
		       {
			  posx2=79;
			  goto irarriba;
		       }
		     break;
		   case KBACK:
		     if(posx2==0)
		       {
			  if(posy2==19)
			    dirvgaxy(79,23,0x20);
			  else 
			    dirvgaxy(79,posy2-1,0x20);
		       }
		     else 
		       dirvgaxy(posx2-1,posy2,0x20);
		     goto irizquierda;
		   case KENTER:
		     posx2=0;
		     goto irabajo;
		   default:
		     if( doscode<0x00ff)
		       dirvgaxy(posx2,posy2,(BYTE) doscode);
		     goto irderecha;
		     break;
		  }
	     }
	}
      temp=0;
      comun=0;
   } while(salir==0);
}


void savedac(void)
{
   WORD i=0;
   outb(0x3c6,0xff);        /*Pel Mask (255)*/
   outb(0x3c7,0);           /* Esto deberia esta en asm con  */
   for(i=0;i<256;i++)
     {      /* rep insb */
	dac[i*3+0]=inb(0x3c9);
	dac[i*3+1]=inb(0x3c9);
	dac[i*3+2]=inb(0x3c9);
     }
   outb(0x3c8,2);
   outb(0x3c9,dac[15*3+0]);
   outb(0x3c9,dac[15*3+1]);
   outb(0x3c9,dac[15*3+2]);
   
   outb(0x3c8,4);
   outb(0x3c9,dac[0*3+0]);
   outb(0x3c9,dac[0*3+1]);
   outb(0x3c9,dac[0*3+2]);
   
   outb(0x3c8,14);
   outb(0x3c9,dac[7*3+0]);
   outb(0x3c9,dac[7*3+1]);
   outb(0x3c9,dac[7*3+2]);
   
   outb(0x3c8,8);
   outb(0x3c9,dac[0*3+0]);
   outb(0x3c9,dac[0*3+1]);
   outb(0x3c9,dac[0*3+2]);

   outb(0x3c8,9);
   outb(0x3c9,dac[3*3+0]);
   outb(0x3c9,dac[3*3+1]);
   outb(0x3c9,dac[3*3+2]);
}
void restdac(void)
{
   WORD i=0;
   outb(0x3c6,0xff);        /*Pel Mask (255)*/
   outb(0x3c8,0);           /* Esto deberia esta en asm con  */
   for(i=0;i<256;i++)
     {      /* rep outb */
	outb(0x3c9,dac[i*3+0]);
	outb(0x3c9,dac[i*3+1]);
	outb(0x3c9,dac[i*3+2]);
     }
}

void save2c(void)
{
   WORD i,j,z;
   FILE *out;
   BYTE chartemp[80];
   BYTE saving[160]="Saving ";
   BYTE todook[]="  OK";
   BYTE todomal[]="  Error";
   BYTE agrega[]=".h";
   BYTE cnt[4];
   
   sprintf(cnt,"%d",save2cctr);
   strcpy(save2nametemp,save2name);
   strcat(save2nametemp,cnt);
   strcat(save2nametemp,agrega);
   strcat(saving,save2nametemp);
   
   for(z=0;z<80*1;z++)
     chartemp[z]=vid_addr2[160*24+z*2];
   for(z=0;z<80*1;z++)
     vid_addr2[160*24+z*2]=0x20;        /* limpia */
   for(z=0;z<80*1;z++)
     {
	if(saving[z]==0x0)
	  break;
	vid_addr2[160*24+z*2]=saving[z];
     }
   umask(18);           /* 022 */
   out=fopen(save2nametemp,"w+");
   if(out==NULL)
     {
	strcat(saving,todomal);
	for(z=0;z<80*1;z++)
	  {
	     if(saving[z]==0x0)
	       break;
	     vid_addr2[160*24+z*2]=saving[z];
	  }
	sleep(1);
	for(z=0;z<80*1;z++)
	  vid_addr2[160*24+z*2]=chartemp[z];
	return;
     }
   init_vga();
   fprintf(out,"/* VChar LI (c) Ricardo Quesada */\n");
   fprintf(out,"#define FONTH  16\n");
   fprintf(out,"#define FONTW   8\n");
   fprintf(out,"#define FONTL 256\n");
   fprintf(out,"unsigned char FONTS[]={\n");
   for(i=0;i<256;i++)
     {
	fprintf(out,"\"");
	for(j=0;j<16;j++)
	  fprintf(out,"\\x%X",vid_addr[i*32+j]);
	fprintf(out,"\"\t\t/* ascii %d */\n",i);
     }
   fprintf(out,"};\n");
   fflush(out);
   fclose(out);
   rest_vga();
   strcat(saving,todook);
   for(z=0;z<80*1;z++)
     {
	if(saving[z]==0x0)
	  break;
	vid_addr2[160*24+z*2]=saving[z];
     }
   sleep(1);
   for(z=0;z<80*1;z++)
     vid_addr2[160*24+z*2]=chartemp[z];
   save2cctr++;
}
void save2bin(void)
{
   WORD z,i;
   int fdbin;
   BYTE cnt[4];
   BYTE chartemp[80];
   BYTE saving[160]="Saving ";
   BYTE todook[]="  OK";
   BYTE todomal[]="  Error";
   BYTE agrega[]=".8x16";
   
   sprintf(cnt,"%d",save2binctr);
   strcpy(save2nametemp,save2name);
   strcat(save2nametemp,cnt);
   strcat(save2nametemp,agrega);
   strcat(saving,save2nametemp);
   
   for(z=0;z<80*1;z++)
     chartemp[z]=vid_addr2[160*24+z*2];
   for(z=0;z<80*1;z++)
     vid_addr2[160*24+z*2]=0x20;        /* limpia */
   for(z=0;z<80*1;z++)
     {
	if(saving[z]==0x0)
	  break;
	vid_addr2[160*24+z*2]=saving[z];
     }
   
   umask(18);              /* 022 */
   fdbin=open(save2nametemp,O_RDWR | O_CREAT | O_TRUNC,420);
   if(fdbin==-1)
     {
	strcat(saving,todomal);
	for(z=0;z<80*1;z++)
	  {
	     if(saving[z]==0x0)
	       break;
	     vid_addr2[160*24+z*2]=saving[z];
	  }
	sleep(1);
	for(z=0;z<80*1;z++)
	  vid_addr2[160*24+z*2]=chartemp[z];
	return;
     }

   init_vga();
   for(i=0;i<256;i++)
     write(fdbin,&vid_addr[i*32],16);
   close(fdbin);
   rest_vga();
   strcat(saving,todook);
   for(z=0;z<80*1;z++)
     {
	if(saving[z]==0x0)
	  break;
	vid_addr2[160*24+z*2]=saving[z];
     }
   sleep(1);
   for(z=0;z<80*1;z++)
     vid_addr2[160*24+z*2]=chartemp[z];
   save2binctr++;
}


void help(void)
{
   printf("\nVChar LI "VCHARVER" - by Ricardo Quesada (c) 1994-1997\n");
   printf("\nFormat:\n\tvchar [-h][-f fontname][-l altura][-b [8|9]][-d][-n savedname]\n\n");
   printf(
	  "\t-h             This help screen\n"
	  "\t-f fontname    Load fonts from a file\n"
	  "\t-l height      Size of char 8xHeight\n"
	  "\t-b [8|9]       Put the screen in 8 or 9 bits\n"
	  "\t-d             Dont load the editor,just the fonts.\n"
	  "\t-n savedname   Name that would have the saved fonts\n"
	  "\t               Default is: vcharfont\n"
	  "\t-q             Quiet mode\n"
	  "\n\nExample:\n"
	  "\tvchar \n"
	  "\tvchar -f /usr/lib/kbd/consolefonts/alt-8x8 -l 8 -b 9 -d\n"
	  "\tvchar -f grandes1 -d\n"
	  "\tvchar -f koi8-14.psf -d -q\n"
	  "\tvchar -f fonts/script\n"
	  "\tvchar -n fonts\n"
	  "\n"
	  );
   exit(-1);
}

/*           * * * *                 main               * * * *             */
int main(int argc,char **argv)
{
	int a,b,q;
	int psf;                     /* psf support - new on v1.01 */
	size_t ffilesize;        /* new on v1.01 */
	BYTE i;
	BYTE dontloop=0;
	BYTE loadfont=0;
	FILE *loadff;
	BYTE *fileaddr;
	BYTE charalt=16;
	
	
	ascii='a';             /* char a editrar */
	salir=0;               /* se apreto F10 */
	comun=0;               /* se apreto una tecla de las comunes */
	macroplayrec=0;macroindice=0;   /* indice y status del macro */
	posx=0;posy=0;posx2=0;posy2=19; /* pos de los cursores */
	minternal=255;           /* interno de los botones */
	autosavec=0;             /* del autosave F9 */
	vcharx1=0x4;             /* para xorear y ver que tabla de fonts se carga */
	vcharx2=0x2;
	bit89=0;
	animflag=0;
	save2binctr=0;           /* contador de los archivos grabados */
	save2cctr=0;
	q=0;                     /* quiet mode */
		
	strcpy(save2name,"vcharfont");
	
	for(i=0;i<MACROSIZE;i++)
		macrobuf[i]=0;
	mode=CARAC;
	
	for(i=1;i<argc;i++)
	  {
		  if(argv[i][0]=='-') 
			{
				switch(argv[i][1]) 
				  {
				  case 'b': case 'B':
					  if(argc>i+1)
						{
							switch(argv[i+1][0]){
							case '8':
								bit89=8;
								break;
							case '9':
								bit89=9;
							}
						}
					  else help();
					  i++;
					  break;
				  case 'd': case 'D':
					  dontloop=1;
					  break;
				  case 'f': case 'F':
					  if(argc>i+1) loadfont=i+1;
					  else help();
					  i++;
					  break;
				  case 'l': case 'L':
					  if (argc > i + 1)
						  charalt = atoi(argv[i + 1]);
					  else
						  help();
					  i++;
					  break;
				  case 'n': case 'N':
					  if (argc > i + 1)
						  strncpy(save2name,argv[i+1],100); /* Overflow bug */
					  else
						  help();
					  i++;
					  break;
				  case 'q': case 'Q':
					  q=1;
					  break;
				  case 'h': case 'H':
				  case '?': case '-':
				  default:
					  help();
					  break;
				  }
			}
		  else {
			  if(argv[i-1][0]!='-')
				  help();
		  }
	  }

	if(q!=1)
	   printf("\nVChar LI "VCHARVER" - by Ricardo Quesada (c) 1994-1997\n");
	
	/*   setuid(geteuid());
	 setgid(getegid()); */
	
	if(init_ports())
	  {
		  fprintf(stderr,"Error 0x01: No se puede tener acceso sobre los ports\n");	
		  return 1;
	  }
	if(init_console()<0)
	  {
		  fprintf(stderr,"Error 0x02: No se puede abrir /dev/console\n");
		  return 2;
	  }
	if(openmem_vid()!=0) 
	  {
		  fprintf(stderr,"Error 0x03: Problemas al mapear memoria.\n");
		  return 3;     /* reserva memoria para el juego de fonts */
	  }  
	if(openmem_char()!=0)
	  {
		  fprintf(stderr,"Error 0x04: Problemas al mapear memoria.\n");
		  return 4;     /* reserva memoria para el juego de fonts */
	  }  
	if(checkconsole()!=0)
	  {
		  fprintf(stderr,"Error 0x09: Imposible abrir pantalla vga.\n");
		  return 9;     /* Impide que se corra bajo X o algo raro como vt100 */
	  }  
	if(bit89>1)
	  {
		  bit89-=8;
		  c89bits();
	  }
	if(loadfont!=0)
	  {
		  strncpy(loadfname,argv[loadfont],300);       /* new on v1.01 */
		  if( (loadff=fopen(loadfname,"rb"))==0)
			{
				strcpy(loadfname,fontpath);
				strncat(loadfname,argv[loadfont],240);    /* overflow bug */
				if( (loadff=fopen(loadfname,"rb"))==0)
				  {
					  strcpy(loadfname,fontpath);
					  strncat(loadfname,argv[loadfont],240);     /* overflow bug */
					  strcat(loadfname,fontsufx);
					  if( (loadff=fopen(loadfname,"rb"))==0)
						{
							fprintf(stderr,"Error 0x07: Check file: %s\n",argv[loadfont]);
							closemem_char();
							closemem_vid();
							return 7;     /* reserva memoria para el juego de fonts */
						}
				  }
			}
		  
		  (void) fseek(loadff,0L,SEEK_END);        /* new on v1.01 */
		  ffilesize=(size_t) ftell(loadff) ;
		  rewind(loadff);
#ifdef DEBUG
		  printf("\nNombre:%s - Size:%i\n",loadfname,ffilesize);
		  sleep(5);
#endif
		  fileaddr= (BYTE*) mmap(
								 NULL,
								 ffilesize,
								 PROT_READ,
								 MAP_SHARED,
								 fileno(loadff),
								 0x0);
		  if(!fileaddr)
			{
				closemem_char();
				closemem_vid();
				fprintf(stderr,"Error 0x08: Error al mapear archivo\n");
				return 8;
			}
		  fflush(stdout);
		  init_vga();
		  for(a=0;a<256*32;a++) /* borra todo */
			  vid_addr[a]=0;
		  
		  psf=0 ;    /* new on v1.01 - psf support */
		  if( fileaddr[0]==0x36 && fileaddr[1]==0x04 && fileaddr[2]==0 )
			{
				psf=4 ;
				charalt=fileaddr[3];
			}
		  for(a=0;a<256;a++)
			{
				for(b=0;b<charalt;b++)
				  {
					  vid_addr[a*32+b]=fileaddr[a*charalt+b+psf];
				  }
			}
		  rest_vga();
		  munmap( (caddr_t) fileaddr,ffilesize);
		  fclose(loadff);
	  }
	
	if(dontloop==1)
	  {
		  closemem_char();
		  closemem_vid();
		  seteuid(getuid());
		  setegid(getgid());            /* no more supervisor rights */
		  return 0;
	  }
	if( (opt_pointer=open(ttyname(0),O_RDWR))==-1)
	  {
		  fprintf(stderr,"Error 0x05: open(ttyname).\n");
		  seteuid(getuid());
		  setegid(getgid());
		  return 5;      /* Abre para el fucking mouse */
	  }
	if(init_mouse()==-1)
	  {
		  fprintf(stderr,"Error 0x06: Cargar el gpm.\n\r");
		  seteuid(getuid());
		  setegid(getgid());            /* no more supervisor rights */
		  return 6;      /* conecta con gpm */
	  }
	
	init_ioctl();                 /* VC */
	init_screen();                /* dibuja pantalla */
	doublefont();                 /* dos fonts activos */
	init_key();                   /* key k_xlate mode */
	init_oldfonts();               /* restaura viejos fonts */
	
	seteuid(getuid());
	setegid(getgid());            /* no more supervisor rights */

	loop();                       /* hace todo */
	
	rest_key();                    /* vuelte al estado anterior del teclado */
	rest_mouse();
	rest_screen();
	doubleno();                   /* 1 font activo */
	closemem_char();
	closemem_vid();
	rest_ioctl();
	return 0;
}
