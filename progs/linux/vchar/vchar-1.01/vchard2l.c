/***********************************************************************
 * Converts VChar NI (dos) saved fonts to VChar LI (linux) saved fonts *

 * Para mantener compatibilidad con la setfont se modifico
 * el formato.
 */



#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#define VVER "1.00"

void help(void)
{
   printf("\nFormat:\n\tvchard2l [-d dosfont][-l linuxfont]\n\n");
   printf(
	  "\t-h               This help screen\n"
	  "\t-d dosfontname   The name of the file that would be converted\n"
	  "\t                 * Required *\n"
	  "\t-l linuxfontname The name that would have the converted file\n"
	  "\t                 Default is: vcharfont.8x16\n"
	  "\n\nExample:\n"
	  "\tvchard2l -d script.com -l script.8x16\n"
	  "\tvchard2l -d font0000.com\n"
	  "\n"
	  );
   exit(-1);
}

int main(int argc, char *argv[])
{
   int req=0;
   int i;
   int fdin,fdout;
   char outname[80]="vcharfont.8x16";
   char inname[80]="";
   char temp[180];
   char compni[8]="VChar NI";
   char comprc[8]="RChar";
   unsigned char fonts[4100];
   
   printf("\nvchard2l "VVER" - por Ricardo Quesada (c) 1996\n");
   for(i=1;i<argc;i++)
     {
	if(argv[i][0]=='-') 
	  {
	     switch(argv[i][1]) 
	       {
		case 'd': case 'D':
		  if(argc>i+1)
		    {
		       if (argc > i + 1)
			 strcpy(inname,argv[i+1]);
		       else
			 help();
		       i++;
		    }
		  else help();
		  req=1;
		  break;
		case 'L': case 'l':
		  if (argc > i + 1)
		    strcpy(outname,argv[i+1]);
		  else
		    help();
		  i++;
		  break;
		case 'h': case 'H':
		case '?': case '-':
		default:
		  help();
		  break;
	       }
	  }
	else {
	   if(argv[i-1][0]!='-')
	     help();
	}
     }
   if(req!=1)
     help();
   
   if( (fdin=open(inname,O_RDONLY,420))==-1)
     {
	fprintf(stderr,"Error al tratar de cargar el archivo:%s\n",inname);
	exit(-1);
     }
   read(fdin,temp,177);           /* chequea si en un archivo LI */
   
   if( strncmp(&temp[2],compni,8)==0 )
     {
	printf("Leyendo datos de %s (VChar NI)\n",inname);
	read(fdin,fonts,4096);           /* chequea si en un archivo LI */
     }
   else if( strncmp(&temp[3],comprc,5)==0 )
     {
	read(fdin,temp,246-177);
	printf("Leyendo datos de %s (RChar)\n",inname);
	read(fdin,fonts,4096);           /* chequea si en un archivo LI */
     }
   else
     {
	fprintf(stderr,"Error.%s no es un archivo grabado por VChar NI\n",inname);
	close(fdin);
	exit(-1);
     }
   if( (fdout=open(outname,O_RDWR | O_CREAT | O_TRUNC,420))==-1)
     {
	fprintf(stderr,"Error al tratar de grabar el archivo:%s\n",outname);
	exit(-1);
     }
   write(fdout,fonts,4096);
   close(fdin);
   close(fdout);
   printf("Archivo convertido con el nombre de %s\n",outname);
   return 0;
}
