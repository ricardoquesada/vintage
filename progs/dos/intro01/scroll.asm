;name: demo
;verion: 1.00
;start  date: 18/7/94
;finish date:
;author: Ricardo Quesada

P286                            ;procesadores 286 para arriba

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;****************************************;
;************* START ********************;
;****************************************;

start:

;******* MANAGE MEMORY **********
        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;*********** inicializar !
        call isvga
        cmp al,1ah              ;compara si es placa VGA
        je vgaonly

        mov ah,9
        mov dx,offset vga_msg
        int 21h

        jmp fincito             ;terminar si no es VGA

vgaonly:
        call clrscr             ;borra pantalla
        call bit8

        mov bp,offset TABLA2
        mov bh,08h           ;cantidad de bytes x char
        mov cx,100h          ;total de chars (256)
        xor dx,dx            ;a partir de que char (0).
        xor bl,bl            ;map 2.Grabar en 1er tabla
        mov ax,1110h         ;Define los carecteres creado por el usuario.
        int 10h

;*********** scroll ************;

scro:
        mov dx,03dah            ;cheaquea vertical retrace
vertical3:
        in al,dx
        test al,8
        jne vertical3
vertical4:
        in al,dx
        test al,8
        je vertical4

scrollagain:
        mov ah,scroll
        inc ah
        cmp ah,9
        jne aaaa
        xor ah,ah
aaaa:
        mov scroll,ah
        cli
        mov dx,3c0h             ;attribute controller
        mov al,33h              ;13h or 20h   bit 5 on IMPORTANT!
        out dx,al               ;
        mov al,scroll           ;nueva poscicion!
        out dx,al               ;de nuevo fierita... Je Je Je...
        sti
        cmp al,8
        je scroll_left
        jmp tekey

scroll_left:
        push ds
        cld                     ;direction flag
        mov ax,0b800h
        mov ds,ax
        mov es,ax
        mov si,2                ;ds:si  fuente
        xor di,di               ;es:di  destino
        mov cx,( 160 * 16 )-2
rep     movsb                   ;mueve todo uno a la izquierda.
        pop ds

        mov bx,puntero
        mov si,offset texto

        mov al,[si+bx]
        or al,al               ;llego a la marca del final
        jne nofindetexto
        jmp fin_de_texto

nofindetexto:
        xor ah,ah
        mov cl,4                ;multiplicar por 16
        shl ax,cl
        mov di,offset tabla
        add di,ax
nextindex:
        cmp indexletra,17
        jne nextchar
        jmp nextletra2
nextchar:
        mov bl,[di]             ;en bl=byte a escribir

        mov cl,letraend
        shl bl,cl               ;lo gira tantas veces.
        test bl,128
        je bitoff

biton:
        push ds
        mov bx,0b800h
        mov ds,bx
        mov al,160
        mul cs:indexletra
        sub ax,2
        mov si,ax
        mov [si],0fb0h              ;pone nuevo caracter en pantalla
        pop ds
        inc di
        inc indexletra
        jmp nextindex

bitoff:
        push ds
        mov bx,0b800h
        mov ds,bx
        mov al,160
        mul cs:indexletra
        sub ax,2
        mov si,ax
        mov [si],0720h          ;pone nuevo caracter en pantalla
        pop ds
        inc di
        inc indexletra
        jmp nextindex

nextletra2:
        mov indexletra,1
        inc letraend
        cmp letraend,8
        jb  nonextletra
nextletra:
        mov letraend,0
        inc puntero             ;incrementa el puntero
nonextletra:
        jmp tekey               ;fin de interrupcion


fin_de_texto:
        push cs
        pop ds
        mov indexletra,1
        mov puntero,0
        mov letraend,0

tekey:
        inc vels
        cmp vels,4
        je animloopend
        jmp scrollagain

animloopend:
        mov vels,0
        cmp anim,5
        jne dacahora
        call animacion
        mov anim,0
dacahora:
        inc anim
        cmp dacfl,10
        jne teclaya
        call dac
        mov dacfl,0
teclaya:
        inc dacfl
        in  al,060h
        cmp al,01h
        je  escape
        jmp scro

escape:
        mov ax,1000h
        mov bx,0813h
        int 10h                 ;posciciona a poscicion original
        call bit9
fincito:
        mov ax,4c00h
        int 21h                 ;fin del programa


;***********************;
;       clrscr          ;
;***********************;
clrscr proc near
        pusha
        mov ah,7
        mov al,0
        mov bh,07h
        mov ch,0
        mov cl,0
        mov dh,50
        mov dl,80
        int 10h                 ;scroll window
        mov ax,500h
        int 10h                 ;Select active display page
        mov al,3                ;es el numero 3
        xor ah,ah
        int 10h                 ;no entonces lo pone
        mov ah,2
        mov bh,0
        mov dx,0ffffh
        int 10h                 ;set cursor position
        popa
        ret
clrscr  endp


;***********************;
;       isvga           ;
;***********************;
isvga proc near
        mov ax,1a00h
        int 10h
        ret
isvga  endp

;*******************************;
;                               ;
;         animacion             ;
;                               ;
;*******************************;
animacion proc near
        call inicializar        ;permite acceso al segmento A000
        mov ax,0a000h           ;segmento a000h
        mov ds,ax
        mov es,ax

        cld
        mov si,176 * 32         ;1er hombre
        mov di,175 * 32         ;2do hombre
        mov cx,32 * 6
        rep movsb

        mov si,175 * 32
        mov di,181 * 32
        mov cx,32
        rep movsb

        call restaurar
        push cs
        pop ds
        ret
animacion endp

;*******************************;
; DAC                           ;
;*******************************;
dac proc near
    mov al,63
    mov dx,3c7h
    out dx,al
    add dx,2

    in al,dx
    mov cl,al
    in al,dx
    mov ch,al
    in al,dx
    mov bl,al

    cmp cs:dacdir,0
    je disdir

    cmp ch,60
    jae chgdir
    inc ch
    inc bl
    jmp short writedac

disdir:
    or ch,ch
    je chgdir
    dec ch
    dec bl

writedac:
    mov al,63
    mov dx,3c8h
    out dx,al
    inc dx
    mov al,cl
    out dx,al
    mov al,ch
    out dx,al
    mov al,bl
    out dx,al
    ret
chgdir:
    xor cs:dacdir,1
    ret
dac endp
;*******************************;
;       inicializar             ;
;*******************************;
;use bitplane 2 , seg at a000h  ;
inicializar proc near
        push cs
        pop ds
        cli                     ;clear interrup flag
        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al
        inc dx                  ; data of Sequencer Controller
        in al,dx                ;
        mov srmap,al            ; restaura valor origianal
        mov al,04h              ; select bitplane 2
        out dx,al               ;
        mov dx,3c4h             ; Sequencer Controller
        mov al,4                ; 4th register
        out dx,al
        inc dx
        in al,dx                ;
        mov srmem,al            ;restaura valor original
        mov al,7
        out dx,al               ;256k & linear processing.
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register
        inc dx
        in al,dx
        mov grmap,al            ;restaura valor original
        mov al,02h              ;Bitplane 2
        out dx,al
        mov dx,3ceh             ;Graphic Controller
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register
        inc dx
        in al,dx
        mov grmod,al            ;restaura valor original
        mov al,0
        out dx,al               ;Write mode 0,linear adresses,16 colors
        mov dx,3ceh             ;Graphic controller
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register
        inc dx
        in al,dx
        mov grmis,al            ;restaura valor original
        mov al,04h
        out dx,al               ;Text,linear addresses,64k at A000h
        sti                     ;set interrupt flag
        ret
inicializar endp

;******************************;
;       restaurar              ;
;******************************;
restaurar proc near
        push cs
        pop ds
        cli
        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al
        inc dx                  ; data of Sequencer Controller
        mov al,srmap            ;
        out dx,al               ; restaura
        mov dx,3c4h
        mov al,4                ; 4th register
        out dx,al
        inc dx
        mov al,srmem            ;
        out dx,al               ;restaura
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register
        inc dx
        mov al,grmap
        out dx,al               ;restaura
        mov dx,3ceh
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register
        inc dx
        mov al,grmod
        out dx,al               ;restaura
        mov dx,3ceh
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register
        inc dx
        mov al,grmis
        out dx,al               ;restaura
        sti
        ret
restaurar endp

;;;;;;;;;;;;;;;;;;;;; bit 8;;;;;;;;;;;;;;;
bit8 proc
    mov dx,3cch
    in al,dx
    and al,11110011b
    mov dx,3c2h
    out dx,al
    cli
    mov dx,3c4h
    mov ax,100h
    out dx,ax
    mov ax,101h
    out dx,ax
    mov ax,300h
    out dx,ax
    sti
    mov ax,1000h
    mov bl,13h
    mov bh,0
    int 10h
ret
bit8 endp
bit9 proc
    mov dx,3cch
    in al,dx
    and al,11110011b
    or al,00000100b
    mov dx,3c2h
    out dx,al
    cli
    mov dx,3c4h
    mov ax,100h
    out dx,ax
    mov ax,001h
    out dx,ax
    mov ax,300h
    out dx,ax
    sti
    mov ax,1000h
    mov bl,13h
    mov bh,8
    int 10h
    ret
bit9 endp



;****************************************;
;*************    DATA    ***************;
;****************************************;
srmap   db ?            ;Sequencer Register, Read Map
srmem   db ?            ;                  , Memory Mode
grmap   db ?            ;Graphics Register, Read Map
grmod   db ?            ;                 , Graph Mode
grmis   db ?            ;                 , Miscellaneous

dacdir  db 0
dacfl   db 0
vels    db 0
anim    db 0
letraend db 0
indexletra db 0
scancode dw 0           ;tecla pulsada Scan Code + Ascii
scroll  db 0            ;valor del scroll
puntero dw 0            ;puntero del scroll
texto db ' Me recuerdan, si soy yo , Erasoft pero esta vez en las PC trantando de hacer una fucking intro...'
db'Espero que salga algo potable... Ja Ja Ja... para empezar quiero hacer algo parcedio a la intro 10 que hice en la comodore 64'
db'Bueno, espero tener suerte... Don Riq.... Ricardo Quesada ... El ni�o del Sol... Erasoft 94....   ',0

vga_msg db'Sorry, VGA only!',13,10,'$'

include font0000.asm
include ffff0000.asm

final   equ this byte
ruti    endp
code    ends
        end ruti
