;name: demo
;verion: 1.00
;start	date: 18/7/94
;finish date:
;author: Ricardo Quesada

P286							;procesadores 286 para arriba

code	segment public byte 'code'
org 	100h
ruti	PROC FAR
		assume cs:code,ds:code,ss:code,es:code

;****************************************;
;************* START ********************;
;****************************************;

start:
        jmp realstart
db 13,10,13,10,'El ni�o del Sol - Octubre de 1994.',13,10,13,10

realstart:
        xor cx,cx
        mov si,offset xorini
        xor al,al
 todavia_sigo:
        xor [si],al
        add al,3
        inc si
        inc cx
        cmp cx,final - xorini
        jb todavia_sigo
        int 20h
xorini:
;******* MANAGE MEMORY **********
		mov ah,4ah
		mov bx,offset final
		add bx,15
		shr bx,4
		inc bx
		int 21h 				;*** Change memory size ***

;*********** inicializar !
        call is286
        call isvga
		cmp al,1ah				;compara si es placa VGA
		je vgaonly

		mov ah,9
		mov dx,offset vga_msg
		int 21h

		jmp fincito 			;terminar si no es VGA

vgaonly:
		call clrscr 			;borra pantalla

; * esto es el titulo * ;

        mov ax,0b800h
        mov es,ax
        push cs
        pop ds
        mov di,18 * 160
        mov si,offset pantalla
        mov ah,4
        xor cx,cx
tituloloop:
        mov al,[si]
        mov es:[di],ax
        inc si
        add di,2
        inc cx
        cmp cx,5 * 80
        jb tituloloop

        push cs
        pop es

    push ds
    mov cx,3 * 160
    mov ax,0b800h
    mov es,ax
    mov di,(24 * 160) + 2       ;es:di
    push cs
    pop ds
    mov si,offset puto          ;ds:si
    rep movsb
    pop ds
    push cs
    pop es

		call bit8
		mov bp,offset TABLA2
		mov bh,08h			 ;cantidad de bytes x char
		mov cx,100h 		 ;total de chars (256)
		xor dx,dx			 ;a partir de que char (0).
		xor bl,bl			 ;map 2.Grabar en 1er tabla
		mov ax,1110h		 ;Define los carecteres creado por el usuario.
		int 10h

		mov ah,2
		mov bh,0
		mov dx,0ffffh
		int 10h 				;set cursor position

        call newcolor

;*********** scroll ************;

scro:
		mov dx,03dah			;cheaquea vertical retrace
vertical3:
		in al,dx
		test al,8
		jne vertical3
vertical4:
		in al,dx
		test al,8
		je vertical4

		push ds
		cld 					;direction flag
		mov ax,0b800h
		mov ds,ax
		mov es,ax
		mov si,2				;ds:si	fuente
		xor di,di				;es:di	destino
        mov cx,160 * 16
rep 	movsb					;mueve todo uno a la izquierda.

		std
		mov si,(160 * 50) - 2	;es de 0 a 49
		mov di,(160 * 50)
        mov cx,( 160 * 16 )
rep 	movsb

		pop ds

		mov bx,puntero
		mov si,offset texto

		mov al,[si+bx]
		or al,al			   ;llego a la marca del final
		jne nofindetexto
		jmp fin_de_texto

nofindetexto:
		xor ah,ah
		mov cl,4				;multiplicar por 16
		shl ax,cl
		mov di,offset tabla
		add di,ax
nextindex:
        cmp indexletra,16
		jne nextchar
		jmp nextletra2
nextchar:
		mov bl,[di] 			;en bl=byte a escribir

		mov cl,letraend
		shl bl,cl				;lo gira tantas veces.
		test bl,128
		je bitoff

biton:
		push ds
		mov bx,0b800h
		mov ds,bx
		mov al,160
        mul cs:indexletra
        mov si,ax
        mov [si],0fb4h              ;pone nuevo caracter en pantalla

		mov al,160
		mul cs:indexletra2
		mov si,ax
        mov [si],0fbah

		pop ds
		inc di
		inc indexletra
		dec indexletra2
		jmp nextindex

bitoff:
		push ds
		mov bx,0b800h
		mov ds,bx

		mov al,160
		mul cs:indexletra
        mov si,ax
        mov [si],0fe6h          ;pone nuevo caracter en pantalla

		mov al,160
		mul cs:indexletra2
		mov si,ax
        mov [si],0fe6h

		pop ds
		inc di
		inc indexletra
		dec indexletra2
		jmp nextindex

nextletra2:
        mov indexletra,0
        mov indexletra2,49
		inc letraend
		cmp letraend,8
		jb	nonextletra
nextletra:
		mov letraend,0
		inc puntero 			;incrementa el puntero
nonextletra:
		jmp tekey				;fin de interrupcion


fin_de_texto:
		push cs
		pop ds
		mov indexletra,1
		mov puntero,0
		mov letraend,0

tekey:
		cmp anim,5
		jne dacahora
		call animacion
		mov anim,0
dacahora:
		inc anim
        cmp dacfl,2
		jne teclaya
		call dac
		mov dacfl,0
teclaya:
		inc dacfl

        cmp anim2,14
        jne incanim2
        call animacion2
        mov anim2,0
incanim2:
        inc anim2

        cmp anim3,7
        jne incanim3
        call animacion3
        mov anim3,0
incanim3:
        inc anim3

        cmp bailarin,500
        je showbailarin
        cmp bailarin,501
        je nomorebailarin
        jne incbailarin
showbailarin:
        call sbailarin
incbailarin:
        inc bailarin
nomorebailarin:

        cmp dacfl2,20
        jne incdac2
        call dac2
        mov dacfl2,0
incdac2:
        inc dacfl2

        cmp esperatexto,250
        je muestratexto
        inc esperatexto
        jmp finstexto
muestratexto:
        cmp texto1fl,0
        je showtext
        cmp texto1fl,350
        je borratexto
        jmp inctexto1
showtext:
        call showtexto
        jmp inctexto1
borratexto:
        call cleantexto
        mov esperatexto,0
        mov texto1fl,0
        jmp finstexto
inctexto1:
        inc texto1fl
finstexto:

        cmp fondo3,2000
        je fondo3si
        inc fondo3
        jmp getkey
fondo3si:
        cmp fondo4,60
        jne fondo4si

        mov fondo4,0
        mov fondo3,0
        call animacion5
        jmp getkey
fondo4si:
        inc fondo4

        cmp fondo1,12
        jne incfondo1
        call animacion4
        mov fondo2,1
        mov fondo1,0
incfondo1:
        inc fondo1

        cmp fondo2,0
        je getkey
        cmp fondo2,6
        jne incfondo2
        call animacion5
        mov fondo2,0
incfondo2:
        inc fondo2
getkey:
        in  al,060h
		cmp al,01h
		je	escape
		jmp scro

escape:
		mov ax,1000h
		mov bx,0813h
		int 10h 				;posciciona a poscicion original
		call bit9
		mov ax,3
        int 10h
fincito:
		mov ax,4c00h
		int 21h 				;fin del programa


;***********************;
;		clrscr			;
;***********************;
clrscr proc near
		pusha
		mov ah,7
		mov al,0
		mov bh,07h
		mov ch,0
		mov cl,0
		mov dh,50
		mov dl,80
		int 10h 				;scroll window
		mov ax,500h
		int 10h 				;Select active display page
		mov al,3				;es el numero 3
		xor ah,ah
		int 10h 				;no entonces lo pone
		popa
		ret
clrscr	endp

;***********************;
; Is286                 ;
;***********************;
is286 proc near
    xor ax,ax
    push ax
    popf
    pushf
    pop ax
    and ax,0f000h
    cmp ax,0f000h
    je not286
    ret
not286:
    mov dx,offset no286msg
    mov ah,9
    int 21h
    mov ax,4cffh
    int 21h
is286 endp
;***********************;
;		isvga			;
;***********************;
isvga proc near
		mov ax,1a00h
		int 10h
		ret
isvga  endp

;*******************************;
;								;
;		  animacion 			;
;								;
;*******************************;
animacion proc near
		call inicializar		;permite acceso al segmento A000
		mov ax,0a000h			;segmento a000h
		mov ds,ax
		mov es,ax

		cld
        mov si,180 * 32         ;1er hombre
        mov di,179 * 32         ;2do hombre
		mov cx,32 * 6
		rep movsb

        mov si,179 * 32
        mov di,185 * 32
		mov cx,32
rep 	movsb

		std
        mov si,(192 * 32) - 1
        mov di,(193 * 32) - 1
		mov cx,32 * 6
rep 	movsb
		cld
        mov si,192 * 32
        mov di,186 * 32
		mov cx,32
rep 	movsb

		call restaurar
		push cs
		pop ds
		ret
animacion endp

animacion2 proc near
        call inicializar
        push ds

        mov ax,0a000h
        mov ds,ax
        xor cx,cx               ;contador
        mov si,252 * 32         ;1er hombre
        mov di,254 * 32         ;2do hombre
seguiggg:
        mov ah,[si]
        xchg [di],ah
        mov [si],ah
        inc si
        inc di
        inc cx
        cmp cx,64
        jb seguiggg
        pop ds
        call restaurar
        ret
animacion2 endp

animacion3 proc near
        call inicializar
        push ds

        cld
        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,219 * 32         ;1er hombre
        mov di,218 * 32         ;2do hombre
        mov cx,32 * 8
rep     movsb
        mov di,226 * 32
        mov si,218 * 32
        mov cx,32
rep     movsb
        pop ds
        call restaurar
        ret
animacion3 endp

animacion4 proc near
        call inicializar
        push ds

        cld
        mov ax,0a000h
        mov ds,ax
        mov cx,32
        mov si,230 * 32         ;fondo del big scroll
ani4loop:
        mov byte ptr [si],255
        inc si
        loop ani4loop
        pop ds
        call restaurar
        ret
animacion4 endp

animacion5 proc near
        call inicializar
        push ds

        cld
        mov ax,0a000h
        mov ds,ax
        mov cx,32
        mov si,230 * 32         ;fondo del big scroll
ani5loop:
        mov byte ptr [si],0
        inc si
        loop ani5loop
        pop ds
        call restaurar
        ret
animacion5 endp

;*************** bailarin ************;
sbailarin proc near
    push es
    mov cx,2 * 160
    mov ax,0b800h
    mov es,ax
    mov di,28 * 160             ;es:di
    mov si,offset garcha        ;ds:si
rep movsb
    pop es
    ret
sbailarin endp

;************** showtexto ***********;
showtexto proc near
    push es
    mov al,cartel           ;cartel leido
    mov ah,80
    mul ah
    mov si,offset textos    ;
    add si,ax               ;ds:si source
    mov al,[si]
    or al,al
    jne noresettexto

    mov cartel,0
    mov si,offset textos

noresettexto:
    mov ax,0b800h
    mov es,ax
    mov di,31 * 160
textoloop:
    mov al,[si]
    mov ah,7
    mov es:[di],ax
    inc si
    add di,2
    inc cx
    cmp cx,80
    jne textoloop
    pop es
    inc cartel
    ret
showtexto endp

;************** cleantexto ***********;
cleantexto proc near
    push es
    mov ax,0b800h
    mov es,ax
    mov di,31 * 160
textoloop2:
    mov ax,0720h
    mov es:[di],ax
    add di,2
    inc cx
    cmp cx,80
    jne textoloop2
    pop es
    ret
cleantexto endp


;*******************************;
; DAC							;
;*******************************;
dac proc near
    mov bx,dacpunt
    mov si,offset colores
    mov ah,[si+bx]
    cmp ah,255
    jne dacqui2

    xor bx,bx
    mov dacpunt,bx
dacqui2:
    mov al,63
	mov dx,3c8h
	out dx,al
    inc dx
    mov cx,3
dacloop:
    mov al,[si+bx]
    out dx,al
    inc bx
    loop dacloop
    mov dacpunt,bx
    ret
dac endp

;******* dac2 ******;
dac2 proc near
    mov bx,dac2punt
    mov si,offset colores
    mov ah,[si+bx]
    cmp ah,255
    jne dacqui

    xor bx,bx
    mov dac2punt,bx

dacqui:
    mov al,1
	mov dx,3c8h
	out dx,al

    inc dx
    mov cx,3
dac2loop:
    mov al,[si+bx]
    out dx,al
    inc bx
    loop dac2loop
    add bx,3*5
    mov al,[si+bx]
    cmp al,255
    jne dac2qui2
    xor bx,bx
dac2qui2:
    mov cx,3
dac2loop2:
    mov al,[si+bx]
    out dx,al
    inc bx
    loop dac2loop2

    mov dac2punt,bx
    ret
dac2 endp
;*******************************;
;		inicializar 			;
;*******************************;
;use bitplane 2 , seg at a000h	;
inicializar proc near
		push cs
		pop ds
		cli 					;clear interrup flag
		mov dx,3c4h 			; address of Sequencer Controller
		mov al,2				; 2nd register
		out dx,al
		inc dx					; data of Sequencer Controller
		in al,dx				;
		mov srmap,al			; restaura valor origianal
		mov al,04h				; select bitplane 2
		out dx,al				;
		mov dx,3c4h 			; Sequencer Controller
		mov al,4				; 4th register
		out dx,al
		inc dx
		in al,dx				;
		mov srmem,al			;restaura valor original
		mov al,7
		out dx,al				;256k & linear processing.
		mov dx,3ceh 			;address of Graphic Controller
		mov al,4				;Select
		out dx,al				;Read Map Register
		inc dx
		in al,dx
		mov grmap,al			;restaura valor original
		mov al,02h				;Bitplane 2
		out dx,al
		mov dx,3ceh 			;Graphic Controller
		mov al,5				;Select
		out dx,al				;Graphics mode Register
		inc dx
		in al,dx
		mov grmod,al			;restaura valor original
		mov al,0
		out dx,al				;Write mode 0,linear adresses,16 colors
		mov dx,3ceh 			;Graphic controller
		mov al,6				;Select
		out dx,al				;Miscellaneous register
		inc dx
		in al,dx
		mov grmis,al			;restaura valor original
		mov al,04h
		out dx,al				;Text,linear addresses,64k at A000h
		sti 					;set interrupt flag
		ret
inicializar endp

;******************************;
;		restaurar			   ;
;******************************;
restaurar proc near
		push cs
		pop ds
		cli
		mov dx,3c4h 			; address of Sequencer Controller
		mov al,2				; 2nd register
		out dx,al
		inc dx					; data of Sequencer Controller
		mov al,srmap			;
		out dx,al				; restaura
		mov dx,3c4h
		mov al,4				; 4th register
		out dx,al
		inc dx
		mov al,srmem			;
		out dx,al				;restaura
		mov dx,3ceh 			;address of Graphic Controller
		mov al,4				;Select
		out dx,al				;Read Map Register
		inc dx
		mov al,grmap
		out dx,al				;restaura
		mov dx,3ceh
		mov al,5				;Select
		out dx,al				;Graphics mode Register
		inc dx
		mov al,grmod
		out dx,al				;restaura
		mov dx,3ceh
		mov al,6				;Select
		out dx,al				;Miscellaneous register
		inc dx
		mov al,grmis
		out dx,al				;restaura
		sti
		ret
restaurar endp

;;;;;;;;;;;;;;;;;;;;; bit 8;;;;;;;;;;;;;;;
bit8 proc
	mov dx,3cch
	in al,dx
	and al,11110011b
	mov dx,3c2h
	out dx,al
	cli
	mov dx,3c4h
	mov ax,100h
	out dx,ax
	mov ax,101h
	out dx,ax
	mov ax,300h
	out dx,ax
	sti
	mov ax,1000h
	mov bl,13h
	mov bh,0
	int 10h
ret
bit8 endp
bit9 proc
	mov dx,3cch
	in al,dx
	and al,11110011b
	or al,00000100b
	mov dx,3c2h
	out dx,al
	cli
	mov dx,3c4h
	mov ax,100h
	out dx,ax
	mov ax,001h
	out dx,ax
	mov ax,300h
	out dx,ax
	sti
	mov ax,1000h
	mov bl,13h
	mov bh,8
	int 10h
	ret
bit9 endp


;********* newcolors *************;
newcolor proc near
    mov dx,3c8h
    mov al,1
    out dx,al
    inc dx
    mov al,0
    out dx,al
    mov al,42
    out dx,al
    mov al,42
    out dx,al
    mov al,0
    out dx,al
    mov al,42
    out dx,al
    mov al,22
    out dx,al
    ret
newcolor endp


;****************************************;
;*************	  DATA	  ***************;
;****************************************;
srmap	db ?			;Sequencer Register, Read Map
srmem	db ?			;				   , Memory Mode
grmap	db ?			;Graphics Register, Read Map
grmod	db ?			;				  , Graph Mode
grmis	db ?			;				  , Miscellaneous


cartel      db 0        ;de la funcion showtexto (que cartel hay que mostrar)

esperatexto db 0        ;esperas para mostrar el texto de abajo
texto1fl    dw 0        ;-duracion en pantalla

dacpunt dw 3 * 30       ;datos para el dac del big scroll
dacfl   db 0

dacfl2     db 0         ;datos para los bailarines
dac2punt dw 0

bailarin dw 0           ;espera de aparicion

fondo1  db 0            ;efectos del fondo prendido (big scroll)
fondo2  db 0            ;
fondo3  dw 0            ;
fondo4  db 0            ;

anim    db 0            ;la del scroll grande
anim2   db 0            ;la de los bailarines
anim3   db 0            ;la de crisis
letraend db 0
indexletra2 db 49       ;big scroll de abajo
indexletra db 0         ;big scroll de arriba
scroll	db 0			;valor del scroll
puntero dw 0            ;puntero del scroll cuando lee las letras

pantalla equ this byte
db'             ����������� ����������� ��� ���������   ��� ���������              '
db'             �         � �         �  �  �            �  �                      '
db'             �           �����������  �  �����������  �  �����������            '
db'             �         � �   �        �            �  �            �            '
db'             ����������� �   ������� ��� ����������� ��� �����������            '

texto db '                                .                            .....................................................              '
db'EEEEEEEEEIIIIIIIIIII !!!!!!!!! viejita...   Aca estamos todos reunidos... El cosmos '
db'esta entre nosotros...   los placeres existen y los vicios tambi�n... Ojo... no te confundas '
db' ...no te equivoques porque los saludos tambi�n van para vos... aca todo vale... '
db'el viento sopla, sube la marea, llueve y anochece... pero jam�s olvides esto: " El ni�o del sol esta con nosotros una vez m�s. " '
db'Recuerda que es un mandamiento muy importante... Para todos los amigos de mi BBS preferido, para ellos van mis greetings... '
db'Esta es mi primer fucking intro para la PC... sin sonido... sin nada... wanna do some music? '
db'Contact me at Crisis BBS...  Historia... otra vez no, please... Mis Mega saludos van para: HighToro,'
db'Dr.Victor,Edith,Javi,Alessia,Gustavo,Nacho,John,Corte,vos,yo,�l,tu amigo,ahhhhhhhh I dont want to continue with this'
db'... Ahhhhhh!      quiero, dame, tomo, agarro (esos son verbos).  Haz lo que tu mente ordene... sigue sus pasos...'
db'  � y ? � qu� estas esperando ? Apreta ESC para terminar...   Esto lo hice entre el 27 y 30 de octubre de 1994... '
db' Quiero a mi commodore, a mi PC y a mi novia... :-)  ',0

textos label byte
db'                                Intro 01 v1.00                                  '
db'                           hecha por El ni�o del Sol                            '
db'                     entre el 27 y 30 de octubre de 1994                        '
db'                           Herramientas utilizadas:                             '
db'        Q Edit 3.0, VChar NI 3.20, Turbo Assembler 3.1, Turbo Linker 5.1        '
db'                              Dont wait for music                               '
db'                          This is my first intro in PC                          '
db'                                   I like it                                    '
db'                               Press ESC to exit                                '
db'                                Nice fonts, :-)                                 '
db'              � A alguien le recuerda a algo el pibito que corre ?              '
db'                      Todas las ma�anas me tomo mi Nesquik                      '
db'                            ...tengo que estudiar...                            '
db'                             pero soy un cacho bago                             '
db'                             No more messages here.                             '
db 0,0,0

vga_msg db'Requiere placa VGA para correr.',13,10,'$'
no286msg db'Requiere una 286 o compatible para correr.',13,10,'$'

include font0000.asm
include ffff0000.asm
include sysop.asm
include garcha.asm
include colores.asm

final   equ this byte
ruti	endp
code	ends
		end ruti
