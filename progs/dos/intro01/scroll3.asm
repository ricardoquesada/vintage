;name: demo
;verion: 1.00
;start  date: 18/7/94
;finish date:
;author: Ricardo Quesada

P286                            ;procesadores 286 para arriba

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;****************************************;
;************* START ********************;
;****************************************;

start:

;******* MANAGE MEMORY **********
        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;*********** inicializar !
        call isvga
        cmp al,1ah              ;compara si es placa VGA
        je vgaonly

        mov ah,9
        mov dx,offset vga_msg
        int 21h

        jmp fincito             ;terminar si no es VGA

vgaonly:
        call clrscr             ;borra pantalla
        call bit8

        mov bp,offset TABLA2
        mov bh,08h           ;cantidad de bytes x char
        mov cx,100h          ;total de chars (256)
        xor dx,dx            ;a partir de que char (0).
        xor bl,bl            ;map 2.Grabar en 1er tabla
        mov ax,1110h         ;Define los carecteres creado por el usuario.
        int 10h
        cli
        mov al,0ffh
        out 21,al
;*********** scroll ************;

scro:
        in  al,060h
        cmp al,01h
        je  escape

waiting_:
        mov dx,03dah
        in  al,dx
        and al,09h
        or  al,al
        je  waiting_

        cmp al,01h
        je change_color
        cmp al,09h
        je retrace_wait_on

        jmp scro

retrace_wait_on:
        in al,dx
        and al,1
        jne retrace_wait_on
        jmp scro

change_color:
        inc horiz
        cmp horiz,50
        je above
        cmp horiz,10
        je below
        jmp scro

below:
        mov dx,3c0h             ;attribute controller
        mov al,33h              ;13h or 20h   bit 5 on IMPORTANT!
        out dx,al               ;
        mov al,7                ;nueva poscicion!
        out dx,al               ;de nuevo fierita... Je Je Je...
        jmp scro

above:
        mov ah,scroll
        inc ah
        cmp ah,9
        jne aaaa
        xor ah,ah
aaaa:
        mov scroll,ah
        mov dx,3c0h             ;attribute controller
        mov al,33h              ;13h or 20h   bit 5 on IMPORTANT!
        out dx,al               ;
        mov al,scroll           ;nueva poscicion!
        out dx,al               ;de nuevo fierita... Je Je Je...
        mov horiz,0
        jmp scro

escape:
        sti
        mov al,000h
        out 21,al
        mov ax,3
        int 010h
        mov ax,1000h
        mov bx,0813h
        int 10h                 ;posciciona a poscicion original
        call bit9
fincito:
        mov ax,4c00h
        int 21h                 ;fin del programa


;***********************;
;       clrscr          ;
;***********************;
clrscr proc near
        pusha
        mov ah,7
        mov al,0
        mov bh,07h
        mov ch,0
        mov cl,0
        mov dh,50
        mov dl,80
        int 10h                 ;scroll window
        mov ax,500h
        int 10h                 ;Select active display page
        mov al,3                ;es el numero 3
        xor ah,ah
        int 10h                 ;no entonces lo pone
        mov ah,2
        mov bh,0
        mov dx,0ffffh
        int 10h                 ;set cursor position

        mov ax,0b800h
        mov ds,ax
        xor di,di
        mov al,65
lupeando:
        mov [di],al
        inc di
        inc di
        cmp di,8000
        jbe lupeando

        popa
        ret
clrscr  endp


;***********************;
;       isvga           ;
;***********************;
isvga proc near
        mov ax,1a00h
        int 10h
        ret
isvga  endp

;*******************************;
;                               ;
;         animacion             ;
;                               ;
;*******************************;
animacion proc near
        call inicializar        ;permite acceso al segmento A000
        mov ax,0a000h           ;segmento a000h
        mov ds,ax
        mov es,ax

        cld
        mov si,176 * 32         ;1er hombre
        mov di,175 * 32         ;2do hombre
        mov cx,32 * 6
        rep movsb

        mov si,175 * 32
        mov di,181 * 32
        mov cx,32
        rep movsb

        call restaurar
        push cs
        pop ds
        ret
animacion endp

;*******************************;
; DAC                           ;
;*******************************;
dac proc near
    mov al,63
    mov dx,3c7h
    out dx,al
    add dx,2

    in al,dx
    mov cl,al
    in al,dx
    mov ch,al
    in al,dx
    mov bl,al

    cmp cs:dacdir,0
    je disdir

    cmp ch,60
    jae chgdir
    inc ch
    inc bl
    jmp short writedac

disdir:
    or ch,ch
    je chgdir
    dec ch
    dec bl

writedac:
    mov al,63
    mov dx,3c8h
    out dx,al
    inc dx
    mov al,cl
    out dx,al
    mov al,ch
    out dx,al
    mov al,bl
    out dx,al
    ret
chgdir:
    xor cs:dacdir,1
    ret
dac endp
;*******************************;
;       inicializar             ;
;*******************************;
;use bitplane 2 , seg at a000h  ;
inicializar proc near
        push cs
        pop ds
        cli                     ;clear interrup flag
        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al
        inc dx                  ; data of Sequencer Controller
        in al,dx                ;
        mov srmap,al            ; restaura valor origianal
        mov al,04h              ; select bitplane 2
        out dx,al               ;
        mov dx,3c4h             ; Sequencer Controller
        mov al,4                ; 4th register
        out dx,al
        inc dx
        in al,dx                ;
        mov srmem,al            ;restaura valor original
        mov al,7
        out dx,al               ;256k & linear processing.
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register
        inc dx
        in al,dx
        mov grmap,al            ;restaura valor original
        mov al,02h              ;Bitplane 2
        out dx,al
        mov dx,3ceh             ;Graphic Controller
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register
        inc dx
        in al,dx
        mov grmod,al            ;restaura valor original
        mov al,0
        out dx,al               ;Write mode 0,linear adresses,16 colors
        mov dx,3ceh             ;Graphic controller
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register
        inc dx
        in al,dx
        mov grmis,al            ;restaura valor original
        mov al,04h
        out dx,al               ;Text,linear addresses,64k at A000h
        sti                     ;set interrupt flag
        ret
inicializar endp

;******************************;
;       restaurar              ;
;******************************;
restaurar proc near
        push cs
        pop ds
        cli
        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al
        inc dx                  ; data of Sequencer Controller
        mov al,srmap            ;
        out dx,al               ; restaura
        mov dx,3c4h
        mov al,4                ; 4th register
        out dx,al
        inc dx
        mov al,srmem            ;
        out dx,al               ;restaura
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register
        inc dx
        mov al,grmap
        out dx,al               ;restaura
        mov dx,3ceh
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register
        inc dx
        mov al,grmod
        out dx,al               ;restaura
        mov dx,3ceh
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register
        inc dx
        mov al,grmis
        out dx,al               ;restaura
        sti
        ret
restaurar endp

;;;;;;;;;;;;;;;;;;;;; bit 8;;;;;;;;;;;;;;;
bit8 proc
    mov dx,3cch
    in al,dx
    and al,11110011b
    mov dx,3c2h
    out dx,al
    cli
    mov dx,3c4h
    mov ax,100h
    out dx,ax
    mov ax,101h
    out dx,ax
    mov ax,300h
    out dx,ax
    sti
    mov ax,1000h
    mov bl,13h
    mov bh,0
    int 10h
ret
bit8 endp
bit9 proc
    mov dx,3cch
    in al,dx
    and al,11110011b
    or al,00000100b
    mov dx,3c2h
    out dx,al
    cli
    mov dx,3c4h
    mov ax,100h
    out dx,ax
    mov ax,001h
    out dx,ax
    mov ax,300h
    out dx,ax
    sti
    mov ax,1000h
    mov bl,13h
    mov bh,8
    int 10h
    ret
bit9 endp



;****************************************;
;*************    DATA    ***************;
;****************************************;
srmap   db ?            ;Sequencer Register, Read Map
srmem   db ?            ;                  , Memory Mode
grmap   db ?            ;Graphics Register, Read Map
grmod   db ?            ;                 , Graph Mode
grmis   db ?            ;                 , Miscellaneous

horiz   dw 0
dacdir  db 0
dacfl   db 0
vels    db 0
anim    db 0
letraend db 0
indexletra db 0
scancode dw 0           ;tecla pulsada Scan Code + Ascii
scroll  db 0            ;valor del scroll
puntero dw 0            ;puntero del scroll
texto db ' Me recuerdan, si soy yo , Erasoft pero esta vez en las PC trantando de hacer una fucking intro...'
db'Espero que salga algo potable... Ja Ja Ja... para empezar quiero hacer algo parcedio a la intro 10 que hice en la comodore 64'
db'Bueno, espero tener suerte... Don Riq.... Ricardo Quesada ... El ni�o del Sol... Erasoft 94....   ',0

vga_msg db'Sorry, VGA only!',13,10,'$'

include font0000.asm
include ffff0000.asm

final   equ this byte
ruti    endp
code    ends
        end ruti
