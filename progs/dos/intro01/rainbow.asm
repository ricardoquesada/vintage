.286c
assume cs:cseg,ds:cseg
cseg segment para public 'Rainbow'
org 100h
mproc proc near

                                cli
                                mov ax,3
                                int 10h
                                mov ax,01300h
                                mov bx,00001h
                                mov cx,23*80
                                mov dx,0000h
                                mov bp,offset tester
                                int 10h

                                mov si,offset color
                                mov al,0ffh
                                out 21,al
waiting_for_retrace:            in  al,060h
                                cmp al,01h
                                je  escape

waiting_:                       mov dx,03dah
                                in  al,dx

                                and al,09h

                                or  al,al
                                je  waiting_

                                cmp al,01h
                                je  change_color

                                cmp al,09h
                                je retrace_wait_on

                                jmp waiting_for_retrace

retrace_wait_on:                in  al,dx
                                and al,1
                                jne retrace_wait_on
                                mov si,offset color
                                jmp waiting_for_retrace

change_color:                   mov dx,03c8h
                                mov al,color_offset
                                out dx,al
                                inc dx

                                outsb
                                outsb
                                outsb

                                jmp waiting_for_retrace

escape:                         sti
                                mov al,000h
                                out 21,al
                                mov ax,3
                                int 010h
                                mov ax,4c00h
                                int 21h

color_offset                    equ 1

tester db '��������������������������������������������������������������������������������'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�             ������� ������� ������� ������� ���� ������� �������             �'
db '�               ���   ������  ������    ���    ��  ��   �� ��  ���             �'
db '�               ���   ������   ������   ���    ��  ��   �� ��   ��             �'
db '�               ���   ������� �������   ���   ���� ��   �� �������             �'
db '�                                                                              �'
db '�             ������� ������� ���� ������� ������� ������� �� �� ��            �'
db '�             ��   �� �������  ��  ��   �� ������� ��   �� �� �� ��            �'
db '�             ������  �������  ��  ��   �� ������� ��   �� �� �� ��            �'
db '�             ��  ��� ��   �� ���� ��   �� ������� ������� ��������            �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '�                                                                              �'
db '��������������������������������������������������������������������������������'


include                         color.inc

mproc endp
cseg ends
end mproc
