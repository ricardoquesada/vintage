Comment|
File:    PCX2SPR
Version: 1.10
Author:  Ricardo QUESADA
Date:    01/03/95
Purpose: Converts .PCX 2 .SPR graphics.
|

P286

FILETIME  = 1 * 2048 + 10 * 32
FILEDATE  =15 * 512 + 03 * 32 + 01

CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE
start:
        JMP INit
db'PCX2SPR v1.10 '
cabeza db'(c) Ricardo Quesada 1995.',13,10
xorvalue db 0
;== DATOS   ====================;
intro_msg DB 13,10,'PCX2SPR v1.10 (c) Ricardo Quesada 1995.',13,10
db "Freeware version. Please don't register.",13,10,13,10,'$'

reg_not_ok equ this byte
db 'ERROR',13,10,'Check that your disk is not write protected.',13,10,'$'
prean_msg label byte
db 13,10,'Trying to remove any virus or compresion...$'
antiv_msg equ this byte
db'OK',13,10,13,10,'ADVICE: Reboot the computer right now.',13,10
                 db'        The virus must be resident.',13,10,'$'

help_msg label byte
db 'PCX2SPR [options] filespec',13,10
db '      /c0          - Convert, NO',13,10
db '        1        * - Convert, YES',13,10
db '      /v0        * - View PCX ,NO',13,10
db '        1          - View PCX, YES',13,10
db '      /b0          - Binary, NO  (.ASM)',13,10
db '        1        * - Binary, YES (.SPR)',13,10
db '      /p0          - Palete, NO',13,10
db '        1        * - Palete, YES',13,10
db '      /a           - Removes any virus or compresion.',13,10
db "                     (Built-in Ricardo Quesada's antivirus).",13,10
db '      filespec     - PCX files to be converted.',13,10
db 13,10,'Options with * are factory defaults.',13,10
db 'Ex: pcx2spr /v1 /c0 *.pcx',13,10
db 'For more information, please read PCX2SPR.DOC',13,10,'$'

;               12345678901234567890
crlf        db 13,10,'$'
vererr_msg  db 'Not a PCX image.$'
encerr_msg  db '"Encode" not sup.$'
morexxy_msg db 'X,Y < 1000 for conv.$'
no256c_msg  db 'Only 256 color PCX.$'
nomem_msg   db 'Not enough memory.$'
nameerr_msg db 'File not found.$'
reading_msg db 'Reading...$'
neading_msg db '          $'
convert_msg db 20 dup(0)
ok_msg      db 'OK.',13,10,'$'
nook_msg    db 'ERROR.',13,10,'Please check out.',13,10,'$'

valor1  db'  0 x '
valor2  db'  0$'

nombrein    db 0                ; Flag = 1, nombre entrado

nombre1 db 50 dup(0)            ;reservo 40 por si las moscas
handle1 dw 0                    ;Su handle
segme   dw ?                    ;segemento de memoria para los archivos.
vidsegme    dw ?                       ; Segmento para guardar contenido de pantalla
cursorpos   dw ?                       ; " Poscicion del cursor
handbak dw 0                    ; handle del archivo .SPR
handbak2    dw 0                       ; handle del archivo .PAL ( new on version 1.10 )
sprext  db '.SPR',0
asmext  db '.ASM',0
palext  db '.PAL',0                    ; new on version 1.10
incext  db '.INC',0                    ; new on version 1.10
asmdat  db '   '
asmcom  db ','
asmini  db 'db '
asmcnt  db 0                    ; No debe exceder los 80
asmlen  db 'db   0,  0  ; Lenght in X,Y.',13,10
asmwrilen  equ $ - asmlen
segmeasm    dw ?                ; segmento cuando se genera el .asm

incini db"db Don,Riq,ERA; DAC '95.",13,10
inclen equ $ - incini
dacnum db 0                      ; valor actual de DAC convirtiendose a .INC

pcxh    label byte
manu    db 10               ; Manufacturer = 10
ver     db ?                ; Version   (0,2,3,4,5)
enc     db 1                ; Encoding = 1
bpp     db ?                ; Bitsperpixel
wind    dw 0,0,0,0          ; Xmin,Ymin,Xmax,Ymax
hdpi    dw ?                ; Horiz resol
vdpi    dw ?                ; Vertic resol
cmap    db 48 dup(?)        ; Palette
        db 0                ; Reserved . Should be set to 0
npla    db 1                ; Number of Color Planes
bypp    dw ?                ; Bytes per plane
palinfo dw ?                ; Pallete info (1=Color/BW,2=Grayscale)
hsize   dw ?                ; horizontal screen size in pixels.
vsize   dw ?                ; vertical "
        db 54 dup(0)        ; to fill 128 chars.

truelenght db 0,0           ;
realx   dw ?                ; lo que ocupa de en serio
realy   dw ?
unx     dw ?                ; Usados en ConvertPCX para show graphic
uny     dw ?
addx    dw ?

viewflag    db 0            ; 0 = off
convflag    db 1            ; 1 = on
binaflag    db 1            ; 1 = on
pallflag    db 1            ; 1 =on    ; new on version 1.10

daceado     db 0                       ; Flag para ver si ya de dividio por 4 el DAC

dta_table equ this byte
            db 21 dup(?)
dta_atrib   db ?            ;atributo
dta_time    dw ?            ;time
dta_date    dw ?            ;date
dta_size    dd ?            ;size
dta_name    db 13 dup(?)    ;name

oneline     db 1000 dup(?)


cabecera label byte
db'Filename         PCX Size      Converted ?             Posibles Errors   ',13,10
db'-------------------------------------------------------------------------',13,10,'$'


;== PROG =======================;
PROG:
        PUSH CS
        POP DS
        MOV DX,OFFSET INTRO_MSG
        MOV AH,9
        INT 21H

        mov nombrein,0          ; Reset Flag

        cld                     ;command line detection
        mov si,81h
alop:
        lodsb
        cmp al,13               ;es CR,si then display help
        je displayhelp
        cmp al,20h
        je alop

        cmp al,'-'
        je opcion2
        cmp al,'/'
        jne newname
opcion2:
        lodsb
        and al,11011111b
        cmp al,'A'
        je jmpanti
        cmp al,'V'
        je view
        cmp al,'C'
        je noconv
        cmp al,'B'
        je nobin
        cmp al,'P'
        je nopal
        jne displayhelp             ;acepta opciones.

jmpanti:
        jmp antivirus               ;
view:
        lodsb
        and al,1
        mov viewflag,al
        jmp alop

noconv:
        lodsb
        and al,1
        mov convflag,al
        jmp alop

nobin:
        lodsb
        and al,1
        mov binaflag,al
        jmp alop

nopal:
        lodsb
        and al,1
        mov pallflag,al
        jmp alop

displayhelp:                    ;***************** no command line
        cmp nombrein,0
        je disphelp
        jmp driveuser
disphelp:
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***
        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***


;----------- Get Name --------------;
newname:                        ;Get open name
        mov nombrein,1
        mov bx,offset nombre1
        mov cx,25
nwnloop1:
        mov [bx],2400h
        add bx,2
loop    nwnloop1                ; Borrar por si abajo quedaba nombre viejo

        cld
        mov bx,offset nombre1
buscanombre:
        cmp al,13
        je endname0
        cmp al,20h              ;Go for another option
        je endname1

        mov [bx],al
        lodsb
        inc bx
        jmp short buscanombre

endname1:
        jmp alop                ; Check another options
endname0:
        mov [bx],2400h          ;24h=$ 0=nul.Uso el $ para usar la func 9 int 21


driveuser:
;---------------------------------------;
;---------- Alloc some memory ----------;
;---------------------------------------;
        mov ah,48h
        mov bx,4000 / 16 + 1
        int 21h
        mov vidsegme,ax                ; Allocated memory segment

;---------------------------------------;
;---------- Abrir el archivo -----------;
;---------------------------------------;
        mov dx,offset cabecera  ;loading messagge
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ah,1ah
        mov dx,offset dta_table
        int 21h                 ; Change DTA.

        mov ah,4eh
        mov cx,63h              ; All attributes
        mov dx,offset nombre1
        int 21h                 ; Find First Matching File (Para saber el largo)

nextfilehere:
        mov si,offset dta_name
        call showit

        call openpcx
        jc   nextfile
        call isitpcx            ; Is it PCX
        jc   nextfile           ;
        call tamanook
        jc   nextfile
        call is256col
        jc   nextfile           ;
        call ReadData
        jc   nextfile
        call convertpcx

nextfile:
        mov ah,9
        mov dx,offset crlf
        int 21h                 ; Next Line
        mov ah,4fh
        int 21h                 ; Find next matching file
        jnc nextfilehere

        mov ah,49h
        mov bx,cs:vidsegme
        mov es,bx
        int 21h                         ; Free allocated memory

        mov ax,4c00h
        int 21h                         ; Go To DOS.



;===============================;
;       openPCX                 ;
;===============================;
openpcx proc
        pusha
        mov ax,3d02h            ;open file with al=2 (W/R access)
        mov dx,offset dta_name
        int 21h
        jnc no_error

        mov dl,55
        call newcolumn
        mov ah,9
        mov dx,offset nameerr_msg
        int 21h
        stc
        popa
        ret

no_error:
        mov handle1,ax
        clc
        popa
        ret
openpcx endp

;===============================;
;       isitPCX                 ;
;===============================;
isitpcx proc
        pusha

        mov bx,handle1          ;handle
        mov cx,128              ;128 bytes
        mov ah,3fh              ;read
        mov dx,offset pcxh      ;DS:DX  -> buffer
        int 21h

        cmp manu,10
        jne versionerror
        cmp enc,1
        jne encodeerror
        jmp pic_ok

encodeerror:
        mov dl,55
        call newcolumn
        mov dx,offset encerr_msg
        mov ah,9
        int 21h                 ;*** output error message ***
        jmp notpcx
versionerror:
        mov dl,55
        call newcolumn
        mov dx,offset vererr_msg
        mov ah,9
        int 21h                 ;*** output error message ***
notpcx:
        mov bx,handle1
        mov ah,3eh
        int 21h
        stc
        popa
        ret                     ; Return with Carry Set -> Error
pic_ok:
        clc                     ; Return with Carry Clear -> Ok
        popa
        ret
isitpcx endp

;===============================;
;       tamanook                ;
;===============================;
tamanook proc
        pusha
        mov bx,offset wind              ; Xmin,Ymin,Xman,Ymax
        mov ax,[bx+4]
        sub ax,[bx]                     ; Xtotal
        inc ax
        xor dx,dx
        mov realx,ax
        mov si,offset valor1
        call tochar
        mov cx,[bx+6]
        sub cx,[bx+2]                   ; Ytotal
        mov ax,cx
        inc ax
        mov realy,ax
        xor dx,dx
        mov si,offset valor2
        call tochar
        mov dl,16
        call newcolumn
        mov dx,offset valor1
        mov ah,9
        int 21h                         ; show valores

        cmp realy,999
        ja noconvert
        cmp realx,999
        jbe conconvert

noconvert:
        mov dl,55
        call newcolumn
        mov ah,9
        mov dx,offset morexxy_msg
        int 21h
        mov bx,handle1
        mov ah,3eh
        int 21h                         ; Close file handle
        stc
        popa
        ret

conconvert:
        clc
        popa
        ret
tamanook endp


;===============================;
;       is256col                ;
;===============================;
is256col proc
        pusha
        mov bx,offset npla              ; Xmin,Ymin,Xman,Ymax
        mov al,[ bx ]
        cmp al,1
        je is256

no256:
        mov dl,55
        call newcolumn
        mov ah,9
        mov dx,offset no256c_msg
        int 21h
        mov bx,handle1
        mov ah,3eh
        int 21h                         ; Close file handle
        stc
        popa
        ret

is256:
        clc
        popa
        ret
is256col endp


;===============================;
;       ReadData                ;
;===============================;
ReadData proc
        pusha
        push ds

        mov dl,31
        call newcolumn
        mov ah,9
        mov dx,offset reading_msg
        int 21h

        mov ah,48h
        mov bx,word ptr dta_size
        shr bx,4                        ; Divide por 16
        inc bx                          ; le suma 1
        int 21h
        mov segme,ax                    ; Allocated memory segment
        jnc readdata1

        mov bx,handle1
        mov ah,3eh
        int 21h                         ;Close file handle

        mov dl,55
        call newcolumn
        mov ah,9
        mov dx,offset nomem_msg
        int 21h

        stc
        pop ds
        popa
        ret

readdata1:
        mov ds,ax
        xor dx,dx                       ; DS:DX -> Buffer
        mov ah,3fh
        mov bx,cs:handle1
        mov cx,word ptr cs:dta_size     ; Datos a leer.
        sub cx,128                      ; la cabecera no ( ya esta leida )
        int 21h

        pop ds
        popa
        clc
        ret
readdata endp

;===============================;
;       convertpcx              ;
;===============================;
convertpcx proc
        pusha
        push ds
        push es

        push cs
        pop ds
        cmp convflag,0
        je notconve
        call nombrebak
        call Trueconv                   ; Convert PCX to SPR
       call savedac                    ; Convert PCX Pal to Pal ( new on version 1.10 )
                                       ; Hace todo el trabajo.
notconve:
        cmp viewflag,0
        je notview
        call viewpcx                    ; Show PCX
notview:
        mov ah,49h
        mov bx,segme
        mov es,bx
        int 21h                         ; Free allocated memory
        mov bx,handle1
        mov ah,3eh
        int 21h                         ;Close file handle

        pop es
        pop ds
        popa
        ret
convertpcx endp


;===============================;
;          viewpcx              ;
;===============================;
viewpcx proc
        push cs
        pop ds

        call savevid                   ; new on version 1.10

        mov ax,13h
        int 10h                         ; Set Graphics mode

        cmp [ver],5
        jne nopall

        call changedac
nopall:
        mov cx,realy
        mov uny,cx
        mov cx,realx                    ; Number of times

        mov ax,320
        sub ax,cx
        mov addx,ax                     ; El aditivo para sumar

        mov ax,0a000h
        mov es,ax
        xor di,di                       ; ES:DI
        mov ax,segme
        mov ds,ax
        xor si,si                       ; DS:SI

mainunpacking:
        mov al,ds:[ si ]
        mov ah,al                       ; 2 hi bits.
        and ah,192
        cmp ah,192
        je  coded                       ;
        mov es:[ di ],al                ; Al= Tenia el valor original todavia

        inc di
        dec cx
        jmp endofline

coded:
        mov ah,al                       ;
        and ah,63                       ; Los otros valores (ah=number of times)
        inc si
        mov al,ds:[ si ]                ; Al= DATA
codedhere:
        mov es:[ di ],al
        inc di
        dec cx
        dec ah
        or cx,cx
        je endofline
        or ah,ah
        jne codedhere
endofline:
        inc si                          ; para que apunte al siguiente
        or cx,cx
        jne mainunpacking

        mov cx,cs:realx
        add di,cs:addx
        dec cs:uny
        cmp cs:uny,0
        jne mainunpacking

        mov ah,8
        int 21h                         ; get key
        mov ax,3h
        int 10h

       call restorevid                 ; new on version 1.10

        push cs
        pop  ds
        ret
viewpcx endp

;===============================;      ; new on version 1.10
;       SaveDAc                 ;
;===============================;
savedac proc
        push ds
        push es

       push cs
       pop ds

        mov cx,word ptr dta_size
        sub cx,128                  ; File Header
        sub cx,769                  ; Debo encontrar un 12
        mov ax,segme
        mov ds,ax
        mov si,cx
        cmp byte ptr [ si ],12
        jne nodac2

        inc si

        push si
        cld
        mov cx,768
dac4div2:
        mov al,[si]
        shr al,2                       ; Divide por 4
        mov [si],al
        inc si
        loop dac4div2
        mov cs:daceado,1               ; Daceado ok!
        pop si

       cmp cs:pallflag,0
       je nodac2                       ; no .PAL info
       call nombrebak2                 ; Crear nombre (.PAL o .INC )
       call savepall                   ; Save Pallete info!

nodac2:
        pop es
        pop ds
        ret
savedac endp

;===============================;      ; new on version 1.10
;       SAvepall                ;
;===============================;
savepall proc
       push ds
       push es

       push cs
       pop ds

        cmp binaflag,1
        je binhere1_2

       call geninc
       jmp closedacbak2

binhere1_2:
        mov ah,40h                      ; Write
        mov bx,handbak2
        mov cx,768                     ; 256 * 3
        mov dx,segme
        mov ds,dx                      ; DS:DX
        mov dx,si
        int 21h                         ; Write usign handle

closedacbak2:
        mov ax,cs
        mov ds,ax
        mov es,ax

        mov bx,handbak2
        mov ah,3eh
        int 21h                         ; Close handle

        mov di,offset convert_msg
        mov al,0
        mov cx,13
        cld
repne   scasb
        jne n_foundit

        dec di
n_foundit:
        cmp binaflag,0
        je inc_ext2
        mov si,offset palext
        jmp pal_ext2
inc_ext2:
        mov si,offset incext
pal_ext2:
        mov cx,5
repne   movsb

        mov dl,31
        call newcolumn
        mov si,offset convert_msg
        call showit

       pop es
       pop ds
       ret
savepall endp


;===============================;
;       ChangeDAc               ;
;===============================;
changedac proc
        push ds
        push es

        mov cx,word ptr dta_size
        sub cx,128                  ; File Header
        sub cx,769                  ; Debo encontrar un 12
        mov ax,segme
        mov ds,ax
        mov si,cx
        cmp byte ptr [ si ],12
        jne nodac

        inc si

        push si
        cmp cs:daceado,1
        je  nodacear

        cld
        mov cx,768
dac4div:
        mov al,[si]
        shr al,2
        mov [si],al
        inc si
        loop dac4div

nodacear:
        pop si
        xor al,al
        mov dx,3c8h
        out dx,al
        inc dx
        mov cx,768
repne   outsb

nodac:
        pop es
        pop ds
        ret
changedac endp

;===============================;
;       TrueConv                ;
;===============================;
trueconv proc
        push cs
        pop ds

        cmp binaflag,1
        je binhere1
        call writeasmlenght
        jmp nobinhere1
binhere1:
        mov ax,realx            ; Write the first 2 bytes ! ( lenght )
        mov bx,realy
        mov truelenght,al
        mov truelenght+1,bl
        mov ah,40h
        mov bx,handbak
        mov cx,2
        mov dx,offset truelenght
        int 21h                 ; Write the first 2 bytes ! (lenght)

nobinhere1:
        mov cx,realy
        mov uny,cx
        mov cx,realx                    ; Number of times

        mov ax,cs
        mov es,ax
        mov di,offset oneline           ; ES:DI
        mov ax,segme
        mov ds,ax
        xor si,si                       ; DS:SI

mainunpacking2:
        mov al,ds:[ si ]
        mov ah,al                       ; 2 hi bits.
        and ah,192
        cmp ah,192
        je  coded2                      ;
        mov es:[ di ],al                ; Al= Tenia el valor original todavia

        inc di
        dec cx
        jmp endofline2

coded2:
        mov ah,al                       ;
        and ah,63                       ; Los otros valores (ah=number of times)
        inc si
        mov al,ds:[ si ]                ; Al= DATA
codedhere2:
        mov es:[ di ],al
        inc di
        dec cx
        dec ah
        or cx,cx
        je endofline2
        or ah,ah
        jne codedhere2
endofline2:
        inc si                          ; para que apunte al siguiente
        or cx,cx
        jne mainunpacking2


        push ds
        push cs
        pop  ds
        cmp binaflag,1
        je  binhere2
        call genasm                     ; Generar archivo ASM
        jmp nobinhere2
binhere2:
        mov ah,40h                      ; Write
        mov bx,handbak
        mov cx,realx
        mov dx,offset oneline
        int 21h                         ; Write usign handle
nobinhere2:
        pop ds

        mov cx,cs:realx
        mov di,offset oneline
        dec cs:uny
        cmp cs:uny,0
        jne mainunpacking2

        mov ax,cs
        mov ds,ax
        mov es,ax

        mov bx,handbak
        mov ah,3eh
        int 21h                         ; Close handle

        mov di,offset convert_msg       ; Display Converted MSG
        mov si,offset dta_name
        cld
        mov cx,13
repne   movsb


        mov dl,31
        call newcolumn
        mov ah,9
        mov dx,offset neading_msg
        int 21h                        ; Primero nada

        mov dl,31
        call newcolumn
        mov si,offset convert_msg
        call showit

        ret
trueconv endp

;---------------------------;
;       WriteASMlenght      ;
;---------------------------;
writeasmlenght proc
        pusha
        mov si,offset asmlen + 3
        mov ax,realx
        call tochar
        mov si,offset asmlen + 7
        mov ax,realy
        call tochar
        mov cx,asmwrilen
        mov ah,40h
        mov bx,handbak
        mov dx,offset asmlen
        int 21h
        popa
        ret
writeasmlenght  endp

;---------------------------;
;       GenASM              ;
;---------------------------;
genasm proc
        pusha
        push es

        mov bx,realx    ; Multiplico por 4 (,000) - Divico por 16 (paragraph)
        shr bx,2        ; = Divido por 4
        inc bx
        mov ah,48h
        int 21h         ; Alloc this mem !
        jc nomemasm
        mov segmeasm,ax ;Save for later

        cld
        mov cx,3
        mov si,offset asmini    ; DS:SI
        mov ax,segmeasm
        mov es,ax
        xor di,di               ; ES:DI
repne   movsb

        mov di,offset oneline   ; CS:DI -> Source
        mov cx,realx            ;Cantidad de veces.
        mov ax,segmeasm
        mov ds,ax               ; DS:SI (Destination, al revez ! )
        mov si,3

untilasm:
        xor ah,ah
        mov al,cs:[ di ]
        call tochar
        add si,3
        mov byte ptr [ si ],','
        inc si
        inc di
loop    untilasm
        dec si
        mov [ si ],0a0dh

        mov ah,40h
        xor dx,dx                   ; Desde donde
        mov cx,cs:realx
        shl cx,2                    ; Multiplico * 4
        add cx,4                    ; + 4
        mov bx,cs:handbak
        int 21h

        push cs
        pop ds

        mov bx,segmeasm
        mov es,bx
        mov ah,49h
        int 21h         ; Free allocted memory (ASM)

nomemasm:
        pop es
        popa
        ret
genasm endp


;---------------------------;
;       GenINC              ;
;---------------------------;
geninc proc
        pusha
        push ds
        push es

       push cs
       pop ds

       mov ax,segme
       mov es,ax

       mov di,si                       ; DI = Source
       mov si,offset incini + 3        ; SI = Destination ( Al reves )

       cld
writeincloop:
       mov si,offset incini + 3        ; SI = Destination ( Al reves )
       mov cx,3                        ; 3 veces solo.
untilasm2:
        xor ah,ah
        mov al,es:[ di ]
        call tochar
        add si,4
        inc di
loop    untilasm2
        add si,5                       ; Ahora grabar que DAC es.
        xor ah,ah
        mov al,dacnum
        call tochar
        mov si,offset incini + inclen - 2
        mov [ si ],0a0dh

        mov ah,40h
        mov dx,offset incini           ; Desde donde
        mov cx,inclen
        mov bx,handbak2
        int 21h                        ; Graba "x" linea de DAC

        inc dacnum
        cmp dacnum,0
        jne writeincloop

        pop es
        pop ds
        popa
        ret
geninc endp


;===============================;
;       nombrebak               ;
;===============================;
nombrebak proc
        pusha
        push es
        push ds

        mov ax,cs
        mov ds,ax
        mov es,ax
        mov di,offset dta_name
        mov al,'.'
        mov cx,8
        cld
repne   scasb
        jne notfoundit

        dec di
notfoundit:
        cmp binaflag,0
        je asm_ext
        mov si,offset sprext
        jmp spr_ext
asm_ext:
        mov si,offset asmext
spr_ext:
        mov cx,5
repne   movsb

        mov ah,3ch
        xor cx,cx               ; File Attrib
        mov dx,offset dta_name
        int 21h                 ; Create File

        mov handbak,ax          ; File handle

        pop ds
        pop es
        popa
        ret
nombrebak endp


;===============================;      ; new on version 1.10
;       nombrebak2              ;
;===============================;
nombrebak2 proc
        pusha
        push es
        push ds
       push si                         ; new on version 1.10

        mov ax,cs
        mov ds,ax
        mov es,ax
        mov di,offset dta_name
        mov al,'.'
        mov cx,8
        cld
repne   scasb
        jne notfoundit2

        dec di
notfoundit2:
        cmp binaflag,0
        je inc_ext
        mov si,offset palext
        jmp pal_ext
inc_ext:
        mov si,offset incext
pal_ext:
        mov cx,5
repne   movsb

        mov ah,3ch
        xor cx,cx               ; File Attrib
        mov dx,offset dta_name
        int 21h                 ; Create File

        mov handbak2,ax          ; File handle

       pop  si                         ; new on version 1.10
        pop ds
        pop es
        popa
        ret
nombrebak2 endp

;===============================;
;       savevid                 ;
;===============================;
savevid proc
       pusha
       push ds
       push es

       push cs
       pop ds

       mov ah,3
       xor bh,bh
       int 10h
       mov cursorpos,dx

       mov ax,vidsegme
       mov es,ax
       xor di,di                       ; ES:DI
       mov ax,0b800h
       mov ds,ax
       xor si,si                       ; DS:SI
       cld
       mov cx,2000
repne  movsw                           ; move 4000 veces

       pop es
       pop ds
       popa
       ret
savevid endp


;===============================;
;       restorevid              ;
;===============================;
restorevid proc
       pusha
       push ds
       push es

       push cs
       pop ds

       mov ah,2
       xor bh,bh
       mov dx,cursorpos
       int 10h                         ; Restore cursor position

       mov ax,vidsegme
       mov ds,ax
       xor si,si                       ; DS:SI
       mov ax,0b800h
       mov es,ax
       xor di,di                       ; ES:DI
       cld
       mov cx,2000
repne  movsw                           ; move 4000 veces

       pop es
       pop ds
       popa
       ret
restorevid endp


;===============================;
;       showit                  ;
;===============================;
; si = asciiz                   ;
showit proc
        push ax
        push dx

showloop1:
        mov ah,2
        mov dl,[ si ]
        or dl,dl
        je showitend
        int 21h
        inc si
        jmp showloop1

showitend:
        pop dx
        pop ax
        ret
showit endp


;===============================;
;        tochar                 ;
;===============================;
; si = Destination              ;
; ax = number                   ;
tochar proc                          ;convierte binary to int
        push dx
        push bx
        push ax
        push si
        mov [ si ],2020h            ; solo 3
        mov byte ptr [ si+2 ],30h   ; 3 espacios

        add si,3
        xor dx,dx
        mov bx,10
        or ax,ax
        je tocharend
ti2:
        dec si
        div bx
        or dl,'0'
        mov [si],dl
        xor dx,dx
        or ax,ax
        jne ti2

tocharend:
        pop si
        pop ax
        pop bx
        pop dx
        ret
tochar endp

;------------------;
;   new column     ;
;------------------;
newcolumn proc
        pusha
        mov al,dl
        mov ah,3
        xor bh,bh
        int 10h
        mov dl,al
        dec ah
        int 10h
        popa
        ret
newcolumn endp



NEWFILE_END label byte
;***************************************;
;***    antivirus                    ***;
;***************************************;
antivirus:
        mov dx,offset prean_msg
        mov ah,9
        int 21h

        call autosave
        jc irerroras

        call xorear                     ;desencriptar

        push cs
        pop ds
        mov dx,offset antiv_msg
        mov ah,9
        int 21h

irerroras:
        mov ah,4ch
        int 21h                         ;volver a DOS.

;***************************************;
;***    autosave                     ***;
;***************************************;
autosave proc near

        mov cx,0ffffh
        xor ax,ax
        xor di,di
        mov es,cs:[2ch]
scanear:
        repne scasb
        cmp byte ptr es:[di],0
        je ok_todo
        scasb
        jne scanear
ok_todo:
        mov dx,di
        add dx,3
        push es
        pop ds                  ;HASTA ACA DS:DX EL NOMBRE DEL PROG

        call newxor             ;change de value of the xor.
        call xorear             ;invierte los caracteres

        mov ah,3ch              ; Create a File
        xor cx,cx               ; in this case Truncate
        int 21h
        jc derror               ; If error then go to derror

        mov bx,ax               ;file handle
        mov cx,ende - start     ;number of bytes
        push cs
        pop ds
        mov dx,offset start     ;a partir de donde (desde el principio)
        mov ah,40h
        int 21h                 ;write
        jc derror

        mov ax,5701h
        mov cx,FILETIME
        mov dx,FILEDATE
        int 21h                 ;Set Time & Date.
        jc derror

        mov ah,3eh
        int 21h                 ;close
        jc derror

        ret

derror:                         ;error de drive.show message
        call xorear             ;mensaje en estado normal

        push cs
        pop ds
        mov dx,offset reg_not_ok
        mov ah,9
        int 21h
        stc                     ;set clear
        ret

autosave endp

;*********************************;
;***      newxor               ***;
;*********************************;
newxor proc near
        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

xoragain:
        xor ah,ah
        int 1ah
        or dl,dl
        je xoragain

        mov xorvalue,dl

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret
newxor endp

;*********************************;
;***      xorear               ***;
;*********************************;
xorear proc near
        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

        mov cx,newfile_end - intro_msg
        mov si,offset intro_msg
 todavia_sigo:
        mov al,xorvalue
        xor [si],al
        inc si
        dec cx
        jne todavia_sigo

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        ret
xorear endp

;*******************************;
; Inicialization                ;
;*******************************;
init:
        call xorear

        mov ah,4ah
        mov bx,offset ende
        add bx,15
        mov cl,4
        shr bx,cl
        inc bx
        int 21h                             ;change memory

        jmp prog
init_ende label near


ende equ this byte
;== END ============================;

        RUTI    ENDP
CODE    ENDS
        END RUTI
