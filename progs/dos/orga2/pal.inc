;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Paleta generada con PCX2SPR v1.10
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Por la simplicidad de esta paleta la voy a hacer automatica m s adelante
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
label DacBN Byte
db   0,  0,  0; DAC   0.
db   0,  0,  0; DAC   1.
db   0,  0,  0; DAC   2.
db   0,  0,  0; DAC   3.
db   0,  0,  0; DAC   4.
db   1,  1,  1; DAC   5.
db   1,  1,  1; DAC   6.
db   1,  1,  1; DAC   7.
db   1,  1,  1; DAC   8.
db   1,  1,  1; DAC   9.
db   1,  1,  1; DAC  10.
db   2,  2,  2; DAC  11.
db   2,  2,  2; DAC  12.
db   2,  2,  2; DAC  13.
db   2,  2,  2; DAC  14.
db   3,  3,  3; DAC  15.
db   3,  3,  3; DAC  16.
db   3,  3,  3; DAC  17.
db   3,  3,  3; DAC  18.
db   4,  4,  4; DAC  19.
db   4,  4,  4; DAC  20.
db   4,  4,  4; DAC  21.
db   4,  4,  4; DAC  22.
db   5,  5,  5; DAC  23.
db   5,  5,  5; DAC  24.
db   5,  5,  5; DAC  25.
db   5,  5,  5; DAC  26.
db   6,  6,  6; DAC  27.
db   6,  6,  6; DAC  28.
db   6,  6,  6; DAC  29.
db   6,  6,  6; DAC  30.
db   7,  7,  7; DAC  31.
db   7,  7,  7; DAC  32.
db   7,  7,  7; DAC  33.
db   7,  7,  7; DAC  34.
db   8,  8,  8; DAC  35.
db   8,  8,  8; DAC  36.
db   8,  8,  8; DAC  37.
db   8,  8,  8; DAC  38.
db   9,  9,  9; DAC  39.
db   9,  9,  9; DAC  40.
db   9,  9,  9; DAC  41.
db   9,  9,  9; DAC  42.
db  10, 10, 10; DAC  43.
db  10, 10, 10; DAC  44.
db  10, 10, 10; DAC  45.
db  10, 10, 10; DAC  46.
db  11, 11, 11; DAC  47.
db  11, 11, 11; DAC  48.
db  11, 11, 11; DAC  49.
db  11, 11, 11; DAC  50.
db  12, 12, 12; DAC  51.
db  12, 12, 12; DAC  52.
db  12, 12, 12; DAC  53.
db  12, 12, 12; DAC  54.
db  13, 13, 13; DAC  55.
db  13, 13, 13; DAC  56.
db  13, 13, 13; DAC  57.
db  13, 13, 13; DAC  58.
db  14, 14, 14; DAC  59.
db  14, 14, 14; DAC  60.
db  14, 14, 14; DAC  61.
db  14, 14, 14; DAC  62.
db  15, 15, 15; DAC  63.
db  15, 15, 15; DAC  64.
db  15, 15, 15; DAC  65.
db  15, 15, 15; DAC  66.
db  16, 16, 16; DAC  67.
db  16, 16, 16; DAC  68.
db  16, 16, 16; DAC  69.
db  16, 16, 16; DAC  70.
db  17, 17, 17; DAC  71.
db  17, 17, 17; DAC  72.
db  17, 17, 17; DAC  73.
db  17, 17, 17; DAC  74.
db  18, 18, 18; DAC  75.
db  18, 18, 18; DAC  76.
db  18, 18, 18; DAC  77.
db  18, 18, 18; DAC  78.
db  19, 19, 19; DAC  79.
db  19, 19, 19; DAC  80.
db  19, 19, 19; DAC  81.
db  19, 19, 19; DAC  82.
db  20, 20, 20; DAC  83.
db  20, 20, 20; DAC  84.
db  20, 20, 20; DAC  85.
db  20, 20, 20; DAC  86.
db  21, 21, 21; DAC  87.
db  21, 21, 21; DAC  88.
db  21, 21, 21; DAC  89.
db  21, 21, 21; DAC  90.
db  22, 22, 22; DAC  91.
db  22, 22, 22; DAC  92.
db  22, 22, 22; DAC  93.
db  22, 22, 22; DAC  94.
db  23, 23, 23; DAC  95.
db  23, 23, 23; DAC  96.
db  23, 23, 23; DAC  97.
db  23, 23, 23; DAC  98.
db  24, 24, 24; DAC  99.
db  24, 24, 24; DAC 100.
db  24, 24, 24; DAC 101.
db  24, 24, 24; DAC 102.
db  25, 25, 25; DAC 103.
db  25, 25, 25; DAC 104.
db  25, 25, 25; DAC 105.
db  25, 25, 25; DAC 106.
db  26, 26, 26; DAC 107.
db  26, 26, 26; DAC 108.
db  26, 26, 26; DAC 109.
db  26, 26, 26; DAC 110.
db  27, 27, 27; DAC 111.
db  27, 27, 27; DAC 112.
db  27, 27, 27; DAC 113.
db  27, 27, 27; DAC 114.
db  28, 28, 28; DAC 115.
db  28, 28, 28; DAC 116.
db  28, 28, 28; DAC 117.
db  28, 28, 28; DAC 118.
db  29, 29, 29; DAC 119.
db  29, 29, 29; DAC 120.
db  29, 29, 29; DAC 121.
db  29, 29, 29; DAC 122.
db  30, 30, 30; DAC 123.
db  30, 30, 30; DAC 124.
db  30, 30, 30; DAC 125.
db  30, 30, 30; DAC 126.
db  31, 31, 31; DAC 127.
db  31, 31, 31; DAC 128.
db  31, 31, 31; DAC 129.
db  31, 31, 31; DAC 130.
db  32, 32, 32; DAC 131.
db  32, 32, 32; DAC 132.
db  32, 32, 32; DAC 133.
db  32, 32, 32; DAC 134.
db  33, 33, 33; DAC 135.
db  33, 33, 33; DAC 136.
db  33, 33, 33; DAC 137.
db  33, 33, 33; DAC 138.
db  34, 34, 34; DAC 139.
db  34, 34, 34; DAC 140.
db  34, 34, 34; DAC 141.
db  34, 34, 34; DAC 142.
db  35, 35, 35; DAC 143.
db  35, 35, 35; DAC 144.
db  35, 35, 35; DAC 145.
db  35, 35, 35; DAC 146.
db  36, 36, 36; DAC 147.
db  36, 36, 36; DAC 148.
db  36, 36, 36; DAC 149.
db  36, 36, 36; DAC 150.
db  37, 37, 37; DAC 151.
db  37, 37, 37; DAC 152.
db  37, 37, 37; DAC 153.
db  37, 37, 37; DAC 154.
db  38, 38, 38; DAC 155.
db  38, 38, 38; DAC 156.
db  38, 38, 38; DAC 157.
db  38, 38, 38; DAC 158.
db  39, 39, 39; DAC 159.
db  39, 39, 39; DAC 160.
db  39, 39, 39; DAC 161.
db  39, 39, 39; DAC 162.
db  40, 40, 40; DAC 163.
db  40, 40, 40; DAC 164.
db  40, 40, 40; DAC 165.
db  40, 40, 40; DAC 166.
db  41, 41, 41; DAC 167.
db  41, 41, 41; DAC 168.
db  41, 41, 41; DAC 169.
db  41, 41, 41; DAC 170.
db  42, 42, 42; DAC 171.
db  42, 42, 42; DAC 172.
db  42, 42, 42; DAC 173.
db  42, 42, 42; DAC 174.
db  43, 43, 43; DAC 175.
db  43, 43, 43; DAC 176.
db  43, 43, 43; DAC 177.
db  43, 43, 43; DAC 178.
db  44, 44, 44; DAC 179.
db  44, 44, 44; DAC 180.
db  44, 44, 44; DAC 181.
db  44, 44, 44; DAC 182.
db  45, 45, 45; DAC 183.
db  45, 45, 45; DAC 184.
db  45, 45, 45; DAC 185.
db  45, 45, 45; DAC 186.
db  46, 46, 46; DAC 187.
db  46, 46, 46; DAC 188.
db  46, 46, 46; DAC 189.
db  46, 46, 46; DAC 190.
db  47, 47, 47; DAC 191.
db  47, 47, 47; DAC 192.
db  47, 47, 47; DAC 193.
db  47, 47, 47; DAC 194.
db  48, 48, 48; DAC 195.
db  48, 48, 48; DAC 196.
db  48, 48, 48; DAC 197.
db  48, 48, 48; DAC 198.
db  49, 49, 49; DAC 199.
db  49, 49, 49; DAC 200.
db  49, 49, 49; DAC 201.
db  49, 49, 49; DAC 202.
db  50, 50, 50; DAC 203.
db  50, 50, 50; DAC 204.
db  50, 50, 50; DAC 205.
db  50, 50, 50; DAC 206.
db  51, 51, 51; DAC 207.
db  51, 51, 51; DAC 208.
db  51, 51, 51; DAC 209.
db  51, 51, 51; DAC 210.
db  52, 52, 52; DAC 211.
db  52, 52, 52; DAC 212.
db  52, 52, 52; DAC 213.
db  52, 52, 52; DAC 214.
db  53, 53, 53; DAC 215.
db  53, 53, 53; DAC 216.
db  53, 53, 53; DAC 217.
db  53, 53, 53; DAC 218.
db  54, 54, 54; DAC 219.
db  54, 54, 54; DAC 220.
db  54, 54, 54; DAC 221.
db  54, 54, 54; DAC 222.
db  55, 55, 55; DAC 223.
db  55, 55, 55; DAC 224.
db  55, 55, 55; DAC 225.
db  55, 55, 55; DAC 226.
db  56, 56, 56; DAC 227.
db  56, 56, 56; DAC 228.
db  56, 56, 56; DAC 229.
db  56, 56, 56; DAC 230.
db  57, 57, 57; DAC 231.
db  57, 57, 57; DAC 232.
db  57, 57, 57; DAC 233.
db  57, 57, 57; DAC 234.
db  58, 58, 58; DAC 235.
db  58, 58, 58; DAC 236.
db  58, 58, 58; DAC 237.
db  58, 58, 58; DAC 238.
db  59, 59, 59; DAC 239.
db  59, 59, 59; DAC 240.
db  59, 59, 59; DAC 241.
db  59, 59, 59; DAC 242.
db  60, 60, 60; DAC 243.
db  60, 60, 60; DAC 244.
db  60, 60, 60; DAC 245.
db  60, 60, 60; DAC 246.
db  61, 61, 61; DAC 247.
db  61, 61, 61; DAC 248.
db  61, 61, 61; DAC 249.
db  61, 61, 61; DAC 250.
db  62, 62, 62; DAC 251.
db  62, 62, 62; DAC 252.
db  62, 62, 62; DAC 253.
db  62, 62, 62; DAC 254.
db  63, 63, 63; DAC 255.
