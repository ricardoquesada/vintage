COMMENT *
       Organizador del Computador II (2do/96)
       Practica 2 Ejercicio 1
       Ricardo Quesada

Descripcion:
       Tengo una matriz de M x N dividida en submatrices de A x B.
*

MODEL SMALL
LOCALS
P286
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa

       mov  ax,5                       ; x
       mov  bx,3                       ; y
       Call Getxy

       mov  ah,4ch
       int 21h

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Getxy
; Entrada: ax = x
;          bx = y
; Salida:  valor = Valor
PROC Getxy
       pusha
       div  A                          ; en al esta x
       mov  subx,al                    ; submatriz x
       mov  indx,ah                    ; reminder y

       mov  ax,bx                      ; en al esta y
       div  B
       mov  suby,al                    ; submatriz y
       mov  indy,ah                    ; reminder y

; Calcula de la Submatriz
       mov  al,subx
       mul  dosbin
       add  al,suby                    ; Deberia sumar a Ax pero se que
       mul  longword
       mov  bx,ax
       lea  bx,tablec[bx]              ; Solo valido con lea. No usar offset

; En dx ahora esta la matriz a buscar
       mov  bx,[bx]
       mov  al,indy
       mul  dosbin
       add  al,indx
       mul  longword                   ; Multiplica todo por longword
       mov  di,ax
       mov  ax,[bx+di]
       mov  valor,ax
       popa
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de datos
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
DATASEG
; Matriz de apuntadores M/A y N/B.
; �����������������������Ŀ
; � mat00 � mat10 � mat20 �
; �����������������������Ĵ
; � mat01 � mat11 � mat21 �
; �������������������������
tablec dw   offset mat00,offset mat01
       dw   offset mat10,offset mat11
       dw   offset mat20,offset mat21

; cada mat es de A x B
;          col0  col1
mat00  dw   100h,101h
       dw   102h,103h
mat10  dw   111h,112h
       dw   113h,114h
mat20  dw   115h,116h
       dw   117h,118h
mat01  dw   119h,120h
       dw   121h,122h
mat11  dw   123h,124h
       dw   130h,131h
mat21  dw   150h,161h
       dw   170h,180h

; Largo de la Matriz M x N
M      db   6
N      db   4
; Largo de las Subatrices A x B
A      db   2
B      db   2

unobin  db  1
dosbin  db  2
tresbin db  3
longword    db 2


; Uninitialized Data
valor  dw   ?                          ; Lugar de retorno

indy   db   ?
indx   db   ?
subx   db   ?
suby   db   ?

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Stack
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STACK 200H

END
