COMMENT *
       Organizador del Computador II
       Proyecto N�mero 1
       Grupo: Tito 2
       Integrantes: Daniel Barea
                    Sebasti�n Cativa Tolosa
                    Ricardo Quesada

       Version: 0.0.1
*
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
DOSSEG                                 ; Como se van a ordenar los segmentos
MODEL SMALL                            ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior
P387                                   ; con copro
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa
       lea  dx,[InitString]
       Call DisplayString              ; Muestra Comienzo.
       Call CargarArchivo              ; Carga archivo. Todo Bien ?
       jc   Salir                      ; No. Hubo Error -> Salir
       Call ValidFormat                ; Es de un formato v�lido ?
       jc   Salir                      ; No. Hubo Error -> Salir
       Call Luminancia
Salir:
       EXITCODE                        ; Finaliza

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Display String
; Input: dx = offset of string
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC DisplayString
       push ax
       mov  ah,9
       int  21h
       pop  ax
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Cargar Archivo
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC CargarArchivo
       mov  ax,3d00h                   ; Open File. Mode Read Only
       lea  dx,[openname]              ; ds:dx-> asciiz name
       int  21h                        ; Open it!
       jnc  @@NoError
       lea  dx,[ErrorString]
       Call DisplayString              ; Mostrar Error String
       stc
       ret
@@NoError:
       mov  [fhandle],ax                 ; Save File handle for future use.
       clc
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Valid Format
; Chequea si el archivo es .pcx o algun otro formato v�lido.
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ValidFormat
       pusha
       mov  bx,[fhandle]               ; File handle
       mov  cx,128                     ; 128 bytes
       mov  ah,3fh                     ; read
       lea  dx,[pcxh]                  ; DS:DX  -> buffer ( destination )
       int  21h

       cmp  [manu],10                  ; Manufacturer ID
       jne  ErrorVersion               ; OK ?
       cmp  [enc],1                    ; Encoding version.
       je   @@ok                       ; Yes, entonces es PCX

ErrorEncode:
       lea  dx,[ErrorPCXEncode]
       Call DisplayString
       jmp  @@notok
ErrorVersion:
       lea  dx,[ErrorPCXVersion]
       Call DisplayString
@@notok:
       mov  bx,[fhandle]
       mov  ah,3eh                     ; Close File Handle
       int  21h                        ; Close it!
       stc                             ; set error flag (carry)
       popa
       ret                             ; Return with Carry Set -> Error
@@ok:
       clc                             ; Return with Carry Clear -> Ok
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 1: Smoothering
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Smooth
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 2: Blur
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Blur
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 3:
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Efecto3
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto Luminancia
; dx = ax * 0,3 (Rojo) + bx * 0,59 (Verde) + cx * 0,11 (Azul)
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Luminancia
       mov  [temp],ax                  ; Rojo
       fild [temp]                     ; Carga el valor entero a st
       fmul [dato030]                  ; Multiplica ax * 0,30
       fst  [suma]                     ; y lo suma
       mov  [temp],bx                  ; Verde
       fild [temp]
       fmul [dato059]                  ; multiplica bx * 0,59
       fadd [suma]                     ; lo suma a lo anterior
       fst  [suma]                     ; y lo guarda
       mov  [temp],cx                  ; Azul
       fild [temp]
       fmul [dato011]                  ; Multiplica cx * 0,11
       fadd [suma]                     ; lo suma a lo anterior
       fst  [suma]                     ; y lo guarda
       fist [sumado]                   ; y lo guarda tambien como entero
       mov  dx,[sumado]                ; devuelve lo sumado
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de datos
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
DATASEG
; String
LABEL InitString byte                  ; Mensaje de comienzo
db 13,10,'Organizador del Computador II - Proyecto I ',13,10
db 'Grupo: Tito 2 ( Barea, Cativa Tolosa, Quesada )',13,10,'$'
LABEL ErrorString byte                 ; Mensaje de Error
db 13,10,'Error: No se pudo abrir el archivo',13,10,'$'
LABEL ErrorPCXVersion byte             ; por ahora los 2 apuntan al mismo.
LABEL ErrorPCXEncode  byte
db 13,10,'Error: Este no es un archivo .pcx',13,10,'$'

openname    db 'tito2.pcx',0           ; nombre temporar ( borrar luego )

dato011 dq  0.11                       ; usado por luminancia
dato059 dq  0.59                       ; usado por luminancia
dato030 dq  0.3                        ; usado por luminancia

INCLUDE 'paleta.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Variables no inicializadas
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
temp   dw   ?                          ; usado por luminancia
sumado dw   ?                          ; usado por luminancia
suma   dq   ?                          ; usado por luminancia
nombre db  80 dup(?)                   ; Nombre del archivo. usado por CargarArchivo
fhandle dw  ?                          ; File handle ( por funciones de archivos )

; Esto podria ser una estructura (STRUC) - par una futura version ;
; Por m�s que halla valores inicializados, esto no se pierden ;
LABEL  pcxh byte                      ; Cabecera de PCX
manu   db   10                         ; Manufacturer = 10
ver    db   ?                          ; Version   (0,2,3,4,5)
enc    db   1                          ; Encoding = 1
bpp    db   ?                          ; Bitsperpixel
wind   dw   0,0,0,0                    ; Xmin,Ymin,Xmax,Ymax
hdpi   dw   ?                          ; Horiz resol
vdpi   dw   ?                          ; Vertic resol
cmap   db   48 dup(?)                  ; Palette
       db   0                          ; Reserved . Should be set to 0
npla   db   1                          ; Number of Color Planes
bypp   dw   ?                          ; Bytes per plane
palinfo dw  ?                          ; Pallete info (1=Color/BW,2=Grayscale)
hsize  dw   ?                          ; horizontal screen size in pixels.
vsize  dw   ?                          ; vertical "
       db   54 dup(0)                  ; to fill 128 chars.

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Stack
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STACK 200H

END
