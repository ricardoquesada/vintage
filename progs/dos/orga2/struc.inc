;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Archivo que contiene estructuras y dem s cosas
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Estructura de la cabecera de un archivo PCX
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STRUC  pcxh                            ; Structura del PCX
manu   db   ?                          ; Manufacturer = 10
ver    db   ?                          ; Version   (0,2,3,4,5)
enc    db   ?                          ; Encoding = 1
bpp    db   ?                          ; Bitsperpixel
wind   dw   ?,?,?,?                    ; Xmin,Ymin,Xmax,Ymax
hdpi   dw   ?                          ; Horiz resol
vdpi   dw   ?                          ; Vertic resol
cmap   db   48 dup(?)                  ; Palette
       db   ?                          ; Reserved . Should be set to 0
npla   db   ?                          ; Number of Color Planes
bypp   dw   ?                          ; Bytes per plane
palinfo dw  ?                          ; Pallete info (1=Color/BW,2=Grayscale)
hsize  dw   ?                          ; horizontal screen size in pixels.
vsize  dw   ?                          ; vertical "
       db   54 dup(?)                  ; to fill 128 chars.
ENDS

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Estructura DTA (Disk Transfer Area)
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STRUC dtah
       db   21 dup(?)
atrib  db   ?                          ; atributo
time   dw   ?                          ; time
date   dw   ?                          ; date
size   dd   ?                          ; size del archivo
nombre db   13 dup(?)                  ; name
ENDS
