COMMENT *
       Organizador del Computador II
       Proyecto N�mero 1
       Grupo: Tito 2
       Integrantes: Barea, Daniel
                    Cativa Tolosa, Sebasti�n
                    Quesada, Ricardo Calixto

       Version: 0.0.3
*

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
DOSSEG                                 ; Como se van a ordenar los segmentos
MODEL SMALL                            ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior
P387                                   ; con copro

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines y Includes ( m�s abajo hay otro include )
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
INCLUDE 'struc.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
; main()
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa
       Call InitMem                    ; Pone bien la memoria ( Modifica la
                                       ; memoria reservada de entrada )
       lea  dx,[InitString]
       Call DisplayString              ; Muestra Comienzo.
       Call Parametros                 ; Chequear los parametros.
@@siguiente:
       Call CargarArchivo              ; Carga archivo. Todo Bien ?
       jc   @@Salir                    ; No. Hubo Error -> Salir
       Call ValidFormat                ; Es de un formato v�lido ? ( pcx,etc...)
       jc   @@Siguiente                ; No. Hubo Error -> Siguiente archivo
       Call ViewGraphic                ; Mostrar el gr�fico en la pantalla
       jc   @@Siguiente                ; No lo pudo mostrar. Siguiente archivo
       Call Luminancia                 ; Transforma a escala de grises
       jmp  @@siguiente                ; ir al siguiente archivo
@@Salir:
       EXITCODE                        ; Finaliza

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Display String
; Input: dx = offset of string
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC DisplayString
       push ax
       mov  ah,9
       int  21h
       pop  ax
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; InitMem
; Por como maneja el DOS la memoria es necesario hacer esto
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC InitMem
       mov  bx,ss                      ; Compute distance between
       mov  ax,es                      ; PSP and stack
       sub  bx,ax

       mov  ax,sp
       add  ax,15
       mov  cl,4
       shr  ax,cl                      ; Divide por 16 ( Paragraphs )

       add  bx,ax

       mov  ah,4ah
       int  21h                        ; Modifica la Memoria reservada
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Parametros
; Chequea que los parametros de la linea sean correctos
; ES:0081 -> empieza.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Parametros
       push ds
       push es
       pusha

       push es                         ; es,ds=ds,es (Doble asignacion)
       push ds
       pop  es
       pop  ds

       cld                             ; Clear Direction flag
       lea  di,[nombtt]                ; nombre default.
       mov  si,81h                     ; ds:si (a partir de 81h estan los
                                       ; parametros )
@@loop:
       lodsb                           ; al <- ds:si ( si++)
       cmp  al,20h                     ; es SPACE ?
       je   @@loop                     ; si, entonces lee el siguiente.
       cmp  al,13                      ; es ENTER ?
       je   @@salir                    ; si
       cmp  al,'/'                     ; es '/' ?
       je   @@param                    ; si.
       jne  @@esnombre                 ; entonces es el nombre


@@param:
       lodsb
       cmp  al,20h                     ; es espacio.
       je   @@help                     ; Si hay error en linea de comando.
       and  al,11011111b               ; minusculas/MAYUSCULAS
       cmp  al,'A'
       je   @@salir                    ; a modo de ejemplo
       jne  @@help                     ; si ir a ayuda.

@@esnombre:                            ; poner el nombre que se quiere buscar.
       lea  di,[nombre]                ; es:di
@@loop2:
       stosb                           ; escribe en es:di
       lodsb
       cmp  al,20h                     ; se acabo el nombre ?
       je   @@loop                     ; volver al primer loop
       cmp  al,13                      ; es enter ?
       jne  @@loop2                    ; no, entonces seguir con el loop
                                       ; si, entonces salir.

@@salir:
       mov  al,0
       stosb                           ; es:di -> 0 (asciiz del nombre)

       popa
       pop  es
       pop  ds
       ret

@@help:                                ; hubo error en la linea de parametros
       popa
       pop  es
       pop  ds
       lea  dx,[ErrorHelp]
       Call DisplayString

       mov  al,1                       ; Error code=1
       EXITCODE                        ; Finaliza
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Cargar Archivo
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC CargarArchivo
       mov  ah,1ah
       lea  dx,[dtaaca]                ; Donde se va a almacenar la DTA
       int  21h                        ; Change DTA.

       mov  ah,4eh
       add  ah,[fnombre]               ; flag (0 la primer vez. Luego 1)
       mov  [fnombre],1                ; Indica si es Find First o Next
       mov  cx,63h                     ; All attributes
       lea  dx,[nombre]                ;
       int  21h                        ; Find (First,Next) Matching File
                                       ; (Para saber el largo)
                                       ; El largo me interesa para poder
                                       ; reservar memoria para cargar el
                                       ; archivo. Y esa info la obtengo de
                                       ; la DTA.
       jc   @@notok                    ; Hubo error.

       mov  ax,3d00h                   ; Open File. Mode Read Only
       lea  dx,[dtaaca.nombre]         ; ds:dx-> asciiz name
       int  21h                        ; Open it!
       jnc  @@ok
@@notok:
       lea  dx,[ErrorAbrir]
       Call DisplayString              ; Mostrar Error String
       stc
       ret
@@ok:
       mov  [fhandle],ax               ; Save File handle for future use.
       clc
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Valid Format
; Chequea si el archivo es .pcx o algun otro formato v�lido.
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ValidFormat
       pusha
       mov  bx,[fhandle]               ; File handle
       mov  cx,128                     ; 128 bytes
       mov  ah,3fh                     ; read
       lea  dx,[pcxaca]                ; DS:DX  -> buffer ( destination )
       int  21h                        ; Lee la cabecera del .pcx

       cmp  [pcxaca.manu],10           ; Manufacturer ID
       jne  ErrorVersion               ; OK ?
       cmp  [pcxaca.enc],1             ; Encoding version.
       je   @@ok                       ; Yes, entonces es PCX

ErrorEncode:
       lea  dx,[ErrorPCXEncode]
       Call DisplayString
       jmp  @@notok
ErrorVersion:
       lea  dx,[ErrorPCXVersion]
       Call DisplayString

@@notok:
       mov  bx,[fhandle]
       mov  ah,3eh                     ; Close File Handle
       int  21h                        ; Close it!
       stc                             ; set error flag (carry)
       jmp  @@salir                    ; retorna.

@@ok:
       Call Es256yFit                  ; Es un pcx que yo puedo ver ?
       jc   @@notok                    ; No. Entonces salir con error.
       clc                             ; Return with Carry Clear -> Ok

@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Es256yFit
; llamada por ValidFormat
; Chequea si el formato (que es v�lido), es soportado.
; OUT: Carry=off , lo soporta
;           =on  , no lo soporta
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Es256yFit
       pusha
       lea  bx,[pcxaca.wind]           ; Xmin,Ymin,Xman,Ymax
       mov  ax,[bx+4]
       sub  ax,[bx]                    ; Xtotal
       inc  ax
       mov  [realx],ax
       mov  cx,[bx+6]
       sub  cx,[bx+2]                  ; Ytotal
       inc  cx
       mov  [realy],cx

       cmp  [realy],200
       ja   @@notok
       cmp  [realx],320
       jbe  @@ok

       cmp  [pcxaca.npla],1            ; Es de 256 colores ?
       je   @@ok                       ; Si, entonces todo bien.

@@notok:
       lea  dx,[ErrorPCXColor]
       Call DisplayString
       stc
       jmp  @@salir
@@ok:
       clc
@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ViewGraphic
; Muestra en pantalla el gr�fico.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ViewGraphic
       Call ReadPCX
       jc   @@notok
       Call ViewPCX
       jnc  @@ok
@@notok:
       mov bx,[fhandle]                ; Hubo error.
       mov ah,3eh                      ;
       int 21h                         ; Close file handle ( del archivo )

       stc
       jmp  @@salir
@@ok:
       push es                         ; Lo guardo, tal vez lo necesite.
       mov  ah,49h                     ; Liberar memoria reservada
       mov  ax,[memseg]
       mov  es,ax
       int  21h
       pop  es                         ; Listo!
       clc
@@salir:
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ReadData
; La encargada real de leer el archivo y ponerlo en memoria.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ReadPCX
       pusha
       push ds
       push es

       mov  ah,48h
       mov  ebx,[dtaaca.size]          ; Tama�o del archivo
       shr  ebx,4                      ; Divide por 16
       inc  ebx                        ; le suma 1
       int  21h                        ; Reverva memoria para cargar el archivo
       jnc  @@ok

       lea  dx,[ErrorNomem]
       Call DisplayString

       stc
       jmp  @@salir

@@ok:
       mov  [memseg],ax                ; Guarda el segmento.
       push ds
       pop  es                         ; ES=DS
       mov  ds,ax                      ; AX tiene el segmento de memoria
       xor  dx,dx                      ; DS:DX -> Buffer
       mov  ah,3fh                     ; Read
       mov  bx,[es:fhandle]
       mov  ecx,[es:dtaaca.size]       ; Cantidad de datos a leer.
       sub  ecx,128                    ; la cabecera no ( ya esta leida )
       int  21h                        ; Finalmente leer.
       clc
@@salir:
       pop  es                         ; Restaura segmentos usados.
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ViewPCX
; Funcion que muestra en pantalla el pcx
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ViewPCX
       pusha
       push ds
       push es

       push ds
       pop  fs
       cld                             ; Clear Direction Flag

       mov  ax,13h
       int  10h                        ; Set Graphics mode

       cmp  [pcxaca.ver],5
       jne  @@here1

       call changedac                  ; Modifica la paleta (viene con paleta)
@@here1:
       mov cx,[realy]
       mov [uny],cx
       mov cx,[realx]                  ; Number of times

       mov  ax,320
       sub  ax,cx
       mov  [addx],ax                  ; El aditivo para sumar

       mov  ax,0a000h
       mov  es,ax                      ; ES cambia de valor
       xor  di,di                      ; ES:DI
       mov  ax,[memseg]
       mov  ds,ax                      ; DS cambia de valor
       xor  si,si                      ; DS:SI

mainunpacking:
       lodsb                           ; al = ds:si / si = si + 1
       mov  ah,al                      ; 2 hi bits.
       and  ah,192                     ; Encodeado ?
       cmp  ah,192                     ; Encodeado ?
       je   coded                      ; parece que si
       stosb                           ; es:di = al / di = di + 1

       dec cx
       jcxz newline
       jmp  mainunpacking

coded:
       mov  ah,al                      ;
       and  ah,63                      ; Los otros valores (ah=number of times)
       lodsb                           ; al = ds:si / si = si + 1
codedhere:
       stosb                           ; es:di = al / di = di + 1
       dec  cx
       dec  ah                         ; Sigue en encode ?
       jne  codedhere                  ; si, entonces repetir valor.
       jcxz newline                    ; Salta si cx=0. Esta linea X lista.
       jmp  mainunpacking
newline:
       mov  cx,[fs:realx]
       add  di,[fs:addx]
       dec  [fs:uny]
       cmp  [fs:uny],0
       jne  mainunpacking

       mov  ah,8
       int  21h                         ; get key
       mov  ax,3h
       int  10h

       pop  es
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ChangeDAC
; Modifica los 256 colores de la paleta
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ChangeDAC
       push ds
       push es

       cld
       mov  ebx,[dtaaca.size]
       sub  bx,897                     ; 897 = 769 + 128
       mov  ax,[memseg]
       mov  ds,ax
       mov  es,ax
       mov  al,12
       mov  di,bx
       scasb
       jne  @@salir

       mov  si,di
       push si
       mov  cx,768
@@loop:
       lodsb
       shr  al,2                       ; Divide por 4 la paleta.
       stosb
       loop @@loop

       pop  si
       xor  al,al
       mov  dx,3c8h
       out  dx,al
       inc  dx
       mov  cx,768
repne  outsb                           ; ds:si -> va al port cx veces

@@salir:
       pop  es
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 1: Smoothering
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Smooth
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 2: Blur
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Blur
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 3:
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Efecto3
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto Luminancia
; dx = ax * 0,3 (Rojo) + bx * 0,59 (Verde) + cx * 0,11 (Azul)
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Luminancia
       mov  [temp],ax                  ; Rojo
       fild [temp]                     ; Carga el valor entero a st
       fmul [dato030]                  ; Multiplica ax * 0,30
       fst  [suma]                     ; y lo suma
       mov  [temp],bx                  ; Verde
       fild [temp]
       fmul [dato059]                  ; multiplica bx * 0,59
       fadd [suma]                     ; lo suma a lo anterior
       fst  [suma]                     ; y lo guarda
       mov  [temp],cx                  ; Azul
       fild [temp]
       fmul [dato011]                  ; Multiplica cx * 0,11
       fadd [suma]                     ; lo suma a lo anterior
       fst  [suma]                     ; y lo guarda
       fist [sumado]                   ; y lo guarda tambien como entero
       mov  dx,[sumado]                ; devuelve lo sumado
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de datos
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
DATASEG
; String
LABEL InitString byte                  ; Mensaje de comienzo
db 13,10,'Organizador del Computador II - Proyecto I ',13,10
db 'Grupo: Tito 2 ( Barea, Cativa Tolosa, Quesada )',13,10,'$'
LABEL ErrorAbrir byte                 ; Mensaje de Error
db 13,10,'Error: No se pudo abrir el archivo',13,10,'$'
LABEL ErrorPCXVersion byte             ; por ahora los 2 apuntan al mismo.
LABEL ErrorPCXEncode  byte
db 13,10,'Error: Este no es un archivo .pcx',13,10,'$'
LABEL ErrorPCXColor  byte
db 13,10,'Error: Este archivo .pcx no es de 256 colores',13,10,'$'
LABEL ErrorNoMem  byte
db 13,10,'Error: Necesita m�s memoria.',13,10,'$'
LABEL ErrorHelp  byte
db 13,10,'Formato: Orga2 [/? | filespec]',13,10,13,10
db 9,'/?           Muestra esta pantalla',13,10
db 9,'Filespec     Puede ir el nombre de un archivo con Wildcards (*,?,etc)',13,10
db 9,'             Default: TITO2.PCX',13,10
db 13,10
db 'Ejemplos: Orga2 *.pcx',13,10
db '          Orga2 dibu??.*',13,10,'$'

fnombre     db 0                       ; Primer nombre ( 0=Find First 1=Find Next)
dato011 dq  0.11                       ; usado por luminancia
dato059 dq  0.59                       ; usado por luminancia
dato030 dq  0.3                        ; usado por luminancia

nombre db   'tito2.pcx'                ; este es el nombre default de Cargar Archivo.
nombtt db    40 dup(?)                 ; Reservo mas por las dudas.

INCLUDE 'paleta.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Variables no inicializadas
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
temp   dw   ?                          ; usado por luminancia
sumado dw   ?                          ; usado por luminancia
suma   dq   ?                          ; usado por luminancia
fhandle dw  ?                          ; File handle ( por funciones de archivos )
realx  dw   ?                          ; lo que ocupa de en serio el pcx
realy  dw   ?                          ; usado por Is256yFit
unx    dw   ?                          ; Usados por ViewPCX
uny    dw   ?                          ;    "    "    "
addx   dw   ?                          ;    "    "    "
memseg dw   ?                          ; Segmento de memoria reservado.
LABEL pcxaca pcxh                      ; Define Structura!
       db   128 dup(?)                 ; Donde ir� la cabecera del .pcx
LABEL dtaaca dtah                      ; Define Structura!
       db   43 dup(?)                  ; Donde va a ir la DTA temporaria

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Stack
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STACK 200H

END
