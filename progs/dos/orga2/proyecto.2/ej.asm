COMMENT *
       Ejercicio para el 2do parcial de Orga2
*

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
DOSSEG                                 ; Como se van a ordenar los segmentos
MODEL SMALL                            ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior
P387                                   ; con copro

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines y Includes ( m s abajo hay otro include )
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
INCLUDE     'nodo.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
; main()
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa
       Call OrdenarLista
       EXITCODE 0                      ; Finaliza

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Ordenar Lista
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   OrdenarLista
       Call DelNodo
       Call InsNodoSort
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      DelNodo
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   DelNodo
       pusha

       mov  bx,[NDelNodo]              ;
       mov  [OffNewNodo],bx            ; Offset del
       mov  cx,[bx+2]
       mov  [NDelNodo],cx              ; Siguiente nodo a borrar

@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      InsNodoSort
;      Inserta un nodo ordenado
;
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   InsNodoSort
       pusha

       lea  di,[Nodo0]                 ; Hay un nodo (nodo 0) que solo apunta
                                       ; al primer nodo.
@@siguientenodo:
       mov  bx,[di+2]                  ; bx ahora tiene la dir del Primer Nodo

       mov  si,[OffNewNodo]            ; Comienzo del nuevo nodo
       mov  ax,[si]                    ; Valor del Nuevo Nodo
       mov  dx,[bx]                    ; Valor de un nodo de la lista ordenada
       cmp  ax,dx                      ; ax=Nodo a ins dx=Viejo Nodo
       jbe  @@insNodo                  ; Insertar Nodo

       cmp  [WORD PTR bx+2],0ffffh     ; Pero es el ultimo nodo ?
       je   @@insnodo

       mov  di,bx

       jmp  @@siguientenodo

@@insnodo:
       mov  [si+2],bx                  ; Le pone al nuevo nodo el offset al
                                       ; siguiente nodo
       mov  [di+2],si                  ; Puntero del nodo anterior a este.
       jmp  @@salir

@@salir:
       popa
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Datos
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
DATASEG

Nodo0  dw   0
       dw   OFFSET N_A

NDelNodo dw  OFFSET N_B                ; Puntero al siguiente nodo a ordenar
                                       ;

N_A:   dw   3                          ; Este es el primer nodo
       dw   0ffffh                     ; No hace falta que lo ordene

N_B:   dw   6
       dw   OFFSET N_C

N_C:   dw   9
       dw   OFFSET N_D

N_D:   dw   10
       dw   OFFSET N_E

N_E:   dw   50
       dw   OFFSET N_F

N_F:   dw   1
       dw   OFFSET N_G

N_G:   dw   0ffffh                     ; No final
       dw   0FFFFh                     ; No hay siguiente nodo


OffNewNodo  dw   ?                     ; Offset del nuevo nodo

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Stack
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STACK 200H

END
