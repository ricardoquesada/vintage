
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Este es el programa .com que se va a ejecutar
; Tiene que cumplir ciertas reglas para que funcione como las siguientes.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
MODEL TINY                             ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines y Includes
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
THISVERSION = 0002h                    ; Resident version

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
; main()
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
STARTUPCODE                            ; Necesario p' compilar. Pero no hace
                                       ; en un .com
       Jmp  Short Start                 ; Looping (2 bytes)
       dw   'T2'                       ; Chequeo de programa
       dw   0001h                      ; Format version
       dw   ?                          ; Para llenar 8
;;;          12345678
       db   'Prueba1 '                 ; Nombre de los programas.
       db   'Prueba2 '
       db   'Prueba3 '
       db   'Prueba4 '
       dw   OFFSET Prog1
       dw   OFFSET Prog2
       dw   OFFSET Prog3
       dw   OFFSET Prog4


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Start
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
Start:
       mov  ax,0b800h
       mov  ds,ax
       mov  si,40h
@@loop:
       inc  [BYTE PTR si]
;       int  08h
       jmp  @@loop

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Programa 1
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Prog1
       sti
       mov  bx,0b800h
       mov  ds,bx
       mov  bx,2h
       inc  [BYTE PTR bx]
       jmp  Prog1
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Programa 2
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Prog2
       sti
       mov  bx,0b800h
       mov  ds,bx
       mov  bx,4h
       inc  [BYTE PTR bx]
       jmp  Prog2
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Programa 3
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Prog3
       sti
       mov  bx,0b800h
       mov  ds,bx
       mov  bx,6h
       inc  [BYTE PTR bx]
       jmp  Prog3
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Programa 4
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Prog4
       sti
       mov  bx,0b800h
       mov  ds,bx
       mov  bx,8h
       inc  [BYTE PTR bx]
       jmp  Prog4
ENDP


END
