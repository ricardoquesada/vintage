;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Estructuras usadas por el proyecto N2 TiPro2 de Orga2
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Lo que tiene que guardar cuando se cambia de programa
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STRUC  Ambiente
ip     dw ?
cs     dw ?
sp     dw ?
ss     dw ?
gs     dw ?
fs     dw ?
es     dw ?
ds     dw ?
flags  dw ?
edi    dd ?                            ; Esto es salvado / restaurado
esi    dd ?                            ; con pushad / popad
ebp    dd ?
esp    dd ?
ebx    dd ?
edx    dd ?
ecx    dd ?
eax    dd ?

ENDS
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      Estructura del programa a ejecutra
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STRUC  Programa
inic   dw   ?                          ; Jmp $+0
copy   dw   ?                          ; 'T2'
ver    dw   ?                          ; Version
       dw   ?                          ; Para hacer 8
Nom1   db   8 dup(?)                   ; Nombres de los programas
Nom2   db   8 dup(?)                   ;
Nom3   db   8 dup(?)                   ;
Nom4   db   8 dup(?)                   ;
Off1   dw   ?                          ; Offsets de los prgramas.
Off2   dw   ?
Off3   dw   ?
Off4   dw   ?                          ;
Offend dw   ?                          ; Rutina que se ejecuta cuando se
                                       ; se terminan de ejecutar los procesos
ENDS
