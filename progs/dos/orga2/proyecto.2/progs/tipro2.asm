COMMENT *
       Organizador del Computador II
       Proyecto N�mero 2
       Grupo: Tito 2
       Nombre: TiPro2
       Integrantes: Barea, Daniel Romulo
                    Belfer, Diego Remolacha
                    Cativa Tolosa, Sebastian Roque
                    Quesada, Ricardo Ruben

       Version: 0.02

       Compilar de la siguiente manera:
                     tasm /m TiPro2
                     tlink /t TiPro2
*

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
MODEL TINY                             ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines y Includes
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
THISVERSION = 0002h                    ; Resident version
STACKLEN    = 256                      ; bytes para las pilas de los progs.

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Estructuras y otras cosas.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
include 'strucs.inc'                   ; Definicion de estructuras.

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
; main()
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa (aunque en .com, nada)
       jmp  Main                       ; ir al comienzo

       db   'hecho por Tito 2',0       ; Mensaje de copyright

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      NEW08H - Handler de la interrupcion 08h
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   NEW08H
       Jmp  short @@aca
       db   'T2'
@@aca:
       cli

       cmp  [cs:estoyactive],0
       je   @@bien
       jmp  [cs:old08h]                ; Salta a la rutina original

@@bien:
       mov  [cs:estoyactive],1

       pop  [cs:oldip]
       pop  [cs:oldcs]
       pop  [cs:oldflags]              ; sp-6
       sub  sp,6                       ; Listo. problema resuelto

       pushf                           ;
       push es                         ;

       mov  es,[cs:oldcs]
       cmp  [WORD PTR es:102h],'T2'    ; Se esta ejecutando TiPro2 ?
       je   @@esmiprog                 ; Parece que si.

       pop  es                         ;
       popf                            ;

       mov  [cs:estoyactive],0
       jmp  [cs:old08h]                ; Salta a la rutina original

@@esmiprog:
       pop  es
       popf

       cli                             ; Cambia la pila de este programa
       mov  [cs:Oldsp],sp              ; a otro lugar asi trabaja sin
       mov  [cs:oldss],ss              ; usar otras pilas.
       mov  [cs:Temp],ax
       mov  ax,cs
       mov  ss,ax
       lea  sp,[MyStack]
       mov  ax,[cs:temp]               ;
       sti

       pushf
       push es
       push ds                         ;
       pusha                           ; Luego hay que restauralos

       push cs
       pop  ds                         ; ds=cs
       push cs
       pop  [ThisCS]                   ; Guardo este CS para un futuro

       cmp  [primer],0                 ; Es la primera vez que se ejecuta.
       jne  @@noes1

       cld                             ; Mueve los datos a la struc
       mov  cx,SIZE Programa           ; Long de la estructura.
       lea  di,[ProgH]
       mov  si,100h                    ; ds:si -> es:di
       push es
       pop  ds
       push cs
       pop  es
rep    movsb                           ; Mueve lo nesario

       push ds
       pop  es                         ; es segmento del programa
       push cs
       pop  ds                         ; ds segmente de esto programa

       mov  [primer],1                 ; Cambia el flag para la proxima.
       mov  [ticks],0                  ; Inicializa el Tiempo
       mov  [activos],0                ; de terminar el programa
       mov  [activos+1],0              ; desde la rutina de terminar todo
       mov  [activos+2],0              ;
       mov  [activos+3],0
       mov  [oldflags],7202h           ; Flags para estos

; Parametros que hay que pasar
;           si: ip     bx= cs
;           di: sp     ss= This cs
;           dx: Default para los demas valores.
;           cx: a que proceso le seteo

       mov  bx,es                      ;;; CS ( No cambia, puede ir
                                       ; afuera del loop )

       mov  cx,1                       ; a que proceso se lo voy a aplicar ?
                                       ; tambien puede ir afuera del loop
@@loop:
       dec  cx
       shl  cx,1                       ; Multiplico por 2
       lea  di,[Progh.Off1]
       add  di,cx                      ; Que prog en verdad ?
       mov  si,[di]                    ;;; IP
       lea  di,[offstk1]
       add  di,cx
       mov  di,[di]                    ;;; SP
       shr  cx,1
       inc  cx                         ;;; CX
       xor  edx,edx                    ;;; EDX
       Call SetEnvi
       inc  cx
       cmp  cx,5
       jne  @@loop

       mov  [Turno],1                  ; Vuelve con el turno a 1

       popa
       pop  ds                         ; sp+4
       pop  es                         ; sp+2
       popf                            ; sp+0

       push cs
       pop  ds                         ; ds=cs

       jmp  Restaurar                  ; Hasta aca estan los valores
                                       ; originales, pero no me importa en
                                       ; verdad.

@@noes1:
       xor  ah,ah                      ; Chequea que quede algun proceso
       add  ah,[Activos]               ; activo
       add  ah,[Activos+1]             ;
       add  ah,[Activos+2]             ;
       add  ah,[Activos+3]             ; Si ah=4 significa que terminaron todos
       cmp  ah,4
       jne  @@hayactivos

       ;;;
       ;;; Se ejecuta esto si ya terminaron los 4 procesos
       ;;;

       popa
       pop  ds                         ; sp+4
       pop  es                         ; sp+2
       popf                            ; sp+0
       cli
       mov  ss,[cs:Oldss]              ; Restaura la pila
       mov  sp,[cs:Oldsp]              ; Restaura la pila
       mov  bp,sp
       mov  ax,[cs:progh.offend]       ; ip de la rutina que hay que ejecutar
       mov  [bp],ax                    ; para que se acabe todo.
       mov  [cs:estoyactive],1         ; No activo nada
       jmp  [cs:old08h]                ; Salta a la rutina original

@@hayactivos:
       mov  al,[Tiempo]
       cmp  [ticks],al                 ; 1 segundo
       jne  @@saltear

       mov  [ticks],0
       Jmp  SaveEnvi                   ; Guarda el Environment del proceso

@@saltear:
       inc  [ticks]                    ;
       popa
       pop  ds                         ; sp+4
       pop  es                         ; sp+2
       popf                            ; sp+0

       cli
       mov  ss,[cs:Oldss]              ; Restaura la pila
       mov  sp,[cs:Oldsp]              ; Restaura la pila
       mov  [cs:estoyactive],0
       jmp  [cs:old08h]                ; Salta a la rutina original
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      SaveEnvi
;      Esta rutina se encarga de guardar los estados de cierto proceso
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   SaveEnvi

       mov  cl,[Turno]                 ;
       dec  cl                         ; -1 ( de 0..3)
       shl  cl,1                       ; Multiplica por 2
       xor  ch,ch                      ;

       lea  bx,[OffPrg1]               ; Offset del programa1
       add  bx,cx                      ; le suma el turno

       mov  ax,[bx]                    ; Donde empieza el programa 1
       add  ax,SIZE Ambiente           ; de verd�
       mov  [Temp],ax                  ; Donde va a ir la pila

       popa
       pop  ds                         ; sp+4
       pop  es                         ; sp+2
       popf                            ; Aca tengo todo los originales.

       cli
       mov  [cs:Thisss],ss             ; y segmento actuales
       mov  [cs:Thissp],sp             ; Guarda

       mov  ss,[cs:ThisCS]             ; De donde voy a sacar los datos
       mov  sp,[cs:Temp]               ; del programa a restaurar
       sti

       pushad                          ; Salva todo lo que se puede
       push [cs:oldflags]              ; del programa ejecutado
       push ds                         ; para poder cambiar al otro
       push es                         ; programa
       push fs
       push gs
       push [cs:Oldss]                 ; push ss
       push [cs:Oldsp]                 ; push sp
       push [cs:oldcs]                 ; push cs
       push [cs:oldip]                 ; push ip

;;;
;;;    Tengo volver a poner ss:sp donde estaba
;;;
       cli
       mov  sp,[cs:Thissp]             ; Viejo ss:sp
       mov  ss,[cs:Thisss]             ;
       sti

       push cs
       pop  ds


;;;
;;;    Hasta aca fue salvado el ambiente del programa X
;;;    Ahora lo que hay que hacer es restaurar el ambiente
;;;    del programa X+1
;;;    No me tiene que importar modificar ahora las variables
;;;    ya que va a ser modificas por las salvadas anteriormente
;;;

@@looping:
       inc  [Turno]
       cmp  [Turno],5                  ; Siguiente turno
       jne  @@aca
       mov  [Turno],1                  ; Le toca al primero

@@aca:
       mov  bl,[Turno]                 ; Solo ejecuta los procesos
       dec  bl                         ; que estan activos
       xor  bh,bh                      ; activo=0
       cmp  [Activos+bx],0             ; stop=1
       jne  @@looping                  ;

Restaurar:                             ; Llamado desde arriba
                                       ; la rutina que se ejecuta
                                       ; solo la primera vez.

       mov  cl,[Turno]                 ;
       dec  cl                         ; -1 ( de 0..3)
       shl  cl,1                       ; Multiplica por 2
       xor  ch,ch                      ;

       lea  bx,[OffPrg1]               ; Offset del programa1
       add  bx,cx                      ; le suma el turno

       mov  ax,[bx]                    ; Donde empieza el programa 1

       cli
       mov  sp,ax                      ; ss:sp -> old envi del prog
       mov  ss,[ThisCS]                ; ss (donde esta la info)
       sti

       pop  [oldip]                    ; Old ip
       pop  [oldcs]                    ; Old cs
       pop  [Oldsp]                    ; Old sp
       pop  [Oldss]                    ; Old ss
       pop  gs
       pop  fs
       pop  es
       pop  ds                         ; a partir de aca usar como ref cs:
       pop  [cs:oldflags]
       popad                           ; En teoria todo resuelto

;;;
;;;    Bien en teoria aca tengo todo lo nuevo. Veamos como sigue
;;;
       mov  [cs:Temp],ax               ; Restaura Ax xq se va a usar
       mov  [cs:Temp2],bp              ; Restaura Bp xq se va a usar
       cli
       mov  sp,[cs:Oldsp]              ; Sp del programa Nuevo
       mov  ss,[cs:Oldss]              ; ss del programa Nuevo
       sti
       mov  ax,[cs:oldip]
       mov  bp,sp
       mov  [bp],ax                    ; cs:ip del programa nuevo
       mov  ax,[cs:oldcs]
       mov  [bp+2],ax
       mov  ax,[cs:oldflags]
       mov  [bp+4],ax
       mov  ax,[cs:Temp]               ; Viejo ax
       mov  bp,[cs:Temp2]              ; Viejo bp

       mov  [cs:estoyactive],0
       jmp  [cs:old08h]                ; Salta a la rutina original
ENDP


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      SetEnvi
;      Esta rutina se encarga de setear los estados iniciales de los procesos
;      IN:
;           si: ip     bx= cs
;           di: sp     ss= This cs
;           dx: Default para los demas valores.
;           cx: a que proceso le seteo
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   SetEnvi

       pusha

       dec  cx                         ; -1 ( de 0..3)
       shl  cx,1                       ; Multiplica por 2

       lea  ax,[OffPrg1]               ; Offset del programa1
       add  ax,cx                      ; le suma el turno

       push bx
       mov  bx,ax
       mov  ax,[bx]                    ; Donde empieza el programa 1
       add  ax,SIZE Ambiente           ; de verd�
       pop  bx

       cli
       mov  [oldss],ss                 ; y segmento actuales
       mov  [oldsp],sp                 ; Guarda

       mov  ss,[ThisCS]                ;
       mov  sp,ax                      ;
       sti

       push edx                        ; eax  Ŀ
       push edx                        ; edx   �
       push edx                        ; ecx   �
       push edx                        ; ebx   � = pushad
       push edx                        ; esp   �
       push edx                        ; ebp   �
       push edx                        ; esi   �
       push edx                        ; edi  ��
       push [oldflags]                 ; del programa ejecutado
       push bx                         ; ds
       push bx                         ; es
       push bx                         ; fs
       push bx                         ; gs
       push cs                         ; ss
       push di                         ; sp
       push bx                         ; cs
       push si                         ; ip

       cli
       mov  sp,[Oldsp]                 ; donde estaba. ss:sp ) - Old sp
       mov  ss,[Oldss]                 ;
       sti

       popa
       ret
ENDP


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      NEW2FH - Handler de la interrupcion 2Fh
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   NEW2FH
       jmp  short @@aca
       db   'T2'                       ; Flag para desinstalarlo
@@aca:
       cli
       cmp  ax,'TI'                    ; Codigo para ver si ya estoy residente
       jne  @@salir
       cmp  bx,'TO'
       jne  @@salir
       cmp  cx,'02'
       je   Estoy_residente            ; Ya estoy residente
       or   dx,dx                      ; dx=0
       je   @@flagactive
       cmp  dx,1
       je   @@allactive
       cmp  dx,2
       jne  @@salir

       jmp  new08h                     ; Debugging...

@@allactive:                           ; Este func se ejecuta justo antes
       mov  [cs:primer],0
       mov  [cs:estoyactive],0         ; Activate fiera.
       jmp  @@salir2

@@flagactive:
       mov  bx,cx
       lea  bx,[Activos+bx]
       mov  [cs:bx],1                  ; Programa X terminado
       jmp  @@salir2

@@salir:
       Jmp  [cs:old2fh]                ; Original int 2f

Estoy_residente:
       xchg ax,bx                      ; 'OTIT'
       mov  cx,THISVERSION
@@salir2:
       sti
       iret
ENDP


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      SEGMENTO DE DATA I - USADOS POR LA PARTE RESIDENTE
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
old08h dd 0                            ; Original interrupt 08h - Timer
old2fh dd 0                            ; Original interrupt 2fh - Multiplexer
primer db 0                            ; 0=No fue ejecutado, 1=Si.
turno  db ?                            ; De quien es el turno
ticks  db ?                            ; Tiempo ejecutado
Tiempo db 18                           ; Default ( cada 1 segundo )

temp   dw ?
temp2  dw ?
Thiscs dw ?                            ; Este cs.
Thisss dw ?                            ; Mi ss
Thissp dw ?                            ; Mi sp
Oldss  dw ?                            ; ss
Oldsp  dw ?                            ; sp
oldcs  dw ?                            ; cs
oldip  dw ?                            ; ip
oldflags dw ?                          ; Flags.

estoyactive db 0

activos     db 0                       ; Cada proceso tiene
            db 0                       ; un estado
            db 0                       ; Si es 0 es running
            db 0                       ; si no esta terminado

offprg1 dw OFFSET Programa1            ; Offsets a las variables
offprg2 dw OFFSET Programa2            ; de los programas.
offprg3 dw OFFSET Programa3
offprg4 dw OFFSET Programa4

label Programa1 Ambiente               ;
       db   SIZE Ambiente dup(?)       ;
label Programa2 Ambiente               ;
       db   SIZE Ambiente dup(?)
label Programa3 Ambiente
       db   SIZE Ambiente dup(?)
label Programa4 Ambiente
       db   SIZE Ambiente dup(?)
Label  ProgH Programa
       db   SIZE Programa dup(?)

;;;
;;; El comienzo de la pila es al final de lo que tiene asignado
;;;
offstk1 dw OFFSET Stack1               ; Offsets a los stacks
offstk2 dw OFFSET Stack2
offstk3 dw OFFSET Stack3
offstk4 dw OFFSET Stack4
       db   STACKLEN dup(?)            ; Estas son las pilas
label stack1 byte                      ; de los 4 programas.
       db   STACKLEN dup(?)
label stack2 byte
       db   STACKLEN dup(?)
label stack3 byte
       db   STACKLEN dup(?)
label stack4 byte

       db   STACKLEN dup(?)            ; Esta es la pila que usa este programa
label  MyStack byte

LABEL HastaAca byte                    ; Hasta aca voy a dejar residente.

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;=-=                                                                        -=-;
;=-=     ACA TERMINA LA PARTE RESIDENTE Y EMPIEZA LA NORMAL                 -=-;
;=-=                                                                        -=-;
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Parametros
; Chequea que los parametros de la linea sean correctos
; CS:81 -> empieza.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Parametros
                                       ; ES=DS=CS
       cld                             ; Clear Direction flag
       mov  si,81h                     ; ds:si (a partir de 81h estan los
                                       ; parametros )
@@loop:
       lodsb                           ; al <- ds:si ( si++)
       cmp  al,20h                     ; es SPACE ?
       je   @@loop                     ; si, entonces lee el siguiente.
       cmp  al,13                      ; es ENTER ?
       je   @@salir                    ; si
       cmp  al,'/'                     ; es '/' ?
       je   @@param                    ; si.
       cmp  al,'-'                     ; es '-' ?
       je   @@param                    ; si.
       jne  @@help                     ; entonces es error

@@salir:
       ret

@@UnInstall:
       Jmp  Uninstall                  ; Desistalame ( y termina con todo )

@@param:
       lodsb
       cmp  al,20h                     ; es espacio.
       je   @@help                     ; Si hay error en linea de comando.
       and  al,11011111b               ; minusculas/MAYUSCULAS
       cmp  al,'U'
       je   @@UnInstall                ; a modo de ejemplo
       cmp  al,'T'                     ;
       je   @@Tiempo                   ; Tiempo que hay que usar
       jne  @@help                     ; si ir a ayuda.


@@help:                                ; hubo error en la linea de parametros
       lea  dx,[ErrorHelp]
       Call DisplayString

       EXITCODE 2                      ; Finaliza
@@Tiempo:
       lodsb                           ; Siguiente ( es un espacio )
       lodsb                           ;
       mov  ah,al                      ; primer byte
       lodsb                           ; ax tengo el tiempo
       sub  ax,3030h                   ;
       mov  bh,ah
       shl  ah,3                       ; Multiplico por 8
       shl  bh,1                       ; Multiplico por 2
       add  ah,bh
       add  al,ah                      ; Multiplique por 10.
       mov  [Tiempo],al                ; Tiempo de espera
       jmp  @@loop
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      UnInstall - Trata de sacar al programa de la memoria
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   UNINSTALL
       Call AmITSR                     ; Ante todo, estoy residente ?
       jnc  @@no                       ; No. Entonces que queres hacer ?

@@si:
       mov  ax,352fh
       int  21h
       cmp  [WORD PTR es:bx+2],'2T'
       jne  @@noseguir                 ; Sigo tratando de desinstalar

       mov  al,08h                     ; Int 08
       int  21h
       cmp  [WORD PTR es:bx+2],'2T'
       je   @@seguir

@@noseguir:
       lea  dx,[NoPuedoUninstall]      ; Habia otro residente antes
       jmp  @@no2

@@seguir:
       mov  ax,252fh                   ; Restaura la vieja handle
       mov  dx,[WORD PTR es:old2fh]    ; Saca esta info del prg residente
       mov  ds,[WORD PTR es:old2fh+2]  ;
       int  21h                        ; Set Handler

       mov  al,08h                     ;
       mov  dx,[WORD PTR es:old08h]    ; Saca esta info del prg residente
       mov  ds,[WORD PTR es:old08h+2]  ;
       int  21h                        ; Set Handler

       push cs                         ; Recordar que estoy en un com
       pop  ds                         ;

       mov  bx,es                      ; free PSP environment of TSR
       mov  es,[es:2ch]                ;
       mov  ah,49h                     ;
       int  21h                        ; Realse memory
       mov  es,bx                      ; free Resident code of TSR
       mov  ah,49h                     ;
       int  21h                        ; Release memory

       lea  dx,[TSRuninstalled]
       Call DisplayString
       EXITCODE 0
@@no:
       lea  dx,[NoEstoyTSRMSG]
@@no2:
       Call DisplayString
       EXITCODE 3
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      DisplayString
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   DisplayString
       mov  ah,9
       int  21h
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      AmITSR - Chequea si estoy residente. Me instala ( o no )
;      Carry On: Si estoy residente
;      Carry Off: No estoy residente
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   AMITSR
       mov  ax,'TI'
       mov  bx,'TO'
       mov  cx,'02'
       int  2fh                        ; estoy residente ?
       cmp  ax,'TO'
       jne  dejar_tsr
       cmp  bx,'TI'
       jne  dejar_tsr
       stc                             ; Si, no hace falta otra vez.
       ret

dejar_tsr:
       clc                             ; No - Entonces dejame TSR
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      INIT - Inicializa la memoria, mensajes, handlers, etc
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   INIT
       mov  ah,4ah
       lea  bx,[Final]
       add  bx,15
       shr  bx,4
       inc  bx
       int  21h                        ; Change memory size

       lea  dx,[CopyString]
       Call DisplayString              ; Muestra Comienzo.
       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      AlreadyTSR - Ya estaba residente, que hago ? nada.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   ALREADYTSR
       mov  dl,cl                      ; Todo esto decodifica el valor
       and  cl,15                      ; y lo transforma a leible.
       or   cl,30h
       mov  [prover+3],cl
       shr  dl,4
       or   dl,30h
       mov  [prover+2],dl
       or   ch,30h
       mov  [prover],ch                ; Version Number.

       lea  dx,[Otraveztsr]
       Call DisplayString

       EXITCODE 1                      ; Chau, ir al DOS.
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      MAIN - Programa Principal
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   MAIN
       Call Init
       Call Parametros                 ; Chequea los argumentos.
       Call AmITSR
       jnc  TSRplease                  ; Dejar TSR
       Jmp  AlreadyTSR                 ; Ya estoy residente.

TSRplease:                             ; Dejar TSR
       mov  ax,352fh                   ; Old int 2fh
       int  21h
       lea  si,[old2fh]
       mov  [si],bx
       mov  [si+2],es                  ; old int handler

       mov  al,08h                     ; Old int 08h
       int  21h
       lea  si,[old08h]
       mov  [si],bx
       mov  [si+2],es                  ; old int handler

       lea  dx,[new2fh]                ; New int 21h
       mov  ax,252fh
       int  21h                        ; ds:dx -> address of int handler

       lea  dx,[new08h]                ; New int 21h
       mov  al,08h                     ; ah=25
       int  21h                        ; ds:dx -> address of int handler

       mov  al,[Tiempo]
       mov  bl,10
       xor  ah,ah
       div  bl
       or   ax,3030h
       mov  [ttt],al
       mov  [ttt+1],ah

       lea  dx,[TSRinstalled]
       Call DisplaySTring              ;

       lea  dx,[HastaAca]              ; Calcula el tamano a dejar
       mov  ax,3100h                   ; residente. ( exit code 0 )
       mov  cl,4
       shr  dx,cl                      ; Divide por 16
       inc  dx                         ; por que la func 31h puede dejar tsr mucho
       int  21h                        ; Terminate and Stay Resident

ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
;      SEGMENTO DE DATA II - NO USADOS POR LA PARTE RESIDENTE
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
LABEL  ErrorHelp byte
db 13,10,'Formato: TiPro2 [opciones]',13,10,13,10
db 9,'Opciones:',13,10
db 9,'             Lo Instala (lo deja TSR)',13,10
db 9,'/?           Muestra esta pantalla',13,10
db 9,'/u           Desinstala el programa (lo saca de la memoria)',13,10
db 9,'/v           Muestra la version instalada',13,10
db 9,'/t nn        Tiempo que le dedica a cada proceso [Min=00,Max=99]',13,10
db 9,'             Default: 18 ( 1 seg por proceso aprox )',13,10
db 13,10
db 'Ejemplos: TiPro2 /t 00',13,10
db '          TiPro2 /t 50',13,10
db '          TiPro2 /u',13,10,'$'

LABEL CopyString byte                  ; Mensaje de comienzo
db 13,10,'Organizador del Computador II - Proyecto II v0.02',13,10
db 'Grupo: Tito II - Integrantes: Befi, Dani, Cati, Riq',13,10,'$'

LABEL  OtraVezTSR byte
db 13,10,'La version '
prover db'0.00 del TiPro2 ya esta residente.',13,10,'$'

LABEL  NoEstoyTSRMSG byte
db 13,10,'Disculpa, pero no estoy residente.',13,10,'$'

LABEL  TSRUninstalled byte
db 13,10,'TiPro2 desinstalado satisfactoriamente.',13,10,'$'

LABEL  NoPuedoUninstall byte
db 13,10,'Error: No se puede desinstalar. Trate de desinstalar primero otros TSR.',13,10,'$'

LABEL  TSRinstalled byte
db 13,10,'TiPro2 residente dandole '
ttt    db 0,0
       db ' ticks a cada proceso.',13,10,'$'

LABEL Final byte
END
