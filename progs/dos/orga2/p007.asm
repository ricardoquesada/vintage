COMMENT *
       Organizador del Computador II
       Proyecto N�mero 1
       Grupo: Tito 2
       Integrantes: Barea, Daniel
                    Cativa Tolosa, Sebasti�n
                    Quesada, Ricardo Calixto

       Version: 0.0.7
*

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
DOSSEG                                 ; Como se van a ordenar los segmentos
MODEL SMALL                            ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior
P387                                   ; con copro

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines y Includes ( m�s abajo hay otro include )
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
MAX_X  = 320
MAX_Y  = 200
INCLUDE 'struc.inc'                    ; Incluye los headers de las estructuras

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
; main()
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa
       Call InitMem                    ; Pone bien la memoria ( Modifica la
                                       ; memoria reservada de entrada )
       lea  dx,[InitString]
       Call DisplayString              ; Muestra Comienzo.
       Call Parametros                 ; Chequear los parametros.
@@siguiente:
       Call CargarArchivo              ; Carga archivo. Todo Bien ?
       jc   @@Salir                    ; No. Hubo Error -> Salir
       Call ValidFormat                ; Es de un formato v�lido ? ( pcx,etc...)
       jc   @@Siguiente                ; No. Hubo Error -> Siguiente archivo
       Call ViewGraphic                ; Mostrar el gr�fico en la pantalla
       jc   @@Siguiente                ; No lo pudo mostrar. Siguiente archivo
       Call BlancoNegro                ; Transforma a escala de grises

       push es
       mov  ah,49h
       mov  es,[memseg2]
       int  21h                        ; Libera mem.
       pop  es

       jmp  @@siguiente                ; ir al siguiente archivo
@@Salir:
       EXITCODE                        ; Finaliza

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Display String
; Input: dx = offset of string
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC DisplayString
       push ax
       mov  ah,9
       int  21h
       pop  ax
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; InitMem
; Por como maneja el DOS la memoria es necesario hacer esto
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC InitMem
       mov  bx,ss                      ; Compute distance between
       mov  ax,es                      ; PSP and stack
       sub  bx,ax

       mov  ax,sp
       add  ax,15
       mov  cl,4
       shr  ax,cl                      ; Divide por 16 ( Paragraphs )

       add  bx,ax

       mov  ah,4ah
       int  21h                        ; Modifica la Memoria reservada
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Parametros
; Chequea que los parametros de la linea sean correctos
; ES:0081 -> empieza.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Parametros
       push ds
       push es
       pusha

       push es                         ; es,ds=ds,es (Doble asignacion)
       push ds
       pop  es
       pop  ds

       cld                             ; Clear Direction flag
       lea  di,[nombtt]                ; nombre default.
       mov  si,81h                     ; ds:si (a partir de 81h estan los
                                       ; parametros )
@@loop:
       lodsb                           ; al <- ds:si ( si++)
       cmp  al,20h                     ; es SPACE ?
       je   @@loop                     ; si, entonces lee el siguiente.
       cmp  al,13                      ; es ENTER ?
       je   @@salir                    ; si
       cmp  al,'/'                     ; es '/' ?
       je   @@param                    ; si.
       jne  @@esnombre                 ; entonces es el nombre


@@param:
       lodsb
       cmp  al,20h                     ; es espacio.
       je   @@help                     ; Si hay error en linea de comando.
       and  al,11011111b               ; minusculas/MAYUSCULAS
       cmp  al,'A'
       je   @@salir                    ; a modo de ejemplo
       jne  @@help                     ; si ir a ayuda.

@@esnombre:                            ; poner el nombre que se quiere buscar.
       lea  di,[nombre]                ; es:di
@@loop2:
       stosb                           ; escribe en es:di
       lodsb
       cmp  al,20h                     ; se acabo el nombre ?
       je   @@loop                     ; volver al primer loop
       cmp  al,13                      ; es enter ?
       jne  @@loop2                    ; no, entonces seguir con el loop
                                       ; si, entonces salir.

@@salir:
       mov  al,0
       stosb                           ; es:di -> 0 (asciiz del nombre)

       popa
       pop  es
       pop  ds
       ret

@@help:                                ; hubo error en la linea de parametros
       popa
       pop  es
       pop  ds
       lea  dx,[ErrorHelp]
       Call DisplayString

       mov  al,1                       ; Error code=1
       EXITCODE                        ; Finaliza
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Cargar Archivo
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC CargarArchivo
       mov  ah,1ah
       lea  dx,[dtaaca]                ; Donde se va a almacenar la DTA
       int  21h                        ; Change DTA.

       mov  ah,4eh
       add  ah,[fnombre]               ; flag (0 la primer vez. Luego 1)
       mov  [fnombre],1                ; Indica si es Find First o Next
       mov  cx,63h                     ; All attributes
       lea  dx,[nombre]                ;
       int  21h                        ; Find (First,Next) Matching File
                                       ; (Para saber el largo)
                                       ; El largo me interesa para poder
                                       ; reservar memoria para cargar el
                                       ; archivo. Y esa info la obtengo de
                                       ; la DTA.
       jc   @@notok                    ; Hubo error.

       mov  ax,3d00h                   ; Open File. Mode Read Only
       lea  dx,[dtaaca.nombre]         ; ds:dx-> asciiz name
       int  21h                        ; Open it!
       jnc  @@ok
@@notok:
       lea  dx,[ErrorAbrir]
       Call DisplayString              ; Mostrar Error String
       stc
       ret
@@ok:
       mov  [fhandle],ax               ; Save File handle for future use.
       clc
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Valid Format
; Chequea si el archivo es .pcx o algun otro formato v�lido.
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ValidFormat
       pusha
       mov  bx,[fhandle]               ; File handle
       mov  cx,128                     ; 128 bytes
       mov  ah,3fh                     ; read
       lea  dx,[pcxaca]                ; DS:DX  -> buffer ( destination )
       int  21h                        ; Lee la cabecera del .pcx

       cmp  [pcxaca.manu],10           ; Manufacturer ID
       jne  ErrorVersion               ; OK ?
       cmp  [pcxaca.enc],1             ; Encoding version.
       je   @@ok                       ; Yes, entonces es PCX

ErrorEncode:
       lea  dx,[ErrorPCXEncode]
       Call DisplayString
       jmp  @@notok
ErrorVersion:
       lea  dx,[ErrorPCXVersion]
       Call DisplayString

@@notok:
       mov  bx,[fhandle]
       mov  ah,3eh                     ; Close File Handle
       int  21h                        ; Close it!
       stc                             ; set error flag (carry)
       jmp  @@salir                    ; retorna.

@@ok:
       Call Es256yFit                  ; Es un pcx que yo puedo ver ?
       jc   @@notok                    ; No. Entonces salir con error.
       clc                             ; Return with Carry Clear -> Ok

@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Es256yFit
; llamada por ValidFormat
; Chequea si el formato (que es v�lido), es soportado.
; OUT: Carry=off , lo soporta
;           =on  , no lo soporta
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Es256yFit
       pusha
       lea  bx,[pcxaca.wind]           ; Xmin,Ymin,Xman,Ymax
       mov  ax,[bx+4]
       sub  ax,[bx]                    ; Xtotal
       inc  ax
       mov  [realx],ax
       mov  cx,[bx+6]
       sub  cx,[bx+2]                  ; Ytotal
       inc  cx
       mov  [realy],cx

;       cmp  [realy],MAX_Y
;       ja   @@notok
;       cmp  [realx],MAX_X
;       jbe  @@ok

       cmp  [pcxaca.npla],1            ; Es de 256 colores ?
       je   @@ok                       ; Si, entonces todo bien.

@@notok:
       lea  dx,[ErrorPCXColor]
       Call DisplayString
       stc
       jmp  @@salir
@@ok:
       clc
@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ViewGraphic
; Muestra en pantalla el gr�fico.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ViewGraphic
       Call ReadPCX
       jc   @@notok
       Call DecodePCX                  ; Desguaza el PCX
       jc   @@notok
       lea  bx,[dac]
       mov  [offdac],bx                ; Usar esta paleta.
       Call ViewGraph                  ; Muestra finalmente el grafico
       jc   @@notok

       jmp  @@ok                       ; Si esta todo bien ir a @@ok

@@notok:
       mov bx,[fhandle]                ; Hubo error.
       mov ah,3eh                      ;
       int 21h                         ; Close file handle ( del archivo )

       stc
       jmp  @@salir
@@ok:
       push es                         ; Lo guardo, tal vez lo necesite.
       mov  ah,49h                     ; Liberar memoria reservada
       mov  ax,[memseg]
       mov  es,ax
       int  21h
       pop  es                         ; Listo!
       clc
@@salir:
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ReadPCX
; La encargada real de leer el archivo y ponerlo en memoria.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ReadPCX
       pusha
       push ds
       push es

       mov  ah,48h
       mov  ebx,[dtaaca.size]          ; Tama�o del archivo
       shr  ebx,4                      ; Divide por 16
       inc  ebx                        ; le suma 1
       int  21h                        ; Reverva memoria para cargar el archivo
       jnc  @@ok

       lea  dx,[ErrorNomem]
       Call DisplayString

       stc
       jmp  @@salir

@@ok:

       mov  ecx,[dtaaca.size]          ; Cantidad de datos a leer.
       sub  ecx,128                    ; la cabecera no ( ya esta leida )

       push ds
       pop  es                         ; ES=DS

       mov  [memseg],ax                ; Guarda el segmento.
       mov  ds,ax                      ; AX tiene el segmento de memoria

       push cx                         ; guardo esto!!!!
@@seguirleyendo:
       cmp  ecx,10000h                 ; es menor que 64k ?
       jb   @@esmenor                  ; si

       xor  cx,cx
       mov  bx,[es:fhandle]            ; File Handle
       sub  ecx,8000h                  ; le resto al size 32k
                                       ; cx=8000h

       xor  dx,dx                      ; offset = 0
       mov  ah,3fh                     ;
       int  21h                        ; Leo los primeros 32k (de 64)

                                       ; sigue cx=8000h . No varia
       mov  dx,8000h                   ; offset = 32k
       mov  ah,3fh
       int  21h                        ; leo otros 32k
       sub  ecx,8000h                  ; ecx=ecx-8000h -> cx=0
                                       ; En total a ecx le reste 64k
       mov  ax,ds
       add  ax,1000h
       mov  ds,ax                      ; incremento 64k ( al segmento )
       jmp  @@seguirleyendo

@@esmenor:
       pop  cx                         ; Me sirve por si es mayor que 64k.
       xor  dx,dx                      ; DS:DX -> Buffer
       mov  ah,3fh                     ; Read
       mov  bx,[es:fhandle]
       int  21h                        ; Finalmente leer. Pone todo en mem reservada

       clc

@@salir:
       pop  es                         ; Restaura segmentos usados.
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; DecodePCX
; Funcion que muestra en pantalla el pcx
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC DecodePCX
       pusha
       push ds
       push es

       mov  ax,[realx]
       mul  [realy]                    ; X * Y = Total de bytes = DX:AX
       mov  cx,4
@@loop:
       clc
       rcr  dx,1
       rcr  ax,1
       loop @@loop

       inc  ax                         ; Ahora en AX tengo el total de
                                       ; paragraph a reservar.
       mov  bx,ax                      ; bx=Numero de paragraph a reservar
       mov  ah,48h                     ; Reservar Memoria.
       int  21h                        ; Devuelve en ax el segmento.
       jnc  @@ok

       lea  dx,[ErrorNoMem]            ; Mostrar mensaje de que falta
       Call DisplayString              ; Memoria.
       stc
       jmp  @@salir                    ; Volver con error

@@ok:
       mov  [MemSeg2],ax               ; Guarda el segemento de memoria.
       mov  es,ax                      ; ES tiene el segmento reservado.

       cld                             ; Clear Direction Flag

       cmp  [pcxaca.ver],5
       jne  @@here1

       call putdac                     ; Modifica la paleta (viene con paleta)

@@here1:
       mov  cx,[realy]
       mov  [uny],cx                   ; Esto es una copia del realy (alto del grafico)
       mov  cx,[realx]                 ; Number of times ( Ancho del grafico )

       xor  di,di                      ; ES:DI  (ES tiene el segmento de mem
                                       ; reservado.Va a tener el grafico)
       push ds
       pop  fs                         ; fs=ds

       mov  ax,[memseg]                ; (Seg reservado.Tiene lo leido del archivo)
       mov  ds,ax                      ; DS cambia de valor
       xor  si,si                      ; DS:SI

mainunpacking:

       cmp  di,0f000h                  ; Codigo que soporta varios segementos
       jb   @@aca1                     ; Es menor entonces no modifico
       mov  ax,es
       add  ax,0f00h
       mov  es,ax                      ; Suma al segmento 0f00h
       sub  di,0f000h                  ; y le resta al offset f000h
@@aca1:
       cmp  si,0f000h                  ; Lo mismo con el otro.
       jb   @@aca2                     ;
       mov  ax,ds
       add  ax,0f00h
       mov  ds,ax                      ; Suma al segmento 0f00h
       sub  si,0f000h                  ; y le resta al offset f000h

@@aca2:
       lodsb                           ; al = ds:si / si = si + 1
       mov  ah,al                      ; 2 hi bits.
       and  ah,192                     ; Encodeado ?
       cmp  ah,192                     ; Encodeado ?
       je   coded                      ; parece que si
       stosb                           ; es:di = al / di = di + 1

       dec cx
       jcxz newline
       jmp  mainunpacking

coded:
       mov  ah,al                      ;
       and  ah,63                      ; Los otros valores (ah=number of times)
       lodsb                           ; al = ds:si / si = si + 1
codedhere:
       stosb                           ; es:di = al / di = di + 1
       dec  cx
       dec  ah                         ; Sigue en encode ?
       jne  codedhere                  ; si, entonces repetir valor.
       jcxz newline                    ; Salta si cx=0. Esta linea X lista.
       jmp  mainunpacking
newline:
       mov  cx,[fs:realx]
       dec  [fs:uny]
       cmp  [fs:uny],0
       jne  mainunpacking

       clc
@@salir:
       pop  es
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ViewGraph
; Esta es la funcion que realmente pone el grafico en la pantalla
; Source: [memseg2]:0000
; Destin: a000:0000
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ViewGraph
       pusha
       push ds
       push es

       push ds
       pop  fs                         ; fs=ds

       mov  ax,0a000h
       mov  es,ax
       mov  ax,[memseg2]
       mov  ds,ax
       xor  di,di                      ;
       xor  si,si                      ; ds:si -> es:di

       mov  ax,13h
       int  10h                        ; Pone Modo grafico 320x200x256

       Call ChangeDac                  ; Modifica la paleta

       mov  cx,[fs:realx]              ; Chequea el valor a sumar para que entre
       sub  cx,MAX_X                   ; en pantalla el dibujo.
       jns  @@mayor                    ; Si hay no signo ( CX => 320)
       neg  cx                         ; Niega CX
@@mayor:
       mov  [fs:addx],cx               ; Diferencia = 320 - AnchoX

       cld
       xor  ax,ax                      ; Contador de X
       xor  bx,bx                      ; Contador de Y
@@loop:
       cmp  si,0f000h                  ; Lo mismo con el otro.
       jb   @@aca2                     ;
       mov  dx,ds
       add  dx,0f00h
       mov  ds,dx                      ; Suma al segmento 0f00h
       sub  si,0f000h                  ; y le resta al offset f000h

@@aca2:
       movsb                           ; ds:si -> es:di
       inc  ax
       cmp  ax,MAX_X
       je   @@xmayor                   ; X es mayor
       cmp  ax,[fs:realx]
       jne  @@loop                     ; X es menor ? no

       add  di,[fs:addx]               ; Xmenor
       jmp  @@contloop
@@xmayor:
       add  si,[fs:addx]               ; Xmayor
@@contloop:
       xor  ax,ax                      ; Resetea AX
       inc  bx
       cmp  bx,MAX_Y                   ; Es fin o algo parecido ?
       je   @@seguir                   ; si
       cmp  bx,[fs:realy]              ; ?
       jne  @@loop                     ; no

@@seguir:

       mov  ah,8
       int  21h                        ; Get Key

       mov  ax,3h                      ; Pone modo texto
       int  10h

       pop  es
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; PutDAC
; Guarda los 256 colores de la paleta
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC PutDAC
       push ds
       push es

       push ds
       pop  es                         ; es=ds

       mov  [tienedac],0               ; No tiene DAC. Default

       cld
       mov  ebx,[dtaaca.size]
       sub  ebx,897                    ; 897 = 769 + 128
       mov  ax,[memseg]
@@aca:
       cmp  ebx,10000h                 ; es mas que 64k ?
       jb   @@seguir
       sub  ebx,10000h
       add  ax,1000h                   ; suma el segmento.
       jmp  @@aca

@@seguir:
       mov  [es:tienedac],1            ; Si tiene dac!
       mov  ds,ax
       cmp  [byte ptr bx],12
       jne  @@salir

       inc  bx
       mov  si,bx
       lea  di,[dac]                   ; es:di
       mov  cx,768
@@loop:
       lodsb
       shr  al,2                       ; Divide por 4 la paleta.
       stosb
       loop @@loop

@@salir:
       pop  es
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ChangeDAC
; [dac]:0000/256 * 3 -> dac
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ChangeDac
       pusha
       push ds
       push si
       mov  ax,@data
       mov  ds,ax

       cmp  [tienedac],1
       jne  @@salir

       cld
       xor  al,al
       mov  dx,3c8h
       out  dx,al
       inc  dx
       mov  cx,768
       mov  si,[offdac]
repne  outsb                           ; ds:si -> va al port cx veces

@@salir:
       pop  si
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 1: Smoothering
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Smooth
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 2: Blur
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Blur
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 3:
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Efecto3
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; BlancoNegro
; Esta es la rutina que transforma a blanco y a negro.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC BlancoNegro
       push ds
       push es
       pusha

       push ds
       pop  es

       mov  ax,[realx]
       mul  [realy]                    ; X * Y = Total de bytes = DX:AX

       mov  bx,[memseg2]               ; Ax tiene el segemento del dibujo bien.
       mov  ds,bx
       xor  si,si                      ; ds:si -> dibujo

@@loop:
       push dx
       push ax                         ; X * Y. Lo guardo.

       mov  al,3
       mul  [byte ptr si]              ; [si](color) * 3.
       mov  di,ax
       lea  di,[dac+di]                ; offset lo tiene  [TablaDac + OFFset ]
       mov  al,[es:di]                 ; Red
       mov  bl,[es:di+1]               ; Green
       mov  cl,[es:di+2]               ; Blue
       xor  ah,ah
       xor  bh,bh
       xor  ch,ch                      ; Partes altas=0
       Call Luminancia                 ; Covierte un byte a blanco y negro.
       mov  [si],dl

       pop  ax
       pop  dx
       or   dx,dx                      ; Parte alta=0 ?
       jne  @@seguir
       cmp  si,ax                      ; Listo. Todo Convert ?
       jne  @@aca
       je   @@salir

@@seguir:
       cmp  si,0f000h
       jne  @@aca
       mov  bx,ds
       add  bx,0f00h
       mov  ds,bx
       sub  si,0f000h

@@aca:
       inc  si
       jmp  @@loop

@@salir:
       popa
       pop  es
       pop  ds

       lea  bx,[DacBN]
       mov  [offdac],bx
       Call ViewGraph

       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto Luminancia
; dx = ax * 0,3 (Rojo) + bx * 0,59 (Verde) + cx * 0,11 (Azul)
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Luminancia
       push ds

       push ax
       mov  ax,@DATA
       mov  ds,ax
       pop  ax

;       finit                           ; Inicializa Copro

       mov  [temp],ax                  ; Rojo
       fild [temp]                     ; Carga el valor entero a st
       fmul [dato030]                  ; Multiplica ax * 0,30
       fstp [suma]                     ; y lo suma y pop
       mov  [temp],bx                  ; Verde
       fild [temp]
       fmul [dato059]                  ; multiplica bx * 0,59
       fadd [suma]                     ; lo suma a lo anterior
       fstp [suma]                     ; y lo guarda y pop
       mov  [temp],cx                  ; Azul
       fild [temp]
       fmul [dato011]                  ; Multiplica cx * 0,11
       fadd [suma]                     ; lo suma a lo anterior
       fst  [suma]                     ; y lo guarda
       fistp [sumado]                  ; y lo guarda tambien como entero y pop
       mov  dx,[sumado]                ; devuelve lo sumado

       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de datos
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
DATASEG
; String
LABEL InitString byte                  ; Mensaje de comienzo
db 13,10,'Organizador del Computador II - Proyecto I ',13,10
db 'Grupo: Tito 2 ( Barea, Cativa Tolosa, Quesada )',13,10,'$'
LABEL ErrorAbrir byte                 ; Mensaje de Error
db 13,10,'Error: No se pudo abrir el archivo',13,10,'$'
LABEL ErrorPCXVersion byte             ; por ahora los 2 apuntan al mismo.
LABEL ErrorPCXEncode  byte
db 13,10,'Error: Este no es un archivo .pcx',13,10,'$'
LABEL ErrorPCXColor  byte
db 13,10,'Error: Este archivo .pcx no es de 256 colores',13,10,'$'
LABEL ErrorNoMem  byte
db 13,10,'Error: Necesita m�s memoria.',13,10,'$'
LABEL ErrorHelp  byte
db 13,10,'Formato: Orga2 [/? | filespec]',13,10,13,10
db 9,'/?           Muestra esta pantalla',13,10
db 9,'Filespec     Puede ir el nombre de un archivo con Wildcards (*,?,etc)',13,10
db 9,'             Default: TITO2.PCX',13,10
db 13,10
db 'Ejemplos: Orga2 *.pcx',13,10
db '          Orga2 dibu??.*',13,10,'$'

fnombre     db 0                       ; Primer nombre ( 0=Find First 1=Find Next)
dato011 dq  0.11                       ; usado por luminancia
dato059 dq  0.59                       ; usado por luminancia
dato030 dq  0.3                        ; usado por luminancia

nombre db   'tito2.pcx'                ; este es el nombre default de Cargar Archivo.
nombtt db    40 dup(?)                 ; Reservo mas por las dudas.

INCLUDE 'paleta.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Variables no inicializadas
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
temp   dw   ?                          ; usado por luminancia
sumado dw   ?                          ; usado por luminancia
suma   dq   ?                          ; usado por luminancia
fhandle dw  ?                          ; File handle ( por funciones de archivos )
realx  dw   ?                          ; lo que ocupa de en serio el pcx
realy  dw   ?                          ; usado por Is256yFit
uny    dw   ?                          ; Usados por Decode PCX
addx   dw   ?                          ;    "    "  ViewGraph
memseg dw   ?                          ; Segmento de memoria reservado.
memseg2 dw  ?                          ; Otro segmento. Usado por DecodePCX

tienedac db ?                          ; Flag para ver si tiene dac
dac    db   256 * 3 dup(?)             ; Almacena los valores de la DAC
offdac dw   ?                          ; Offset de las dac

LABEL pcxaca pcxh                      ; Define Structura!
       db   128 dup(?)                 ; Donde ir� la cabecera del .pcx
LABEL dtaaca dtah                      ; Define Structura!
       db   43 dup(?)                  ; Donde va a ir la DTA temporaria

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Stack
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STACK 200H

END
