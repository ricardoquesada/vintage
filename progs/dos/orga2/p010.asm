COMMENT *
       Organizador del Computador II
       Proyecto N�mero 1
       Grupo: Tito 2
       Integrantes: Barea, Daniel
                    Cativa Tolosa, Sebasti�n
                    Quesada, Ricardo Calixto

       Version: 0.0.10
*

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Simplified directives
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
WARN                                   ; Warn All
MULTERRS                               ; Multiple Errors per line
IDEAL                                  ; Entrar en modo IDEAL de TASM
DOSSEG                                 ; Como se van a ordenar los segmentos
MODEL SMALL                            ; Tipo de modelo
LOCALS                                 ; Permite usar saltos locales (@@)
JUMPS                                  ; Automatiza los saltos por si me paso
P386                                   ; 386 o superior
P387                                   ; con copro

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Defines y Includes ( m�s abajo hay otro include )
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
MAX_X  = 320
MAX_Y  = 200
INCLUDE 'struc.inc'                    ; Incluye los headers de las estructuras

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de codigo
; main()
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
CODESEG
       STARTUPCODE                     ; Inicializa
       Call Init                       ; Inicializa ( pone valores bien, etc)
       Call Parametros                 ; Chequear los parametros.
       lea  dx,[Mostrar]
       Call DisplayString              ; Muestra la barra
@@loop:
       Call CargarArchivo              ; Carga archivo. Todo Bien ?
       jc   @@Salir                    ; No. Hubo Error -> Salir
       Call ValidFormat                ; Es de un formato v�lido ? ( pcx,etc...)
       jc   @@Siguiente                ; No. Hubo Error -> Siguiente archivo
       Call ViewGraphic                ; Mostrar el gr�fico en la pantalla
       jc   @@Siguiente                ; No lo pudo mostrar. Siguiente archivo
       Call BlancoNegro                ; Transforma a escala de grises
       Call Smoothering                ; Transforma a efecto 1

@@siguiente:
       Call LiberarMem
       jmp  @@loop                     ; ir al siguiente archivo

@@Salir:
       Call RestInit                   ; Libera cualquier tipo de memoria.

       EXITCODE                        ; Finaliza

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; showit
; Funcion que pone en patalla lo que esta es ds:si ( asciiz )
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Showit
       pusha
@@loop:
       mov  ah,2
       mov  dl,[si]
       or   dl,dl
       je   @@salir
       int  21h
       inc  si
       jmp  @@loop

@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Tochar
; Funcion que convierte de binario a ascii imprimible
; si = Donde pongo el numero convertido
; ax = numero ( Toma todos los numeros como positivos )
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Tochar
       pusha
       push ds
       push si

       mov  bx,@DATA
       mov  ds,bx

       mov [WORD PTR si],2020h         ; llena con espacios
       mov [WORD PTR si+2],3020h       ; 3 en total

       add  si,4                       ; empieza por el de mas a la derecha
       xor  dx,dx
       mov  bx,10
@@loop:
       or   ax,ax                      ; ax ( el numero a conv ) es 0 ?
       je   @@salir                    ; si. Listo
       dec  si                         ; ir al numero de la izq.
       div  bx                         ; divide por 10
       or   dl,'0'                     ; le sumo 30h
       mov [si],dl                     ; lo pongo
       xor dx,dx
       jmp  @@loop

@@salir:
       pop  si
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; NewColumn
; Funcion que me posciona en una nueva columna
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC NewColumn
       pusha
       mov  al,dl
       mov  ah,3
       xor  bh,bh
       int  10h
       mov  dl,al
       dec  ah
       int  10h
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Display String
; Input: dx = offset of string
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC DisplayString
       push ax
       mov  ah,9
       int  21h
       pop  ax
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Init
; Esta funcion inicializa la memoria, y reserva memoria y otras cosas.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Init
       Call InitMem                    ; Pone bien la memoria ( Modifica la
                                       ; memoria reservada de entrada )
       lea  dx,[InitString]
       Call DisplayString              ; Muestra Comienzo.

       mov  ah,48h
       mov  bx,4000 / 16 + 1           ; 4000 = 80x25x2
       int  21h
       mov  [memvid],ax                ; Guarda la memoria de la pantalla.

       mov  ah,1ah
       lea  dx,[dtaaca]                ; Donde se va a almacenar la DTA
       int  21h                        ; Change DTA.

       finit                           ; Inicializa Copro

       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; LiberarMem
; Libera memoria y otras
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC LiberarMem
       pusha

       mov  ah,49h
       mov  bx,[memseg]                ; Donde esta el archivo encodeado
       mov  es,bx
       int  21h                        ; Libera memoria

       mov  ah,49h
       mov  bx,[memseg2]               ; Donde esta el archivo desencodeado
       mov  es,bx
       int  21h                        ; Libera memoria

       mov  ah,3eh                     ; Cerrar File Handle
       mov  bx,[fhandle]
       int  21h

       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; RestInit
; Libera memoria y otras
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC RestInit
       mov  ah,49h
       mov  bx,[memvid]
       mov  es,bx
       int  21h                        ; Libera memoria

       finit                           ; Reset Copro
       
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Savevid
; Funcion que salva el contenido de la pantalla de textos en mem
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Savevid
       pusha
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax

       mov  ah,3
       xor  bh,bh
       int  10h
       mov  [cursorpos],dx

       mov  ax,[memvid]
       mov  es,ax
       xor  di,di                      ; ES:DI
       mov  ax,0b800h                  ; Memoria de video de texto b8000
       mov  ds,ax
       xor  si,si                      ; DS:SI
       cld
       mov  cx,2000
repne  movsw                           ; mueve 2000 words ( 4000 bytes )

       pop es
       pop ds
       popa
       ret
ENDP


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Restorevid
; Restaura el contenido de la pantalla
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC   Restorevid
       pusha
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax

       mov  ah,2
       xor  bh,bh
       mov  dx,[cursorpos]
       int  10h                        ; Restore cursor position

       mov  ax,[memvid]
       mov  ds,ax
       xor  si,si                      ; DS:SI
       mov  ax,0b800h
       mov  es,ax
       xor  di,di                      ; ES:DI
       cld
       mov  cx,2000
repne  movsw                           ; mueve 2000 words ( 4000 bytes )

       pop es
       pop ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; InitMem
; Por como maneja el DOS la memoria es necesario hacer esto
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC InitMem
       mov  bx,ss                      ; Compute distance between
       mov  ax,es                      ; PSP and stack
       sub  bx,ax

       mov  ax,sp
       add  ax,15
       mov  cl,4
       shr  ax,cl                      ; Divide por 16 ( Paragraphs )

       add  bx,ax

       mov  ah,4ah
       int  21h                        ; Modifica la Memoria reservada
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Parametros
; Chequea que los parametros de la linea sean correctos
; ES:0081 -> empieza.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Parametros
       push ds
       push es
       pusha

       push es                         ; es,ds=ds,es (Doble asignacion)
       push ds
       pop  es
       pop  ds

       cld                             ; Clear Direction flag
       lea  di,[nombtt]                ; nombre default.
       mov  si,81h                     ; ds:si (a partir de 81h estan los
                                       ; parametros )
@@loop:
       lodsb                           ; al <- ds:si ( si++)
       cmp  al,20h                     ; es SPACE ?
       je   @@loop                     ; si, entonces lee el siguiente.
       cmp  al,13                      ; es ENTER ?
       je   @@salir                    ; si
       cmp  al,'/'                     ; es '/' ?
       je   @@param                    ; si.
       jne  @@esnombre                 ; entonces es el nombre


@@param:
       lodsb
       cmp  al,20h                     ; es espacio.
       je   @@help                     ; Si hay error en linea de comando.
       and  al,11011111b               ; minusculas/MAYUSCULAS
       cmp  al,'A'
       je   @@salir                    ; a modo de ejemplo
       jne  @@help                     ; si ir a ayuda.

@@esnombre:                            ; poner el nombre que se quiere buscar.
       lea  di,[nombre]                ; es:di
@@loop2:
       stosb                           ; escribe en es:di
       lodsb
       cmp  al,20h                     ; se acabo el nombre ?
       je   @@loop                     ; volver al primer loop
       cmp  al,13                      ; es enter ?
       jne  @@loop2                    ; no, entonces seguir con el loop
                                       ; si, entonces salir.

@@salir:
       mov  al,0
       stosb                           ; es:di -> 0 (asciiz del nombre)
       mov  al,'$'                     ; para mostrar.
       stosb

       popa
       pop  es
       pop  ds
       ret

@@help:                                ; hubo error en la linea de parametros
       popa
       pop  es
       pop  ds
       lea  dx,[ErrorHelp]
       Call DisplayString

       mov  al,1                       ; Error code=1
       EXITCODE                        ; Finaliza
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Cargar Archivo
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC CargarArchivo
       lea  dx,[Intro]
       Call DisplayString              ; Mostrar un Enter

       mov  ah,4eh                     ; 4eh = Find First. 4fh= Find Next
       add  ah,[fnombre]               ; flag (0 la primer vez. Luego 1)
       mov  bh,ah                      ; lo necesito para saber el error
       mov  [fnombre],1                ; Indica si es Find First o Next
       mov  cx,63h                     ; All attributes
       lea  dx,[nombre]                ; ds:dx (asciiz)
       int  21h                        ; Find (First,Next) Matching File
                                       ; (Para saber el largo)
                                       ; El largo me interesa para poder
                                       ; reservar memoria para cargar el
                                       ; archivo. Y esa info la obtengo de
                                       ; la DTA.
       jc   @@notok                    ; Hubo error.

       lea  si,[dtaaca.nombre]         ; Que es lo que voy a mostrar.
       Call Showit                     ; Muestro el nombre del archivo encontrado

       mov  ax,3d00h                   ; Open File. Mode Read Only
       lea  dx,[dtaaca.nombre]         ; ds:dx-> asciiz name
       int  21h                        ; Open it!
       jnc  @@ok
@@notok:
       cmp  bh,4fh                     ; Si es findNext no es error
       je   @@aca                      ; porque no encontro al archivo.

       lea  dx,[nombre]
       Call DisplayString              ; Muestra el nombre con wildcard

       mov  dl,35
       Call NewColumn                  ; Nueva Columna
       lea  dx,[ErrorAbrir]
       Call DisplayString              ; Mostrar Error String
@@aca:
       stc
       ret
@@ok:
       mov  [fhandle],ax               ; Save File handle for future use.
       clc
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Valid Format
; Chequea si el archivo es .pcx o algun otro formato v�lido.
; Return: Carry = 0 : no erros
;               = 1 : errors
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ValidFormat
       pusha
       mov  bx,[fhandle]               ; File handle
       mov  cx,128                     ; 128 bytes
       mov  ah,3fh                     ; read
       lea  dx,[pcxaca]                ; DS:DX  -> buffer ( destination )
       int  21h                        ; Lee la cabecera del .pcx

       cmp  [pcxaca.manu],10           ; Manufacturer ID
       jne  ErrorVersion               ; OK ?
       cmp  [pcxaca.enc],1             ; Encoding version.
       je   @@ok                       ; Yes, entonces es PCX

ErrorEncode:
       mov  dl,35
       Call NewColumn                  ; Nueva Columna
       lea  dx,[ErrorPCXEncode]
       Call DisplayString
       jmp  @@notok
ErrorVersion:
       mov  dl,35
       Call NewColumn                  ; Nueva Columna
       lea  dx,[ErrorPCXVersion]
       Call DisplayString

@@notok:
       stc                             ; set error flag (carry)
       jmp  @@salir                    ; retorna.

@@ok:
       Call VerSize                    ; Chequea Tamano y Color(256)
       jc   @@notok                    ; No. Entonces salir con error.

       clc                             ; Return with Carry Clear -> Ok

@@salir:
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; VerSize
; llamada por ValidFormat
; Averigua el tamanio del grafico
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC VerSize
       pusha
       lea  bx,[pcxaca.wind]           ; Xmin,Ymin,Xman,Ymax
       mov  ax,[bx+4]
       sub  ax,[bx]                    ; Xtotal
       inc  ax
       mov  [realx],ax
       mov  cx,[bx+6]
       sub  cx,[bx+2]                  ; Ytotal
       inc  cx
       mov  [realy],cx

       lea  si,[valor1]
       mov  ax,[realx]
       Call ToChar
       lea  si,[valor2]
       mov  ax,[realy]
       Call ToChar
       mov  dl,15
       Call NewColumn
       lea  dx,[valor1]
       Call DisplayString              ; Muestra en pantalla el tamano

       cmp  [pcxaca.npla],1            ; Es de 256 colores ?
       je   @@ok                       ; Si, entonces todo bien.

@@notok:
       mov  dl,35
       Call NewColumn
       lea  dx,[ErrorPCXColor]
       Call DisplayString
       stc
       jmp  @@salir
@@ok:
       clc
@@salir:

       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ViewGraphic
; Muestra en pantalla el gr�fico.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ViewGraphic
       Call ReadPCX
       jc   @@notok
       Call DecodePCX                  ; Desguaza el PCX
       jc   @@notok

       lea  bx,[dac]
       mov  [offdac],bx                ; Usar esta paleta.
       Call ViewGraph                  ; Muestra finalmente el grafico

       mov  dl,35
       Call NewColumn
       lea  dx,[leyendo]
       Call DisplayString

       jmp  @@ok                       ; Si esta todo bien ir a @@ok

@@notok:
       stc
       jmp  @@salir
@@ok:
       clc
@@salir:
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ReadPCX
; La encargada real de leer el archivo y ponerlo en memoria.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ReadPCX
       pusha
       push ds
       push es

       mov  ah,48h
       mov  ebx,[dtaaca.size]          ; Tama�o del archivo
       shr  ebx,4                      ; Divide por 16
       inc  ebx                        ; le suma 1
       int  21h                        ; Reverva memoria para cargar el archivo
       jnc  @@ok

       mov  dl,35
       Call NewColumn                  ; Nueva Columna
       lea  dx,[ErrorNomem]
       Call DisplayString

       stc
       jmp  @@salir

@@ok:

       mov  ecx,[dtaaca.size]          ; Cantidad de datos a leer.
       sub  ecx,128                    ; la cabecera no ( ya esta leida )

       push ds
       pop  es                         ; ES=DS

       mov  [memseg],ax                ; Guarda el segmento.
       mov  ds,ax                      ; AX tiene el segmento de memoria

       push cx                         ; guardo esto!!!!
@@seguirleyendo:
       cmp  ecx,10000h                 ; es menor que 64k ?
       jb   @@esmenor                  ; si

       xor  cx,cx
       mov  bx,[es:fhandle]            ; File Handle
       sub  ecx,8000h                  ; le resto al size 32k
                                       ; cx=8000h

       xor  dx,dx                      ; offset = 0
       mov  ah,3fh                     ;
       int  21h                        ; Leo los primeros 32k (de 64)

                                       ; sigue cx=8000h . No varia
       mov  dx,8000h                   ; offset = 32k
       mov  ah,3fh
       int  21h                        ; leo otros 32k
       sub  ecx,8000h                  ; ecx=ecx-8000h -> cx=0
                                       ; En total a ecx le reste 64k
       mov  ax,ds
       add  ax,1000h
       mov  ds,ax                      ; incremento 64k ( al segmento )
       jmp  @@seguirleyendo

@@esmenor:
       pop  cx                         ; Me sirve por si es mayor que 64k.
       xor  dx,dx                      ; DS:DX -> Buffer
       mov  ah,3fh                     ; Read
       mov  bx,[es:fhandle]
       int  21h                        ; Finalmente leer. Pone todo en mem reservada

       clc

@@salir:
       pop  es                         ; Restaura segmentos usados.
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; DecodePCX
; Funcion que muestra en pantalla el pcx
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC DecodePCX
       pusha
       push ds
       push es

       mov  ax,[realx]
       mul  [realy]                    ; X * Y = Total de bytes = DX:AX
       mov  cx,4
@@loop:
       clc
       rcr  dx,1
       rcr  ax,1
       loop @@loop

       inc  ax                         ; Ahora en AX tengo el total de
                                       ; paragraph a reservar.
       mov  bx,ax                      ; bx=Numero de paragraph a reservar
       mov  ah,48h                     ; Reservar Memoria.
       int  21h                        ; Devuelve en ax el segmento.
       jnc  @@ok

       mov  dl,35
       Call NewColumn                  ; Nueva Columna
       lea  dx,[ErrorNoMem]            ; Mostrar mensaje de que falta
       Call DisplayString              ; Memoria.
       stc
       jmp  @@salir                    ; Volver con error

@@ok:
       mov  [MemSeg2],ax               ; Guarda el segemento de memoria.
       mov  es,ax                      ; ES tiene el segmento reservado.

       cld                             ; Clear Direction Flag

       cmp  [pcxaca.ver],5
       jne  @@here1

       call putdac                     ; Modifica la paleta (viene con paleta)

@@here1:
       mov  cx,[realy]
       mov  [uny],cx                   ; Esto es una copia del realy (alto del grafico)
       mov  cx,[realx]                 ; Number of times ( Ancho del grafico )

       xor  di,di                      ; ES:DI  (ES tiene el segmento de mem
                                       ; reservado.Va a tener el grafico)
       push ds
       pop  fs                         ; fs=ds

       mov  ax,[memseg]                ; (Seg reservado.Tiene lo leido del archivo)
       mov  ds,ax                      ; DS cambia de valor
       xor  si,si                      ; DS:SI

mainunpacking:

       cmp  di,0f000h                  ; Codigo que soporta varios segementos
       jb   @@aca1                     ; Es menor entonces no modifico
       mov  ax,es
       add  ax,0f00h
       mov  es,ax                      ; Suma al segmento 0f00h
       sub  di,0f000h                  ; y le resta al offset f000h
@@aca1:
       cmp  si,0f000h                  ; Lo mismo con el otro.
       jb   @@aca2                     ;
       mov  ax,ds
       add  ax,0f00h
       mov  ds,ax                      ; Suma al segmento 0f00h
       sub  si,0f000h                  ; y le resta al offset f000h

@@aca2:
       lodsb                           ; al = ds:si / si = si + 1
       mov  ah,al                      ; 2 hi bits.
       and  ah,192                     ; Encodeado ?
       cmp  ah,192                     ; Encodeado ?
       je   coded                      ; parece que si
       stosb                           ; es:di = al / di = di + 1

       dec cx
       jcxz newline
       jmp  mainunpacking

coded:
       mov  ah,al                      ;
       and  ah,63                      ; Los otros valores (ah=number of times)
       lodsb                           ; al = ds:si / si = si + 1
codedhere:
       stosb                           ; es:di = al / di = di + 1
       dec  cx
       dec  ah                         ; Sigue en encode ?
       jne  codedhere                  ; si, entonces repetir valor.
       jcxz newline                    ; Salta si cx=0. Esta linea X lista.
       jmp  mainunpacking
newline:
       mov  cx,[fs:realx]
       dec  [fs:uny]
       cmp  [fs:uny],0
       jne  mainunpacking

       clc
@@salir:
       pop  es
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ViewGraph
; Esta es la funcion que realmente pone el grafico en la pantalla
; Source: [memseg2]:0000
; Destin: a000:0000
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ViewGraph
       pusha
       push ds
       push es

       push ds
       pop  fs                         ; fs=ds

       mov  ax,0a000h
       mov  es,ax
       mov  ax,[memseg2]
       mov  ds,ax
       xor  di,di                      ;
       xor  si,si                      ; ds:si -> es:di

       Call SaveVid                    ; Guarda el contenido de la pantalla
                                       ; antes de cambiar de modo grafico
       mov  ax,13h
       int  10h                        ; Pone Modo grafico 320x200x256

       Call ChangeDac                  ; Modifica la paleta

       mov  cx,[fs:realx]              ; Chequea el valor a sumar para que entre
       sub  cx,MAX_X                   ; en pantalla el dibujo.
       jns  @@mayor                    ; Si hay no signo ( CX => 320)
       neg  cx                         ; Niega CX
@@mayor:
       mov  [fs:addx],cx               ; Diferencia = 320 - AnchoX

       cld
       xor  ax,ax                      ; Contador de X
       xor  bx,bx                      ; Contador de Y
@@loop:
       cmp  si,0f000h                  ; Lo mismo con el otro.
       jb   @@aca2                     ;
       mov  dx,ds
       add  dx,0f00h
       mov  ds,dx                      ; Suma al segmento 0f00h
       sub  si,0f000h                  ; y le resta al offset f000h

@@aca2:
       movsb                           ; ds:si -> es:di
       inc  ax
       cmp  ax,MAX_X
       je   @@xmayor                   ; X es mayor
       cmp  ax,[fs:realx]
       jne  @@loop                     ; X es menor ? no

       add  di,[fs:addx]               ; Xmenor
       jmp  @@contloop
@@xmayor:
       add  si,[fs:addx]               ; Xmayor
@@contloop:
       xor  ax,ax                      ; Resetea AX
       inc  bx
       cmp  bx,MAX_Y                   ; Es fin o algo parecido ?
       je   @@seguir                   ; si
       cmp  bx,[fs:realy]              ; ?
       jne  @@loop                     ; no

@@seguir:

       mov  ah,8
       int  21h                        ; Get Key

       mov  ax,3h                      ; Pone modo texto
       int  10h

       Call RestoreVid                 ; Restaura la pantalla

       pop  es
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; PutDAC
; Guarda los 256 colores de la paleta
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC PutDAC
       push ds
       push es

       push ds
       pop  es                         ; es=ds

       mov  [tienedac],0               ; No tiene DAC. Default

       cld
       mov  ebx,[dtaaca.size]
       sub  ebx,897                    ; 897 = 769 + 128
       mov  ax,[memseg]
@@aca:
       cmp  ebx,10000h                 ; es mas que 64k ?
       jb   @@seguir
       sub  ebx,10000h
       add  ax,1000h                   ; suma el segmento.
       jmp  @@aca

@@seguir:
       mov  [es:tienedac],1            ; Si tiene dac!
       mov  ds,ax
       cmp  [byte ptr bx],12
       jne  @@salir

       inc  bx
       mov  si,bx
       lea  di,[dac]                   ; es:di
       mov  cx,768
@@loop:
       lodsb
       shr  al,2                       ; Divide por 4 la paleta.
       stosb
       loop @@loop

@@salir:
       pop  es
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; ChangeDAC
; [dac]:0000/256 * 3 -> dac
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC ChangeDac
       pusha
       push ds
       push si
       mov  ax,@data
       mov  ds,ax

       cmp  [tienedac],1
       jne  @@salir

       cld
       xor  al,al
       mov  dx,3c8h
       out  dx,al
       inc  dx
       mov  cx,768
       mov  si,[offdac]
repne  outsb                           ; ds:si -> va al port cx veces

@@salir:
       pop  si
       pop  ds
       popa
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; RGetXY
; Real Get XY
; IN:  bx=X,
;      cx=y
; OUT: dl=valor
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC RGetXY
       push ds
       push si
       push ax
       push bx
       push cx

       mov  ax,@DATA
       mov  ds,ax                      ; Consigo este segmento (DATA)
       mov  es,ax

       mov  ax,[memseg2]
       mov  ds,ax                      ; para averiguar donde esta otra seg

       mov  ax,[es:realx]              ; ax=Ancho
       mul  cx                         ; cx=y . Hasta aca es dx:ax= Y * Ancho
       add  ax,bx                      ; bx=x .      "    "     ax= Y * Ancho + X
       adc  dx,0                       ; Suma Carry. "    "  dx:ax= Y * Ancho + X
       mov  si,ax                      ; offset
       or   dx,dx                      ; dx=0 ?
       je   @@getxy

       mov  cl,12
       shl  dx,cl                      ; Lo corro 16 veces!!!
       mov  ax,ds                      ; Arreglar para 64k
       add  ax,dx                      ; Sumo 64k * N veces (es otro segmento)
       mov  ds,ax
@@getxy:
       mov  dl,[si]                    ; dl=[ds:si]

       pop  cx
       pop  bx
       pop  ax
       pop  si
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; RSetXY
; Real Set XY
; IN: bx=X,
; IN: cx=y
; IN: dl=valor
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC RSetXY
       push ds
       push si
       push ax
       push bx
       push cx
       push dx

       mov  ax,@DATA
       mov  ds,ax                      ; Consigo este segmento (DATA)
       mov  es,ax

       mov  ax,[memseg2]
       mov  ds,ax                      ; para averiguar donde esta otra seg

       mov  ax,[es:realx]              ; ax=Ancho
       mul  cx                         ; cx=y . Hasta aca es dx:ax= Y * Ancho
       add  ax,bx                      ; bx=x .      "    "     ax= Y * Ancho + X
       adc  dx,0                       ; Suma Carry. "    "  dx:ax= Y * Ancho + X
       mov  si,ax                      ; offset
       or   dx,dx                      ; dx=0 ?
       je   @@setxy

       mov  cl,12
       shl  dx,cl                      ; Lo corro 16 veces!!!
       mov  ax,ds                      ; Arreglar para 64k
       add  ax,dx                      ; Sumo 64k * N veces (es otro segmento)
       mov  ds,ax
@@setxy:
       pop  dx
       mov  [si],dl                    ; [ds:si]=dl
       pop  cx
       pop  bx
       pop  ax
       pop  si
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto Tito2
; Este es el efecto propio del grupo Tito II
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Tito2
       push ds
       push es
       pusha

       ;
       ; Rotaciones
       ;

       popa
       pop  es
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Smoothering
; Se encarga de aplicar el efecto smooth en todo el dibujo.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Smoothering
       push ds
       push es

       xor  cx,cx                      ; Y=0
@@loop1:
       xor  bx,bx                      ; X=0
@@loop2:
       mov  ax,[realy]                 ; Chequea si ya se llego al final Y
       sub  ax,cx                      ; realy - posy
       cmp  ax,3
       jae  @@bien
       not  ax                         ; ax es el valor que le voy a restar a Y
       and  ax,3                       ; para que de bien la cuenta
       sub  cx,ax                      ; a Y le resto ax

@@bien:
       mov  ax,[realx]                 ; Chequea si ya se llego al final X
       sub  ax,bx                      ; realx - posx
       cmp  ax,3
       jae  @@ok
       not  ax                         ; ax es el valor que le voy a restar a X
       and  ax,3                       ; para que de bien la cuenta
       sub  bx,ax                      ; a X le resto ax

@@ok:
       Call Smooth                     ; Le aplica la funcion a (X,Y)
       add  bx,3                       ; Siguiente matriz
       cmp  bx,[realx]
       jb   @@loop2
       add  cx,3
       cmp  cx,[realy]
       jb   @@loop1

       pop  es
       pop  ds

       Call ViewGraph                  ; Ver como quedo

       ret
ENDP
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 1: Smoothering
; este efecto creo que es asi.
; Entrada:
;  bx = X
;  cx = y   y hace en este cuadrado eso
;  �����������Ŀ                      �����������Ŀ
;  � A � B � C �          --->        � X � X � X �
;  �����������Ĵ                      �����������Ĵ
;  � D � E � F �   A+B+C+D+E+F+G+H+I  � X � X � X �
;  �����������Ĵ X=�����������������  �����������Ĵ
;  � G � H � I �           9          � X � X � X �
;  �������������                      �������������
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Smooth
       push ds
       push es
       pusha

       push bx
       push cx

       mov  ax,@DATA
       mov  ds,ax
       mov  es,ax                      ; es=DATA

       mov  ax,[memseg2]
       mov  ds,ax
       xor  si,si                      ; ds:si --> grafico

       fldz                            ; Carga Zero al Copro

       ; PRIMERA PARTE
       ; Conseguir todos los valores de la matriz. y aplicarles la fma.

       mov  ax,bx                      ; Salva bx
       mov  [es:smo01],0
@@loop1:
       mov  [es:smo02],0
       mov  bx,ax                      ; Restaura BX
@@loop2:
       Call RGetXY                     ; bx,cx=x,y
       xor  dh,dh
       mov  [es:tempW],dx
       fiadd [es:tempW]

       inc  bx                         ; x++
       inc  [es:smo02]
       cmp  [es:smo02],3               ; 3 veces
       jne  @@loop2

       inc  cx                         ; y++
       inc  [es:smo01]
       cmp  [es:smo01],3               ; 3 veces
       jne  @@loop1

       mov  [es:tempW],9
       fidiv [es:tempW]                ; Divide todo por 9

       fistp [es:tempW]                ; [es:tempB] tiene el valor

       pop  cx                         ; Restaura Y
       pop  bx                         ; Restaura X


       ; SEGUNDA PARTE
       ; Ahora Tiene que poner el valor de es:tempB en la matriz

       mov  ax,bx                      ; Salva bx
       mov  [es:smo01],0
@@loop3:
       mov  [es:smo02],0
       mov  bx,ax                      ; Restaura BX
@@loop4:
       mov  dx,[es:TempW]
       xor  dh,dh
       Call RSetXY                     ; bx,cx=x,y

       inc  bx                         ; x++
       inc  [es:smo02]
       cmp  [es:smo02],3               ; 3 veces
       jne  @@loop4

       inc  cx                         ; y++
       inc  [es:smo01]
       cmp  [es:smo01],3               ; 3 veces
       jne  @@loop3

       popa
       pop  es
       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 2: Blur
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Blur
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto 3:
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Efecto3
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; BlancoNegro
; Esta es la rutina que transforma a blanco y a negro.
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC BlancoNegro
       push ds
       push es
       pusha

       push ds
       pop  es

       mov  ax,[realx]
       mul  [realy]                    ; X * Y = Total de bytes = DX:AX

       mov  bx,[memseg2]               ; Ax tiene el segemento del dibujo bien.
       mov  ds,bx
       xor  si,si                      ; ds:si -> dibujo

@@loop:
       push dx
       push ax                         ; X * Y. Lo guardo.

       mov  al,3
       mul  [byte ptr si]              ; [si](color) * 3.
       mov  di,ax
       lea  di,[dac+di]                ; offset lo tiene  [TablaDac + OFFset ]
       mov  al,[es:di]                 ; Red
       mov  bl,[es:di+1]               ; Green
       mov  cl,[es:di+2]               ; Blue
       xor  ah,ah
       xor  bh,bh
       xor  ch,ch                      ; Partes altas=0
       Call Luminancia                 ; Covierte un byte a blanco y negro.
       mov  [si],dl

       pop  ax
       pop  dx
       or   dx,dx                      ; Parte alta=0 ?
       jne  @@seguir
       cmp  si,ax                      ; Listo. Todo Convert ?
       jne  @@aca
       je   @@salir

@@seguir:
       cmp  si,0f000h
       jne  @@aca
       mov  bx,ds
       add  bx,0f00h
       mov  ds,bx
       sub  si,0f000h
       sub  ax,0f000h
       sbb  dx,0

@@aca:
       inc  si
       jmp  @@loop

@@salir:
       popa
       pop  es
       pop  ds

       lea  bx,[DacBN]
       mov  [offdac],bx
       Call ViewGraph

       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Efecto Luminancia
; dx = ax * 0,3 (Rojo) + bx * 0,59 (Verde) + cx * 0,11 (Azul)
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
PROC Luminancia
       push ds

       push ax
       mov  ax,@DATA
       mov  ds,ax
       pop  ax

       mov  [temp],ax                  ; Rojo
       fild [temp]                     ; Carga el valor entero a st
       fmul [dato030]                  ; Multiplica ax * 0,30
       fstp [suma]                     ; y lo suma y pop
       mov  [temp],bx                  ; Verde
       fild [temp]
       fmul [dato059]                  ; multiplica bx * 0,59
       fadd [suma]                     ; lo suma a lo anterior
       fstp [suma]                     ; y lo guarda y pop
       mov  [temp],cx                  ; Azul
       fild [temp]
       fmul [dato011]                  ; Multiplica cx * 0,11
       fadd [suma]                     ; lo suma a lo anterior
       fst  [suma]                     ; y lo guarda
       fistp [sumado]                  ; y lo guarda tambien como entero y pop
       mov  dx,[sumado]                ; devuelve lo sumado

       pop  ds
       ret
ENDP

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de datos
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
DATASEG
; String
LABEL InitString byte                  ; Mensaje de comienzo
db 13,10,'Organizador del Computador II - Proyecto I v0.0.10',13,10
db 'Grupo: Tito II - Integrantes: Dani, Cati, Riq',13,10,'$'

LABEL Mostrar byte
db 13,10
db 'Archivo        Tamano Graf        Mensajes',13,10
db '----------------------------------------------------------------------------$'
Intro  db   13,10,'$'

LABEL ErrorPCXVersion byte             ; por ahora los 2 apuntan al mismo.
ErrorPCXEncode db 'Error: No es .pcx$'
ErrorAbrir     db 'Error: No se pudo abrir el archivo$'
ErrorPCXColor  db 'Error: .pcx no es de 256 colores$'
ErrorNoMem     db 'Error: Necesita m�s memoria.$'
Leyendo        db 'Archivo Mostrado$'

LABEL ErrorHelp  byte
db 13,10,'Formato: Orga2 [/? | filespec]',13,10,13,10
db 9,'/?           Muestra esta pantalla',13,10
db 9,'Filespec     Puede ir el nombre de un archivo con Wildcards (*,?,etc)',13,10
db 9,'             Default: TITO2.PCX',13,10
db 13,10
db 'Ejemplos: Orga2 *.pcx',13,10
db '          Orga2 dibu??.*',13,10,'$'

fnombre     db 0                       ; Primer nombre ( 0=Find First 1=Find Next)
dato011 dq  0.11                       ; usado por luminancia
dato059 dq  0.59                       ; usado por luminancia
dato030 dq  0.3                        ; usado por luminancia

nombre db   'tito2.pcx'                ; este es el nombre default de Cargar Archivo.
nombtt db    40 dup(?)                 ; Reservo mas por las dudas.

valor1 db   '   0 x'                   ; Muestra los numeros.
valor2 db   '   0$'

INCLUDE 'paleta.inc'

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Variables no inicializadas
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
temp   dw   ?                          ; usado por luminancia
sumado dw   ?                          ; usado por luminancia
suma   dq   ?                          ; usado por luminancia
fhandle dw  ?                          ; File handle ( por funciones de archivos )
realx  dw   ?                          ; lo que ocupa de en serio el pcx
realy  dw   ?                          ; usado por Is256yFit
uny    dw   ?                          ; Usados por Decode PCX
addx   dw   ?                          ;    "    "  ViewGraph
memseg dw   ?                          ; Segmento de memoria reservado.
memseg2 dw  ?                          ; Otro segmento. Usado por DecodePCX
memvid dw   ?                          ; Segemte de mem para guardar consola.

cursorpos   dw ?                       ; Pos del cursor (por Resotore/SaveVid )
tienedac db ?                          ; Flag para ver si tiene dac
dac    db   256 * 3 dup(?)             ; Almacena los valores de la DAC
offdac dw   ?                          ; Offset de las dac

xcount dw   ?                          ; Contador X. Usado por Smooth
ycount dw   ?                          ; contador y
smo01  dw   ?                          ; Temporal de Smooth
smo02  dw   ?                          ; Temporal de Smooth

tempB  db   ?                          ; Byte Temporal
tempW  dw   ?                          ; Word Temporal

LABEL pcxaca pcxh                      ; Define Structura!
       db   128 dup(?)                 ; Donde ir� la cabecera del .pcx
LABEL dtaaca dtah                      ; Define Structura!
       db   43 dup(?)                  ; Donde va a ir la DTA temporaria

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
; Segmento de Stack
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-;
STACK 200H

END
