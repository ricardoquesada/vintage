/* libreria que permite usar menues con barra highligh  */
/* es dificil de usar y no tiene tantas cosas buenas    */
/* pero la hice yo en mi primer mes de C.		*/
/* asi que es buena igual				*/

#include <conio.h>
#include <bios.h>

#define LIGHTOFF 0x07
#define LIGHTON  0x70

/**********scursor*************/
void scursor(unsigned int shape)
{
 union REGS reg;
 reg.h.ah = 1;
 reg.x.cx = shape;
 int86(0X10, &reg, &reg);
}

/*********gcursor*************/
int gcursor(void)
{
 union REGS reg;
 reg.h.ah = 3;
 int86(0X10, &reg, &reg);
 return(reg.x.cx);
}

/**********************************************/
/*************** GKEY  (high)******************/
/**********************************************/
int gkey(void)
{
 int key, lo, hi;
 key = bioskey(0);
 lo = key & 0X00FF;
 hi = (key & 0XFF00) >> 8;
 return((lo == 0) ? hi + 256 : lo);
}

/***********************************************/
/****************** LIGHT (high)****************/
/***********************************************/
void light(int x,int y,int lenght,int attrib)
{
    /* variables */
    char buf[160];
    int i;

    gettext(x,y,x+lenght,y,buf);

    for( i=1 ; i<lenght*2 ; ) {
	buf[i] = attrib;
	i++;
	i++;
    }
    puttext(x,y,x+lenght,y,buf);
}
/***********************************************/
/******************* ABAJO (high)***************/
/***********************************************/
int abajo(int count,int num,int *y,int x,int xx)
{
    count++;
    if (count >= num)
	return count-1;
    light(x,*y,xx,LIGHTOFF);
    (*y)++;
    light(x,*y,xx,LIGHTON);
    return count;
}

/***********************************************/
/******************* ARRIBA (high)**************/
/***********************************************/
int arriba(int count,int *y,int x,int xx)
{
    count--;
    if (count < 0)
	return count+1;
    light(x,*y,xx,LIGHTOFF);
    (*y)--;
    light(x,*y,xx,LIGHTON);
    return count;
}

/***********************************************/
/*************    highlight     ****************/
/***********************************************/
/**
*  count: se llama con 0.
*  y    : minimos highlights.
*  num  : maximos highlights.
*  x    : longitud min a highlight.
*  xx   : largo de la cadena.
*  ret  : count... posicion seleccionada.
**/
int highlight(int y,int num,int x,int xx)
{
    /* variables locales */
    int k;
    int count = 0;
    int crsshape;

    crsshape = gcursor();
    scursor(0x2000);
    light(x,y,xx,LIGHTON);

    while(1) {
	k = gkey();
	switch (k) {
	  case 27 :scursor(crsshape);return -1;
	  case 328:                 /* crs up    */
	    count = arriba(count,&y,x,xx);
	    break;
	  case 336:                 /* crs dw    */
	    count = abajo(count,num,&y,x,xx);
	    break;
	  case 13: scursor(crsshape);return count;
	}
    }
}
