/* ESTE PROGRAMA LO USO SOLO PARA PRUEBA */
/* RICARDO QUESADA 1993                  */

/* Include files */

#include <graphics.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* declaracion de funciones  */

/* declaracion de variables */

/*           *****************************
                    MAIN PROGRAM
             *****************************
*/
int main (void)
{
    int gdriver = DETECT,gmode,errorcode;
    initgraph(&gdriver,&gmode,"");
    errorcode = graphresult();
    if (errorcode != grOk)
    {
        cprintf("Error: %s./r/n",grapherrormsg(errorcode));
        cprintf("Press a key to continue./r/n");
        getch();
        exit(1);
    }

  unsigned char buffer[]="Hola Felipe .como andas?";
  int midx,midy;
  midx = getmaxx() / 2;
  midy = getmaxy() / 2;
  line (0,0,getmaxx(),getmaxy() );
  settextjustify(CENTER_TEXT,CENTER_TEXT);
  outtextxy(midx,midy,buffer);
  getch();
  closegraph();
  return 0;


}
