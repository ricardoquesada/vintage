/* programa que detecta el crc */
/* por Ricardo Quesada */
#include <conio.h>
#include <string.h>
#include <fcntl.h>
#include <io.h>
#include <sys\stat.h>

unsigned short updcrc( register c, register unsigned crc)
{
	register count;

	for (count=8; --count>=0;) {
		if (crc & 0x8000) {
			crc <<= 1;
			crc += (((c<<=1) & 0400)  !=  0);
			crc ^= 0x1021;
		}
		else {
			crc <<= 1;
			crc += (((c<<=1) & 0400)  !=  0);
		}
	}
	return crc;	
}


int main( int argc, char *argv[]  )
{
    int handle,error;
    unsigned i,len;
    unsigned crc=0;
    unsigned char cp;

    cprintf("\r\n\r\n***   CRC por Ricardo Quesada. ***\r\n\r\n");

    if( argc <= 1)
    {
        cprintf("Error linea de comando.\r\n");
        cprintf("CRC archivo.\r\n");
        cprintf("\r\nej: crc command.com\r\n");
        exit(1);
    }

    if( (handle = open( argv[1] , O_RDWR | O_BINARY ) ) == -1 ) {
        perror();
        exit(1);
    }

    len = 0;
    while(1) {

        error = read(handle, &cp ,1);

        switch(error) {
            case 0:
                cprintf("Archivo: %s\r\n",argv[1]);
                cprintf("CRC: %u\r\n",crc);
                cprintf("longitud: %u\r\n",len);
                exit(0);
            case -1:
                cprintf("Error leyendo archivo.\r\n");
                perror();
                exit(1);
            default:

                crc = updcrc( cp, crc);
                len++;
                break;
        }
    };



}

