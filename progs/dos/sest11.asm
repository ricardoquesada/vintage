Comment|
File:    SeSt.ASM
Version: 1.1
Author:  RICARDO QUESADA
Date:    04/2/94
Purpose: MUESTRA EL ESTADO DE LA LINEA DE TEL. Y DEL MODEM...
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE



PRINCIPIO:

        JMP INTRO
        DB '(C)RQ94'

;***************************************************
;                INT 1CH
;***************************************************

NEW1CH: JMP SHORT ALA
        DB 'SE'
ALA:    CLI
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH SI
        PUSH DI

        MOV DI,0
        MOV DS,CS:ADDR

DISP:   MOV AH,3
        XOR DH,DH
        MOV DL,CS:COM        ;0=COM1  1=COM2 2=COM3... ETC
        INT 14H         ;AH=LINE STATUS  AL=MODEM STATUS

        MOV CH,1
VUELTA2:
        MOV CL,8        ;cantidad de bits a mostrar

VUELTA:
        SHL AH,1
        JC CARRYON
        MOV BL,'0'
        JMP SHORT CERCA
CARRYON:
        MOV BL,'1'
CERCA:
        MOV DS:DI,BL
        ADD DI,2
        DEC cl
        CMP cl,0
        JNE VUELTA
        CMP CH,2
        JE FINAL
        INC CH
        MOV DI,160
        mov ah,al
        JMP SHORT VUELTA2

FINAL:  STI
        POP DI
        POP SI
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD1CH]
        IRET


;**********************************************
;**************** NEW 2FH *********************
;**********************************************

NEW2FH:
        JMP SHORT ACA_ACA
        DB'SE'
ACA_ACA:
        CMP AH,0DEH
        JE ES_ESTO
FIN_:
        JMP CS:[OLD2FH]

ES_ESTO:
        CMP AL,30H
        JNE ES_ESTO2
        XOR AL,AL
        IRET

ES_ESTO2:
        CMP AL,31H
        JNE FIN_
        MOV AX,3031H
        MOV BX,3130H
        IRET

;**************************************************************
;**************************************************************
;---------------------------------DATA
OLD1CH  DD 0
OLD2FH  DD 0
ADDR    DW 0
DSP     DB 0
COM     DB 0

;      ******************************************
;           EMPIEZA LA UN/INS-TALACION
;      ******************************************

INTRO:  CLD
        MOV SI,81H
ALOPA:  LODSB
        CMP AL,13
        JNE LULU
        JMP SISIE
LULU:   CMP AL,'/'
        JNE ALOPA

DETEC:  LODSB
        CMP AL,13
        JNE LULA
        JMP SISIE
LULA:   AND AL,11011111B
        CMP AL,'H'              ;HELP
        JE HELP
        CMP AL,'V'              ;VERSION
        JE VERSION_
        CMP AL,'N'
        JE COMNUM

ERROR_MSG:
        PUSH CS                 ;OPTION ERROR
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;DESPUES DEL ERROR DISPLAY HELP SCREEN

HELP:   PUSH CS                 ;HELP SCREEN
        POP DS
        MOV DX,OFFSET MSGHLP
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H


VERSION_:
        MOV AX,0DE31H           ;AVERIGUA NM VERSION
        INT 2FH
        CMP AH,0DEH
        JE ERRVER_

        PUSH CS
        POP DS
        MOV [NUMBER],AH
        MOV [NUMBER+1],AL
        MOV [NUMBER+3],BH
        MOV [NUMBER+4],BL
        MOV DX,OFFSET VERNUM
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

ERRVER_:
        PUSH CS
        POP DS
        MOV AH,9
        MOV DX,OFFSET ERRVER
        INT 21H
        MOV AH,4CH
        INT 21H

COMNUM:
        LODSB
        SUB AL,31H
        MOV CS:COM,AL

SISIE:
        MOV ADDR,0B800H         ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H

NOMONO:
        MOV AX,0DE30H
        INT 2FH
        OR AL,AL                ;AL=0 INST, AL=30H UNINST
        JE UN_INST                ;NO ES INSTALADO
        JMP INST

;**************************************
;---------------------------------------UNINSTALL
UN_INST:
        MOV AX,351CH
        INT 21H
        CMP ES:[BX+2],'ES'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'ES'
        JE UNLOAD

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNL
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

UNLOAD:
        MOV SI,OFFSET OLD1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        PUSH CS
        POP DS
        MOV AH,9                ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H
        MOV AH,4CH
        INT 21H                 ;FIN DE TSR

;---------------------------------------INSTALL

INST:
        MOV AX,351CH            ;OLD1CH
        INT 21H
        MOV SI,OFFSET OLD1CH
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS                 ;NEW 1CH
        POP DS
        MOV DX,OFFSET NEW1CH
        MOV AX,251CH
        INT 21H

        MOV AX,352FH            ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH    ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV DX,OFFSET INSTAL    ;MENSAJE DE INTALADO
        MOV AH,09H
        INT 21H

        MOV DX,OFFSET INTRO
        INT 27H                 ;TSR

INSTAL  DB 13,10,'Serial Status v1.1 - (C)Ricardo Quesada.',13,10
        DB'Llame a SeSt de nuevo para desinstalarlo.',13,10
        DB'y SeSt /h para ver el menu de ayuda.',13,10,'$'

UNINST  DB'SeSt desinstalado.',13,10,'$'
VERNUM  DB'Numero de version: '
NUMBER  DB'00.00',13,10,'$'
ERRVER  DB'****ERROR:SeSt no esta instalado.',13,10,'$'
ERRUNL  DB'****ERROR:Imposible desinstalarlo.',13,10,'$'
ERROPT  DB'****ERROR:Opcion invalida.',13,10,'$'


MSGHLP  DB'**** SeSt v1.1 - Menu de ayuda ****',13,10,13,10
        DB'SeSt [opciones]',13,10
        DB'     /Nn  n es el numero de com.Default=1.',13,10
        DB'     /h   Muestra esta pantalla.',13,10
        DB'     /v   Muestra la version de SeSt instalado.',13,10,13,10
        DB'SeSt muestra en pantalla el estado del modem y de la linea de tel',13,10,13,10,'$'


        RUTI    ENDP
CODE    ENDS
        END RUTI
