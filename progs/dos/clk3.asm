COMMENT|
NOMBRE:   CLK
VERSION:  3.0
AUTOR:    Ricardo QUESADA
FECHA:    9-3-93
|

CODE    SEGMENT PUBLIC BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE
        JMP INTRO

        DB '(C)RQ93'
;***************************************************
;                INT 1CH
;***************************************************

NEW1CH: JMP SHORT ALA
        DB 'CL'
ALA:    CLI
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH SI
        PUSH DI

        XOR AH,AH
        MOV AL,CS:DSP
        MOV DI,AX
        MOV AL,CS:FLAG1
        CMP AL,0
        JNE CONTA
        JMP FINAL

CONTA:  MOV AX,CS:ADDR
        MOV DS,AX
        CMP DS:[DI+4],703AH ;COMPARA CON ':'
        JE DISP
        CMP DS:[DI],702AH   ;COMPARA CON '*'
        JE DISP
STOBUF: XOR BX,BX
CONA:   MOV SI,OFFSET BUFF
        MOV AX,DS:[DI+BX]
        MOV CS:[SI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE CONA

DISP:   MOV AH,2
        INT 1AH
        JNC LULU    ;DEAD CLOCK BATTERY, NO SO? SO JMP LULU
        XOR BX,BX
        MOV SI,OFFSET NOBAT
ALOPA:  MOV AX,CS:[SI+BX]
        MOV DS:[DI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE ALOPA
        JMP SHORT FINAL
LULU:   MOV BL,CL  ;MINUTOS *************
        AND BL,00001111B
        ADD BL,30H
        SHR CL,4
        AND CL,00001111B
        ADD CL,30H
        MOV DS:[DI+6],CL
        MOV DS:[DI+8],BL
        MOV BL,CH  ;HORAS *************
        AND BL,00001111B
        ADD BL,30H
        SHR CH,4
        AND CH,00001111B
        ADD CH,30H
        MOV DS:[DI],CH
        MOV DS:[DI+2],BL
        MOV BL,DH     ;SEGUNDOS *************
        AND BL,00001111B
        ADD BL,30H
        SHR DH,4
        AND DH,00001111B
        ADD DH,30H
        MOV DS:[DI+12],DH
        MOV DS:[DI+14],BL

        MOV AX,703AH
        MOV DS:[DI+4],AX
        MOV DS:[DI+10],AX
        MOV DS:[DI+1],AH
        MOV DS:[DI+3],AH
        MOV DS:[DI+7],AH
        MOV DS:[DI+9],AH
        MOV DS:[DI+13],AH
        MOV DS:[DI+15],AH

FINAL:  STI
        POP DI
        POP SI
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD1CH]
        IRET

;*********************************************************
;               INT 09H
;**********************************************************

NEW09H: PUSHF
        PUSH AX
        PUSH DS
        CLI
        IN AL,60H
        TEST AL,80H
        JNE SEGU
        CMP AL,9      ;TECLA 8 PULSADA
        JNE SEGU
        MOV AX,0040H    ;ALT PULSADA
        MOV DS,AX
        MOV AX,DS:[17H]
        AND AX,8
        CMP AX,8
        JNE SEGU
;---------------------
        MOV AL,255
        XOR CS:FLAG1,AL
        CMP CS:FLAG1,AL
        JE SEGU
;-------------------- DISPLAY BUFFER
        PUSH BX
        PUSH SI
        PUSH DI
        XOR AH,AH
        MOV AL,CS:DSP
        MOV DI,AX
        MOV AX,CS:ADDR
        MOV DS,AX
        XOR BX,BX
        MOV SI,OFFSET BUFF
LOOP1:  MOV AX,CS:[SI+BX]
        MOV DS:[DI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE LOOP1
        POP DI
        POP SI
        POP BX
;------------------
SEGU:   MOV AL,20H
        OUT 20H,AL
        POP DS
        POP AX
        POPF
        PUSHF
        CALL CS:[OLD09H]
        IRET

;**********************************VECTORES AND ETC...**************

OLD1CH  DD 0
OLD09H  DD 0
ADDR    DW 0
FLAG1   DB 255
HOR1    DB 00
HOR2    DB 00
MIN1    DB 00
MIN2    DB 00
SEC1    DB 00
SEC2    DB 00
BUFF    DB  16 DUP(0)
DSP     DB 128
NOBAT   DB 2AH,70H,'N',70H,'O',70H,' ',70H,'B',70H,'A',70H,'T',70H,'T',70H
;*********************************************************************
;               INSTALACION, DES-INSTALACION, ETC...
;*********************************************************************
INTRO:  CLD
        MOV SI,81H
LOD:    LODSB
        CMP AL,13
        JNE ACA1
        JMP INTR
ACA1:   CMP AL,'+'
        JE AP
        CMP AL,'-'
        JE AM
        AND AL,11011111B
        CMP AL,'H'
        JE HELP
        JMP SHORT LOD
HELP:   PUSH CS
        POP DS
        MOV DX,OFFSET MSGHEL
        MOV AH,9
        INT 21H
        MOV AX,4C00H
        INT 21H
; * * * * * * *     CUANDO SE ENTRA DISPLACEMENT
AP:     LODSB               ;DISPLACEMENT
        CMP AL,13           ;CR?
        JE SALTOB           ;YES, SO GO INTR
        MOV AH,AL
        LODSB
        CMP AL,13           ;CR?
        JE ALOB             ;YES, ONE DIGIT
        CMP AL,'/'
        JE ALOA
        SUB AL,30H
        AND AH,7            ;2 DIGITO NO MAYOR DE 7
        MOV BL,AL
        MOV AL,10
        MUL AH
        ADD AL,BL
SALTO:  MOV AH,2            ;TOTAL * 2 POR LOS ATRIBUTOS
        MUL AH
        MOV CS:DSP,AL
        JMP SHORT LOD       ;SEGUIR POR SI HAY MAS ESPECIFICACIONES

ALOA:   SUB AH,30H
        MOV AL,AH
        JMP SHORT SALTO

ALOB:   SUB AH,30H
        MOV AL,AH
        MOV AH,2
        MUL AH
        MOV CS:DSP,AL
SALTOB: JMP SHORT INTR
 ; * * * * * * * *     CUANDO SE ENTRA FECHA
AM:     XOR BX,BX
        MOV DI,OFFSET HORX
LOD2:   LODSB
        CMP AL,13
        JE INTR
        CMP AL,'/'
        JE SETD
        SUB AL,30H
        MOV CS:[DI+BX],AL
        INC BX
        CMP BX,8
        JB LOD2
SETD:   MOV CH,CS:HORX
        MOV BH,CS:HORX+1
        SHL CH,4
        OR CH,BH      ;HOR
        MOV CL,CS:MINX
        MOV BH,CS:MINX+1
        SHL CL,4
        OR CL,BH     ;MIN
        MOV DH,CS:SEGX
        MOV BH,CS:SEGX+1
        SHL DH,4
        OR DH,BH     ;SEG
        MOV DL,0     ;STANDAR TIME

        MOV AH,3         ;LLAMA PARA SET TIME
        INT 1AH
        JMP LOD

;+ + + + + + + + + + + + + + + + UN IN S TALACION
INTR:   MOV CS:ADDR,0B800H
        MOV AX,0040H
        MOV ES,AX
        MOV AL,30H
        AND AL,ES:[10H]
        CMP AL,30H
        JNE SIG
        MOV CS:ADDR,0B000H
SIG:    MOV AX,351CH
        INT 21H
        CMP ES:[BX+2],'LC'
        JNE INSTAL

;DES INSTALACION--------------------------------
        MOV SI,OFFSET OLD09H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2509H
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H
        MOV SI,OFFSET OLD1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H
        PUSH CS
        POP DS
        MOV DX,OFFSET MSGDES
        MOV AH,9
        INT 21H
        MOV AX,4C00H
        INT 21H


;INSTALACION-----------------------------------
INSTAL: MOV SI,OFFSET OLD1CH
        MOV [SI],BX
        MOV [SI+2],ES
        PUSH CS
        POP DS
        MOV DX,OFFSET NEW1CH
        MOV AX,251CH
        INT 21H
        MOV AX,3509H
        INT 21H
        MOV SI,OFFSET OLD09H
        MOV [SI],BX
        MOV [SI+2],ES
        MOV DX,OFFSET NEW09H
        MOV AX,2509H
        INT 21H
        MOV AH,9
        MOV DX,OFFSET MSGINS
        INT 21H
        MOV DX,OFFSET INTRO
        INT 27H
HORX    DB 1,2,0
MINX    DB 0,0,0
SEGX    DB 0,0
MSGINS  DB 13,10,'CLK V3.0 - (C)Ricardo QUESADA',13,10
        DB 'CLK /H display the HELP SCREEN.',13,10,'$'
MSGDES  DB 'CLK uninstalled',13,10,'$'
MSGHEL  DB '**** CLK V3.0 - HELP SCREEN ****',13,10,13,10
        DB 'CLK [OPTION/][OPTION/]',13,10
        DB '    +XX/ where XX, a number between 0 and 79,',13,10
        DB '         is the offset of the display. Default:64.',13,10
        DB '    -hh:mm:ss/ ,Hour,Min,Seg.',13,10,13,10
        DB 'IMPORTANT:Every option must end with an inverted bar ("/").',13,10
        DB 'e.g.: CLK  +05/ -15:15:00/',13,10,13,10
        DB 'Alt + 8 toggle the display ON and OFF.',13,10
        DB 'If CLK is installed, call again to uninstall it, and viceversa.',13,10,13,10
        DB 'CLK should not be sold. For personal use only.',13,10
        DB '(C) Copyright 1993 by Ricardo QUESADA.',13,10,'$'
RUTI    ENDP
CODE    ENDS
        END RUTI
