code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code


        mov ah,3ch
        xor cx,cx
        mov dx,offset nombre
        push cs
        pop ds
        int 21h
        mov handle,ax

        mov bx,ax
        mov ah,40h
        mov cx,fffinal - principio      ;longitud
        mov dx,offset principio
        push cs
        pop ds
        int 21h                         ;grabar cabecera...

        mov si,offset dataa
        mov asc3,0                      ;el primero es 0

vueltaapay2:
        mov cx,16
        call grabar3

vueltaapay:
        mov ah,[si]
        mov datab,ah
        call valorasc
        call grabar1
        inc si
        dec cx
        or cx,cx
        jne vueltaapay

        mov ax,4201h                    ;move file pointer 1 char atras
        mov bx,handle
        mov cx,0ffffh
        mov dx,0ffffh                   ;-1
        int 21h

        mov ah,asc3
        mov datab,ah
        call valorasc
        call grabar2

        inc asc3
        cmp asc3,0
        jne vueltaapay2

        mov ah,3eh
        mov bx,handle
        int 21h

        mov ah,4ch
        int 21h

;***************************************;
;***     valorasc                    ***;
;***************************************;

valorasc proc near

        push ax
        push dx
        push ds

        push cs
        pop ds

        mov numero,'0'          ;inicializa los valores
        mov numero+1,'0'
        mov numero+2,'0'

        mov ah,datab
        mov asc2,ah

db1:
        cmp asc2,0
        je disasc
        dec asc2               ;decrementa el valor

        cmp numero+2,'9'
        je db2
        inc numero+2
        jmp short db1
db2:
        mov numero+2,'0'
        cmp numero+1,'9'
        je db3
        inc numero+1
        jmp short db1
db3:
        mov numero+1,'0'
        inc numero
        jmp short db1

disasc:
        pop ds
        pop dx
        pop ax

        ret

valorasc endp

;***************************************;
;***    grabar1                      ***;
;***************************************;
grabar1 proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,40h
        mov bx,handle
        mov cx,4
        mov dx,offset numero
        int 21h

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret

grabar1 endp

;***************************************;
;***    grabar2                      ***;
;***************************************;
grabar2 proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,40h
        mov bx,handle
        mov cx,11
        mov dx,offset numero-8
        int 21h

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret

grabar2 endp

;***************************************;
;***    grabar3                      ***;
;***************************************;
grabar3 proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,40h
        mov bx,handle
        mov cx,5
        mov dx,offset defbyt
        int 21h

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret

grabar3 endp

;***************************************;
;***        data                     ***;
;***************************************;
handle  dw 0
nombre  db 'prupru',0

principio db';VCHAR 2.00 - Generador de fuente de caracteres...',13,10
          db';Tabla para Assembler',13,10,13,10
          db'tablita equ this byte',13,10
fffinal db '$'

defbyt  db 13,10,'db '

asc2    db 0            ;valor temporal de ascii
asc3    db 0
        db ' ;ascii '
numero  db '000,'

datab   db 0            ;valor temporal de la tabla
dataa   db 128,1,1,1,1,0,255,123,56,56,222,111,0,1,2,3,4,5,6,7,8,9,0,1,2,3
        db 'hola como te va a mi me va muy bien por suerte','$'


final equ this byte

ruti    endp
code    ends
        end ruti
