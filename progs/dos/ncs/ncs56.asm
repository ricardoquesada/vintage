Comment|
File:    NCS.ASM
Version: 5.8
Author:  Ricardo QUESADA
Date:    3/5/93
Purpose: INFORMS THE STATUS OF THE LEDS+INSERT
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE
        JMP INTRO
;-------------------------------------INT 1CH------------
        DB '(C)RQ93'
NEW1C:  JMP SHORT ALA
        DB 'NC'
ALA:    PUSH AX
        PUSH BX
        PUSH CX
        PUSH SI
        PUSH DI
        PUSH DS
        PUSH ES
        ;------------
        MOV AL,CS:CUCH
        CMP AL,255
        JE SEGOR
        JMP FIA
        ;------------
SEGOR:  XOR AH,AH           ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX
        MOV AX,CS:ADDR
        MOV DS,AX
        MOV AL,''
        CMP DS:[DI+6],AL
        JE DISPLA
        MOV AL,''
        CMP DS:[DI+6],AL
        JE DISPLA
        ;------------SI NO GUARDA LA INF EN EL BUF
        XOR BX,BX
        MOV SI,OFFSET BUF
CONA:   MOV AX,DS:[DI+BX]
        MOV CS:[SI+BX],AX
        ADD BX,2
        CMP BX,8
        JNE CONA
        ;------------
DISPLA: MOV AX,0040h
        MOV ES,AX
        MOV BL,ES:[17H]
        AND BL,10H
        JNE BITON   ;IF BL NOT 0
        MOV AL,'s'
        JMP SHORT CONTI
BITON:  MOV AL,'S'
CONTI:  MOV DS:[DI+4],AL
;__________________________________
        MOV BL,ES:[17H]
        AND BL,20H
        JNE BITON2  ;IF BL NOT 0
        MOV AL,'n'
        JMP SHORT CONTI2
BITON2: MOV AL,'N'
CONTI2: MOV DS:[DI],AL
;__________________________________
        MOV BL,ES:[17H]
        AND BL,40H
        JNE BITON3
        MOV AL,'c'
        JMP SHORT CONTI3
BITON3: MOV AL,'C'
CONTI3: MOV DS:[DI+2],AL
;---------------------------------
        MOV BL,ES:[17H]
        AND BL,80H
        JNE BITON4
        MOV AL,''
        JMP SHORT CONTI4
BITON4: MOV AL,''
CONTI4: MOV DS:[DI+6],AL
;-------------------------------
        MOV AL,70h              ;ATRIBUTOS DEL FONDO
        MOV CX,8
LOOP1:  MOV BX,CX
        MOV DS:[DI+BX-1],AL
        DEC CX
        LOOP LOOP1
;-------------------------------
FIA:    POP ES
        POP DS
        POP DI
        POP SI
        POP CX
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD1C]
        IRET
DSP     DB 146
OLD1C   DD 0
OLD9H   DD 0
OLD2F   DD 0
ADDR    DW 0
CUCH    DB 255
BUF     DB 8 DUP(0)
;****************************************************************************
;***************************************INT 9H*******************************
NEW9H:
        JMP SHORT SEGUIRE
        DB'NC'
SEGUIRE:
        PUSHF
        PUSH AX
        PUSH DS
        PUSH DI
        CLI
        XOR AH,AH      ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX
;===================
        IN AL,60H
        TEST AL,80H
        JNE CON9
        CMP AL,0AH           ;TECLA 9 PULSADA
        JNE CON9             ;NO , THEN JUMP CON15H
        MOV AX,0040H
        MOV DS,AX
        MOV AX,DS:[17H]
        AND AX,8
        CMP AX,8
        JNE CON9
;------------------
        MOV AL,255
        XOR CS:CUCH,AL
        CMP CS:CUCH,AL
        JE CON9
;------------------
        PUSH SI
        PUSH BX
        MOV AX,CS:ADDR
        MOV DS,AX
        XOR BX,BX
        MOV SI,OFFSET BUF
LOOPA:  MOV AX,CS:[SI+BX]
        MOV DS:[DI+BX],AX
        ADD BX,2
        CMP BX,8
        JNE LOOPA
        POP BX
        POP SI
;--------------------
CON9:   MOV AL,20H
        OUT 20H,AL
        POP DI
        POP DS
        POP AX
        POPF
        PUSHF
        CALL CS:[OLD9H]
        IRET

;***********************NEW 2F*****************************;
;**********************************************************;
NEW2F:
        JMP SHORT SEGUIRE2
        DB'NC'
SEGUIRE2:
        CMP AH,0DEH
        JE SI_
FIN_:
        JMP CS:[OLD2F]

SI_:
        CMP AL,10H          ;INSTALLED?
        JNE SI2_
        XOR AL,AL
        IRET
SI2_:
        CMP AL,11H
        JNE FIN_
        MOV AX,0005H
        MOV BX,0600H
        IRET
;***************************************************************************
;****************       EMPIEZA LA UN/INS-TALACION
;***************************************************----------------------
INTRO:  CLD
        MOV SI,81H
ALOP:   LODSB
        CMP AL,13
        JNE SEGUIR_
        JMP SICOLO
SEGUIR_:
        CMP AL,'/'
        JNE ALOP      ;LOOP UNTIL '/' OR CR IS FOUND

        LODSB
        AND AL,11011111B
        CMP AL,'D'
        JE AGDIS
        CMP AL,'H'
        JE HELP_
        CMP AL,'V'
        JE VERSION_
        CMP AL,'S'
        JNE ERROPT_
        JMP SAVE_

ERROPT_:               ;IF ANY OF THIS THEN ERROPT_
        PUSH CS
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H          ;THE DISPLAY THE HELP MESSAGE
HELP_:
        PUSH CS          ;DISPLAY HELP MESSAGE
        POP DS
        MOV AH,9
        MOV DX,OFFSET MSGHLP
        INT 21H
        MOV AH,4CH        ;THEN TERMINATE
        INT 21H

AGDIS:
        LODSB
        CMP AL,13
        JE ERROPT_  ;INVALID OPTION
        MOV AH,AL
        LODSB
        CMP AL,13   ;ONE DIGIT AND TERMINATE LOOP
        JNE ACA_1
        CALL CS:[ALOA]
        JMP SICOLO  ;                 "
ACA_1:
        CMP AL,20H  ;ONE DIGIT AND CONTINUE LOOP
        JNE ACA_2
        CALL CS:[ALOA]
        JMP ALOP    ;                 "
ACA_2:              ;TWO DIGITS
        AND AH,7    ;FIRST DIGIT  (NO MAS DE 70)
        SUB AL,30H  ;SECOND DIGIT
        MOV BL,AL
        MOV AL,10
        MUL AH
        ADD AL,BL

        SHL AL,1     ;MULTIPLICA POR 2 POR LOS ATRIBUTOS
        MOV CS:DSP,AL
        JMP SHORT ALOP

ALOA    PROC NEAR
        SUB AH,30H
        SHL AH,1           ;MUL X 2
        MOV CS:DSP,AH
        RET
ALOA    ENDP

VERSION_:
        MOV AX,0DE11H
        INT 2FH
        CMP AH,0DEH
        JE ERR_VER
        ADD AH,30H
        MOV CS:[NUMBER],AH
        ADD AL,30H
        MOV CS:[NUMBER+1],AL
        ADD BH,30H

        MOV CS:[NUMBER+3],BH
        ADD BL,30H
        MOV CS:[NUMBER+4],BL
        PUSH CS
        POP DS
        MOV DX,OFFSET VERSIO
        MOV AH,9
        INT 21H
        JMP SHORT FINAL_

ERR_VER:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRVNM
        MOV AH,9
        INT 21H
FINAL_:
        MOV AH,4CH
        INT 21H

SAVE_:
        MOV CX,0FFFFH
        XOR DI,DI
        XOR AX,AX
        MOV ES,CS:[2CH]

SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR

OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS

        MOV AX,3D02H
        INT 21H         ;** OPEN **
        JC ERRDRV_

        MOV CX,1618    ;FILE SIZE IN BYTES
        MOV DX,100H    ;DS:DX ORG OF FILE
        PUSH CS
        POP DS
        MOV BX,AX      ;BX HANDLER
        MOV AH,40H     ;** WRITE **
        INT 21H
        JC ERRDRV_

        MOV AH,3EH     ;** CLOSE HANDLER **
        INT 21H
        JC ERRDRV_
        JMP ALOP

ERRDRV_:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRDRV
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

SICOLO:
        MOV ADDR,0B800H       ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H
NOMONO:
        MOV AX,0DE10H          ;CHANGE THE OLD VECTOR TO THE NEW ONE
        INT 2FH
        CMP AL,10H
        JNE UNLOAD
        JMP INST
;---------------------------------------UNINSTALL
UNLOAD:
        MOV AX,351CH         ;CHECKEA SI SE PUEDE DESINSTALAR
        INT 21H              ;TODAS LAS INTERRUPCIONES
        CMP ES:[BX+2],'CN'   ;SI NO MENSAJE DE ABORTO
        JNE ERRUNLOAD

        MOV AX,3509H
        INT 21H
        CMP ES:[BX+2],'CN'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'CN'
        JE DESINSTALAR

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNI
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

DESINSTALAR:
        MOV SI,OFFSET OLD1C   ;INT 1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD9H       ;INT 9H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2509H
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2F   ;RELEASE INT 2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        PUSH CS
        POP DS
        MOV AH,09H          ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H

        MOV AH,4CH
        INT 21H             ;FIN DE TSR
;---------------------------------------INSTALL
INST:
        MOV AX,351CH              ;INT 1CH
        INT 21H
        MOV SI,OFFSET OLD1C
        MOV [SI],BX
        MOV [SI+2],ES
        PUSH CS
        POP DS
        MOV DX,OFFSET NEW1C
        MOV AX,251CH
        INT 21H

        MOV DX,OFFSET INSTAL     ;MENSAJE DE INTALADO
        MOV AH,09H
        INT 21H

        MOV AX,352FH            ;INT 2FH
        INT 21H
        MOV SI,OFFSET OLD2F
        MOV [SI],BX
        MOV [SI+2],ES
        MOV DX,OFFSET NEW2F
        MOV AX,252FH
        INT 21H

        MOV AX,3509H              ;INT 09H
        INT 21H
        MOV SI,OFFSET OLD9H
        MOV [SI],BX
        MOV [SI+2],ES
        MOV DX,OFFSET NEW9H
        MOV AX,2509H
        INT 21H  

        MOV DX,OFFSET INTRO
        INC DX
        INT 27H               ;TSR

VERSIO  DB'Version number: '
NUMBER  DB'00.00',13,10,'$'
ERRVNM  DB'****ERROR: NCS not loaded or Version less than 05.60',13,10,'$'
ERRUNI  DB'****ERROR: Unable to unload.',13,10,'$'
ERRDRV  DB'****ERROR: Drive failure.',13,10,'$'
ERROPT  DB'****ERROR: Invalid option.',13,10,'$'
INSTAL  DB 13,10,'NCS v5.6 - (C)Ricardo Quesada.',13,10
        DB'Call NCS again to unload it.',13,10
        DB'Type NCS /h to view the HELP SCREEN.',13,10,'$'
MSGHLP  DB'**** NCS v5.6 - HELP SCREEN ****',13,10,13,10
DB'NCS [options]',13,10
DB'    /dNN  where NN, a nunber between 0 and 79,',13,10
DB'          is the offset of the display.',13,10
DB'    /s    save the offset of the display to drive.',13,10
DB'    /v    view version of loaded NCS.',13,10
DB'    /h    display this screen.',13,10,13,10
DB'NCS displays the status of Num, Caps, Scroll & Insert.',13,10
db'[Alt]+9 toggles the display ON and OFF.',13,10,13,10
DB'Should not be sold. For personal use only.',13,10
DB'(C) 1993 by Ricardo Quesada.',13,10,'$'
UNINST  DB'NCS is unloaded.',13,10,'$'
RUTI    ENDP
CODE    ENDS
        END RUTI
