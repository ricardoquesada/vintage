Comment|
File:    NCS.ASM
Version: 6.0
Author:  Ricardo QUESADA
Date:    2/9/93
Purpose: INFORMS THE STATUS OF THE LEDS+INSERT
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE
        JMP INTRO
;-------------------------------------INT 1CH------------
        DB '(C)RQ93'
NEW1C:  JMP SHORT ALA
        DB'NC'

ALA:    PUSH AX
        PUSH BX
        PUSH CX
        PUSH SI
        PUSH DI
        PUSH DS
        PUSH ES

        MOV AX,CS:ADDR          ;ADDR ES POS 0B000H o 0B800H
        MOV DS,AX
        XOR AH,AH               ;DI ES EL OFFSET DEL DISPLAY
        MOV AL,CS:DSP
        MOV DI,AX

        MOV AL,CS:F_DISPLAY     ;PREGUNTA INICIAL
        CMP AL,255              ;�FLAG DE DISPLAY?
        JE F_BUFFER             ;255=OFF
        JMP DISPLA              ;0=ON
        ;------------

F_BUFFER:                       ;F_DISPLAY=OFF
        MOV AL,CS:FLAG_BUFF
        CMP AL,255              ;(255=OFF)
        JE FLAG_BU_OFF

        XOR BX,BX               ;TRANSFER DATA TO BUFFER (0=ON)
        MOV SI,OFFSET BUF
CONA:   MOV AX,DS:[DI+BX]
        MOV CS:[SI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE CONA
        JMP FIA
        ;------------

FLAG_BU_OFF:                    ;DISPLAY BUFFER
        XOR BX,BX
        MOV SI,OFFSET BUF
SEGU_:  MOV AX,CS:[SI+BX]
        MOV DS:[DI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE SEGU_

        XOR AL,AL               ;FLAG_BUFF=ON (0=ON)
        MOV CS:FLAG_BUFF,AL
        JMP FIA

        ;----------
DISPLA:                         ;F_DISPLAY=ON
        MOV AH,02H
        INT 16H                 ;Se llama a la INT 16 para obtener los valores.

        MOV BL,AL
        AND AL,10H
        JNE BITON               ;IF BL NOT 0
        MOV AH,'s'
        JMP SHORT CONTI
BITON:  MOV AH,'S'
CONTI:  MOV DS:[DI+4],AH
;__________________________________
        MOV AL,BL
        AND AL,20H
        JNE BITON2              ;IF BL NOT 0
        MOV AH,'n'
        JMP SHORT CONTI2
BITON2: MOV AH,'N'
CONTI2: MOV DS:[DI],AH
;__________________________________
        MOV AL,BL
        AND AL,40H
        JNE BITON3
        MOV AH,'c'
        JMP SHORT CONTI3
BITON3: MOV AH,'C'
CONTI3: MOV DS:[DI+2],AH
;---------------------------------
        MOV AL,BL
        AND AL,80H
        JNE BITON4
        MOV AH,'i'
        JMP SHORT CONTI4
BITON4: MOV AH,'I'
CONTI4: MOV DS:[DI+6],AH
;-------------------------------
        MOV AL,BL
        AND AL,01H
        JNE BITON5
        MOV AH,'r'
        JMP SHORT CONTI5
BITON5: MOV AH,'R'
CONTI5: MOV DS:[DI+10],AH
;----------------------------------
        MOV AL,BL
        AND AL,02H
        JNE BITON6
        MOV AH,'l'
        JMP SHORT CONTI6
BITON6: MOV AH,'L'
CONTI6: MOV DS:[DI+8],AH
;----------------------------------
        MOV AL,BL
        AND AL,04
        JNE BITON7
        MOV AH,'c'
        JMP SHORT CONTI7
BITON7: MOV AH,'C'
CONTI7: MOV DS:[DI+12],AH
;------------------------------------
        MOV AL,BL
        AND AL,08
        JNE BITON8
        MOV AH,'a'
        JMP SHORT CONTI8
BITON8: MOV AH,'A'
CONTI8: MOV DS:[DI+14],AH

        MOV AL,70h              ;ATRIBUTOS DEL FONDO
        MOV CX,16
LOOP1:  MOV BX,CX
        MOV DS:[DI+BX-1],AL
        DEC CX
        LOOP LOOP1

        MOV AL,255              ;FLAG_BUFF=OFF
        MOV CS:FLAG_BUFF,AL

;-------------------------------
FIA:    POP ES
        POP DS
        POP DI
        POP SI
        POP CX
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD1C]
        IRET

DSP     DB 138
OLD1C   DD 0
OLD9H   DD 0
OLD2F   DD 0
ADDR    DW 0
F_DISPLAY DB 0
FLAG_BUFF DB 255
BUF     DB 16 DUP(0)
;****************************************************************************
;***************************************INT 9H*******************************
NEW9H:
        JMP SHORT SEGUIRE
        DB'NC'
SEGUIRE:
        PUSHF
        PUSH AX
        PUSH DS
        PUSH DI
        CLI
        XOR AH,AH               ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX
;===================
        IN AL,60H
        TEST AL,80H
        JNE CON9
        CMP AL,0AH              ;TECLA 9 PULSADA
        JNE CON9                ;NO , THEN JUMP CON15H

        MOV AH,2                ;TECLA ALT PULSADA
        INT 16H
        AND AL,8
        CMP AL,8
        JNE CON9

;------------------
        MOV AL,255
        XOR CS:F_DISPLAY,AL

CON9:   MOV AL,20H
        OUT 20H,AL
        POP DI
        POP DS
        POP AX
        POPF
        PUSHF
        CALL CS:[OLD9H]
        IRET

;***********************NEW 2F*****************************;
;**********************************************************;
NEW2F:
        JMP SHORT SEGUIRE2
        DB'NC'
SEGUIRE2:
        CMP AH,0DEH
        JE SI2_
FIN_:
        JMP CS:[OLD2F]

SI2_:
        CMP AL,11H
        JNE FIN_
        MOV AX,0006H
        MOV BX,0000H
        IRET

;***************************************************************************
;****************       EMPIEZA LA UN/INS-TALACION
;***************************************************----------------------
INTRO:  CLD
        MOV SI,81H
ALOP:   LODSB
        CMP AL,13
        JNE SEGUIR_
        JMP SICOLO
SEGUIR_:
        CMP AL,'/'
        JNE ALOP                ;LOOP UNTIL '/' OR CR IS FOUND

        LODSB
        AND AL,11011111B
        CMP AL,'D'
        JE AGDIS
        CMP AL,'H'
        JE HELP_
        CMP AL,'V'
        JE VERSION_
        CMP AL,'S'
        JE SAVE_
        CMP AL,'U'
        JE UNINSTALL_
        CMP AL,'I'
        JE INSTALL_

ERROPT_:                        ;IF ANY OF THIS THEN ERROPT_
        PUSH CS
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;THE DISPLAY THE HELP MESSAGE
HELP_:
        PUSH CS                 ;DISPLAY HELP MESSAGE
        POP DS
        MOV AH,9
        MOV DX,OFFSET MSGHLP
        INT 21H
        MOV AH,4CH              ;THEN TERMINATE
        INT 21H

UNINSTALL_:
        JMP UNLOAD
SAVE_:
        JMP SAVE_1
INSTALL_:
        MOV AL,255
        XOR CS:FLAG_DE_SI_,AL
        JMP ALOP

AGDIS:
        LODSB
        CMP AL,13
        JE ERROPT_              ;INVALID OPTION
        MOV AH,AL
        LODSB
        CMP AL,13               ;ONE DIGIT AND TERMINATE LOOP
        JNE ACA_1
        CALL CS:[ALOA]
        JMP SICOLO              ;                 "
ACA_1:
        CMP AL,20H              ;ONE DIGIT AND CONTINUE LOOP
        JNE ACA_2
        CALL CS:[ALOA]
        JMP ALOP                ;                 "
ACA_2:                          ;TWO DIGITS
        AND AH,7                ;FIRST DIGIT  (NO MAS DE 70)
        SUB AL,30H              ;SECOND DIGIT
        MOV BL,AL
        MOV AL,10
        MUL AH
        ADD AL,BL

        SHL AL,1                ;MULTIPLICA POR 2 POR LOS ATRIBUTOS
        MOV CS:DSP,AL
        JMP ALOP

ALOA    PROC NEAR
        SUB AH,30H
        SHL AH,1                ;MUL X 2
        MOV CS:DSP,AH
        RET
ALOA    ENDP

VERSION_:
        MOV AX,0DE11H
        INT 2FH
        CMP AH,0DEH
        JE ERR_VER
        ADD AH,30H
        MOV CS:[NUMBER],AH
        ADD AL,30H
        MOV CS:[NUMBER+1],AL
        ADD BH,30H

        MOV CS:[NUMBER+3],BH
        ADD BL,30H
        MOV CS:[NUMBER+4],BL
        PUSH CS
        POP DS
        MOV DX,OFFSET VERSIO
        MOV AH,9
        INT 21H
        JMP SHORT FINAL_

ERR_VER:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRVNM
        MOV AH,9
        INT 21H
FINAL_:
        MOV AH,4CH
        INT 21H

SAVE_1:
        MOV CX,0FFFFH
        XOR DI,DI
        XOR AX,AX
        MOV ES,CS:[2CH]

SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR

OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS

        MOV AX,3D02H
        INT 21H                         ;** OPEN **
        JC ERRDRV_

        MOV CX,1815                     ;FILE SIZE IN BYTES
        MOV DX,100H                     ;DS:DX ORG OF FILE
        PUSH CS
        POP DS
        MOV BX,AX                       ;BX HANDLER
        MOV AH,40H                      ;** WRITE **
        INT 21H
        JC ERRDRV_

        MOV AH,3EH                      ;** CLOSE HANDLER **
        INT 21H
        JC ERRDRV_
        JMP ALOP

ERRDRV_:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRDRV
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

SICOLO:
        MOV ADDR,0B800H                 ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H
NOMONO:
        MOV AL,CS:FLAG_DE_SI_
        CMP AL,0
        JE INSTALAMO
        JMP INST_SI_
INSTALAMO:
        JMP INST
;---------------------------------------UNINSTALL
UNLOAD:
        MOV AX,0DE11H                   ;CHECKEA SI ES LA
        INT 2FH                         ;VERSION CORRECA
        CMP AX,0006H                    ;06.00
        JNE ERRUNLOAD
        CMP BX,0000H
        JNE ERRUNLOAD

        MOV AX,351CH                    ;CHECKEA SI SE PUEDE DESINSTALAR
        INT 21H                         ;TODAS LAS INTERRUPCIONES
        CMP ES:[BX+2],'CN'              ;SI NO MENSAJE DE ABORTO
        JNE ERRUNLOAD

        MOV AX,3509H
        INT 21H
        CMP ES:[BX+2],'CN'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'CN'
        JE DESINSTALAR

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNI
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

DESINSTALAR:
        MOV SI,OFFSET OLD1C             ;INT 1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD9H             ;INT 9H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2509H
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2F             ;RELEASE INT 2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        PUSH CS
        POP DS
        MOV AH,09H                      ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H

        MOV AH,4CH
        INT 21H                         ;FIN DE TSR
;---------------------------------------INSTALL
INST:
        MOV AX,0DE11H                   ;LLAMA A LA VERSION
        INT 2FH                         ;Y SI DA VER>0
        CMP AX,0DE11H                   ;NO INSTALA
        JNE ERR_INST_

INST_SI_:
        MOV AX,351CH                    ;INT 1CH
        INT 21H
        MOV SI,OFFSET OLD1C
        MOV [SI],BX
        MOV [SI+2],ES
        PUSH CS
        POP DS
        MOV DX,OFFSET NEW1C
        MOV AX,251CH
        INT 21H

        MOV DX,OFFSET INSTAL            ;MENSAJE DE INTALADO
        MOV AH,09H
        INT 21H

        MOV AX,352FH                    ;INT 2FH
        INT 21H
        MOV SI,OFFSET OLD2F
        MOV [SI],BX
        MOV [SI+2],ES
        MOV DX,OFFSET NEW2F
        MOV AX,252FH
        INT 21H

        MOV AX,3509H                    ;INT 09H
        INT 21H
        MOV SI,OFFSET OLD9H
        MOV [SI],BX
        MOV [SI+2],ES
        MOV DX,OFFSET NEW9H
        MOV AX,2509H
        INT 21H  

        MOV DX,OFFSET INTRO
        INC DX
        INT 27H                         ;TSR

ERR_INST_:
        MOV DX,OFFSET ERRINS
        MOV AH,9
        INT 21H
        MOV AX,4CFFH
        INT 21H

FLAG_DE_SI_ DB 0
CUCU    DB 0
VERSIO  DB'Version number: '
NUMBER  DB'06.00',13,10,'$'
ERRINS  DB'****ERROR: NCS is alredy installed.',13,10,'$'
ERRVNM  DB'****ERROR: NCS is not installed or the version is less than 5.6',13,10,'$'
ERRUNI  DB'****ERROR: Unable to uninstall.',13,10,'$'
ERRDRV  DB'****ERROR: Drive failure.',13,10,'$'
ERROPT  DB'****ERROR: Invalid option.',13,10,13,10,'$'
INSTAL  DB 13,10,'NCS v6.0 Beta 1.0 - (C) Ricardo Quesada.',13,10
        DB'Type NCS /h to view the HELP SCREEN.',13,10,'$'
MSGHLP  DB'**** NCS v6.0 - HELP SCREEN ****',13,10,13,10
DB'NCS [options]',13,10
DB'    /dNN  where NN, a nunber between 0 and 79,',13,10
DB'          is the offset of the display.',13,10
DB'    /s    save the offset of the display.',13,10
DB'    /v    view version of loaded NCS.',13,10
DB'    /h    display this screen.',13,10
DB'    /u    uninstall NCS.',13,10
DB'    /i    install NCS.',13,10,13,10
DB'NCS displays the status of Num,Caps,Scroll,Insert,Shifts,Alt & Control.',13,10
db'[Alt]+9 toggles the display ON and OFF.',13,10,13,10
DB'Should not be sold. For personal use only.',13,10
DB'(C) 1993 by Ricardo Quesada.',13,10,'$'

UNINST  DB'NCS have been uninstalled.',13,10,'$'

RUTI    ENDP
CODE    ENDS
        END RUTI
