Comment|
************************************

File:    SCRSVR.ASM
Version: 1.02
Author:  RICARDO QUESADA
Date:    10/2/94
Purpose: PROTECTOR DE PANTALLA

*************************************
|


CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

PRINCIPIO:

        JMP INIT
        DB '(C) 1994 SCREEN SAVER POR RICARDO QUESADA.',13,10


;*****************************************
;**************  INT 09H *****************
;*****************************************

NEW09H: JMP SHORT CONT09
        DB 'SS'

cont09:
        PUSH AX
        PUSH CX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        xor ax,ax
        mov cs:timer,ax

        cmp cs:flag,1
        jne finalcito

        mov ah,0
        mov al,cs:videomode
        or al,80h                       ;dont clear the new screen.
        int 10h                         ;set old video mode.

        mov ah,2
        mov bh,cs:pagenumber
        mov dh,cs:cursorrow
        mov dl,cs:cursorcol
        int 10h                         ;set old cursor pos with page

        mov ch,cs:cursorstr
        mov cl,cs:cursorend
        mov ah,1
        int 10h                         ;set old values of cursor size.

        cld                             ;foward direction
        mov cx,4000                     ;counter
        xor si,si
        mov ax,cs:allmem
        mov ds,ax                       ;ds:si  (source)
        mov ax,cs:addr
        mov es,ax
        xor di,di                       ;es:di  (destination)
rep     movsb                           ;restaura screen con datos originales

        mov cs:flag,0

finalcito:
        POP SI
        POP DI
        POP ES
        POP DS
        POP CX
        POP AX

        pushf
        call cs:[OLD09H]
        iret

;****************************************
;*************** INT 1CH  ***************
;****************************************

NEW1CH:
        JMP SHORT ALA
        DB 'SS'
ALA:    CLI
        PUSH ES
        PUSH DS
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH SI
        PUSH DI


        inc cs:timer
        mov ax,cs:tiempo
        cmp cs:timer,ax

        jne aqui1

        mov cs:flag,1                   ;puede empezar a ejecutarce es saver

        cld                             ;foward direction
        mov cx,4000                     ;counter
        mov ax,cs:addr
        mov ds,ax
        xor si,si                       ;ds:si  (source)
        xor di,di
        mov ax,cs:allmem
        mov es,ax                       ;es:di  (destination)
rep     movsb                           ;save the original screen

        mov ah,0fh
        int 10h                         ;get video mode
        mov cs:videomode,al
        mov cs:pagenumber,bh

        mov ah,3
        int 10h                         ;get cursor pos and size
        mov cs:cursorrow,dh
        mov cs:cursorcol,dl
        mov cs:cursorstr,ch
        mov cs:cursorend,cl

        or ch,20h
        xor cl,cl
        mov ah,1
        int 10h                         ;turn cursor off.

        mov al,3
        cmp cs:addr,0b800h              ;es color?
        je es_color_
        mov al,7                        ;entonces blanco y negro
es_color_:
        mov ah,0
        or al,80h                       ;don't clear the screen
        int 10h                         ;set video mode to 3 or 7 depending on monitor.

aqui1:
        cmp cs:flag,1
        je saver
        jmp FINAL

;********************************************************
;**** ACA EMPIEZA LO QUE HACE CUANDO LLEGA EL TIEMPO ****
;**********        S   A   V   E   R      ***************
saver:

        xor ax,ax
        mov cs:timer,ax

        cld                             ;foward direction
        mov cx,160                      ;counter
        mov ax,cs:addr
        mov ds,ax
        xor si,si                       ;ds:si  (source)
        mov di,offset buffer
        push cs
        pop es                          ;es:di  (destination)
rep     movsb

        mov ah,6
        mov al,1                        ;number of lines to scroll up
        mov bh,0                        ;attribute for new lines
        mov ch,0                        ;row upper left
        mov cl,0                        ;column upper left
        mov dh,24                       ;row lower right
        mov dl,79                       ;column lower right
        int 10h                         ;SCROLL WINDOW UP

        cld                             ;foward direction
        mov cx,160                      ;counter
        mov si,offset buffer
        push cs
        pop ds                          ;ds:si  (source)
        mov ax,cs:addr
        mov es,ax
        mov di,24 * 2 * 80                     ;es:di  (destination)
rep     movsb

FINAL:  STI
        POP DI
        POP SI
        POP DX
        POP CX
        POP BX
        POP AX
        POP DS
        POP ES
        PUSHF
        CALL CS:[OLD1CH]
        IRET


;**********************************************
;**************** NEW 2FH *********************
;**********************************************

NEW2FH:
        JMP SHORT ACA_ACA
        DB'SS'
ACA_ACA:
        CMP AH,0DEH
        JE ES_ESTO
FIN_:
        JMP CS:[OLD2FH]

ES_ESTO:
        CMP AL,50H
        JNE ES_ESTO2
        XOR AL,AL
        IRET

ES_ESTO2:
        CMP AL,51H
        JNE FIN_
        MOV AX,3031H                    ;version 01.
        MOV BX,3032H                    ;version .02
        IRET

;********************************************
;*****************   D A T A ****************
;********************************************

OLD09H  DD 0
OLD1CH  DD 0
OLD2FH  DD 0

VIDEOMODE DB 0                  ;VIDEO MODE
PAGENUMBER DB 0                 ;PAGE NUMBER
CURSORROW  DB 0                 ;CURSOR ROW
CURSORCOL  DB 0                 ;CURSOR COL
CURSOREND  DB 0                 ;CURSOR END LINE
CURSORSTR  DB 0                 ;CURSOR START LINE
ADDR    DW 0                    ;B000 B/W B800 COLOR
ALLMEM  DW 0                    ;SEGMENT OF ALLOCATED MEMORY.
TIMER   DW 0                    ;SE USA CON 'TIEMPO' PARA COMPARACION.
TIEMPO  DW 3276                 ;3 MINUTOS POR DEFECTO DE TIEMPO DE ESPERA
BUFFER  DB 160 DUP (?)          ;160 BYTES PARA ALMACENAR LAS LINEAS...
FLAG    DB 0


;      ******************************************
;      *                                        *
;      *               INTRO                    *
;      *                                        *
;      *    EMPIEZA LA UN/INS-TALACION          *
;      *                                        *
;      *                                        *
;      ******************************************

INTRO:  CLD
        MOV SI,81H
ALOPA:  LODSB
        CMP AL,13
        JNE LULU
        JMP SISIE
LULU:   CMP AL,'/'
        JNE ALOPA

DETEC:  LODSB
        CMP AL,13
        JNE LULA
        JMP SISIE
LULA:   AND AL,11011111B
        CMP AL,'H'              ;HELP
        JE HELP
        CMP AL,'V'              ;VERSION
        JE VERSION_
        CMP AL,'T'
        JE TIEMPITO

ERROR_MSG:
        PUSH CS                 ;OPTION ERROR
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;DESPUES DEL ERROR DISPLAY HELP SCREEN

HELP:   PUSH CS                 ;HELP SCREEN
        POP DS
        MOV DX,OFFSET MSGHLP
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

TIEMPITO:
        LODSB                   ;LOS MINUTOS DE RETRASO
        CMP AL,30H
        JB ERROR_MSG
        CMP AL,39H
        JA ERROR_MSG
        SUB AL,30H
        XOR AH,AH
        MOV BX,1092
        MUL BX                  ;1092 ES UN MINUTO  (18.2 POR 60)
        MOV CS:TIEMPO,AX
        JMP SHORT SISIE

VERSION_:
        MOV AX,0DE51H           ;AVERIGUA NM VERSION
        INT 2FH
        CMP AH,0DEH
        JE ERRVER_

        PUSH CS
        POP DS
        MOV [NUMBER],AH
        MOV [NUMBER+1],AL
        MOV [NUMBER+3],BH
        MOV [NUMBER+4],BL
        MOV DX,OFFSET VERNUM
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

ERRVER_:
        PUSH CS
        POP DS
        MOV AH,9
        MOV DX,OFFSET ERRVER
        INT 21H
        MOV AH,4CH
        INT 21H

SISIE:
        MOV ADDR,0B800H         ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H

NOMONO:
        MOV AX,0DE50H
        INT 2FH
        OR AL,AL                ;AL=0 INST, AL=50H UNINST
        JE UN_INST                ;NO ES INSTALADO
        JMP INST


;**************************************
;************** UNINSTALL *************
;**************************************
UN_INST:
        MOV AX,0DE51H           ;AVERIGUA NM VERSION
        INT 2FH

        CMP AX,3031H                    ;v e r s i o n :  01.
        JNE ERRUNLOAD2
        CMP BX,3032H                    ;v e r s i o n :  .02
        JE UN_INST2
ERRUNLOAD2:
        PUSH CS
        POP DS
        MOV [NUMBER2],AH
        MOV [NUMBER2+1],AL
        MOV [NUMBER2+3],BH
        MOV [NUMBER2+4],BL
        MOV DX,OFFSET ERRUNL2
        MOV AH,9
        INT 21H

        MOV AX,4C01H
        INT 21H


UN_INST2:
        MOV AX,351CH
        INT 21H
        CMP ES:[BX+2],'SS'
        JNE ERRUNLOAD

        MOV AX,3509H
        INT 21H
        CMP ES:[BX+2],'SS'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'SS'
        JE UNLOAD

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNL
        MOV AH,9
        INT 21H
        MOV AX,4C01H
        INT 21H

UNLOAD:
        MOV SI,OFFSET OLD1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        MOV SI,OFFSET OLD09H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2509H
        INT 21H




        PUSH CS
        POP DS
        MOV AH,9                ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H
        MOV AX,4C00H
        INT 21H                 ;FIN DE TSR


;**************************************
;**************  INSTALL  *************
;**************************************
INST:
        mov ah,48h              ;dos function service
        mov bx,250              ;number of paragraphs to allocate
        int 21h                 ;ALLOCATE MEMORY

        or ax,ax                ;if ax=0 then error
        jne mem_ok

        push cs
        pop ds
        mov dx,offset nomem_msg
        mov ah,9
        int 21h                 ;Display message

        mov ax,4c01h
        int 21h                 ;return to DOS with errorlevel=1

mem_ok:
        mov cs:allmem,ax        ;returns in ax segment of allocated memory


        MOV AX,351CH            ;OLD1CH
        INT 21H
        MOV SI,OFFSET OLD1CH
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS                 ;NEW 1CH
        POP DS
        MOV DX,OFFSET NEW1CH
        MOV AX,251CH
        INT 21H

        MOV AX,352FH            ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH    ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV AX,3509H            ;OLD 09
        INT 21H
        MOV SI,OFFSET OLD09H
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW09H    ;NEW 09H
        MOV AX,2509H
        INT 21H

        MOV DX,OFFSET INSTAL    ;MENSAJE DE INSTALADO
        MOV AH,09H
        INT 21H

        MOV DX,OFFSET INTRO
        INT 27H                 ;TSR


;****************************
;***** mensajes *************
;****************************
INSTAL  DB 13,10,'ScreenSaver v1.02 - (C)Ricardo Quesada.',13,10
        DB'Llame a ScrSvr denuevo para desinstalarlo.',13,10
        DB'y ScrSvr /h para ver el menu de ayuda.',13,10,'$'

UNINST  DB'ScrSvr desinstalado.',13,10,'$'
VERNUM  DB'Numero de version: '
NUMBER  DB'RQ.CO',13,10,'$'
ERRVER  DB'****ERROR:ScrSvr no esta instalado.',13,10,'$'
ERRUNL  DB'****ERROR:Imposible desinstalarlo.',13,10,'$'
ERRUNL2 DB'****ERROR:Con esta version (1.02) no se puede desinstalar la '
NUMBER2 DB'RQ.CO.',13,10,'Use esa version para desinstalarlo.',13,10,'$'
ERROPT  DB'****ERROR:Opci�n invalida.',13,10,'$'

NOMEM_MSG DB 13,10,'Error.',13,10,'Necesita por lo menos 4k de memoria libre.',13,10,'$'

MSGHLP  DB'**** Screen Saver v1.02 - Menu de ayuda ****',13,10,13,10
        DB'ScrSvr [opciones]',13,10
        DB'       /Tn  Tiempo de retraso para que empieze la protecci�n.',13,10
        DB'            n puede tomar valores entre 1 y 9 min.Por defecto son 3.',13,10
        DB'       /H   Muestra esta pantalla.',13,10
        DB'       /V   Muestra la version de ScrSvr instalado.',13,10,13,10
        DB'ScrSvr protege el fosforo de la pantalla.',13,10,'$'


;***************************;
;*                         *;
;*        I N I T          *;
;*                         *;
;***************************;
INIT:
        MOV AH,4AH
        MOV BX,OFFSET ENDE
        ADD BX,15
        MOV CL,4
        SHR BX,CL
        INC BX
        INT 21H                         ;CHANGE MEMORY SIZE

        MOV SP,OFFSET ENDE
        JMP INTRO

INIT_ENDE LABEL NEAR

;**************************;
;    S   T   A   C   K     ;
;**************************;

DW (256-((INIT_ENDE-INIT) SHR 1 )) DUP (?)

ENDE    EQU THIS BYTE

;***************************;
;           end             ;
;***************************;

RUTI    ENDP
CODE    ENDS
        END RUTI
