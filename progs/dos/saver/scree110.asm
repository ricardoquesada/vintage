Comment|
************************************

File:    SCRSVR.ASM
Version: 1.1
Author:  RICARDO QUESADA
Date:    4 Mar 94
Purpose: PROTECTOR DE PANTALLA

*************************************
|


CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

PRINCIPIO:

        JMP INIT
        DB '(C) 1994 SCREEN SAVER POR RICARDO QUESADA.',13,10


;***********  BORRAR PANTALLA ***********;

borrar_pant     PROC

        push ax
        push bx
        push cx
        push dx

        mov ah,6                        ;valor
        mov al,0                        ;borrar pantalla.
        mov bh,7                        ;attribute
        mov ch,0                        ;fila
        mov cl,0                        ;columna
        mov dh,49                       ;fila (por si esta en vga 80x50)
        mov dl,79                       ;columna
        int 10h                         ;interrupcion de video

        pop dx
        pop cx
        pop bx
        pop ax

        ret

borrar_pant     ENDP

;************ AZAR **************;
;retorna en cx un numero.        ;

azar            PROC

        push ax
        push cx

        mov ah,0
        int 1ah

        add dx,cx
        xchg dl,dh
        add dl,dh

        pop cx
        pop ax

        ret

azar            ENDP

;***********  save screen ***************;

save_screen     PROC

        cld                             ;foward direction
        mov cx,cs:bytes                 ;depende si es grafico o texto
        mov ax,cs:addrg
        mov ds,ax
        xor si,si                       ;ds:si  (source)

        xor di,di
        mov ax,cs:allmem
        mov es,ax                       ;es:di (destination)
rep     movsb                           ;save the original screen

        ret

save_screen     ENDP


;*****************************************
;**************  INT 09H *****************
;*****************************************

NEW09H: JMP SHORT CONT09
        DB 'SS'

cont09:
        PUSH AX
        PUSH CX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        xor ax,ax
        mov cs:timer,ax

        cmp cs:flag,1
        jne finalcito

        cmp cs:clpant,0
        je restaurapant                 ;no se debe cambiar modo grafico.


;******* todo lo referente al modo grafico ******

        mov ah,0
        mov al,cs:videomode
        or al,80h                       ;dont clear the new screen.
        int 10h                         ;set old video mode.

        and al,01111111b                ;ES EL MODO 7 (HERCULES);
        cmp al,7
        jne restaurapant

;solo Hercules.
        mov ah,2                        ;set cursor position.
        mov bh,cs:pagenumber
        mov dh,cs:cursorrow
        mov dl,cs:cursorcol
        int 10h

        mov ah,1
        mov ch,cs:cursorstr
        mov cl,cs:cursorend
        int 10h                         ;set cursor size.


;****** modo grafico end **********

restaurapant:
        cld                             ;foward direction
        mov cx,cs:bytes                 ;depende si es graficos o texto.
        xor si,si                       ;ds:si (source)
        mov ax,cs:allmem
        mov ds,ax

        mov ax,cs:addrg
        mov es,ax
        xor di,di                       ;es:di  (destination)

rep     movsb                           ;restaura screen con datos originales

        mov cs:flag,0

finalcito:
        POP SI
        POP DI
        POP ES
        POP DS
        POP CX
        POP AX

        pushf
        call cs:[OLD09H]
        iret

;****************************************
;*************** INT 08H  ***************
;****************************************

NEW08H:
        JMP SHORT ALA
        DB 'SS'
ALA:    CLI
        PUSH ES
        PUSH DS
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH SI
        PUSH DI

        cmp cs:flag,1                   ;si esta activado directo a saver.
        jne eaca
        jmp saver                       ;directo a saver.


eaca:
        inc cs:timer                    ;timer para ver si se tiene que activar la pantalla.

        mov ax,cs:tiempo
        cmp cs:timer,ax

        je aqui1
        jmp final

aqui1:
        mov cs:flag,1                   ;puede empezar a ejecutarce es saver

;******** todo lo referente al modo grafico ;


        mov ah,0fh
        int 10h                         ;get video mode
        mov cs:videomode,al
        mov cs:pagenumber,bh

        mov cs:clpant,0                 ;(Default) No se debe cambiar de modo grafico.
        mov bx,cs:addr
        mov cs:addrg,bx                 ;(Default) Es modo texto es el grafico


        cmp al,03h
        jbe nochange

        mov cs:clpant,1                 ;Si se deba cambiar de modo grafico.

        cmp al,07h                      ;Hercules(usa el mismo modo para graficos o texto)
        jne thencolor

;solo Hercules.
        mov ah,3                        ;get cursor size and position.
        mov bh,cs:pagenumber            ;page number.
        int 10h                         ;***********

        mov cs:cursorstr,ch
        mov cs:cursorend,cl
        mov cs:cursorrow,dh
        mov cs:cursorcol,dl


        mov cs:addrg,0b000h             ;hercules graficos.
        jmp nadamas

thencolor:
        mov cs:addrg,0a000h             ;Es modo grafico.
nadamas:
        call save_screen

        mov al,3
        cmp cs:addr,0b800h              ;es color?
        je es_color_
        mov al,7                        ;entonces blanco y negro
es_color_:
        mov ah,0
        or al,80h                       ;dont clean screen.
        int 10h                         ;set video mode to 3 or 7 depending on monitor.

;
        mov ah,2
        mov bh,cs:pagenumber
        mov dh,25                       ;fila 25 (invisible).
        int 10h                         ;cursor invisible.
;

        jmp prev

nochange:                               ;no se necesita cambiar de modo grafico jump.

        call save_screen

prev:
        mov cs:timer2,119               ;recordar que aca empieza.


;********************************************************
;**** ACA EMPIEZA LO QUE HACE CUANDO LLEGA EL TIEMPO ****
;**********        S   A   V   E   R      ***************
saver:
        inc cs:timer2

        cmp cs:timer2,120                       ;mas o menos 6 segundos.
        jne final

        call borrar_pant                        ;llama a borrar pantalla.

        call azar
sresta:                                         ;el numero va a ser el
        cmp dx,3900                             ;el offset del mensaje
        jb estabien                             ;no tiene que ser mayor que 4000
        sub dx,3900
        jmp sresta

estabien:
        and dl,11111110b                        ;numero par.

        mov ax,cs:addr                          ; donde esta pantalla
        mov ds,ax
        mov si,dx                               ;offset sacado de azar.

        push cs
        pop es                                  ; es:di
        mov di,offset mensaje                   ;donde esta Mensaje

seguircon:
        mov al,es:[di]
        mov ah,dh
        and ah,00001111b                        ;color retornado de azar.
        mov ds:[si],ax

        inc di
        add si,2
        cmp byte ptr es:[di],0
        jne seguircon

        mov cs:timer2,0


FINAL:  STI
        POP DI
        POP SI
        POP DX
        POP CX
        POP BX
        POP AX
        POP DS
        POP ES
        PUSHF
        CALL CS:[OLD08H]
        IRET


;**********************************************
;**************** NEW 2FH *********************
;**********************************************

NEW2FH:
        JMP SHORT ACA_ACA
        DB'SS'
ACA_ACA:
        CMP AH,0DEH
        JE ES_ESTO
FIN_:
        JMP CS:[OLD2FH]

ES_ESTO:
        CMP AL,50H
        JNE ES_ESTO2
        XOR AL,AL
        IRET

ES_ESTO2:
        CMP AL,51H
        JNE ES_ESTO3                    ;AGREGADO EN VERSION 1.1
        MOV AX,3031H                    ;version 01.
        MOV BX,3130H                    ;version .10
        IRET

ES_ESTO3:
        cmp al,52h                      ;retorna en dx el cs
        jne fin_                        ;y en bx el segmento a release.

        push cs
        pop ds
        mov dx,ds

        mov bx,cs:allmem
        iret                            ;


;********************************************
;*****************   D A T A ****************
;********************************************

OLD09H  DD 0
OLD08H  DD 0
OLD2FH  DD 0


VIDEOMODE DB 0                  ;VIDEO MODE
PAGENUMBER DB 0                 ;PAGE NUMBER
CURSORROW  DB 0                 ;CURSOR ROW
CURSORCOL  DB 0                 ;CURSOR COL
CURSOREND  DB 0                 ;CURSOR END LINE
CURSORSTR  DB 0                 ;CURSOR START LINE

ADDR    DW 0                    ;B000 B/W B800 COLOR
ADDRG   DW 0                    ;ES 0A000H SI ES GRAFICOS O LO DE ARRIBA TEXTO
TIMER   DW 0                    ;SE USA CON 'TIEMPO' PARA COMPARACION.
TIMER2  DB 0                    ;TIMER DENTRO DE SAVER.
TIEMPO  DW 3276                 ;3 MINUTOS POR DEFECTO DE TIEMPO DE ESPERA
FLAG    DB 0                    ;FLAG DE ACTIVADO 1=TRUE
CLPANT  DB 0                    ;FLAG CAMBIA DE MODO GRAFICO 1=TRUE
MENSAJE DB 'Screen Saver v1.10 - RQ - Una tecla para seguir...',0
ALLMEM  DW 0
paragraphs dw 500               ;default modo texto.(en paragraphs)
bytes      dw 8000              ;default modo texto.(en bytes).


;      ******************************************
;      *                                        *
;      *               INTRO                    *
;      *                                        *
;      *    EMPIEZA LA UN/INS-TALACION          *
;      *                                        *
;      *                                        *
;      ******************************************

INTRO:  CLD
        MOV SI,81H
ALOPA:  LODSB
        CMP AL,13
        JNE LULU
        JMP SISIE
LULU:   CMP AL,'/'
        JNE ALOPA

DETEC:  LODSB
        CMP AL,13
        JNE LULA
        JMP SISIE
LULA:   AND AL,11011111B
        CMP AL,'H'              ;HELP
        JE HELP
        CMP AL,'V'              ;VERSION
        JE VERSION_
        CMP AL,'T'
        JE TIEMPITO
        CMP AL,'G'
        JE MODOGRAFICO
        CMP AL,'R'
        JE REGISTRACION

ERROR_MSG:
        PUSH CS                 ;OPTION ERROR
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;DESPUES DEL ERROR DISPLAY HELP SCREEN

HELP:   PUSH CS                 ;HELP SCREEN
        POP DS
        MOV DX,OFFSET MSGHLP
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

TIEMPITO:
        LODSB                   ;LOS MINUTOS DE RETRASO
        CMP AL,30H
        JB ERROR_MSG
        je ahoraya
        CMP AL,39H
        JA ERROR_MSG
        SUB AL,30H
        XOR AH,AH
        MOV BX,1092
        MUL BX                  ;1092 ES UN MINUTO  (18.2 POR 60)
        MOV CS:TIEMPO,AX
        JMP ALOPA               ;VUELVE A EMPEZAR EL LOOP.

ahoraya:
        xor ax,ax
        mov al,90
        mov cs:TIEMPO,ax
        jmp alopa               ;vuelve a empezar el loop


MODOGRAFICO:
        mov cs:paragraphs,4096  ;reservar 64k.
        mov cs:bytes,0ffffh     ;lo mismo pero en bytes.
        jmp alopa               ;vuelve a empezar el loop.

VERSION_:
        MOV AX,0DE51H           ;AVERIGUA NM VERSION
        INT 2FH
        CMP AH,0DEH
        JE ERRVER_

        PUSH CS
        POP DS
        MOV [NUMBER],AH
        MOV [NUMBER+1],AL
        MOV [NUMBER+3],BH
        MOV [NUMBER+4],BL
        MOV DX,OFFSET VERNUM
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

ERRVER_:
        PUSH CS
        POP DS
        MOV AH,9
        MOV DX,OFFSET ERRVER
        INT 21H
        MOV AH,4CH
        INT 21H

REGISTRACION:
        MOV AH,9
        MOV DX,OFFSET REGIST_MSG
        INT 21H

        MOV AX,4C00H
        INT 21H

SISIE:
        MOV ADDR,0B800H         ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H

NOMONO:
        MOV AX,0DE50H
        INT 2FH
        OR AL,AL                ;AL=0 INST, AL=50H UNINST
        JE UN_INST                ;NO ES INSTALADO
        JMP INST


;**************************************
;************** UNINSTALL *************
;**************************************
UN_INST:
        MOV AX,0DE51H           ;AVERIGUA NM VERSION
        INT 2FH

        CMP AX,3031H                    ;v e r s i o n :  01.
        JNE ERRUNLOAD2
        CMP BX,3130H                    ;v e r s i o n :  .10
        JE UN_INST2
ERRUNLOAD2:
        PUSH CS
        POP DS
        MOV [NUMBER2],AH
        MOV [NUMBER2+1],AL
        MOV [NUMBER2+3],BH
        MOV [NUMBER2+4],BL
        MOV DX,OFFSET ERRUNL2
        MOV AH,9
        INT 21H

        MOV AX,4C01H
        INT 21H


UN_INST2:
        MOV AX,3508H
        INT 21H
        CMP ES:[BX+2],'SS'
        JNE ERRUNLOAD

        MOV AX,3508H
        INT 21H
        CMP ES:[BX+2],'SS'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'SS'
        JE UNLOAD

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNL
        MOV AH,9
        INT 21H
        MOV AX,4C01H
        INT 21H

UNLOAD:
        mov ax,0de52h
        int 2fh                 ;llama para saber donde esta el block a liberar.

        MOV AH,49H
        MOV ES,BX
        INT 21H                 ;RELEASE ALLOCATED MEMORY

        mov es,dx               ;valor obtenido en funcion 52h

        MOV SI,OFFSET OLD08H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2508H
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        MOV SI,OFFSET OLD09H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2509H
        INT 21H

        PUSH CS
        POP DS
        MOV AH,9                ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H
        MOV AX,4C00H
        INT 21H                 ;FIN DE TSR


;**************************************
;**************  INSTALL  *************
;**************************************
INST:
        mov ah,48h              ;dos function service
        mov bx,cs:paragraphs    ;number of paragraphs to allocate
        int 21h                 ;ALLOCATE MEMORY

        or ax,ax                ;if ax=0 then error
        jne mem_ok

        push cs
        pop ds
        mov dx,offset nomem_msg
        mov ah,9
        int 21h                 ;Display message

        mov ax,4c01h
        int 21h                 ;return to DOS with errorlevel=1

mem_ok:
        mov cs:allmem,ax        ;returns in ax segment of allocated memory


        MOV AX,3508H            ;OLD08H
        INT 21H
        MOV SI,OFFSET OLD08H
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS                 ;NEW 08H
        POP DS
        MOV DX,OFFSET NEW08H
        MOV AX,2508H
        INT 21H

        MOV AX,352FH            ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH    ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV AX,3509H            ;OLD 09
        INT 21H
        MOV SI,OFFSET OLD09H
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW09H    ;NEW 09H
        MOV AX,2509H
        INT 21H

        MOV DX,OFFSET INSTAL    ;MENSAJE DE INSTALADO
        MOV AH,09H
        INT 21H

        MOV DX,OFFSET INTRO
        ADD DX,15               ;NEXT PARAGRAPH
        SHR DX,4                ;DIVIDE POR 16.
        ADD DX,1
        MOV AL,0AAH             ;DATO QUE SE PASA. AA = TSR. (PARA MI,OBVIO.)
        MOV AH,31H              ;TERMINATE AND STAY RESIDENT.
        INT 21H                 ;TSR


;****************************
;***** mensajes *************
;****************************
INSTAL  DB 13,10,'ScreenSaver v1.1 - (C)Ricardo Quesada.',13,10
        DB'Llame a ScrSvr denuevo para desinstalarlo.',13,10
        DB'y ScrSvr /h para ver el menu de ayuda.',13,10,'$'

UNINST  DB'ScrSvr desinstalado.',13,10,'$'
VERNUM  DB'Numero de version: '
NUMBER  DB'RQ.CO',13,10,'$'
ERRVER  DB'****ERROR:ScrSvr no esta instalado.',13,10,'$'
ERRUNL  DB'****ERROR:Imposible desinstalarlo.',13,10,'$'
ERRUNL2 DB'****ERROR:Con esta version (1.1) no se puede desinstalar la '
NUMBER2 DB'RQ.CO.',13,10,'Use esa version para desinstalarlo.',13,10,'$'
ERROPT  DB'****ERROR:Opci�n invalida.',13,10,'$'

NOMEM_MSG DB 'Error',13,10,'Insuficiente memoria. Necesita 64k para modo grafico',13,10
          db 'Y 8k si lo usa con texto.',13,10,'$'

REGIST_MSG DB'                       -- ScrSvr 1.10 -- ',13,10,10,10
db'Registracion: No hace falta pagar una registracion ya que este programa',13,10
db'es HeartWare. O sea que su registracion es valida mientras avise al autor',13,10
db'lo que opina del programa. Se pueden mandar insultos, mimitos, o lo que',13,10
db'sientan mientras que sean con el corazon.(Con sentimiento, vio.)',13,10
db'Ricardo Quesada',13,10
db'805-1247',13,10,'$'


MSGHLP  DB'**** Screen Saver v1.1 - Menu de ayuda ****',13,10,13,10
        DB'ScrSvr [opciones]',13,10
        DB'       /Tn  Tiempo de retraso para que empieze la protecci�n.',13,10
        DB'            n puede tomar valores entre 1 y 9 min.Por defecto son 3.',13,10
        DB'       /G   Solo usar si se va usar en modo grafico.',13,10
        DB'            Lo que hace es reservar mas memoria para los graficos.',13,10
        DB'       /H   Muestra esta pantalla.',13,10
        DB'       /V   Muestra la version de ScrSvr instalado.',13,10
        db'       /R   Registracion.',13,10,13,10
        DB'Advertencia:',13,10
        DB'Este programa funciona solo bajo DOS.',13,10
        DB'Recuerde que si lo usa con /G reservar� 64k. De lo contrario 8k.',13,10,13,10
        DB'ScrSvr protege el fosforo del monitor.',13,10,'$'


;***************************;
;*                         *;
;*        I N I T          *;
;*                         *;
;***************************;
INIT:
        MOV AH,4AH
        MOV BX,OFFSET ENDE
        ADD BX,15
        MOV CL,4
        SHR BX,CL
        INC BX
        INT 21H                         ;CHANGE MEMORY SIZE

        MOV SP,OFFSET ENDE
        JMP INTRO

INIT_ENDE LABEL NEAR

;**************************;
;    S   T   A   C   K     ;
;**************************;

DW (256-((INIT_ENDE-INIT) SHR 1 )) DUP (?)

ENDE    EQU THIS BYTE

;***************************;
;           end             ;
;***************************;

RUTI    ENDP
CODE    ENDS
        END RUTI
