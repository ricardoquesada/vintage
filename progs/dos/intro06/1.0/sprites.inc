;-----------------------------------------------------------------------------;
;                                                                             ;
;                        Funciones para manejar Sprites                       ;
;  Todas fueron hechas por Ricardo Quesada salvo las que digan lo contrario   ;
;                                                                             ;
;-----------------------------------------------------------------------------;

p286

;---------------------------;
;		CreateSpr			;
;---------------------------;
;	 Create a sprite		;
;---------------------------;
;	In: 					;
;		cx	= N of sprite	;
;---------------------------;
createspr proc near
    cli
    pusha
	push ds
    push cx

    push cx
	mov cl,3
	call setpage
	pop  cx 					; Todos los sprites se crean en p�gina 3

	push cs
	pop ds

	shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

    mov  ax,[ si.sp_orgx ]              ; Copiar a los datos
    mov  [ si.sp_orgxx ],ax             ; que son usados por SprImage.
    mov  ax,[ si.sp_orgy ]
    mov  [ si.sp_orgyy ],ax
    mov  ax,[ si.sp_tamx ]
    mov  [ si.sp_tamxx ],ax
    mov  ax,[ si.sp_tamy ]
    mov  [ si.sp_tamyy ],ax
    mov  ax,[ si.sp_mask ]
    mov  [ si.sp_maskk ],ax


    mov cx,0

createloop1:
    push cx

    mov di,[ si.sp_dato ]       ; Donde estan los datos!
    mov ax,[ si.sp_tamy ]       ; Max y del sprite
    mov sprmaxy,ax              ; Mueve todos esos datos
    mov ax,[ si.sp_tamx ]       ; Max x del sprite
    mov sprmaxx,ax              ;
    mul cl                      ; Multiplica ( ancho * (1 | 2 | 3 | 4) )
    mov bx,[ si.sp_orgx ]       ; Pos original del Sprite x
    add bx,ax                   ; Le suma lo multiplicado.
    add bx,cx                   ; Todo Joya pap�.
    mov sprx,bx                 ; a varibles internas de la
    mov ax,[ si.sp_orgy ]       ; Pos original del Sprite y
    mov spry,ax                 ; funci�n

	xor bx,bx	;		y
showloop2:
	xor ax,ax	;		x
showloop1:
	mov cx,[di] 				; Puntero de donde estan los datos.

	push ax
	push bx

	add  bx,spry
	add  ax,sprx
    cmp  ax,319                 ; no escribir despu�s de 319
    ja   nopixelear
	call setpix

nopixelear:
	pop bx
	pop ax

	inc di
	inc ax

	cmp ax,sprmaxx
	jb	showloop1
	inc bx
	cmp bx,sprmaxy
	jb	showloop2

    pop cx
    inc cx
    cmp cx,4
    jb  createloop1

;---------------------------; Crear m�scara.

    mov cx,0

createloop2:
    push cx

    mov di,[ si.sp_mask ]       ; Offset de la m�scara.
    mov ax,[ si.sp_tamx ]       ; Max x del sprite
    mov bx,[ si.sp_tamy ]       ; Max y del sprite
    push ax
    push bx

    mul bx                      ; multiplicar ancho por largo ( ax * bx )
    shr ax,3                    ; Dividir por 8
    mul cl                      ; y multiplicar por ( 0 1 2 3 )
    add di,ax                   ; Offset de la m�scara listo!

    mov ax,[ si.sp_tamx ]       ; Otra vez este valor.
    mul cl                      ; otra vez multiplicarlo por  0 1 2 3
    mov cx,[ si.sp_orgx ]       ; Pos original del Sprite x
    add cx,ax                   ; Offset de la pos org de x

    mov dx,[ si.sp_orgy ]       ; Pos original del Sprite y
    pop bx
    pop ax

    add ax,cx
	mov sprmaxx,ax				; Lo m�ximo
	add bx,dx
	mov sprmaxy,bx				; Mueve todos esos datos
	mov sprx,cx 				; a varibles internas de la
    mov sprx2,cx                ; pos orginal!
    mov spry,dx                 ; funci�n

mskmainloop:
	mov dl,[di] 				; byte de la m�scara.
	mov cx,8
mskloop1:
	mov  ax,sprx
	mov  bx,spry
	call getpix

	push ax

	inc  sprx
	mov  ax,sprx
	cmp  ax,sprmaxx
	jb	 mskhere1

    mov  ax,sprx2               ; pone en x la pos original
	mov  sprx,ax				;
	inc  spry
	mov  ax,spry
	cmp  ax,sprmaxy
	jb	 mskhere1

	pop  ax 					; acabo la msk
	jmp  mskfin

mskhere1:
	pop  ax

	shr  dl,1

	cmp  byte ptr pcolor,0
	je	 transparent
	or	 dl,80h
transparent:
	loop mskloop1
	mov  [di],dl
	inc  di
	jmp  mskmainloop

mskfin:
    pop  cx
    inc  cx
    cmp  cx,4
    je   createfin
    jmp  createloop2

createfin:
    pop  cx

    push cx

    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

    pop  cx                             ; Number of sprite
    cmp  byte ptr [ si.sp_visi ],0
    je   notvisible
    mov  ax,[ si.sp_orgx ]              ; Coordenadas x
    mov  bx,[ si.sp_orgy ]              ;             y
    call putspr
notvisible:

    pop ds
	popa
    sti
    ret

sprmaxx dw  ?
sprmaxy dw	?
sprx	dw	?
sprx2   dw  ?
spry    dw  ?

createspr endp

;---------------------------;
;		PutSpr				;
;---------------------------;
;	Put sprite n in (x,y)	;
;---------------------------;
;	In: 					;
;		ax	= x 			;
;		bx	= y 			;
;		cx	= Num of Sprite ;
;---------------------------;
putspr proc near
    cli
    pusha
	push ds

	push cs
	pop ds

    push cx
    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

    mov [ si.sp_nowx ],ax           ; Pone al spr donde se encuentra. X
    mov [ si.sp_oldx ],ax
    mov [ si.sp_nowy ],bx           ; Pone al spr donde se encuentra. Y
    mov [ si.sp_oldy ],bx

    pop cx
    call gsprbkg                ; Get Sprite CX Background

    xor  dh,dh
    mov  dl,[ si.sp_offs ]

    mov  tox,ax
    mov  toy,bx
    mov  frompage,3

    mov  ax,[ si.sp_tamx ]
    mov  pwidth,ax              ; Max x del spr
    mov  bx,[ si.sp_orgx ]
    mov  cl,[ si.sp_offs ]
    mul  cl
    add  bx,ax                  ; Sera esto solo ?
    mov  fromx,bx               ; x org del spr
    mov  ax,[ si.sp_orgy ]
    mov  fromy,ax               ; y org del spr
    mov  al,[ si.sp_page]
    mov  byte ptr topage,al
    mov  ax,[ si.sp_tamy ]
    mov  pheight,ax             ; Max y del spr

    mov  bx,[ si.sp_mask ]      ; todo esto es para calcular el offset
    push bx
    mov  ax,[ si.sp_tamx ]
    mov  bx,[ si.sp_tamy ]
    mul  bx
    shr  ax,3
    mov  cl,[ si.sp_offs ]
    mul  cl
    pop  bx
    add  bx,ax
    mov  bmskp,bx               ; M�scara del sprite ( 0 = no mascara )
    call blockmove

	pop ds
	popa
    sti
    ret
putspr endp

;---------------------------;
;         PosSpr            ;
;---------------------------;
;    Posicionar Sprite      ;
;---------------------------;
;	In: 					;
;		ax	= to x			;
;		bx	= to y			;
;		cx	= N of Sprite	;
;---------------------------;
posspr proc near
    cli
    pusha
    push ds

    push cs
    pop  ds

    push cx
    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]				; Get Sprite Structure
    pop  cx

    push ax
    or   ax,ax                      ; evitar dividir por 0
    je   nodiv0

    mov  dl,4
;   idiv dl                        ; si hay problemas usar este!
    div dl                         ; Dividir AX/4

nodiv0:
    mov  [ si.sp_offs ],ah          ; El Reminder va en AH.
    pop  ax

poshere1:

    mov  [ si.sp_nowx ],ax          ; To x
    mov  [ si.sp_nowy ],bx          ; To y

    pop  ds
    popa
    sti
    ret
posspr endp


;---------------------------;
;       MoveSprites         ;
;---------------------------;
;      Move Sprites         ;
;---------------------------;
;   In:                     ;
;       cx  = Max of Spr    ;
;---------------------------;
movesprites proc near
    cli
    pusha
    push ds

    push cs
    pop  ds

    inc  cx                     ; ( 1 - x ) en vez de ( 0 - (x-1) )

    mov totspr,cx               ; Guardar CX.

moveloop1:
    mov  totsprv,cx             ; Contador del loop

    dec  cx                     ; Este valor es temporal a nivel loop.
                                ; Va uno m�s abajo que el cx del loop.
    push cx
    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]				; Get Sprite Structure

    mov  cl,[ si.sp_visi ]
    or   cl,cl
    jne  movesprhere1           ; Es visible ? si entonces todo.

    pop  cx
    jmp  movesprhere2

movesprhere1:
    mov  cl,[ si.sp_page ]
    xor  cl,1
    and  cl,1                   ; invierte el valor de [ si + 8 ]
    mov  [ si.sp_page ],cl
    call setpage                ; Setea la otra p�gina para trabajar.

    pop  cx
    call gsprbkg                ; Get Sprite Background [ si + 4 ], [ si + 6 ]

    mov  ax,[ si.sp_nowx ]      ;
    mov  tox,ax                 ;
    mov  bx,[ si.sp_nowy ]      ;
    mov  toy,bx                 ;

    mov  frompage,3             ;
    mov  ax,[ si.sp_tamx ]
    mov  pwidth,ax              ; Max x del spr
    mov  bx,[ si.sp_orgx ]
    mov  cl,[ si.sp_offs ]
    mul  cl
    add  bx,ax                  ; Sera esto solo ?
    mov  fromx,bx               ; x org del spr
    mov  ax,[ si.sp_orgy ]
    mov  fromy,ax               ; y org del spr
    mov  al,[ si.sp_page ]
    mov  byte ptr topage,al     ; Tener en cuenta que la p�gina var�a.
    mov  ax,[ si.sp_tamy ]
    mov  pheight,ax             ; Max y del spr

    mov  bx,[ si.sp_mask ]      ; todo esto es para calcular el offset
    push bx
    mov  ax,[ si.sp_tamx ]      ; de la mascara.
    mov  bx,[ si.sp_tamy ]
    mul  bx
    shr  ax,3                   ; ancho por largo / 8
    mov  cl,[ si.sp_offs ]
    mul  cl
    pop bx
    add  bx,ax
    mov  bmskp,bx               ; M�scara del sprite ( 0 = no mascara )

    call blockmove
movesprhere2:                   ; si no es visible no hace todo eso!
    mov  cx,totsprv
loop moveloop1_                 ; ---- FIN DE LA PRIMERA PARTE.
    jmp parche

moveloop1_:
    jmp moveloop1


parche:
    mov cl,[ si.sp_page ]
    call showpage               ; Mostrar la otra p�gina.

    mov cx,0                    ; Empezar del Sprite 0 ( de adelante para atras )
moveloop2:
    mov  totsprv,cx

    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]				; Get Sprite Structure

    mov  cl,[ si.sp_visi ]
    or   cl,cl
    jne  movesprhere3

    jmp  movesprhere4
movesprhere3:


    mov al,[ si.sp_page ]       ; Trabajar con el sprite de la otra p�gina.
    xor al,1
    and al,1
    mov [ si.sp_page],al

    mov  dx,[ si.sp_nowx ]      ; To x
    push dx
    mov  dx,[ si.sp_nowy ]      ; To y
    push dx

    mov  ax,[ si.sp_oldx ]
    mov  bx,[ si.sp_oldy ]
    mov  [ si.sp_nowx ],ax      ; Old x
    mov  [ si.sp_nowy ],bx      ; Old y

    mov  cx,totsprv
    call rsprbkg
    pop bx
    pop ax
    mov  [ si.sp_nowx ],ax      ; To x  : new
    mov  [ si.sp_oldx ],ax      ;       : old
    mov  [ si.sp_nowy ],bx      ; To y  : new
    mov  [ si.sp_oldy ],bx      ;       : old

    mov al,[ si.sp_page ]       ; Poner al sprite en la p�gina OK.
    xor al,1
    and al,1
    mov [ si.sp_page ],al

movesprhere4:
    mov cx,totsprv              ; LOOP 2
    inc cx
    cmp cx,totspr
    jne moveloop2              ; ------: FIN

    pop ds
    popa
    sti
    ret

totspr  dw ?
totsprv dw ?

movesprites endp

;---------------------------;
;		GSprBkg 			;
;---------------------------;
;	Get sprite background	;
;---------------------------;
;	In: 					;
;		cx	= N of Sprite	;
;---------------------------;
gsprbkg proc near
    cli
    pusha
    push ds

    push cs
    pop ds

    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

    mov ax,[ si.sp_nowx ]
    mov fromx,ax                ; de donde ? x
    mov ax,[ si.sp_nowy ]
	mov fromy,ax				; de donde ? y
    mov al,[ si.sp_page ]       ; From pagina
    mov byte ptr frompage,al    ;
	mov topage,2				; To page

    mov bx,[ si.sp_tamxx ]
    mov pwidth,bx               ; Max x del spr
    mov ax,[ si.sp_tamyy ]
	mov pheight,ax				; Max y del spr

    mov ax,[ si.sp_orgxx]       ;
;    mov ax,[ si.sp_orgx ]       ;
    cmp byte ptr [ si.sp_page ],0
    je frompage0
    add ax,bx                   ; Si es p�gina 1, then tox = orgx + maxx
frompage0:
    mov tox,ax                  ; To x - org
    mov ax,[ si.sp_orgyy]       ;
;    mov ax,[ si.sp_orgy ]       ;
	mov toy,ax					; To y - org
	mov bmskp,0 				; M�scara del sprite ( 0 = no mascara )
	call blockmove

    pop ds
    popa
    sti
    ret
gsprbkg endp

;---------------------------;
;		RSprBkg 			;
;---------------------------;
; Restore sprite background ;
;---------------------------;
;	In: 					;
;		cx	= N of Sprite	;
;---------------------------;
rsprbkg proc near
    cli
    pusha
    push ds

    push cs
    pop ds

	shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

    mov ax,[ si.sp_nowx ]       ;
    mov tox,ax                  ; a donde ? x
    mov ax,[ si.sp_nowy ]       ;
	mov toy,ax					; a donde ? y
    mov al,[ si.sp_page ]       ; a pagina
    mov byte ptr topage,al      ;
	mov frompage,2				; de page

    mov bx,[ si.sp_tamxx ]
    mov pwidth,bx               ; Max x del spr
    mov ax,[ si.sp_tamyy ]
	mov pheight,ax				; Max y del spr


    mov ax,[ si.sp_orgxx ]      ;
    cmp byte ptr [ si.sp_page ],0
    je  frompage02
    add ax,bx
frompage02:
    mov fromx,ax                ; de x - org
    mov ax,[ si.sp_orgyy ]      ;
	mov fromy,ax				; de y - org
	mov bmskp,0 				; M�scara del sprite ( 0 = no mascara )
	call blockmove

    pop ds
	popa
    sti
    ret
rsprbkg endp

;---------------------------;
;       SprImage            ;
;---------------------------;
;      Sprite Image         ;
;---------------------------;
;	In: 					;
;       cx  = to Sprite     ;
;       dx  = from Sprite   ;
;---------------------------;
sprimage proc near
    cli
    pusha
    push ds

    push cs
    pop ds

    shl  dx,1                       ; From Sprite
	mov  di,offset spritestruc
    add  di,dx
	mov  si,[ di ]

    mov  ax,[ si.sp_orgxx ]
    push ax
    mov  ax,[ si.sp_orgyy ]
    push ax
    mov  ax,[ si.sp_tamxx ]
    push ax
    mov  ax,[ si.sp_tamyy ]
    push ax
    mov  ax,[ si.sp_maskk ]
    push ax

    shl  cx,1                       ; To Sprite
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

    pop  ax
    mov  [ si.sp_mask ],ax
    pop  ax
    mov  [ si.sp_tamy ],ax
    pop  ax
    mov  [ si.sp_tamx ],ax
    pop  ax
    mov  [ si.sp_orgy ],ax
    pop  ax
    mov  [ si.sp_orgx ],ax

    pop  ds
    popa
    sti
    ret
sprimage endp

;*********************************************************************;
;*	  Author		 : Michael Tischer								 *;
;*	  Developed on	 : 09/05/90 									 *;
;*	  Last update	 : 02/17/92 									 *;
;*********************************************************************;
;                                                                     ;
; Modificada en 1994 por Ricardo Quesada.                             ;
; Esta funci�n fue modifica para su mejor interfase                   ;
;                                                                     ;
;---------------------------------------------------------------------;

;== Constants =========================================================

SC_INDEX	   = 3c4h			  ;Index register for sequencer ctrl.
SC_MAP_MASK    = 2				  ;Number of map mask register
SC_MEM_MODE    = 4				  ;Number of memory mode register

GC_INDEX	   = 3ceh			  ;Index register for graphics ctrl.
GC_GRAPH_MODE  = 5				  ;Number of graphic mode register

VERT_RESCAN    = 3DAh			  ;Input status register #1
PIXX		   = 320			  ;Horizontal resolution
;== Program ============================================================

additive   dw ? 				  ;Local variable
restc	   dw ?
movec	   dw ?
frompage   dw ? 				  ;From page
fromx	   dw ? 				  ;From X-coordinate
fromy	   dw ? 				  ;From Y-coordinate
topage	   dw ? 				  ;To page
tox 	   dw ? 				  ;To X-coordinate
toy 	   dw ? 				  ;To Y-coordinate
pwidth	   dw ? 				  ;Pixel width
pheight    dw ? 				  ;Pixel height
bmskp	   dw ? 				  ;Pointer to buffer with bit mask


;-- BLOCKMOVE: Moves a group of pixels in video RAM -------------------
;-- Declaration :
;		In: Parametros pasados en variables de prog.
;	   Out: None
;----------------------------------------------------------------------
blockmove proc near
       cli
	   pusha
	   push  ds
	   push  es

	   mov	 dx,GC_INDEX	  ;Get current write mode and
	   mov	 al,GC_GRAPH_MODE ;initialize write mode 1
	   out	 dx,al
	   inc	 dx
	   in	 al,dx
	   push  ax 			  ;Push current mode onto stack
	   and	 al,not 3
	   or	 al,1
	   out	 dx,al

	   mov	 al,4			  ;Move DS to start of FROM page
	   mov	 cl,byte ptr frompage
	   mul	 cl
	   or	 al,0A0h
	   xchg  ah,al
	   mov	 ds,ax

	   mov	 al,4			  ;Move ES to start of TO page
	   mov	 cl,byte ptr cs:topage
	   mul	 cl
	   or	 al,0A0h
	   xchg  ah,al
	   mov	 es,ax

	   mov	 ax,PIXX / 4	  ;Move SI to FROM starting position
	   mul	 cs:fromy
	   mov	 si,cs:fromx
	   shr	 si,1
	   shr	 si,1
	   add	 si,ax

	   mov	 ax,PIXX / 4	  ;Move DI to TO position
	   mul	 cs:toy
	   mov	 di,cs:tox
	   shr	 di,1
	   shr	 di,1
	   add	 di,ax

	   mov	 dh,byte ptr cs:pheight ;DH = Pixel lines
	   mov	 dl,byte ptr cs:pwidth	;DL = Bytes
	   shr	 dl,1
	   shr	 dl,1

	   mov	 bx,PIXX / 4	  ;Move BX as offset to next line
	   sub	 bl,dl
	   xor	 ch,ch			  ;High byte of counter is always 0
	   cmp	 cs:bmskp,0 	  ;No background?
	   jne	 mt2			  ;None, use other copy routine

	   push  dx 			  ;Push DX onto stack
	   mov	 dx,SC_INDEX	  ;Get bitplane access
	   mov	 ah,0Fh
	   mov	 al,SC_MAP_MASK
	   out	 dx,ax
	   pop	 dx 			  ;Pop DX

	   ;-- Copy routine for all four bitplanes,
	   ;-- without checking the background

mt1:   mov	 cl,dl			  ;Number of bytes to CL

	   rep movsb			  ;Copy lines
	   add	 di,bx			  ;DI and
	   add	 si,bx			  ;SI in next line
	   dec	 dh 			  ;Still more lines?
	   jne	 mt1			  ;Yes --> Continue
	   jmp short mtend		  ;No --> Get out of routine

	   ;-- Copy routine for individual bitplanes using
	   ;-- the specified bit mask arrays

mt2:
	   mov	 byte ptr cs:restc,dh ;First specify variables
	   mov	 byte ptr cs:movec,dl ;placed in local variables
	   mov	 cs:additive,bx 	  ;on the stack

	   mov	 al,SC_MAP_MASK   ;Address permanent
	   mov	 dx,SC_INDEX	  ;Map mask register
	   out	 dx,al
	   inc	 dx 			  ;Increment DX on data register

	   mov	 bx,cs:bmskp	  ;BX is pointer to bit mask array
	   push  ds
	   push  cs
	   pop	 ds 			  ; antiguamente  mov ds,cs:data_seg
	   mov	 al,[bx]		  ;Load first byte		tiene que ir [bx]
	   xor	 ah,ah			  ;Start with an even byte
	   pop	 ds

mt3:   mov	 cl,cs:byte ptr movec ;Move number of bytes to CL

mt4:   out	 dx,al			  ;Set bit mask
	   movsb				  ;Copy 4 bytes

	   inc	 ah 			  ;Increment odd/even counter
	   test  ah,1			  ;Odd again?
	   jne	 mt5			  ;Yes --> Move nibble

	   ;-- Get next byte from buffer on every even byte -----------

	   inc	 bx 			  ;BX to next bit mask byte
	   push  ds
	   push  cs
	   pop	 ds 			  ; antiguamente, mov ds,cs:data_seg
	   mov	 al,[bx]		  ;Load next byte		tiene que ir [bx]
	   pop	 ds
	   loop  mt4			  ;Next four latches
	   jmp	 short mt6

mt5:   shr	 al,1			  ;Get odd byte bit mask from
	   shr	 al,1			  ;low nibble
	   shr	 al,1
	   shr	 al,1
	   loop  mt4			  ;Next four latches

mt6:   add	 di,cs:additive 	 ;Add DI and
	   add	 si,cs:additive 	 ;SI to next line
	   dec	 byte ptr cs:restc	 ;Still more lines?
	   jne	 mt3				 ;Yes --> Continue

mtend: mov	 dx,GC_INDEX	  ;Revert to old
	   pop	 ax 			  ;write mode
	   mov	 ah,al
	   mov	 al,GC_GRAPH_MODE
	   out	 dx,ax

	   pop	 es
	   pop	 ds
	   popa
       sti
	   ret
blockmove endp

;---------------------------;
;		Estructuras 		;
;---------------------------;
sprstruc struc
	sp_orgx dw	?				; pos original x
	sp_orgy dw	?				; pos original y
	sp_nowx dw	?				; ubicaci�n de ahora x
	sp_nowy dw	?				; ubicaci�n de ahroa y
	sp_page db	?				; p�gina donde esta
	sp_tamx dw	?				; tama�o x
	sp_tamy dw	?				; tama�o y
	sp_dato dw	?				; datos del sprite 1
	sp_mask dw	?				; datos de la m�scara.
	sp_oldx dw	?				; ubicaci�n old de x
	sp_oldy dw	?				; ubicaci�n old de y
	sp_visi db	?				; sprite visible al crearse ?
	sp_offs db	?				; offset del sprite (0-3)
	sp_orgxx dw ?				; Copias de orgx
	sp_orgyy dw ?				;			orgy
	sp_tamxx dw ?				;			tamx
	sp_tamyy dw ?				;			tamy
	sp_maskk dw ?				;			mask
sprstruc ends


