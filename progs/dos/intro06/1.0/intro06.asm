;-----------------------------------------------;
;												;
;                   Intro 06                    ;
;           El Ni�o del Sol (c) 1995.           ;
;												;
;-----------------------------------------------;
;       Tercera intro en modo gr�fico           ;
;               Hecha para scrolls              ;
;-----------------------------------------------;
P286

VERT_RESCAN    = 3DAh             ;Input status register #1


code	segment public byte 'code'
org 	100h
intro05 PROC
assume cs:code,ds:code,ss:code,es:code

;---------------------------;
start:
;---------------------------;
        call releasemem
        call is286
        call isvga
        call mainprogram

        mov ax,3
        int 10h             ;volver al modo texto.

        mov ax,4c00h
        int 21h

;---------------------------;
;        releasemem         ;
;---------------------------;
releasemem  proc near
        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***
        ret
releasemem endp

;---------------------------;
;		Is286				;
;---------------------------;
is286 proc near
        xor ax,ax
        push ax
        popf
        pushf
        pop ax
        and ax,0f000h
        cmp ax,0f000h
        je not286
        ret
not286:
        mov dx,offset no286msg
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc near
		mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near

        cli

        mov ax,0eh
        int 10h


        mov ah,12h
        mov bl,32h
        mov al,0                ; al=1 disable
        int 10h                 ;*** Enable CPU access to video Ram ***


        mov al,0ffh
        out 21h,al

        call changedac

;        call displaytitle   ;


mainhere1:
        call scrolltitle1
        call scrolltitle2
        call scrollhoriz1
        call scrollhoriz2
        call scrollhoriz3
        call scrollhoriz4

        call verticalr
        call getkey
        jne  mainhere1

        mov al,0h
        out 21h,al

        ret
mainprogram endp

;---------------------------;
;   changedac               ;
;---------------------------;
changedac   proc near
        push cs
        pop  ds
        mov al,0                    ; a partir de que color
        mov dx,3c8h
        out dx,al
        inc dx
        mov cx,16 * 3               ; hasta que color
        mov si,offset colores1      ; fuente de los colores
dacloop:
        mov al,[si]
        out dx,al
        inc si
        loop dacloop
        ret
changedac endp

colores1 label byte
        db 00,00,00             ; 0000
        db 63,63,63             ; 0001
        db 32,32,32             ; 0010
        db 63,63,63             ; 0011
        db 16,16,16             ; 0100
        db 63,63,63             ; 0101
        db 32,32,32             ; 0110
        db 63,63,63             ; 0111
        db 08,08,08             ; 1000
        db 63,63,63             ; 1001
        db 32,32,32             ; 1010
        db 63,63,63             ; 1011
        db 16,16,16             ; 1100
        db 63,63,63             ; 1101
        db 32,32,32             ; 1110
        db 63,63,63             ; 1111


;---------------------------;
;		VerticalR			;
;---------------------------;
verticalr proc near
        mov   dx,VERT_RESCAN   ;Wait for end of
ula1:
        in    al,dx            ;vertical rescan
        test  al,8
        jne   ula1
ula2:
        in    al,dx            ;Go to start of rescan
        test  al,8
        je    ula2
        ret
verticalr endp


;---------------------------;
;   ScrollHoriz1            ;
;---------------------------;
scrollhoriz1 proc near
        mov ax,cs
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,cs
        mov ds,ax

        mov si,offset scrollbuffer +  ( 80 * 50 )
        mov cx,80 * 50
scrollshiftl:
        rcl byte ptr [ si ],1
        dec si
        loop scrollshiftl

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret

scrollbuffer    db 80  * 51 dup ( 0 )

scrollhoriz1 endp

;---------------------------;
;   ScrollHoriz2            ;
;---------------------------;
scrollhoriz2 proc near
        mov ax,cs
        mov es,ax
        mov di,offset scrollbuffer2
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 100             ; del medio al final
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,cs
        mov ds,ax

        mov si,offset scrollbuffer2
        mov cx,80 * 50
scrollshiftr:
        rcr byte ptr [ si ],1
        inc si
        loop scrollshiftr

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer2
        mov di, 80 * 100        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret

scrollbuffer2    db 80  * 51 dup ( 0 )

scrollhoriz2 endp


;---------------------------;
;   ScrollHoriz3            ;
;---------------------------;
scrollhoriz3 proc near
        mov ax,cs
        mov es,ax
        mov di,offset scrollbuffer3
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0104h                ; read from plane 2 ( = 1 )
        out dx,ax
        cld
        rep movsb

        mov ax,cs
        mov ds,ax

        mov si,offset scrollbuffer3 +  ( 80 * 50 )
        mov cx,80 * 50
scrollshiftl2:
        rcl byte ptr [ si ],1
        dec si
        loop scrollshiftl2

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer3
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret

scrollbuffer3   db 80  * 51 dup ( 0 )

scrollhoriz3 endp

;---------------------------;
;   ScrollHoriz4            ;
;---------------------------;
scrollhoriz4 proc near
        mov ax,cs
        mov es,ax
        mov di,offset scrollbuffer4
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 100             ; del medio al final
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0104h                ; read from plane 2 ( = 1 )
        out dx,ax
        cld
        rep movsb

        mov ax,cs
        mov ds,ax

        mov si,offset scrollbuffer4
        mov cx,80 * 50
scrollshiftr2:
        rcr byte ptr [ si ],1
        inc si
        loop scrollshiftr2

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer4
        mov di, 80 * 100        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret

scrollbuffer4    db 80  * 51 dup ( 0 )

scrollhoriz4 endp


;---------------------------;
;   ScrollTitle1            ;
;---------------------------;
scrolltitle1 proc near

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 1
        out dx,ax

        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,80 * 149
        mov di,80 * 150
        mov cx,80 * 100
        std
        mov dx,3ceh
        mov ax,0105h        ; Read Mode 0, Write mode 1
        out dx,ax
        rep movsb

        ret
scrolltitle1 endp

;---------------------------;
;   ScrollTitle2            ;
;---------------------------;
scrolltitle2 proc near

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,80 * 51
        mov di,80 * 50
        mov cx,80 * 100
        cld
        mov dx,3ceh
        mov ax,0105h        ; Read Mode 0, Write mode 1
        out dx,ax
        rep movsb

        mov ax,cs
        mov ds,ax
        mov bx,scrollinc1

        xor cx,cx               ; un contador.
scrolltutto:
        lea si,scrolltxt
        xor ah,ah
        mov al,[ si + bx ]
        or al,al
        jne scrollno0

        xor bx,bx
        mov scrollinc1,bx
        mov scrollinc3,bx

scrollno0:
        shl ax,4                ; multiplica por 16 ( offset de los chars )
        add ax,offset tabla     ; Poscionado en los datos del char!
        add ax,scrollinc2       ; ( incremento de la altura ) elejir entre 0-15

        lea si,scrollbytes
        add si,cx               ; Que byte ?
        xchg ax,di
        mov al,[ di ]
        mov [ si ],al           ; ds:si source del dato
        inc bx
        inc cx
        cmp cx,40
        jb  scrolltutto

        lea si,scrollbytes
        mov di,80 * 150 + 20 ; linea 200 , centrado.

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,40
        rep movsb               ;mueve todo el bloque


;
        mov ax,cs
        mov es,ax
        mov di,offset scrollbytes2
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 1               ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,cs
        mov ds,ax

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbytes2
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 1
        rep movsb               ;mueve todo el bloque


        inc scrollinc2
        and scrollinc2,15

        cmp scrollinc2,0
        jne scrollend

        add scrollinc1,40

scrollend:
        ret

scrollinc1  dw  0               ; offset del texto  ( 1 - ... )
scrollinc2  dw  0               ; offset del char ( 0 - 15 ) [ alto de los chars )
scrollinc3  dw  0               ; offset de los bytes a pasar (0 - 79 )
scrollbytes db 40 dup (0)

scrollbytes2    db 82 dup(0)    ; Buffer para copia Bitplane 1 a 2

scrolltitle2 endp


;---------------------------;
;   DisplayTitle            ;
;---------------------------;
displaytitle proc near

;BITPLANE 1
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax
        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov ax,0a000h           ;destiny
        mov es,ax
        xor di,di

        mov ax,cs               ;origin
        mov ds,ax
        xor si,si

        mov cx,10000
        rep movsb               ;mueve todo el bloque


; BITPLANE 2
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax
        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov ax,0a000h           ;destiny
        mov es,ax
        mov di,80 * 225

        mov ax,cs               ;origin
        mov ds,ax
        xor si,si

        mov cx,10000
        rep movsb               ;mueve todo el bloque


        ret
displaytitle endp


;---------------------------;
;		getkey				;
;---------------------------;
getkey proc near
        push ax
;        mov  ah,1
;        int  16h

        in  al,060h
        cmp al,01h
        pop ax
	ret
getkey endp

;---------------------------;
;           TXT             ;
;---------------------------;
vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'
svgamsg  db 13,10,13,10,7,'Your video card is not fast enough !',13,10,'Continue anyway (Y/N) ? $'

scrolltxt label byte
db'��������������������������������������¿'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'����������������������������������������'
db'�                                      �'
db'�                                      �'
db'�                                      �'
db'�     Eso no fue una propaganda del    �'
db'�                                      �'
db'�           plan rombo ni de           �'
db'�                                      �'
db'�               Renault.               �'
db'�                                      �'
db'�                                      �'
db'�        Es la presentaci�n de         �'
db'�                                      �'
db'�                                      �'
db'�                                      �'
db'�                                      �'
db'�                                      �'
db'�                 T h e                �'
db'�                                      �'
db'�            ',9,10,'          �'
db'�            *          �'
db'�                                      �'
db'�                by Riq                �'
db'�                                      �'
db'�                                      �'
db'�                                      �'
db'� Se empez� con el proyecto en el a�o  �'
db'�                                      �'
db'� 1988 con la gloriosa Commodore 128.  �'
db'�                                      �'
db'� En ese a�o surgi� la primera versi�n �'
db'�                                      �'
db'� que fue muy popular en mi casa...    �'
db'�                                      �'
db'� El Sex Games II, fue un poquito m�s  �'
db'�                                      �'
db'� elaborado, pero la fama la consigui� �'
db'�                                      �'
db'� en 1989 cuando finalmente sali� el   �'
db'�                                      �'
db'� Sex Games III, con tipos de 3 o 4    �'
db'�                                      �'
db'� sprites, y algunos chiches m�s...    �'
db'�                                      �'
db'� Ahora, desp�es de haber estado medio �'
db'�                                      �'
db'� diciembre del 94, trabajando en las  �'
db'�                                      �'
db'� rutinas de sprites, me animo a mos-  �'
db'�                                      �'
db'� trarles la primera versi�n para PC.  �'
db'�                                      �'
db'�                . . .                 �'
db'�                                      �'
db'� Saludos: A Nacho Paz (un gran amigo),�'
db'� a Javi Victorica, a Emy Bunge, a Tati�'
db'� Cichero, a Edy Mac Harg, a Gonchi    �'
db'� Fernandez Favaron, a Charly Tavares, �'
db'� (que son grandes amigos y compa�eros �'
db'� de colegio ), a Viole ( mi hermana   �'
db'� querida ), a mis padres, a Johny     �'
db'� Lough ( un gran amigo ), a la Lucky  �'
db'� Principe ( que fue uno de los pocos  �'
db'� que conoci� el debut del Sex Games ) �'
db'� ,a toda la gente que apoya y difunde �'
db'� el Assembler, al Toro Jaacks (con su �'
db'� Zenith Cumbre),a Archon, (mi querido �'
db'� Boss ), a The Last Hackers (que hacen�'
db'� espectaculares intros), a mi abuela  �'
db'� (mi querida Carlota Madre ) y a todas�'
db'� las personas que se decidan a leer   �'
db'� scrolls ( eso es para vos amigo!)    �'
db'�                                      �'
db'�                . . .                 �'
db'�                                      �'
db'� Acerca del programa: Fue totalmente  �'
db'� hecho en Turbo Assembler 3.1, sin    �'
db'� ninguna libreria. Las rutians de los �'
db'� sprites estan basadas en la Commodore�'
db'� 64/128 y en funciones de M.Tisher.   �'
db'� Esto significa que observ� como fun- �'
db'� cionan los mencionados, y luego los  �'
db'� hice yo solo en mi PC mezclando ideas�'
db'� de ambos. El juego en si es muy pare-�'
db'� cido al Sex Games III que hice hace  �'
db'� mucho tiempo atras.                  �'
db'�                                      �'
db'�                . . .                 �'
db'�                                      �'
db'� Herramientas utilizadas: Tasm 3.1,   �'
db'� Tlink 5.1, MGraph 3.5, VChar NI 3.33,�'
db'� y hasta ah� llegu�...                �'
db'�                                      �'
db'�                . . .                 �'
db'�                                      �'
db'� Acerca del autor: No soy ning�n sexo-�'
db'� pata ni nada parecido, solo que me   �'
db'� divierte hacer este tipo de cosas.   �'
db'� Tuve mi primer computadora a mediados�'
db'� de 1986 ( mi primera Commodore 64),  �'
db'� donde hacia algunas cositas en Basic.�'
db'� Al cabo de 2 a�os empec� con el ASM. �'
db'� Me gust� bastante y al a�o siguiente �'
db'� tuve mi primera Commodore 128 ( la   �'
db'� cual todav�a sigue enchufada al lado �'
db'� de mi PC )... Empec� a hacer cositas �'
db'� con el Basic y ASM. En el a�o 1990   �'
db'� hice mi primer fucking intro para la �'
db'� C=64, ese a�o hice el juego "EL CRUCE�'
db'� " y mi primer editor de chars, al    �'
db'� a�o siguiente sacaria Race, Roketo 2,�'
db'� Fire Him, el Vuelo, y otras cositas  �'
db'� para terminar en el 92 con The Race( �'
db'� mi consagraci�n en la C=128), CharDef�'
db'� v2.32(un gran editor de Chars) y     �'
db'� Puncher(un juego de boxeo programable�'
db'� ). En ese momento hacia el servicio  �'
db'� militar. A fin de a�o mi padre se    �'
db'� compr� una PC (386Dx40 con Hercules).�'
db'� Ser�a el fin de mi programaci�n para �'
db'� el 6502/6510/8502. Empec� con ASM en �'
db'� la PC, ya que era el �nico lenguaje  �'
db'� que m�s o menos sab�a. Al principio  �'
db'� del 93 hice mis primeros residentes. �'
db'� Al tiempo mi suegro ( hoy es mi ex-  �'
db'� suegro) me pidi� un residente que    �'
db'� capture imagenes gr�ficas y ah� empe-�'
db'� c� con el Savepic. El problema era   �'
db'� que el que tenia PC era mi padre,    �'
db'� y encima con Hercules y yo tenia que �'
db'� ir ah� a prograr. Me compre un 286   �'
db'� con SVGA ( la idea era que �l se que-�'
db'� de con la 286 y me de a mi la 386 ). �'
db'� Eso hasta el d�a de hoy no pudo ser  �'
db'� posible. En el 94 empec� a programar �'
db'� en C. Hice un terminal con Turbo     �'
db'� Vision y otras boludeces y en Julio  �'
db'� empec� con el VChar ( un lindo pgm   �'
db'� para modificar chars ). En Octubre   �'
db'� hice mi primer Intro para PC y en    �'
db'� Diciembre hice la 5ta con las ruti-  �'
db'� nas de sprites que usa este juego.   �'
db'� Todo Enero del 95 estuve afuera cono-�'
db'� ciendo el viejo continente y ahora   �'
db'� de regreso estoy haciendo el The Sex �'
db'� Games con mi 286 que muy pero muy    �'
db'� pronto pasara a ser historia.        �'
db'�                                      �'
db'�                                      �'
db'�                                      �'
db'�   H A S T A   L A   P R O X I M A    �'
db'�                                      �'
db'�                            Riq       �'
db'�                       ( el autor )   �'
db'�                                      �'
db'�                                      �'
db'����������������������������������������',0

;---------------------------;
;		Includes			;
;---------------------------;
include font0000.inc

final   equ this byte

intro05 endp
code	ends
		end intro05
