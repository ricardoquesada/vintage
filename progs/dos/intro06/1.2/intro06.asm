;-----------------------------------------------;
;												;
;                   Intro 06                    ;
;           El Ni�o del Sol (c) 1995.           ;
;												;
;-----------------------------------------------;
;       Tercera intro en modo gr�fico           ;
;               Hecha para scrolls              ;
;                 .EXE version                  ;
;-----------------------------------------------;
P286

;DEFINES
VERT_RESCAN    = 3DAh             ;Input status register #1

;== Stack ==============================================================
stackseg  segment para STACK 'STACK'      ;Definition of stack segment
          dw 100h dup (?)          ;The stack comprises 256 words
stackseg  ends                     ;End of stack segment 


;== Data ===============================================================
data      segment para 'DATA'     ;Definition of data segment 

colores1 label byte             ; colores usados en changedac
        db 00,00,00             ; 0000
        db 63,63,63             ; 0001
        db 32,32,32             ; 0010
        db 63,63,63             ; 0011
        db 16,16,16             ; 0100
        db 63,63,63             ; 0101
        db 32,32,32             ; 0110
        db 63,63,63             ; 0111
        db 08,08,08             ; 1000
        db 63,63,63             ; 1001
        db 32,32,32             ; 1010
        db 63,63,63             ; 1011
        db 16,16,16             ; 1100
        db 63,63,63             ; 1101
        db 32,32,32             ; 1110
        db 63,63,63             ; 1111

scrollbuffer    db 80  * 51 dup ( 0 )   ; buffer de scrollhoriz1
scrollbuffer2    db 80  * 51 dup ( 0 )
scrollbuffer3   db 80  * 51 dup ( 0 )
scrollbuffer4    db 80  * 51 dup ( 0 )


scrollinc1  dw  0               ; offset del texto  ( 1 - ... )
scrollinc2  dw  0               ; offset del char ( 0 - 15 ) [ alto de los chars )
scrollinc3  dw  0               ; offset de los bytes a pasar (0 - 79 )
scrollbytes db 60 dup (0)
scrollbytes2    db 82 dup(0)    ; Buffer para copia Bitplane 1 a 2


include font0000.inc            ; fuente de los fonts
include  fire.inc               ; Paletea ( color dac )

indice2 dw 0                    ; indices del rain8
indice  dw 0
indice3 dw 0
indice4 dw 0

vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'
svgamsg  db 13,10,13,10,7,'Your video card is not fast enough !',13,10,'Continue anyway (Y/N) ? $'

scrolltxt label byte
db'             dif              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             e e              '
db'             gjh              '
db'                              '
db'                              '
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e                            e'
db'e  ESO NO FUE UNA PROPAGANDA e'
db'e                            e'
db'e     DEL PLAN ROMBO NI DE   e'
db'e                            e'
db'e           RENAULT.         e'
db'e                            e'
db'e                            e'
db'e   ES LA PRESENTACION DE    e'
db'e                            e'
db'e            THE             e'
db'e                            e'
db'e     nopqrs  tuvwxypqno     e'
db'e            e'
db'e                            e'
db'e           V1.00            e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e                            e'
db'e BASADO EN EL SEX GAMES III e'
db'e      QUE HICE PARA LA      e'
db'e       COMMODORE 128        e'
db'e          EN 1989           e'
db'e                            e'
db'e       HECHO 100k EN        e'
db'e         ASSEMBLER          e'
db'e     SIN LIBRERIAS NI       e'
db'e       NADA PARECIDO        e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e HISTORIAL:                 e'
db'e     1988 - SEX GAMES       e'
db'e     1988 - SEX GAMES II    e'
db'e     1989 - SEX GAMES III   e'
db'e     1995 - THE SEX GAMES   e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e     ACERCA DEL AUTOR:      e'
db'e                            e'
db'e         m   m   m          e'
db'e                            e'
db'e NO SOY NINGUN SEXOPATA NI  e'
db'e       NADA PARECIDO.       e'
db'e                            e'
db'e         m   m   m          e'
db'e                            e'
db'e TUVE MI PRIMER COMPUTADORA e'
db'e   A MEDIADOS DE 1986. MI   e'
db'e QUERIDA COMMODORE 64, A LA e'
db'e CUAL YO AME, Y ADORE POR   e'
db'e       ALGUNOS A%OS.        e'
db'e       EN 1988 TUVE         e'
db'e MI QUERIDA COMMODORE 128,  e'
db'e   LA CUAL TODAVIA SIGUE    e'
db'e          ANDANDO.          e'
db'e   HICE VARIAS COSAS PARA   e'
db'e  ELLA, VARIAS COSAS. Y FUE e'
db'e     CON ELLA CON LA QUE    e'
db'e      APRENDI ASSEMBLER.    e'
db'e A FINES DEL 92, YO TODAVIA e'
db'e         CUMPLIENDO         e'
db'e       EL FASTIDIOSO        e'
db'e     INSERVICIO MILITAR,    e'
db'e  MI PAPI SE COMPRO UNA 386 e'
db'e        CON HERCULES        e'
db'e Y EMPEZO EN ESE MOMENTO MI e'
db'e  PROGRAMACION PARA LA PC.  e'
db'e       EMPECE CON ASM       e'
db'e   PORQUE DE HECHO ERA EL   e'
db'e  UNICO LENGUAJE QUE SABIA. e'
db'e                            e'
db'e    EMPECE HACIENDO MINI    e'
db'e  RESIDENTES, HASTA QUE MI  e'
db'e  EX-SUEGRO ( POR AHORA )   e'
db'e      ME PIDIO QUE LE       e'
db'e    HAGA UN PROGRAMA PARA   e'
db'e     CAPTURAR IMAGENES      e'
db'e          GRAFICAS.         e'
db'e   AHI HICE MI PRIMER PRG   e'
db'e        UTIL PARA PC        e'
db'e        ( SAVEPIC ).        e'
db'e  A FIN DE ESE A%O EMPECE   e'
db'e      A PROGRAMAR EN C      e'
db'e EN EL 94 HICE UN TERMINAL  e'
db'e  CON TURBO VISION QUE SI   e'
db'e   FUNCIONA ES DE MILAGRO   e'
db'e    Y A MITAD DE ESE A%O    e'
db'e     EMPECE CON EL VCHAR    e'
db'e   ( OTRO PROG QUE TIENE    e'
db'e     SUS RAICES EN LA       e'
db'e          C-128 )           e'
db'e     HICE ALGUNAS INTROS    e'
db'e   DESDE OCT. HASTA DIC.    e'
db'e         Y AHORA EN         e'
db'e   FEBRERO DEL 95 DESPUES   e'
db'e DE UNAS BUENAS VACACIONES  e'
db'e    ME ESTOY DEDICANDO A    e'
db'e HACER ESTE ENTRETENIMIENTO e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e          SALUDOS           e'
db'e                            e'
db'e ME PARECE VERY COOL m QUE  e'
db'e  EMPIECE POR NACHO PAZ,    e'
db'e      ( UN AMIGAZO )        e'
db'e   A JAVI, TATI, EMY, EDY,  e'
db'e   JOHNY, CHARLY, GONCHI,   e'
db'e  ( OTROS BUENOS AMIGOS )   e'
db'e       A LULU Y MAITE       e'
db'e    ( GOOD TRAVELMATES )    e'
db'e   A VIOLE Y CARLOTAMADRE   e'
db'e    ( PARTE DE MI FLIA )    e'
db'e          A TORO            e'
db'e   ( PERSONA VERY COOL )    e'
db'e     A ARCHON, Y ALE        e'
db'e      ( MIS BOSSES )        e'
db'e     THE LAST HACKERS       e'
db'e   ( GOOD DEMO MAKERS )     e'
db'e         Y A VOS            e'
db'e ( GRAN LECTOR DE SCROLLS ) e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e  HERRAMIENTAS UTILIZADAS:  e'
db'e                            e'
db'e        Q-EDIT V3.0         e'
db'e      TURBO LINKER V5.1     e'
db'e     TURBO ASSEMBLER V3.1   e'
db'e     TURBO DEBUGGER V3.0    e'
db'e     TURBO PROFILER V2.0    e'
db'e      MICRO GRAPHS V3.5     e'
db'e       VCHAR NI V3.33       e'
db'e          BIN2ASM           e'
db'e                            e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e                            e'
db'e  HASTA LA PROXIMA AMIGO,   e'
db'e    PRESS ESC TO START      e'
db'e       THE SEX GAMES        e'
db'e                            e'
db'e                            e'
db'e                    RIQ     e'
db'e               ( EL AUTOR ) e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              ',0
data      ends                    ;End of data segment


;== Code ===============================================================
code      segment para 'CODE'     ;Definition of CODE segment
          assume cs:code, ds:data, ss:stackseg

prog    proc far
          
        mov  ax,data            ;Load segment address of data segment
        mov  ds,ax              ;into the DS register

;        call setfree
        call is286
        call isvga
        call mainprogram

        mov ax,3
        int 10h                 ;volver al modo texto.

        mov ax,4c00h
        int 21h

prog      endp                    ;End of PROG procedure

;-- SETFREE: Release unused memory -------------------------------------
;-- Input    : ES = Address of PSP
;-- Output   : none 
;-- Registers: AX, BX, CL and FLAGS are affected
;-- Info     : Since the stack segment is always the last segment in an
;              EXE file, ES:0000 points to the beginning of the program
;              in memory, and SS:SP points to the end. This allows easy
;              calculation of the program's length.
setfree   proc near

          mov  bx,ss              ;Compute distance between
          mov  ax,es              ;PSP and stack
          sub  bx,ax

          mov  ax,sp              ;Compute stack length
          add  ax,15              ;in paragraphs
          mov  cl,4                         
          shr  ax,cl              ;Stack length

          add  bx,ax              ;Add two values of current length

          mov  ah,4ah             ;Reserve this memory only
          int  21h                ;DOS call

          ret                     ;Return to caller

setfree   endp


;---------------------------;
;		Is286				;
;---------------------------;
is286 proc near
        xor ax,ax
        push ax
        popf
        pushf
        pop ax
        and ax,0f000h
        cmp ax,0f000h
        je not286
        ret
not286:
        mov dx,offset no286msg
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc near
		mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near

        cli

        mov ax,0eh
        int 10h


        mov ah,12h
        mov bl,32h
        mov al,0                ; al=1 disable
        int 10h                 ;*** Enable CPU access to video Ram ***


        mov al,0ffh
        out 21h,al

        call changedac

mainhere1:
        call scrolltitle1
        call scrolltitle2
        call scrollhoriz1
        call scrollhoriz2
        call scrollhoriz3
        call scrollhoriz4

;        call verticalr
        call getkey
        jne  mainhere1

        mov al,0h
        out 21h,al
        ret
mainprogram endp



;---------------------------;
;   changedac               ;
;---------------------------;
changedac   proc near
        mov ax,data
        mov ds,ax                   ; ds=data
        mov al,0                    ; a partir de que color
        mov dx,3c8h
        out dx,al
        inc dx
        mov cx,16 * 3               ; hasta que color
        mov si,offset colores1      ; fuente de los colores
dacloop:
        mov al,ds:[si]
        out dx,al
        inc si
        loop dacloop
        ret
changedac endp

;---------------------------;
;   dacocena                ;
;---------------------------;
dacocean   proc near
        mov ax,data
        mov ds,ax
        cld
        mov si,offset colorpal
        mov bx,colorntx
        add si,bx
        add bx,3
        cmp bx,256 * 3
        jb  changecol
        xor bx,bx
changecol:
        mov colorntx,bx

        mov dx,03c8h
        mov al,1                        ; numero de color
        out dx,al
        inc dx
        cld
        mov cx,3
rep     outsb
        sub si,3
        mov dx,03c8h
        mov al,3                        ; numero de color
        out dx,al
        inc dx
        mov cx,3
rep     outsb

        ret
dacocean endp

;---------------------------;
;		VerticalR			;
;---------------------------;
verticalr proc near
        mov   dx,VERT_RESCAN   ;Wait for end of
ula1:
        in    al,dx            ;vertical rescan
        test  al,8
        jne   ula1
ula2:
        in    al,dx            ;Go to start of rescan
        test  al,8
        je    ula2
        ret
verticalr endp


;---------------------------;
;   ScrollHoriz1            ;
;---------------------------;
scrollhoriz1 proc near
        mov ax,data
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,data
        mov ds,ax

        mov si,offset scrollbuffer +  ( 80 * 50 )
        mov cx,80 * 50 /2
scrollshiftl:
        mov ax, [ si ]
        xchg al,ah
        rcl ax,1
        xchg al,ah
        mov [ si ], ax
        dec si
        dec si
        loop scrollshiftl

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret
scrollhoriz1 endp

;---------------------------;
;   ScrollHoriz2            ;
;---------------------------;
scrollhoriz2 proc near
        mov ax,data
        mov es,ax
        mov di,offset scrollbuffer2
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 100             ; del medio al final
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,data
        mov ds,ax

        mov si,offset scrollbuffer2
        mov cx,80 * 50 / 2
scrollshiftr:
        mov ax, [ si ]
        xchg al,ah
        rcr ax,1
        xchg al,ah
        mov [ si ], ax
        inc si
        inc si
        loop scrollshiftr

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer2
        mov di, 80 * 100        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret
scrollhoriz2 endp


;---------------------------;
;   ScrollHoriz3            ;
;---------------------------;
scrollhoriz3 proc near
        mov ax,data
        mov es,ax
        mov di,offset scrollbuffer3
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0104h                ; read from plane 2 ( = 1 )
        out dx,ax
        cld
        rep movsb

        mov ax,data
        mov ds,ax

        mov si,offset scrollbuffer3 +  ( 80 * 50 )
        mov cx,80 * 50 / 2
scrollshiftl2:
        mov ax, [ si ]
        xchg al,ah
        rcl ax,1
        xchg al,ah
        mov [ si ], ax
        dec si
        dec si
        loop scrollshiftl2

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer3
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret
scrollhoriz3 endp

;---------------------------;
;   ScrollHoriz4            ;
;---------------------------;
scrollhoriz4 proc near
        mov ax,data
        mov es,ax
        mov di,offset scrollbuffer4
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 100             ; del medio al final
        mov cx,80 * 50              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0104h                ; read from plane 2 ( = 1 )
        out dx,ax
        cld
        rep movsb

        mov ax,data
        mov ds,ax

        mov si,offset scrollbuffer4
        mov cx,80 * 50 / 2
scrollshiftr2:
        mov ax, [ si ]
        xchg al,ah
        rcr ax,1
        xchg al,ah
        mov [ si ], ax
        inc si
        inc si
        loop scrollshiftr2

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer4
        mov di, 80 * 100        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 50
        rep movsb               ;mueve todo el bloque

        ret
scrollhoriz4 endp


;---------------------------;
;   ScrollTitle1            ;
;---------------------------;
scrolltitle1 proc near

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 1
        out dx,ax

        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,80 * 149
        mov di,80 * 150
        mov cx,80 * 100
        std
        mov dx,3ceh
        mov ax,0105h        ; Read Mode 0, Write mode 1
        out dx,ax
        rep movsb

        ret
scrolltitle1 endp

;---------------------------;
;   ScrollTitle2            ;
;---------------------------;
scrolltitle2 proc near

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,80 * 51
        mov di,80 * 50
        mov cx,80 * 100
        cld
        mov dx,3ceh
        mov ax,0105h        ; Read Mode 0, Write mode 1
        out dx,ax
        rep movsb

        mov ax,data
        mov ds,ax
        mov bx,scrollinc1

        xor cx,cx               ; un contador. (cantidad de letras 0-40 aca)
scrolltutto:
        lea si,scrolltxt
        xor ah,ah
        mov al,[ si + bx ]
        or al,al
        jne scrollno0

        xor bx,bx
        mov scrollinc1,bx
        mov scrollinc3,bx

scrollno0:
        push ax                 ; Guarda el varlor para repetir con bit 7 on
        shl ax,4                ; multiplica por 16 ( offset de los chars )
        add ax,offset tabla     ; Poscionado en los datos del char!
        add ax,scrollinc2       ; ( incremento de la altura ) elejir entre 0-15

        lea si,scrollbytes
        add si,cx               ; Que byte ?
        xchg ax,di
        mov al,[ di ]
        mov [ si ],al           ; ds:si source del dato

        inc cx                  ; incrementa uno CX (contador de guardar bytes)

        pop ax
        or al,128               ; bit 7 on
        shl ax,4                ; multiplica por 16 ( offset de los chars )
        add ax,offset tabla     ; Poscionado en los datos del char!
        add ax,scrollinc2       ; ( incremento de la altura ) elejir entre 0-15
        lea si,scrollbytes
        add si,cx               ; Que byte ?
        xchg ax,di
        mov al,[ di ]
        mov [ si ],al           ; ds:si source del dato

        inc bx                  ; incrementa contador de letras del scroll
        inc cx                  ;
        cmp cx,60               ; 40 bytes almacenados ?
        jb  scrolltutto

        lea si,scrollbytes
        mov di,80 * 150 + 10 ; linea 200 , centrado.

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,60
        rep movsb               ;mueve todo el bloque


;
        mov ax,data
        mov es,ax
        mov di,offset scrollbytes2
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 1               ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,data
        mov ds,ax

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbytes2
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 1
        rep movsb               ;mueve todo el bloque


        inc scrollinc2
        and scrollinc2,15

        cmp scrollinc2,0
        jne scrollend

        add scrollinc1,30

scrollend:
        ret

scrolltitle2 endp


;---------------------------;
;		getkey				;
;---------------------------;
getkey proc near
        push ax
;        mov  ah,1
;        int  16h

        in  al,060h
        cmp al,01h
        pop ax
	ret
getkey endp

code      ends                    ;End of CODE segment
          end  prog               ;Begin execution with PROG procedure
