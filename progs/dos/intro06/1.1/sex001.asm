COMMENT |
							 Sex Games
                       Copyright 1988 - 1995
						   Ricardo Quesada

Historial:		Sex Games 1 - (c) 1988 R.Q. progs para Commodore 128
				Sex Games 2 - (c) 1988 R.Q. progs para Commodore 128
				Sex Games 3 - (c) 1989 R.Q. progs para Commodore 128
				The Sex Games - (c) 1995 El ni�o para PC

|
P286
code    segment public byte 'code'
org 	100h
sexgame PROC FAR
		assume cs:code,ds:code,ss:code,es:code



;---------------------------;
start:
;---------------------------;
		call releasemem
		call is286
		call isvga
		call issvga
		call init320200
		call mainprogram

        mov ax,3
        int 10h
        mov ax,4c00h
		int 21h 				; Return to DOS.


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near

        mov al,0ffh
		out 21h,al

        mov ah,12h
        mov bl,32h
        mov al,0                ; al=1 disable
        int 10h                 ;*** Enable CPU access to video Ram ***

		mov ax,cs
		mov ds,ax
		mov es,ax

        mov  cl,0
        call setpage
		call maintitle

mainhere1:
        call getkey
        jne  mainhere1

		mov al,0h
		out 21h,al
		ret
mainprogram endp


;---------------------------;
;	Main Title				;
;---------------------------;
maintitle proc near
        mov incolor,1
        mov ax,0
        mov cx,16
maintiloop2:
        mov bx,0
maintiloop1:
        mov dx,1
        mov di,1
        mov si,offset sgtxt0
        call displayletras
        add cx,incolor
        cmp cx,30
        je mthere2
        cmp cx,16
        jne mthere1

mthere2:
        neg incolor
mthere1:
        add bx,8
        cmp bx,180
        jb maintiloop1
        add ax,40
        cmp ax,300
        jb maintiloop2

        mov ax,105
        mov bx,64
        mov cx,1
        mov dx,0
        mov di,1
        mov si,offset sgtxt0
        call displayletras

        mov ax,85
        mov bx,84
        mov cx,4
        mov dx,0
        mov di,1
        mov si,offset sgtxt1
        call displayletras

        mov ax,75
        mov bx,104
        mov cx,2
        mov dx,0
        mov di,1
        mov si,offset sgtxt2
        call displayletras

        ret

incolor dw 1
maintitle endp

;---------------------------;
;	DATOS DE TEXTO			;
;---------------------------;
sgtxt0   db      'The Sex Games',0
sgtxt1   db    'by Ricardo Quesada',0
sgtxt2   db   'Copyright 1988 - 1995',0

;---------------------------;
;		Includes			;
;---------------------------;
include 	320200.inc					; Incluye funciones para manejo de pixels.
include 	init.inc					; Rutinas de init (release mem,Is286,IsVGA,..)
include     font0001.inc

final	equ this byte

sexgame endp
code	ends
		end sexgame
