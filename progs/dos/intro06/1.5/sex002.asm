;-----------------------------------------------;
;                                               ;
;                THE SEX GAMES                  ;
;               El Ni�o del Sol                 ;
;                                               ;                                               ;
;-----------------------------------------------;
;                                               ;
;           Copyright (c) 1998,1995.            ;
;                                               ;
;-----------------------------------------------;

;DEFINES
VERT_RESCAN    = 3DAh             ;Input status register #1


P286
DOSSEG
MODEL COMPACT
DOSHEAP = 0

STACK 200H

DATASEG

;======= Valores del sprite final ======================================

colores1 label byte             ; colores usados en changedac ( Scroll Final )
        db 00,00,00             ; 0000
        db 63,63,63             ; 0001
        db 32,32,32             ; 0010
        db 63,63,63             ; 0011
        db 16,16,16             ; 0100
        db 63,63,63             ; 0101
        db 32,32,32             ; 0110
        db 63,63,63             ; 0111
        db 08,08,08             ; 1000
        db 63,63,63             ; 1001
        db 32,32,32             ; 1010
        db 63,63,63             ; 1011
        db 16,16,16             ; 1100
        db 63,63,63             ; 1101
        db 32,32,32             ; 1110
        db 63,63,63             ; 1111

circoff label byte              ; recorrido del circulo
        db 5
        db 4
        db 3
        db 2
        db 1
        db 1,0
        db 1,0,0
        db 1,0,0,0
        db 1,0,0,0,0
        db 255                  ; end of circ

circoff2 label byte              ; recorrido del circulo
        db 1,0,0,0,0
        db 1,0,0,0
        db 1,0,0
        db 1,0
        db 1
        db 2
        db 3
        db 4
        db 5
        db 255                  ; end of circ


scrollbuffer    db 80  * 21 dup ( 0 )   ; buffer de scrollhoriz1


scrollinc1  dw  0               ; offset del texto  ( 1 - ... )
scrollinc2  dw  0               ; offset del char ( 0 - 15 ) [ alto de los chars )
scrollinc3  dw  0               ; offset de los bytes a pasar (0 - 79 )
scrollbytes db 60 dup (0)
scrollbytes2    db 82 dup(0)    ; Buffer para copia Bitplane 1 a 2


include font0000.inc            ; fuente de los fonts

indice2 dw 0                    ; indices del rain8
indice  dw 0
indice3 dw 0
indice4 dw 0


scrolltxt label byte
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e                            e'
db'e                            e'
db'e            THE             e'
db'e                            e'
db'e     nopqrs  tuvwxypqno     e'
db'e            e'
db'e                            e'
db'e                            e'
db'e     BY EL NI%O DEL SOL     e'
db'e                            e'
db'e    COPYRIGHT 1998,1995     e'
db'e                            e'
db'e                            e'
db'e                            e'
db'e BASADO EN EL SEX GAMES III e'
db'e      QUE HICE PARA LA      e'
db'e       COMMODORE 128        e'
db'e          EN 1989           e'
db'e                            e'
db'e       HECHO 100k EN        e'
db'e         ASSEMBLER          e'
db'e  SIN LIBRERIAS ADICIONALES e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e HISTORIAL:                 e'
db'e     1988 - SEX GAMES       e'
db'e     1988 - SEX GAMES II    e'
db'e     1989 - SEX GAMES III   e'
db'e     1995 - THE SEX GAMES   e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e     ACERCA DEL AUTOR:      e'
db'e                            e'
db'e                            e'
db'e NO SOY NINGUN SEXOPATA NI  e'
db'e       NADA PARECIDO.       e'
db'e                            e'
db'e         m   m   m          e'
db'e                            e'
db'e  MUSICA: NO TUVE GANAS DE  e'
db'e   PONERME A HACER ALGUNA   e'
db'e     RUTINA, SO NO MUSIC    e'
db'e           AT ALL.          e'
db'e BUT I LIKE MOZART,VANGELIS e'
db'e  J.M.JARRE,ABBA,ERASURE,   e'
db'e       RIMSKY-KORSAKOV,     e'
db'e       ENNIO MORRICONE,     e'
db'e        TCHAIKOVSKY...      e'
db'e                            e'
db'e BUT MY FAVOURITE COMPOSER  e'
db'e            IS:             e'
db'e                            e'
db'e      l l l l l l l l       e'
db'e     l                l     e'
db'e     l      SIR       l     e'
db'e     l     ANDREW     l     e'
db'e     l  LLOYD WEBBER  l     e'
db'e     l                l     e'
db'e       l l l l l l l l      e'
db'e                            e'
db'e                            e'
db'e          SALUDOS           e'
db'e                            e'
db'e ME PARECE VERY COOL m QUE  e'
db'e  EMPIECE POR NACHO PAZ,    e'
db'e      ( UN AMIGAZO )        e'
db'e   A JAVI, TATI, EMY, EDY,  e'
db'e   JOHNY, CHARLY, GONCHI,   e'
db'e  ( OTROS BUENOS AMIGOS )   e'
db'e       A LULU Y MAITE       e'
db'e    ( GOOD TRAVELMATES )    e'
db'e   A VIOLE Y CARLOTAMADRE   e'
db'e    ( PARTE DE MI FLIA )    e'
db'e          A TORO            e'
db'e   ( PERSONA VERY COOL )    e'
db'e     A ARCHON, Y ALE        e'
db'e      ( MIS BOSSES )        e'
db'e     THE LAST HACKERS       e'
db'e   ( GOOD DEMO MAKERS )     e'
db'e         Y A VOS            e'
db'e ( GRAN LECTOR DE SCROLLS ) e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e  HERRAMIENTAS UTILIZADAS:  e'
db'e    ( ALFABETICAMENTE )     e'
db'e                            e'
db'e    DELUXE PAINT II z3.0    e'
db'e       NEO PAINT z2.1       e'
db'e        PCX2SPR z1.1        e'
db'e        Q-EDIT z3.0         e'
db'e    TURBO ASSEMBLER z3.1    e'
db'e    TURBO DEBUGGER z3.0     e'
db'e     TURBO LINKER z5.1      e'
db'e    TURBO PROFILER z2.0     e'
db'e       VCHAR NI z3.33       e'
db'e                            e'
db'e                            e'
db'e   LETRAS DE PRESENTACION   e'
db'e    FUERON SUSTRAIDAS DE    e'
db'e      INTRO MAKER 3.0       e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e                            e'
db'e  HASTA LA PROXIMA AMIGO,   e'
db'e     PRESS ESC TO EXIT      e'
db'e       THE SEX GAMES        e'
db'e                            e'
db'e                            e'
db'e                 BYE, RIQ.  e'
db'e               ( EL AUTOR ) e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh',0


;======= Valores de la precentaci�n - Sprites en movimiento ============

sprit1x dw 0                    ; Poscicionarlo en alg�n lugar
sprit1y dw 140                  ;
sprit1c db 0                    ; Valor para saber si hay que mostra la otra imagen
spritcc db 0

;== Valores usados por las funciones de sprites ========================

include     sprdat.inc                      ; Valores de los sprites


;== FarData ============================================================
FARDATA DATOSLEJOS1

;        titledat db 64000 dup(0)
        include   titledat.inc
        include   titledac.inc

;== Code ===============================================================
CODESEG
   ;This marks the start of executable code
   STARTUPCODE
   ;EXE program has all available memory allocated to it

IF DOSHEAP
   ;Release all memory except the amount currently being used
   ;End of stack is end of non-heap portion of program
   MOV BX,SP
   ADD BX,15    ;convert SP into paragraphs
   SHR BX,4
   MOV AX,SS    ;calculate size of program using ES PSP address
   ADD BX,AX
   MOV AX,ES
   SUB BX,AX
   MOV AH,4AH   ;resize memory block with PSP
   INT 21H      ;address in ES
ENDIF


        mov  ax,@DATA           ;Load segment address of data segment
        mov  ds,ax              ;into the DS register

        call cs:is286
        call cs:isvga
        call cs:mainprogram

        mov ax,3
        int 10h                 ;volver al modo texto.
        mov ax,4c00h
        int 21h


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc
        call loadpic                ; Show Title
        call sprinit                ; Initilalize the sprites
        call thelastscroll          ; Mensaje final
        ret
mainprogram endp


;---------------------------;   ; Called from main Program
;        loadpic            ;
;---------------------------;
loadpic proc

        mov ax,@data
        mov ds,ax
        call init320200

        cld                            ; Real Change DAC
        mov ax,DATOSLEJOS1
        mov ds,ax
        mov si,offset titledac
        xor al,al
        mov dx,3c8h
        out dx,al
        inc dx
        mov cx,768
repne   outsb

        mov ax,@data
        mov ds,ax

        mov cl,1
        call showpage           ; Muestra p�gina 2 (mientras de dibuja la 1 )
        mov cs:picdestiny,0a000h
loadpicloop2:
        mov cs:picsource,0
        mov cs:picplane,1
        mov cs:pic4plane,0

loadpicloop1:
;=========  BITPLANE ===========;
        mov dx,3c4h             ; SEQUENCER CONTROLLER
        mov al,2                ; MAP MASK REGISTER
        mov ah,cs:picplane      ; Bitplane=1
        out dx,ax

        mov si,cs:picsource
        mov ax,DATOSLEJOS1
        mov ds,ax               ; DS:SI
        mov di,offset titledat
        mov ax,cs:picdestiny
        mov es,ax               ; ES:DI
        xor cx,cx
lpicloop1:
        mov al,ds:[ si ]
        mov es:[ di ],al
        inc di
        add si,4
        inc cx
        cmp cx,80 * 200
        jne lpicloop1

        inc cs:picsource
        shl cs:picplane,1
        inc cs:pic4plane
        cmp cs:pic4plane,4
        jne loadpicloop1

        cmp cs:picdestiny,0a400h
        je loadpicend
        mov cl,0
        call showpage               ; Muestra ahora p�gina 0 ( una vez completa )
        mov cs:picdestiny,0a400h
        jmp loadpicloop2

loadpicend:
        ret

picsource   dw 0                    ; Comienzo
picplane    db 1                    ; First , 1st bitplane
pic4plane   db 0
picdestiny  dw 0a000h               ; Primer Plano!

loadpic endp

;---------------------------;       ; Called from MainProgram
;     SprInit               ;
;---------------------------;
SprInit proc

       mov ax,cs
       mov es,ax
        mov ax,@data
        mov ds,ax


       mov cl,3
       call showpage

        xor cx,cx
crearsss:
        call createspr
        inc cx
        cmp cx,8
        jne crearsss                    ; Create 0,1,2,3,4,5,6 Sprites


sprinitloop1:
        call movethem                   ; La verdadera rutina !

        mov  ax,sprit1x                 ; x = 0
        mov  bx,sprit1y                 ; y = 140
        mov  cx,4                       ; Sprite 4
        call posspr                     ; Mov Sprite
        mov  cx,4                       ; To sprite
        xor  dh,dh
        mov  dl,sprit1c                 ; From sprite
        call sprimage
        mov  cx,4                       ; sprites a mover ( 0,1 )
        call movesprites                ;
        call waitesc
        jne  sprinitloop1

        ret
SprInit endp

;---------------------------;       ; Called from SprInit
;     movethem              ;
;---------------------------;
movethem proc
        mov  ax,sprit1x
        dec  ax
        mov  sprit1x,ax
        inc  spritcc
        cmp  spritcc,10
        jne  movethem_
        mov  spritcc,0
        inc  sprit1c
        cmp  sprit1c,8
        jne  movethem_
        mov  sprit1c,4
movethem_:
        ret
movethem endp

;---------------------------;       ; Called from MainProgram
;     thelastscroll         ;
;---------------------------;
thelastscroll proc
        cli
        mov ax,0eh
        int 10h

        mov ah,12h
        mov bl,32h
        mov al,0                ; al=1 disable
        int 10h                 ;*** Enable CPU access to video Ram ***

        mov al,0ffh
        out 21h,al

        call cs:changedac

mainhere1:
        call cs:scrolltitle1
        call cs:scrolltitle2
        call cs:scrollcirc1                ;   Color de arriba
        call cs:scrollcirc3                ;   Color de arriba
        call cs:copy2bitplane
        call cs:scrollcirc2                ;   Color de fondo
        call cs:scrollcirc4                ;   Color de fondo

;        call cs:dacocean
        call cs:verticalr
        call cs:waitesc
        jne  mainhere1

        mov al,0h
        out 21h,al
        ret
thelastscroll endp

;---------------------------;
;   changedac               ;
;---------------------------;
changedac   proc
        mov ax,@DATA
        mov ds,ax                   ; ds=data
        mov al,0                    ; a partir de que color
        mov dx,3c8h
        out dx,al
        inc dx
        mov cx,16 * 3               ; hasta que color
        mov si,offset colores1      ; fuente de los colores
dacloop:
        mov al,ds:[si]
        out dx,al
        inc si
        loop dacloop
        ret
changedac endp

;---------------------------;
;		VerticalR			;
;---------------------------;
verticalr proc
        mov   dx,VERT_RESCAN   ;Wait for end of
ula1:
        in    al,dx            ;vertical rescan
        test  al,8
        jne   ula1
ula2:
        in    al,dx            ;Go to start of rescan
        test  al,8
        je    ula2
        ret
verticalr endp


;---------------------------;
;   ScrollCirc1             ;
;---------------------------;
scrollcirc1 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  + 80                    ; VALORES INICIALES
        mov bx,offset circoff       ; recorrido del circulo
nextcirc:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc
        cmp dl,255
        je  endcir

contcirc:
        call cs:rotateleft
        dec dl
        or dl,dl
        jne contcirc

nocirc:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc

endcir:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc1 endp

;---------------------------;
;   ScrollCirc2             ;
;---------------------------;
scrollcirc2 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0104h                ; read from plane 2 ( = 1 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  + 80                    ; VALORES INICIALES
        mov bx,offset circoff       ; recorrido del circulo
nextcirc2:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc2
        cmp dl,255
        je  endcir2

contcirc2:
        call cs:rotateleft
        dec dl
        or dl,dl
        jne contcirc2

nocirc2:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc2

endcir2:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc2 endp

;---------------------------;
;   ScrollCirc3             ;
;---------------------------;
scrollcirc3 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 131             ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  ; VALORES INICIALES
        mov bx,offset circoff2      ; recorrido del circulo
nextcirc3:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc3
        cmp dl,255
        je  endcir3

contcirc3:
        call cs:rotateright
        dec dl
        or dl,dl
        jne contcirc3

nocirc3:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc3

endcir3:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 131        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc3 endp

;---------------------------;
;   ScrollCirc4             ;
;---------------------------;
scrollcirc4 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 131             ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0104h                ; read from plane 2 ( = 1 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  ; VALORES INICIALES
        mov bx,offset circoff2      ; recorrido del circulo
nextcirc4:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc4
        cmp dl,255
        je  endcir4

contcirc4:
        call cs:rotateright
        dec dl
        or dl,dl
        jne contcirc4

nocirc4:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc4

endcir4:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 131        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc4 endp


;---------------------------;
;       RotateLeft          ;
;---------------------------;
rotateleft proc
        clc                 ; Clear Carrier
        mov cx,80 / 2       ; Cantidad de veces a rotar
scrollshiftl:
        mov ax, [ si ]      ; DS:SI
        xchg al,ah
        rcl ax,1            ;
        xchg al,ah          ; [ SI ] << 1
        mov [ si ], ax
        dec si              ; SI -= 2
        dec si
        loop scrollshiftl
        add si,80           ; le suma los 80 restados !
        ret
rotateleft endp


;---------------------------;
;       RotateRight         ;
;---------------------------;
rotateright proc
        clc                 ; Clear Carrier
        mov cx,80 / 2       ; Cantidad de veces a rotar
scrollshiftr:
        mov ax, [ si ]      ; DS:SI
        xchg al,ah
        rcr ax,1            ;
        xchg al,ah          ; [ SI ] << 1
        mov [ si ], ax
        inc si              ; SI -= 2
        inc si
        loop scrollshiftr
        sub si,80           ; le suma los 80 restados !
        ret
rotateright endp



;---------------------------;
;   ScrollTitle1            ;
;---------------------------;
scrolltitle1 proc

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 2
        mov ah,2                ; Bitplane = 2
        out dx,ax

        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,80 * 149
        mov di,80 * 150
        mov cx,80 * 100
        std
        mov dx,3ceh
        mov ax,0105h        ; Read Mode 0, Write mode 1
        out dx,ax
        rep movsb

        ret
scrolltitle1 endp

;---------------------------;
;   ScrollTitle2            ;
;---------------------------;
scrolltitle2 proc

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        mov ax,0a000h
        mov ds,ax
        mov es,ax
        mov si,80 * 51
        mov di,80 * 50
        mov cx,80 * 100
        cld
        mov dx,3ceh
        mov ax,0105h        ; Read Mode 0, Write mode 1
        out dx,ax
        rep movsb

        mov ax,@DATA
        mov ds,ax
        mov bx,scrollinc1

        xor cx,cx               ; un contador. (cantidad de letras 0-40 aca)
scrolltutto:
        lea si,scrolltxt
        xor ah,ah
        mov al,[ si + bx ]
        or al,al
        jne scrollno0

        xor bx,bx
        mov scrollinc1,bx
        mov scrollinc3,bx

scrollno0:
        push ax                 ; Guarda el varlor para repetir con bit 7 on
        shl ax,4                ; multiplica por 16 ( offset de los chars )
        add ax,offset tabla     ; Poscionado en los datos del char!
        add ax,scrollinc2       ; ( incremento de la altura ) elejir entre 0-15

        lea si,scrollbytes
        add si,cx               ; Que byte ?
        xchg ax,di
        mov al,[ di ]
        mov [ si ],al           ; ds:si source del dato

        inc cx                  ; incrementa uno CX (contador de guardar bytes)

        pop ax
        or al,128               ; bit 7 on
        shl ax,4                ; multiplica por 16 ( offset de los chars )
        add ax,offset tabla     ; Poscionado en los datos del char!
        add ax,scrollinc2       ; ( incremento de la altura ) elejir entre 0-15
        lea si,scrollbytes
        add si,cx               ; Que byte ?
        xchg ax,di
        mov al,[ di ]
        mov [ si ],al           ; ds:si source del dato

        inc bx                  ; incrementa contador de letras del scroll
        inc cx                  ;
        cmp cx,60               ; 40 bytes almacenados ?
        jb  scrolltutto

        lea si,scrollbytes
        mov di,80 * 150 + 10 ; linea 200 , centrado.

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,60
        rep movsb               ;mueve todo el bloque


        inc scrollinc2
        and scrollinc2,15

        cmp scrollinc2,0
        jne scrollend

        add scrollinc1,30

scrollend:
        ret

scrolltitle2 endp


;---------------------------;
;    Copy2bitplane          ;
;---------------------------;
copy2bitplane proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbytes2
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 1               ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,2                ; Bitplane = 2
        out dx,ax

        lea si,scrollbytes2
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 1
        rep movsb               ;mueve todo el bloque
        ret
copy2bitplane endp


;---------------------------;
;       waitesc             ;
;---------------------------;
waitesc proc
        push ax
        in  al,060h
        cmp al,01h
        pop ax
        ret
waitesc endp

;---------------------------;
;		getkey				;
;---------------------------;
getkey proc
        push ax
        mov ah,8
        int 21h                 ;*** wait until a key is pressed ***
        pop ax
        ret
getkey endp


;== Funciones usadas por los Sprites ===================================

include     320200.inc                  ; Incluye funciones para manejo de pixels.
include     init.inc                    ; Rutinas de init (release mem,Is286,IsVGA,..)
include     sprites.inc                 ; Funciones de manejar sprites.

END
