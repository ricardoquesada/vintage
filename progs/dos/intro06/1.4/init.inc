COMMENT |

        Rutinas de inicialización y otras varias.

            - Release mem
            - Is 286
            - Is VGA
            - Is SVGA
            - GetKey
|

;== Code ===============================================================
CODESEG
;          assume cs:code, ds:data, ss:stackseg

;---------------------------;
;		Is286				;
;---------------------------;
is286 proc
        xor ax,ax
        push ax
        popf
        pushf
        pop ax
        and ax,0f000h
        cmp ax,0f000h
        je not286
        ret
not286:
        mov ax,@data
        mov ds,ax
        mov dx,offset no286msg
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc
        mov ax,@data
        mov ds,ax
        mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp

;---------------------------;
;       issvga              ;
;---------------------------;
issvga proc
        mov ax,@data
        mov ds,ax
        mov ah,48h              ; allocate memory
        mov bx,256 / 16         ; 256 bytes
        int 21h
        push ax                 ; push segment !

        mov ax,4f00h
        xor di,di
        pop es                  ; pop segment
        int 10h                 ; Is SVGA ( en verdad cheque si soporta VESA )
        or ah,ah
        je itissvga

        mov ah,9
        lea dx,svgamsg
        int 21h                 ; Display not SVGA.

svgahere1:
        mov ah,8
        int 21h                 ; Get Char.
        and al,not 20h
        cmp al,'N'
        je svgahere2
        cmp al,'Y'
        jne svgahere1

itissvga:
        mov ah,49h
        int 21h                 ; Free memory
		ret
svgahere2:
        mov ah,49h
        int 21h                 ; Free memory
        mov ax,4cffh
        int 21h
issvga  endp


;== Data ===============================================================
DATASEG
vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'
svgamsg  db 13,10,13,10,7,'Your video card is not fast enough !',13,10,'Continue anyway (Y/N) ? $'
