;---------------------------;
; Sprites	-	Estructuras ;
;---------------------------;
spritestruc equ this word
    dw  offset spr_hombre1          ;  0
    dw  offset spr_hombre2          ;  1
    dw  offset spr_hombre3          ;  2
    dw  offset spr_hombre4          ;  3

spr_hombre1 equ this word
    dw  0               ; pos original x
    dw  0               ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  40              ; tama�o y
    dw  offset sd_hom1  ; datos del sprite 0
    dw  offset sb_hom1  ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_hombre2 equ this word
    dw  80              ; pos original x
    dw  0               ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  40              ; tama�o y
    dw  offset sd_hom2  ; datos del sprite 0
    dw  offset sb_hom2  ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_hombre3 equ this word
    dw  160             ; pos original x
    dw  0               ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  40              ; tama�o y
    dw  offset sd_hom3  ; datos del sprite 0
    dw  offset sb_hom3  ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_hombre4 equ this word
    dw  240             ; pos original x
    dw  0               ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  40              ; tama�o y
    dw  offset sd_hom4  ; datos del sprite 0
    dw  offset sb_hom4  ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

sd_hom1 label byte
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 70,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72,  0, 71, 70, 70, 72, 70, 70, 68, 68,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72, 66, 66, 66, 70, 70, 68, 70, 70, 68, 74,  0,  0,  0,  0,  0,  0
db   0,  0, 71, 72, 66, 71, 72, 72, 70, 70, 68, 70, 68, 68,  0,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 68, 68, 68, 72, 72, 70, 68, 68, 70, 66,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 72, 72, 68, 66, 71, 71, 66, 70, 70, 70,  0,  0,  0,  0,  0
db   0, 72, 72, 68, 69, 72, 74,136,136, 66,136,136,136,136, 70,136,  0,  0,  0,  0
db   0, 70, 69, 72, 72, 66, 71, 74,209,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0, 72, 72, 68, 68, 74, 74, 71, 71,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0,  0, 69, 68,209,209, 74, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 72,209,209, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 68,209,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0
db   0,  0, 68, 68,209,209,209,209,209,209,209,209,206,206,206,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 68,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,209,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0, 80,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 80, 80, 80, 80,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 80, 80, 80, 80,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 83, 83, 83, 80, 80,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 80, 80, 80, 83, 83, 83, 83, 80,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83,209,209,209, 80, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,209,209,209,209, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,209,209,209, 83, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,209,209,209, 83, 83, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,209,209, 83, 83, 83, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,209,209,209, 83, 83, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,209,209,209, 83, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,209,209,209,209, 83, 83, 83, 83, 83,209,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83,209,209,209, 83, 83, 83, 83, 83,209,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,123,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,123,123,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55,123,123,123,123,123,122,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55,123,123,123,123,  0,122,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55,123,123,123,  0,  0,  0,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55, 55, 55,  0,  0,  0,  0,122,122,122,122,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 55, 55, 55, 55, 55,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
sb_hom1 db 20 * 40 / 2 dup ( 0 )

sd_hom2 label byte
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 70,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72,  0, 71, 70, 70, 72, 70, 70, 68, 68,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72, 66, 66, 66, 70, 70, 68, 70, 70, 68, 74,  0,  0,  0,  0,  0,  0
db   0,  0, 71, 72, 66, 71, 72, 72, 70, 70, 68, 70, 68, 68,  0,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 68, 68, 68, 72, 72, 70, 68, 68, 70, 66,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 72, 72, 68, 66, 71, 71, 66, 70, 70, 70,  0,  0,  0,  0,  0
db   0, 72, 72, 68, 69, 72, 74,136,136, 66,136,136,136,136, 70,136,  0,  0,  0,  0
db   0, 70, 69, 72, 72, 66, 71, 74,209,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0, 72, 72, 68, 68, 74, 74, 71, 71,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0,  0, 69, 68,209,209, 74, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 72,209,209, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 68,209,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0
db   0,  0, 68, 68,209,209,209,209,209,209,209,209,206,206,206,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 68,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,209,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0, 80,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 80, 80, 80, 80,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 80, 80, 80, 80,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 80, 83, 83, 80, 80,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 83, 80,209, 80, 83, 83, 83, 80,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 80,209,209,209, 80, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 83,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83,209,209,209, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83,209,209,209, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,123,122,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,123,123,122,122,122,122,122,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,123,123,123,122,122,122,122,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,123,123,123,122,122,122,122,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 32, 32, 32, 32, 32, 32, 32,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
sb_hom2 db 20 * 40 / 2 dup ( 0 )

sd_hom3 label byte
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 70,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72,  0, 71, 70, 70, 72, 70, 70, 68, 68,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72, 66, 66, 66, 70, 70, 68, 70, 70, 68, 74,  0,  0,  0,  0,  0,  0
db   0,  0, 71, 72, 66, 71, 72, 72, 70, 70, 68, 70, 68, 68,  0,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 68, 68, 68, 72, 72, 70, 68, 68, 70, 66,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 72, 72, 68, 66, 71, 71, 66, 70, 70, 70,  0,  0,  0,  0,  0
db   0, 72, 72, 68, 69, 72, 74,136,136, 66,136,136,136,136, 70,136,  0,  0,  0,  0
db   0, 70, 69, 72, 72, 66, 71, 74,209,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0, 72, 72, 68, 68, 74, 74, 71, 71,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0,  0, 69, 68,209,209, 74, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 72,209,209, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 68,209,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0
db   0,  0, 68, 68,209,209,209,209,209,209,209,209,206,206,206,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 68,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,209,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0, 80,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 80, 80, 80, 80,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 80, 80, 80, 80,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 83, 83, 83, 80, 80,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 83, 80, 80, 83, 83, 83, 83, 80,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 80,209,209, 80, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 83,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83,209,209,209,209,209, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83,209,209,209,209,209,209,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 83, 83,209,209,209,209,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 83, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,122,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,122,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55,122,122,122,122,122,122,123,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55,122,122,122,122,  0,123,123,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55,122,122,122,  0,  0,  0,123,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 55, 55, 55,  0,  0,  0,  0,123,123,123,123,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 55, 55, 55, 55, 55,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
sb_hom3 db 20 * 40 / 2 dup ( 0 )


sd_hom4 label byte
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 70,  0, 70, 70,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72,  0, 71, 70, 70, 72, 70, 70, 68, 68,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0, 72, 66, 66, 66, 70, 70, 68, 70, 70, 68, 74,  0,  0,  0,  0,  0,  0
db   0,  0, 71, 72, 66, 71, 72, 72, 70, 70, 68, 70, 68, 68,  0,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 68, 68, 68, 72, 72, 70, 68, 68, 70, 66,  0,  0,  0,  0,  0
db   0,  0, 68, 72, 68, 72, 72, 68, 66, 71, 71, 66, 70, 70, 70,  0,  0,  0,  0,  0
db   0, 72, 72, 68, 69, 72, 74,136,136, 66,136,136,136,136, 70,136,  0,  0,  0,  0
db   0, 70, 69, 72, 72, 66, 71, 74,209,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0, 72, 72, 68, 68, 74, 74, 71, 71,209,209,209,209,127,127,127,  0,  0,  0,  0
db   0,  0, 69, 68,209,209, 74, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 72,209,209, 74,209,209,209,209,209,209,209,210,  0,  0,  0,  0
db   0,  0, 68, 68, 68,209,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0
db   0,  0, 68, 68,209,209,209,209,209,209,209,209,206,206,206,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 68,209,209,209,209,209,209,209,209,209,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,209,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0, 80,209,209,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0, 80, 80, 80, 80,209,209,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 80, 80, 80, 80,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83, 80, 83, 83, 80, 80,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 83, 80,209, 80, 83, 83, 83, 80,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 80,209,209,209, 80, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83, 83,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0, 83, 83,209,209,209, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83,209,209,209, 83, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83,209,209,209, 83, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83,209,209,209,209, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 83, 83, 83,209,209,209, 83, 83, 83,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,122,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,122,122,122,122,123,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,122,122,122,122,122,123,123,123,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,122,122,122,122,122,123,123,  0,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0, 32, 32, 32, 32, 32, 32, 32, 32,  0,  0,  0,  0,  0,  0,  0
db   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
sb_hom4 db 20 * 40 / 2 dup ( 0 )
