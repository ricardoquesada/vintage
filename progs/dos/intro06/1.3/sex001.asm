COMMENT |
							 Sex Games
                       Copyright 1988 - 1995
						   Ricardo Quesada

Historial:		Sex Games 1 - (c) 1988 R.Q. progs para Commodore 128
				Sex Games 2 - (c) 1988 R.Q. progs para Commodore 128
				Sex Games 3 - (c) 1989 R.Q. progs para Commodore 128
				The Sex Games - (c) 1995 El ni�o para PC

|
P286
;== Stack ==============================================================
stackseg  segment para STACK 'STACK'      ;Definition of stack segment
          dw 100h dup (?)          ;The stack comprises 256 words
stackseg  ends                     ;End of stack segment 


;== Data ===============================================================
data      segment para 'DATA'     ;Definition of data segment 

incolor     dw 1                            ; Fn: MainTitle

sgtxt0      db      'The Sex Games',0
sgtxt1      db    'by Ricardo Quesada',0
sgtxt2      db   'Copyright 1988 - 1995',0

include     font0001.inc                    ; Fonts
include     sprdat.inc                      ; Valores de los sprites

data      ends

;== Code ===============================================================
code      segment para 'CODE'     ;Definition of CODE segment
          assume cs:code, ds:data, ss:stackseg

prog    proc far
        mov  ax,data            ;Load segment address of data segment
        mov  ds,ax              ;into the DS register
        call mainprogram
        mov ax,3
        int 10h                 ;volver al modo texto.
        mov ax,4c00h
        int 21h
prog    endp                    ;End of PROG procedure


;---------------------------;
;       mainprogram         ;
;---------------------------;
mainprogram proc near

        call init320200

        mov ah,12h
        mov bl,32h
        mov al,0                ; al=1 disable
        int 10h                 ;*** Enable CPU access to video Ram ***

		mov ax,cs
		mov es,ax
        mov ax,data
        mov ds,ax

;        mov  cl,0
;        call setpage
;        call maintitle

        mov  cl,3
        call showpage

        xor cx,cx
crearsss:
        call createspr
        inc cx
        cmp cx,7
        jne crearsss

mainhere1:
        call getkey
        jne  mainhere1

		ret
mainprogram endp


;---------------------------;
;      MainTitle            ;
;---------------------------;
maintitle proc near
        mov ax,data
        mov ds,ax
        mov incolor,1
        mov ax,0
        mov cx,16
maintiloop2:
        mov bx,0
maintiloop1:
        mov dx,1
        mov di,1
        mov si,offset sgtxt0
        call displayletras
        add cx,incolor
        cmp cx,30
        je mthere2
        cmp cx,16
        jne mthere1

mthere2:
        neg incolor
mthere1:
        add bx,8
        cmp bx,180
        jb maintiloop1
        add ax,40
        cmp ax,300
        jb maintiloop2

        mov ax,105
        mov bx,64
        mov cx,1
        mov dx,0
        mov di,1
        mov si,offset sgtxt0
        call displayletras

        mov ax,85
        mov bx,84
        mov cx,4
        mov dx,0
        mov di,1
        mov si,offset sgtxt1
        call displayletras

        mov ax,75
        mov bx,104
        mov cx,2
        mov dx,0
        mov di,1
        mov si,offset sgtxt2
        call displayletras

        ret
maintitle endp
code      ends                    ;End of CODE segment

include 	320200.inc					; Incluye funciones para manejo de pixels.
include 	init.inc					; Rutinas de init (release mem,Is286,IsVGA,..)
include     sprites.inc                 ; Funciones de manejar sprites.

          end  prog               ;Begin execution with PROG procedure
