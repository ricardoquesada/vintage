;======================================;
;                                      ;
;              Rotaciones              ;
;                by Riq                ;
;                                      ;                                              ;
;======================================;
;                                      ;
;     Fecha de inicio: 12 / 4 / 95     ;
;              Version 1.0             ;
;                                      ;
;======================================;
;                                      ;
;          Copyright (c) 1995.         ;
;                                      ;
;======================================;


DOSSEG
MODEL SMALL

;======================================; DEFINES
ADD_X       = 320
ADD_Y       = 100
VERT_RESCAN = 3DAh

;======================================; STACK
STACK 200H

;======================================; DATA
DATASEG

Colores1 label byte                    ; colores usados en changedac ( Scroll Final )
        db 00,00,00                    ; 0000
        db 63,63,63                    ; 0001
        db 50,50,50                    ; 0010
        db 63,63,63                    ; 0011
        db 32,32,32                    ; 0100
        db 63,63,63                    ; 0101
        db 50,50,50                    ; 0110
        db 63,63,63                    ; 0111
        db 08,08,08                    ; 1000
        db 63,63,63                    ; 1001
        db 50,50,50                    ; 1010
        db 63,63,63                    ; 1011
        db 32,32,32                    ; 1100
        db 63,63,63                    ; 1101
        db 50,50,50                    ; 1110
        db 63,63,63                    ; 1111

circoff label byte                     ; recorrido del
       db 5
       db 4
       db 3
       db 2
       db 1
       db 1,0
       db 1,0,0
       db 1,0,0,0
       db 1,0,0,0,0
       db 255                          ; end of circ

circoff2 label byte                    ; recorrido del circulo
       db 1,0,0,0,0
       db 1,0,0,0
       db 1,0,0
       db 1,0
       db 1
       db 2
       db 3
       db 4
       db 5
       db 255                          ; end of circ


scrollbuffer    db 80  * 21 dup ( 0 )  ; buffer de scrollhoriz1

scrollinc1  dw  0                      ; offset del texto  ( 1 - ... )
scrollinc2  dw  0                      ; offset del char ( 0 - 15 ) [ alto de lo)
scrollinc3  dw  0                      ; offset de los bytes a pasar (0 - 79 )
scrollbytes db 60 dup (0)
scrollbytes2    db 82 dup(0)           ; Buffer para copia Bitplane 1 a 2


include font0000.inc                   ; fuente de los fonts

indice2     dw 0                       ; indices del rain8
indice      dw 0
indice3     dw 0
indice4     dw 0


scrolltxt label byte
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e                            e'
db'e                            e'
db'e            THE             e'
db'e                            e'
db'e     nopqrs  tuvwxypqno     e'
db'e            e'
db'e                            e'
db'e                            e'
db'e     BY EL NI%O DEL SOL     e'
db'e                            e'
db'e    COPYRIGHT 1998,1995     e'
db'e                            e'
db'e                            e'
db'e                            e'
db'e BASADO EN EL SEX GAMES III e'
db'e      QUE HICE PARA LA      e'
db'e       COMMODORE 128        e'
db'e          EN 1989           e'
db'e                            e'
db'e       HECHO 100k EN        e'
db'e         ASSEMBLER          e'
db'e  SIN LIBRERIAS ADICIONALES e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e HISTORIAL:                 e'
db'e     1988 - SEX GAMES       e'
db'e     1988 - SEX GAMES II    e'
db'e     1989 - SEX GAMES III   e'
db'e     1995 - THE SEX GAMES   e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh'
db'                              '
db'diiiiiiiiiiiiiiiiiiiiiiiiiiiif'
db'e     ACERCA DEL AUTOR:      e'
db'e                            e'
db'e                            e'
db'e NO SOY NINGUN SEXOPATA NI  e'
db'e       NADA PARECIDO.       e'
db'e                            e'
db'e         m   m   m          e'
db'e                            e'
db'e  MUSICA: NO TUVE GANAS DE  e'
db'e   PONERME A HACER ALGUNA   e'
db'e     RUTINA, SO NO MUSIC    e'
db'e           AT ALL.          e'
db'e BUT I LIKE MOZART,VANGELIS e'
db'e  J.M.JARRE,ABBA,ERASURE,   e'
db'e       RIMSKY-KORSAKOV,     e'
db'e       ENNIO MORRICONE,     e'
db'e        TCHAIKOVSKY...      e'
db'e                            e'
db'e BUT MY FAVOURITE COMPOSER  e'
db'e            IS:             e'
db'e                            e'
db'e      l l l l l l l l       e'
db'e     l                l     e'
db'e     l      SIR       l     e'
db'e     l     ANDREW     l     e'
db'e     l  LLOYD WEBBER  l     e'
db'e     l                l     e'
db'e       l l l l l l l l      e'
db'e                            e'
db'e                            e'
db'e          SALUDOS           e'
db'e                            e'
db'e ME PARECE VERY COOL m QUE  e'
db'e  EMPIECE POR NACHO PAZ,    e'
db'e      ( UN AMIGAZO )        e'
db'e   A JAVI, TATI, EMY, EDY,  e'
db'e   JOHNY, CHARLY, GONCHI,   e'
db'e  ( OTROS BUENOS AMIGOS )   e'
db'e       A LULU Y MAITE       e'
db'e    ( GOOD TRAVELMATES )    e'
db'e   A VIOLE Y CARLOTAMADRE   e'
db'e    ( PARTE DE MI FLIA )    e'
db'e          A TORO            e'
db'e   ( PERSONA VERY COOL )    e'
db'e     A ARCHON, Y ALE        e'
db'e      ( MIS BOSSES )        e'
db'e     THE LAST HACKERS       e'
db'e   ( GOOD DEMO MAKERS )     e'
db'e         Y A VOS            e'
db'e ( GRAN LECTOR DE SCROLLS ) e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e  HERRAMIENTAS UTILIZADAS:  e'
db'e    ( ALFABETICAMENTE )     e'
db'e                            e'
db'e    DELUXE PAINT II z3.0    e'
db'e       NEO PAINT z2.1       e'
db'e        PCX2SPR z1.1        e'
db'e        Q-EDIT z3.0         e'
db'e    TURBO ASSEMBLER z3.1    e'
db'e    TURBO DEBUGGER z3.0     e'
db'e     TURBO LINKER z5.1      e'
db'e    TURBO PROFILER z2.0     e'
db'e       VCHAR NI z3.33       e'
db'e                            e'
db'e                            e'
db'e   LETRAS DE PRESENTACION   e'
db'e    FUERON SUSTRAIDAS DE    e'
db'e      INTRO MAKER 3.0       e'
db'e                            e'
db'e         l   l   l          e'
db'e                            e'
db'e                            e'
db'e  HASTA LA PROXIMA AMIGO,   e'
db'e     PRESS ESC TO EXIT      e'
db'e       THE SEX GAMES        e'
db'e                            e'
db'e                            e'
db'e                 BYE, RIQ.  e'
db'e               ( EL AUTOR ) e'
db'e                            e'
db'gjjjjjjjjjjjjjjjjjjjjjjjjjjjjh',0


;SinTable created by Jon Beltran de Heredia
;Sines are in 8.8 fixedpoint, from 0 degree to 360+90 degree (in order to
; calculate the cosines with the same table)
SinTbl	LABEL	WORD
dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256
dw 256,256,256,256,255,255,255,254,254,253,252,251,250,249,248
dw 247,246,245,243,242,241,239,237,236,234,232,230,228,226,224
dw 222,219,217,215,212,210,207,204,202,199,196,193,190,187,184
dw 181,178,175,171,168,165,161,158,154,150,147,143,139,136,132
dw 128,124,120,116,112,108,104,100,96,92,88,83,79,75,71
dw 66,62,58,53,49,44,40,36,31,27,22,18,13,9,4
dw 0,-4,-9,-13,-18,-22,-27,-31,-36,-40,-44,-49,-53,-58,-62
dw -66,-71,-75,-79,-83,-88,-92,-96,-100,-104,-108,-112,-116,-120,-124
dw -128,-132,-136,-139,-143,-147,-150,-154,-158,-161,-165,-168,-171,-175,-178
dw -181,-184,-187,-190,-193,-196,-199,-202,-204,-207,-210,-212,-215,-217,-219
dw -222,-224,-226,-228,-230,-232,-234,-236,-237,-239,-241,-242,-243,-245,-246
dw -247,-248,-249,-250,-251,-252,-253,-254,-254,-255,-255,-255,-256,-256,-256
dw -256,-256,-256,-256,-255,-255,-255,-254,-254,-253,-252,-251,-250,-249,-248
dw -247,-246,-245,-243,-242,-241,-239,-237,-236,-234,-232,-230,-228,-226,-224
dw -222,-219,-217,-215,-212,-210,-207,-204,-202,-199,-196,-193,-190,-187,-184
dw -181,-178,-175,-171,-168,-165,-161,-158,-154,-150,-147,-143,-139,-136,-132
dw -128,-124,-120,-116,-112,-108,-104,-100,-96,-92,-88,-83,-79,-75,-71
dw -66,-62,-58,-53,-49,-44,-40,-36,-31,-27,-22,-18,-13,-9,-4
dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256,256

Dots   label word                      ; X,Y,Z,1
       dw    30,-30,-30,100h
       dw    30, 30,-30,100h
       dw   -30,-30,-30,100h
       dw   -30, 30,-30,100h
       dw    30,-30, 30,100h
       dw    30, 30, 30,100h
       dw   -30,-30, 30,100h
       dw   -30, 30, 30,100h
       ;;;
       dw    25,-25,-25,100h
       dw    25, 25,-25,100h
       dw   -25,-25,-25,100h
       dw   -25, 25,-25,100h
       dw    25,-25, 25,100h
       dw    25, 25, 25,100h
       dw   -25,-25, 25,100h
       dw   -25, 25, 25,100h
       ;;;
       dw    20,-20,-20,100h
       dw    20, 20,-20,100h
       dw   -20,-20,-20,100h
       dw   -20, 20,-20,100h
       dw    20,-20, 20,100h
       dw    20, 20, 20,100h
       dw   -20,-20, 20,100h
       dw   -20, 20, 20,100h
       ;;;
       dw    15,-15,-15,100h
       dw    15, 15,-15,100h
       dw   -15,-15,-15,100h
       dw   -15, 15,-15,100h
       dw    15,-15, 15,100h
       dw    15, 15, 15,100h
       dw   -15,-15, 15,100h
       dw   -15, 15, 15,100h
       ;;;
       dw    10,-10,-10,100h
       dw    10, 10,-10,100h
       dw   -10,-10,-10,100h
       dw   -10, 10,-10,100h
       dw    10,-10, 10,100h
       dw    10, 10, 10,100h
       dw   -10,-10, 10,100h
       dw   -10, 10, 10,100h
       ;;;

TOTALDOTS = ($-DOTS)/8

Vertex_Rot  label word
       dw   TOTALDOTS dup(0,0,0,0)

Matrix_Rot  label word                 ; Datos usados por Mul_Matrix
m00    dw   0,0,0,0
m01    dw   0,0,0,0
m02    dw   0,0,0,0
m03    dw   0,0,0,0

_X     dw   0                          ; Datos Temporales de Mul_Matrix
_Y     dw   0
_Z     dw   0

angle  dw 0                            ; 0 - 360
addangle dw 1


;======================================; CODE
CODESEG
       STARTUPCODE
P286
;======================================;
;      Mainprogram                     ;
;======================================;
Mainprogram proc
       cld
       mov  ax,@DATA
       mov  ds,ax
       mov  ax,0a000h
       mov  es,ax

       call Main_Loop

       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h
       ret
Mainprogram endp


;======================================;
;      Main_Loop                       ;
;======================================;
Main_Loop   proc

        cli
        mov ax,0eh
        int 10h

        mov     dx,3dah
	in	al,dx
	mov	dl,0c0h
	xor	ax,ax
	mov	cx,16
@@b2:	out	dx,al
	out	dx,al
	inc	al
	loop	@@b2
	mov	al,20h
	out	dx,al


;        mov ah,12h
;        mov bl,32h
;        mov al,0                ; al=1 disable
;        int 10h                 ;*** Enable CPU access to video Ram ***

;        mov al,0ffh
;        out 21h,al

        call cs:changedac

M_Loop1:
       Call New_Angle
       Call Clean_Dots
       Call Rot_Dots
       Call Draw_Dots

       Call Scrolltitle1
       Call Scrolltitle2
       Call Scrollcirc1                ;   Color de arriba
       Call Scrollcirc3                ;   Color de arriba
       Call Copy2bitplane
       Call Scrollcirc2                ;   Color de fondo
       Call Scrollcirc4                ;   Color de fondo

       Call Verticalr
       Call Waitesc
       jne  M_Loop1

 ;      mov al,0h
 ;      out 21h,al
       ret

Main_Loop   endp


;======================================;
;      Changedac                       ;
;======================================;
changedac   proc
       mov  ax,@DATA
       mov  ds,ax                      ; ds=data
       mov  al,0                       ; a partir de que color
       mov  dx,3c8h
       out  dx,al
       inc  dx
       mov  cx,16 * 3                  ; hasta que color
       mov  si,offset colores1         ; fuente de los colores
dacloop:
       mov  al,ds:[si]
       out  dx,al
       inc  si
       loop dacloop
       ret
changedac endp

;---------------------------;
;   ScrollCirc1             ;
;---------------------------;
scrollcirc1 proc
       mov  ax,@DATA
       mov  es,ax
       mov  di,offset scrollbuffer
       mov  ax,0a000h
       mov  ds,ax
       mov  si,80 * 50                 ; al principio
       mov  cx,80 * 20                 ; una linea completa  * 50

       mov  dx,3ceh
       mov  ax,0005h
       out  dx,ax
       mov  ax,0004h                ; read from plane 1 ( = 0 )
       out  dx,ax
       cld
       rep  movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  + 80                    ; VALORES INICIALES
        mov bx,offset circoff       ; recorrido del circulo
nextcirc:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc
        cmp dl,255
        je  endcir

contcirc:
        call cs:rotateleft
        dec dl
        or dl,dl
        jne contcirc

nocirc:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc

endcir:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc1 endp

;---------------------------;
;   ScrollCirc2             ;
;---------------------------;
scrollcirc2 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 50              ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0204h                ; read from plane 2 ( = 3 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  + 80                    ; VALORES INICIALES
        mov bx,offset circoff       ; recorrido del circulo
nextcirc2:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc2
        cmp dl,255
        je  endcir2

contcirc2:
        call cs:rotateleft
        dec dl
        or dl,dl
        jne contcirc2

nocirc2:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc2

endcir2:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,4                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 50         ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc2 endp

;---------------------------;
;   ScrollCirc3             ;
;---------------------------;
scrollcirc3 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 131             ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0004h                ; read from plane 1 ( = 0 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  ; VALORES INICIALES
        mov bx,offset circoff2      ; recorrido del circulo
nextcirc3:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc3
        cmp dl,255
        je  endcir3

contcirc3:
        call cs:rotateright
        dec dl
        or dl,dl
        jne contcirc3

nocirc3:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc3

endcir3:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,1                ; Bitplane = 1
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 131        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc3 endp

;---------------------------;
;   ScrollCirc4             ;
;---------------------------;
scrollcirc4 proc
        mov ax,@DATA
        mov es,ax
        mov di,offset scrollbuffer
        mov ax,0a000h
        mov ds,ax
        mov si,80 * 131             ; al principio
        mov cx,80 * 20              ; una linea completa  * 50

        mov dx,3ceh
        mov ax,0005h
        out dx,ax
        mov ax,0204h                ; read from plane 3 ( = 2 )
        out dx,ax
        cld
        rep movsb

        mov ax,@DATA
        mov ds,ax

        mov si,offset scrollbuffer  ; VALORES INICIALES
        mov bx,offset circoff2      ; recorrido del circulo
nextcirc4:
        mov dl,[ bx ]
        or  dl,dl
        je  nocirc4
        cmp dl,255
        je  endcir4

contcirc4:
        call cs:rotateright
        dec dl
        or dl,dl
        jne contcirc4

nocirc4:
        add si,80               ; procesar los siguientes 80 bytes
        inc bx
        jmp short nextcirc4

endcir4:
        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,4                ; Bitplane = 2
        out dx,ax

        lea si,scrollbuffer
        mov di, 80 * 131        ; linea 200 , centrado.
        mov ax,0a000h
        mov es,ax

        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 0
        out dx,ax
        mov ax,0000h
        out dx,ax
        mov ax,0001h            ;Enable Set/Reset
        out dx,ax
        mov ax,0003h            ;Function select register - replace mode
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register.
        out dx,ax

        mov cx,80 * 20
        rep movsb               ;mueve todo el bloque

        ret
scrollcirc4 endp


;---------------------------;
;       RotateLeft          ;
;---------------------------;
rotateleft proc
        clc                 ; Clear Carrier
        mov cx,80 / 2       ; Cantidad de veces a rotar
scrollshiftl:
        mov ax, [ si ]      ; DS:SI
        xchg al,ah
        rcl ax,1            ;
        xchg al,ah          ; [ SI ] << 1
        mov [ si ], ax
        dec si              ; SI -= 2
        dec si
        loop scrollshiftl
        add si,80           ; le suma los 80 restados !
        ret
rotateleft endp


;======================================;
;      RotateRight                     ;
;======================================;
rotateright proc
       clc                             ; Clear Carrier
       mov  cx,80 / 2                  ; Cantidad de veces a rotar
scrollshiftr:
       mov  ax, [ si ]                 ; DS:SI
       xchg al,ah
       rcr  ax,1                       ;
       xchg al,ah                      ; [ SI ] << 1
       mov  [ si ], ax
       inc  si                         ; SI -= 2
       inc  si
       loop scrollshiftr
       sub  si,80                      ; le suma los 80 restados !
       ret
rotateright endp



;======================================;
;      ScrollTitle1                    ;
;======================================;
scrolltitle1 proc

       mov  dx,3c4h
       mov  ax,0402h                   ; map mask register with bitplane 3
       out  dx,ax

       mov  dl,0ceh
       mov  ax,0105h
       out  dx,ax
       mov  ax,0204h
       out  dx,ax

       mov  ax,0a000h
       mov  ds,ax
       mov  es,ax
       mov  si,80 * 149
       mov  di,80 * 150
       mov  cx,80 * 100
       std
       rep  movsb

       ret
scrolltitle1 endp

;======================================;
;      ScrollTitle2                    ;
;======================================;
scrolltitle2 proc
       mov  dx,3c4h
       mov  ax,0102h                   ; map mask register with bitplane 1
       out  dx,ax

       mov  dl,0ceh
       mov  ax,0105h
       out  dx,ax
       mov  ax,0004h
       out  dx,ax

       mov  ax,0a000h
       mov  ds,ax
       mov  es,ax
       mov  si,80 * 51
       mov  di,80 * 50
       mov  cx,80 * 100
       cld
       rep  movsb

       mov  ax,@DATA
       mov  ds,ax
       mov  bx,scrollinc1

       xor  cx,cx                      ; un contador. (cantidad de letras 0-40 aca)
scrolltutto:
       lea  si,scrolltxt
       xor  ah,ah
       mov  al,[ si + bx ]
       or   al,al
       jne  scrollno0

       xor  bx,bx
       mov  scrollinc1,bx
       mov  scrollinc3,bx

scrollno0:
       push ax                         ; Guarda el varlor para repetir con bit 7 on
       shl  ax,4                       ; multiplica por 16 ( offset de los chars )
       add  ax,offset tabla            ; Poscionado en los datos del char!
       add  ax,scrollinc2              ; ( incremento de la altura ) elejir entre 0-15

       lea  si,scrollbytes
       add  si,cx                      ; Que byte ?
       xchg ax,di
       mov  al,[ di ]
       mov  [ si ],al                  ; ds:si source del dato

       inc cx                          ; incrementa uno CX (contador de guardar bytes)

       pop  ax
       or   al,128                     ; bit 7 on
       shl  ax,4                       ; multiplica por 16 ( offset de los chars )
       add  ax,offset tabla            ; Poscionado en los datos del char!
       add  ax,scrollinc2              ; ( incremento de la altura ) elejir entre 0-15
       lea  si,scrollbytes
       add  si,cx                      ; Que byte ?
       xchg ax,di
       mov  al,[ di ]
       mov  [ si ],al                  ; ds:si source del dato

       inc bx                          ; incrementa contador de letras del scroll
       inc cx                          ;
       cmp cx,60                       ; 40 bytes almacenados ?
       jb  scrolltutto

       lea  si,scrollbytes
       mov  di,80 * 150 + 10           ; linea 200 , centrado.

       mov  dx,3ceh
       mov  ax,0004h
       out  dx,ax
       mov  ax,0005h                   ;read mode 0,write mode 0
       out  dx,ax
       mov  ax,0000h
       out  dx,ax
       mov  ax,0001h                   ;Enable Set/Reset
       out  dx,ax
       mov  ax,0003h                   ;Function select register - replace mode
       out  dx,ax
       mov  ax,0ff08h                  ;bit mask to bit mask register.
       out  dx,ax


       mov  cx,60
       rep  movsb                      ;mueve todo el bloque

       inc  scrollinc2
       and  scrollinc2,15

       cmp  scrollinc2,0
       jne  scrollend

       add  scrollinc1,30

scrollend:
       ret

scrolltitle2 endp

;======================================;
;      Copy2bitplane                   ;
;======================================;
copy2bitplane proc
       mov  ax,0a000h
       mov  es,ax
       mov  ds,ax
       mov  si,80 * 50                 ; al principio
       mov  di,80 * 50
       mov  cx,80 * 1                  ; una linea completa  * 50

       mov  dx,3c4h
       mov  ax,0402h                   ; map mask register with bitplane 1
       out  dx,ax

       mov  dx,3ceh
       mov  ax,0005h
       out  dx,ax
       mov  ax,0004h                   ; read from plane 1 ( = 0 )
       out  dx,ax
       cld
       rep  movsb

       ret
copy2bitplane endp


;======================================;
;      Aca empieza la parte de los     ;
;              vectores                ;
;======================================;


;======================================;
;      New_Angle                       ;
;======================================;
New_Angle   proc
       mov  bx,angle
       lea  di,Matrix_Rot
       cmp  addangle,1
       je   a_z
       cmp  addangle,2
       je   a_a
       cmp  addangle,3
       je   a_y
       cmp  addangle,4
       je   a_b
       cmp  addangle,5
       je   a_x

a_c:
       call Init_Matrix_C
       jmp a_fin
a_b:
       call Init_Matrix_B
       jmp a_fin
a_a:
       call Init_Matrix_A
       jmp a_fin

a_x:
       call Init_Matrix_X
       jmp  a_fin
A_Z:
       Call Init_Matrix_Z
       jmp  a_fin
a_Y:
       Call INit_Matrix_Y

a_fin:
       mov  bx,9
       add  angle,bx
       cmp  angle,360
       jb   New_Angle_
       mov  angle,0

       cmp  addangle,6
       je   Reset_Angle
       inc  addangle
       jmp  New_Angle_
Reset_Angle:
       mov  addangle,1

New_Angle_:
       ret
New_Angle   endp


;======================================;
;      Draw_Dots                       ;
;======================================;
Draw_Dots   proc
       xor  cx,cx                      ; Dots Counter
DD_loop:
       push cx
       lea  si,Vertex_Rot              ; Donde esta el nuevo valor
       shl  cx,3                       ; Multiplica * 8 ( 4 * 2 )
       add  si,cx
       mov  ax,[ si + 0 ]              ; X
       mov  bx,[ si + 2 ]              ; Y
       mov  cx,15                      ; Color
       add  ax,ADD_X
       add  bx,ADD_Y
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  DD_loop

       ret
Draw_Dots   endp

;======================================;
;      Clean_Dots                      ;
;======================================;
Clean_Dots   proc
       xor  cx,cx                      ; Dots Counter
CD_loop:
       push cx
       lea  si,Vertex_Rot
       shl  cx,3                       ; Multiplica * 8
       add  si,cx
       mov  ax,[ si + 0 ]              ; X
       mov  bx,[ si + 2 ]              ; Y
       add  ax,ADD_X
       add  bx,ADD_Y
       xor  cx,cx                      ; Color
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  CD_loop

       ret
Clean_Dots   endp

;======================================;
;      Rot_Dots                        ;
;======================================;
Rot_Dots   proc
       pusha
       xor  cx,cx                      ; Dots Counter
RD_loop:
       push cx
       lea  di,Dots
       lea  bx,Vertex_Rot
       lea  si,Matrix_Rot
       shl  cx,3                       ; Multiplica * 8
       add  di,cx
       add  bx,cx

       mov  ax,[ di + 0 ]              ; X
       mov  [ bx + 0 ],ax
       mov  ax,[ di + 2 ]              ; Y
       mov  [ bx + 2 ],ax
       mov  ax,[ di + 4 ]              ; Z
       mov  [ bx + 4 ],ax
       mov  ax,[ di + 6 ]              ; 1
       mov  [ bx + 6 ],ax
       call Mul_Matrix                 ; [BX] = New Pos

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  RD_loop

       popa
       ret
Rot_Dots   endp


;======================================;
;      Mul_Matrix                      ;
;======================================;
;      In:  bx -> offset Vertex (1x4)  ;
;           si -> offset Matrix (4x4)  ;
;      Out: bx -> offset new Vertex    ;
;======================================;
;
;  [ x y z 1 ]  *  [ A B C D ]
;                  [ E F G H ]
;                  [ I J K L ]
;                  [ M N O P ]
;
.386
Mul_Matrix  proc
       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + m00 - m00 + 0 ]  ; A...
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       mov  _X,ax                      ; ....

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + m01 - m00 + 0 ]  ; ....
       imul cx                         ; E...
       shrd ax,dx,8                    ; ....
       add  _X,ax                      ; ....

       mov  ax,[ bx + 4 ]              ; AX = Z
       mov  cx,[ si + m02 - m00 + 0 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; I...
       add  _X,ax                      ; ....

       mov  ax,[ bx + 6 ]              ; AX = 1
       mov  cx,[ si + m03 - m00 + 0 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       add  _X,ax                      ; M...

       ;;;

       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + m00 - m00 + 2 ]  ; .B..
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       mov  _Y,ax                      ; ....

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + m01 - m00 + 2 ]  ; ....
       imul cx                         ; .F..
       shrd ax,dx,8                    ; ....
       add  _Y,ax                      ; ....

       mov  ax,[ bx + 4 ]              ; AX = Z
       mov  cx,[ si + m02 - m00 + 2 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; .J..
       add  _Y,ax                      ; ....

       mov  ax,[ bx + 6 ]              ; AX = 1
       mov  cx,[ si + m03 - m00 + 2 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       add  _Y,ax                      ; .N..

       ;;;

       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + m00 - m00 + 4 ]  ; ..C.
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       mov  _Z,ax                      ; ....

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + m01 - m00 + 4 ]  ; ....
       imul cx                         ; ..G.
       shrd ax,dx,8                    ; ....
       add  _Z,ax                      ; ....

       mov  ax,[ bx + 4 ]              ; AX = Z
       mov  cx,[ si + m02 - m00 + 4 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ..K.
       add  _Z,ax                      ; ....

       mov  ax,[ bx + 6 ]              ; AX = 1
       mov  cx,[ si + m03 - m00 + 4 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       add  _Z,ax                      ; ..O.

       mov  ax,_X
       mov  [ bx + 0 ],ax              ;
       mov  ax,_Y
       mov  [ bx + 2 ],ax              ;
       mov  ax,_Z
       mov  [ bx + 4 ],ax              ;

       ret
Mul_Matrix  endp

;======================================;
;      Init_Matrix                     ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix proc
.286
       mov  ax,100h
       xor  cx,cx
       mov  [ di + m00 - m00 + 0 ],ax  ; 1
       mov  [ di + m00 - m00 + 2 ],cx
       mov  [ di + m00 - m00 + 4 ],cx
       mov  [ di + m00 - m00 + 6 ],cx
       ;;;
       mov  [ di + m01 - m00 + 0 ],cx
       mov  [ di + m01 - m00 + 2 ],ax  ; 1
       mov  [ di + m01 - m00 + 4 ],cx
       mov  [ di + m01 - m00 + 6 ],cx
       ;;;
       mov  [ di + m02 - m00 + 0 ],cx
       mov  [ di + m02 - m00 + 2 ],cx
       mov  [ di + m02 - m00 + 4 ],ax  ; 1
       mov  [ di + m02 - m00 + 6 ],cx
       ;;;
       mov  [ di + m03 - m00 + 0 ],cx
       mov  [ di + m03 - m00 + 2 ],cx
       mov  [ di + m03 - m00 + 4 ],cx
       mov  [ di + m03 - m00 + 6 ],ax  ; 1
       ;;;
       ret
Init_Matrix endp

;======================================;
;      Init_Matrix_A                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_A proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m00 - m00 + 2 ],ax  ; -sin
       mov  [ di + m00 - m00 + 4 ],cx  ;  sin

       mov  [ di + m01 - m00 + 4 ],cx  ; -sin

       neg  ax
       mov  [ di + m02 - m00 + 0 ],ax  ;  cos
       mov  [ di + m02 - m00 + 2 ],ax  ;  cos

       popa
       ret
Init_Matrix_A endp

;======================================;
;      Init_Matrix_B                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_B proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin

       mov  [ di + m00 - m00 + 0 ],cx
       mov  [ di + m00 - m00 + 2 ],ax
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       popa
       ret
Init_Matrix_B endp


;======================================;
;      Init_Matrix_C                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_C proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin

       mov  [ di + m00 - m00 + 0 ],cx
       mov  [ di + m00 - m00 + 2 ],ax
       mov  [ di + m01 - m00 + 0 ],cx
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       mov  [ di + m01 - m00 + 0 ],ax
       popa
       ret
Init_Matrix_C endp


;======================================;
;      Init_Matrix_X                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_X proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       popa
       ret
Init_Matrix_X endp

;======================================;
;      Init_Matrix_Y                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_Y proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m00 - m00 + 0 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m02 - m00 + 0 ],ax  ;  sin
       neg  ax
       mov  [ di + m00 - m00 + 4 ],ax  ; -sin
       popa
       ret
Init_Matrix_Y endp

;======================================;
;      Init_Matrix_Z                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_Z proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m00 - m00 + 0 ],cx  ;  cos
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m00 - m00 + 2 ],ax  ;  sin
       neg  ax
       mov  [ di + m01 - m00 + 0 ],ax  ; -sin
       popa
       ret
Init_Matrix_Z endp


;======================================;
;      Put_Dot                         ;
;======================================;
;      In:  ax -> X                    ;
;           bx -> Y                    ;
;           cl -> Color                ;
;======================================;
Put_Dot     proc
       pusha
       push cx
       push bx
       push ax

       mov  dx,3c4h
       mov  ax,0202h                   ; map mask register with bitplane 2
       out  dx,ax

       mov  dl,0ceh
       mov  ax,0104h
       out  dx,ax

       mov  ax,0a000h
       mov  es,ax

       pop  ax
       pop  bx
       pop  cx

       xchg cl,ch                      ; Color = Ch ( Necesito Cl para count )

       mov  dx,bx
       shl  bx,6                       ; = bx * 80 = bx * 64 + bx * 16 =
       shl  dx,4                       ; = bx * 2^6 + bx * 2^4
       add  bx,dx

       mov  cl,7
       and  cl,al
       shr  ax,3
       add  bx,ax                      ; BX = offset listo!

       mov  al,128                     ; Bit 7 por default!
       shr  al,cl                      ; Traduce Num de bit a valor de Bit

       mov  dl,es:[ bx ]               ; Dl = Dato original
       or   ch,ch                      ; Prender o apagar ?
       jne  prender_bit

apagar_bit:
       not  al
       and  dl,al                      ; Debe decir     and dl,al
       jmp  DrawDot

prender_bit:
       or   dl,al                      ; debe decir      or dl,al
DrawDot:
       mov  es:[ bx ],dl               ; Cl

       popa
       ret
Put_Dot     endp

;======================================;
;      WaitEsc                         ;
;======================================;
waitesc proc
       push ax
       in   al,060h
       cmp  al,01h
       pop  ax
       ret
waitesc endp

;======================================;
;      VerticalR                       ;
;======================================;
verticalr proc
       mov  dx,VERT_RESCAN
ula1:
       in   al,dx            ;vertical rescan
       test al,8
       jne  ula1
ula2:
       in   al,dx
       test al,8
       je   ula2
       ret
verticalr endp

END
