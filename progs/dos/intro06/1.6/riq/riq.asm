;======================================;
;				       ;
;	       Rotaciones	       ;
;		 by Riq 	       ;
;				       ;					      ;
;======================================;
;				       ;
;     Fecha de inicio: 12 / 4 / 95     ;
;	       Version 1.2	       ;
;	     Fixed Point 8.8	       ;
;				       ;
;				       ;
;	      .COM version	       ;
;				       ;
;======================================;
;				       ;
;	   Copyright (c) 1995.	       ;
;				       ;
;======================================;

DOSSEG
MODEL TINY
P386
LOCALS

;======================================; DEFINES
VERT_RESCAN = 3DAh		       ; Vertical Rescan
ADD_X	    = 160		       ; Centrar Dots
ADD_Y	    = 100		       ;

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

;======================================;
;      Main_Loop		       ;
;======================================;
Main_Loop   proc
       cld
       Call Is386                      ; >=386
       Call IsVga                      ; Only Vga
       mov  ax,0a000h
       mov  es,ax
       mov  ax,13h
       int  10h 		       ; Sets standard 320x200x256

       mov  angle,0
       mov  addangle,1
       mov  incangle,3
       mov  out_,0
       mov  Loop1_Cnt,0
       Lea  di,Matrix_Sca
       Call Init_Matrix
       Lea  di,Vertex_Sca
       Call Init_Vertex

@@Loop1:
       Call New_Angle
       Call Clean_Dots
       Call Rot_Dots
       Call Draw_Dots
       Call VerticalR
       Call WaitEsc
       je   @@Loop2
       Inc  Loop1_Cnt
       Cmp  Loop1_Cnt,880
       jne  @@Loop1

@@Loop2:
       Call New_Angle
       Lea  di,Matrix_Sca
       Call Init_Matrix_Out
       Call Clean_Dots
       Call Rot_Dots
       Call Sca_Dots
       Call Draw_Dots
       Call VerticalR
       Inc  Out_
       Cmp  Out_,100
       jne  @@Loop2

       Jmp  MsgFinal
Main_Loop   endp

;======================================;
;      New_Angle		       ;
;======================================;
New_Angle   proc
       mov  bx,angle
       lea  di,Matrix_Rot
       cmp  addangle,1
       je   a_z
       cmp  addangle,2
       je   a_a
       cmp  addangle,3
       je   a_y
       cmp  addangle,4
       je   a_b
       cmp  addangle,5
       je   a_x

a_c:
       call Init_Matrix_C
       jmp a_fin
a_b:
       call Init_Matrix_B
       jmp a_fin
a_a:
       call Init_Matrix_A
       jmp a_fin

a_x:
       call Init_Matrix_X
       jmp  a_fin
A_Z:
       Call Init_Matrix_Z
       jmp  a_fin
a_Y:
       Call INit_Matrix_Y

a_fin:
       mov  bx,incangle
       add  angle,bx
       cmp  angle,360
       jb   New_Angle_
       mov  angle,0

       Call CpyVertex

       cmp  addangle,6
       je   Reset_Angle
       inc  addangle
       jmp  New_Angle_
Reset_Angle:
       mov  addangle,1

New_Angle_:
       ret
New_Angle   endp


;======================================;
;      Draw_Dots		       ;
;======================================;
Draw_Dots   proc
       xor  cx,cx		       ; Dots Counter
DD_loop:
       push cx

       mov  bx,cx                      ; : Calculo si el punto

       mov  cl,7
       and  cl,bl                      ; Pone el cl el Resto!
       shr  bx,3

       mov  dl,128
       shr  dl,cl                      ; Calculo el bit a ejecutar

       pop cx
       and  dl,Vertex_True[ bx ]
       je   @@here2                    ; Igual 0 ->

@@here1:
       push cx
       lea  si,Vertex_Rot              ; Donde esta el nuevo valor
       shl  cx,3		       ; Multiplica * 8 ( 4 * 2 )
       add  si,cx
       mov  ax,[ si + 0 ]	       ; X
       mov  bx,[ si + 2 ]	       ; Y
       mov  cl,15                      ; Color
       Call Put_Dot
       pop  cx
@@here2:
       inc  cx
       cmp  cx,TOTALDOTS
       jne  DD_loop

       ret
Draw_Dots   endp

;======================================;
;      Clean_Dots		       ;
;======================================;
Clean_Dots   proc
       xor  cx,cx		       ; Dots Counter
CD_loop:
       push cx
       lea  si,Vertex_Rot
       shl  cx,3		       ; Multiplica * 8
       add  si,cx
       mov  ax,[ si + 0 ]	       ; X
       mov  bx,[ si + 2 ]	       ; Y
       xor  cl,cl                      ; Color
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  CD_loop

       ret
Clean_Dots   endp

;======================================;
;      Rot_Dots 		       ;
;======================================;
Rot_Dots   proc
       pusha
       xor  cx,cx		       ; Dots Counter
RD_loop:
       push cx
       lea  di,Vertex_Sca
       lea  bx,Vertex_Rot
       lea  si,Matrix_Rot
       shl  cx,3		       ; Multiplica * 8
       add  di,cx
       add  bx,cx

       mov  ax,[ di + 0 ]	       ; X
       mov  [ bx + 0 ],ax
       mov  ax,[ di + 2 ]	       ; Y
       mov  [ bx + 2 ],ax
       mov  ax,[ di + 4 ]	       ; Z
       mov  [ bx + 4 ],ax
       mov  ax,[ di + 6 ]	       ; 1
       mov  [ bx + 6 ],ax
       call Mul_Matrix		       ; [BX] = New Pos

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  RD_loop

       popa
       ret
Rot_Dots   endp


;======================================;
;      Sca_Dots 		       ;
;======================================;
Sca_Dots   proc
       pusha
       xor  cx,cx		       ; Dots Counter
SD_loop:
       push cx
       lea  bx,Vertex_Sca
       lea  si,Matrix_Sca
       shl  cx,3		       ; Multiplica * 8
       add  bx,cx

       call Mul_Matrix		       ; [BX] = New Pos

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  SD_loop

       popa
       ret
Sca_Dots   endp

;======================================;
;      CpyVertex		       ;
;======================================;
CpyVertex  proc
       pusha
       xor  cx,cx		       ; Dots Counter
CV_loop:
       push cx
       lea  di,Vertex_Sca
       lea  si,Vertex_Rot
       shl  cx,3		       ; Multiplica * 8
       add  di,cx
       add  si,cx

;	mov  ax,[ si + 0 ]		; X
;	mov  [ di + 0 ],ax
;	mov  ax,[ si + 2 ]		; Y
;	mov  [ di + 2 ],ax
       mov  ax,[ si + 4 ]	       ; Z
       mov  [ di + 4 ],ax

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  CV_loop

       popa
       ret
CpyVertex   endp

;======================================;
;      Init_Vertex                     ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Vertex proc                       ; Funci�n para ahorrar bytes!
       pusha
       xor  cx,cx
       mov  dx,100h
       mov  bx,-40
@@loop2:
       mov  ax,-110
@@loop1:
       mov  [ di + 0 ],ax              ; Copia Patron (x,y,z,100h)
       mov  [ di + 2 ],bx              ; en todos los puntos
       mov  [ di + 4 ],cx              ; que van a ser movidos
       mov  [ di + 6 ],dx              ; De -30 a 40 = Y
       add  di,8                       ; De -110 a 120 = X
       add  ax,10
       cmp  ax,130                     ; Hasta 120
       jne  @@loop1
       add  bx,10
       cmp  bx,60                      ; Hasta 50
       jne  @@loop2

       popa
       ret
Init_Vertex endp


;======================================;
;      Mul_Matrix		       ;
;======================================;
;      In:  bx -> offset Vertex (1x4)  ;
;	    si -> offset Matrix (4x4)  ;
;      Out: bx -> offset new Vertex    ;
;======================================;
;
;  [ x y z 1 ]	*  [ A B C D ]
;		   [ E F G H ]
;		   [ I J K L ]
;		   [ M N O P ]
;
Mul_Matrix  proc
       mov  ax,[ bx + 0 ]	       ; AX = X
       mov  cx,[ si + m00 - m00 + 0 ]  ; A...
       imul cx			       ; ....
       shrd ax,dx,8		       ; ....
       mov  _X,ax		       ; ....

       mov  ax,[ bx + 2 ]	       ; AX = Y
       mov  cx,[ si + m01 - m00 + 0 ]  ; ....
       imul cx			       ; E...
       shrd ax,dx,8		       ; ....
       add  _X,ax		       ; ....

       mov  ax,[ bx + 4 ]	       ; AX = Z
       mov  cx,[ si + m02 - m00 + 0 ]  ; ....
       imul cx			       ; ....
       shrd ax,dx,8		       ; I...
       add  _X,ax		       ; ....

       mov  ax,[ bx + 6 ]	       ; AX = 1
       mov  cx,[ si + m03 - m00 + 0 ]  ; ....
       imul cx			       ; ....
       shrd ax,dx,8		       ; ....
       add  _X,ax		       ; M...

       ;;;

       mov  ax,[ bx + 0 ]	       ; AX = X
       mov  cx,[ si + m00 - m00 + 2 ]  ; .B..
       imul cx			       ; ....
       shrd ax,dx,8		       ; ....
       mov  _Y,ax		       ; ....

       mov  ax,[ bx + 2 ]	       ; AX = Y
       mov  cx,[ si + m01 - m00 + 2 ]  ; ....
       imul cx			       ; .F..
       shrd ax,dx,8		       ; ....
       add  _Y,ax		       ; ....

       mov  ax,[ bx + 4 ]	       ; AX = Z
       mov  cx,[ si + m02 - m00 + 2 ]  ; ....
       imul cx			       ; ....
       shrd ax,dx,8		       ; .J..
       add  _Y,ax		       ; ....

       mov  ax,[ bx + 6 ]	       ; AX = 1
       mov  cx,[ si + m03 - m00 + 2 ]  ; ....
       imul cx			       ; ....
       shrd ax,dx,8		       ; ....
       add  _Y,ax		       ; .N..

       ;;;

       mov  ax,[ bx + 0 ]	       ; AX = X
       mov  cx,[ si + m00 - m00 + 4 ]  ; ..C.
       imul cx			       ; ....
       shrd ax,dx,8		       ; ....
       mov  _Z,ax		       ; ....

       mov  ax,[ bx + 2 ]	       ; AX = Y
       mov  cx,[ si + m01 - m00 + 4 ]  ; ....
       imul cx			       ; ..G.
       shrd ax,dx,8		       ; ....
       add  _Z,ax		       ; ....

       mov  ax,[ bx + 4 ]	       ; AX = Z
       mov  cx,[ si + m02 - m00 + 4 ]  ; ....
       imul cx			       ; ....
       shrd ax,dx,8		       ; ..K.
       add  _Z,ax		       ; ....

       mov  ax,[ bx + 6 ]	       ; AX = 1
       mov  cx,[ si + m03 - m00 + 4 ]  ; ....
       imul cx			       ; ....
       shrd ax,dx,8		       ; ....
       add  _Z,ax		       ; ..O.

       mov  ax,_X
       mov  [ bx + 0 ],ax	       ;
       mov  ax,_Y
       mov  [ bx + 2 ],ax	       ;
       mov  ax,_Z
       mov  [ bx + 4 ],ax	       ;

       ret
Mul_Matrix  endp

;======================================;
;      Init_Matrix		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix proc
       mov  ax,100h
       xor  cx,cx
       mov  [ di + m00 - m00 + 0 ],ax  ; 1
       mov  [ di + m00 - m00 + 2 ],cx
       mov  [ di + m00 - m00 + 4 ],cx
       mov  [ di + m00 - m00 + 6 ],cx
       ;;;
       mov  [ di + m01 - m00 + 0 ],cx
       mov  [ di + m01 - m00 + 2 ],ax  ; 1
       mov  [ di + m01 - m00 + 4 ],cx
       mov  [ di + m01 - m00 + 6 ],cx
       ;;;
       mov  [ di + m02 - m00 + 0 ],cx
       mov  [ di + m02 - m00 + 2 ],cx
       mov  [ di + m02 - m00 + 4 ],ax  ; 1
       mov  [ di + m02 - m00 + 6 ],cx
       ;;;
       mov  [ di + m03 - m00 + 0 ],cx
       mov  [ di + m03 - m00 + 2 ],cx
       mov  [ di + m03 - m00 + 4 ],cx
       mov  [ di + m03 - m00 + 6 ],ax  ; 1
       ;;;
       ret
Init_Matrix endp

;======================================;
;      Init_Matrix_Out		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_Out proc
       pusha
       Call Init_Matrix
       mov  ax,-2h
       mov  bx,-2h
       mov  cx,1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;
       popa
       ret
Init_Matrix_Out endp


;======================================;
;      Init_Matrix_A		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;	    bx -> Angle 	       ;
;======================================;
Init_Matrix_A proc
       pusha
       Call Init_Matrix
       push bx
       Call Cosin                      ;
       mov  cx,ax                      ; Cx = Cos
       pop  bx
       Call Sin                        ; Ax = Sin
       mov  [ di + m00 - m00 + 2 ],ax  ; -sin
       mov  [ di + m00 - m00 + 4 ],cx  ;  sin

       mov  [ di + m01 - m00 + 4 ],cx  ; -sin

       neg  ax
       mov  [ di + m02 - m00 + 0 ],ax  ;  cos
       mov  [ di + m02 - m00 + 2 ],ax  ;  cos

       popa
       ret
Init_Matrix_A endp

;======================================;
;      Init_Matrix_B		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;	    bx -> Angle 	       ;
;======================================;
Init_Matrix_B proc
       pusha
       Call Init_Matrix
       push bx
       Call Cosin                      ;
       mov  cx,ax                      ; Cx = Cos
       pop  bx
       Call Sin                        ; Ax = Sin
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin

       mov  [ di + m00 - m00 + 0 ],cx
       mov  [ di + m00 - m00 + 2 ],ax
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       popa
       ret
Init_Matrix_B endp


;======================================;
;      Init_Matrix_C		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;	    bx -> Angle 	       ;
;======================================;
Init_Matrix_C proc
       pusha
       Call Init_Matrix
       push bx
       Call Cosin                      ;
       mov  cx,ax                      ; Cx = Cos
       pop  bx
       Call Sin                        ; Ax = Sin
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin

       mov  [ di + m00 - m00 + 0 ],cx
       mov  [ di + m00 - m00 + 2 ],ax
       mov  [ di + m01 - m00 + 0 ],cx
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       mov  [ di + m01 - m00 + 0 ],ax
       popa
       ret
Init_Matrix_C endp


;======================================;
;      Init_Matrix_X		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;	    bx -> Angle 	       ;
;======================================;
Init_Matrix_X proc
       pusha
       Call Init_Matrix
       push bx
       Call Cosin                      ;
       mov  cx,ax                      ; Cx = Cos
       pop  bx
       Call Sin                        ; Ax = Sin
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       popa
       ret
Init_Matrix_X endp

;======================================;
;      Init_Matrix_Y		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;	    bx -> Angle 	       ;
;======================================;
Init_Matrix_Y proc
       pusha
       Call Init_Matrix
       push bx
       Call Cosin                      ;
       mov  cx,ax                      ; Cx = Cos
       pop  bx
       Call Sin                        ; Ax = Sin
       mov  [ di + m00 - m00 + 0 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m02 - m00 + 0 ],ax  ;  sin
       neg  ax
       mov  [ di + m00 - m00 + 4 ],ax  ; -sin
       popa
       ret
Init_Matrix_Y endp

;======================================;
;      Init_Matrix_Z		       ;
;======================================;
;      In:  di -> offset Matrix        ;
;	    bx -> Angle 	       ;
;======================================;
Init_Matrix_Z proc
       pusha
       Call Init_Matrix
       push bx
       Call Cosin                      ;
       mov  cx,ax                      ; Cx = Cos
       pop  bx
       Call Sin                        ; Ax = Sin
       mov  [ di + m00 - m00 + 0 ],cx  ;  cos
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m00 - m00 + 2 ],ax  ;  sin
       neg  ax
       mov  [ di + m01 - m00 + 0 ],ax  ; -sin
       popa
       ret
Init_Matrix_Z endp


;======================================;
;      Put_Dot			       ;
;======================================;
;      In:  ax -> X		       ;
;	    bx -> Y		       ;
;	    dx -> Z		       ;
;	    cl -> Color 	       ;
;======================================;
Put_Dot     proc
       pusha

       add  ax,ADD_X
       add  bx,ADD_Y

       cmp  ax,320
       jae  Put_Dot_
       Cmp  bx,200
       jae  Put_Dot_

       mov  dx,bx
       shl  bx,8		       ; = bx * 320 = bx * 256 + bx * 64 =
       shl  dx,6		       ; = bx * 2^8 + bx * 2^6
       add  bx,dx
       add  bx,ax		       ; BX -> Offset del Video
       mov  es:[ bx ],cl               ; Cl

Put_Dot_:
       popa
       ret
Put_Dot     endp

;======================================;
;      WaitEsc			       ;
;======================================;
waitesc proc
       push ax
       in   al,060h
       cmp  al,01h
       pop  ax
       ret
waitesc endp

;======================================;
;      Sin - Cosin                     ;
;======================================;
;      In:  Bx -> Angle 0 - 359        ;
;     Out:  Ax -> f( Ax )              ;
;======================================;
Cosin:
       add  bx,90                      ; Suma 90 al �ngulo
Sin:
       cmp  bx,90
       jb   Cuadran1
       cmp  bx,180
       jb   Cuadran2
       cmp  bx,270
       jb   Cuadran3
       cmp  bx,360
       jb   Cuadran4
       sub  bx,360                     ; Es mayor que 360 -> cos
       jmp   Cuadran1

Cuadran2:
       mov  dx,90
       sub  bx,dx                      ; Primero le resto 90
       sub  dx,bx                      ; Luego a 90 le resto lo que quedo
       xchg bx,dx                      ; y Ahora en bx esta el angulo a calcular
Cuadran1:
       Call Fn_sin
       ret

Cuadran4:
       mov  dx,270
       sub  bx,dx                      ; Primero le resto 270
       sub  dx,bx                      ; Luego a 270 le resto lo que quedo
       xchg bx,dx                      ; y Ahora en bx esta el angulo a calcular
Cuadran3:
       sub  bx,180
       Call Fn_sin
       neg  ax
       ret

Fn_sin:
       shl  bx,1
       mov  ax,SinTbl[bx]
       ret


;======================================;
;      VerticalR		       ;
;======================================;
verticalr proc
       mov  dx,3dah
@@here1:
       in   al,dx
       test al,8
       jne  @@here1
@@here2:
       in   al,dx
       test al,8
       je   @@here2
       ret
verticalr endp

;======================================;
;      Is386                           ;
;======================================;
Is386  proc
       mov  ax,7000h
       push ax
       popf
       pushf
       pop  ax
       and  ax,7000h
       je   @@here1
       ret
@@here1:                               ; Not 386
       jmp  MsgFinal
Is386  endp

;======================================;
;      IsVga                           ;
;======================================;
IsVga  proc
       mov  ax,1a00h
       int  10h
       cmp  al,01ah
       jne  @@here1
       cmp  bl,7
       jb   @@here1
       cmp  bl,9
       ja   @@here1
       ret
@@here1:                               ; Not VGA
       jmp  MsgFinal
IsVga  endp

;======================================;
;      MsgFinal                        ;
;======================================;
MsgFinal    proc
       mov  ax,3
       int  10h

       mov  cx,Msg_Lenght
       xor  bx,bx
@@loop1:
       mov  ah,2
       mov  dl,Final_Msg[ bx ]
       xor  dl,21
       inc  bx
       int  21h
       loop @@loop1

       mov  ah,4ch
       int  21h
MsgFinal    endp

;======================================; INITIALIZED DATA

; SinTable created by Jon Beltran de Heredia
; Sines are in 8.8 fixedpoint
; Reducida a solo 90 valores por Ricardo Quesada. Original de 450 valores.
; Funcion Cosin,Sin Creadas por Ricardo Quesada

SinTbl  LABEL   WORD

dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62                    ; 0
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124          ; 15
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178  ; 30
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219  ; 45
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246  ; 60
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256  ; 75
dw 256

Vertex_True label Byte
db     11111110b,00000000b,00000000b
db     11111111b,00111000b,00000000b
db     11000011b,00111000b,01111101b
db     11111111b,00000000b,11111111b
db     11111110b,01111100b,11000011b
db     11000011b,00111000b,11000011b
db     11000011b,00111000b,11111111b
db     11000011b,01111100b,01111111b
db     00000000b,00000000b,00000011b
db     00000000b,00000000b,00000011b

Final_Msg label byte
db     'I' XOR 21,'n' XOR 21,'t' XOR 21,'r' XOR 21,'o' XOR 21,' ' XOR 21,'6' XOR 21,' ' XOR 21,'"' XOR 21,'R' XOR 21,'i' XOR 21,'q' XOR 21,' ' XOR 21,'-' XOR 21,' ' XOR 21,'0' XOR 21,'1' XOR 21,'"' XOR 21,' ' XOR 21,'b' XOR 21,'y' XOR 21,' ' XOR 21,'R' XOR 21,'i' XOR 21,'q' XOR 21,'.' XOR 21,' ' XOR 21,'-' XOR 21,' ' XOR 21,'1' XOR 21,'4' XOR 21,'-' XOR 21,'1' XOR 21,'6' XOR 21,'/' XOR 21,'4' XOR 21,'/' XOR 21,'9' XOR 21,'5' XOR 21,' ' XOR 21,'-' XOR 21,13 XOR 21,10 XOR 21
Msg_lenght  = $-Final_Msg

;======================================; UNINITIALIZED DATA

Vertex_Sca  label word                 ; X,Y,Z,Movible
       dw   10*8*3 dup(0,0,0,0)

TOTALDOTS = ($-VERTEX_SCA)/8


Vertex_Rot  label word
       dw   TOTALDOTS dup(0,0,0,0)

angle	 dw ?			       ; 0 - 360
incangle dw ?			       ; 1 - Incremento del angulo.
addangle dw ?			       ; 1 -
Out_	 dw ?			       ; 0 - Contador de espera de salida despues del Esc
Loop1_Cnt dw ?                         ; 0 - Contador del Main Loop

Matrix_Rot  label word		       ; Datos usados por Mul_Matrix
m00    dw   0,0,0,0
m01    dw   0,0,0,0
m02    dw   0,0,0,0
m03    dw   0,0,0,0

Matrix_Sca  label word		       ; Datos usados por Mul_Matrix
       dw   0,0,0,0
       dw   0,0,0,0
       dw   0,0,0,0
       dw   0,0,0,0

p00    = 0			       ; Defines inline
_X     dw   0			       ; Datos Temporales de Mul_Matrix
p01    = $ - _X
_Y     dw   0
p02    = $ - _X
_Z     dw   0
p03    = $ - _X


END
