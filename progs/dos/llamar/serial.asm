;Usando la salida Serial...
;por Ricardo (que se olvido de programar) Quesada...
;Loco ... Funciona aunque no lo creas...!!!

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:

        mov ah,0
        mov al,10100011B                ;2400baud / P: none/SB: 1/WL: 8bits
        mov dx,0                        ;dh=0 com1 /=1 com2
        int 14h

        mov si,offset send_msg - 1

loop1:

        inc si
        mov ah,1
        mov dx,0                        ;com 1
        mov al,[si]                     ;send one char to com
        int 14h

        and ah,10000000b
        cmp ah,10000000b
        je error

        mov dl,[si]
        mov ah,2
        int 21h

        cmp byte ptr [si],13
        jne loop1



        mov ah,2
        mov dl,13
        int 21h
        mov dl,10                               ;(Por prolijidad)
        int 21h                                 ;solo manda un Enter




        xor di,di
        mov si,offset msg_msg0
loop2:
        mov ah,2
        mov dx,0
        int 14h                         ;recieve one char in AL

        cmp di,2
        jne seguir2
        inc si

seguir2:
        mov [si],al
        cmp al,13
        jne seguir
        inc di

seguir:
        cmp di,3
        jb loop2


        mov ah,2
        mov dl,13
        int 21h
        mov dl,10                               ;(Por prolijidad)
        int 21h                                 ;solo manda un Enter


final:
        mov ah,9
        mov dx,offset msg_msg1
        int 21h


        mov cx,5
        lea si,msg_msg1
        lea di,msg_msg2
        mov ah,[di]
repe    cmps msg_msg1,msg_msg2
        jne Distintos

        mov ah,9
        mov dx,offset con_msg
        int 21h

        mov ax,4c00h
        int 21h

Distintos:
        mov ah,9
        mov dx,offset bus_msg
        int 21h

        mov ax,4c01h
        int 21h


error:
        mov ah,9
        mov dx,offset error_msg
        int 21h

        mov ax,4c00h
        int 21h



error_msg db 13,10,'Error...',13,10,'$'
send_msg  db 'ATDP409931',13
msg_msg0  db '$$'
msg_msg1  db '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
msg_msg2  db 'CONNECT'
con_msg   db 13,10,'Conectado...',13,10,'$'
bus_msg   db 13,10,'Otra vez sera...  Gil!!',13,10,'$'


ruti    endp
code    ends
        end ruti
