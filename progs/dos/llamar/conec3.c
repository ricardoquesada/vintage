/****************************************************/
/*                                                  */
/*                                                  */
/*                  llamar.exe                      */
/*                                                  */
/*           conec3.c    y    varios m s...         */
/*                                                  */
/*   llama a los numero que estan en llamar.cfg     */
/*                                                  */
/****************************************************/

#define FALSE 0
#define TRUE not FALSE


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <io.h>
#include <dos.h>
#include <asynch_1.h>
#include <asynch_h.h>
#include <process.h>
#include "texto.h"

/* variables globales */
int handle;
int com;
static char buf[200];
static MODEM **modem;


/*************************************************************/
/************* void error ( int ) ****************************/
/*************************************************************/
/*              reporta el error que se le mande             */
/*************************************************************/
void error( int ercode )
{

    static char *ermsg[] =
    {
	   "\nTerminacion normal. De casualidad!\n",    /* 0 */
	   "\nError en la lectura de archivo.\n",        /* 1 */
	   "\nError al inicializar el modem.\n",         /* 2 */
	   "\nError desconocido.\n",                     /* 3 */
	   "\nArchivo LLAMAR.CFG no encontrado.\n",      /* 4 */
	   "\nError en comunicacion.\n",                 /* 5 */
	   "\nError en linea de comando.\nFormato: llamar [/COM] .\nDonde COM es el numero de com [de 1 a 8].\nPor defecto se asume el COM = 1.\n",
	   "\nError al cerrar el com.\n",                /* 7 */
	   "\nError al intentar el llamado.\n",          /* 8 */
	   "\nError: Lcom no se ha encontrado.\n",       /* 9 */
	   "\n...\n"
    };

    printf( "%s", ermsg[ercode] );
    exit(ercode);
}

/*************************************************************/
/************* void mreport ( int ) **************************/
/*************************************************************/
/*          reporta la respuesta del modem                   */
/*************************************************************/
int mreport( int code )
{

    static char *codemsg[] =
    {
	"\nOK\n",                           /* 0 */
	"\nCONNECT\n",                      /* 1 */
	"\nRING\n",                         /* 2 */
	"\nNO CARRIER\n",                   /* 3 */
	"\nERROR\n",                        /* 4 */
	"\nCONNECT 1200\n",                 /* 5 */
	"\nNO DIALTONE\n",                  /* 6 */
	"\nBUSY\n",                         /* 7 */
	"\nNO ANSWER\n",                    /* 8 */
	"\RESERVED\n",                      /* 9 */
	"\nCONNECT 2400\n"                  /* 10 */
    };

    if (code > 10 || code < 0 )
	printf("INVALID RESPONSE: %d.\n",code);

    else printf( "%s", codemsg[code] );

    return code;
}

/*************************************************************/
/**************  int lcom ( void )  **************************/
/*************************************************************/
int lcom(void)
{
    /* variables */
    int er;
    char far ** vect;
    unsigned seg,env;

    er = iscom_a1( 0 ,&seg ,&env ,vect );
    return( er );
}
/*************************************************************/
/************** void abrir ( void ) **************************/
/*************************************************************/
void abrir(void)
{
    if ( (handle = _open("llamar.cfg",O_RDWR)) == -1)
	error(4);
}


/*************************************************************/
/************** void cerrar( void ) **************************/
/*************************************************************/
void cerrar( void )
{

    close_a1( com );
    _close( handle );
    spawnl(P_WAIT,"lcom.exe","lcom","/r","/q",NULL);

}

/*************************************************************/
/*************** int inic_modem ( void ) *********************/
/*************************************************************/
int inic_modem(void)
{

 /* variables locales */
    int er;
    int rcode;
    int loop = 1;

    printf("\nTratando de inicializar modem...\n");

    while (loop == 1) {
	er = open_a1( com,50,50,0,0,buf);
	switch (er) {
		case 0: loop = 0;
			break;
		case 9: close_a1( com );
			break;
		default: error(2);
	}
    }

    setop_a1( com,1,B_2400 );
    setop_a1( com,2,0 );
    setop_a1( com,3,3 );
    setop_a1( com,4,0 );
    er = struc_hm( modem );        /* tira null pointer assigment */
    er += init_hm( *modem , &rcode , 1);
    er += rcode;
    if (er)           /* not zero */
	error(2);
    return(0);
}

/*************************************************************/
/*************** int leer ( void ) ***************************/
/*************************************************************/
int leer(void)
{
   /* variables locales */
   char buff[80];
   char paso;
   int i=0;
   int num=1;
   int bbb;
   int er = 1; 		/* igual 0 es end of file */

    do {
	if ( (er =_read(handle,buff+i,1)) == -1 )
	    error(1);

	if (buff[i] == ';')
	{
	    do
	    {
		 if ( (er =_read(handle,&paso,1)) == -1)
		    error(1);
	    } while (paso != 10);
	    i--;
	}

	if (buff[i] == 10)
	{
	    buff[i-1] = 0;
	    printf("DATOS RECOGIDOS: %s.\n",buff);
	    buff[10] = 0;
	    printf("Llamando al numero:%s\n",buff);

	    if ( dial_hm( *modem,buff,&bbb,0) != 0)
		error(8);

	    if ( mreport( bbb ) == 1 ) {
		hngup_hm( *modem );
		cerrar();
		error( 0 );
	    }

	    printf("\nIntentando con otro numero.\n");

	    i = -1;
	    num++;

	}
	i++;

    } while ( er != 0 );

    return 0;
}



/*************************************************************/
/**************        main()      ***************************/
/*************************************************************/
void main(int argc, char* argv[])
{

   /* variables locales */
   unsigned er;

   clrscr();
   textcolor(WHITE);

   caja(5,1,75,8);
   centrar("Llamar v1.0b",3);
   centrar("Hecho por Ricardo Quesada.",4);
   centrar("Aunque todavia tenga algunos bugs...",5);
   centrar("esto funciona!!!",6);

   textcolor(LIGHTGRAY);
   com = 1;     	     /* por defecto se asume com 1 */

   printf("\n\n\n\n\n");

   if (argc == 2) {
     if ( (com = atoi( argv[1]+1 ) ) == 0 )
	error(6);
     else if( com < 1 || com > 8 )
	error(6);
   }
   else if (argc > 2)
	error(6);

   printf("COM:%d.\n",com);

   if( lcom() == 0 ) {
	printf("Cargando LCOM.EXE...");
	er = spawnl(P_WAIT,"lcom.exe","lcom","/q",NULL);
	if( er != 768 )             /* es el que tira lcom si se carga */
		error(9);
	printf("OK.\n");
   }


   abrir();             /* llamar.cfg */
   inic_modem();                /* inicializa modem */
   printf("Modem inicializado.\n");

   leer();                      /* lee de llamar.cfg y disca */

   cerrar();

   return;
}
