comment|
Se mezcla con conec2.c
Es parte de un programa de comunicaciones.
|
	DOSSEG
	PUBLIC _DialNum, _Comuniq, _MandarC
	.MODEL small, C
	.CODE


;***********************************;
;_DialNum.
;Disca un numero por telefono.
;Usada por C.
;Entrada:
;Sptr:offset de la cadena de informacion
;Com:numero de com
;Salida:
;AX:0=OK,1=Error.
;***********************************;
_DialNum PROC USES DS SI DI, Sptr, Com:WORD


	mov si,Sptr
        mov dx,Com
	dec dx
	mov di,offset numero

	mov byte ptr cx,7
vuelta:
	mov ax,[si]
	mov cs:[di],ax
	inc si
	inc di
	dec cx
	cmp cx,0
	jne vuelta


	push dx

	mov ah,0
	mov al,10100011B                ;2400baud / P: none/SB: 1/WL: 8bits
	int 14h

	pop dx

	mov si,offset send_atdp - 1
	push cs
	pop ds
	call Mandar

	ret

send_atdp db 'ATDP'
numero    db 13,13,13,13,13,13,13,13,13,13,'$'

_DialNum ENDP


;***********************************;
;_MandarC                           ;
;Manda un cadena al com             ;
;Usada por C.                       ;
;Entrada:                           ;
;*datos, com
;Salida:                            ;
;AX: 0=comunicado, 1=no comunicado  ;
;***********************************;
_MandarC PROC
        USES DS,DX,SI
        ARG Cadena,Com

        mov si,Cadena - 1
        mov dx,Com
        call Mandar

        ret

_mandarC ENDP

;***********************************;
;_Comuniq.                          ;
;Detecta si se establecio comunicacion
;Usada por C.                       ;
;Entrada:                           ;
;Com: numero de com
;Salida:                            ;
;AX: 0=comunicado, 1=no comunicado  ;
;***********************************;
_Comuniq PROC USES DS, Com:WORD

        mov dx,Com
	dec dx
	mov ah,3
	int 14h

	and al,10000000b        ;carrier dectect if on
	cmp al,10000000b
	jne no_carrier

	xor ax,ax
	ret                     ;carrier detected...

no_carrier:
	mov byte ptr ax,1
	ret

_Comuniq ENDP


;***********************************;
;Mandar.                            ;
;Manda una cadena al com
;Entrada:                           ;
;ds:si - 1:offset del mensaje a mandar ;
;dx: que es el com
;Salida:                            ;
;ax: 0=OK, 1=Error.                 ;
;***********************************;
Mandar PROC
       USES DS,DX,SI,DI

	mov cs:caca,dx
loop1:
	inc si
	mov ah,1
	mov al,[si]                     ;send one char to com
	mov dx,cs:caca
	int 14h

	and ah,10000000b
	cmp ah,10000000b
	je error

	mov dl,[si]
	mov ah,2
	int 21h                         ;escribe en pantalla lo que manda

	cmp byte ptr [si],13
	jne loop1

	mov ah,2
	mov dl,13
	int 21h
	mov dl,10                               ;(Por prolijidad)
	int 21h                                 ;solo manda un Enter

	xor ax,ax
	ret

error:
	mov ah,2
	mov dl,13
	int 21h
	mov dl,10                               ;(Por prolijidad)
	int 21h                                 ;solo manda un Enter

	mov byte ptr ax,1
	ret

caca	dw 0

Mandar  ENDP


        END

