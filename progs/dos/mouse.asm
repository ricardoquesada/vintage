;name: mouse
;verion: 1.00
;start  date: 10/11/93
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:
        mov ax,004dh
        int 33h

denuevo:
        mov dl,es:[di]
        cmp dl,0
        je fin

        mov ah,2
        int 21h
        inc di
        jmp short denuevo


fin:
        mov ax,4c00h
        int 21h


ruti    endp
code    ends
        end ruti
