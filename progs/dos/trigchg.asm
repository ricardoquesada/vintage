PSEG    SEGMENT PARA PUBLIC 'CODE'
BEGIN   EQU     $
        ASSUME  CS:PSEG, DS:PSEG, ES:PSEG, SS:NOTHING
        ORG     100H
MAIN    PROC    NEAR
START   EQU     $
        push    cs
        push    cs
        pop     ds
        pop     es
        MOV     DX, OFFSET CFH
        MOV     AH, 09h
        INT     21h
        MOV     DI, OFFSET ARGS
        CALL    GET_ARGS
        INC     DI
        mov     ax, word ptr [di]
        mov     word ptr [num], ax
        mov     cx, 2
        mov     si, di
        call    convert_to_binary_number

        MOV     DX, OFFSET DISAB
        cmp     bx, 0
        JZ      DO_IT

        MOV     DX, OFFSET TRIGMSG

DO_IT:  PUSH    DX
        mov     ax, 69h
        mov     cx, ax
        INT     49h
        CMP     AX, 1
        JNE     NOT_INSTALLED
        CMP     BX, 2
        JNE     NOT_INSTALLED
        CMP     CX, 3
        JNE     NOT_INSTALLED
        CMP     DX, 4
        JNE     NOT_INSTALLED
        POP     DX
        MOV     AH, 09h
        INT     21h
        MOV     AX, 4C00h
        INT     21h

NOT_INSTALLED:
        MOV     DX, OFFSET NOT_IN
        MOV     AH, 09H
        INT     21h
        JMP     BAD_EXIT


NOT_GOOD:
        MOV     DX, OFFSET BAD_MSG
        MOV     AH, 09h
        INT     21h

BAD_EXIT:
        MOV     AX, 4C01h
        INT     21h
MAIN    ENDP


ten             dw 10
ARGS    DB 127 DUP(0)
BAD_MSG DB "[1;5;31mInvalid command line parameter[0;1;37m",13,10,13,10
        DB "SYNTAX:   TRIGCHG #",13,10,13,10
        DB "# = a number from 0 to 99",13,10,13,10
        DB "TRIGCHG 0  will DISABLE the phone line monitor[0m",13,10,13,10,"$"
CFH     DB 13,10,13,10,"[1;37;44mTrigger-point changer by "
        DB "[1;33;44mCFH[1;37;44mSoftware[1;37;40m",13,10,13,10,"$"
disab   DB "[1;5;31mThe PHONE monitor has been disabled[0m",13,10,13,10,"$"
trigmsg DB "The PHONE monitor will reboot the system on ring number "
num     DB "  [0m",13,10,13,10,"$"
NOT_IN  DB 13,10,"CFH's Phone monitor isn't installed!!!",13,10,"$"

; get_args
; gets the command line information from the psp
;ENTRY:
;     di points to a 127 byte buffer into which you want the command line
;        info copied.
;EXIT:
;     cx contains the number of characters on the command line
;     di points to the "buffer" area into which the info was copied
;     all other registers previous values ARE preserved (i.e. not destroyed)
get_args proc
        push ax
        push bx
        push di
        push si
        push ds
        mov ah, 62h
        int 21h
        push bx
        pop ds
        mov si, 80h
        lodsb
        xor ch, ch
        mov cl, al
        push cx
        rep movsb
        pop cx
        pop ds
        pop si
        pop di
        pop bx
        pop ax
        ret
get_args endp

convert_to_binary_number proc
        xor bx, bx
@@:
        xor ax, ax
        lodsb
        cmp al, 57
        jg non_numeric
        sub ax, 48
        jl non_numeric
        push ax
        mov ax, bx
        mul ten
        mov bx, ax
        pop ax
        add bx, ax
space:  loop @b
non_numeric:
        ret
convert_to_binary_number endp

PRG_END EQU     $
PSEG    ENDS
        END     MAIN
