  
PAGE  59,132
  
;��������������������������������������������������������������������������
;��								         ��
;��			        PARK2				         ��
;��								         ��
;��      Created:   20-Sep-88					         ��
;��      Version:						         ��
;��      Passes:    9	       Analysis Options on: H		         ��
;��								         ��
;��								         ��
;��������������������������������������������������������������������������
  
data_1e		equ	6Ch			; (0040:006C=8D97h)
  
seg_a		segment	byte public
		assume	cs:seg_a, ds:seg_a
  
  
		org	100h
  
park2		proc	far
  
start:
		jmp	short loc_1		; (0108)
		db	90h
data_2		dw	0
data_3		dw	9097h
data_4		db	0
loc_1:
		cld				; Clear direction
		call	sub_3			; (01CB)
		mov	si,80h
loc_2:
		mov	ah,8
		mov	dx,si
		int	13h			; Disk  dl=drive 0  ah=func 08h
						;  read parameters for drive dl
		add	dl,30h			; '0'
		mov	data_7,dl		; (9097:0235=31h)
		xor	dh,dh			; Zero register
		add	dx,4Fh
		mov	di,dx
		cmp	di,80h
		jb	loc_8			; Jump if below
		jz	loc_3			; Jump if zero
		mov	byte ptr data_8,73h	; (9097:023C=2Eh) 's'
		nop
loc_3:
		inc	ch
		jnc	loc_4			; Jump if carry=0
		add	cl,40h			; '@'
loc_4:
		mov	ax,0C01h
		mov	dx,si
		int	13h			; Disk  dl=drive 0  ah=func 0Ch
						;  seek, dh=head, cx=cylinder
		sti				; Enable interrupts
		inc	si
		cmp	si,di
		jbe	loc_2			; Jump if below or =
		mov	si,1ECh
		call	sub_1			; (01A0)
		mov	si,26Dh
		call	sub_1			; (01A0)
		mov	ah,2
		xor	bh,bh			; Zero register
		mov	dx,1800h
		int	10h			; Video display   ah=functn 02h
						;  set cursor location in dx
		mov	ax,40h
		mov	cx,19h
		push	ds
		mov	ds,ax
  
locloop_5:
		mov	ax,ds:data_1e		; (0040:006C=8DA0h)
loc_6:
		cmp	ax,ds:data_1e		; (0040:006C=8DA0h)
		je	loc_6			; Jump if equal
		loop	locloop_5		; Loop if cx > 0
  
		pop	ds
		mov	ax,3509h
		int	21h			; DOS Services  ah=function 35h
						;  get intrpt vector al in es:bx
		mov	data_2,bx		; (9097:0103=0)
		mov	data_3,es		; (9097:0105=9097h)
		mov	dx,1E1h
		mov	ax,2509h
		int	21h			; DOS Services  ah=function 25h
						;  set intrpt vector al to ds:dx
loc_7:
		sti				; Enable interrupts
		test	data_4,0FFh		; (9097:0107=0)
		jz	loc_7			; Jump if zero
		mov	dx,cs:data_2		; (9097:0103=0)
		mov	ds,cs:data_3		; (9097:0105=9097h)
		mov	ax,2509h
		int	21h			; DOS Services  ah=function 25h
						;  set intrpt vector al to ds:dx
		call	sub_3			; (01CB)
loc_8:
		int	20h			; Program Terminate
  
park2		endp
  
;��������������������������������������������������������������������������
;			       SUBROUTINE
;��������������������������������������������������������������������������
  
sub_1		proc	near
		lodsw				; String [si] to ax
		mov	dx,ax
loc_9:
		cmp	byte ptr [si],0
		je	loc_10			; Jump if equal
		mov	ah,2
		xor	bh,bh			; Zero register
		int	10h			; Video display   ah=functn 02h
						;  set cursor location in dx
		call	sub_2			; (01C5)
		inc	dh
		jmp	short loc_9		; (01A3)
loc_10:
		mov	ah,2
		xor	bh,bh			; Zero register
		mov	dx,1700h
		int	10h			; Video display   ah=functn 02h
						;  set cursor location in dx
		retn
sub_1		endp
  
loc_11:
		mov	ah,0Eh
		mov	bh,0
		int	10h			; Video display   ah=functn 0Eh
						;  write char al, teletype mode
  
;��������������������������������������������������������������������������
;			       SUBROUTINE
;��������������������������������������������������������������������������
  
sub_2		proc	near
		lodsb				; String [si] to al
		cmp	al,0
		jne	loc_11			; Jump if not equal
		retn
sub_2		endp
  
  
;��������������������������������������������������������������������������
;			       SUBROUTINE
;��������������������������������������������������������������������������
  
sub_3		proc	near
		mov	ax,600h
		mov	bh,7
		xor	cx,cx			; Zero register
		mov	dx,184Fh
		int	10h			; Video display   ah=functn 06h
						;  scroll up, al=lines
		mov	ax,200h
		xor	dx,dx			; Zero register
		xor	bh,bh			; Zero register
		int	10h			; Video display   ah=functn 02h
						;  set cursor location in dx
		retn
sub_3		endp
  
  
;��������������������������������������������������������������������������
;
;			External Entry Point
;
;��������������������������������������������������������������������������
  
int_09h_entry	proc	far
		mov	cs:data_4,0FFh		; (9097:0107=0)
		jmp	dword ptr cs:data_2	; (9097:0103=0)
		db	1Dh, 7, 0DAh
		db	18 dup (0C4h)
		db	0BFh, 0, 0B3h, 20h, 0DBh
		db	14 dup (0DFh)
		db	0DBh, 20h, 0B3h, 0, 0B3h, 20h
		db	0DBh
		db	' Heads Parked '
		db	0DBh, 20h, 0B3h, 0, 0B3h, 20h
		db	0DBh
		db	20h, 20h, 6Fh, 6Eh, 20h
data_7		db	31h
		db	20h, 64h, 72h, 69h, 76h, 65h
data_8		db	2Eh
		db	20h, 0DBh, 20h, 0B3h, 0, 0B3h
		db	20h, 0DBh
		db	14 dup (0DCh)
		db	0DBh, 20h, 0B3h, 0, 0C0h
		db	18 dup (0C4h)
		db	0D9h, 0, 0, 0, 17h
		db	80 dup (0C4h)
		db	0
		db	'                        (Press A'
		db	'ny Key to Return to DOS)'
		db	0, 0
int_09h_entry	endp
  
  
seg_a		ends
  
  
  
		end	start
