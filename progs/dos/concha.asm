;name: beep
;verion: 1.00
;start  date: 28/10/93
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:

        mov dx,3cch             ;cambia la frecuencia del monitor
        in al,dx
        mov ah,al               ;guarda el valor de al para despues
        and al,11110011b        ;borra lo que halla
        and ah,00000100b
        xor ah,00000100b
        or al,ah
        mov dx,3c2h
        out dx,al

        mov ax,4c00h
        int 21h

ruti    endp
code    ends
        end ruti
