/*                               */
/* Soporte para ver archivos PCX */
/*                               */
/* por Ricardo Quesada.          */
/*                               */
/* version 0.01                  */

#include <stdio.h>
#include <stdlib.h>
#include <vga.h>
#include <vgagl.h>

FILE *file_ptr;
int  *int_ptr;
char pcxhead[128]; 
int  xx,yy;
char mipaleta[768];

int encget(int *pbyt, int *pcnt )
{
   int i;
   *pcnt = 1;
   if ( EOF == ( i=getc( file_ptr ))) 
     return ( EOF );
   if ( 0xc0 == (0xc0 & i )) 
     {
	*pcnt = 0x3f & i;
	if ( EOF == ( i = getc( file_ptr ))) 
	  return ( EOF );
     }
   *pbyt = i;
   return(0);
}

void pcx2buffer( void )
{
   int chr,cnt;
   int i,l,j=0;
   for(l=0; l < 100000 ; )
     {
	if ( EOF == encget( &chr, &cnt )) 
	  break;
	for ( i=0;i<cnt;i++) 
	  int_ptr[j++]= chr;
	l=+cnt;
     }
}

void viewpcx( void ) 
{
   int i=0,x=0,y=0,color;
   int j=0,k=0;
   
   fseek(file_ptr,-769,SEEK_END);   
   
   if( getc( file_ptr ) == 12 )  {
      fread( &mipaleta,1,768,file_ptr);
      for(k=0;k<768;k++) mipaleta[k]=mipaleta[k]>>2;
      gl_setpalette( mipaleta );
   }

   do  {
      vga_setcolor( int_ptr[i++] );
      vga_drawpixel( x,y );
      x++;
      if(x==xx) {
	 x=0;y++;
      }
   } while(y<yy);
   while ( vga_getkey()!='q');
}

void main ( int argc , char **argv )
{
   int i,files=0;
   if( argc < 2 )  {
      printf("ViewPCX v0.01 - Para ver archivos .pcx\n"
	     "syntax: viewpcx files... files... files...\n");
      exit( 1 );
   }
   argc--;
   vga_init();
   vga_setmode(G320x200x256);
   do {
      files++;
      if ( (file_ptr = (fopen( argv[files],"rb"))) == NULL )  {
	 printf("Unable to open file %s\n",argv[files]);
      }
      if ( (int_ptr = malloc( 100000 ) ) == NULL ) {
	 printf("Memoria insuficiente.\n");
      }
	    
      fread( &pcxhead,1,128,file_ptr);
   
      if( (pcxhead[0]== 10) && (pcxhead[1]==5 ) )  {
	 xx=++pcxhead[8] -pcxhead[4];
	 yy=++pcxhead[10] -pcxhead[6];
	 pcx2buffer(); 
	 viewpcx();
      }
      else  {
	 printf("Only .pcx version 5 (256 colors).\n");
      }
      free( int_ptr );	 
      fclose( file_ptr );
   } while( files < argc );
   vga_setmode( TEXT );
 }
	
