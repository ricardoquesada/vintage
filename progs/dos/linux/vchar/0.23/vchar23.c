/*************************************************/
/* VChar NI: (c) 1994,1996 by Ricardo Quesada    */
/* version: 0.21                                 */
/*************************************************/
/* Send bugs and comments to: rquesada@dc.uba.ar */
/*************************************************/
/* ??/9/95 - Investigar svgalib */
/* 22/8/96 - Segundo intento de hacer el VChar para Linux  */
/* 30/8/96 - 0.17 - Algo potable */
/* 31/8/96 - 0.21 - Algo mas potable */

#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/kd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <termio.h>
#include <sys/vt.h>
#include "vchar.h"
#include "keyboard.h"
#include "vcharfont.h"

#define VCHARVER "0.23"
#define MACROSIZE 200

#define MACROPLAY 2
#define MACROREC 1
#define MACROSTOP 0

/* definicion de funciones */
void init_vga(void);
void rest_vga(void);
void savedac(void);
void restdac(void);
void save2c(void);
void save2bin(void);

/* variables globales... */
static WORD srmap,srmem,grmap,grmod,grmis;

static FILE *fh;       /* usadas por openmem */
static BYTE* vid_addr;
static FILE *fh2;      /* usadas por openmem */
static BYTE* vid_addr2;

static BYTE ascii;
static int consolefd;
static int keymode;   /* estado del teclado */
static struct termio  consoleold,consolenew;
static struct vt_sizes newvtsizes;
BYTE posx,posy;
BYTE posx2,posy2;
BYTE salir;
BYTE comun;
BYTE mode;
BYTE macroplayrec;
BYTE macroindice;
WORD macrobuf[MACROSIZE];
BYTE dac[256*3];              /* DAC */

static struct caracter{
   BYTE car[2][32];        /* principal y para futuro uso*/
} caracter;

   
/* abre 0xa0000 */
int openmem_char( void ){
   fh=fopen("/dev/mem","r+");
   vid_addr= (BYTE*) mmap(
			   /* where to map, dont mind */
			   NULL,
			   /* how many bytes */
			   0x10000,
			   /* want to read and write */
			   PROT_READ | PROT_WRITE,
			   /* no copy on write */
			   MAP_SHARED,
			   /* handle to /dev/mem */
			   fileno(fh),
			   /* hopefully the Text-buffer :-) */
			   0xa0000);
   
   if(!vid_addr) return 1;
   return 0;
}
/* cierra mem 0xa0000 */
void closemem_char(void){
   munmap( (caddr_t) vid_addr,0x10000);
   fclose(fh);
}

int openmem_vid( void ){
   fh2=fopen("/dev/mem","r+");
   vid_addr2= (BYTE*) mmap(
			   /* where to map, dont mind */
			   NULL,
			   /* No se sabe, max 0x8000 */
			   0x1000,
			   /* want to read and write */
			   PROT_READ | PROT_WRITE,
			   /* no copy on write */
			   MAP_SHARED,
			   /* handle to /dev/mem */
			   fileno(fh2),
			   /* hopefully the Text-buffer :-) */
			   0xb8000 );
   
   if(!vid_addr2) return 1;
   return 0;
}
void closemem_vid(void){
   munmap( (caddr_t) vid_addr2,0x1000);
   fclose(fh);
}


void setxy(BYTE x,BYTE y){
   WORD xy=y*80+x;
   outb(0x3d4,0x0f); /* low */
   outb(0x3d5,(BYTE) xy & 255 );
   outb(0x3d4,0x0e); /* high */
   outb(0x3d5,(BYTE) ((xy >> 8)&255) );
}

void getxy(void){
   BYTE alto,bajo;
   WORD pos;

   outb(0x3d4,0x0f); /* low */
   bajo=inb(0x3d5);
   outb(0x3d4,0x0e); /* high */
   alto=inb(0x3d5);
   pos=alto*256 + bajo;
   posx=pos%80;
   posy=pos/80;
}

void init_vid(void){
   /* Linux tiene su consola en b8000+StrAdrr . Tengo que averiguar */
   /* Donde esta para el acceso directo a la pantalla de video */
   /* Que loco */
   /* CRT controller */

   setxy(0,16);
}
   
void showactive(void){
   BYTE i;
   init_vga();
   for(i=0;i<32;i++) vid_addr[0x4000+i] = caracter.car[0][i];
   rest_vga();
}
void dirvgaxy(int x,int y,BYTE carac){
   vid_addr2[y*160+x*2]=carac;
}
void dirvgaxya(int x,int y,WORD carcol){
   BYTE alto,bajo;
   bajo=(BYTE) carcol & 255;
   alto=(BYTE) ((carcol >> 8) & 255);
   vid_addr2[y*160+x*2  ]=bajo;
   vid_addr2[y*160+x*2+1]=alto;
}

void view_ascii(BYTE asc){
   BYTE asc_temp1;
   asc_temp1=asc/100;
   asc=asc%100;
   dirvgaxy(5,16,asc_temp1|0x30);
   asc_temp1=asc/10;
   asc=asc%10;
   dirvgaxy(6,16,asc_temp1|0x30);
   dirvgaxy(7,16,asc|0x30);
}

/* muestra el char que esta en caracter.car[0 o 1] */
void viewfont(void){
   short x,y;
   BYTE car,cartemp;
   
   for(y=0;y<16;y++){
      cartemp=caracter.car[0][y];
      for(x=0;x<8;x++){
	 car=cartemp;
	 if( (cartemp&128)==0)
	   dirvgaxya(x,y,BigVacio);
	 else 
	   dirvgaxya(x,y,BigLleno);
	 cartemp=car<<1;
      }
   }
   vid_addr2[0xa00]=ascii;
   view_ascii(ascii);
   showactive();
}

/* obtiene los bits del font */
void getfont(BYTE font){
   BYTE i;
   for(i=0;i<32;i++){
      caracter.car[0][i]=vid_addr[32*font+i];
   }
}
void putfont(BYTE font){
   BYTE i;
   for(i=0;i<32;i++){
      vid_addr[32*font+i]=caracter.car[1][i]=caracter.car[0][i];
   }
}

/* modo ascii del teclado */
void init_key(void){
   ioctl(consolefd,KDGKBMODE,&keymode);
   ioctl(consolefd,KDSKBMODE,K_XLATE);
   /* rutina sacada de showkey.c by Eric S. Raymond <esr@snark.thyrsus.com>, 1 Nov 88 */
   (void) ioctl(0, TCGETA, &consoleold);
   (void) memcpy(&consolenew, &consoleold, sizeof(struct termio));
   consolenew.c_lflag &=~ (ICANON | ECHO | ISIG | IEXTEN );
   consolenew.c_iflag &=~ (ICRNL | INLCR |IGNCR  | IXON | IGNBRK | BRKINT | PARMRK );
   consolenew.c_oflag &=~ (OPOST);
   consolenew.c_cc[VMIN] =  1;
   consolenew.c_cc[VTIME] = 0;
   (void) ioctl(0, TCSETA, &consolenew);
}
void res_key(void){
   (void) ioctl(0, TCSETA, &consoleold);
   ioctl(consolefd,KDSKBMODE,keymode);
}
void init_ioctl(void){
   newvtsizes.v_rows=25;
   newvtsizes.v_cols=80;
   newvtsizes.v_scrollsize=0;
   ioctl(consolefd,VT_RESIZE,&newvtsizes);
}
void res_ioctl(void){
}

void init_screen(void)
{
   WORD z=0;
   int x,y;
   BYTE titulo[]={
 

        "        ���                       �  �  �        � �                            "
	"        � �                       �  �  �  �  �  � �  "VCHARVER"                      "
	"        ���                       �� Ricardo Quesada                             "
   };
   BYTE help[]={
	"                                   vchar -? for help                            "
	"                    F1: Char Editor                 @F1: Text Editor            "
	"                                                     F2: Ascii Table            "
	"                    F4: Save Char                   @F2: Clear Text Editor      "
	"                   F10: Quit                                                    "
	"                                                                                "
	"                    F5: On/Off                       F9: Auto Save Char (n/a)   "
	"                    F6: Up/Down                                                 "
	"                    F7: Left/Right                  F11:Rec Macro  @F11:Copy    "
	"                    F8: Clean                       F12:PlayMacro  @F12:Paste   "
	"                                                                                "
	"                                                                                "
	"                                                                                "
	"                                                                                "
	"     F3: font0000.8x16      Ctrl F3: font0000.h     Shift F3: font000.txt       "
   };
   BYTE anim[]={
        "������������� � ������������� � ������������� � ������������� � ������������� � "
   };
   BYTE mensaje[]={
        " * Clickea estas letras con el boton derecho e izquierdo. Nota las diferencias !"
	" * Ejecuta este programa con el RChar residente. Ver�s cambios en los botones.  "
	" * Si sos un depresivo lee VChar.txt                                            "
	"                                                         ...simplemente �����   "
	"                                                                        �����   "
   };
   BYTE copyri[]={
	"                                VChar para Linux                                "
   };
  
   savedac();          /* Change dac colors */

   /* clear screen */
   for(x=0;x<80;x++){
      for(y=0;y<18;y++)
	dirvgaxya(x,y,BorrarB);
   }
   for(x=0;x<80;x++){
	dirvgaxya(x,18,Borrar2);
   }
   for(x=0;x<80;x++){
      for(y=19;y<25;y++)
	dirvgaxya(x,y,Borrar);
   }
   for(x=0;x<80;x++){
      for(y=0;y<3;y++)
	dirvgaxya(x,y,Fondo);
   }
   for(x=0;x<80;x++){
	dirvgaxya(x,24,Fondo);
   }
   for(x=8;x<80;x++){
	dirvgaxya(x,13,MacroFo);
   }
   for(z=0;z<80*3;z++){
      vid_addr2[z*2]=titulo[z];
   }
   for(z=0;z<80*15;z++){
      vid_addr2[160*3+z*2]=help[z];
   }
   for(z=0;z<80*1;z++){
      vid_addr2[160*18+z*2]=anim[z];
   }
   for(z=0;z<80*5;z++){
      vid_addr2[160*19+z*2]=mensaje[z];
   }
   for(z=0;z<80*1;z++){
      vid_addr2[160*24+z*2]=copyri[z];
   }
   
   vid_addr2[0xa01]=0x07;
   dirvgaxya(9,1,SActive);          /* font que se ve en el momento */
}
void rest_screen(void){
   BYTE x,y;
   for(x=0;x<80;x++){
      for(y=0;y<25;y++)
	dirvgaxya(x,y,Limpio);
   }
   restdac();
}

/* setea la pantalla. En 0xa0000 estan los fonts */
void init_vga( void ){
   outb(0x3c4,2);           /* addr Sequencer Controller 2nd register */
   srmap=inb(0x3c5);
   outb(0x3c5,4);           /* select bitplane 2 */

   outb(0x3c4,4);           /* Seq Controller 4th register */
   srmem=inb(0x3c5);
   outb(0x3c5,6);           /* 256k & linear processing */

   outb(0x3ce,4);           /* address of Graphic Controller */
   grmap=inb(0x3cf);        /* Select Read Map reg */
   outb(0x3cf,2);           /* Bitplane 2 */
   
   outb(0x3ce,5);           /* Select Graphics mode Register */
   grmod=inb(0x3cf);
   outb(0x3cf,0);           /* Write mode 0, linear addresses, 16 colors */
   
   outb(0x3ce,6);           /* Select miscellaneous register */
   grmis=inb(0x3cf);
   outb(0x3cf,4);           /* Text, linear addressess 64k at 0xa000 */
}
/* Restaura la VGA como debe ser */
void rest_vga( void ){
   outb(0x3c4,2);           /* addr Sequencer Controller 2nd register */
   outb(0x3c5,srmap); 

   outb(0x3c4,4);           /* Seq Controller 4th register */
   outb(0x3c5,srmem);       /* 256k & linear processing */

   outb(0x3ce,4);           /* address of Graphic Controller */
   outb(0x3cf,grmap);       /* Bitplane 2 */
   
   outb(0x3ce,5);           /* Select Graphics mode Register */
   outb(0x3cf,grmod);       /* Write mode 0, linear addresses, 16 colors */
   
   outb(0x3ce,6);           /* Select miscellaneous register */
   outb(0x3cf,grmis);       /* Text, linear addressess 64k at 0xa000 */
}

/* pone los permisos necesarios para acceder a los ports */
int init_ports(void){
   return (ioperm(0x3b4, 0x3df - 0x3b4 + 1 , 1 ));
}
int init_console(void){
   return (consolefd=open("/dev/console",O_RDONLY));
}

void doublefont(void){
   WORD i,j;
   init_vga();
   for(j=0;j<256;j++){
      for(i=0;i<16;i++){
	 vid_addr[0x4000+i+j*32]=vcharfont[j*16+i];
      }
   }
   outb(0x3c4,3);
   outb(0x3c5, (inb(0x3c5) & 0xc0) | 1);
   outw(0x3c4,0x0403); /* select 1st and 2nd font */
   rest_vga();
}
void doubleno(void){
   outb(0x3c4,3);
   outb(0x3c5,(inb(0x3c5) & 0xc0));
}
void cinvert(void){
   BYTE i;
   for(i=0;i<32;i++){
      caracter.car[0][i]^=255;
   }
   viewfont();
}
void cupdown(void){
   BYTE i;
   for(i=0;i<8;i++){
      caracter.car[0][i]^=caracter.car[0][15-i];
      caracter.car[0][15-i]^=caracter.car[0][i];
      caracter.car[0][i]^=caracter.car[0][15-i];
   }
   viewfont();
}
void cleftright(void){
   BYTE i,j,k,l;
   for(i=0;i<16;i++){
      j=caracter.car[0][i];
      k=0;
      for(l=0;l<4;l++){
	 k |= ( (j & (  1<< (7-l) )) >> (7-l*2) );
	 k |= ( (j & (128>> (7-l) )) << (7-l*2) );
      }
      
      caracter.car[0][i]=k;
   }
   viewfont();
}
void scrollup(void){
   BYTE i,temp;
   temp=caracter.car[0][0];
   for(i=0;i<16;i++)
     caracter.car[0][i]=caracter.car[0][i+1];
   caracter.car[0][15]=temp;
   viewfont();
}
void scrolldown(void){
   BYTE i,temp;
   temp=caracter.car[0][15];
   for(i=15;i>0;i--)
     caracter.car[0][i]=caracter.car[0][i-1];
   caracter.car[0][0]=temp;
   viewfont();
}
void scrollright(void){
   BYTE i,temp;
   for(i=0;i<16;i++){
      temp=caracter.car[0][i] >> 1;
      caracter.car[0][i]=temp | ( (caracter.car[0][i]&1) << 7) ;
   }
   viewfont();
}
void scrollleft(void){
   BYTE i,temp;
   for(i=0;i<16;i++){
      temp=caracter.car[0][i] << 1;
      caracter.car[0][i]=temp | ( (caracter.car[0][i]&128) >> 7) ;
   }
   viewfont();
}
void cclean(void){
   BYTE i;
   for(i=0;i<16;i++)
     caracter.car[0][i]=0;
   viewfont();
}
void macroview(void){
   BYTE asc,z,asc_temp1;
   BYTE macrotit[]={
      "Recording macro...Press F11 to stop. Buffer size[   ]"
   };
   for(z=0;z<53;z++)
     vid_addr2[160*13+z*2+17*2]=macrotit[z];
   asc=MACROSIZE-macroindice;
   asc_temp1=asc/100;
   asc=asc%100;
   dirvgaxy(66,13,asc_temp1|0x30);
   asc_temp1=asc/10;
   asc=asc%10;
   dirvgaxy(67,13,asc_temp1|0x30);
   dirvgaxy(68,13,asc|0x30);
}
void macronoview(void){
   BYTE z;
   for(z=0;z<53;z++)
     vid_addr2[160*13+z*2+17*2]=0x20;
}
void macrorec(void){
   WORD i;
   switch(macroplayrec){
    case 0: /* start recording */
      macroplayrec=MACROREC;
      macroindice=0;
      for(i=0;i<MACROSIZE;i++)
	macrobuf[i]=0;
      macroview();
      break;
    case 1: /* stop recording */
      macrobuf[macroindice-1]=0;
      macroplayrec=MACROSTOP;
      macronoview();
      break;
   }
}
void macroplay(void){
   if(macroplayrec==MACROSTOP){
      if(macrobuf[0]!=0){
	 macroindice=0;
	 macroplayrec=MACROPLAY;
      }
   }
   else if(macroplayrec==MACROREC)
     macroindice--;
}
void asciitable(void){
   WORD z;
   for(z=0;z<256;z++)
     vid_addr2[160*19+z*2]=(BYTE)z;
}
void cleartextedi(void){
   WORD z;
   for(z=0;z<5*80;z++)
     vid_addr2[160*19+z*2]=0x20;
}
void copyc(void){
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[1][i]=caracter.car[0][i];
}
void pastec(void){
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[0][i]=caracter.car[1][i];
   viewfont();
}

/* Teclas que son comunes a todos los modos */
void keycomun(WORD doscode){
   switch(doscode){
    case KF1:
      if(mode==CARAC){
	 mode=EDIT;
	 setxy(posx,posy);
      }
      else if(mode==EDIT){
	 mode=CARAC;
	 setxy(0,16);
      }
      else if(mode==TEXTEDIT){
	 mode=EDIT;
	 setxy(posx,posy);
      }
      comun=1;break;
    case KCF1:
      if(mode==CARAC){
	 mode=TEXTEDIT;
	 setxy(posx2,posy2);
      }
      else if(mode==EDIT){
	 mode=TEXTEDIT;
	 setxy(posx2,posy2);
      }
      else if(mode==TEXTEDIT){
	 mode=CARAC;
	 setxy(0,16);
      }
      comun=1;break;
    case KF2:
      asciitable();
      comun=1;break;
    case KCF2:
      cleartextedi();
      comun=1;break;
    case KF4:
      init_vga();
      putfont(ascii);
      rest_vga();
      comun=1;break;
    case KF5:
      cinvert();
      comun=1;break;
    case KF6:
      cupdown();
      comun=1;break;
    case KF7:
      cleftright(); 
      comun=1;break;
    case KF8:
      cclean(); 
      comun=1;break;
    case KF10:
      salir=1;
      comun=1;break;
    case KF11:
      macrorec();
      comun=1;break;
    case KF12:
      macroplay();
      comun=1;break;
    case KCF11:
      copyc();
      comun=1;break;
    case KCF12:
      pastec();
      comun=1;break;
    case KINSERT:
      ascii--;
      init_vga();
      getfont(ascii);
      rest_vga();
      viewfont();
      comun=1;break;
    case KPGUP:
      ascii++;
      init_vga();
      getfont(ascii);
      rest_vga();
      viewfont();
      comun=1;break;
    case KHOME: 
    case KGUP:
      scrollup();
      comun=1;break;
    case KEND:
    case KGDOWN:
      scrolldown();
      comun=1;break;
    case KDELETE:
    case KGLEFT:
      scrollleft();
      comun=1;break;
    case KPGDOWN:
    case KGRIGHT:
      scrollright();
      comun=1;break;
    case KCF3:
      save2c();
      comun=1;break;
    case KF3:
      save2bin();
      comun=1;break;
   }
}

/* esta rutina la necesito para usar los mismos valores que en DOS */
void convertcode(BYTE *charbuf,WORD *doscode){
   WORD temp=0,ORI=0;
   int modifiers;
   
   modifiers=6; /* code for the ioctl */
   if (ioctl(0,TIOCLINUX,&modifiers)<0)
     modifiers=0;
   if (modifiers&1) ORI|=KSHIFT;
   if (modifiers&2) ORI|=KRALT;
   if (modifiers&4) ORI|=KCONTROL;
   if (modifiers&8) ORI|=KLALT;
   if (!modifiers)  ORI|=KNONE;

   switch(charbuf[0]){
    case 0x1b:
      switch(charbuf[3]){       /* teclas grises */
       case 0x7e:
	 switch(charbuf[2]){
	  case 0x35:
	    temp=KPGUP;
	    break;
	  case 0x36:
	    temp=KPGDOWN;
	    break;
	  case 0x31:
	    temp=KHOME;
	    break;
	  case 0x34:
	    temp=KEND;
	    break;
	  case 0x32:
	    temp=KINSERT;
	    break;
	  case 0x33:
	    temp=KDELETE;
	    break;
	 }
	 break;
      }
      switch(charbuf[2]){
	  case 0x41:
	    temp=KUP;
	    break;
	  case 0x42:
	    temp=KDOWN;
	    break;
	  case 0x43:
	    temp=KRIGHT;
	    break;
	  case 0x44:
	    temp=KLEFT;
	    break;
	  case 0x78:
	    temp=KGUP;
	    break;
	  case 0x72:
	    temp=KGDOWN;
	    break;
	  case 0x76:
	    temp=KGRIGHT;
	    break;
	  case 0x74:
	    temp=KGLEFT;
	    break;
       case 0x5b:
	 switch(charbuf[3]){
	  case 0x41:
	    temp=KF1;
	    break;
	  case 0x42:
	    temp=KF2;
	    break;
	     case 0x43:
	    temp=KF3;
	    break;
	  case 0x44:
	    temp=KF4;
	    break;
	  case 0x45:
	    temp=KF5;
	    break;
	 }
	 break;
       case 0x31:
	 switch(charbuf[3]){
	  case 0x37:
	    temp=KF6;
	    break;
	  case 0x38:
	    temp=KF7;
	    break;
	  case 0x39:
	    temp=KF8;
	    break;
	 }
	 break;
       case 0x32:
	 switch(charbuf[3]){
	  case 0x30:
	    temp=KF9;
	    break;
	  case 0x31:
	    temp=KF10;
	    break;
	  case 0x33:
	    temp=KF11;
	    break;
	  case 0x34:
	    temp=KF12;
	    break;
	 }
	 break;
      }
      break; 
   }
   temp=(temp==0) ? charbuf[0] & 0xff:temp;
   doscode[0]=( (temp&0x0f00)==0) ? temp : temp|ORI;
#ifdef DEBUG
   printf("doscode 0x%4x     ori 0x4%4x \n\r",doscode[0],ORI); 
#endif
}

void loop(void){
   BYTE charbuf[6];
   WORD doscode;
   
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();

   do{
      if(macroplayrec==MACROPLAY){
	 doscode=macrobuf[macroindice];
	 macroindice++;
	 if(macrobuf[macroindice]==0) {
	    macroplayrec=MACROSTOP;macronoview();}
	 if(macroindice>=MACROSIZE){
 	    macroplayrec=MACROSTOP;macronoview();}
	 
      }
      else if(read(0, &charbuf, sizeof(charbuf)) != 0){
	 convertcode(&charbuf[0],&doscode);
	 if(macroplayrec==MACROREC){
	    macrobuf[macroindice]=doscode;
	    macroview();
	    macroindice++;
	    if(macroindice>=MACROSIZE){
	       macroplayrec=MACROSTOP;macronoview();}
	 }
      }
      {
	 if(mode==CARAC){
	    keycomun(doscode);       /* teclas comunes para todos los modos */
	    if(comun==0){
	       switch(doscode){
		case KUP:
		  ascii++; break;
		case KDOWN:
		  ascii--; break;
		case KRIGHT:
		  ascii=ascii+10;
		  break;
		case KLEFT:
		  ascii=ascii-10;
		  break;
		default:
		  if( doscode<0x00ff)
		    ascii=(BYTE) doscode;
		  break;
	       }
	       init_vga();
	       getfont(ascii);
	       rest_vga();
	       viewfont();
	    }
	    else comun=0;
	 }
	 else if(mode==EDIT){
	    keycomun(doscode);       /* teclas comunes para todos los modos */
	    if(comun==0){
	       switch(doscode){
		case KUP:
		  posy--;
		  posy=(posy>15)?15:posy;
		  setxy(posx,posy); 
		  break;
		case KDOWN:
		  posy++;
		  posy=(posy>15)?0:posy;
		  setxy(posx,posy); 
		  break;
		case KRIGHT:
		  posx++;
		  posx=(posx>7)?0:posx;
		  setxy(posx,posy); 
		  break;
		case KLEFT:
		  posx--;
		  posx=(posx>7)?7:posx;
		  setxy(posx,posy); 
		  break;
		case KSPACE:
		  caracter.car[0][posy]^= 1 << (7-posx);
		  viewfont();
		  break;
		case K1:
		  caracter.car[0][posy]|= 1 << (7-posx);
		  viewfont();
		  break;
		case K2:
		  caracter.car[0][posy]&=255 -( 1 << (7-posx));
		  viewfont();
		  break;
		default:
		  break;
	       }
	    }
	    else comun=0;
	 }
	 else if(mode==TEXTEDIT){
	    keycomun(doscode);       /* teclas comunes para todos los modos */
	    if(comun==0){
	       switch(doscode){
		case KUP:
irarriba:
		  posy2--;
		  posy2=(posy2<19)?23:posy2;
		  setxy(posx2,posy2);
		  break;
		case KDOWN:
irabajo:
		  posy2++;
		  posy2=(posy2>23)?19:posy2;
		  setxy(posx2,posy2);
		  break;
		case KRIGHT:
irderecha:
		  posx2++;
		  if(posx2<=79)
		    setxy(posx2,posy2);
		  else{
		     posx2=0;
		     goto irabajo;
		  }
		  break;
		case KLEFT:
irizquierda:
		  posx2--;
		  if(posx2<=79)
		    setxy(posx2,posy2);
		  else{
		     posx2=79;
		     goto irarriba;
		  }
		  break;
		case KBACK:
		  if(posx2==0){
		     if(posy2==19)
		       dirvgaxy(79,23,0x20);
		     else 
		       dirvgaxy(79,posy2-1,0x20);
		  }
		  else 
		    dirvgaxy(posx2-1,posy2,0x20);
		  goto irizquierda;
		case KENTER:
		  posx2=0;
		  goto irabajo;
		default:
		  if( doscode<0x00ff)
		    dirvgaxy(posx2,posy2,(BYTE) doscode);
		  goto irderecha;
		  break;
	       }
	    }
	    else comun=0;
	 }
      }
      (void) fflush(stdout);
   } while(salir==0);
}



void savedac(void){
   WORD i=0;
   outb(0x3c6,0xff);        /*Pel Mask (255)*/
   outb(0x3c7,0);           /* Esto deberia esta en asm con  */
   for(i=0;i<256;i++){      /* rep insb */
      dac[i*3+0]=inb(0x3c9);
      dac[i*3+1]=inb(0x3c9);
      dac[i*3+2]=inb(0x3c9);
   }
   outb(0x3c8,2);
   outb(0x3c9,dac[15*3+0]);
   outb(0x3c9,dac[15*3+1]);
   outb(0x3c9,dac[15*3+2]);

   outb(0x3c8,4);
   outb(0x3c9,dac[0*3+0]);
   outb(0x3c9,dac[0*3+1]);
   outb(0x3c9,dac[0*3+2]);

   outb(0x3c8,14);
   outb(0x3c9,dac[7*3+0]);
   outb(0x3c9,dac[7*3+1]);
   outb(0x3c9,dac[7*3+2]);

   outb(0x3c8,8);
   outb(0x3c9,dac[0*3+0]);
   outb(0x3c9,dac[0*3+1]);
   outb(0x3c9,dac[0*3+2]);

   outb(0x3c8,9);
   outb(0x3c9,dac[3*3+0]);
   outb(0x3c9,dac[3*3+1]);
   outb(0x3c9,dac[3*3+2]);
}
void restdac(void){
   WORD i=0;
   outb(0x3c6,0xff);        /*Pel Mask (255)*/
   outb(0x3c8,0);           /* Esto deberia esta en asm con  */
   for(i=0;i<256;i++){      /* rep outb */
      outb(0x3c9,dac[i*3+0]);
      outb(0x3c9,dac[i*3+1]);
      outb(0x3c9,dac[i*3+2]);
   }
}

void save2c(void){
   WORD i,j;
   FILE *out;

   init_vga();
   umask(18);           /* 022 */
   out=fopen("font.h","w+");
   fprintf(out,"/* VChar NI para Linux */\n");
   fprintf(out,"#define FONTH  16\n");
   fprintf(out,"#define FONTW   8\n");
   fprintf(out,"#define FONTL 256\n");
   fprintf(out,"unsigned char FONTS[]={\n");
   for(i=0;i<256;i++){
      fprintf(out,"\"");
      for(j=0;j<16;j++){
	 fprintf(out,"\\x%X",vid_addr[i*32+j]);
      }
      fprintf(out,"\"\n");
   }
   fprintf(out,"};\n");
   rest_vga();
   fclose(out);
}
void save2bin(void){
   WORD i,fdbin;
   init_vga();
   umask(18);              /* 022 */
   fdbin=open("font.8x16",O_RDWR | O_CREAT | O_TRUNC,420);
   for(i=0;i<256;i++){
      write(fdbin,&vid_addr[i*32],16);
   }
   close(fdbin);
   rest_vga();
}


/*           * * * *                 main               * * * *             */
int main (int argc,char **argv){
   BYTE i;

   printf("\nVChar - por Ricardo Quesada (c) 1994-1996\n");
   if(init_ports()){
      printf("Error: No se puede tener acceso sobre los ports\n");
      return 1;
   }
   if(init_console()<0){
      printf("Error: No se puede abrir /dev/console\n");
      return 1;
   }

   ascii='a';
   salir=0;
   comun=0;
   macroplayrec=0;
   macroindice=0;
   posx=0;posy=0;
   posx2=0;posy2=19;
   
   for(i=0;i<MACROSIZE;i++)
     macrobuf[i]=0;
   mode=CARAC;

   init_ioctl();
   init_vid();       /* averiaguar donde se debe el StrAdd */
   if(openmem_vid()!=0) return 1;      /* reserva memoria de video */
   if(openmem_char()!=0) return 1;     /* reserva memoria para el juego de fonts */
   init_screen();
   init_key();                   /* key k_xlate mode */
   doublefont();                 /* dos fonts activos */
   
   loop();                       /* hace todo */
   
   doubleno();                   /* 1 font activo */
   res_key();                    /* vuelte al estado anterior del teclado */
   rest_screen();
   closemem_char();
   closemem_vid();
   res_ioctl();
   
   return 0;
}
