/* SVGAlib, Copyright 1993 Harm Hanemaayer */
/* VGAlib version 1.2 - (c) 1993 Tommy Frandsen */
/* partially copyrighted (C) 1993 by Hartmut Schirmer */

/* Modificado para adaptarlo al VChar por Ricardo Quesada */
/* Internal definitions. */

#ifndef _VCHAR_H
#define _VCHAR_H

/* --------------------- Macro definitions shared by library modules */

/* colores */
#define Fondo    0x3820
#define BigLleno 0x7fb2
#define BigVacio 0x7fd8
#define Borrar   0x7220
#define Borrar2  0x3420
#define BorrarB  0x0e20
#define SActive  0x7f00
#define MacroFo  0x8920
#define Limpio   0x0720

#define EDIT     0x00
#define CARAC    0x01
#define TEXTEDIT 0x02

#define NOSEQUE  0x8080
#define MOUSE_SI 0x8081
#define KEY_SI   0x8082

#define UPDATEF   0x01
#define NOUPDATEF 0x02
#define BotonOn   0x3f
#define BotonOff  0x7f

/* cantidad total de botones */
#define BOTONES  29
#define BOTLEN   14
#define MAXBOTLEN BOTLEN * 2 + 2
#define BOO      0
#define BUD      1
#define BID      2
#define BCL      3
#define BGR      4
#define BOR      5
#define B01      6
#define B02      7
#define B10      8
#define B11      9
#define B12      10
#define B13      11
#define B14      12
#define B15      13
#define B16      14
#define B17      15
#define BSA      16
#define BSB      17
#define BSH      18
#define BAU      19
#define BAD      20
#define BF1      21
#define BC1      22
#define BL1      23
#define BL2      24
#define BL3      25
#define BL4      26
#define BVC      27
#define BAN      28

/* macros de las macros */
#define MACROSIZE 200
#define MACROPLAY 2
#define MACROREC 1
#define MACROSTOP 0

/* type declarations */
typedef unsigned char BYTE;
typedef unsigned short int WORD;
typedef void fn_hand(void);

/* VGA index register ports */
#define CRT_IC  0x3D4		/* CRT Controller Index - color emulation */
#define CRT_IM  0x3B4		/* CRT Controller Index - mono emulation */
#define ATT_IW  0x3C0		/* Attribute Controller Index & Data Write Register */
#define GRA_I   0x3CE		/* Graphics Controller Index */
#define SEQ_I   0x3C4		/* Sequencer Index */
#define PEL_IW  0x3C8		/* PEL Write Index */
#define PEL_IR  0x3C7		/* PEL Read Index */

/* VGA data register ports */
#define CRT_DC  0x3D5		/* CRT Controller Data Register - color emulation */
#define CRT_DM  0x3B5		/* CRT Controller Data Register - mono emulation */
#define ATT_R   0x3C1		/* Attribute Controller Data Read Register */
#define GRA_D   0x3CF		/* Graphics Controller Data Register */
#define SEQ_D   0x3C5		/* Sequencer Data Register */
#define MIS_R   0x3CC		/* Misc Output Read Register */
#define MIS_W   0x3C2		/* Misc Output Write Register */
#define IS1_RC  0x3DA		/* Input Status Register 1 - color emulation */
#define IS1_RM  0x3BA		/* Input Status Register 1 - mono emulation */
#define PEL_D   0x3C9		/* PEL Data Register */
#define PEL_MSK 0x3C6		/* PEL mask register */


/* EGA-specific registers */

#define GRA_E0	0x3CC		/* Graphics enable processor 0 */
#define GRA_E1	0x3CA		/* Graphics enable processor 1 */

/* standard VGA indexes max counts */
#define CRT_C   24		/* 24 CRT Controller Registers */
#define ATT_C   21		/* 21 Attribute Controller Registers */
#define GRA_C   9		/* 9  Graphics Controller Registers */
#define SEQ_C   5		/* 5  Sequencer Registers */
#define MIS_C   1		/* 1  Misc Output Register */

/* VGA registers saving indexes */
#define CRT     0		/* CRT Controller Registers start */
#define ATT     CRT+CRT_C	/* Attribute Controller Registers start */
#define GRA     ATT+ATT_C	/* Graphics Controller Registers start */
#define SEQ     GRA+GRA_C	/* Sequencer Registers */
#define MIS     SEQ+SEQ_C	/* General Registers */
#define EXT     MIS+MIS_C	/* SVGA Extended Registers */


#define GRAPH_BASE 0xA0000
#define FONT_BASE  0xA0000

#define GRAPH_SIZE 0x10000
#define FONT_SIZE  0x2000
#define GPLANE16   G640x350x16

static inline void port_out(int value, int port)
{
    __asm__ volatile ("outb %0,%1"
	      ::"a" ((unsigned char) value), "d"((unsigned short) port));
}

static inline void port_outw(int value, int port)
{
    __asm__ volatile ("outw %0,%1"
	     ::"a" ((unsigned short) value), "d"((unsigned short) port));
}

static inline int port_in(int port)
{
    unsigned char value;
    __asm__ volatile ("inb %1,%0"
		      :"=a" (value)
		      :"d"((unsigned short) port));
    return value;
}

static inline int port_inw(int port)
{
    unsigned short value;
    __asm__ volatile ("inw %1,%0"
		      :"=a" (value)
		      :"d"((unsigned short) port));
    return value;
}

#define gr_readb(off)		(GM[(off)])
#define gr_readw(off)		(*(unsigned short*)((GM)+(off)))
#define gr_readl(off)		(*(unsigned long*)((GM)+(off)))
#define gr_writeb(v,off)	(GM[(off)] = (v))
#define gr_writew(v,off)	(*(unsigned short*)((GM)+(off)) = (v))
#define gr_writel(v,off)	(*(unsigned long*)((GM)+(off)) = (v))


/* Note that the arguments of outb/w are reversed compared with the */
/* kernel sources. The XFree86 drivers also use this format. */
#define inb port_in
#define inw port_inw
#define outb(port, value) port_out(value, port)
#define outw(port, value) port_outw(value, port)

#endif
