
/*************************************/
/* Tabla de definicion de las teclas */
/*************************************/

#define META	0x80
#define ESC	0x1b

#define KUNDEF      0xffff
#define KESC        0x0100
#define KF1         0x0101
#define KF2         0x0102
#define KF3         0x0103
#define KF4         0x0104
#define KF5         0x0105
#define KF6         0x0106
#define KF7         0x0107
#define KF8         0x0108
#define KF9         0x0109
#define KF10        0x0110
#define KF11        0x0111
#define KF12        0x0112
#define KLEFT       0x0122
#define KRIGHT      0x0123
#define KUP         0x0124
#define KDOWN       0x0125
#define KGLEFT      0x0126
#define KGRIGHT     0x0127
#define KGUP        0x0128
#define KGDOWN      0x0129
#define KPGUP       0x0130
#define KPGDOWN     0x0131
#define KHOME       0x0132
#define KEND        0x0133
#define KINSERT     0x0134
#define KDELETE     0x0135

#define KCF1        0x4101
#define KCF2        0x4102
#define KCF3        0x4103
#define KCF4        0x4104
#define KCF5        0x4105
#define KCF6        0x4106
#define KCF7        0x4107
#define KCF8        0x4108
#define KCF9        0x4109
#define KCF10       0x4110
#define KCF11       0x4111
#define KCF12       0X4112

#define KSPACE      0x0020
#define K1          0x0031
#define K2          0x0032
#define KBACK       0x007f
#define KENTER      0x000d

/* modifiers */
#define KSHIFT      0x1000
#define KRALT       0x2000
#define KCONTROL    0x4000
#define KLALT       0x8000
#define KNONE       0x0000
