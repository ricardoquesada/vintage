/************************************************
 *                                              *
 *                   V C h a r                  *
 *                      for                     *
 *                     Linux                    *
 *                                              *
 *             Copyright 1994, 1995             *
 *                Ricardo Quesada               *
 *            Buenos Aires, Argentina           *
 *                                              *
 ************************************************/


/************************************************
 *                                              *
 *                 includes                     *
 *                                              *
 ************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libvga.h"


/************************************************
 *                                              *
 *           defines and typedefs               *
 *                                              *
 ************************************************/
typedef unsigned char BYTE;

/************************************************
 *                                              *
 *           variables globales                 *
 *                                              *
 ************************************************/
BYTE srmap;                  /* Sequencer register: map mask value */
BYTE srmem;                  /*                   : memory mode    */
BYTE grmap;                  /* Graphic controller: read map select*/
BYTE grmod;                  /*                   : graphics mode  */
BYTE grmis;                  /*                   : miscellaneous  */

char *font_pnt;

char letraA[16]= {
   255,129,129,129,129,129,129,129,129,129,129,129,129,129,129,255 
};

int __svgalib_mem_fd = -1;  /* /dev/mem file descriptor  */
int __svgalib_tty_fd = -1;	/* /dev/tty file descriptor */
int __svgalib_console_fd = -1;	/* /dev/console file descriptor */

/* Dummy buffer for mmapping grahics memory; points to 64K VGA framebuffer. */
unsigned char *__svgalib_graph_mem;
/* Exported variable (read-only) is shadowed from internal variable, for */
/* better shared library performance. */
unsigned char *graph_mem;

/************************************************
 *                                              *
 *        This functions were taken from        *
 *            vga.c of svgalib v1.2.4           *
 *                                              *
 ************************************************/
/* open /dev/mem */
static void open_mem(void) {
    if (__svgalib_mem_fd < 0) 
	if ((__svgalib_mem_fd = open("/dev/mem", O_RDWR) ) < 0) {
	    printf("svgalib: Cannot open /dev/mem.\n");
	    exit (-1);
	}
}
static void open_devconsole(void) {
	if (__svgalib_console_fd != -1)
		return;
	if ((__svgalib_console_fd = open("/dev/console", O_RDONLY) ) < 0) {
	    printf("svgalib: Cannot open /dev/console.\n");
	    exit (-1);
	}
}
void __vga_get_perm(void)
{
	static int done = 0;

	/* Only do this once. */
	if (done)
        	return;
	done = 1;

	/* Get I/O permissions for VGA registers. */
	/* If IOPERM is set, assume permissions have already been obtained */
	/* by a calling (exec-ing) process, e.g. ioperm(1). */

	if (getenv("IOPERM") == NULL)
	        if (ioperm(0x3b4, 0x3df - 0x3b4 + 1, 1)) {
			printf("svgalib: Cannot get I/O permissions.\n");
			exit(-1);
		}

	/* Open /dev/mem (also needs supervisor rights; ioperm(1) can be */
	/* used together with a special group that has r/w access on */
	/* /dev/mem to facilitate this). */
	open_mem();
 
 	open_devconsole();
 
    /* color or monochrome text emulation? */
    if (CHIPSET != EGA)
	color_text = port_in(MIS_R)&0x01;
    else
    	color_text = 1;	/* EGA is assumed color */

    /* chose registers for color/monochrome emulation */
    if (color_text) {
	CRT_I = CRT_IC;
	CRT_D = CRT_DC;
	IS1_R = IS1_RC;
    } else {
	CRT_I = CRT_IM;
	CRT_D = CRT_DM;
	IS1_R = IS1_RM;
    }
}

static void __vga_mmap()
{
    #if 1
    /* This may waste 64K; mmap can probably do better by now. */
    /* valloc allocates aligned on page boundary */
    if ((__svgalib_graph_mem = valloc(GRAPH_SIZE)) == NULL) {
	printf("svgalib: allocation error \n");
	exit (-1);
    }
    __svgalib_graph_mem = (unsigned char *)mmap(
	(caddr_t)GM,
	GRAPH_SIZE,
	PROT_READ|PROT_WRITE,
	MAP_SHARED|MAP_FIXED,
	__svgalib_mem_fd,
	GRAPH_BASE
    );
    #endif
    #if 0
    /* This assumes pl10+. */
    /* Still seems to waste 64K, so don't bother. */
    GM = (unsigned char *)mmap(
	(caddr_t) 0,
	GRAPH_SIZE,
	PROT_READ|PROT_WRITE,
	MAP_SHARED,
	__svgalib_mem_fd,
	GRAPH_BASE
    );
    #endif
    graph_mem = __svgalib_graph_mem;	/* Exported variable. */
}


/*****************************************
 *  init2bitplane                        *
 * This function puts the font table in  *
 * A0000                                 *
 *****************************************/
void init2bitplane( void ) {
   outb( SEQ_I,2 );
   srmap = inb( SEQ_D );
   outb( SEQ_D,4 );
   
   outb( SEQ_I,4 );
   srmem = inb( SEQ_D );
   outb( SEQ_D,7 );
   
   outb( GRA_I, 4 );
   grmap = inb( GRA_I );
   outb( GRA_D, 2 );
   
   outb( GRA_I, 5 );
   grmod = inb( GRA_D );
   outb( GRA_D,0 );
   
   outb( GRA_I, 6 );
   grmis = inb( GRA_D );
   outb( GRA_D,4 );
}

/**************************************
 * rest2bitplane                      *
 * This funcion restore the address of*
 * the table                          *
 **************************************/
void rest2bitplane( void )  {
   outb( SEQ_I,2 );
   outb( SEQ_D,srmap );
   
   outb( SEQ_I,4 );
   outb( SEQ_D,srmem );
   
   outb( GRA_I,4 );
   outb( GRA_D,grmap );
   
   outb( GRA_I,5 );
   outb( GRA_D,grmod );
   
   outb( GRA_I,6 );
   outb( GRA_D,grmis );
}

void putlettera( void )  {
   init2bitplane();
   memcpy( font_pnt, letraA , 16 );
   rest2bitplane();
}

void main( void ) {
   putlettera();
}
