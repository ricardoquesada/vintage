/* SVGAlib, Copyright 1993 Harm Hanemaayer */
/* VGAlib version 1.2 - (c) 1993 Tommy Frandsen */
/* partially copyrighted (C) 1993 by Hartmut Schirmer */

/* Internal definitions. */

#ifndef _LIBVGA_H
#define _LIBVGA_H

/* --------------------- Macro definitions shared by library modules */

/* VGA index register ports */
#define CRT_IC  0x3D4   /* CRT Controller Index - color emulation */
#define CRT_IM  0x3B4   /* CRT Controller Index - mono emulation */
#define ATT_IW  0x3C0   /* Attribute Controller Index & Data Write Register */
#define GRA_I   0x3CE   /* Graphics Controller Index */
#define SEQ_I   0x3C4   /* Sequencer Index */
#define PEL_IW  0x3C8   /* PEL Write Index */
#define PEL_IR  0x3C7   /* PEL Read Index */

/* VGA data register ports */
#define CRT_DC  0x3D5   /* CRT Controller Data Register - color emulation */
#define CRT_DM  0x3B5   /* CRT Controller Data Register - mono emulation */
#define ATT_R   0x3C1   /* Attribute Controller Data Read Register */
#define GRA_D   0x3CF   /* Graphics Controller Data Register */
#define SEQ_D   0x3C5   /* Sequencer Data Register */
#define MIS_R   0x3CC   /* Misc Output Read Register */
#define MIS_W   0x3C2   /* Misc Output Write Register */
#define IS1_RC  0x3DA   /* Input Status Register 1 - color emulation */
#define IS1_RM  0x3BA   /* Input Status Register 1 - mono emulation */
#define PEL_D   0x3C9   /* PEL Data Register */
#define PEL_MSK 0x3C6	/* PEL mask register */

/* 8514/MACH regs we need outside of the mach32 driver.. */
#define PEL8514_D	0x2ED
#define PEL8514_IW	0x2EC
#define PEL8514_IR	0x2EB
#define PEL8514_MSK	0x2EA

/* EGA-specific registers */

#define GRA_E0	0x3CC	/* Graphics enable processor 0 */
#define GRA_E1	0x3CA	/* Graphics enable processor 1 */

/* standard VGA indexes max counts */
#define CRT_C   24      /* 24 CRT Controller Registers */
#define ATT_C   21      /* 21 Attribute Controller Registers */
#define GRA_C   9       /* 9  Graphics Controller Registers */
#define SEQ_C   5       /* 5  Sequencer Registers */
#define MIS_C   1       /* 1  Misc Output Register */

/* VGA registers saving indexes */
#define CRT     0               /* CRT Controller Registers start */
#define ATT     CRT+CRT_C       /* Attribute Controller Registers start */
#define GRA     ATT+ATT_C       /* Graphics Controller Registers start */
#define SEQ     GRA+GRA_C       /* Sequencer Registers */
#define MIS     SEQ+SEQ_C       /* General Registers */
#define EXT     MIS+MIS_C       /* SVGA Extended Registers */

/* Shorthands for chipset (driver) specific calls */
#define chipset_saveregs driverspecs->saveregs
#define chipset_setregs driverspecs->setregs
#define chipset_unlock driverspecs->unlock
#define chipset_test driverspecs->test
#define chipset_setpage driverspecs->setpage
#define chipset_setmode driverspecs->setmode
#define chipset_modeavailable driverspecs->modeavailable
#define chipset_getmodeinfo driverspecs->getmodeinfo

/* Shorthands for internal variables and functions */
#define CI	__svgalib_cur_info
#define GM	__svgalib_graph_mem
#define CM	__svgalib_cur_mode
#define VMEM	__svgalib_videomemoryused
#define DREP	__svgalib_driver_report
#define CRITICAL __svgalib_critical
#define COL	__svgalib_cur_color
#define CHIPSET __svgalib_chipset
#define SCREENON __svgalib_screenon
#define MODEX 	__svgalib_modeX
#define MODEFLAGS __svgalib_modeflags
#define infotable __svgalib_infotable

#define SVGADRV		 2
#define STDVGADRV	 1
#define STDVGAMODE(mode) (chipset_modeavailable(mode) == STDVGADRV)
#define SVGAMODE(mode)   (chipset_modeavailable(mode) == SVGADRV)

#define GRAPH_BASE 0xA0000
#define GRAPH_SIZE 0x10000
#define FONT_BASE  0xA0000
#define FONT_SIZE  0x2000
#define GPLANE16   G640x350x16

/* graphics mode information */
struct info {
    int xdim;
    int ydim;
    int colors;
    int xbytes;
    int bytesperpixel;
};

/* --------------------- Variable definitions shared by library modules */

extern int CRT_I;			/* current CRT index register address */
extern int CRT_D;			/* current CRT data register address */
extern int IS1_R;			/* current input status register address */
extern struct info CI;			/* current video parameters */
extern int         COL;			/* current color            */
extern int         CM;			/* current video mode       */
extern struct info infotable[];
extern int SCREENON;			/* screen visible if != 0 */
extern unsigned char *GM;		/* graphics memory frame */
extern int MODEX;			/* TRUE after vga_setmodeX() */
extern int MODEFLAGS;			/* copy of flags of current modeinfo->flags */

extern int __svgalib_mem_fd;
extern int __svgalib_tty_fd;
extern int __svgalib_console_fd;

/* --------------------- Function definitions shared by library modules */

extern int __vga_setregs(const unsigned char *regs);
extern int __vga_saveregs(unsigned char *regs);
extern void __vga_get_perm(void);
extern int __vga_getchipset(void); 
extern int __vga_name2number(char *modename);
extern void __vga_delay();
extern int __vga_addmode(int xdim, int ydim, int cols, int xbytes, int bytespp);
extern int __svgalib_physmem();
extern void __svgalib_waitvtactive();
extern void __svgalib_flipaway();

static inline void port_out( int value, int port )
{
	__asm__ volatile ("outb %0,%1"
	: : "a" ((unsigned char)value), "d" ((unsigned short)port));
}

static inline void port_outw( int value, int port ) {
	__asm__ volatile("outw %0,%1"
	: : "a" ((unsigned short)value), "d" ((unsigned short)port));
}

static inline int port_in( int port )
{
	unsigned char value;
	__asm__ volatile ("inb %1,%0"
		: "=a" (value)
		: "d" ((unsigned short)port));
	return value;
}

static inline int port_inw( int port )
{
	unsigned short value;
	__asm__ volatile ("inw %1,%0"
		: "=a" (value)
		: "d" ((unsigned short)port));
	return value;
}

/* Note that the arguments of outb/w are reversed compared with the */
/* kernel sources. The XFree86 drivers also use this format. */
#define inb port_in
#define inw port_inw
#define outb(port, value) port_out(value, port)
#define outw(port, value) port_outw(value, port)

#endif
