/*************************************************/
/* VChar NI: (c) 1994,1996 by Ricardo Quesada    */
/* version: 0.21                                 */
/*************************************************/
/* Send bugs and comments to: rquesada@dc.uba.ar */
/*************************************************/
/* ??/9/95 - Investigar svgalib - No sabia como empezar. */
/* 22/8/96 - Segundo intento de hacer el VChar para Linux.Despues de casi 1 ano */
/* 30/8/96 - 0.17 - Algo potable */
/* 31/8/96 - 0.21 - Algo mas potable */
/* 31/8/96 - 0.24 - Empezar con el mouse */
/*  2/9/96 - 0.26 - El mouse ya es una realidad. Las mil una me pasaron */
/*  2/9/96 - 0.27 - Se empezo con la version LI (Linux Interface ) */
/*  2/9/96 - 0.29 - Bastante aceptable */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <termio.h>
#include <curses.h>
#include <string.h>
#include <gpm.h>                 /* mouse support. New on v0.24 */
#include <sys/vt.h>
#include <sys/kd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include "vchar.h"
#include "keyboard.h"
#include "vcharfont.h"

#define VCHARVER "0.30"
#define MACROSIZE 200

#define MACROPLAY 2
#define MACROREC 1
#define MACROSTOP 0
#define SELECT_TIME 600

/* definicion de funciones */
void init_vga(void);
void rest_vga(void);
void savedac(void);
void restdac(void);
void save2c(void);
void save2bin(void);
void viewfont(void);
BYTE getvgaxy(short x,short y);
void getfont(BYTE i);
void cinvert(void);
void cleftright(void);
void cupdown(void);
void cclean(void);
void nada(void);
void csavechar(void);
void casciiup(void);
void casciidown(void);
void cleartextedi(void);
void asciitable(void);
void copyc(void);
void pastec(void);
void csalir(void);
void xautosave(void);
void cff1(void);
void cfcf1(void);
void macrorec(void);
void macroplay(void);
void scrollup(void);
void scrolldown(void);
void scrollleft(void);
void scrollright(void);

/* variables globales... */
static WORD srmap,srmem,grmap,grmod,grmis;

static FILE *fh;       /* usadas por openmem */
static BYTE* vid_addr;
static FILE *fh2;      /* usadas por openmem */
static BYTE* vid_addr2;

static BYTE ascii;
static int consolefd;
static int keymode;   /* estado del teclado */
static struct termio  consoleold,consolenew;
static struct vt_sizes newvtsizes;
BYTE posx,posy;
BYTE posx2,posy2;
BYTE salir;
BYTE comun;
BYTE mode;
BYTE macroplayrec;
BYTE macroindice;
WORD macrobuf[MACROSIZE];
BYTE dac[256*3];              /* DAC */
BYTE charbuf[6];
BYTE updatef;
BYTE autosavec;

Gpm_Connect conn;             /* del mouse */
int opt_pointer;              /* del mouse */
BYTE minternal;               /* internal del mouse handler */

static struct caracter
{
   BYTE car[2][32];        /* principal y para futuro uso*/
} caracter;
static struct boton
{
   char nombre[2][MAXBOTLEN];
   BYTE x,y;
   BYTE activo;          /* esta prendido */
   BYTE n;               /* que mensaje mostrar */
   fn_hand *fn;          /* funcion asocioda */
   BYTE l;               /* logitud del boton */
   WORD k;               /* key asociada */
   WORD k2;              /* otra key asociada */
} boton[BOTONES];

/**************************************************** Funciones generales */
/* abre 0xa0000 */
int openmem_char( void ){
   fh=fopen("/dev/mem","r+");
   vid_addr= (BYTE*) mmap(
			   /* where to map, dont mind */
			   NULL,
			   /* how many bytes */
			   0x10000,
			   /* want to read and write */
			   PROT_READ | PROT_WRITE,
			   /* no copy on write */
			   MAP_SHARED,
			   /* handle to /dev/mem */
			   fileno(fh),
			   /* hopefully the Text-buffer :-) */
			   0xa0000);
   
   if(!vid_addr) return 1;
   return 0;
}
/* cierra mem 0xa0000 */
void closemem_char(void){
   munmap( (caddr_t) vid_addr,0x10000);
   fclose(fh);
}

int openmem_vid( void ){
   fh2=fopen("/dev/mem","r+");
   vid_addr2= (BYTE*) mmap(
			   /* where to map, dont mind */
			   NULL,
			   /* No se sabe, max 0x8000 */
			   0x1000,
			   /* want to read and write */
			   PROT_READ | PROT_WRITE,
			   /* no copy on write */
			   MAP_SHARED,
			   /* handle to /dev/mem */
			   fileno(fh2),
			   /* hopefully the Text-buffer :-) */
			   0xb8000 );
   
   if(!vid_addr2) return 1;
   return 0;
}
void closemem_vid(void){
   munmap( (caddr_t) vid_addr2,0x1000);
   fclose(fh);
}

/******************************************* Funciones relacionadas con VGA */
void setxy(BYTE x,BYTE y){
   WORD xy=y*80+x;
   outb(0x3d4,0x0f); /* low */
   outb(0x3d5,(BYTE) xy & 255 );
   outb(0x3d4,0x0e); /* high */
   outb(0x3d5,(BYTE) ((xy >> 8)&255) );
}

void getxy(void){
   BYTE alto,bajo;
   WORD pos;

   outb(0x3d4,0x0f); /* low */
   bajo=inb(0x3d5);
   outb(0x3d4,0x0e); /* high */
   alto=inb(0x3d5);
   pos=alto*256 + bajo;
   posx=pos%80;
   posy=pos/80;
}

void init_vid(void){
   /* Linux tiene su consola en b8000+StrAdrr . Tengo que averiguar */
   /* Donde esta para el acceso directo a la pantalla de video */
   /* Que loco */
   /* CRT controller */

   setxy(0,16);
}
   
void showactive(void){
   BYTE i;
   init_vga();
   for(i=0;i<32;i++) vid_addr[0x4000+i] = caracter.car[0][i];
   rest_vga();
}
BYTE getvgaxy(short x,short y){
   return vid_addr2[y*160+x*2];
}
void dirvgaxy(int x,int y,BYTE carac){
   vid_addr2[y*160+x*2]=carac;
}
void dirvgaxya(int x,int y,WORD carcol){
   BYTE alto,bajo;
   bajo=(BYTE) carcol & 255;
   alto=(BYTE) ((carcol >> 8) & 255);
   vid_addr2[y*160+x*2  ]=bajo;
   vid_addr2[y*160+x*2+1]=alto;
}

/*********************************************** LI - Linux Interface */
void init_but(void)
{
   strcpy(boton[BOO].nombre[0],"F5:On/Off     ");
   strcpy(boton[BOO].nombre[1],"F5:Off/On     ");
   boton[BOO].x=11;
   boton[BOO].y=5;
   boton[BOO].activo=0;
   boton[BOO].n=0;
   boton[BOO].fn=cinvert;
   boton[BOO].l=BOTLEN;
   boton[BOO].k=KF5;
   boton[BOO].k2=KUNDEF;
   strcpy(boton[BUD].nombre[0],"F6:Up/Down    ");
   strcpy(boton[BUD].nombre[1],"F6:Down/Up    ");
   boton[BUD].x=11;
   boton[BUD].y=7;
   boton[BUD].activo=0;
   boton[BUD].n=0;
   boton[BUD].fn=cupdown;
   boton[BUD].l=BOTLEN;
   boton[BUD].k=KF6;
   boton[BUD].k2=KUNDEF;
   strcpy(boton[BID].nombre[0],"F7:Left/Right ");
   strcpy(boton[BID].nombre[1],"F7:Right/Left ");
   boton[BID].x=11 ;
   boton[BID].y=9;
   boton[BID].activo=0;
   boton[BID].n=0;
   boton[BID].fn=cleftright;
   boton[BID].l=BOTLEN;
   boton[BID].k=KF7;
   boton[BID].k2=KUNDEF;
   strcpy(boton[BCL].nombre[0],"F8:Clear      ");
   strcpy(boton[BCL].nombre[1],"F8:Clear      ");
   boton[BCL].x=11;
   boton[BCL].y=11;
   boton[BCL].activo=0;
   boton[BCL].n=0;
   boton[BCL].fn=cclean;
   boton[BCL].l=BOTLEN;
   boton[BCL].k=KF8;
   boton[BCL].k2=KUNDEF;
   strcpy(boton[BGR].nombre[0],"@F5:Orig Char ");
   strcpy(boton[BGR].nombre[1],"@F5:Orig Char ");
   boton[BGR].x=11 + BOTLEN + 2;
   boton[BGR].y=5;
   boton[BGR].activo=0;
   boton[BGR].n=0;
   boton[BGR].fn=nada;
   boton[BGR].l=BOTLEN;
   boton[BGR].k=KCF5;
   boton[BGR].k2=KUNDEF;
   strcpy(boton[BOR].nombre[0],"@F6:Rotate 90 ");
   strcpy(boton[BOR].nombre[1],"@F6:Rotate 90 ");
   boton[BOR].x=11 + BOTLEN + 2;
   boton[BOR].y=7;
   boton[BOR].activo=0;
   boton[BOR].n=0;
   boton[BOR].fn=nada;
   boton[BOR].l=BOTLEN;
   boton[BOR].k=KCF6;
   boton[BOR].k2=KUNDEF;
   strcpy(boton[B01].nombre[0],"@F7:Copy      ");
   strcpy(boton[B01].nombre[1],"@F7:Copy      ");
   boton[B01].x=11 + BOTLEN + 2;
   boton[B01].y=9;
   boton[B01].activo=0;
   boton[B01].n=0;
   boton[B01].fn=copyc;
   boton[B01].l=BOTLEN;
   boton[B01].k=KCF7;
   boton[B01].k2=KUNDEF;
   strcpy(boton[B02].nombre[0],"@F8:Paste     ");
   strcpy(boton[B02].nombre[1],"@F8:Paste     ");
   boton[B02].x=11 + BOTLEN + 2;
   boton[B02].y=11;
   boton[B02].activo=0;
   boton[B02].n=0;
   boton[B02].fn=pastec;
   boton[B02].l=BOTLEN;
   boton[B02].k=KCF8;
   boton[B02].k2=KUNDEF;
   strcpy(boton[BSA].nombre[0],"F4:        Save Char           ");
   strcpy(boton[BSA].nombre[1],"F4:        Save Char           ");
   boton[BSA].x=11;
   boton[BSA].y=13;
   boton[BSA].activo=0;
   boton[BSA].n=0;
   boton[BSA].fn=csavechar;
   boton[BSA].l=BOTLEN * 2 + 2;
   boton[BSA].k=KF4;
   boton[BSA].k2=KUNDEF;
   strcpy(boton[BAU].nombre[0]," \x18 ");
   strcpy(boton[BAU].nombre[1]," \x18 ");
   boton[BAU].x=11+3;
   boton[BAU].y=15;
   boton[BAU].activo=0;
   boton[BAU].n=0;
   boton[BAU].fn=casciiup;
   boton[BAU].l=3;
   boton[BAU].k=KINSERT;
   boton[BAU].k2=KUNDEF;
   strcpy(boton[BAD].nombre[0]," \x19 ");
   strcpy(boton[BAD].nombre[1]," \x19 ");
   boton[BAD].x=11+3;
   boton[BAD].y=17;
   boton[BAD].activo=0;
   boton[BAD].n=0;
   boton[BAD].fn=casciidown;
   boton[BAD].l=3;
   boton[BAD].k=KPGUP;
   boton[BAD].k2=KUNDEF;
   strcpy(boton[BL1].nombre[0]," \x18 ");
   strcpy(boton[BL1].nombre[1]," \x18 ");
   boton[BL1].x=19+6;
   boton[BL1].y=15;
   boton[BL1].activo=0;
   boton[BL1].n=0;
   boton[BL1].fn=scrollup;
   boton[BL1].l=3;
   boton[BL1].k=KHOME;
   boton[BL1].k2=KGUP;
   strcpy(boton[BL2].nombre[0]," \x19 ");
   strcpy(boton[BL2].nombre[1]," \x19 ");
   boton[BL2].x=19+6;
   boton[BL2].y=17;
   boton[BL2].activo=0;
   boton[BL2].n=0;
   boton[BL2].fn=scrolldown;
   boton[BL2].l=3;
   boton[BL2].k=KEND;
   boton[BL2].k2=KGDOWN;
   strcpy(boton[BL3].nombre[0]," \x1b ");
   strcpy(boton[BL3].nombre[1]," \x1b ");
   boton[BL3].x=15+6;
   boton[BL3].y=16;
   boton[BL3].activo=0;
   boton[BL3].n=0;
   boton[BL3].fn=scrollleft;
   boton[BL3].l=3;
   boton[BL3].k=KDELETE;
   boton[BL3].k2=KGLEFT;
   strcpy(boton[BL4].nombre[0]," \x1a ");
   strcpy(boton[BL4].nombre[1]," \x1a ");
   boton[BL4].x=23+6;
   boton[BL4].y=16;
   boton[BL4].activo=0;
   boton[BL4].n=0;
   boton[BL4].fn=scrollright;
   boton[BL4].l=3;
   boton[BL4].k=KPGDOWN;
   boton[BL4].k2=KGRIGHT;

   strcpy(boton[BF1].nombre[0],"F1:Edit Char  ");
   strcpy(boton[BF1].nombre[1],"F1:Edit Char  ");
   boton[BF1].x=82 - ( (BOTLEN + 2) * 2) ;
   boton[BF1].y=5;
   boton[BF1].activo=0;
   boton[BF1].n=0;
   boton[BF1].fn=cff1;
   boton[BF1].l=BOTLEN;
   boton[BF1].k=KF1;
   boton[BF1].k2=KUNDEF;
   strcpy(boton[BC1].nombre[0],"@F1:Edit Text ");
   strcpy(boton[BC1].nombre[1],"@F1:Edit Text ");
   boton[BC1].x=82 - ( (BOTLEN + 2) * 1) ;
   boton[BC1].y=5;
   boton[BC1].activo=0;
   boton[BC1].n=0;
   boton[BC1].fn=cfcf1;
   boton[BC1].l=BOTLEN;
   boton[BC1].k=KCF1;
   boton[BC1].k2=KUNDEF;
   strcpy(boton[B10].nombre[0],"F2:Ascii Table");
   strcpy(boton[B10].nombre[1],"F2:Ascii Table");
   boton[B10].x=82 - ( (BOTLEN + 2) * 2) ;
   boton[B10].y=7;
   boton[B10].activo=0;
   boton[B10].n=0;
   boton[B10].fn=asciitable;
   boton[B10].l=BOTLEN;
   boton[B10].k=KF2;
   boton[B10].k2=KUNDEF;
   strcpy(boton[B11].nombre[0],"F9:Autosave On");
   strcpy(boton[B11].nombre[1],"F9:AutosaveOff");
   boton[B11].x=82 - ( (BOTLEN + 2) * 2) ;
   boton[B11].y=9;
   boton[B11].activo=0;
   boton[B11].n=0;
   boton[B11].fn=xautosave;
   boton[B11].l=BOTLEN;
   boton[B11].k=KF9;
   boton[B11].k2=KUNDEF;
   strcpy(boton[B12].nombre[0],"F11:Rec Macro ");
   strcpy(boton[B12].nombre[1],"F11:Stop Rec  ");
   boton[B12].x=82 - ( (BOTLEN + 2) * 2) ;
   boton[B12].y=11;
   boton[B12].activo=0;
   boton[B12].n=0;
   boton[B12].fn=macrorec;
   boton[B12].l=BOTLEN;
   boton[B12].k=KF11;
   boton[B12].k2=KUNDEF;
   strcpy(boton[B13].nombre[0],"F12:Play Macro ");
   strcpy(boton[B13].nombre[1],"F12:Play Macro ");
   boton[B13].x=82 - ( (BOTLEN + 2) * 2) ;
   boton[B13].y=13;
   boton[B13].activo=0;
   boton[B13].n=0;
   boton[B13].fn=macroplay;
   boton[B13].l=BOTLEN;
   boton[B13].k=KF12;
   boton[B13].k2=KUNDEF;
   strcpy(boton[B14].nombre[0],"@F2:Clear Text");
   strcpy(boton[B14].nombre[1],"@F2:Clear Text");
   boton[B14].x=82 - ( (BOTLEN + 2) * 1) ;
   boton[B14].y=7;
   boton[B14].activo=0;
   boton[B14].n=0;
   boton[B14].fn=cleartextedi;
   boton[B14].l=BOTLEN;
   boton[B14].k=KCF2;
   boton[B14].k2=KUNDEF;
   strcpy(boton[B15].nombre[0],"F10:Quit      ");
   strcpy(boton[B15].nombre[1],"F10:Quit      ");
   boton[B15].x=82 - ( (BOTLEN + 2) * 1) ;
   boton[B15].y=9;
   boton[B15].activo=0;
   boton[B15].n=0;
   boton[B15].fn=csalir;
   boton[B15].l=BOTLEN;
   boton[B15].k=KF10;
   boton[B15].k2=KUNDEF;
   strcpy(boton[B16].nombre[0],"@F11:8 Bits   ");
   strcpy(boton[B16].nombre[1],"@F11:9 Bits   ");
   boton[B16].x=82 - ( (BOTLEN + 2) * 1) ;
   boton[B16].y=11;
   boton[B16].activo=0;
   boton[B16].n=0;
   boton[B16].fn=nada;
   boton[B16].l=BOTLEN;
   boton[B16].k=KCF11;
   boton[B16].k2=KUNDEF;
   strcpy(boton[B17].nombre[0],"@F12:Grid On  ");
   strcpy(boton[B17].nombre[1],"@F12:Grid Off ");
   boton[B17].x=82 - ( (BOTLEN + 2) * 1) ;
   boton[B17].y=13;
   boton[B17].activo=0;
   boton[B17].n=0;
   boton[B17].fn=nada;
   boton[B17].l=BOTLEN;
   boton[B17].k=KCF12;
   boton[B17].k2=KUNDEF;
   strcpy(boton[BSB].nombre[0],"F3:      Save fonts 2 bin      ");
   strcpy(boton[BSB].nombre[1],"F3:      Save fonts 2 bin      ");
   boton[BSB].x=82 - ( (BOTLEN + 2) * 2);
   boton[BSB].y=15;
   boton[BSB].activo=0;
   boton[BSB].n=0;
   boton[BSB].fn=save2c;
   boton[BSB].l=BOTLEN * 2 + 2;
   boton[BSB].k=KF3;
   boton[BSB].k2=KUNDEF;
   strcpy(boton[BSH].nombre[0],"@F3:      Save fonts 2 c       ");
   strcpy(boton[BSH].nombre[1],"@F3:      Save fonts 2 c       ");
   boton[BSH].x=82 - ( (BOTLEN + 2) * 2);
   boton[BSH].y=17;
   boton[BSH].activo=0;
   boton[BSH].n=0;
   boton[BSH].fn=save2bin;
   boton[BSH].l=BOTLEN * 2 + 2;
   boton[BSH].k=KCF3;
   boton[BSH].k2=KUNDEF;
}

void redrawbut(void)
{
   BYTE att;
   WORD i,j;
   
   for(i=0;i<BOTONES;i++)
     {
	for(j=0;j<boton[i].l;j++)
	  {
	     att=(boton[i].activo==0) ? BotonOff : BotonOn;
	     vid_addr2[ (boton[i].y-1) * 160 + (boton[i].x-1) * 2 + j*2 + 1] = att;
	     vid_addr2[ (boton[i].y-1) * 160 + (boton[i].x-1) * 2 + j*2 ] = boton[i].nombre[ boton[i].n ][j];
	  }
     }
}

/*********************************************** Bati mouse - keyboard */

/* Esta funcion es una modificacion de Gpm_Getc de gpm-1.10 de liblow.c */
int Riq_Getc(FILE *f)
{
   fd_set selSet;
   int max, flag, result;
   static Gpm_Event ev;
   int fd=fileno(f);
   static int count;

  /* Hmm... I must be sure it is unbuffered */
  if (!(count++))
    setvbuf(f,NULL,_IONBF,0);

  if (!gpm_flag) return NOSEQUE;

  /* If the handler asked to provide more keys, give them back */
   if (gpm_morekeys) 
     {
	(*gpm_handler)(&ev,gpm_data);
	return MOUSE_SI;
     }
  gpm_hflag=0;
   
   max = (gpm_fd>fd) ? gpm_fd : fd;
   
   /*...................................................................*/
   if (gpm_fd>=0)                                            /* linux */
     while(1)
     {
	if (gpm_visiblepointer) GPM_DRAWPOINTER(&ev);
	do
	  {
	     FD_ZERO(&selSet);
	     FD_SET(fd,&selSet);
	     if (gpm_fd>-1)
	       FD_SET(gpm_fd,&selSet);
	     gpm_timeout.tv_sec=SELECT_TIME;
	     flag=select(max+1,&selSet,(fd_set *)NULL,(fd_set *)NULL,&gpm_timeout);
	  }
	while (!flag);
	
	if (flag==-1)
	  continue;
	
	/************ Aca esta el secreto!!!! **********************/
	if(FD_ISSET(fd,&selSet))
	  {
	     read(fd, &charbuf, sizeof(charbuf));
#ifdef DEBUG
	     printf("0x%2x, ",charbuf[0]);
	     printf("0x%2x, ",charbuf[1]);
	     printf("0x%2x, ",charbuf[2]);
	     printf("0x%2x, ",charbuf[3]);
	     printf("0x%2x, ",charbuf[4]);
	     printf("0x%2x\n\r",charbuf[5]);
	     (void) fflush(stdout);
#endif
	     return KEY_SI;
	  }
	
	if (Gpm_GetEvent(&ev) && gpm_handler && (result=(*gpm_handler)(&ev,gpm_data)))
	  {
	     gpm_hflag=1;
	     return MOUSE_SI;
	  }
     }
   return NOSEQUE;                        /* no mouse available */
}
	

/***********************************************  Funciones del mouse */
int mouse_handler(Gpm_Event *event, void *data)
{
   BYTE temp,i;
#ifdef DEBUG
   printf("mouse x,y:%i,%i\n\r",event->x,event->y);
#endif

   GPM_DRAWPOINTER(event);

   if (event->y <=16 && event->x <=8 && event->buttons==GPM_B_LEFT)
     {
	caracter.car[0][event->y-1]|= 1 << (7-(event->x-1));
	viewfont();
	updatef=UPDATEF;
     }
   else if (event->y <=16 && event->x <=8 && event->buttons==GPM_B_RIGHT)
     {
	caracter.car[0][event->y-1]&=255 -( 1 << (7-(event->x-1)));
	viewfont();
	updatef=UPDATEF;
     }
   else if ( event->y >= 20 && event->y <=24 && event->buttons !=0 )
     {
	switch( event->buttons )
	  {
	   case GPM_B_LEFT:
	     ascii=getvgaxy(event->x-1,event->y-1);
	     init_vga();
	     getfont(ascii);
	     rest_vga();
	     viewfont();
	     break;
	   case GPM_B_RIGHT:
	     temp=getvgaxy(event->x-1,event->y-1);
	     init_vga();
	     getfont(temp);
	     rest_vga();
	     viewfont();
	     break;
	  }
     }
   else 
     {
	for(i=0;i<BOTONES;i++)
	  {
	     if(event->y == boton[i].y &&
		event->x >= boton[i].x &&
		event->x <  boton[i].x+ boton[i].l )
	       {
		  if(event->type & GPM_DOWN)
		    {
		       minternal=i;
		       boton[i].activo=1;
		       redrawbut();
		    }
		  else if(event->type & GPM_UP)
		    {
		       if(minternal==i)
			 {
			    (boton[i].fn)();           /* ejecuta la funcion asociada */
			    boton[i].activo=0;
			    boton[i].n^=1;
			    redrawbut();
			 }
		    }
	       }
	  }
	if( (event->type & GPM_UP) && minternal!=255 )
	  {
	     for(i=0;i<BOTONES;i++)
	       boton[i].activo=0;
	     redrawbut();
	     minternal=255;
	  }
     }
   
   if( updatef==UPDATEF && ( event->dy!=0 || event->dx!=0))
     {
	viewfont();
	updatef=NOUPDATEF;
	GPM_DRAWPOINTER(event);
     }

   return 0;
}
void init_mouse(void)
{
   gpm_handler=mouse_handler;
   conn.eventMask=~0;
   conn.defaultMask=~GPM_HARD;
   conn.minMod=0;
   conn.maxMod=0;
   gpm_visiblepointer=0;
   
   /* do not force vc number (segun liblow.c) */
   if (Gpm_Open(&conn,0)==-1)
     fprintf(stderr,"Can't open mouse connection\n");
}
void rest_mouse(void)
{
   while (Gpm_Close()); /* close all the stack */
}
/***********************************************  Funciones del VChar */
void view_ascii(BYTE asc)
{
   BYTE asc_temp1;
   asc_temp1=asc/100;
   asc=asc%100;
   dirvgaxy(5,16,asc_temp1|0x30);
   asc_temp1=asc/10;
   asc=asc%10;
   dirvgaxy(6,16,asc_temp1|0x30);
   dirvgaxy(7,16,asc|0x30);
}

/* muestra el char que esta en caracter.car[0 o 1] */
void viewfont(void)
{
   short x,y;
   BYTE car,cartemp;
   
   for(y=0;y<16;y++)
     {
	cartemp=caracter.car[0][y];
	for(x=0;x<8;x++)
	  {
	     car=cartemp;
	     if( (cartemp&128)==0)
	       dirvgaxya(x,y,BigVacio);
	     else 
	       dirvgaxya(x,y,BigLleno);
	     cartemp=car<<1;
	  }
     }
   vid_addr2[0xa00]=ascii;
   view_ascii(ascii);
   showactive();
   if(autosavec==1)
     csavechar();
}

/* obtiene los bits del font */
void getfont(BYTE font)
{
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[0][i]=vid_addr[32*font+i];
}
void putfont(BYTE font)
{
   BYTE i;
   for(i=0;i<32;i++)
      vid_addr[32*font+i]=caracter.car[0][i];
}

/* modo ascii del teclado */
void init_key(void)
{
   ioctl(consolefd,KDGKBMODE,&keymode);
   ioctl(consolefd,KDSKBMODE,K_XLATE);
   ioctl(0, TCGETA, &consoleold);
   memcpy(&consolenew, &consoleold, sizeof(struct termio));
   consolenew.c_lflag &=~ (ICANON | ECHO | ISIG | IEXTEN );
   consolenew.c_iflag &=~ (ICRNL | INLCR |IGNCR  | IXON | IGNBRK | BRKINT | PARMRK );
   consolenew.c_oflag &=~ (OPOST);
   consolenew.c_cc[VMIN] =  1;
   consolenew.c_cc[VTIME] = 0;
   ioctl(0, TCSETA, &consolenew);
}
void rest_key(void)
{
   ioctl(0, TCSETA, &consoleold);
   ioctl(consolefd,KDSKBMODE,keymode);
}
void init_ioctl(void)
{
   newvtsizes.v_rows=25;
   newvtsizes.v_cols=80;
   newvtsizes.v_scrollsize=0;
   ioctl(consolefd,VT_RESIZE,&newvtsizes);
}
void rest_ioctl(void)
{
}

void init_screen(void)
{
   WORD z=0;
   int x,y;
   BYTE titulo[]=
     {
        "        ���                       �  �  �        � �                            "
	"        � �                       �  �  �  �  �  � �  "VCHARVER"                      "
	"        ���                       �� Ricardo Quesada                             "
     };
   BYTE anim[]=
     {
        "������������� � ������������� � ������������� � ������������� � ������������� � "
     };
   BYTE mensaje[]=
     {
        " * Clickea estas letras con el boton derecho e izquierdo. Nota las diferencias !"
	" * Ejecuta este programa con el RChar residente. Ver�s cambios en los botones.  "
	" * Si sos un depresivo lee VChar.txt                                            "
	"                                                         ...simplemente �����   "
	"                                                                        �����   "
     };
   BYTE copyri[]=
     {
	"                                VChar para Linux                                "
     };
  
   savedac();          /* Change dac colors */

   /* clear screen */
   for(x=0;x<80;x++)
     {
	for(y=0;y<3;y++)
	  dirvgaxya(x,y,Fondo);
     }
   for(x=0;x<80;x++)
     {
	for(y=3;y<18;y++)
	  dirvgaxya(x,y,BorrarB);
     }
   for(x=0;x<80;x++)
     {
	dirvgaxya(x,18,Borrar2);
     }
   for(x=0;x<80;x++)
     {
	for(y=19;y<24;y++)
	  dirvgaxya(x,y,Borrar);
     }
   for(x=0;x<80;x++)
     {
      	dirvgaxya(x,24,Fondo);
     }
   for(x=8;x<80;x++)
     {
	dirvgaxya(x,13,MacroFo);
     }
   for(z=0;z<80*3;z++)
     {
	vid_addr2[z*2]=titulo[z];
     }
   for(z=0;z<80*1;z++)
     {
	vid_addr2[160*18+z*2]=anim[z];
     }
   for(z=0;z<80*5;z++)
     {
	vid_addr2[160*19+z*2]=mensaje[z];
     }
   for(z=0;z<80*1;z++)
     {
	vid_addr2[160*24+z*2]=copyri[z];
     }
   
   vid_addr2[0xa01]=0x07;
   dirvgaxya(9,1,SActive);          /* font que se ve en el momento */

   init_but();                     /* new on VChar LI */
   redrawbut();
}
void rest_screen(void)
{
   BYTE x,y;
   for(x=0;x<80;x++)
     {
	for(y=0;y<25;y++)
	  dirvgaxya(x,y,Limpio);
     }
   restdac();
}

/* setea la pantalla. En 0xa0000 estan los fonts */
void init_vga( void )
{
   outb(0x3c4,2);           /* addr Sequencer Controller 2nd register */
   srmap=inb(0x3c5);
   outb(0x3c5,4);           /* select bitplane 2 */

   outb(0x3c4,4);           /* Seq Controller 4th register */
   srmem=inb(0x3c5);
   outb(0x3c5,6);           /* 256k & linear processing */

   outb(0x3ce,4);           /* address of Graphic Controller */
   grmap=inb(0x3cf);        /* Select Read Map reg */
   outb(0x3cf,2);           /* Bitplane 2 */
   
   outb(0x3ce,5);           /* Select Graphics mode Register */
   grmod=inb(0x3cf);
   outb(0x3cf,0);           /* Write mode 0, linear addresses, 16 colors */
   
   outb(0x3ce,6);           /* Select miscellaneous register */
   grmis=inb(0x3cf);
   outb(0x3cf,4);           /* Text, linear addressess 64k at 0xa000 */
}
/* Restaura la VGA como debe ser */
void rest_vga( void )
{
   outb(0x3c4,2);           /* addr Sequencer Controller 2nd register */
   outb(0x3c5,srmap); 

   outb(0x3c4,4);           /* Seq Controller 4th register */
   outb(0x3c5,srmem);       /* 256k & linear processing */

   outb(0x3ce,4);           /* address of Graphic Controller */
   outb(0x3cf,grmap);       /* Bitplane 2 */
   
   outb(0x3ce,5);           /* Select Graphics mode Register */
   outb(0x3cf,grmod);       /* Write mode 0, linear addresses, 16 colors */
   
   outb(0x3ce,6);           /* Select miscellaneous register */
   outb(0x3cf,grmis);       /* Text, linear addressess 64k at 0xa000 */
}

/* pone los permisos necesarios para acceder a los ports */
int init_ports(void)
{
   return (ioperm(0x3b4, 0x3df - 0x3b4 + 1 , 1 ));
}
int init_console(void)
{
   return (consolefd=open("/dev/console",O_RDONLY));
}

void doublefont(void)
{
   WORD i,j;
   init_vga();
   for(j=0;j<256;j++)
     {
	for(i=0;i<16;i++)
	  {
	     vid_addr[0x4000+i+j*32]=vcharfont[j*16+i];
	  }
     }
   outb(0x3c4,3);
   outb(0x3c5, (inb(0x3c5) & 0xc0) | 1);
   outw(0x3c4,0x0403); /* select 1st and 2nd font */
   rest_vga();
}
void doubleno(void)
{
   outb(0x3c4,3);
   outb(0x3c5,(inb(0x3c5) & 0xc0));
}
void nada(void){}
void casciiup(void)
{
   ascii--;
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();
}
void casciidown(void)
{
   ascii++;
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();
}
void csavechar(void)
{
   init_vga();
   putfont(ascii);
   rest_vga();
}
void csalir(void)
{
   salir=1;
}
void xautosave(void)
{
   autosavec^=1;
}
void cfcf1(void)
{
   if(mode==CARAC){
      mode=TEXTEDIT;
      setxy(posx2,posy2);
   }
   else if(mode==EDIT){
      mode=TEXTEDIT;
      setxy(posx2,posy2);
   }
   else if(mode==TEXTEDIT){
      mode=CARAC;
      setxy(0,16);
   }
}
void cff1(void)
{
   if(mode==CARAC){
      mode=EDIT;
      setxy(posx,posy);
   }
   else if(mode==EDIT){
      mode=CARAC;
      setxy(0,16);
   }
   else if(mode==TEXTEDIT){
      mode=EDIT;
      setxy(posx,posy);
   }
}
void cinvert(void)
{
   BYTE i;
   for(i=0;i<32;i++)
     {
	caracter.car[0][i]^=255;
     }
   viewfont();
}
void cupdown(void)
{
   BYTE i;
   for(i=0;i<8;i++)
     {
	caracter.car[0][i]^=caracter.car[0][15-i];
	caracter.car[0][15-i]^=caracter.car[0][i];
	caracter.car[0][i]^=caracter.car[0][15-i];
     }
   viewfont();
}
void cleftright(void)
{
   BYTE i,j,k,l;
   for(i=0;i<16;i++)
     {
	j=caracter.car[0][i];
	k=0;
	for(l=0;l<4;l++)
	  {
	     k |= ( (j & (  1<< (7-l) )) >> (7-l*2) );
	     k |= ( (j & (128>> (7-l) )) << (7-l*2) );
	  }
	
	caracter.car[0][i]=k;
     }
   viewfont();
}
void scrollup(void)
{
   BYTE i,temp;
   temp=caracter.car[0][0];
   for(i=0;i<16;i++)
     caracter.car[0][i]=caracter.car[0][i+1];
   caracter.car[0][15]=temp;
   viewfont();
}
void scrolldown(void)
{
   BYTE i,temp;
   temp=caracter.car[0][15];
   for(i=15;i>0;i--)
     caracter.car[0][i]=caracter.car[0][i-1];
   caracter.car[0][0]=temp;
   viewfont();
}
void scrollright(void)
{
   BYTE i,temp;
   for(i=0;i<16;i++)
     {
	temp=caracter.car[0][i] >> 1;
	caracter.car[0][i]=temp | ( (caracter.car[0][i]&1) << 7) ;
     }
   viewfont();
}
void scrollleft(void){
   BYTE i,temp;
   for(i=0;i<16;i++){
      temp=caracter.car[0][i] << 1;
      caracter.car[0][i]=temp | ( (caracter.car[0][i]&128) >> 7) ;
   }
   viewfont();
}
void cclean(void){
   BYTE i;
   for(i=0;i<16;i++)
     caracter.car[0][i]=0;
   viewfont();
}
void macroview(void){
   BYTE asc,z,asc_temp1;
   BYTE macrotit[]={
      "Recording macro...Press F11 to stop. Buffer size[   ]"
   };
   for(z=0;z<53;z++)
     vid_addr2[160*13+z*2+17*2]=macrotit[z];
   asc=MACROSIZE-macroindice;
   asc_temp1=asc/100;
   asc=asc%100;
   dirvgaxy(66,13,asc_temp1|0x30);
   asc_temp1=asc/10;
   asc=asc%10;
   dirvgaxy(67,13,asc_temp1|0x30);
   dirvgaxy(68,13,asc|0x30);
}
void macronoview(void){
   BYTE z;
   for(z=0;z<53;z++)
     vid_addr2[160*13+z*2+17*2]=0x20;
}
void macrorec(void){
   WORD i;
   switch(macroplayrec){
    case 0: /* start recording */
      macroplayrec=MACROREC;
      macroindice=0;
      for(i=0;i<MACROSIZE;i++)
	macrobuf[i]=0;
      macroview();
      break;
    case 1: /* stop recording */
      macrobuf[macroindice-1]=0;
      macroplayrec=MACROSTOP;
      macronoview();
      break;
   }
}
void macroplay(void){
   if(macroplayrec==MACROSTOP){
      if(macrobuf[0]!=0){
	 macroindice=0;
	 macroplayrec=MACROPLAY;
      }
   }
   else if(macroplayrec==MACROREC)
     macroindice--;
}
void asciitable(void){
   WORD z;
   for(z=0;z<256;z++)
     vid_addr2[160*19+z*2]=(BYTE)z;
}
void cleartextedi(void){
   WORD z;
   for(z=0;z<5*80;z++)
     vid_addr2[160*19+z*2]=0x20;
}
void copyc(void){
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[1][i]=caracter.car[0][i];
}
void pastec(void){
   BYTE i;
   for(i=0;i<32;i++)
     caracter.car[0][i]=caracter.car[1][i];
   viewfont();
}

/* Teclas que son comunes a todos los modos */
void keycomun(WORD doscode){
   BYTE i;
   for(i=0;i<BOTONES;i++)
     {
	if(doscode==boton[i].k || doscode==boton[i].k2 )
	  {
	     (boton[i].fn)();           /* ejecuta la funcion asociada */
	     boton[i].activo=0;
	     boton[i].n^=1;
	     redrawbut();
	     comun=1;
	  }
     }
}


/* esta rutina la necesito para usar los mismos valores que en DOS */
void convertcode(WORD *doscode){
   WORD temp=0,ORI=0;
   int modifiers;
   
   modifiers=6; /* code for the ioctl */
   if (ioctl(0,TIOCLINUX,&modifiers)<0)
     modifiers=0;
   if (modifiers&1) ORI|=KSHIFT;
   if (modifiers&2) ORI|=KRALT;
   if (modifiers&4) ORI|=KCONTROL;
   if (modifiers&8) ORI|=KLALT;
   if (!modifiers)  ORI|=KNONE;

   switch(charbuf[0]){
    case 0x1b:
      switch(charbuf[3]){       /* teclas grises */
       case 0x7e:
	 switch(charbuf[2]){
	  case 0x35:
	    temp=KPGUP;
	    break;
	  case 0x36:
	    temp=KPGDOWN;
	    break;
	  case 0x31:
	    temp=KHOME;
	    break;
	  case 0x34:
	    temp=KEND;
	    break;
	  case 0x32:
	    temp=KINSERT;
	    break;
	  case 0x33:
	    temp=KDELETE;
	    break;
	 }
	 break;
      }
      switch(charbuf[2]){
	  case 0x41:
	    temp=KUP;
	    break;
	  case 0x42:
	    temp=KDOWN;
	    break;
	  case 0x43:
	    temp=KRIGHT;
	    break;
	  case 0x44:
	    temp=KLEFT;
	    break;
	  case 0x78:
	    temp=KGUP;
	    break;
	  case 0x72:
	    temp=KGDOWN;
	    break;
	  case 0x76:
	    temp=KGRIGHT;
	    break;
	  case 0x74:
	    temp=KGLEFT;
	    break;
       case 0x5b:
	 switch(charbuf[3]){
	  case 0x41:
	    temp=KF1;
	    break;
	  case 0x42:
	    temp=KF2;
	    break;
	     case 0x43:
	    temp=KF3;
	    break;
	  case 0x44:
	    temp=KF4;
	    break;
	  case 0x45:
	    temp=KF5;
	    break;
	 }
	 break;
       case 0x31:
	 switch(charbuf[3]){
	  case 0x37:
	    temp=KF6;
	    break;
	  case 0x38:
	    temp=KF7;
	    break;
	  case 0x39:
	    temp=KF8;
	    break;
	 }
	 break;
       case 0x32:
	 switch(charbuf[3]){
	  case 0x30:
	    temp=KF9;
	    break;
	  case 0x31:
	    temp=KF10;
	    break;
	  case 0x33:
	    temp=KF11;
	    break;
	  case 0x34:
	    temp=KF12;
	    break;
	 }
	 break;
      }
      break; 
   }
   temp=(temp==0) ? charbuf[0] & 0xff:temp;
   doscode[0]=( (temp&0x0f00)==0) ? temp : temp|ORI;
#ifdef DEBUG
   printf("doscode 0x%4x     ori 0x4%4x \n\r",doscode[0],ORI); 
#endif
}

void loop(void)
{
   BYTE temp;
   WORD doscode;
   
   init_vga();
   getfont(ascii);
   rest_vga();
   viewfont();

   temp=0;
   
   do{
      if(macroplayrec==MACROPLAY)
	{
	   doscode=macrobuf[macroindice];
	   macroindice++;
	   if(macrobuf[macroindice]==0)
	     {
		macroplayrec=MACROSTOP;
		macronoview();
	     }
	   if(macroindice>=MACROSIZE)
	     {
		macroplayrec=MACROSTOP;
		macronoview();
	     }
	   temp=1;
	}
      else if((Riq_Getc(stdin))==KEY_SI)
	{
	   convertcode(&doscode);
	   if(macroplayrec==MACROREC)
	     {
		macrobuf[macroindice]=doscode;
		macroview();
		macroindice++;
		if(macroindice>=MACROSIZE)
		  {
		     macroplayrec=MACROSTOP;macronoview();
		  }
	     }
	   temp=1;
	}
      if(temp==1)
	{
	   if(mode==CARAC)
	     {
		keycomun(doscode);       /* teclas comunes para todos los modos */
		if(comun==0)
		  {
		     switch(doscode)
		       {
			case KUP:
			  ascii++; break;
			case KDOWN:
			  ascii--; break;
			case KRIGHT:
			  ascii=ascii+10;
			  break;
			case KLEFT:
			  ascii=ascii-10;
			  break;
			default:
			  if( doscode<0x00ff)
			    ascii=(BYTE) doscode;
			  break;
		       }
		     init_vga();
		     getfont(ascii);
		     rest_vga();
		     viewfont();
		  }
		else comun=0;
	     }
	   else if(mode==EDIT)
	     {
		keycomun(doscode);       /* teclas comunes para todos los modos */
		if(comun==0)
		  {
		     switch(doscode)
		       {
			case KUP:
			  posy--;
			  posy=(posy>15)?15:posy;
			  setxy(posx,posy); 
			  break;
			case KDOWN:
			  posy++;
			  posy=(posy>15)?0:posy;
			  setxy(posx,posy); 
			  break;
			case KRIGHT:
			  posx++;
			  posx=(posx>7)?0:posx;
			  setxy(posx,posy); 
			  break;
			case KLEFT:
			  posx--;
			  posx=(posx>7)?7:posx;
			  setxy(posx,posy); 
			  break;
			case KSPACE:
			  caracter.car[0][posy]^= 1 << (7-posx);
			  viewfont();
			  break;
			case K1:
			  caracter.car[0][posy]|= 1 << (7-posx);
			  viewfont();
			  break;
			case K2:
			  caracter.car[0][posy]&=255 -( 1 << (7-posx));
			  viewfont();
			  break;
			default:
			  break;
		       }
		  }
		else comun=0;
	     }
	   else if(mode==TEXTEDIT)
	     {
		keycomun(doscode);       /* teclas comunes para todos los modos */
		if(comun==0)
		  {
		     switch(doscode)
		       {
			case KUP:
			  irarriba:
			  posy2--;
			  posy2=(posy2<19)?23:posy2;
			  setxy(posx2,posy2);
			  break;
			case KDOWN:
			  irabajo:
			  posy2++;
			  posy2=(posy2>23)?19:posy2;
			  setxy(posx2,posy2);
			  break;
			case KRIGHT:
			  irderecha:
			  posx2++;
			  if(posx2<=79)
			    setxy(posx2,posy2);
			  else
			    {
			       posx2=0;
			       goto irabajo;
			    }
			  break;
			case KLEFT:
			  irizquierda:
			  posx2--;
			  if(posx2<=79)
			    setxy(posx2,posy2);
			  else
			    {
			       posx2=79;
			       goto irarriba;
			    }
			  break;
			case KBACK:
			  if(posx2==0)
			    {
			       if(posy2==19)
				 dirvgaxy(79,23,0x20);
			       else 
				 dirvgaxy(79,posy2-1,0x20);
			    }
			  else 
			    dirvgaxy(posx2-1,posy2,0x20);
			  goto irizquierda;
			case KENTER:
			  posx2=0;
			  goto irabajo;
			default:
			  if( doscode<0x00ff)
			    dirvgaxy(posx2,posy2,(BYTE) doscode);
			  goto irderecha;
			  break;
		       }
		  }
		else comun=0;
	     } /* mode==TEXT */
	   temp=0;
	} /* if(temp==1) */
   } while(salir==0);
}


void savedac(void)
{
   WORD i=0;
   outb(0x3c6,0xff);        /*Pel Mask (255)*/
   outb(0x3c7,0);           /* Esto deberia esta en asm con  */
   for(i=0;i<256;i++)
     {      /* rep insb */
	dac[i*3+0]=inb(0x3c9);
	dac[i*3+1]=inb(0x3c9);
	dac[i*3+2]=inb(0x3c9);
     }
   outb(0x3c8,2);
   outb(0x3c9,dac[15*3+0]);
   outb(0x3c9,dac[15*3+1]);
   outb(0x3c9,dac[15*3+2]);
   
   outb(0x3c8,4);
   outb(0x3c9,dac[0*3+0]);
   outb(0x3c9,dac[0*3+1]);
   outb(0x3c9,dac[0*3+2]);
   
   outb(0x3c8,14);
   outb(0x3c9,dac[7*3+0]);
   outb(0x3c9,dac[7*3+1]);
   outb(0x3c9,dac[7*3+2]);
   
   outb(0x3c8,8);
   outb(0x3c9,dac[0*3+0]);
   outb(0x3c9,dac[0*3+1]);
   outb(0x3c9,dac[0*3+2]);

   outb(0x3c8,9);
   outb(0x3c9,dac[3*3+0]);
   outb(0x3c9,dac[3*3+1]);
   outb(0x3c9,dac[3*3+2]);
}
void restdac(void)
{
   WORD i=0;
   outb(0x3c6,0xff);        /*Pel Mask (255)*/
   outb(0x3c8,0);           /* Esto deberia esta en asm con  */
   for(i=0;i<256;i++)
     {      /* rep outb */
	outb(0x3c9,dac[i*3+0]);
	outb(0x3c9,dac[i*3+1]);
	outb(0x3c9,dac[i*3+2]);
     }
}

void save2c(void)
{
   WORD i,j;
   FILE *out;

   init_vga();
   umask(18);           /* 022 */
   out=fopen("font.h","w+");
   fprintf(out,"/* VChar NI para Linux */\n");
   fprintf(out,"#define FONTH  16\n");
   fprintf(out,"#define FONTW   8\n");
   fprintf(out,"#define FONTL 256\n");
   fprintf(out,"unsigned char FONTS[]={\n");
   for(i=0;i<256;i++)
     {
	fprintf(out,"\"");
	for(j=0;j<16;j++)
	  {
	     fprintf(out,"\\x%X",vid_addr[i*32+j]);
	  }
	fprintf(out,"\"\n");
     }
   fprintf(out,"};\n");
   rest_vga();
   fclose(out);
}
void save2bin(void)
{
   WORD i,fdbin;
   init_vga();
   umask(18);              /* 022 */
   fdbin=open("font.8x16",O_RDWR | O_CREAT | O_TRUNC,420);
   for(i=0;i<256;i++)
     {
	write(fdbin,&vid_addr[i*32],16);
     }
   close(fdbin);
   rest_vga();
}


/*           * * * *                 main               * * * *             */
int main(int argc,char **argv)
{
   BYTE i;

   printf("\nVChar - por Ricardo Quesada (c) 1994-1996\n");
   if(init_ports())
     {
	printf("Error: No se puede tener acceso sobre los ports\n");
	return 1;
     }
   if(init_console()<0)
     {
	printf("Error: No se puede abrir /dev/console\n");
	return 1;
     }
   
   opt_pointer=open(ttyname(0),O_RDWR); /* can't be 0 */
   
   ascii='a';
   salir=0;
   comun=0;
   macroplayrec=0;
   macroindice=0;
   posx=0;posy=0;
   posx2=0;posy2=19;
   minternal=255;
   autosavec=0;
   
   for(i=0;i<MACROSIZE;i++)
     macrobuf[i]=0;
   mode=CARAC;

   init_ioctl();
   init_vid();       /* averiaguar donde se debe el StrAdd */
   if(openmem_vid()!=0) return 1;      /* reserva memoria de video */
   if(openmem_char()!=0) return 1;     /* reserva memoria para el juego de fonts */
   init_screen();
   doublefont();                 /* dos fonts activos */
   init_mouse();
   init_key();                   /* key k_xlate mode */
   
   loop();                       /* hace todo */
   
   rest_key();                    /* vuelte al estado anterior del teclado */
   rest_mouse();
   rest_screen();
   doubleno();                   /* 1 font activo */
   closemem_char();
   closemem_vid();
   rest_ioctl();
   
   return 0;
}
