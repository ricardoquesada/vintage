#include <ncurses.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <stdlib.h>

#define KeyUp    'w'
#define KeyDown  's'
#define KeyLeft  'a'
#define KeyRight 'd'
#define KeyMarca ' '

enum coordenadas {
   a,b,c,d,e,f,g,h,i,j
};

enum barcos {
   agua,barco,tocado,hundido,nada 
};

enum direccion {
   arriba,abajo,derecha,izquierda
};

enum Boolean {
   False,True
};

const char CarTbl[5] = {
   'A','B','T','H','-' 
};

enum visulizacion {
   oculto,visible
};

const char CoorY[10] =  {
   'A','B','C','D','E','F','G','H','I','J' 
};

class Tablero  {
 public :
   Tablero ( int, int, int, int );
   void Print ( enum visulizacion );
   void ReadCar ( );
   enum barcos QueHay ( int r, int c ) { return tabla[r][c]; };
   void PutBarco ( int, int, enum barcos );
   enum barcos RcvShoot ( int, int );
   void SendTabla( ); 
   void RecvTabla( );
 private :
   WINDOW *winTbl;
   void Hundir ( int, int );
   enum Boolean FueHundido ( int, int, enum direccion );
   barcos tabla[10][10];
   int stepx,stepy; // cada cuantas columnas o renglones respectivamente se imprime el tablero
};

// Constructor del tablero
Tablero::Tablero ( int posr, int posc, int stepy=2, int stepx=2 )  {
   Tablero::stepx=stepx;
   Tablero::stepy=stepy;
   for (int cont1=0; cont1<10; cont1++ )
     for (int cont2=0; cont2<10; cont2++ )
	 tabla[cont1][cont2]=nada;
   winTbl=newwin(2 + stepy * 10,2 + stepx * 10,posr,posc);
   //keypad(MyTablero,1);
}

// Imprime el tablero
// la variable siveo habilita o no la impresion de barcos no tocados 
void Tablero::Print ( enum visulizacion siveo = visible ) {
   #ifdef A_COLOR
     if (has_colors()) 
     wattron(winTbl,COLOR_PAIR(COLOR_WHITE));
   #endif /* A_COLOR */
	    
   for (int cont=0; cont<10; cont++)  {
      mvwaddch(winTbl,0,(cont * stepx)+2,(char) (cont+48));
      mvwaddch(winTbl,(cont * stepy)+2,0,CoorY[cont]);  
   };
   for (int cont1=0; cont1<10; cont1++ )
      for (int cont2=0; cont2<10; cont2++ ) {
         switch (tabla[cont1][cont2]) {
	  case nada :
	    #ifdef A_COLOR
	      if (has_colors())
	      wattron(winTbl,COLOR_PAIR(COLOR_RED));
            #endif /* A_COLOR */
	    break;
	  case agua :
	    #ifdef A_COLOR
	      if (has_colors())
	      wattron(winTbl,COLOR_PAIR(COLOR_CYAN));
            #endif /* A_COLOR */
	    break;
	  case barco :
	    #ifdef A_COLOR
	      if (has_colors())
	      wattron(winTbl,COLOR_PAIR( ((siveo==visible)?COLOR_BLUE:COLOR_RED) ));
            #endif /* A_COLOR */
	    break;
	  case tocado :
	    #ifdef A_COLOR
	      if (has_colors())
	      wattron(winTbl,COLOR_PAIR(COLOR_GREEN));
            #endif /* A_COLOR */
	    break;
	  case hundido :
	    #ifdef A_COLOR
	      if (has_colors())
	      wattron(winTbl,COLOR_PAIR(COLOR_MAGENTA));
            #endif /* A_COLOR */
	    break;
	 }
	 mvwaddch(winTbl,(cont1 * stepy)+2,(cont2 * stepx)+2,
( (siveo || tabla[cont1][cont2]!=barco)? CarTbl[(tabla[cont1][cont2])]:'-') );
      }
   #ifdef A_COLOR
      attrset(0);
   #endif /* A_COLOR */
	    
   keypad(winTbl,1);
   wrefresh(winTbl);
};
// pone algo (barco, agua, tocado, hundido, etc) en la posicion (r,c)
void Tablero::PutBarco ( int r, int c, enum barcos B ) {
   if (!(r>9 || c>9 || r<0 || c<0)) 
     tabla[r][c] = (barcos) B;
};
// lee los barcos del tablero
void Tablero::ReadCar ( )  {
   keypad(winTbl,1);
   char v;
   int x,y;
   noecho();
   wmove(winTbl,2,2);
   do  {
      getyx(winTbl,y,x);
      v=wgetch(winTbl);
      switch (v)   {
       case KeyUp :
	 if (y>2) for (int cont=1; cont<=stepy; cont++)
	   wmove(winTbl,(--y),x);
	 break;
       case KeyDown :
         if (y<2+(9*stepy)) for (int cont=1; cont<=stepy; cont++) 
	   wmove(winTbl,(++y),x);
	 break;
       case KeyLeft :
	 if (x>2) for (int cont=1; cont<=stepx; cont++)
	   wmove(winTbl,y,(--x));
	 break;
       case KeyRight :
         if (x<2+(9*stepx)) for (int cont=1; cont<=stepx; cont++)
	   wmove(winTbl,y,(++x));
	 break;
       case KeyMarca :
	 int divy=(int) (y-2)/stepy ,divx=(int) (x-2)/stepx;
	 
	 if ( QueHay(divy,divx)==nada )  {
	    //waddch(winTbl,CarTbl[barco]);
	    PutBarco(divy,divx,barco);
	    Print( );
	 } else  {
	    //waddch(winTbl,CarTbl[nada]);
	    PutBarco(divy,divx,nada);
	    Print( );
	 }
	 wmove(winTbl,y,x);
         break;
      }
   } while ('Q' != v);
   echo();
};

// Se le manda las coordenadas de disparo y devuleve lo ocurrido y modifica el tablero
enum barcos Tablero::RcvShoot ( int CoorR, int CoorC ) {
   if (CoorR>9 || CoorC>9 || CoorR<0 || CoorC<0) return nada;
   barcos QuePaso;
   QuePaso=QueHay(CoorR,CoorC);
   switch (QuePaso) {
    case hundido :
    case tocado :
    case agua :
      return nada;
      break;
    case nada :
      PutBarco(CoorR,CoorC,agua);
      return agua;
      break;
    case barco :
      PutBarco(CoorR,CoorC,tocado);
      if (FueHundido(CoorR,CoorC,arriba) && FueHundido(CoorR,CoorC,abajo) &&  
	  FueHundido(CoorR,CoorC,izquierda) && FueHundido(CoorR,CoorC,derecha)) {
	     Hundir(CoorR,CoorC);
	     return hundido;
	  }
      else {
	 PutBarco(CoorR-1,CoorC-1,agua);
	 PutBarco(CoorR-1,CoorC+1,agua);
	 PutBarco(CoorR+1,CoorC-1,agua);
	 PutBarco(CoorR+1,CoorC+1,agua);
	 return tocado;
      }
      break;
   }
}

// Pone como hundido a un barco
void Tablero::Hundir ( int CoorR, int CoorC ) {
   if (!(CoorR>9 || CoorC>9 || CoorR<0 || CoorC<0))
      if (QueHay(CoorR,CoorC)==nada) PutBarco(CoorR,CoorC,agua); 
      else if (QueHay(CoorR,CoorC)==tocado) {
	 PutBarco(CoorR,CoorC,hundido);
	 PutBarco(CoorR-1,CoorC-1,agua);
	 PutBarco(CoorR-1,CoorC+1,agua);
	 PutBarco(CoorR+1,CoorC-1,agua);
	 PutBarco(CoorR+1,CoorC+1,agua);
	 Hundir(CoorR+1,CoorC);
	 Hundir(CoorR,CoorC+1);
	 Hundir(CoorR-1,CoorC);
	 Hundir(CoorR,CoorC-1);
      }
}

// analiza si el barco tocado fue hundido
enum Boolean Tablero::FueHundido( int CoorR, int CoorC, enum direccion direc )  {
   if (CoorR>9 || CoorC>9 || CoorR<0 || CoorC<0) return True;
   barcos Barco;
   Barco=QueHay(CoorR,CoorC);
   if (Barco==agua || Barco==nada) return True;
   else if (Barco==tocado)
      switch (direc) {
       case arriba : 
	 return FueHundido(CoorR-1,CoorC,direc);
	 break;
       case abajo :
	 return FueHundido(CoorR+1,CoorC,direc);
	 break;
       case izquierda :
	 return FueHundido(CoorR,CoorC-1,direc);
	 break;
       case derecha :
	 return FueHundido(CoorR,CoorC+1,direc);
	 break;
      };
   return False;
}

// Simula la recepcion de un tablero por parte del server 
void Tablero::RecvTabla ( ) {
   barcos auxTbl[10][10] = 
     { nada ,barco,barco,barco,nada ,nada ,nada ,nada ,nada ,nada ,
       nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,barco,
       barco,barco,nada ,nada ,nada ,nada ,nada ,nada ,nada ,barco,
       nada ,nada ,nada ,nada ,nada ,nada ,barco,barco,nada ,barco,
       nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,barco,
       nada ,barco,barco,barco,nada ,nada ,nada ,nada ,nada ,nada ,
       nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,barco,
       barco,barco,nada ,nada ,nada ,nada ,nada ,nada ,nada ,barco,
       nada ,nada ,nada ,nada ,nada ,nada ,barco,barco,nada ,barco,
       nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,nada ,barco
     };
   for (int cont1=0; cont1<10; ++cont1)
     for (int cont2=0; cont2<10; ++cont2)
     tabla[cont1][cont2]=auxTbl[cont1][cont2];
}

class Player  {
 public :
   Player ( char[], char[], int, int, int, int );
   char *GetNombre( )  { return (nombre); };
   char *GetOtherNombre( )  { return (OtherName); };
   void PrintMyTbl ( );  
   void PrintOtherTbl ( );
   void IngMyTbl ( )  { MyTablero->ReadCar(); }; // Lee los barcos de mi tablero
   void GetOtherTbl ( )  { OtherTbl->RecvTabla(); }; // Recive el tablero de un rival para imprimir
   void GetShot ( );
 private :
   class Tablero *OtherTbl;
   class Tablero *MyTablero;
   char *nombre;
   char *OtherName;
   int Rmywin,Cmywin,Rotwin,Cotwin; // Coordenadas de los tableros
   inline void SetNombre (char name[] )  { nombre=name; };
   inline void SetOtherNombre (char name[] )  { OtherName=name; };
};

// Inicieliza el jugador con los valores por defecto
Player::Player ( char name[], char rival[]="Rival", int rmy=3, int cmy=2, int rot=3, int cot=18 ) {
   SetNombre(name);
   SetOtherNombre(rival);
   Rmywin=rmy; Cmywin=cmy; Rotwin=rot; Cotwin=cot;
   MyTablero = new Tablero(Rmywin,Cmywin,1,1);
   OtherTbl = new Tablero(Rotwin,Cotwin,1,1);
}

//imprime mi tablero
void Player::PrintMyTbl ( ) {
   mvaddstr(Rmywin-2,Cmywin,GetNombre());
   MyTablero->Print();
   refresh();
};

// Imprime el tabero rival de turno 
void Player::PrintOtherTbl ( ) { 
   mvaddstr(Rotwin-2,Cotwin,GetOtherNombre());
   OtherTbl->Print(oculto);
   refresh();
};

void Player::GetShot ( ) {
   // lee un disparo del teclado lo manda
   WINDOW *vent;
   vent=newwin(6,15,15,63);
   char carR,carC;
   mvwprintw(vent,0,0,"Renglon :    ");
   wrefresh(vent);
   do {
      carR=mvwgetch(vent,0,10);
   } while ((65> (int) carR) || (75< (int) carR));
   mvwprintw(vent,1,0,"Columna :    ");
   wrefresh(vent);
   do {
      carC=mvwgetch(vent,1,10);
   } while ((48> (int) carC) || (58< (int) carC));
   // Aca tendria que mandarle el disparo al server 
   mvwprintw(vent,3,0,"(%c,%c) : %c ",carR,carC,CarTbl[OtherTbl->RcvShoot( ((int) carR) - 65, ((int) carC) - 48)]);
   //mvwdelch(vent,0,10);
   //mvwdelch(vent,1,10);
   wrefresh(vent);
}

void inicializa ( ) {
   // inicializa la pantalla y los colores
   initscr();
   cbreak();
   //noecho();
   //keypad(,1);
   #ifdef A_COLOR
   start_color();
   init_pair(COLOR_BLACK, COLOR_BLACK, COLOR_BLACK);
   init_pair(COLOR_GREEN, COLOR_GREEN, COLOR_BLACK);
   init_pair(COLOR_RED, COLOR_RED, COLOR_BLACK);
   init_pair(COLOR_CYAN, COLOR_CYAN, COLOR_BLACK);
   init_pair(COLOR_WHITE, COLOR_WHITE, COLOR_BLACK);
   init_pair(COLOR_MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
   init_pair(COLOR_BLUE, COLOR_BLUE, COLOR_BLACK);
   init_pair(COLOR_YELLOW, COLOR_YELLOW, COLOR_BLACK);
   #endif /* A_COLOR */
}

void main(void)
     {
	inicializa();
	class Player jugador("Sebastian");
	//Para terminar de poner los barcos apretar ''Q'' (mayuscula).
	//Las Letras del las coordenadas van tambien en mayuscula".
	jugador.PrintMyTbl( );
	jugador.IngMyTbl( );
	jugador.PrintMyTbl( );
	jugador.GetOtherTbl( );
	jugador.PrintOtherTbl( );
        // Repite 10 tiros al tablero rival
 	for (int cont=0; cont<10; cont++) {
	   jugador.GetShot( );
	   jugador.PrintMyTbl( );
	   jugador.PrintOtherTbl( );
	}
	endwin();
     };

