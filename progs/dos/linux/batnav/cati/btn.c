#include <ncurses.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <stdlib.h>

struct Player  {
   char *nombre;
   char *TermArch;
   char *TermType;
   SCREEN *TermScreen;
   FILE *fout,*fin;
   char tablero[10][10];
   WINDOW *MyTablero;
   WINDOW *OtherTbl;
};

struct Lista {
   struct Player *player;
   struct Player *next;
};

void MakePlayer (char name[] ,char termarch[] ,char termtype[] , struct Player **ply)
     {
	int cont1,cont2;
	*ply=malloc(sizeof(struct Player));
        (* ply)->nombre=name;
	(* ply)->TermArch=termarch;
	(* ply)->TermType=termtype;
	(* ply)->fout=fopen((* ply)->TermArch,"w+");
	(* ply)->fin=fopen((* ply)->TermArch,"r+");
	(* ply)->TermScreen=newterm((* ply)->TermType,(* ply)->fout,(* ply)->fin);
	for (cont1=0; cont1<10; cont1++ )
	  for (cont2=0; cont2<10; cont2++ )
	      (* ply)->tablero[cont1][cont2]='-';
	(* ply)->MyTablero=newwin(20,20,3,2);
	(* ply)->OtherTbl=newwin(20,20,3,25);
     }
	  
void PrintMyTbl (struct Player *ply )
     {
	int cont1,cont2;
	set_term(ply->TermScreen);
	for (cont1=0; cont1<10; cont1++ )
	  for (cont2=0; cont2<10; cont2++ )
	  mvwaddch(ply->MyTablero,cont1 * 2,cont2 * 2,(char) ply->tablero[cont1][cont2]);
	wrefresh(ply->MyTablero);
     }

/*void PrintPart(struct Lista *lista ) {
   if (lista!=NULL)  {
      printf("%s\r\n",lista->(player->nombre));
      PrintPart(lista->next);
   }
}*/

/* /void addlist( char name[], char termarch[], char termtype[], struct Lista **lista)  {
//   do while (*lista!=NULL) lista; */
   

void main(void)
     {
/* Este ejemplo imprime la pantalla en la tty6 */
	struct Player *jugador1;
	struct Lista primero;
	initscr();
	/*noecho();*/
	cbreak();
	MakePlayer("Sebastian","/dev/tty6","console",&jugador1);
	set_term(jugador1->TermScreen);
	addstr("Nombre : ");
	addnstr(jugador1->nombre,-1); 
	PrintMyTbl(jugador1);
	/* addch( (char) jugador1->tablero[2][2]);*/
	refresh();
	endwin();
     };

