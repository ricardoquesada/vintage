/************************************************************************
 * BATALLA NAVAL.
 * Cliente para X
 * por Ricardo Quesada
 * version: 0.1
 ***********************************************************************/

#include <X11/Xlib.h>
#include <xview/xview.h>
#include <xview/window.h>
#include <xview/openmenu.h>
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/xv_xrect.h>
/* 
 *                   VARIABLES GLOBALES
 */
Frame frame;
Panel panel;
Canvas canvas;

/* 
 *                        FUNCIONES
 */

void nada( void ) {
   printf("apretastes el boton CONECTAR\n");
}

void quit( ) {
   xv_destroy_safe( frame );
}

void canvas_repaint_proc ( Canvas canvas, Xv_Window paint_window, Display *dpy,
			  Window xwin, Xv_xrectlist  *xrects )
{
   GC gc;
   /* int width,height; */
   int i,w,h;
   
   gc = DefaultGC(dpy, DefaultScreen(dpy));
/*   width = (int) xv_get( paint_window , XV_WIDTH );
   height = (int) xv_get( paint_window , XV_HEIGHT ); */
   
   h=20;
   w=20;

   for(i=0;i<=10;i++) 
     XDrawLine( dpy, xwin, gc, 0, i*h, w*10, i*h );
   for(i=0;i<=10;i++) 
     XDrawLine( dpy, xwin, gc, i*w, 0, i*w, h*10 );
   
}

void batnav_xv_init( void ) {
   
   frame = (Frame)xv_create(NULL, FRAME,
			    XV_HEIGHT,300,
			    XV_WIDTH,400,
			    FRAME_LABEL,"Batalla Naval",
			    FRAME_SHOW_FOOTER,FALSE,
			    NULL);
   
   panel = xv_create( frame,PANEL,
		     PANEL_LAYOUT,PANEL_VERTICAL,
		     XV_WIDTH, 100,
		     NULL ); 
   
   canvas = xv_create( frame, CANVAS,
		      WIN_RIGHT_OF, canvas_paint_window( panel ),
		      XV_WIDTH, 300,
		      XV_HEIGHT, xv_get( panel , XV_HEIGHT ),
		      CANVAS_X_PAINT_WINDOW,TRUE,
		      CANVAS_REPAINT_PROC, canvas_repaint_proc,
		      NULL );
   
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Conectar",
	     PANEL_NOTIFY_PROC,nada,
	     NULL);
   
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Quit",
	     PANEL_NOTIFY_PROC,quit,
	     NULL);
   
   xv_main_loop(frame);

}

/*
 *                        main()
 */

void main( int argc, char *argv[] )
{
   
   xv_init( XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
   batnav_xv_init();
   
}
