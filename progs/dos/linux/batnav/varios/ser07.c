/*****************************************************************************/
/*                                                                           */
/*                          Batalla Naval v0.07                              */
/*                                                                           */
/*                               Servidor                                    */
/*                                                                           */
/*               por Ricardo Quesada y Sebastian Cativa Tolosa               */
/*                                                                           */
/*****************************************************************************/

/* DEFINES */
#define SERVERSION  "0.07"
#define CLIVERSION  ""
#define BATNAV_TCP_PORT 1066
#define MAXPLAYER 5
#define TRUE 0
#define FALSE 1

/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include "protocol.h"                  /* definicion del protocolo */
#include "criollo.h"                   /* mensajes en castellano */


/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */
typedef struct tabla_typ {
   int  x,y;
   char p[10][10];
} tabla,*tabla_ptr;

struct shmemoria {
   int nro_tot[MAXPLAYER];     /* 1 byte por jugador */
   tabla table[MAXPLAYER];
} *mimem;
#define SEGSIZE sizeof(struct shmemoria)

/*****************************************************************************/
/*                             FUNCIONES                                     */
/*****************************************************************************/

/*                            ALGORITMO
 *                  CHEQUEAR SI LOS BARCOS ESTAN BIEN
 */

/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco(int num_jug) {
   if(mimem->table[num_jug].x==9) {
     mimem->table[num_jug].x=0;
     mimem->table[num_jug].y++;
   }
   else mimem->table[num_jug].x++;
   if( mimem->table[num_jug].y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y,int num_jug) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) )
     return FALSE;
   if( mimem->table[num_jug].p[x][y]==1 )
     return TRUE;
   if( mimem->table[num_jug].p[x][y]==0 )
     return FALSE;
   printf("BTSERVER: BUG or someone is cheating in haybarco()\n");
   return FALSE;
}

/* return TRUE if table is OK .else return FALSE  */
int algoritmo(int num_jug)  {
   int i,xx,yy,barcos,subarco;
   int barquitos[4];
   
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   mimem->table[num_jug].x=0;
   mimem->table[num_jug].y=0;
   barcos=0;
   
   for(;;) {
      if(haybarco(mimem->table[num_jug].x,mimem->table[num_jug].y,num_jug)==FALSE) {
	 if(nextbarco(num_jug)==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       return FALSE;
	    }
	 }
      }
      else { 
	 if( haybarco(mimem->table[num_jug].x,mimem->table[num_jug].y,num_jug)==TRUE )  {
	    if( (haybarco(mimem->table[num_jug].x,mimem->table[num_jug].y-1,num_jug)==FALSE) && (haybarco(mimem->table[num_jug].x-1,mimem->table[num_jug].y,num_jug)==FALSE) ) {
	       subarco=1;
	       barcos++;
	       xx=mimem->table[num_jug].x;yy=mimem->table[num_jug].y;
	       
	       for(;;) {
		  if( (haybarco(mimem->table[num_jug].x-1,mimem->table[num_jug].y+1,num_jug)==TRUE) || (haybarco(mimem->table[num_jug].x+1,mimem->table[num_jug].y+1,num_jug)==TRUE) ) {
		     return FALSE;
		  }
		  if( (haybarco(mimem->table[num_jug].x+1,mimem->table[num_jug].y,num_jug)==FALSE) && (haybarco(mimem->table[num_jug].x,mimem->table[num_jug].y+1,num_jug)==FALSE) ) {
		     mimem->table[num_jug].x=xx;
		     mimem->table[num_jug].y=yy;          /* restauro pos */
		     barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
		     break;
		  }
		  else if( haybarco(mimem->table[num_jug].x+1,mimem->table[num_jug].y,num_jug) == TRUE ) {
		     subarco++;
		     barcos++;
		     mimem->table[num_jug].x++;
		  }          
		  else if( haybarco(mimem->table[num_jug].x,mimem->table[num_jug].y+1,num_jug) == TRUE ) {
		     mimem->table[num_jug].y++;
		     barcos++;
		     subarco++;
		  }
		  if( subarco > 4) {
		     return FALSE;
		  }
	       } /* for(;;) */
	    } /* if(haybarco(x,y-1)==FALSE) */
	 } /* if(haybarco(x,y)==TRUE) */
	 if(nextbarco(num_jug)==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       return FALSE;
	    }
	 } /* if nextbarco()==FALSE) */
      } /* else */
   } /* for(;;) */
} /* void algoritomo() */

/***************************************************************************
 *            FUNCIONES RELACIONADAS CON LAS TABLAS 
 ***************************************************************************/
int wtable( int jug,struct protocolo *proto ) {
   int i,x,y;
   for(i=0,x=0,y=0;i<100;i++) {
      mimem->table[jug].p[x][y]=proto->bnpmsg[i];
      x++;
      if(x==10) {
	 x=0;y++;
      }
   }
   return( algoritmo(jug));
}


/***************************************************************************** 
 *                           funciones TCP/IP
 ***************************************************************************/
size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
/*   short int i; */
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   strcpy(proto.bnpmsg,buf);
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN) ) ;
}

int play_batnav(int in,int out) {
   int i;
   int jugyo;                          /* jugador real de este proceso */
   char hostname[MSGMAXLEN];
   char outbuf[MSGMAXLEN];
   struct protocolo proto;
   
   for(i=0;i<MAXPLAYER;i++) {
      if(mimem->nro_tot[i]==0)
	   break;
   }
   printf("jugador nro %i\n",i+1);
   jugyo=i+1;
   mimem->nro_tot[i]=1;
   if(i>=5)  {
      bnwrite(out,outbuf,BNALL,jugyo,0,jugyo);
      return(1);           /* exit this process */
   }
   gethostname(hostname,MSGMAXLEN);
   bnwrite(out,outbuf,BNJUG,jugyo,0,jugyo);

   while( read(in,&proto,MAXLEN) > 0)  {
      switch (proto.bnptip0) {
       case BNREA:
	 break;
       case BNWRI:
	 if( wtable(jugyo-1,&proto)==TRUE)
	   bnwrite(out,outbuf,BNTOK,jugyo,0,jugyo);
	 else {
	    bnwrite(out,outbuf,BNTXX,jugyo,0,jugyo);
	 }
	 break;
       case BNJUG:
	 bnwrite(out,outbuf,BNJUG,jugyo,0,jugyo);
       case BNTST:
	 bnwrite(out,outbuf,BNTST,0,0,jugyo);
	 break;
       case BNEXT:
	 mimem->nro_tot[jugyo-1]=0;
	 return(2); /* exit this process */
       default:
	 break;
      }
   }
}

/*****************
 * main function *
 *****************/
void main() {
   int i,id,sock,fd,client_len;
   struct sockaddr_in server,client;
/*   struct shmid_ds shmbuf; */

   printf("%s",btmsg[MSG_SCOP]);

   id=shmget(IPC_PRIVATE,SEGSIZE,IPC_CREAT | 0644 );
   if( id < 0 )  {
      perror("btserver: shmget error:");
      exit(1);
   }
   
   mimem = (struct shmemoria *) shmat(id,0,0);
   if(mimem <= (struct shmemoria *) (0)) {
      perror("btserver: shmat error:");
      exit(2);
   }

   /* Set default parameters */
   for(i=0;i<MAXPLAYER;i++) {
      mimem->nro_tot[i]=0;
   }

   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("btserver: socket error:");
      exit(1);
   }
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=htonl(INADDR_ANY);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(bind(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("btserver: bind error:");
      exit(2);
   }
   listen(sock,5);
   signal(SIGCHLD,SIG_IGN);         /* evita zombies */
   while(1) {
      client_len = sizeof(client);
      if((fd=accept(sock,(struct sockaddr *)&client, &client_len))<0) {
	 perror("btserver: accept error:");
	 exit(3);
      }
      if(fork()==0) {
	 play_batnav(fd,fd);
	 exit(0);
      } 
      else
      	close (fd);
   }
}
