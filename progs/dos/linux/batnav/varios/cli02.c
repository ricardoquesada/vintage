/* a simple server */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include "protocolo.h"

#define BATVERSION  "0.2"
#define BATNAV_TCP_PORT 1066


void main(int argc, char *argv[]) {
   int sock,count;
   struct sockaddr_in server;
   struct hostent *host_info;
   struct protocolo proto;
   char inlin[MAXLEN],outline[MAXLEN];
   char *server_name;
   
   printf("Cliente para el Batalla Naval v"BATVERSION".\n");
   
   /* Get server name from command line */
   server_name=(argc>1) ? argv[1] : "localhost";
   
   /* Create socket */
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("creating stream socket");
      exit(1);
   }
   host_info = gethostbyname(server_name);
   if(host_info==NULL) {
      fprintf(stderr,"%s:unknown host: %s\n",argv[0],server_name);
      exit(2);
   }
   
   server.sin_family=host_info->h_addrtype;
   memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("connecting to server");
      exit(3);
   }
   printf("connected to server %s\n",server_name);
   
   if(fork()==0) {
      while( (count=read(sock,&proto,MAXLEN) ) > 0)  {
	 write(1,proto.bnpmsg,strlen(proto.bnpmsg));
      }
      exit(1);
   }
   else {
      while( (count=read(0,outline,MAXLEN))>0){
	 write(sock,outline,count);
      }
   }
}
