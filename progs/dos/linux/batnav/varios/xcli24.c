/*****************************************************************************/
/*                                                                           */
/*                             Batalla Naval                                 */
/*                                                                           */
/*                               Cliente X                                   */
/*                                                                           */
/*****************************************************************************/

/*
 *         Idea: Sebastian Cativa Tolosa y Ricardo Quesada
 *
 * Programacion:
 *            X: Ricardo Quesada
 *      ncurses: Sebastian Cativa Tolosa
 *       tcp/ip: Ricardo Quesada
 */


#include <X11/Xlib.h> 
#include <xview/xview.h> 
#include <xview/window.h> 
#include <xview/openmenu.h> 
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/xv_xrect.h>
#include <xview/frame.h>
#include <xview/cms.h>
#include <sspkg/canshell.h>
#include <sspkg/array.h>
#include <sspkg/drawobj.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>                    /* perhaps you may put <sys/signal.h> */
#include <errno.h>
#include <string.h>
#include "protocol.h"                  /* definicion del protocolo */
/*            de la parte x y otros */

/*
 *                DEFINES
 */ 
#define  WHITE  0  
#define  BLACK  1
#define  GREEN  2
#define  BLUE   3
#define  LBLUE  4
#define  YELLOW 5
#define  RED    6

#define BN_XVER "0.24"
#define LARGO 20
#define ANCHO 20

#define USRPLAY 0
#define USRCONN 1
#define USREXIT 2
#define USROUT  3
#define USRHIT  4
#define USRJUG  5
#define USRSTR  6
#define DEFAULT 0x328


/* 
 *                   VARIABLES GLOBALES
 */

int sock;
struct sockaddr_in server;
struct hostent *host_info;
char mitabla[10][10];          /* mi tabla de barcos */
char tutabla[10][10];          /* tabla para disparar */
char barquitos[4];             /* tama;os de los barcos 1 de 4, 2 de 3, etc. */
short int x,y;                 /* punteros de la tabla mia */
Drawrect barcos_ptr[100];      /* nombre de los objetos de cada barco */
Drawrect tusbarcos_ptr[100];   /*   "     "    del enemigo */

Frame frame,subframe1,subframe2;
Panel panel,panelcito,panelinfo,panelcit2;
Canvas canvas;
Menu menu;
Drawrect proto_drawrect;                 /* sspkg */
Array_tile array_tile;                   /* sspkg */
Rectobj_ops *rectobj_ops;                /* sspkg */
Canvas_shell shell,tushell;
Cms cms;
GC gc;
XGCValues gc_val;
Display *display;
XID xid;

unsigned long *colors;

Xv_singlecolor cms_colors[]= {
     { 255,255,255 },              /* white */
     {   0,  0,  0 },              /* black */
     {   0,255,  0 },              /* green */
     {   0,  0, 255},              /* blue */
     {  30,230, 250},              /* light blue */
     { 255,255,  0 },              /* yellow */
     { 255,  0,  0 },              /* red */
};

/* pasa a ser memoria compartida v0.18 */
struct usuario {
   char server_name[50];             /* server name */
   int port;                         /* server port */
   char nombre[40];                  /* user name */

   pid_t pid;                        /* child process id */
   int play;                         /* already playing ? */
   int numjug;                       /* number of player */
   int sigtype,sigvalue;             /* se usan para pasar parametros */
   int sigval0,sigval1;              /* para pasar mas parametros */
   char msg[MSGMAXLEN];              /* para pasar mensajes varios */
} usuario,*usuario_ptr;
#define SEGSIZE sizeof(struct usuario)


/* 
 *                        FUNCIONES
 */


/* 
 * funciones varias
 */

/* 2 funciones para escribir mensajes abajo */
void foot_right( char msg[] ) {
   xv_set(frame,
	  FRAME_RIGHT_FOOTER,msg,
	  NULL);
}
void foot_left( char msg[] ) {
   xv_set(frame,
	  FRAME_LEFT_FOOTER,msg,
	  NULL);
}
/* paint mi cell */
void pmicell(int x,int y,int color) {
   int i;
   i=10*y+x;
   (Drawrect) xv_set(barcos_ptr[i],
		     RECTOBJ_BG, color,
		     NULL );
}
/* paint tu cell */
void ptucell(int x,int y,int color) {
   int i;
   i=10*y+x;
   (Drawrect) xv_set(tusbarcos_ptr[i],
		     RECTOBJ_BG, color,
		     NULL );
}

void repaintmi( void )  {
   int x,y;
   for(x=0;x<10;x++) {
      for(y=0;y<10;y++) {
	 if( mitabla[x][y]==1 )
	   pmicell(x,y,BLUE);
	 else{	   
	    if( mitabla[x][y]==0 )
	      pmicell(x,y,WHITE);
	 }
      }
   }
}

int iwtable( char *dest) {
   int i,x,y;
   for(i=0,x=0,y=0;i<100;i++) {
      dest[i]=mitabla[x][y];
      x++;
      if(x==10) {
	 x=0;y++;
      }
   }
}

void sig_update( int stype ) {
   int x,y,color;
   switch( usuario_ptr->sigtype ) {
    case USRPLAY:
      if(usuario_ptr->sigval1==1) {
	 usuario_ptr->play=2;
	 xv_set(panelcit2,
		PANEL_LABEL_STRING,"Start",
		NULL); 
	 foot_right("Board is OK");
      }
      else {
	 if(usuario_ptr->sigval1==0) {
	    usuario_ptr->play=1;
	   foot_right("Check the board.");
	 }
      }
      usuario_ptr->sigtype=DEFAULT;
      break;
    case USRSTR:
      if(usuario_ptr->play==2) {
	 usuario_ptr->play=3;
	 xv_set(panelcit2,
		PANEL_LABEL_STRING,"Started",
		NULL);
      }
      usuario_ptr->sigtype=DEFAULT;
      break;
    case USROUT:
      foot_right("Out of connections");
      usuario_ptr->sigtype=DEFAULT;
      break;
    case USRHIT:
      if(usuario_ptr->play==3) {
	 x=usuario_ptr->sigval0;
	 y=usuario_ptr->sigval1;
	 color=usuario_ptr->sigvalue;
	 pmicell(x,y,color);
      }
      usuario_ptr->sigtype=DEFAULT;
      break;
    case USRJUG:
      foot_right(usuario_ptr->msg);
      usuario_ptr->sigtype=DEFAULT;
      break;
    default:
      foot_right("Someone is sending me SIGUSR1 signals");
   }
   signal( SIGUSR1, sig_update );
}
   
/*
 *             FUNCIONES DE COMUNICACIONES
 */

size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
   int i;
   
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   for(i=0;i<MSGMAXLEN;i++)
     proto.bnpmsg[i]=buf[i];
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN) ) ;
}

void proceso( void ) {
   struct protocolo proto;
   /*
    * INSTALL A PROCESS
    */
   if(fork()==0) {
      usuario_ptr->pid=getpid();
      while( read(sock,&proto,MAXLEN) >0) {
	 switch(proto.bnptip0) {
	  case BNTST:
	    break;
	  case BNTOK:
	    usuario_ptr->sigval1=1;
	    usuario_ptr->sigtype=USRPLAY;
	    kill(getppid(),SIGUSR1);
	    wait(0);
	    break;
	  case BNTXX:
	    usuario_ptr->sigval1=0;
	    usuario_ptr->sigtype=USRPLAY;
	    kill(getppid(),SIGUSR1);
	    wait(0);
	    break;
	  case BNJUG:
	    usuario_ptr->numjug=proto.bnptip1;
	    usuario_ptr->sigtype=USRJUG;
	    sprintf(usuario_ptr->msg,"Connected to server:%s",proto.bnpmsg);
	    kill(getppid(),SIGUSR1);
	    wait(0);
	    printf("Ud. es el usuario numero %i\n",usuario_ptr->numjug);
	    break;
	  case BNMSG:
	    break;
	  case BNALL:
	    usuario_ptr->sigtype=USROUT;
	    kill(getppid(),SIGUSR1);
	    wait(0);
	    break;
	  case BNHIT:
	    usuario_ptr->sigval0=proto.bnpmsg[0];
	    usuario_ptr->sigval1=proto.bnpmsg[1];
	    usuario_ptr->sigvalue=GREEN;
	    usuario_ptr->sigtype=USRHIT;
	    kill(getppid(),SIGUSR1);
	    wait(0);
	    break;
	  case BNSTR:
	    usuario_ptr->sigtype=USRSTR;
	    kill(getppid(),SIGUSR1);
	    wait(0);
	    break;
	  default:
	    break;
	 } 
      }
   } /* close fork */
}

int init_cliente( void ) {
   char outbuf[MSGMAXLEN];
   
   if(usuario_ptr->play==0) {                  /* estado de un boton */
      /* Create socket */
      sock=socket(AF_INET,SOCK_STREAM,0);
      if(sock <0) {
	 foot_right("ERROR: creating stream socket");
	 return(1);
      }
      host_info = gethostbyname(usuario.server_name);
      if(host_info==NULL) {
	 foot_right("ERROR: unknown host");
	 return(2);
      }
      server.sin_family=host_info->h_addrtype;
      memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
      server.sin_port=htons(usuario.port);
      
      
      if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
	 foot_right("ERROR: connecting to server");
	 return(2);
      }
      proceso(); /* instalar el proceso que escucha */
      bnwrite(sock,outbuf,BNJUG,0,0,0);       /* WHO AM I */

      usuario_ptr->play = 1;
      xv_set(panelcito,
	     PANEL_LABEL_STRING,"Disconnect",
	     NULL); 
      return(0);
   }
   else if(usuario_ptr->play!=0) {
      xv_set(panelcito,
	     PANEL_LABEL_STRING,"Connect",
	     NULL); 
      xv_set(panelcit2,
	     PANEL_LABEL_STRING,"Send Board",
	     NULL); 
      
      usuario_ptr->play=0;
      bnwrite(sock,outbuf,BNEXT,0,0,usuario_ptr->numjug);
      close(sock);
      kill( usuario_ptr->pid,SIGKILL);
      wait(0);
   }
}

/*
 *                            FUNCIONES X  y otras...
 */
void nada( void ) {
}

void about( Frame item, Event *event ) {
   xv_set( subframe1, XV_SHOW, TRUE, NULL );
}

void about_proc( Canvas c, Xv_window pw, Display *display, Window xid, Xv_xrectlist *xrects ) {
   int w,h;
   w = xv_get(pw,XV_WIDTH);
   h = xv_get(pw,XV_HEIGHT);
   
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, 0, w, h/3 );
   XSetForeground(display,gc,colors[WHITE]);
   XFillRectangle(display, xid, gc, 0, h/3, w, h/3*2 );
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, h/3*2, w,h );
   XSetForeground(display,gc,colors[YELLOW]);
   XFillArc(display, xid, gc, w/3 + w/3 * .15, h/3 + h/3 *.15, h/3 * .7, h/3 * .7, 0, 360*64 );
   
}
   
/*
void info( Frame item, Event *event ) {
   xv_set( subframe2, XV_SHOW, TRUE, NULL );
}
*/
/*                            ALGORITMO
 *                  CHEQUEAR SI LOS BARCOS ESTAN BIEN
 */

/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco() {
   if(x==9) { 
      x=0;
      y++;
   }
   else x++;
   if( y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) ) 
     return FALSE;
   if( mitabla[x][y]==1 ) 
     return TRUE;
   if( mitabla[x][y] == 0 ) 
     return FALSE;
   foot_right("ABNORMAL ERROR: please contact the authors.");
}

/* return TRUE if board is OK .else return FALSE  */
int algoritmo( void )  {
   int i,xx,yy,barcos,subarco;
   char temporal[60];
   
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   x=0;y=0;
   barcos=0;
   
   for(;;) {
      if(haybarco(x,y)==FALSE) {
	 if(nextbarco()==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
/*	       foot_right("OK"); */
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       sprintf(temporal,"ERROR: [4]=%i,[3]=%i,[2]=%i,[1]=%i",barquitos[3],barquitos[2],barquitos[1],barquitos[0]);
	       foot_right(temporal);
	       return FALSE;
	    }
	 }
      }
      else { 
	 if( haybarco(x,y)==TRUE )  {
	    if( (haybarco(x,y-1)==FALSE) && (haybarco(x-1,y)==FALSE) ) {
	       /* this is the first time i check this */
	       subarco=1;
	       barcos++;
	       xx=x;yy=y;
	       
	       for(;;) {
		  if( (haybarco(x-1,y+1)==TRUE) || (haybarco(x+1,y+1)==TRUE) ) {
		     sprintf(temporal,"Collision in (x,y)=%i,%i",x,y);
		     foot_right(temporal);
		     pmicell(x,y,RED);
		     return FALSE;
		  }
		  if( (haybarco(x+1,y)==FALSE) && (haybarco(x,y+1)==FALSE) ) {
		     x=xx;y=yy;          /* restauro pos */
		     barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
		     break;
		  }
		  else if( haybarco(x+1,y) == TRUE ) {
		     subarco++;
		     barcos++;
		     x++;
		  }          
		  else if( haybarco(x,y+1) == TRUE ) {
		     y++;
		     barcos++;
		     subarco++;
		  }
		  if( subarco > 4) {
		     sprintf(temporal,"Ship longer than 4 units in (x,y)=%i,%i",x,y);
		     foot_right(temporal);
		     pmicell(x,y,RED);
		     return FALSE;
		  }
	       } /* for(;;) */
	    } /* if(haybarco(x,y-1)==FALSE) */
	 } /* if(haybarco(x,y)==TRUE) */
	 if(nextbarco()==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       foot_right("OK");
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       sprintf(temporal,"ERROR: [4]=%i,[3]=%i,[2]=%i,[1]=%i",barquitos[3],barquitos[2],barquitos[1],barquitos[0]);
	       foot_right(temporal);
	       return FALSE;
	    }
	 } /* if nextbarco()==FALSE) */
      } /* else */
   } /* for(;;) */
} /* void algoritomo() */

/*
 *                     EMPEZAR LA PARTIDA
 */
int play( void ) {
   char outbuf[MSGMAXLEN];
   
   if(usuario_ptr->play==0) {         /* estado del boton SEND BOARD */
      foot_right("First establish a connection");
      return(FALSE);
   }
   else if( usuario_ptr->play==1)  {
      repaintmi();
      if( algoritmo()==FALSE) {
	 return(FALSE);
      }
      else if( algoritmo()==TRUE) {
	 iwtable( outbuf );
	 bnwrite(sock,outbuf,BNWRI,usuario_ptr->numjug,0,usuario_ptr->numjug);
	 return( TRUE );
      } /* close algorito==ok */
   }
   else if(usuario_ptr->play==2) {
      bnwrite(sock,outbuf,BNSTR,usuario_ptr->numjug,0,usuario_ptr->numjug);
   }
}
/*
 *                          QUIT
 */
void quit( ) {
   char outbuf[MSGMAXLEN];
   if(usuario_ptr->play!=0){             /* if connected then kill child */
      bnwrite(sock,outbuf,BNEXT,0,0,usuario_ptr->numjug);
      close(sock);
      kill( usuario_ptr->pid,SIGKILL);
      wait(0);
   }
   xv_destroy_safe( frame );
}

/*
 *                           BUTTON PRESSED IN MY BOARD
 */
int mitabla_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   
   int x,y;
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;

   if( usuario_ptr->play == 3 ) {
      foot_right("You cant modify while playing!");
      return(1);
   }

   switch( mitabla[x][y] )  {
    case 0:
      mitabla[x][y]=1;
      xv_set(r,
	     RECTOBJ_BG,BLUE,
	     NULL);
      break;
    case 1:
      mitabla[x][y]=0;
      xv_set(r,
	     RECTOBJ_BG,WHITE, 
	     NULL);
      break;
    default:
      printf("ACA NUNCA TENDRIA QUE LLEGAR: X=%i,Y=%i,S=%i\n",x,y,mitabla[x][y]);
      break;
   }
}
/*
 *                           BUTTON PRESSED IN ENEMY BOARD
 */
int tutabla_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   int x,y;
   char outbuf[MSGMAXLEN];
   
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;

   if( usuario_ptr->play != 3 ) {
      foot_right("Not started yet!");
      return(1);
   }
   switch( tutabla[x][y] )  {
    case 0:
    case 1:
      tutabla[x][y]=1;
      xv_set(r,
	     RECTOBJ_BG,RED,
	     NULL);
      outbuf[0]=x;
      outbuf[1]=y;
      bnwrite(sock,outbuf,BNHIT,0,0,usuario_ptr->numjug);
      break;
/*    case 1:
      foot_right("You are a stupid guy!");
      break; */
    default:
      printf("ACA NUNCA TENDRIA QUE LLEGAR: X=%i,Y=%i,S=%i\n",x,y,tutabla[x][y]);
      break;
   }
}

/*
 *                               INICIALIZAR VARIABLES
 */
void datos_init( void ) {
   int i,j,id;

/* Install shared memory */
   if( (id=shmget(IPC_PRIVATE,SEGSIZE,IPC_CREAT | 0644 )) < 0 ) {
      perror("btclient: shmget error:");
      exit(1);
   }
   usuario_ptr = (struct usuario * ) shmat(id,0,0);
   if(usuario_ptr <= (struct usuario *) (0)) {
      perror("btclient: shmat error:");
      exit(2);
   }
   
   usuario_ptr->play = 0;
   for(i=0;i<10;i++) {          /* clean tabla */
      for(j=0;j<10;j++)
      	mitabla[i][j]=0;
   } 

}

/*
 *                                 INICIALIZAR X
 */
void batnav_xv_init( void ) {
   
   int i;
   
   /* crea los colores */
   cms = (Cms) xv_create( XV_NULL,CMS,
			 CMS_SIZE,7,
			 CMS_TYPE, XV_STATIC_CMS,
			 CMS_COLORS,cms_colors,
			 NULL );
   /* ventana principal */
   frame = (Frame) xv_create(NULL, FRAME,
			     XV_HEIGHT,230,
			     XV_WIDTH,800,
			     FRAME_LABEL,"Batalla Naval v"BN_XVER,
			     FRAME_SHOW_FOOTER,TRUE,
			     FRAME_LEFT_FOOTER,"Batalla Naval",
			     FRAME_RIGHT_FOOTER,"(c) 1995",
			     NULL);
   

/* panel para los botones quit,play,etc */
   panel = xv_create( frame,PANEL,
		     PANEL_LAYOUT,PANEL_VERTICAL,
		     XV_WIDTH, 100,
		     NULL ); 

/* shell para mi tabla */
   shell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				    WIN_CMS,cms,
				    NULL );
   xv_create( shell, DRAWTEXT,
	     DRAWTEXT_STRING_PTR,"My ships",
	     XV_WIDTH,ANCHO*10,
	     DRAWTEXT_JUSTIFY,DRAWTEXT_JUSTIFY_CENTER,
	     RECTOBJ_SELECTABLE,FALSE,
	     NULL); 
   array_tile = (Array_tile) xv_create( shell, ARRAY_TILE,
				       XV_Y,20, 
				       XV_WIDTH,ANCHO*10,
				       XV_HEIGHT,LARGO*10,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 1,
				       ARRAY_TILE_COLUMN_GAP, 1,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, mitabla_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);
   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   for(i=0;i<100;i++) {    /* 10 * 10 */
      barcos_ptr[i] = (Drawrect) xv_create( array_tile, DRAWRECT,
					   RECTOBJ_OPS, rectobj_ops,
					   XV_WIDTH,ANCHO,
					   XV_HEIGHT,LARGO,
					   DRAWRECT_BORDER2,1,
					   DRAWOBJ_FILLED,TRUE,
					   RECTOBJ_BG2, WHITE,
					   RECTOBJ_BG, WHITE,
					   RECTOBJ_FG, BLACK,
					   NULL );
   }
   xv_set( shell, 
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) + 20 ,
	  NULL ); 

/* shell para enemy tabla */
   tushell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				      WIN_CMS,cms,
				      NULL );
   xv_create( tushell, DRAWTEXT,
	     DRAWTEXT_STRING_PTR,"Enemy ships",
	     XV_WIDTH,ANCHO*10,
	     DRAWTEXT_JUSTIFY,DRAWTEXT_JUSTIFY_CENTER,
	     RECTOBJ_SELECTABLE,FALSE,
	     NULL); 
   array_tile = (Array_tile) xv_create( tushell, ARRAY_TILE,
				       XV_Y,20, 
				       XV_WIDTH,ANCHO*10,
				       XV_HEIGHT,LARGO*10,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 1,
				       ARRAY_TILE_COLUMN_GAP, 1,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, tutabla_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);
   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   for(i=0;i<100;i++) {    /* 10 * 10 */
      tusbarcos_ptr[i] = (Drawrect) xv_create( array_tile, DRAWRECT,
					      RECTOBJ_OPS, rectobj_ops,
					      XV_WIDTH,ANCHO,
					      XV_HEIGHT,LARGO,
					      DRAWRECT_BORDER2,1,
					      DRAWOBJ_FILLED,TRUE,
					      RECTOBJ_BG2, WHITE,
					      RECTOBJ_BG, WHITE,
					      RECTOBJ_FG, BLACK,
					      NULL );
   }
   xv_set( tushell, 
	  XV_X,320,
	  XV_Y,0,
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) + 20 ,
	  NULL ); 

   
   /* creacion de botones */
   panelcito = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING,"Connect",
				 PANEL_NOTIFY_PROC,init_cliente,
				 NULL);
   panelcit2 = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING,"Send Board",
				 PANEL_NOTIFY_PROC,play,
				 NULL);
   menu = (Menu) xv_create (NULL, MENU,
			    MENU_GEN_PIN_WINDOW, frame, "Players",
			    MENU_NOTIFY_PROC, nada,
			    MENU_STRINGS, "Player 1","Player 2","Player 3","Player 4","Player 5",NULL,
			    NULL);
   xv_create( panel, PANEL_BUTTON,
 	     PANEL_LABEL_STRING,"Players",
 	     PANEL_NOTIFY_PROC,nada,
	     PANEL_ITEM_MENU,menu,
	     NULL);
/*   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Info",
	     PANEL_NOTIFY_PROC,info,
	     NULL); */
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"About",
	     PANEL_NOTIFY_PROC,about,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Quit",
	     PANEL_NOTIFY_PROC,quit,
	     NULL);

   
/* 
 * todo lo referente a about
 */
   subframe1=(Frame) xv_create(frame,FRAME,
			       XV_HEIGHT,300,
			       XV_WIDTH,300, 
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) 1995",
			       FRAME_LABEL, "About",
			       NULL);
   canvas = xv_create( subframe1,CANVAS,
		      CANVAS_X_PAINT_WINDOW,TRUE,
		      XV_VISUAL_CLASS, PseudoColor,
		      WIN_CMS,cms,
		      CANVAS_REPAINT_PROC,about_proc,
		      NULL );
   display = (Display *) xv_get( subframe1, XV_DISPLAY);
   xid = (XID) xv_get(  canvas_paint_window( canvas ) ,XV_XID );
   gc = XCreateGC( display, xid, NULL , &gc_val );
   colors = (unsigned long*) xv_get( canvas,WIN_X_COLOR_INDICES);

/* 
 * todo lo referente a info
 */
/*   subframe2=(Frame) xv_create(frame,FRAME_CMD,
			       XV_HEIGHT,300,
			       XV_WIDTH,300,
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) 1995",
			       FRAME_LABEL, "Information",
			       NULL);
   panelinfo = (Panel) xv_get( subframe2,FRAME_CMD_PANEL);
   (void) xv_create( panelinfo, PANEL_BUTTON,
		    XV_X,50,
		    XV_Y,150,
		    PANEL_LABEL_STRING,"Cancel",
		    PANEL_NOTIFY_PROC,nada,
		    NULL);
   (void) xv_create( panelinfo, PANEL_BUTTON,
		    XV_X,150,
		    XV_Y,150,
		    PANEL_LABEL_STRING,"OK",
		    PANEL_NOTIFY_PROC,nada,
		    NULL);
   (Panel_item) xv_create( panelinfo, PANEL_TEXT,
   		          XV_X,20,
                          XV_Y,45,
			  PANEL_VALUE,usuario.server_name,
			  PANEL_LABEL_STRING,"Server name:",
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  PANEL_VALUE_DISPLAY_LENGTH,20,
			  NULL);
   (Panel_item) xv_create( panelinfo, PANEL_NUMERIC_TEXT,
   		          XV_X,20,
                          XV_Y,70,
			  PANEL_LABEL_STRING,"Server port:",
			  PANEL_VALUE,usuario.port,
			  PANEL_MAX_VALUE,65535,
			  PANEL_MIN_VALUE,0,
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  NULL);
(Panel_item) xv_create( panelinfo, PANEL_TEXT,
   		          XV_X,20,
                          XV_Y,95,
			  PANEL_VALUE,usuario.nombre,
			  PANEL_LABEL_STRING,"Your name:",
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  PANEL_VALUE_DISPLAY_LENGTH,20,
			  NULL);
*/   
   /*
    * startup x, and run
    */
   window_fit( frame ); 

}

/*
 *                        main()
 */

void main( int argc, char *argv[] )
{
   xv_init( XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
   if(argc>1) strcpy( usuario.server_name,argv[1]);
   else strcpy( usuario.server_name,"localhost");
   if(argc>2) usuario.port=atoi(argv[2]);
   else usuario.port=1066;
   strcpy( usuario.nombre,getlogin());
   datos_init();                  /* inicializa variables */
   signal(SIGCHLD, SIG_IGN);      /* evita zombies */
   signal(SIGUSR1, sig_update );  /* redirecciona la senal SIGUSR1 */
   batnav_xv_init();              /* inicializa entorno y juego */
/*   init_cliente(); */
   /* establecer coneccion ahora */
   xv_main_loop(frame);
   printf("Hola chicos llegue al final\n");
}
