/*****************************************************************************/
/*                                                                           */
/*                          Batalla Naval v0.04                              */
/*                                                                           */
/*                               Servidor                                    */
/*                                                                           */
/*               por Ricardo Quesada y Sebastian Cativa Tolosa               */
/*                                                                           */
/*****************************************************************************/

/* DEFINES */
#define SERVERSION  "0.04"
#define CLIVERSION  ""
#define BATNAV_TCP_PORT 1066
#define MAXPLAYER 5

/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include "protocol.h"                  /* definicion del protocolo */
#include "criollo.h"                   /* mensajes en castellano */


/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */
typedef struct tabla_typ {
   char p[10][10];
} tabla,*tabla_ptr;

struct shmemoria {
   int nro_jug;                 /* cantidad de jugadores contectados */
   char nro_tot[MAXPLAYER];     /* 1 byte por jugador */
   tabla table[MAXPLAYER]; 
} *mimem;
#define SEGSIZE sizeof(struct shmemoria)

/*************/
/* FUNCIONES */
/*************/
size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
/*   short int i; */
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   strcpy(proto.bnpmsg,buf);
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN) ) ;
}

void play_batnav(int in,int out) {
   char jugyo;                          /* jugador real de este proceso */
   char hostname[MSGMAXLEN];
   char outbuf[MSGMAXLEN],nombre[MSGMAXLEN];
   int pid;
   struct protocolo proto;
   
   (jugyo) = mimem-> nro_jug++;           /* la info la saca de shared memory */
   gethostname(hostname,MSGMAXLEN);
   
   sprintf(outbuf,"%s %s\n\n",btmsg[MSG_PLAY],hostname);
   bnwrite(out,outbuf,BNMSG,0,0,jugyo);

   sprintf(outbuf,"%s %i.\n",btmsg[MSG_JUGA],(jugyo) );
   bnwrite(out,outbuf,BNMSG,0,0,jugyo);
   
   if(mimem->nro_jug==2)  {
      sprintf(outbuf,btmsg[MSG_WAIT]);
      bnwrite(out,outbuf,BNMSG,0,0,jugyo);
      bnwrite(out,outbuf,BN1ST,0,0,jugyo);
   }
   do{}while(mimem->nro_jug==2);
 
   while( read(in,&proto,MAXLEN) > 0)  {
      switch (proto.bnptip0) {
       case BNREA:
	 break;
       case BNWRI:
	 break;
       case BNTST:
	 bnwrite(out,outbuf,BNTST,0,0,jugyo);
	 break;
       default:
	 break;
      }
   }
}

/*****************
 * main function *
 *****************/
void main() {
   int id,sock,fd,client_len;
   struct sockaddr_in server,client;
/*   struct shmid_ds shmbuf; */

   printf("%s",btmsg[MSG_SCOP]);

   id=shmget(IPC_PRIVATE,SEGSIZE,IPC_CREAT | 0644 );
   if( id < 0 )  {
      perror("btserver: shmget error:");
      exit(1);
   }
   
   mimem = (struct shmemoria *) shmat(id,0,0);
   if(mimem <= (struct shmemoria *) (0)) {
      perror("btserver: shmat error:");
      exit(2);
   }

   /* Set default parameters */
   mimem->nro_jug=1;
   
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("btserver: socket error:");
      exit(1);
   }
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=htonl(INADDR_ANY);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(bind(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("btserver: bind error:");
      exit(2);
   }
   listen(sock,5);
   signal(SIGCHLD,SIG_IGN);
   while(1) {
      client_len = sizeof(client);
      if((fd=accept(sock,(struct sockaddr *)&client, &client_len))<0) {
	 perror("btserver: accept error:");
	 exit(3);
      }
      if(fork()==0) {
	 play_batnav(fd,fd);
	 exit(0);
      } else
      	close (fd);
   }
}
