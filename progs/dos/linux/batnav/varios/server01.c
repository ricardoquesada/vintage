/* a simple server */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>

#define BATVERSION  "0.01a"
#define HANGMAN_TCP_PORT 1066
#define MAXLEN 80

void play_batnav(int in,int out) {
   char hostname[MAXLEN];
   char outbuf[MAXLEN],nombre[MAXLEN];
   gethostname(hostname,MAXLEN);
   sprintf(outbuf,"Playing Batalla Naval v"BATVERSION" on host %s:\n\n",hostname);
   write(out,outbuf,strlen(outbuf));
   sprintf(outbuf,"Por favor, ingrese su nombre\n");
   write(out,outbuf,strlen(outbuf));
   while( read(in,nombre,MAXLEN) <0) {
      if(errno != EINTR)
      	exit(4);
      printf("re-starting the read\n");
   }
   strcat(nombre,":lo siento, nombre incorrecto.\n");
   sprintf(outbuf,nombre);
   write(out,outbuf,strlen(outbuf));
   
}

void main() {
   int sock,fd,client_len;
   struct sockaddr_in server,client;
   printf("Servidor para el Batalla Naval v"BATVERSION".\n"
	  "Por Ricardo Quesada. email: rquesada@dc.uba.ar\n");
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("creating stream socket");
      exit(1);
   }
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=htonl(INADDR_ANY);
   server.sin_port=htons(HANGMAN_TCP_PORT);
   if(bind(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("binding socket");
      exit(2);
   }
   listen(sock,5);
   signal(SIGCHLD,SIG_IGN);
   while(1) {
      client_len = sizeof(client);
      if((fd=accept(sock,(struct sockaddr *)&client, &client_len))<0) {
	 perror("accepting connection");
	 exit(3);
      }
      if(fork()==0) {
	 play_batnav(fd,fd);
	 exit(0);
      } else
      	close (fd);
   }
}
