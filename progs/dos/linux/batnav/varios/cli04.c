/*****************************************************************************/
/*                                                                           */
/*                          Batalla Naval v0.04                              */
/*                                                                           */
/*                               Cliente                                     */
/*                                                                           */
/*               por Ricardo Quesada y Sebastian Cativa Tolosa               */
/*                                                                           */
/*****************************************************************************/


/* defines */
#define SERVERSION  ""
#define CLIVERSION  "0.4"
#define BATNAV_TCP_PORT 1066

/* includes */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include "protocol.h"                  /* definicion del protocolo */
#include "criollo.h"                   /* mensajes en castellano */


/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */
typedef struct tabla_typ {
   char p[10][10];
} tabla,*tabla_ptr;
int sock;
struct sockaddr_in server;
struct hostent *host_info;
char *server_name;

/*************/
/* FUNCIONES */
/*************/
size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   strcpy(proto.bnpmsg,buf);
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN) ) ;
}

void init_cliente( void ) {
   
   /* Create socket */
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("btclient: creating stream socket");
      exit(1);
   }
   host_info = gethostbyname(server_name);
   if(host_info==NULL) {
      fprintf(stderr,"btclient: unknown host: %s\n",server_name);
      exit(2);
   }
   
   server.sin_family=host_info->h_addrtype;
   memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("btclient: connecting to server");
      exit(3);
   }   
}

void cliente( int in, int out ) {
   struct protocolo proto;
   int pid;                              /* process id */
   char jugyo=0;                         /* que numero de jugador soy */
   char outbuf[MAXLEN];
   
   if( (pid=fork())==0) {
      printf("pid=%i\n",pid);
      while( read(in,&proto,MAXLEN) > 0)  {
	 switch (proto.bnptip0) {
	  case BNMSG:
	    write(1,proto.bnpmsg,strlen(proto.bnpmsg));
	    break;
	  case BNEXT:
	    /* debo salir del cliente */
	    break;
	  case BN1ST:
	    /* primer jugador */
	  case BNREA:
	    /* me informa de la planilla que le pedi */
	    break;
	  case BNJUG:
	    /* me dice que jugador soy */
	    jugyo=proto.jugador;
	    break;
	  case BNTST:
	    printf("me escribistes una a negro \n");
	  default:
	    break;
	 }
      }
      exit(1);
   }
   else {
      for(;;) {
	 switch( getchar() ) {
	  case 'a':
	    bnwrite(out,outbuf,BNTST,0,0,jugyo);
	    break;
	  case 'b':
	    bnwrite(out,outbuf,BNREA,0,0,jugyo);
	    break;
	  default:
	    break;
	 }
      }
   }
}

void main(int argc, char *argv[]) {
   
   printf("%s",btmsg[MSG_CCOP]);
   
   /* Get server name from command line */
   server_name=(argc>1) ? argv[1] : "localhost";

   init_cliente();

   printf("Conectado al server %s\n",server_name);
   
   cliente( sock, sock );

}
