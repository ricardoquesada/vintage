/* a simple server */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>

#define BATVERSION  "0.01a"
#define HANGMAN_TCP_PORT 1066
#define MAXLEN 80


void main(int argc, char *argv[]) {
   int sock,count;
   struct sockaddr_in server;
   struct hostent *host_info;
   char inlin[MAXLEN],outline[MAXLEN];
   char *server_name;
   
   printf("Cliente para el Batalla Naval v"BATVERSION".\n"
	  "Por Ricardo Quesada. email: rquesada@dc.uba.ar\n");
   /* Get server name from command line */
   server_name=(argc>1) ? argv[1] : "localhost";
   
   /* Create socket */
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("creating stream socket");
      exit(1);
   }
   host_info = gethostbyname(server_name);
   if(host_info==NULL) {
      fprintf(stderr,"%s:unknown host: %s\n",argv[0],server_name);
      exit(2);
   }
   
   server.sin_family=host_info->h_addrtype;
   memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
   server.sin_port=htons(HANGMAN_TCP_PORT);
   if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("connecting to server");
      exit(3);
   }
   printf("connected to server %s\n",server_name);
   while((count=read(sock,inlin,MAXLEN))>0) {
      write(1,inlin,count);
      count=read(0,outline,MAXLEN);
      write(sock,outline,count);
   }
}
