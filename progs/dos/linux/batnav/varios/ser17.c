/*****************************************************************************/
/*                                                                           */
/*                            Batalla Naval                                  */
/*                                                                           */
/*                               Servidor                                    */
/*                          por Ricardo Quesada                              */
/*                                                                           */
/* The total game was programmed by:                                         *
 *            X client : by Ricardo Quesada                                  *
 *      ncurses client : by Sebastian Cativa Tolosa                          *
 *              server : by Ricardo Quesada                                  *
 *****************************************************************************/


/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "protocol.h"                  /* definicion del protocolo */

/* DEFINES */
#define SERVERSION  "0.17"
#define BATNAV_TCP_PORT 1066
#define MAXPLAYER 5

#define TRUE   0                      /* perhaps this is the other way */
#define FALSE  1                      /* but i dont care */

#define WHITE  0
#define BLACK  1
#define GREEN  2
#define BLUE   3
#define LBLUE  4
#define YELLOW 5
#define RED    6

#define AGUA   -2
#define NOBARCO 0                      /* PARA FUTURA IMPLEMENTACION */
#define BARCO   1 
#define TOCADO  2
#define HUNDIDO 3


/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */

typedef struct tabla_typ {
   int  x,y;
   char p[10][10];
} tabla,*tabla_ptr;

struct datos {
   int nro_tot[MAXPLAYER];     /* 1 int por jugador */
   int nro_fd[MAXPLAYER];      /* 1 file descriptor por jugador */
   int hits[MAXPLAYER];        /* how many hits has a player received */
   tabla table[MAXPLAYER];
} mimem;
int temp;                       /* variable temporal */
char tempbuf[MSGMAXLEN];        /* varible general */
/*****************************************************************************
 *                                   FUNCIONES
 *****************************************************************************/

/***************************************************************************** 
 *                           funciones TCP/IP
 ***************************************************************************/
size_t bnwrite(int fde,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
   int i;
   
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   for(i=0;i<MSGMAXLEN;i++)
     proto.bnpmsg[i]=buf[i];
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fde,&proto,MAXLEN) ) ;
}
   
/***************************************************************************** 
 *                              CHEQUEO DE ALGORITOMO
 ****************************************************************************/
/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco(int num_jug) {
   if(mimem.table[num_jug].x==9) {
     mimem.table[num_jug].x=0;
     mimem.table[num_jug].y++;
   }
   else mimem.table[num_jug].x++;
   if( mimem.table[num_jug].y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y,int num_jug) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) )
     return FALSE;
   if( mimem.table[num_jug].p[x][y]==1 )
     return TRUE;
   if( mimem.table[num_jug].p[x][y]==0 )
     return FALSE;
   printf("BTSERVER: BUG or someone is cheating in haybarco()\n");
   return FALSE;
}

/* return TRUE if table is OK .else return FALSE  */
int algoritmo(int num_jug)  {
   int i,xx,yy,barcos,subarco;
   int barquitos[4];
   
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   mimem.table[num_jug].x=0;
   mimem.table[num_jug].y=0;
   barcos=0;
   
   for(;;) {
      if(haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y,num_jug)==FALSE) {
	 if(nextbarco(num_jug)==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       return FALSE;
	    }
	 }
      }
      else { 
	 if( haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y,num_jug)==TRUE )  {
	    if( (haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y-1,num_jug)==FALSE) && (haybarco(mimem.table[num_jug].x-1,mimem.table[num_jug].y,num_jug)==FALSE) ) {
	       subarco=1;
	       barcos++;
	       xx=mimem.table[num_jug].x;yy=mimem.table[num_jug].y;
	       
	       for(;;) {
		  if( (haybarco(mimem.table[num_jug].x-1,mimem.table[num_jug].y+1,num_jug)==TRUE) || (haybarco(mimem.table[num_jug].x+1,mimem.table[num_jug].y+1,num_jug)==TRUE) ) {
		     return FALSE;
		  }
		  if( (haybarco(mimem.table[num_jug].x+1,mimem.table[num_jug].y,num_jug)==FALSE) && (haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y+1,num_jug)==FALSE) ) {
		     mimem.table[num_jug].x=xx;
		     mimem.table[num_jug].y=yy;          /* restauro pos */
		     barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
		     break;
		  }
		  else if( haybarco(mimem.table[num_jug].x+1,mimem.table[num_jug].y,num_jug) == TRUE ) {
		     subarco++;
		     barcos++;
		     mimem.table[num_jug].x++;
		  }          
		  else if( haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y+1,num_jug) == TRUE ) {
		     mimem.table[num_jug].y++;
		     barcos++;
		     subarco++;
		  }
		  if( subarco > 4) {
		     return FALSE;
		  }
	       } /* for(;;) */
	    } /* if(haybarco(x,y-1)==FALSE) */
	 } /* if(haybarco(x,y)==TRUE) */
	 if(nextbarco(num_jug)==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       return FALSE;
	    }
	 } /* if nextbarco()==FALSE) */
      } /* else */
   } /* for(;;) */
} /* void algoritomo() */
 /***************************************************************************
 *            FUNCIONES RELACIONADAS CON LAS TABLAS 
 ***************************************************************************/
/* write to  board */
int wtable( int jug,struct protocolo *proto ) {
   int i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) {
      mimem.table[jug].p[x][y]=proto->bnpmsg[i];
      x++;
      if(x>=10) {
	 x=0;
	 y++;
      }
   }
   return( algoritmo(jug));
}
/* read from board */
int rtable( char outbuf[],int jug,char sup) {
   int i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) {
      if(sup==BNSUP)
      	outbuf[i]=mimem.table[jug].p[x][y];
      else {
	 if(mimem.table[jug].p[x][y]==1)
	   outbuf[i]=0;
	 else
	   outbuf[i]=mimem.table[jug].p[x][y];
      }
      x++;
      if(x>=10) {
	 x=0;
	 y++;
      }
   }
}
/* clean board */
int ctable( int jug) {
   int i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) {
      mimem.table[jug].p[x][y]=0;
      x++;
      if(x>=10) {
	 x=0;
	 y++;
      }
   }
}
/*****************************************************************************
 *                EL BARCO ESTA HUNDIDO ? y todo tipo de funciones
 *****************************************************************************/
int quebarco( int i,int x, int y) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) )
     return 0;
   return( mimem.table[i].p[x][y]);
}
/* 1 funciones recursiva para chequear si esta hundido */
int goudlr( int i,int x,int y,int xx,int yy) {
   if (quebarco(i,x,y)==2)
     goudlr(i,x+xx,y+yy,xx,yy);
   else if(quebarco(i,x,y)==1)
     temp+=1;
}
/* 1 funcion recursiva para pintar al barco hundido */
int pgoudlr( int i,int x,int y,int xx,int yy) {
   if(quebarco(i,x,y)==2) {
      mimem.table[i].p[x][y]=3;              /* 3 means hundido (sunk) */
      tempbuf[0]=(char)x;
      tempbuf[1]=(char)y;
      tempbuf[2]=BLACK;                       /* hundido */
      bnwrite(mimem.nro_fd[i],tempbuf,BNHIT,0,0,i+1);
      pgoudlr(i,x+xx,y+yy,xx,yy);
   }
}

/* i assume that quebarco(i,x,y)==2 o 1 */
int eshundido(int i,int x,int y) {
   int xx,yy;                           /* direccion a buscar */
   if(quebarco(i,x,y) <= 0)             /* IDIOTA, COMO VA A SER HUNDIDO */
     return FALSE;
   if(quebarco(i,x,y)==3)               /* estaba hundido de antes idiota */
     return TRUE;
   mimem.table[i].p[x][y]=2;            /* tocado por ahora*/
   temp=0;
   xx=-1;yy=0;goudlr(i,x,y,xx,yy);
   xx=0;yy=-1;goudlr(i,x,y,xx,yy);
   xx=1;yy=0;goudlr(i,x,y,xx,yy);
   xx=0;yy=1;goudlr(i,x,y,xx,yy);

   if(temp==0) {
      xx=-1;yy=0;pgoudlr(i,x,y,xx,yy);
      xx=0;yy=-1;pgoudlr(i,x,y-1,xx,yy);
      xx=1;yy=0;pgoudlr(i,x+1,y,xx,yy);
      xx=0;yy=1;pgoudlr(i,x,y+1,xx,yy);
      mimem.hits[i]++;
      if(mimem.hits[i]==10)  {
	 bnwrite(mimem.nro_fd[i],tempbuf,BNLOS,0,0,i+1);
	 mimem.nro_tot[i]=0;
	 mimem.hits[i]=0;
	 ctable(i);
	 /* close(mimem.nro_fd[i]); */
      }
      return TRUE;
   }
   return FALSE;
}
/***************************************************************************** 
 *                           funciones CONTROL DE CHILD Y JUEGO
 ***************************************************************************/
int play_batnav( int fd) {
   int i,j,x,y;
   char outbuf[MSGMAXLEN];
   struct protocolo proto;

   if( read(fd,&proto,MAXLEN) < 0)
     return(-1);
   j=proto.jugador-1;

   switch (proto.bnptip0) {
    case BNREA:
      x=proto.bnptip1-1;               /* which player do you want to read */
      rtable(outbuf,x,proto.bnptip2);
      bnwrite(fd,outbuf,BNREA,x,0,j);       /* i'm sending the board */
      break;
    case BNWRI:
      for(i=0;i<MAXPLAYER;i++) {
	 if( mimem.nro_tot[i]>=3)
	   break;
      }
      if(i!=MAXPLAYER)                     /* im sorry but people is playing */
      	bnwrite(fd,outbuf,BNWAI,0,0,0);    
      else {
	 if( wtable(j,&proto)==TRUE)  {
	    mimem.nro_tot[j]=2;                    /* send board ok */
	    outbuf[0]=1;
	 }
	 else {
	    mimem.nro_tot[j]=1;                     /* send board not ok */
	    outbuf[0]=0;
	 }
	 bnwrite(fd,outbuf,BNWRI,0,0,0);
      }
      break;
    case BNJUG:
      for(i=0;i<MAXPLAYER;i++) {
	 if(mimem.nro_tot[i]==0)
	   break;
      }
      if(i==MAXPLAYER)                          /* im sorry, server is full*/
      	bnwrite(fd,outbuf,BNALL,i+1,0,i+1);
      else  {
	 mimem.nro_tot[i]=1;                     /* connected */
	 mimem.nro_fd[i]=fd;
	 gethostname(outbuf,MSGMAXLEN);
	 bnwrite(fd,outbuf,BNJUG,i+1,0,i+1);
      }
      break;
    case BNTST:
      bnwrite(fd,outbuf,BNTST,0,0,0);
      break;
    case BNEXT:
      mimem.nro_tot[j]=0;
      mimem.hits[j]=0;
      ctable(j);  /* clear board */
      mimem.nro_tot[j]=0;
      close(mimem.nro_fd[j]);
      break;
    case BNHIT:
      if(mimem.nro_tot[j]==4) {                           /* is it my turn */
	 x=outbuf[0]=proto.bnpmsg[0];
	 y=outbuf[1]=proto.bnpmsg[1];
	 for(i=0;i<MAXPLAYER;i++) {
	    if(mimem.nro_tot[i]>=3){                      /* started ?*/
	       if(i!=j) {                                   /* dont shoot shooter */
		  if(mimem.table[i].p[x][y]>=1) {
		     if(eshundido(i,x,y)==FALSE) {            /* */
			outbuf[2]=RED;
			mimem.table[i].p[x][y]=2;              /* tocado */
			bnwrite(mimem.nro_fd[i],outbuf,BNHIT,0,0,i+1);
		     }
		  }
		  else if(mimem.table[i].p[x][y]<=0) {
		     outbuf[2]=GREEN;
		     mimem.table[i].p[x][y]=-1;              /* agua */
		     bnwrite(mimem.nro_fd[i],outbuf,BNHIT,0,0,i+1);
		  }
	       }
	    }
	 }
	 /* apartir del siguiente jugador */
	 mimem.nro_tot[j]=3;                       /* it is no more your turn */
	 for(i=proto.jugador;i<MAXPLAYER;i++) {
	    if(mimem.nro_tot[i]==3)
	      break;
	 }
	 if(i==MAXPLAYER) {                            /* llego al ultimo */
	    for(i=0;i<MAXPLAYER;i++) {
	       if(mimem.nro_tot[i]==3)
	       	 break;
	    }
	 }
	 if( (i==j) || (i==MAXPLAYER)) {     /* si el turno es para el mismo jugador gano */
	    bnwrite(mimem.nro_fd[i],outbuf,BNWIN,0,0,i+1);
	    mimem.nro_tot[i]=0;
	    mimem.hits[i]=0;
	    ctable(i);  /* clear board */
        /*  close(mimem.nro_fd[i]); */
	 }
	 else {
	    mimem.nro_tot[i]=4;                      /* siguiente turno */
	    bnwrite(mimem.nro_fd[i],outbuf,BNTRN,0,0,i+1);
	 }
      }
      break;
    case BNSTR:
      for(i=0;i<MAXPLAYER;i++) {
	 if(mimem.nro_fd[i]>=2) {
	    bnwrite(mimem.nro_fd[i],outbuf,BNSTR,0,0,i+1);
	    mimem.nro_tot[i]=3;                        /* started */
	 }
      }
      for(i=0;i<MAXPLAYER;i++) {
	 if(mimem.nro_tot[i]==3) {
	    bnwrite(mimem.nro_fd[i],outbuf,BNTRN,0,0,i+1);
	    mimem.nro_tot[i]=4;
	    break;                                       /* started */
	 }
      }
      break;
    default:
      break;
   }
}


/*****************
 * main function *
 *****************/
void main() {
   FILE **stream;
   fd_set test_set, ready_set;
   int sock,fd,client_len;
   int max_fd,i;
   struct rlimit limit_info;
   struct sockaddr_in server;
   struct sockaddr client;

   printf("Server Batalla Naval v"SERVERSION" (c) 1995\n"
	  "by Ricardo Quesada. email: rquesada@dc.uba.ar\n");

   /* Set default parameters */
   for(i=0;i<MAXPLAYER;i++) {
      mimem.nro_tot[i]=0;
      mimem.nro_fd[i]=0;
      ctable(i);
   }

   if(getrlimit(RLIMIT_NOFILE,&limit_info) <0)  {
      perror("btserver: getrlimit error");
      exit(1);
   }
   
   stream = (FILE **) malloc(limit_info.rlim_cur * sizeof(FILE *));
   if(stream == NULL) {
      fprintf(stderr,"btserver: malloc error: not enough mem ?\n");
      exit(1);
   }
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("btserver: socket error");
      exit(1);
   }
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=htonl(INADDR_ANY);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(bind(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("btserver: bind error:");
      exit(2);
   }
   listen(sock,5);
   max_fd=sock;
   FD_ZERO(&test_set);
   FD_SET(sock,&test_set);
   while(1) {
      memcpy(&ready_set,&test_set,sizeof(test_set));
      select(max_fd+1,&ready_set,NULL,NULL,NULL);
      if(FD_ISSET(sock,&ready_set)) {
	 client_len = sizeof(client);
	 fd=accept(sock,(struct sockaddr *)&client,&client_len);
	 FD_SET(fd,&test_set);
	 if(fd>max_fd) max_fd=fd;
      }
      for(fd=0;fd<=max_fd;fd++) {
	 if((fd!=sock) && FD_ISSET(fd,&ready_set)) {
	    if(play_batnav(fd)<0) {
	       close(fd);
	       FD_CLR(fd,&test_set);
	    }
	 } /* if(fd!=sock */
      } /* for(fd=0) */
   } /* while(1) */
}
