/*****************************************************************************/
/*                                                                           */
/*                             Batalla Naval                                 */
/*                                                                           */
/*                               Cliente X                                   */
/*                                                                           */
/*****************************************************************************/

/*
 *         Idea: Sebastian Cativa Tolosa y Ricardo Quesada
 *
 * Programacion:
 *            X: Ricardo Quesada
 *      ncurses: Sebastian Cativa Tolosa
 *       tcp/ip: Ricardo Quesada
 */

/* defines */
#define SERVERSION  ""
#define CLIVERSION  "0.4"
#define BATNAV_TCP_PORT 1066


/*             de la parte de comunicaciones y otros         */

#include <X11/Xlib.h> 
#include <xview/xview.h> 
#include <xview/window.h> 
#include <xview/openmenu.h> 
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/xv_xrect.h>
#include <xview/frame.h>
#include <xview/cms.h>
#include <sspkg/canshell.h>
#include <sspkg/array.h>
#include <sspkg/drawobj.h>

#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include "protocol.h"                  /* definicion del protocolo */
#include "criollo.h"                   /* mensajes en castellano */
/*            de la parte x y otros */

/*
 *                DEFINES
 */ 
#define  WHITE 0
#define  BLACK 1
#define  GREEN 2
#define  BLUE 3
#define  LBLUE 4
#define  YELLOW 5
#define  RED 6

#define BN_XVER "0.01"
#define LARGO 20
#define ANCHO 20


/* 
 *                   VARIABLES GLOBALES
 */

int sock;
struct sockaddr_in server;
struct hostent *host_info;
int mitabla[10][10];          /* mi tabla de barcos */
int tutabla[10][10];          /* tabla para disparar */
int xxtable[4][20];           /* tabla que contiene los valores donde no se debe chequear */
int barquitos[4];             /* tama;os de los barcos 1 de 4, 2 de 3, etc. */
int x,y;                      /* punteros de la tabla mia */
Drawrect barcos_ptr[100];     /* nombre de los objetos de cada barco */
Drawrect tusbarcos_ptr[100];  /*   "     "    del enemigo */

Frame frame,subframe1,subframe2;
Panel panel,panelcito,panelinfo;
Canvas canvas;
Menu menu;
Drawrect proto_drawrect;                 /* sspkg */
Array_tile array_tile;                   /* sspkg */
Rectobj_ops *rectobj_ops;                /* sspkg */
Canvas_shell shell,tushell;
Cms cms;
GC gc;
XGCValues gc_val;
Display *display;
XID xid;

unsigned long *colors;

Xv_singlecolor cms_colors[]= {
     { 255,255,255 },              /* white */
     {   0,  0,  0 },              /* black */
     {   0,255,  0 },              /* green */
     {   0,  0, 255},              /* blue */
     {  30,230, 250},              /* light blue */
     { 255,255,  0 },              /* yellow */
     { 255,  0,  0 },              /* red */
};

struct usuario {
   char nombre[40];
   char alias[12];
   int conection;                    /* connected ? */
   int play;                         /* already playing ? */
   int numjug;                       /* number of player */
   int port;                         /* server port */
   char server_name[50];             /* server name */
} usuario;


/* 
 *                        FUNCIONES
 */

/*
 *             FUNCIONES DE COMUNICACIONES
 */
size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   strcpy(proto.bnpmsg,buf);
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN) ) ;
}

int init_cliente( void ) {
   
   /* Create socket */
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      xv_set(frame,
	     FRAME_RIGHT_FOOTER,"ERROR: creating stream socket",
	     NULL);
      usuario.conection = 0;
      return(1);
   }
   
   host_info = gethostbyname(usuario.server_name);
   if(host_info==NULL) {
      xv_set(frame,
	     FRAME_RIGHT_FOOTER,"ERROR: unknown host",
	     NULL);
      usuario.conection = 0;
      return(2);
   }
   server.sin_family=host_info->h_addrtype;
   memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      xv_set(frame,
	     FRAME_RIGHT_FOOTER,"ERROR: connecting to server",
	     NULL);
      usuario.conection = 0;
      return(2);
   }
   xv_set(frame,
	  FRAME_RIGHT_FOOTER,"Connection established",
	  NULL);
   
   usuario.conection = 1;
   xv_set(panelcito,
 	  PANEL_LABEL_STRING,"Disconnect",
	  NULL); 
   
   return(0);
}

void cliente( int in, int out ) {
   struct protocolo proto;
   int pid;                              /* process id */
   char jugyo=0;                         /* que numero de jugador soy */
   char outbuf[MAXLEN];
   
   if( (pid=fork())==0) {
      printf("pid=%i\n",pid);
      while( read(in,&proto,MAXLEN) > 0)  {
	 switch (proto.bnptip0) {
	  case BNMSG:
	    write(1,proto.bnpmsg,strlen(proto.bnpmsg));
	    break;
	  case BNEXT:
	    /* debo salir del cliente */
	    break;
	  case BN1ST:
	    /* primer jugador */
	  case BNREA:
	    /* me informa de la planilla que le pedi */
	    break;
	  case BNJUG:
	    /* me dice que jugador soy */
	    jugyo=proto.jugador;
	    break;
	  case BNTST:
	    printf("me escribistes una a negro \n");
	  default:
	    break;
	 }
      }
      exit(1);
   }
   else {
      for(;;) {
	 switch( getchar() ) {
	  case 'a':
	    bnwrite(out,outbuf,BNTST,0,0,jugyo);
	    break;
	  case 'b':
	    bnwrite(out,outbuf,BNREA,0,0,jugyo);
	    break;
	  default:
	    break;
	 }
      }
   }
}

/*
 *                            FUNCIONES X  y otras...
 */
void nada( void ) {
}

void about( Frame item, Event *event ) {
   xv_set( subframe1, XV_SHOW, TRUE, NULL );
}

void about_proc( Canvas c, Xv_window pw, Display *display, Window xid, Xv_xrectlist *xrects ) {
   int w,h;
   w = xv_get(pw,XV_WIDTH);
   h = xv_get(pw,XV_HEIGHT);
   
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, 0, w, h/3 );
   XSetForeground(display,gc,colors[WHITE]);
   XFillRectangle(display, xid, gc, 0, h/3, w, h/3*2 );
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, h/3*2, w,h );
   XSetForeground(display,gc,colors[YELLOW]);
   XFillArc(display, xid, gc, w/3 + w/3 * .15, h/3 + h/3 *.15, h/3 * .7, h/3 * .7, 0, 360*64 );
   
}
   

void info( Frame item, Event *event ) {
   xv_set( subframe2, XV_SHOW, TRUE, NULL );
}

/*                            ALGORITMO
 *                  CHEQUEAR SI LOS BARCOS ESTAN BIEN
 */

/* returns TRUE if i have to check this pos. FALSE if it was already checked */
/* this rutine uses local x,y */
int checkthispos(int x,int y) {
   int j=0;

   for(;;) {
      if( xxtable[3][j] == 0) return TRUE;
      if((xxtable[0][j] <= x ) && (xxtable[2][j] >= x) &&
	 (xxtable[1][j] <= y ) && (xxtable[3][j] >= y) ) return FALSE;
      else j++;
   }
}
/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco() {
   if(x==9) { 
      x=0;
      y++;
   }
   else x++;
   if( y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) ) 
     return FALSE;
   if( mitabla[x][y]==1 ) 
     return TRUE;
   if( mitabla[x][y] == 0 ) 
     return FALSE;
   printf("haybarco:error, x=%i,y=%i\n",x,y);
}

/* return TRUE if table is OK .else return FALSE  */
int algoritmo( void )  {
   int i,j,xx,yy,barcos,subarco;
   
   for(i=0;i<4;i++) {          /* clean xxtable */
      for(j=0;j<20;j++)
      	xxtable[i][j]=0;
   } 
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   x=0;y=0;
   j=barcos=0;
   
   for(;;) {
      if( checkthispos(x,y)==TRUE )  {
	 if(haybarco(x,y)==FALSE) {
	    if(nextbarco()==FALSE) {
	       if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
		  printf("OK.barcos=%i,%i,%i\n",barcos,x,y);
		  return TRUE;         /* tabla bien puesta */
	       }
	       else {
		  printf("Error. Tamanos de barcos erroneos\n");
		  printf("[3]=%i,[2]=%i,[1]=%i,[0]=%i\n",barquitos[3],barquitos[2],barquitos[1],barquitos[0]);
		  return FALSE;
	       }
	    }
	    /* aca el nextbarco==TRUE y hace un loop */
	 }
	 else { 
	    if( haybarco(x,y)==TRUE )  {
	       subarco=1;
	       xx=x;yy=y;
	       barcos++;
	       printf("barco encontrado en(x,y)=%i,%i\n",x,y);
	       
	       for(;;) {
		  if( (haybarco(x-1,y+1)==TRUE) || (haybarco(x+1,y+1)==TRUE) ) {
		     printf("1-barcos=%i,%i,%i\n",barcos,x,y);
		     return FALSE;
		  }
		  if( (haybarco(x+1,y)==TRUE) && (haybarco(x,y+1)==TRUE) ) {
		     printf("2-barcos=%i,%i,%i\n",barcos,x,y);
		     return FALSE;
		  }
		  if( (haybarco(x+1,y)==FALSE) && (haybarco(x,y+1)==FALSE) ) {
		     xxtable[0][j]=xx-1; /* los originales */
		     xxtable[1][j]=yy-1;
		     xxtable[2][j]=x+1;  /* los nuevos */
		     xxtable[3][j]=y+1;
		     j++;                /* incremento sig. tabla */
		     x=xx;y=yy;          /* restauro pos */
		     barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
		     break;
		  }
		  else if( haybarco(x+1,y) == TRUE ) {
		     subarco++;
		     barcos++;
		     x++;
		     printf("                   (x,y)=%i,%i\n",x,y);
		  }          
		  else if( haybarco(x,y+1) == TRUE ) {
		     y++;
		     barcos++;
		     subarco++;
		     printf("                   (x,y)=%i,%i\n",x,y);
		  }
		  if( subarco > 4) {
		     printf("Barco mayor que 4.(x,y)=%i,%i\n",x,y);
		     return FALSE;
		  }
		  if( barcos > 20 ) {
		     printf("3-barcos=%i,%i,%i\n",barcos,x,y);
		     return FALSE;
		  }
	       } /* hasta es el bucle */
	    }
	 }
      }
      else if( checkthispos(x,y)==FALSE )  {
	 if(nextbarco()==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       printf("OK.barcos=%i,%i,%i\n",barcos,x,y);
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       printf("Error. Tamanos de barcos erroneos\n");
	       printf("[3]=%i,[2]=%i,[1]=%i,[0]=%i\n",barquitos[3],barquitos[2],barquitos[1],barquitos[0]);
	       return FALSE;
	    }
	 }
	 /* aca el nextbarco==TRUE y loopea */
      }
   }
}

/*
*                     EMPEZAR LA PARTIDA
*/
int play( void ) {
   if( usuario.conection != 1) {
      xv_set( frame,
	     FRAME_RIGHT_FOOTER,"First establish a connection",
	     NULL);
      return(1);
   }
   if( usuario.play != 0)  {
      xv_set( frame,
	     FRAME_RIGHT_FOOTER,"You are already playing",
	     NULL);
      return(1);
   }
   if( algoritmo()==FALSE) {
      xv_set( frame,
	     FRAME_RIGHT_FOOTER,"Check the board!",
	     NULL);
      usuario.play=0;
      return(1);
   }
   else if( algoritmo()==TRUE) {
      xv_set( frame,
	     FRAME_RIGHT_FOOTER,"Board is OK!",
	     NULL);
      usuario.play=1;
      return(0);
   }
}

/*
 *                          QUIT
 */
void quit( ) {
   xv_destroy_safe( frame );
}

/*
 *                           BUTTON PRESSED IN MY TABLE
 */
int mitabla_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   
   int x,y;
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;

   if( usuario.play != 0 ) {
      xv_set(frame,
	     FRAME_RIGHT_FOOTER,"You cant modify while playing!",
	     NULL);
      return(1);
   }
   
   switch( mitabla[x][y] )  {
    case 0:
      mitabla[x][y]=1;
      xv_set(r,
/*   	     RECTOBJ_BG2,BLUE, */
	     RECTOBJ_BG,BLUE,
	     NULL);
      break;
    case 1:
      mitabla[x][y]=0;
      xv_set(r,
/*	     RECTOBJ_BG2,WHITE, */
	     RECTOBJ_BG,WHITE, 
	     NULL);
      break;
    default:
      printf("ACA NUNCA TENDRIA QUE LLEGAR: X=%i,Y=%i,S=%i\n",x,y,mitabla[x][y]);
      break;
   }
}
/*
 *                           BUTTON PRESSED IN ENEMY TABLE
 */
int tutabla_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   
   int x,y;
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;

   switch( tutabla[x][y] )  {
    case 0:
      tutabla[x][y]=1;
      xv_set(r,
/*   	     RECTOBJ_BG2,RED, */
	     RECTOBJ_BG,RED,
	     NULL);
      break;
    case 1:
      tutabla[x][y]=0;
      xv_set(r,
/*	     RECTOBJ_BG2,WHITE, */
	     RECTOBJ_BG,WHITE, 
	     NULL);
      break;
    default:
      printf("ACA NUNCA TENDRIA QUE LLEGAR: X=%i,Y=%i,S=%i\n",x,y,tutabla[x][y]);
      break;
   }
}

/*
 *                               INICIALIZAR VARIABLES
 */
void datos_init( void ) {
   int i,j;
   
   usuario.conection = 0;
   usuario.play = 0;
   
   for(i=0;i<10;i++) {          /* clean tabla */
      for(j=0;j<10;j++)
      	mitabla[i][j]=0;
   } 

}

/*
 *                                 INICIALIZAR X
 */
void batnav_xv_init( void ) {
   
   int i;

 
   /* crea los colores */
   cms = (Cms) xv_create( XV_NULL,CMS,
			 CMS_SIZE,7,
			 CMS_TYPE, XV_STATIC_CMS,
			 CMS_COLORS,cms_colors,
			 NULL );
   /* ventana principal */
   frame = (Frame)xv_create(NULL, FRAME,
			    XV_HEIGHT,230,
			    XV_WIDTH,800,
			    FRAME_LABEL,"Batalla Naval v"BN_XVER,
			    FRAME_SHOW_FOOTER,TRUE,
			    FRAME_LEFT_FOOTER,"Batalla Naval",
			    FRAME_RIGHT_FOOTER,"(c) 1995",
			    NULL);
  

/* panel para los botones quit,play,etc */
   panel = xv_create( frame,PANEL,
		     PANEL_LAYOUT,PANEL_VERTICAL,
		     XV_WIDTH, 100,
		     NULL ); 

/* shell para mi tabla */
   shell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				    WIN_CMS,cms,
				    NULL );
   xv_create( shell, DRAWTEXT,
	     DRAWTEXT_STRING_PTR,"My ships",
	     XV_WIDTH,ANCHO*10,
	     DRAWTEXT_JUSTIFY,DRAWTEXT_JUSTIFY_CENTER,
	     RECTOBJ_SELECTABLE,FALSE,
	     NULL); 
   array_tile = (Array_tile) xv_create( shell, ARRAY_TILE,
				       XV_Y,20, 
				       XV_WIDTH,ANCHO*10,
				       XV_HEIGHT,LARGO*10,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 1,
				       ARRAY_TILE_COLUMN_GAP, 1,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, mitabla_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);
   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   for(i=0;i<100;i++) {    /* 10 * 10 */
      barcos_ptr[i] = (Drawrect) xv_create( array_tile, DRAWRECT,
					   RECTOBJ_OPS, rectobj_ops,
					   XV_WIDTH,ANCHO,
					   XV_HEIGHT,LARGO,
					   DRAWRECT_BORDER2,1,
					   DRAWOBJ_FILLED,TRUE,
					   RECTOBJ_BG2, WHITE,
					   RECTOBJ_BG, WHITE,
					   RECTOBJ_FG, BLACK,
					   NULL );
   }
   xv_set( shell, 
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) + 20 ,
	  NULL ); 

/* shell para enemy tabla */
   tushell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				      WIN_CMS,cms,
				      NULL );
   xv_create( tushell, DRAWTEXT,
	     DRAWTEXT_STRING_PTR,"Enemy ships",
	     XV_WIDTH,ANCHO*10,
	     DRAWTEXT_JUSTIFY,DRAWTEXT_JUSTIFY_CENTER,
	     RECTOBJ_SELECTABLE,FALSE,
	     NULL); 
   array_tile = (Array_tile) xv_create( tushell, ARRAY_TILE,
				       XV_Y,20, 
				       XV_WIDTH,ANCHO*10,
				       XV_HEIGHT,LARGO*10,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 1,
				       ARRAY_TILE_COLUMN_GAP, 1,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, tutabla_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);
   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   for(i=0;i<100;i++) {    /* 10 * 10 */
      tusbarcos_ptr[i] = (Drawrect) xv_create( array_tile, DRAWRECT,
					      RECTOBJ_OPS, rectobj_ops,
					      XV_WIDTH,ANCHO,
					      XV_HEIGHT,LARGO,
					      DRAWRECT_BORDER2,1,
					      DRAWOBJ_FILLED,TRUE,
					      RECTOBJ_BG2, WHITE,
					      RECTOBJ_BG, WHITE,
					      RECTOBJ_FG, BLACK,
					      NULL );
   }
   xv_set( tushell, 
	  XV_X,320,
	  XV_Y,0,
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) + 20 ,
	  NULL ); 

   
   /* creacion de botones */
   panelcito = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING,"Connect",
				 PANEL_NOTIFY_PROC,init_cliente,
				 NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Play",
	     PANEL_NOTIFY_PROC,play,
	     NULL);
   menu = (Menu) xv_create (NULL, MENU,
			    MENU_GEN_PIN_WINDOW, frame, "Players",
			    MENU_NOTIFY_PROC, nada,
			    MENU_STRINGS, "Player 1","Player 2","Player 3","Player 4","Player 5",NULL,
			    NULL);
   xv_create( panel, PANEL_BUTTON,
 	     PANEL_LABEL_STRING,"Players",
 	     PANEL_NOTIFY_PROC,nada,
	     PANEL_ITEM_MENU,menu,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Info",
	     PANEL_NOTIFY_PROC,info,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"About",
	     PANEL_NOTIFY_PROC,about,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Quit",
	     PANEL_NOTIFY_PROC,quit,
	     NULL);

   
/* 
 * todo lo referente a about
 */
   subframe1=(Frame) xv_create(frame,FRAME,
			       XV_HEIGHT,300,
			       XV_WIDTH,300, 
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) 1995",
			       FRAME_LABEL, "About",
			       NULL);
   canvas = xv_create( subframe1,CANVAS,
		      CANVAS_X_PAINT_WINDOW,TRUE,
		      XV_VISUAL_CLASS, PseudoColor,
		      WIN_CMS,cms,
		      CANVAS_REPAINT_PROC,about_proc,
		      NULL );
   display = (Display *) xv_get( subframe1, XV_DISPLAY);
   xid = (XID) xv_get(  canvas_paint_window( canvas ) ,XV_XID );
   gc = XCreateGC( display, xid, NULL , &gc_val );
   colors = (unsigned long*) xv_get( canvas,WIN_X_COLOR_INDICES);

/* 
 * todo lo referente a info 
 */
   subframe2=(Frame) xv_create(frame,FRAME_CMD,
			       XV_HEIGHT,300,
			       XV_WIDTH,300,
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) 1995",
			       FRAME_LABEL, "Information",
			       NULL);
   panelinfo = (Panel) xv_get( subframe2,FRAME_CMD_PANEL);
   (void) xv_create( panelinfo, PANEL_BUTTON,
		    XV_X,50,
		    XV_Y,150,
		    PANEL_LABEL_STRING,"Cancel",
		    PANEL_NOTIFY_PROC,nada,
		    NULL);
   (void) xv_create( panelinfo, PANEL_BUTTON,
		    XV_X,150,
		    XV_Y,150,
		    PANEL_LABEL_STRING,"OK",
		    PANEL_NOTIFY_PROC,nada,
		    NULL);
   (Panel_item) xv_create( panelinfo, PANEL_TEXT,
   		          XV_X,20,
                          XV_Y,45,
			  PANEL_VALUE,usuario.server_name,
			  PANEL_LABEL_STRING,"Server name:",
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  PANEL_VALUE_DISPLAY_LENGTH,20,
			  NULL);
   (Panel_item) xv_create( panelinfo, PANEL_TEXT,
   		          XV_X,20,
                          XV_Y,70,
			  PANEL_LABEL_STRING,"Your name:",
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  PANEL_VALUE_DISPLAY_LENGTH,20,
			  NULL);
   (Panel_item) xv_create( panelinfo, PANEL_TEXT,
   		          XV_X,20,
                          XV_Y,95,
			  PANEL_LABEL_STRING,"Your alias:",
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  PANEL_VALUE_DISPLAY_LENGTH,20,
			  NULL);
   (Panel_item) xv_create( panelinfo, PANEL_NUMERIC_TEXT,
   		          XV_X,20,
                          XV_Y,120,
			  PANEL_LABEL_STRING,"Server port:",
			  PANEL_VALUE,1066,
			  PANEL_MAX_VALUE,65535,
			  PANEL_MIN_VALUE,0,
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  NULL);

   /*
    * startup x, and run
    */
   window_fit( frame ); 
   init_cliente();                 /* establecer coneccion ahora */
   xv_main_loop(frame);

}

/*
 *                        main()
 */

void main( int argc, char *argv[] )
{
   xv_init( XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
   if(argc>1) strcpy( usuario.server_name,argv[1]);
   else strcpy( usuario.server_name,"localhost");
   datos_init();                 /* inicializa variables */
   batnav_xv_init();             /* inicializa entorno y juego */
}
