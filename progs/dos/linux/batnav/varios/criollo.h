/* mensajes es CRIOLLO */
#define MSG_SCOP 0x00                         /* copyright notice SERVER */
#define MSG_CCOP 0x01                         /* copyright notice CLIENT */
#define MSG_THNX 0x02                         /* gracias por jugar */
#define MSG_PLAY 0X03                         /* te conectastes a un server */
#define MSG_JUGA 0x04                         /* hey, you are player number */
#define MSG_WAIT 0x05                         /* hey, you are the 1st player */
#define MSG_READ 0x06                         /* re reading error */

char *btmsg[] = {
   "\nServidor de Batalla Naval v"SERVERSION" (c) 1995\npor Ricardo Quesada y Sebastian Cativa Tolosa\nemail: rquesada@dc.uba.ar , scativa@dc.uba.ar\n\n",
   "\nCliente de Batalla Naval v"CLIVERSION" (c) 1995\npor Ricardo Quesada y Sebastian Cativa Tolosa\nemail: rquesada@dc.uba.ar , scativa@dc.uba.ar\n\n",
   "Gracias por jugar a Batalla Naval\n",
   "Conectado a un servidor Batalla Naval v"SERVERSION" en el host",
   "Usted es el jugador",
   "Por favor espere a que ingresen otros jugadores",
   "btserver: recomenzando lectura del jugador"
};
