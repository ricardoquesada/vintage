/*****************************************************************************/
/*                                                                           */
/*                          Batalla Naval v0.02                              */
/*                                                                           */
/*                               Servidor                                    */
/*                                                                           */
/*               por Ricardo Quesada y Sebastian Cativa Tolosa               */
/*                                                                           */
/*****************************************************************************/

/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "protocolo.h"

/* DEFINES */
#define BATVERSION  "0.02a"
#define BATNAV_TCP_PORT 1066
#define MAXPLAYER 5
#define KEY ((key_t) (1243))

/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */
struct shmemoria {
   int nro_jug;
} *mimem;
#define SEGSIZE sizeof(struct shmemoria)

/* FUNCIONES */

size_t bnwrite(int fd,char *buf,char tip0,char tip1 ) {
   short int i;
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   strcpy(proto.bnpmsg,buf);
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN - (MSGMAXLEN - strlen(buf) ) );
}
   

void play_batnav(int in,int out) {
   int  jugyo;
   char hostname[MSGMAXLEN];
   char outbuf[MSGMAXLEN],nombre[MSGMAXLEN];
 
   jugyo = mimem-> nro_jug++;
   gethostname(hostname,MSGMAXLEN);
   sprintf(outbuf,"Playing Batalla Naval v"BATVERSION" on host %s:\n\n",hostname);
   bnwrite(out,outbuf,BNMSG,0);
   
   if(mimem->nro_jug==2)  {
      sprintf(outbuf,"Por favor aguarde a que entren otros jugadores.");
      bnwrite(out,outbuf,BNMSG,0);
    }
   do{}while(mimem->nro_jug==2);
   	  
   sprintf(outbuf,"Por favor, ingrese su nombre[%i]\n",jugyo);
   bnwrite(out,outbuf,BNMSG,0);
   while( read(in,nombre,MAXLEN) <0) {
      if(errno != EINTR)
      	exit(4);
      printf("re-starting the read\n");
   }
   strcat(nombre,":lo siento, nombre incorrecto.\n");
   sprintf(outbuf,nombre);
   bnwrite(out,outbuf,BNMSG,0);
}

void main() {
   int id,sock,fd,client_len;
   struct sockaddr_in server,client;
   struct shmid_ds shmbuf;
   
   printf("Servidor para el Batalla Naval v"BATVERSION".\n");
   id=shmget(KEY,SEGSIZE,IPC_CREAT | 0644 );
   if( id < 0 )  {
      perror("btserver: shmget failed:");
      exit(1);
   }
   
   mimem = (struct shmemoria *) shmat(id,0,0);
   if(mimem <= (struct info *) (0)) {
      perror("btserver: shmat failed:");
      exit(2);
   }

   /* Set default parameters */
   mimem->nro_jug=1;
   
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("btserver: creating stream socket");
      exit(1);
   }
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=htonl(INADDR_ANY);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(bind(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("btserver: binding socket");
      exit(2);
   }
   listen(sock,5);
   signal(SIGCHLD,SIG_IGN);
   while(1) {
      client_len = sizeof(client);
      if((fd=accept(sock,(struct sockaddr *)&client, &client_len))<0) {
	 perror("btserver: accepting connection");
	 exit(3);
      }
      if(fork()==0) {
	 play_batnav(fd,fd);
	 exit(0);
      } else
      	close (fd);
   }
}
