/******************************************
 * Archivo que pertenece al batalla naval *
 *                                        * 
 *    definicion del protocolo v0.07      *
 *                                        *
 ******************************************/

struct protocolo  {
   char bnphead[3];                       /* bnp head */
   char bnpver;                           /* protocol version */
   char jugador;                          /* nro de cliente */
   char bnptip0;                          /* token */
   char bnptip1;                          /* value de token */
   char bnptip2;                          /* user o root */
   char bnpmsg[100];                      /* any message o table */
};
#define MSGMAXLEN 100
#define MAXLEN sizeof(struct protocolo)
#define HEADLEN MAXLEN - MSGMAXLEN

#define BNPHEAD "BNP"
#define BNPVER   0x07
/* valores posibles del tipo 0 */
#define BNMSG    0x41          /* es un msg */               

#define BNEXT    0x43          /* si lo manda el server: que se vaya el cli */
#define BNJUG    0x44          /* te dice que jugador sos */

#define BNALL    0x46          /* sorry, pero ya hay muchos logueados */
#define BNWAI    0x47          /* Wait, people are already playing */

#define BNREA    0x57          /* read tablas de tipo 1 */
#define BNWRI    0x58          /* write tablas de tipo 1 . I think this if for cheat */
#define BNHIT    0x59          /* fire at (x,y) = message */
#define BNSTR    0x5C          /* Start the play. */
#define BNTRN    0x5D          /* Your turn to hit. */
#define BNLOS    0x5E          /* you lost */
#define BNWIN    0x5F          /* you win */

#define BNTST    0x20          /* this is for test only */

/* valores posibles del tipo 2 */
#define BNUSR    0x38          /* soy user normal */
#define BNSUP    0x39          /* i want to cheat */
