/*****************************************************************************
 *                                                                           *
 *                            Batalla Naval                                  *
 *                                                                           *
 *                              Cliente X                                    *
 *                          por Ricardo Quesada                              *
 *                                                                           *
 * The whole game was programmed by:                                         *
 *            X client : by Ricardo Quesada                                  *
 *      ncurses client : by Sebastian Cativa Tolosa (in development)         *
 *              server : by Ricardo Quesada                                  *
 *****************************************************************************/


#include <X11/Xlib.h> 
#include <xview/xview.h> 
#include <xview/window.h> 
#include <xview/openmenu.h> 
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/xv_xrect.h>
#include <xview/frame.h>
#include <xview/cms.h>
#include <xview/notify.h>
#include <sspkg/canshell.h>
#include <sspkg/array.h>
#include <sspkg/drawobj.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include "protocol.h"                  /* definicion del protocolo */
/*            de la parte x y otros */

/*
 *                DEFINES
 */ 
#define  WHITE  0  
#define  BLACK  1
#define  GREEN  2
#define  BLUE   3
#define  LBLUE  4
#define  YELLOW 5
#define  RED    6

#define BN_XVER "0.30"
#define LARGO 20
#define ANCHO 20

#define RAPID 10                       /* the lower, more time for select() */

/* 
 *                   VARIABLES GLOBALES
 */

int sock;
struct sockaddr_in server;
struct hostent *host_info;
char mitabla[10][10];          /* mi tabla de barcos */
char tutabla[10][10];          /* tabla para disparar */
char barquitos[4];             /* tama;os de los barcos 1 de 4, 2 de 3, etc. */
short int x,y;                 /* punteros de la tabla mia */
Drawrect barcos_ptr[100];      /* nombre de los objetos de cada barco */
Drawrect tusbarcos_ptr[100];   /*   "     "    del enemigo */

Frame frame,subframe1,subframe2;
Panel panel,panelcito,panelinfo,panelcit2;
Canvas canvas;
Menu menu;
Drawrect proto_drawrect;                 /* sspkg */
Array_tile array_tile;                   /* sspkg */
Drawtext   drawtext1;                    /* sspkg */
Rectobj_ops *rectobj_ops;                /* sspkg */
Canvas_shell shell,tushell;
Cms cms;
GC gc;
XGCValues gc_val;
Display *display;
XID xid;
fd_set readfds;                           /* used by proceso() */
struct itimerval timer;                   /* used by proceso() */
char bnsup;                               /* usado por bnwrite */

#ifndef __linux
struct timeval timeout=  { 0,250000 };
#else
struct timeval timeout;
#endif

unsigned long *colors;

Xv_singlecolor cms_colors[]= {
     { 255,255,255 },              /* white */
     {   0,  0,  0 },              /* black */
     {   0,  0, 255},              /* blue  INVERTED in v0.29 */
     {   0,255,  0 },              /* green INVERTED in v0.29 */
     {  30,230, 250},              /* light blue */
     { 255,255,  0 },              /* yellow */
     { 255,  0,  0 },              /* red */
};

/* pasa a ser memoria compartida v0.18 */
struct usuario {
   char server_name[50];             /* server name */
   int port;                         /* server port */
   char nombre[40];                  /* user name */

   int play;                         /* already playing ? */
   int numjug;                       /* number of player */
   int usrfrom;                      /* player who is owner of enemy window */
} usuario;


/* 
 *                        FUNCIONES
 */


/* 
 * funciones varias
 */

/* 2 funciones para escribir mensajes abajo */
void foot_right( char msg[] ) {
   xv_set(frame,
	  FRAME_RIGHT_FOOTER,msg,
	  NULL);
}
void foot_left( char msg[] ) {
   xv_set(frame,
	  FRAME_LEFT_FOOTER,msg,
	  NULL);
}
/* paint mi cell */
void pmicell(int x,int y,int color) {
   int i;
   i=10*y+x;
   xv_set(barcos_ptr[i],
	  RECTOBJ_BG, color,
	  NULL );
}
/* paint tu cell */
void ptucell(int x,int y,int color) {
   int i;
   i=10*y+x;
   xv_set(tusbarcos_ptr[i],
	  RECTOBJ_BG, color,
	  NULL );
}

void repaintmi( void )  {
   int x,y;
   for(x=0;x<10;x++) {
      for(y=0;y<10;y++) {
	 if( mitabla[x][y]==1 )
	   pmicell(x,y,BLUE);
	 else{	   
	    if( mitabla[x][y]==0 )
	      pmicell(x,y,WHITE);
	 }
      }
   }
}
void repainttu( char outbuf[] )  {
   int x,y,i;
   for(x=0,y=0,i=0;i<MSGMAXLEN;i++) {
      if( outbuf[i]==2)
      	ptucell(x,y,RED);
      else if(outbuf[i]==3)
      	ptucell(x,y,BLACK);
      else if(outbuf[i]==-1)
      	ptucell(x,y,GREEN);
      else if(outbuf[i]==0)
      	ptucell(x,y,WHITE);
      else if(outbuf[i]==1)
      	ptucell(x,y,BLUE);       
      x++;
      if(x>=10) {
	 x=0;y++;
      }
   }
}

int iwtable( char *dest) {
   int i,x,y;
   x=0;y=0;
   for(i=0;i<100;i++) {
      dest[i]=mitabla[x][y];
      x++;
      if(x>=10) {
	 x=0;
	 y++;
      }
   }
}
   
/*
 *             FUNCIONES DE COMUNICACIONES
 */

size_t bnwrite(int fd,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
   int i;
   
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   for(i=0;i<MSGMAXLEN;i++)
     proto.bnpmsg[i]=buf[i];
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fd,&proto,MAXLEN) ) ;
}

Notify_value proceso( ) {
   struct protocolo proto;
   char outbuf[MSGMAXLEN];
   
   FD_SET(sock,&readfds);
   if(select(FD_SETSIZE,&readfds,NULL,NULL,&timeout)) {
      if( read(sock,&proto,MAXLEN) >0) {
	 switch(proto.bnptip0) {
	  case BNTST:
	    break;
	  case BNWRI:
	    if(proto.bnpmsg[0]==1) {
	       xv_set(panelcit2,
		      PANEL_LABEL_STRING,"Start",
		      NULL); 
	       foot_right("The board is OK.");
	       usuario.play=2;
	    }
	    else if(proto.bnpmsg[0]==0) {
	       foot_right("Check the board.");
	       usuario.play=1;
	    }
	    break;
	  case BNJUG:
	    usuario.numjug=proto.bnptip1;
	    sprintf(outbuf,"Connected to server:%s",proto.bnpmsg);
	    foot_right(outbuf);
	    sprintf(outbuf,"Player %i",usuario.numjug);
	    foot_left(outbuf);
	    usuario.play = 1;
	    xv_set(panelcito,
		   PANEL_LABEL_STRING,"Disconnect",
		   NULL); 
	    if(usuario.numjug==1) {
	       usuario.usrfrom=2;
	       xv_set( drawtext1,
		      DRAWTEXT_STRING,"Player 2 board",
		      NULL );
	    }
	    else {
	       usuario.usrfrom=1;
	       xv_set( drawtext1,
		      DRAWTEXT_STRING,"Player 1 board",
		      NULL );
	    }
	    break;
	  case BNMSG:
	    break;
	  case BNALL:
	    foot_right("I am sorry, but server is full. Try later");
	    close(sock);
	    break;
	  case BNWAI:
	    foot_right("Hey, people are playing now. Try later.");
	    break;
	  case BNHIT:
	    if(usuario.play>=3) {
	       pmicell(proto.bnpmsg[0],proto.bnpmsg[1],proto.bnpmsg[2]);
	    }
	    bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
	    break;
	  case BNREA:
	    repainttu(proto.bnpmsg);
	    break;
	  case BNSTR:
	    if(usuario.play==2) {
	       usuario.play=3;
	       xv_set(panelcit2,
		      PANEL_LABEL_STRING,"Started",
		      NULL);
	    }
	    break;
	  case BNCON:
	    sprintf(outbuf,"Player %i is ready to play!",proto.bnptip1);
	    foot_right(outbuf);
	    break;
	  case BNTRN:
	    usuario.play=4;                    /* oh, it is my turn */
	    foot_right("It is my turn!");
	    break;
	  case BNLOS:
	    usuario.play=0;                    /* oh, i lost the game */
	    foot_right("You are DEAD man");
	    xv_set(panelcito,
		   PANEL_LABEL_STRING,"Connect",
		   NULL); 
	    xv_set(panelcit2,
		   PANEL_LABEL_STRING,"Send Board",
		   NULL); 
	    foot_left("Batalla Naval");
	    break;
	  case BNWIN:
	    usuario.play=0;                    /* oh, i lost the game */
	    foot_right("CONGRATULATIONS: You are the winner.");
	    xv_set(panelcito,
		   PANEL_LABEL_STRING,"Connect",
		   NULL);
	    xv_set(panelcit2,
		   PANEL_LABEL_STRING,"Send Board",
		   NULL); 
	    foot_left("Batalla Naval");
	    break;
	  case BNSOL:
	    foot_right("You cant play alone");
	    break;
	  default:
	    break;
	 }
      }
   }
}

int init_cliente( void ) {
   char outbuf[MSGMAXLEN];
   
   if(usuario.play==0) {                  /* estado de un boton */
      /* Create socket */
      sock=socket(AF_INET,SOCK_STREAM,0);
      if(sock <0) {
	 foot_right("ERROR: creating stream socket");
	 return(1);
      }
      host_info = gethostbyname(usuario.server_name);
      if(host_info==NULL) {
	 foot_right("ERROR: unknown host");
	 return(2);
      }
      server.sin_family=host_info->h_addrtype;
      memcpy( (char*) &server.sin_addr, host_info->h_addr,host_info->h_length);
      server.sin_port=htons(usuario.port);
      
      
      if(connect(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
	 foot_right("ERROR: connecting to server");
	 return(2);
      }

      timer.it_value.tv_usec = RAPID * 1000;
      timer.it_interval.tv_usec = RAPID * 1000;
      notify_set_itimer_func(frame, proceso,ITIMER_REAL, &timer, NULL);

      bnwrite(sock,outbuf,BNJUG,0,0,0);       /* WHO AM I */

      return(0);
   }
   else if(usuario.play!=0) {
      xv_set(panelcito,
	     PANEL_LABEL_STRING,"Connect",
	     NULL); 
      xv_set(panelcit2,
	     PANEL_LABEL_STRING,"Send Board",
	     NULL); 
      foot_right("(c) 1995,1996");
      foot_left("Batalla Naval");
      usuario.play=0;
      bnwrite(sock,outbuf,BNEXT,0,0,usuario.numjug);
      close(sock);
   }
}

/*
 *                            FUNCIONES X  y otras...
 */
void nada( void ) {
}
void menuses( Menu menu,Menu_item menu_item) {
   char outbuf[MSGMAXLEN];
   
   if(!strcmp((char *)xv_get(menu_item,MENU_STRING),"Player 1")) {
      xv_set( drawtext1, 
	     DRAWTEXT_STRING,"Player 1 board",
	     NULL );
      usuario.usrfrom=1;
   }
   else if(!strcmp((char *)xv_get(menu_item,MENU_STRING),"Player 2")) {
      xv_set( drawtext1,
	     DRAWTEXT_STRING,"Player 2 board",
	     NULL );
      usuario.usrfrom=2;
   }
   else if(!strcmp((char *)xv_get(menu_item,MENU_STRING),"Player 3")) {
      xv_set( drawtext1, 
	     DRAWTEXT_STRING,"Player 3 board",
	     NULL );
      usuario.usrfrom=3;
   }
   else if(!strcmp((char *)xv_get(menu_item,MENU_STRING),"Player 4")) {
      xv_set( drawtext1, 
	     DRAWTEXT_STRING,"Player 4 board",
	     NULL );
      usuario.usrfrom=4;
   }
   else if(!strcmp((char *)xv_get(menu_item,MENU_STRING),"Player 5")) {
      xv_set( drawtext1,
	     DRAWTEXT_STRING,"Player 5 board",
	     NULL );
      usuario.usrfrom=5;
   }
   if(usuario.play>=3)
     bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
}

void about( Frame item, Event *event ) {
   xv_set( subframe1, XV_SHOW, TRUE, NULL );
}

void about_proc( Canvas c, Xv_window pw, Display *display, Window xid, Xv_xrectlist *xrects ) {
   int w,h;
   w = xv_get(pw,XV_WIDTH);
   h = xv_get(pw,XV_HEIGHT);
   
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, 0, w, h/3 );
   XSetForeground(display,gc,colors[WHITE]);
   XFillRectangle(display, xid, gc, 0, h/3, w, h/3*2 );
   XSetForeground(display,gc,colors[LBLUE]);
   XFillRectangle(display, xid, gc, 0, h/3*2, w,h );
   XSetForeground(display,gc,colors[YELLOW]);
   XFillArc(display, xid, gc, w/3 + w/3 * .15, h/3 + h/3 *.15, h/3 * .7, h/3 * .7, 0, 360*64 );
   XSetForeground(display,gc,colors[BLACK]);
   XDrawString(display,xid,gc,42, 40,"XCLIENT BATALLA NAVAL (c) 1995,1996",35);
   XDrawString(display,xid,gc,18, 60,"BY RICARDO QUESADA (email:rquesada@dc.uba.ar)",45);
   XDrawString(display,xid,gc,88,100,"NCURSES CLIENT BY:",18);
   XDrawString(display,xid,gc,8,120,"SEBASTIAN CATIVA TOLOSA (email:scativa@dc.uba.ar)",48);
   XDrawString(display,xid,gc,110,145,"SERVER BY:",10);
   XDrawString(display,xid,gc,94,165,"RICARDO QUESADA",15);
   XDrawString(display,xid,gc,110,200,"RIQ THANKS:",11);
   XDrawString(display,xid,gc,30,215,"NACHO,JOHN,MORKY (amigos de Chapadmalal)",40);
   XDrawString(display,xid,gc,30,230,"EMY,JAVI,EDU,SANTI,JUAN,GONCHI(del Cole)",40);
   XDrawString(display,xid,gc,40,245,"Y A TODA LA GENTE DE LA UBA, EXACTAS",36);
   XDrawString(display,xid,gc,66,275,"HECHO EN BUENOS AIRES, ARGENTINA",32);
}
   
void info( Frame item, Event *event ) {
   xv_set( subframe2, XV_SHOW, TRUE, NULL );
}
int server_port( Panel_item item, Event *event ) {
   usuario.port=(int)xv_get(item,PANEL_VALUE);
   return PANEL_NEXT;
}
int server_name( Panel_item item, Event *event ) {
   strcpy(usuario.server_name,(char *)xv_get(item,PANEL_VALUE));
   return PANEL_NEXT;
}
int your_name( Panel_item item, Event *event ) {
   strcpy(usuario.nombre,(char *)xv_get(item,PANEL_VALUE));
   return PANEL_NEXT;
}
/*                            ALGORITMO
 *                  CHEQUEAR SI LOS BARCOS ESTAN BIEN
 */

/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco() {
   if(x==9) { 
      x=0;
      y++;
   }
   else x++;
   if( y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) ) 
     return FALSE;
   if( mitabla[x][y]==1 ) 
     return TRUE;
   if( mitabla[x][y] == 0 ) 
     return FALSE;
   foot_right("ABNORMAL ERROR: please contact the authors.");
}

/* return TRUE if board is OK .else return FALSE  */
int algoritmo( void )  {
   int i,xx,yy,barcos,subarco;
   char temporal[60];
   
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   x=0;y=0;
   barcos=0;
   
   for(;;) {
      if(haybarco(x,y)==FALSE) {
	 if(nextbarco()==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       sprintf(temporal,"ERROR: [4]=%i,[3]=%i,[2]=%i,[1]=%i",barquitos[3],barquitos[2],barquitos[1],barquitos[0]);
	       foot_right(temporal);
	       return FALSE;
	    }
	 }
      }
      else { 
	 if( haybarco(x,y)==TRUE )  {
	    if( (haybarco(x,y-1)==FALSE) && (haybarco(x-1,y)==FALSE) ) {
	       /* this is the first time i check this */
	       subarco=1;
	       barcos++;
	       xx=x;yy=y;
	       
	       for(;;) {
		  if( (haybarco(x-1,y+1)==TRUE) || (haybarco(x+1,y+1)==TRUE) ) {
		     sprintf(temporal,"Collision in (x,y)=%i,%i",x,y);
		     foot_right(temporal);
		     pmicell(x,y,RED);
		     return FALSE;
		  }
		  if( (haybarco(x+1,y)==FALSE) && (haybarco(x,y+1)==FALSE) ) {
		     x=xx;y=yy;          /* restauro pos */
		     barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
		     break;
		  }
		  else if( haybarco(x+1,y) == TRUE ) {
		     subarco++;
		     barcos++;
		     x++;
		  }          
		  else if( haybarco(x,y+1) == TRUE ) {
		     y++;
		     barcos++;
		     subarco++;
		  }
		  if( subarco > 4) {
		     sprintf(temporal,"Ship longer than 4 units in (x,y)=%i,%i",x,y);
		     foot_right(temporal);
		     pmicell(x,y,RED);
		     return FALSE;
		  }
	       } /* for(;;) */
	    } /* if(haybarco(x,y-1)==FALSE) */
	 } /* if(haybarco(x,y)==TRUE) */
	 if(nextbarco()==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       sprintf(temporal,"ERROR: [4]=%i,[3]=%i,[2]=%i,[1]=%i",barquitos[3],barquitos[2],barquitos[1],barquitos[0]);
	       foot_right(temporal);
	       return FALSE;
	    }
	 } /* if nextbarco()==FALSE) */
      } /* else */
   } /* for(;;) */
} /* void algoritomo() */

/*
 *                     EMPEZAR LA PARTIDA
 */
int play( void ) {
   char outbuf[MSGMAXLEN];
   
   if(usuario.play==0) {         /* estado del boton SEND BOARD */
      foot_right("First establish a connection");
      return(FALSE);
   }
   else if( usuario.play==1)  {
      repaintmi();
      if( algoritmo()==FALSE) {
	 return(FALSE);
      }
      else if( algoritmo()==TRUE) {
	 foot_right("");         /* borrar mensaje de abajo */
	 iwtable( outbuf );
	 bnwrite(sock,outbuf,BNWRI,usuario.numjug,0,usuario.numjug);
	 return( TRUE );
      } /* close algorito==ok */
   }
   else if(usuario.play==2) {
      bnwrite(sock,outbuf,BNSTR,usuario.numjug,0,usuario.numjug);
   }
   else if(usuario.play==3) {
      foot_right("Que muchacho?.This mean that you are playing!");
   }
}
/*
 *                          QUIT
 */
void quit( ) {
   char outbuf[MSGMAXLEN];
   if(usuario.play!=0){             /* if connected then kill child */
      bnwrite(sock,outbuf,BNEXT,0,0,usuario.numjug);
      close(sock);
   }
   xv_destroy_safe( frame );
}

/*
 *                           BUTTON PRESSED IN MY BOARD
 */
int mitabla_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   
   int x,y;
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;

   if( usuario.play >= 3 ) {
      foot_right("You cant modify while playing!");
      return(1);
   }
   if( usuario.play == 2 ) {
      foot_right("The board is OK. You cant modify it!");
      return(1);
   }

   switch( mitabla[x][y] )  {
    case 0:
      mitabla[x][y]=1;
      xv_set(r,
	     RECTOBJ_BG,BLUE,
	     NULL);
      break;
    case 1:
      mitabla[x][y]=0;
      xv_set(r,
	     RECTOBJ_BG,WHITE, 
	     NULL);
      break;
    default:
      printf("ACA NUNCA TENDRIA QUE LLEGAR: X=%i,Y=%i,S=%i\n",x,y,mitabla[x][y]);
      break;
   }
}
/*
 *                           BUTTON PRESSED IN ENEMY BOARD
 */
int tutabla_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   int x,y;
   char outbuf[MSGMAXLEN];
   
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;

   if( usuario.play < 3 ) {
      foot_right("Not started yet!");
      return(1);
   }
   else if( usuario.play == 3 ) {
      foot_right("It is not your turn!");
      return(1);
   }
   usuario.play=3;                        /* it is no more your turn */
   foot_right("");
   outbuf[0]=x;
   outbuf[1]=y;
   bnwrite(sock,outbuf,BNHIT,0,0,usuario.numjug);
   bnwrite(sock,outbuf,BNREA,usuario.usrfrom,bnsup,usuario.numjug);
}

/*
 *                               INICIALIZAR VARIABLES
 */
void datos_init( void ) {
   int i,j;

   usuario.play = 0;
   usuario.usrfrom=1;
   for(i=0;i<10;i++) {          /* clean tabla */
      for(j=0;j<10;j++)
      	mitabla[i][j]=0;
   } 
}

/*
 *                                 INICIALIZAR X
 */
void batnav_xv_init( void ) {
   
   int i;
   
   /* crea los colores */
   cms = (Cms) xv_create( XV_NULL,CMS,
			 CMS_SIZE,7,
			 CMS_TYPE, XV_STATIC_CMS,
			 CMS_COLORS,cms_colors,
			 NULL );
   /* ventana principal */
   frame = (Frame) xv_create(NULL, FRAME,
			     XV_HEIGHT,230,
			     XV_WIDTH,800,
			     FRAME_LABEL,"Batalla Naval v"BN_XVER,
			     FRAME_SHOW_FOOTER,TRUE,
			     FRAME_LEFT_FOOTER,"Batalla Naval",
			     FRAME_RIGHT_FOOTER,"(c) 1995,1996",
			     FRAME_SHOW_RESIZE_CORNER,FALSE,
			     NULL);
   

/* panel para los botones quit,play,etc */
   panel = xv_create( frame,PANEL,
		     PANEL_LAYOUT,PANEL_VERTICAL,
		     XV_WIDTH, 100,
		     NULL ); 

/* shell para mi tabla */
   shell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				    WIN_CMS,cms,
				    NULL );
   xv_create( shell, DRAWTEXT,
	     DRAWTEXT_STRING,"My board",
	     XV_WIDTH,ANCHO*10,
	     DRAWTEXT_JUSTIFY,DRAWTEXT_JUSTIFY_CENTER,
	     RECTOBJ_SELECTABLE,FALSE,
	     NULL); 
   array_tile = (Array_tile) xv_create( shell, ARRAY_TILE,
				       XV_Y,20, 
				       XV_WIDTH,ANCHO*10,
				       XV_HEIGHT,LARGO*10,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 1,
				       ARRAY_TILE_COLUMN_GAP, 1,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, mitabla_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);
   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   for(i=0;i<100;i++) {    /* 10 * 10 */
      barcos_ptr[i] = (Drawrect) xv_create( array_tile, DRAWRECT,
					   RECTOBJ_OPS, rectobj_ops,
					   XV_WIDTH,ANCHO,
					   XV_HEIGHT,LARGO,
					   DRAWRECT_BORDER2,1,
					   DRAWOBJ_FILLED,TRUE,
					   RECTOBJ_BG2, WHITE,
					   RECTOBJ_BG, WHITE,
					   RECTOBJ_FG, BLACK,
					   NULL );
   }
   xv_set( shell, 
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) + 20 ,
	  NULL ); 

/* shell para enemy tabla */
   tushell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				      WIN_CMS,cms,
				      NULL );
   drawtext1= (Drawtext) xv_create( tushell, DRAWTEXT,
			DRAWTEXT_STRING,"Enemy board",
			XV_WIDTH,ANCHO*10,
			DRAWTEXT_JUSTIFY,DRAWTEXT_JUSTIFY_CENTER,
			RECTOBJ_SELECTABLE,FALSE,
			NULL);
   array_tile = (Array_tile) xv_create( tushell, ARRAY_TILE,
				       XV_Y,20, 
				       XV_WIDTH,ANCHO*10,
				       XV_HEIGHT,LARGO*10,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 1,
				       ARRAY_TILE_COLUMN_GAP, 1,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, tutabla_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);
   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   for(i=0;i<100;i++) {    /* 10 * 10 */
      tusbarcos_ptr[i] = (Drawrect) xv_create( array_tile, DRAWRECT,
					      RECTOBJ_OPS, rectobj_ops,
					      XV_WIDTH,ANCHO,
					      XV_HEIGHT,LARGO,
					      DRAWRECT_BORDER2,1,
					      DRAWOBJ_FILLED,TRUE,
					      RECTOBJ_BG2, WHITE,
					      RECTOBJ_BG, WHITE,
					      RECTOBJ_FG, BLACK,
					      NULL );
   }
   xv_set( tushell, 
	  XV_X,320,
	  XV_Y,0,
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) + 20 ,
	  NULL ); 

   
   /* creacion de botones */
   panelcito = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING,"Connect",
				 PANEL_NOTIFY_PROC,init_cliente,
				 NULL);
   panelcit2 = (Panel) xv_create(panel,PANEL_BUTTON,
				 PANEL_LABEL_STRING,"Send Board",
				 PANEL_NOTIFY_PROC,play,
				 NULL);
   menu = (Menu) xv_create (NULL, MENU,
			    MENU_GEN_PIN_WINDOW, frame, "Players",
			    MENU_NOTIFY_PROC, menuses,
			    MENU_STRINGS, "Player 1","Player 2","Player 3","Player 4","Player 5",NULL,
			    NULL);
   xv_create( panel, PANEL_BUTTON,
 	     PANEL_LABEL_STRING,"Players",
 	     PANEL_NOTIFY_PROC,nada,
	     PANEL_ITEM_MENU,menu,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Config",
	     PANEL_NOTIFY_PROC,info,
	     NULL); 
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"About",
	     PANEL_NOTIFY_PROC,about,
	     NULL);
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Quit",
	     PANEL_NOTIFY_PROC,quit,
	     NULL);

   
/* 
 * todo lo referente a about
 */
   subframe1=(Frame) xv_create(frame,FRAME,
			       XV_HEIGHT,300,
			       XV_WIDTH,300,
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) 1995,1996",
			       FRAME_LABEL, "About",
			       FRAME_SHOW_RESIZE_CORNER,FALSE,
			       NULL);
   canvas = xv_create( subframe1,CANVAS,
		      CANVAS_X_PAINT_WINDOW,TRUE,
		      XV_VISUAL_CLASS, PseudoColor,
		      WIN_CMS,cms,
		      CANVAS_REPAINT_PROC,about_proc,
		      NULL );
   display = (Display *) xv_get( subframe1, XV_DISPLAY);
   xid = (XID) xv_get(  canvas_paint_window( canvas ) ,XV_XID );
   gc = XCreateGC( display, xid, NULL , &gc_val );
   colors = (unsigned long*) xv_get( canvas,WIN_X_COLOR_INDICES);

/* 
 * todo lo referente a info
 */
   subframe2=(Frame) xv_create(frame,FRAME_CMD,
			       XV_HEIGHT,200,
			       XV_WIDTH,300,
			       FRAME_SHOW_FOOTER,TRUE,
			       FRAME_LEFT_FOOTER,"Batalla Naval",
			       FRAME_RIGHT_FOOTER,"(c) 1995,1996",
			       FRAME_LABEL, "Configuration",
			       NULL);
   panelinfo = (Panel) xv_get( subframe2,FRAME_CMD_PANEL);

/* (void) xv_create( panelinfo, PANEL_BUTTON,
*		    XV_X,50,
*		    XV_Y,150,
*		    PANEL_LABEL_STRING,"Cancel",
*		    PANEL_NOTIFY_PROC,nada,
*		    NULL); */
   xv_create( panelinfo, PANEL_BUTTON,
	     XV_X,130,
	     XV_Y,150,
	     PANEL_LABEL_STRING,"OK",
	     PANEL_NOTIFY_PROC,nada,
	     NULL); 
   xv_create( panelinfo, PANEL_TEXT,
   		          XV_X,20,
                          XV_Y,45,
			  PANEL_VALUE,usuario.server_name,
			  PANEL_LABEL_STRING,"Server name:",
			  PANEL_LAYOUT,PANEL_HORIZONTAL,
			  PANEL_NOTIFY_PROC, server_name,
			  PANEL_CLIENT_DATA, frame,
			  PANEL_VALUE_DISPLAY_LENGTH,20,
			  NULL);
   xv_create( panelinfo, PANEL_NUMERIC_TEXT,
	     XV_X,20,
	     XV_Y,70,
	     PANEL_LABEL_STRING,"Server port:",
	     PANEL_VALUE,usuario.port,
	     PANEL_MAX_VALUE,65535,
	     PANEL_MIN_VALUE,0,
	     PANEL_NOTIFY_PROC, server_port,
	     PANEL_CLIENT_DATA, frame,
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     NULL);
   xv_create( panelinfo, PANEL_TEXT,
	     XV_X,20,
	     XV_Y,95,
	     PANEL_VALUE,usuario.nombre,
	     PANEL_LABEL_STRING,"Your name:",
	     PANEL_LAYOUT,PANEL_HORIZONTAL,
	     PANEL_VALUE_DISPLAY_LENGTH,20,
	     PANEL_NOTIFY_PROC, your_name,
	     PANEL_CLIENT_DATA, frame,
	     NULL);
  
   /*
    * startup x, and run
    */
   window_fit( frame ); 

}

/*
 *                        main()
 */

void main( int argc, char *argv[] )
{
   xv_init( XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
   if(argc>1) strcpy( usuario.server_name,argv[1]);
   else strcpy( usuario.server_name,"localhost");
   if(argc>2) usuario.port=atoi(argv[2]);
   else usuario.port=1995;        /* well known port */
   if(argc>3) bnsup=argv[3][0];
   strcpy( usuario.nombre,getlogin());
   datos_init();                  /* inicializa variables */
   batnav_xv_init();              /* inicializa entorno y juego */
   init_cliente();
   xv_main_loop(frame);
}
