/*****************************************************************************/
/*                                                                           */
/*                            Batalla Naval                                  */
/*                                                                           */
/*                               Servidor                                    */
/*                                                                           */
/*               por Ricardo Quesada y Sebastian Cativa Tolosa               */
/*                                                                           */
/*****************************************************************************/


/* INCLUDES */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdio.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

/* DEFINES */
#define SERVERSION  "0.13"
#define CLIVERSION  ""
#define BATNAV_TCP_PORT 1066
#define MAXPLAYER 5
#define TRUE 0
#define FALSE 1
#include "protocol.h"                  /* definicion del protocolo */
#include "criollo.h"                   /* mensajes en castellano */


/* VARIABLES GLOBALES y STRUCT y TYPEDEF y ... */

typedef struct tabla_typ {
   int  x,y;
   char p[10][10];
} tabla,*tabla_ptr;

struct datos {
   int nro_tot[MAXPLAYER];     /* 1 int por jugador */
   int nro_fd[MAXPLAYER];      /* 1 file descriptor por jugador */
   tabla table[MAXPLAYER];
   int hitx,hity;
} mimem;

/*****************************************************************************/
/*                             FUNCIONES                                     */
/*****************************************************************************/
/***************************************************************************** 
 *                           funciones CHEQUEO DE ALGORITOMO
 ****************************************************************************/
/* points to next pos.return TRUE . FALSE in case there are no more ships */
/* this rutine uses global x,y */
int nextbarco(int num_jug) {
   if(mimem.table[num_jug].x==9) {
     mimem.table[num_jug].x=0;
     mimem.table[num_jug].y++;
   }
   else mimem.table[num_jug].x++;
   if( mimem.table[num_jug].y==10 ) return FALSE;
   else return TRUE;
}    	
/* return TRUE if in (x,y) hay barco. else FALSE */
/* this rutine uses local x,y */
int haybarco( int x, int y,int num_jug) {
   if( (x < 0) || ( x > 9 ) || (y < 0) || (y >9 ) )
     return FALSE;
   if( mimem.table[num_jug].p[x][y]==1 )
     return TRUE;
   if( mimem.table[num_jug].p[x][y]==0 )
     return FALSE;
   printf("BTSERVER: BUG or someone is cheating in haybarco()\n");
   return FALSE;
}

/* return TRUE if table is OK .else return FALSE  */
int algoritmo(int num_jug)  {
   int i,xx,yy,barcos,subarco;
   int barquitos[4];
   
   for(i=0;i<4;i++) barquitos[i]=0;
   
   /* global x,y */
   mimem.table[num_jug].x=0;
   mimem.table[num_jug].y=0;
   barcos=0;
   
   for(;;) {
      if(haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y,num_jug)==FALSE) {
	 if(nextbarco(num_jug)==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       return FALSE;
	    }
	 }
      }
      else { 
	 if( haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y,num_jug)==TRUE )  {
	    if( (haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y-1,num_jug)==FALSE) && (haybarco(mimem.table[num_jug].x-1,mimem.table[num_jug].y,num_jug)==FALSE) ) {
	       subarco=1;
	       barcos++;
	       xx=mimem.table[num_jug].x;yy=mimem.table[num_jug].y;
	       
	       for(;;) {
		  if( (haybarco(mimem.table[num_jug].x-1,mimem.table[num_jug].y+1,num_jug)==TRUE) || (haybarco(mimem.table[num_jug].x+1,mimem.table[num_jug].y+1,num_jug)==TRUE) ) {
		     return FALSE;
		  }
		  if( (haybarco(mimem.table[num_jug].x+1,mimem.table[num_jug].y,num_jug)==FALSE) && (haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y+1,num_jug)==FALSE) ) {
		     mimem.table[num_jug].x=xx;
		     mimem.table[num_jug].y=yy;          /* restauro pos */
		     barquitos[subarco-1]++; /* update cantidad de tama;os de barcos */
		     break;
		  }
		  else if( haybarco(mimem.table[num_jug].x+1,mimem.table[num_jug].y,num_jug) == TRUE ) {
		     subarco++;
		     barcos++;
		     mimem.table[num_jug].x++;
		  }          
		  else if( haybarco(mimem.table[num_jug].x,mimem.table[num_jug].y+1,num_jug) == TRUE ) {
		     mimem.table[num_jug].y++;
		     barcos++;
		     subarco++;
		  }
		  if( subarco > 4) {
		     return FALSE;
		  }
	       } /* for(;;) */
	    } /* if(haybarco(x,y-1)==FALSE) */
	 } /* if(haybarco(x,y)==TRUE) */
	 if(nextbarco(num_jug)==FALSE) {
	    if( (barquitos[3]==1) && (barquitos[2]==2) && (barquitos[1]==3) && (barquitos[0]==4) ) {
	       return TRUE;         /* tabla bien puesta */
	    }
	    else {
	       return FALSE;
	    }
	 } /* if nextbarco()==FALSE) */
      } /* else */
   } /* for(;;) */
} /* void algoritomo() */

/***************************************************************************
 *            FUNCIONES RELACIONADAS CON LAS TABLAS 
 ***************************************************************************/
int wtable( int jug,struct protocolo *proto ) {
   int i,x,y;
   for(i=0,x=0,y=0;i<100;i++) {
      mimem.table[jug].p[x][y]=proto->bnpmsg[i];
      x++;
      if(x==10) {
	 x=0;y++;
      }
   }
   return( algoritmo(jug));
}


/***************************************************************************** 
 *                           funciones TCP/IP
 ***************************************************************************/
size_t bnwrite(int fde,char *buf,char tip0,char tip1,char tip2,char jugyo ) {
   struct protocolo proto;
   proto.bnptip0=tip0;
   proto.bnptip1=tip1;
   proto.bnptip2=tip2;
   proto.jugador=jugyo;
   strcpy(proto.bnpmsg,buf);
   strcpy(proto.bnphead,BNPHEAD);
   return( write(fde,&proto,MAXLEN) ) ;
}
/***************************************************************************** 
 *                           funciones CONTROL DE CHILD Y JUEGO
 ***************************************************************************/
int play_batnav( int fd) {
   int i;
   char outbuf[MSGMAXLEN];
   struct protocolo proto;

   if( read(fd,&proto,MAXLEN) < 0)
     return(-1);

   switch (proto.bnptip0) {
    case BNREA:
      break;
    case BNWRI:
      if( wtable(proto.jugador-1,&proto)==TRUE)
      	bnwrite(fd,outbuf,BNTOK,0,0,0);
      else {
	 bnwrite(fd,outbuf,BNTXX,0,0,0);
      }
      break;
    case BNJUG:
      for(i=0;i<MAXPLAYER;i++) {
	 if(mimem.nro_tot[i]==0)
	   break;
      }
      mimem.nro_tot[i]=1;
      mimem.nro_fd[i]=fd;
      gethostname(outbuf,MSGMAXLEN);
      bnwrite(fd,outbuf,BNJUG,i+1,0,i+1);
      printf("server says: your are player number %i at %s\n",i+1,outbuf);
      break;
    case BNTST:
      break;
    case BNEXT:
      i=proto.jugador-1;
      mimem.nro_tot[i]=0;
      break;
    case BNHIT:
      outbuf[0]=proto.bnpmsg[0];
      outbuf[1]=proto.bnpmsg[1];
      for(i=0;i<MAXPLAYER;i++) {
	 if(mimem.nro_tot[i]==1)
	   bnwrite(mimem.nro_fd[i],outbuf,BNHIT,0,0,0);
      }
      break;
    case BNSTR:
      for(i=0;i<MAXPLAYER;i++) {
	 if(mimem.nro_tot[i]==1)
	   bnwrite(mimem.nro_fd[i],outbuf,BNSTR,0,0,0);
      }
      break;
    default:
      break;
   }
}


/*****************
 * main function *
 *****************/
void main() {
   FILE **stream;
   fd_set test_set, ready_set;
   int sock,fd,client_len;
   int max_fd,i;
   struct rlimit limit_info;
   struct sockaddr_in server;
   struct sockaddr client;

   printf("%s",btmsg[MSG_SCOP]);

   /* Set default parameters */
   for(i=0;i<MAXPLAYER;i++) {
      mimem.nro_tot[i]=0;
   }

   if(getrlimit(RLIMIT_NOFILE,&limit_info) <0)  {
      perror("btserver: getrlimit error");
      exit(1);
   }
   
   stream = (FILE **) malloc(limit_info.rlim_cur * sizeof(FILE *));
   if(stream == NULL) {
      fprintf(stderr,"btserver: malloc error: not enough mem ?\n");
      exit(1);
   }
   sock=socket(AF_INET,SOCK_STREAM,0);
   if(sock <0) {
      perror("btserver: socket error");
      exit(1);
   }
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=htonl(INADDR_ANY);
   server.sin_port=htons(BATNAV_TCP_PORT);
   if(bind(sock,(struct sockaddr *)&server,sizeof(server))< 0) {
      perror("btserver: bind error:");
      exit(2);
   }
   listen(sock,5);
   max_fd=sock;
   FD_ZERO(&test_set);
   FD_SET(sock,&test_set);
   while(1) {
      memcpy(&ready_set,&test_set,sizeof(test_set));
      select(max_fd+1,&ready_set,NULL,NULL,NULL);
      if(FD_ISSET(sock,&ready_set)) {
	 client_len = sizeof(client);
	 fd=accept(sock,(struct sockaddr *)&client,&client_len);
	 FD_SET(fd,&test_set);
	 if(fd>max_fd) max_fd=fd;
      }
      for(fd=0;fd<=max_fd;fd++) {
	 if((fd!=sock) && FD_ISSET(fd,&ready_set)) {
	    if(play_batnav(fd)<0) {
	       close(fd);
	       FD_CLR(fd,&test_set);
	    }
	 } /* if(fd!=sock */
      } /* for(fd=0) */
   } /* while(1) */
}
