/************************************************************************
 * BATALLA NAVAL.
 * Cliente para X
 * por Ricardo Quesada
 * version: 0.1
 ***********************************************************************/

#include <X11/Xlib.h>
#include <xview/xview.h>
#include <xview/window.h> 
#include <xview/openmenu.h> 
#include <xview/panel.h>
#include <xview/canvas.h>
#include <xview/xv_xrect.h>
#include <xview/frame.h>
#include <xview/cms.h>
#include <sspkg/canshell.h>
#include <sspkg/array.h>
#include <sspkg/drawobj.h>

/*
 *                DEFINES
 */ 
#define BN_XVER "0.01"
#define LARGO 20
#define ANCHO 20
#define COLOR1A 1
#define COLOR1B 3
#define COLOR2A 15
#define COLOR2B 14


/* 
 *                   VARIABLES GLOBALES
 */
Frame frame;
Panel panel;
Canvas canvas;
Drawrect proto_drawrect;                 /* sspkg */
Array_tile array_tile;                   /* sspkg */
Rectobj_ops *rectobj_ops;                /* sspkg */
Canvas_shell shell;
Cms cms;
int mitabla[9][9];

/* 
 *                        FUNCIONES
 */

void nada( void ) {
   printf("apretastes el boton CONECTAR\n");
}

void quit( ) {
   xv_destroy_safe( frame );
}

void callback_fn( Xv_window pw, Event *e, Canvas_shell cs, Rectobj r, int state)
{
   int x,y;
   x=xv_get(r,XV_X) / ANCHO;
   y=xv_get(r,XV_Y) / LARGO;
   
   switch( mitabla[x][y] )  {
    case 0:
      mitabla[x][y]=1;
      xv_set(r,
	     RECTOBJ_BG2,COLOR2A,
	     RECTOBJ_BG,COLOR2B,
	     NULL);
      printf("X=%i,Y=%i,S=%i\n",x,y,mitabla[x][y]);
      break;
    case 1:
      mitabla[x][y]=0;
      xv_set(r,
	     RECTOBJ_BG2,COLOR1A,
	     RECTOBJ_BG,COLOR1B,
	     NULL);
      printf("X=%i,Y=%i,S=%i\n",x,y,mitabla[x][y]);
      break;
    default:
      printf("ACA NUNCA TENDRIA QUE LLEGAR: X=%i,Y=%i,S=%i\n",x,y,mitabla[x][y]);
      break;
   }
}

void batnav_xv_init( void ) {
   
   int i,j;

   for(i=0;i<10;i++) {          /* clean tabla */
      for(j=0;j<10;j++)
      	mitabla[i][j]=0;
   }
   
   cms = (Cms) xv_create( XV_NULL, CMS,
			 CMS_CONTROL_CMS,TRUE,
			 NULL );
   
   frame = (Frame)xv_create(NULL, FRAME,
			    XV_HEIGHT,200,
			    XV_WIDTH,400, 
			    FRAME_LABEL,"Batalla Naval",
			    FRAME_SHOW_FOOTER,FALSE,
			    NULL);
   
   panel = xv_create( frame,PANEL,
		     PANEL_LAYOUT,PANEL_VERTICAL,
		     XV_WIDTH, 100,
		     NULL ); 
   
   shell = (Canvas_shell) xv_create( frame, CANVAS_SHELL,
				    WIN_CMS,cms,
				    NULL );
   				    
   array_tile = (Array_tile) xv_create( shell, ARRAY_TILE,
				       ARRAY_TILE_N_ROWS, 10,
				       ARRAY_TILE_N_COLUMNS, 10,
				       ARRAY_TILE_ROW_GAP, 0,
				       ARRAY_TILE_COLUMN_GAP, 0,
				       ARRAY_TILE_ROW_HEIGHT, 1,
				       ARRAY_TILE_COLUMN_WIDTH, 1,
				       RECTOBJ_EVENT_PROC, NULL,
				       NULL );
				       
   proto_drawrect = (Drawrect) xv_create( XV_NULL, DRAWRECT,
					 RECTOBJ_SINGLE_CLICK_PROC, callback_fn,
					 RECTOBJ_EVENT_PROC, rectobj_button_event_proc,
  					 NULL);

   rectobj_ops = (Rectobj_ops*) xv_get(proto_drawrect,RECTOBJ_OPS);
   
   for(i=0;i<100;i++) {    /* 10 * 10 */
      xv_create( array_tile, DRAWRECT,
		RECTOBJ_OPS, rectobj_ops,
		XV_WIDTH,ANCHO,
		XV_HEIGHT,LARGO,
		DRAWRECT_BORDER2,2,
		DRAWOBJ_FILLED,TRUE,
		RECTOBJ_BG2, COLOR1A,
		RECTOBJ_BG, COLOR1B,
		NULL );
   }
   
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Connect",
	     PANEL_NOTIFY_PROC,nada,
	     NULL);

   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Play",
	     PANEL_NOTIFY_PROC,nada,
	     NULL);
   
   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"View",
	     PANEL_NOTIFY_PROC,nada,
	     NULL);

   xv_create(panel,PANEL_BUTTON,
	     PANEL_LABEL_STRING,"Quit",
	     PANEL_NOTIFY_PROC,quit,
	     NULL);

   xv_set( shell,
	  XV_WIDTH, xv_get( array_tile, XV_WIDTH) ,
	  XV_HEIGHT, xv_get( array_tile, XV_HEIGHT) ,
	  NULL );
   window_fit( frame );
   
   xv_main_loop(frame);

}

/*
 *                        main()
 */

void main( int argc, char *argv[] )
{
   
   xv_init( XV_INIT_ARGC_PTR_ARGV, &argc, argv, NULL);
   batnav_xv_init();
   
}
