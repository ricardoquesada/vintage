 modectl equ     43h
timer2  equ     42h
portb   equ     61h
 delay   proc                            ; DELAY = 10ms
        push    cx
        mov     cx,42                   ; Number of REPs
dly_a:  mov     al,98h                  ; 16-Bit binary counter, STS mode
        out     modectl,al              ; RL-Least only, Counter 2
        xor     al,al
        out     timer2,al
        in      al,portb
        or      al,1
        out     portb,al
res:    in      al,timer2
        cmp     al,1
        ja      res
        xor     al,al
        out     timer2,al
        loop    dly_a
        pop     cx
        ret
delay   endp
 Saludos,
 
        Gustavo 

--- DB B2208sl/004789
 * Origin: G Corner (4:900/203)
