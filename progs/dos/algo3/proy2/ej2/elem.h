//
// Definiciones Clase Elem
//

#ifndef _ELEM_
#define _ELEM_

#define MAXCAMPO 40
class Elem
{
public:
	Elem();
	Elem(char*, char*, char*, char*, char*);
	char* Apellido();
	char* Nombre();
	char* Telefono();
	char* Direccion();
	char* E_mail();
	int Menor(int, const Elem&) const;
	int Igual(int, const Elem&) const;
	Elem& operator=(const Elem&);
	int operator==(const Elem&) const;
	friend ostream& operator<<(ostream&, const Elem&);
	friend istream& operator>>(istream&, Elem&);
private:
	char apellido[MAXCAMPO];
	char nombre[MAXCAMPO];
	char telefono[MAXCAMPO];
	char direccion[MAXCAMPO];
	char e_mail[MAXCAMPO];
};

#endif