#include <iostream.h>
#include <stdlib.h>
#include "elem.cpp"
#include "kdtree.cpp"
#include "kdt_nodo.cpp"

void main(void)
{
	CKDT<Elem> qt=3;

	cout << "Este es un programa de prueba del KDT\n";
	cout << "Tama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
	cout << "Ahora se insertan 99 elementos en el arbol\n";
	char cd[10];
	randomize();
	for (int i=50; i<100; i++)
	{
		itoa( i, cd, 10);
		cout << cd << ";";
		Elem e(cd, cd, cd, cd, cd);
		qt.Insertar(e);
	}
	for (i=1; i<50; i++)
	{
		itoa( i, cd, 10);
		cout << cd << ";";
		Elem e(cd, cd, cd, cd, cd);
		qt.Insertar(e);
	}


	cout << "\nLISTA: " << qt;

	Elem f0("50","50","50","","");
	Elem f1("51","51","51","","");
	Elem f2("52","52","52","","");
	Elem f3("53","53","53","","");
	Elem f4("54","54","54","","");
	Elem f5("55","55","55","","");
	qt.Insertar(f1);
	qt.Insertar(f2);
	qt.Insertar(f3);
	qt.Insertar(f4);
	qt.Insertar(f5);

	qt.Borrar(f0);
	qt.Borrar(f1);
	qt.Borrar(f2);
	qt.Borrar(f3);
	qt.Borrar(f4);
	qt.Borrar(f5);
/*
	for (i=1; i<100; i++)
	{
		itoa(i, cd, 10);
		cout << cd << ";";
		Elem e(cd, cd, cd, "", "");
		cout << e << qt.Borrar(e);
		i++;
	} */

	cout << "\nBuscar " << (qt.Buscar(f1)?"OK":"ERROR") << " elemento: " << f1;
	qt.BusRango(f1,0,f1,0,"salida.txt");
	cout << "\nBorrar " << (qt.Borrar(f1)?"OK":"ERROR") << " elemento: " << f1;

	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";


	Elem eb1("18","18","18","","");
	Elem eb2("20","20","20","","");
	qt.BusRango(eb1, 7, eb2, 7, "salida2.txt");

	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
	cout << "Ahora se limpia el arbol\n";
	qt.Limpiar();
	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
}