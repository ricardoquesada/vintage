#include <iostream.h>
#include <fstream.h>
#include <stdlib.h>
#include "elem.cpp"
#include "kdt_nodo.h"
#include "kdtree.cc"

void main(void)
 {
	KDT<Elem> k(3);

	cout << "Este es un programa de prueba del KDT\n";
	ifstream handle ("salida.txt", ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";
	handle >> k;
	cout << "\nLISTA: " << k;
	cout << "\nTama�o: " << k.Tamano() << "\n";
	cout << "Vacio?: " << (k.Vacio()?"NO":"SI") << "\n";
	handle.close();
}