//
// Implementacion de la Clase CKDT
//

#include <fstream.h>
#include <stdlib.h>
#include <stdio.h>
#include "kdtree.h"
#include "kdt_nodo.cpp"

/******************************************************************************
				 INSERTAR
******************************************************************************/
template<class T>
int
CKDT<T>::Insertar(const T&t)
{
	return Insertar_Recur( t, &raiz, 1 );
}

template<class T>
int
CKDT<T>::Insertar_Recur(const T&t, kdt_nodo<T> **puntero, int disc)
{
	if( *puntero==NULL)
	{
		*puntero = new kdt_nodo<T>;
		Error( *puntero==NULL,"Insertar_Recur: No hay  mas memoria\n" );
		(*puntero)->datos = t;
		(*puntero)->izq = NULL ;
		(*puntero)->der = NULL ;
		tamano++;
		return 1;
	}
	else
	{
		if( (*puntero)->datos == t )
			return 0;
		else
		{
			if( Menor( disc, t , (*puntero)->datos ))
				return (Insertar_Recur( t, &(*puntero)->izq, nextdisc(disc) ));
			else
				return (Insertar_Recur( t, &(*puntero)->der, nextdisc(disc) ));
		}
	}
}

/******************************************************************************
				     BUSCAR
******************************************************************************/
template<class T>
int
CKDT<T>::Buscar(T&t)
{
	return Buscar_Recur( t, &raiz, 1 );
}

template<class T>
int
CKDT<T>::Buscar_Recur(T&t, kdt_nodo<T> **puntero, int disc )
{
	if( *puntero==NULL)			// Nodo no encontrado
		return 0;
	else
	{
		if( (*puntero)->datos == t )   // Nodo encontrado
		{
			t = (*puntero)->datos ;
			return 1;
		}
		else	// Veo si tengo que seguir por el izq o der
		{
			if( Menor( disc, t , (*puntero)->datos ))
				return (Buscar_Recur( t, &(*puntero)->izq, nextdisc(disc) ));
			else
				return (Buscar_Recur( t, &(*puntero)->der, nextdisc(disc) ));
		}
	}
}



/*****************************************************************************
				  BORRAR
*****************************************************************************/
template<class T>
int
CKDT<T>::Borrar(const T&t)
{
	kdt_nodo<T>** ptr_temp;
	int disc=1;

	ptr_temp = Borrar_BuscarNodo( t, &raiz, &disc );

	if ( ptr_temp==NULL )			// Nodo no encontrado
		return 0;

	Borrar_Recur( ptr_temp, disc ) ;	// Nodo encontrado
	tamano--;
	return 1;
}

// Simple funcion que busca un nodo
template<class T>
kdt_nodo<T>**
CKDT<T>::Borrar_BuscarNodo( const T& t, kdt_nodo<T>** ptr, int *disc )
{

	if( (*ptr) == NULL )			// Nodo Nulo, no encontrado
		return NULL ;
	if( (*ptr)->datos == t ) 		// Nodo Encontrado
		return ptr ;

	// Si es menor voy hacia la izquierda
	if( Menor( (*disc), t ,(*ptr)->datos ) )
	{
		(*disc) = nextdisc( (*disc) );
		return (Borrar_BuscarNodo(t, &(*ptr)->izq, disc ) );
	}
	// Si es mayor voy hacia la derecha
	else
	{
		(*disc) = nextdisc( (*disc) );
		return (Borrar_BuscarNodo(t, &(*ptr)->der, disc ) );
	}
}

// Funcion que borra el nodo que le dicen y arregla su subarbol.
template<class T>
void
CKDT<T>::Borrar_Recur( kdt_nodo<T>** ptr, int disc )
{
#define CKDT_MAYOR 0
#define CKDT_MENOR 1
	kdt_nodo<T>** ptr_tmp ;
	int disc_temp = disc ;

	if( (*ptr)->der==NULL && (*ptr)->izq==NULL)
	{
		delete (*ptr);
		(*ptr)=NULL;				// Su padre no tiene hijo
		return ;
	}
	else if( (*ptr)->der != NULL)
	{	// Busca el nodo con menor disc dentro del arbol derecho
		ptr_tmp = Borrar_BuscarDisc( &(*ptr)->der, disc, CKDT_MENOR, &disc_temp );
		(*ptr)->datos = (*ptr_tmp)->datos;	// No borra el nodo, solo
							// copia los datos.
		Borrar_Recur( ptr_tmp, disc_temp );
		return ;
	}
	else if( (*ptr)->izq!=NULL)
	{	// Busca el nodo de con disc mayor en el arbol izq
		ptr_tmp = Borrar_BuscarDisc( &(*ptr)->izq, disc, CKDT_MAYOR, &disc_temp );
		(*ptr)->datos = (*ptr_tmp)->datos;	// No borra el nodo, solo
							// copia los datos.
		Borrar_Recur( ptr_tmp, disc_temp );
		return ;
	}
}

// Esta funcion busca el mayor o menor disc_valor de un arbol
// Devuelve el puntero al nodo encontrado.
// Devuelve el discriminado del nodo encontrado en *disc_temp
template<class T>
kdt_nodo<T>**
CKDT<T>::Borrar_BuscarDisc( kdt_nodo<T> **ptr, int disc, int clausula, int *disc_temp )
{
	// Variables auxiliares
	kdt_nodo<T>** ptr_tmp;
	kdt_nodo<T>** ptr_izq;
	kdt_nodo<T>** ptr_der;
	int disc_a ;
	int disc_izq ;
	int disc_der ;

	ptr_der = NULL ;
	ptr_izq = NULL ;


	(*disc_temp) = nextdisc( (*disc_temp) );
	disc_a = (*disc_temp);
	disc_izq = (*disc_temp);
	disc_der = (*disc_temp);

	// Si no tengo hijos me devuelvo yo, no ?
	if( (*ptr)->izq==NULL && (*ptr)->der==NULL )
		return ptr ;

	// En el caso que disc sea igual a disc_temp
	// puedo podar el arbol. Esto optimiza un poco este algoritmo.
	if( disc == (*disc_temp) ) 			// disc == disc_temp
	{
		if(clausula==CKDT_MENOR) 		// Clausula CKDT_MENOR
		{
			if( (*ptr)->izq!=NULL )
			{
				// Solo pregunto por mis hijos izquierdos
				ptr_izq = Borrar_BuscarDisc( &(*ptr)->izq,disc,clausula, &disc_izq );
			}
			else if ( (*ptr)->izq==NULL )
			{
				// Se que el mi hermano derecho tiene
				// un mayor disc, entonces no pregunto por
				// el, y me devuelvo yo
				return ptr ;
			}
		}
		else if(clausula==CKDT_MAYOR)		// Clausula CKDT_MAYOR
		{
			if( (*ptr)->der!=NULL )
			{
				// Solo pregunto por mis hijos derechos
				ptr_der = Borrar_BuscarDisc( &(*ptr)->der,disc,clausula, &disc_der );
			}
			else if ( (*ptr)->der==NULL )
			{
				// Se que el mi hermano izquierdo tiene
				// un menor disc, entonces no pregunto por
				// el, y me devuelvo yo
				return ptr ;
			}
		}
	}
	// En caso de tener disc!=disc_temp
	// tengo que preguntar en las dos ramas ( izq y der )
	// ya que no puede acotar.
	else if( disc!= (*disc_temp) )			// disc != disc_temp
	{
		if( (*ptr)->izq!=NULL )
			ptr_izq = Borrar_BuscarDisc( &(*ptr)->izq,disc,clausula, &disc_izq );
		if( (*ptr)->der!=NULL )
			ptr_der = Borrar_BuscarDisc( &(*ptr)->der,disc,clausula, &disc_der );
	}

	ptr_tmp = ptr ;					// Valor que necesito
	disc_a = (*disc_temp);				// despues

	// Si ptr_der es NULL significa que no me interesa
	// preguntar por el hijo derecho aunque exista
	if( ptr_der!=NULL )
	{
		if(clausula==CKDT_MAYOR)
		{
			// Elijo con que nodo y discriminador me quedo
			ptr_tmp = ( Mayor( disc, (*ptr)->datos,	(*ptr_der)->datos ) ? ptr : ptr_der );
			disc_a  = ( Mayor( disc, (*ptr)->datos, (*ptr_der)->datos ) ? (*disc_temp): disc_der );
		}
		else if(clausula==CKDT_MENOR)
		{
			// Elijo con que nodo y discriminador me quedo
			ptr_tmp = (!Menor( disc, (*ptr)->datos, (*ptr_der)->datos ) ? ptr_der : ptr );
			disc_a  = (!Menor( disc, (*ptr)->datos, (*ptr_der)->datos ) ? disc_der: (*disc_temp) );
		}
	}

	// Si ptr_izq es NULL significa que no me interesa
	// preguntar por el hijo izq aunque exista
	if( ptr_izq!=NULL )
	{
		if(clausula==CKDT_MAYOR)
		{
			// No hace falta pregunta por Mayor ya que
			// no modifica nada
			// Elijo con que nodo y discriminador me quedo
			if(!Mayor(disc,(*ptr_tmp)->datos, (*ptr_izq)->datos ))
			{
				ptr_tmp = ptr_izq ;
				disc_a = disc_izq ;
			}
		}
		else if(clausula==CKDT_MENOR)	// clausula CKDT_MENOR
		{
			// No hace falta pregunta por Menor ya
			// que no modifica nada
			// Elijo con que nodo y discriminador me quedo
			if(!Menor(disc, (*ptr_tmp)->datos, (*ptr_izq)->datos ))
			{
				ptr_tmp = ptr_izq  ;
				disc_a  = disc_izq ;
			}
		}
	}
	(*disc_temp) = disc_a ;    // Devuelvo en disc_temp el disc del nodo
	return ptr_tmp;		   // Devuelvo nodo encontrado
}

/*****************************************************************************
				  BUSRANGO
*****************************************************************************/
template<class T>
void
CKDT<T>::BusRango(T& tinf, int cinf,
	      T& tsup, int csup,
	      char *filename )
{
	ofstream fh( filename, ios::out );		// File Handle
	BusRango_Recur( tinf, cinf, tsup, csup, fh, raiz, 1 );
	fh.close();
}

// Funcion que hace la busqueda por rango.
template<class T>
void
CKDT<T>::BusRango_Recur( T& tinf, int cinf,
			T& tsup, int csup,
			ofstream& fh, kdt_nodo<T>* ptr, int disc )
{
	int a;
	int cinf_tmp;
	int csup_tmp;
	int ctmp;
	int ctmp2;

	// Si soy un puntero a nada, return, no ?
	if(ptr==NULL)
		return;

	// Primero pregunto por el nodo actual
	a=0;

	csup_tmp = csup;
	cinf_tmp = cinf;
	for(int i=1; i<=maxdisc; i++ )
	{
		ctmp = cinf_tmp & 1;
		if(ctmp==1 )
		{
			if(ptr->datos.Menor(i, tinf ))
				break;
		}
		ctmp = csup_tmp & 1;
		if(ctmp==1)
		{
			if(!ptr->datos.Menor(i, tsup ) && !ptr->datos.Igual(i, tsup ))
				break;
		}

		csup_tmp >>= 1;
		cinf_tmp >>= 1;
		a++;
	}

	// Estoy en el rango ?
	if( a == maxdisc )
		fh << ptr->datos ;

	// Si no tengo mas hijos listo, no ?
	if( ptr->izq == NULL && ptr->der==NULL )
		return ;

	ctmp = cinf ;
	ctmp2 = csup ;
	for(a=1;a<=disc;a++)
	{
		cinf_tmp = ctmp &1;
		csup_tmp = ctmp2 &1;
		ctmp >>= 1;
		ctmp2 >>= 1 ;
	}
	// pregunto si estoy acodo por cinf o csup
	if( cinf_tmp ==0 && csup_tmp ==0 )		// No estoy acotado
	{
		// Voy a izquierda y a derecha
		BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->izq, nextdisc(disc) );
		BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->der, nextdisc(disc) );
		return ;
	}
	else if( cinf_tmp ==0 && csup_tmp ==1 )		// Acotado sup
	{
		// Si mi valor es > que cota voy a la izq
		if( !ptr->datos.Menor(disc, tsup ) && !ptr->datos.Igual(disc, tsup ))
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->izq, nextdisc(disc) );
			return ;
		}
		// Si mi valor es <= que cota voy a la izq y der
		else
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->izq, nextdisc(disc) );
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->der, nextdisc(disc) );
			return ;
		}
	}
	else if( cinf_tmp ==1 && csup_tmp ==0 )		// Acotado inf
	{
		// Si mi valor es <= que cota voy a la der
		if( ptr->datos.Menor(disc, tinf ) )
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->der, nextdisc(disc) );
			return ;
		}
		// Si mi valor es > que cota voy a la izq y der
		else
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->izq, nextdisc(disc) );
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->der, nextdisc(disc) );
			return ;
		}
	}
	else	// Acotado inf y sup
	{
		// Si mi valor es > que cota sup voy a izq
		if(  !ptr->datos.Menor(disc, tsup ) && !ptr->datos.Igual(disc, tsup ))
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->izq, nextdisc(disc) );
			return ;
		}
		// Si mi valor es < que cota inf voy a der
		else if( ptr->datos.Menor(disc, tinf ) )
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->der, nextdisc(disc) );
			return ;
		}
		// Si mi valor esta en el medio de las cotas voy a izq y der
		else
		{
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->izq, nextdisc(disc) );
			BusRango_Recur( tinf, cinf, tsup, csup, fh, ptr->der, nextdisc(disc) );
			return ;
		}
	}
}

/***************************************************************************
				LIMPIAR
***************************************************************************/
template<class T>
void
CKDT<T>::Limpiar()
{
	BorrarTodo( raiz );
	raiz = NULL ;
	tamano = 0;
}
template<class T>
void
CKDT<T>::BorrarTodo( kdt_nodo<T> *ptr )
{
	if(ptr==NULL )
		return ;
	else
	{
		BorrarTodo( ptr->izq );
		BorrarTodo( ptr->der );
		delete ptr;
	}
}


template<class T>
int
CKDT<T>::Tamano()
{
	return tamano;
}

template<class T>
int
CKDT<T>::Vacio()
{
	return ( tamano!=0 );
}


template<class T>
CKDT<T>::CKDT()
{
	tamano = 0;
	maxdisc = 1;
	raiz = NULL;
}

template<class T>
CKDT<T>::CKDT( int kdim )
{
	Error(kdim<1, "La dimension no es correcta\n");
	tamano = 0;
	maxdisc = kdim ;
	raiz = NULL;
}

template<class T>
CKDT<T>::~CKDT()
{
	BorrarTodo( raiz );
}

// Copy constructor
template<class T>
CKDT<T>::CKDT(const CKDT<T>&k)
{
	tamano=k.tamano;
	maxdisc=k.maxdisc;
	raiz=Copiar(k.raiz);
}

// Operador de asignacion
template<class T>
CKDT<T>& CKDT<T>::operator=( const CKDT<T> &k )
{
	BorrarTodo(raiz);
	tamano=k.tamano;
	maxdisc=k.maxdisc;
	raiz=Copiar(k.raiz);
	return *this;
}

template <class T>
ostream& operator<<( ostream& salida, const CKDT<T>& k)
{
	RecorrerTodo( k.raiz, k, salida );
	return salida;
}

template <class T>
istream& operator>>( istream& entrada, CKDT<T>& k)
{
	T elemento;
	int res;

	entrada >> elemento;	// inserta todos los elementos en el kdt
	while (entrada)
	{
		res=k.Insertar(elemento);
		Error(!res, "No se pudo insertar el elemento");
		entrada >> elemento;
	}
	return entrada;
}

// Funcion que dice cual es el siguiente discriminador
template<class T>
int
CKDT<T>::nextdisc( int disc)
{
	if ( disc >= maxdisc )
		return 1;
	else
		return ++disc;
}

// Menor
//  IN: clave, nodo1, nodo2
// OUT:  1 si nodo 1 es MENOR que nodo2
//       0 si nodo 1 es MAYOR que nodo2
template<class T>
int
CKDT<T>::Menor( int k, const T &ptr1, const T &ptr2 )
{
	int i,j;

	for(i=k;i<=maxdisc;i++)
	{
		j = ptr1.Igual( i, ptr2 );
		if(j!=1)
		{
			j = ptr1.Menor( i, ptr2 );
			if(j==1)
				return 1;
			else
				return 0;
		}
	}
	for(i=1;i<k;i++)
	{
		j = ptr1.Igual( i, ptr2 );
		if(j!=1)
		{
			j = ptr1.Menor( i, ptr2 );
			if(j==1)
				return 1;
			else
				return 0;
		}
	}
	return 0;
}



// Mayor
//  IN: clave, nodo1, nodo2
// OUT:  1 si nodo 1 es MAYOR que nodo2
//       0 si nodo 1 es MENOR que nodo2
template<class T>
int
CKDT<T>::Mayor( int k, const T &ptr1, const T &ptr2 )
{
	int i,j;

	for(i=k;i<=maxdisc;i++)
	{
		j = ptr1.Igual( i, ptr2 );
		if(j!=1)
		{
			j = ptr1.Menor( i, ptr2 );
			if(j==1)
				return 0;
			else
				return 1;
		}
	}
	for(i=1;i<k;i++)
	{
		j = ptr1.Igual( i, ptr2 );
		if(j!=1)
		{
			j = ptr1.Menor( i, ptr2 );
			if(j==1)
				return 0;
			else
				return 1;
		}
	}
	return 0;
}
template <class T>                 		// fc. recursiva que copia
kdt_nodo<T>* CKDT<T>::Copiar(kdt_nodo<T>* actual)	// todos los nodos del arbol
{
	kdt_nodo<T>* ele;

	if(actual != NULL)
	{
		ele=new kdt_nodo<T>;
		Error(!ele,"No hay mas memoria");
		ele->datos=actual->datos;
		ele->izq=Copiar(actual->izq);
		ele->der=Copiar(actual->der);
		return ele;
	}
	else
		return NULL;
}

template <class T>
void
RecorrerTodo(const kdt_nodo<T>* actual, const CKDT<T>& k, ostream& salida)
{                              		// fc. recursiva que pone todos los
	if(actual!=NULL)			// nodos en el stream de sal.
	{
		salida << actual->datos;	// pone en el stream cada
						// elemento
		RecorrerTodo(actual->izq, k, salida);
		RecorrerTodo(actual->der, k, salida);
	}
}


#ifndef _error_
#define _error_
void Error(int cond, char* str)
{
	if (cond)
	{
		cout << str;
		exit(1);
	}
}
#endif

