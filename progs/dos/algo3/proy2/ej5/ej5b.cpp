#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "dicca.cpp"

void main(int nArgCount, char *sArgValues[])
{
	Diccionario dicc;

	if (nArgCount != 3)
	{
		cout << "\aError de sintaxis en la linea de comandos!\n\n";
		cout << "Sintaxis: ej5 <infpunto.ext> <consulta.ext>\n";
	}
	else
	{
		dicc.LeerPuntos(sArgValues[1]);
		dicc.ProcesarConsultas(sArgValues[2]);
	}
}
