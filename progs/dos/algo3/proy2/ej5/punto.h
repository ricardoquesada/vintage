//
// Definiciones Clase Punto
//

#ifndef _Punto_
#define _Punto_

#define MAXCAMPO 60
class Punto
{
public:
	Punto();
	Punto(double, double, double, double);
	double ValX();
	double ValY();
	double ValZ();
	double Luminosidad();
	int Menor(int, const Punto&) const;
	int Igual(int, const Punto&) const;
	Punto& operator=(const Punto&);
	int operator==(const Punto&) const;
	int operator<(const Punto&) const;
	friend ostream& operator<<(ostream&, const Punto&);
	friend istream& operator>>(istream&, Punto&);
private:
	double x, y, z, lum;
};

#endif