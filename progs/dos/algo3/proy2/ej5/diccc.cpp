//
// Miembros Clase Diccionario
//

#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "punto.cpp"
#include "cqt.cpp"
#include "dicc.h"

Diccionario::Diccionario()		// constructor
{
	CQT<Punto> aux(3);		// prepara la variable puntos especif. 3 cla-
	puntos=aux;				// ves
}

void Diccionario::LeerPuntos( char* filename )	// se lee de un archivo los
{							// puntos y se los al-
							// macena en la estruc.
	ifstream handle (filename, ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";
	handle >> puntos;
	handle.close();
}

void Diccionario::ProcesarConsultas(char* entrada)
{					// se ejecuta un archivo de consultas
	ifstream handle (entrada, ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";

	Parse(handle); 		      	// se llama al parse
	handle.close();
}

void Diccionario::Parse( istream& handle )
{					// reconoce las consultas y las	ejecuta
	signed char linea[255];
	char xi[12],xs[12];		// limites inf. y sup. de x
	char yi[12],ys[12];		// limites inf. y sup. de y
	char zi[12],zs[12];		// limites inf. y sup. de z
	char par1[60];			// limite inferior aux.
	char par2[60];			// limite superior aux.
	char comando[2];
	int corchete;
	int i, j, par, nco=1;
	int cinf,csup;			// campos especificados

	handle.getline(linea, 255, '\n');
	while (handle)			// mientras no haya fin de archivo
	{					// analiza linea por linea
		i=0;			// inicializa variables para nueva
		j=0;				// linea a analizar
		corchete=0;
		xi[0]=0; yi[0]=0; zi[0]=0;
		xs[0]=0; ys[0]=0; zs[0]=0;
		par1[0]=0; par2[0]=0; comando[0]=0;
		while(i<strlen(linea))
		{		 		// analiza la condicion
			if(linea[i]!=' ')
			{
				switch(linea[i])
				{
				case '[': corchete=1;	// empiezan los limi-
					  par=0;		// tes
					  j=0;
					  break;
				case ']': corchete=0;	// se termino la con-
					  j=0;			// dicion
					  break;
				case ',': if(corchete)	// se termino el 1er.
					  {			// limite
						par=1;
						j=0;
					  }
					  break;
				default : if(!corchete)
					  {
						if(j<1) // arma el campo
						{
							comando[j]=linea[i];
							comando[j+1]=0;
						}
					  }
					  else
					  {
						if(!par) // arma limite inf.
						{
							par1[j]=linea[i];
							par1[j+1]=0;
						}
						else
						{	 // arma limite sup.
							par2[j]=linea[i];
							par2[j+1]=0;
						}
					  }
					  j++;
					  break;
				}
				if(linea[i]==']')
				{		// verifica que tipo de campo
							// se esta pidiendo
							// y copia los limites
					if(stricmp(comando,"X")==0)
					{
						strcpy(xi, par1);
						strcpy(xs, par2);
					}
					if(stricmp(comando,"Y")==0)
					{
						strcpy(yi, par1);
						strcpy(ys, par2);
					}
					if(stricmp(comando,"Z")==0)
					{
						strcpy(zi, par1);
						strcpy(zs, par2);
					}
					j=0;  	// inicializa variables para
					par=0;		// nueva restriccion
					par1[0]=0;
					par2[0]=0;
					comando[0]=0;
				}
			}
			i++;
		}
		i++;

					// prepara rango para consultar
		Punto inf(atof(xi),atof(yi),atof(zi), 0);
					// punto inferior
		Punto sup(atof(xs),atof(ys),atof(zs), 0);
					// punto superior

		cinf=0;
		csup=0;
		if(xi[0]!=0)	    	// se arma el indicador de claves
			cinf++;			// activas inferiores
		if(yi[0]!=0)
			cinf+=2;
		if(zi[0]!=0)
			cinf+=4;
		if(xs[0]!=0)		// se arma el indicador de claves
			csup++;			// activas superiores
		if(ys[0]!=0)
			csup+=2;
		if(zs[0]!=0)
			csup+=4;

		cout << "\n" << "Consulta " << nco << ": (" << linea << ")\n";
		nco++;

		if(inf==sup && cinf>6 && csup>6)
		{		// si se busca sin rango
			if(puntos.Buscar(inf))
				cout << inf;
		}
		else
		{
			puntos.BusRango(inf, cinf, sup, csup,
					"salaux.txt");
				// utiliza como archivo aux. salaux
			ifstream handle1 ("salaux.txt", ios::in);
			if(!handle1)
				cout << "No se pudo generar la consulta\n";
				while (handle1.getline(linea, 255, '\n'))
				cout << linea << "\n";
				// muestra por pantalla los resultados
			handle1.close();
		}

		handle.getline(linea, 255, '\n');
	}
}