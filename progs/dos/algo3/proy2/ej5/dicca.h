//
// Definiciones Clase Diccionario
//

#ifndef _diccionario_
#define _diccionario_
#include "punto.h"
#include "cabbb.h"

class Diccionario
{
public:
	Diccionario();
	void LeerPuntos(char* filename);
	void ProcesarConsultas(char* filename);
private:
	CABBB<Punto> puntos;		// abbb de puntos espaciales

	void Parse(istream&);
					// reconocimiento del archivo
};
#endif
