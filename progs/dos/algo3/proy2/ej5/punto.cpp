//
// Miembros Clase Punto
//

#include <fstream.h>
#include <string.h>
#include <stdlib.h>
#include "Punto.h"

Punto::Punto()			// constructor default de la clase
{
	x=0;
	y=0;
	z=0;
	lum=0;
}

Punto::Punto(double vx, double vy, double vz, double vl)
{			       	// construye la clase con sus datos
	x=vx;
	y=vy;
	z=vz;
	lum=vl;
}

double Punto::ValX()		// funciones de observacion de los datos
{
	return x;
}
double Punto::ValY()
{
	return y;
}
double Punto::ValZ()
{
	return z;
}

double Punto::Luminosidad()
{
	return lum;
}

int Punto::operator<(const Punto& pto) const
{			       // operator< (verifica que el punto se menor
					// para dar true)
	if (x < pto.x)
		return 1;
	else if (x==pto.x && y<pto.y)
		return 1;
	else if (x==pto.x && y==pto.y && z<pto.z)
		return 1;
	else
		return 0;
}

int Punto::operator==(const Punto& pto) const
{			       // operator== (verifica que todas las claves
					// sean iguales para dar true)
	return (x==pto.x && y==pto.y && z==pto.z);
}

Punto& Punto::operator=(const Punto& pto) // operador de asignacion
{
	x=pto.x;
	y=pto.y;
	z=pto.z;
	lum=pto.lum;

	return *this;		// devuelve puntero al nuevo Punto
}

int Punto::Menor(int i, const Punto& pto) const
{				// verifica que un punto sea menor a otro con
	int res;			// respecto a su clave 'i'

	switch(i)
	{
		case 1: res=(x<pto.x);
			break;
		case 2: res=(y<pto.y);
			break;
		case 3: res=(z<pto.z);
			break;
	}
	return (res);
}

int Punto::Igual(int i, const Punto& pto) const
{				// verifica que un punto sea igual a otro con
	int res;			// respecto a su clave 'i'

	switch(i)
	{
		case 1: res=(x==pto.x);
			break;
		case 2: res=(y==pto.y);
			break;
		case 3: res=(z==pto.z);
			break;
	}
	return (res);
}

ostream& operator<<(ostream& salida, const Punto& pto)
{			       // operacion stream de salida
	salida << '"';
	salida << pto.x;
	salida << '"' << "," << '"';
	salida << pto.y;
	salida << '"' << "," << '"';
	salida << pto.z;
	salida << '"' << "," << '"';
	salida << pto.lum;
	salida << '"' << '\n';

	return salida;
}

istream& operator>>(istream& entrada, Punto& pto)
{			// operacion stream de entrada
	char caracter;
	signed char linea[200];
	char vx[20];
	char vy[20];
	char vz[20];
	char vlum[20];
	int comillas=0;
	int campo=1,j=0;

	entrada.getline(linea, 200, *("\n"));
	for(int i=0;i<strlen(linea);i++)
	{			// analiza la linea tomada del stream
		caracter=linea[i];
		if((caracter==*(",") && comillas==0) || caracter==34)
		{
			if(caracter==34)
				comillas=!(comillas);
			else
				campo++;
			j=0;
		}
		else
		{
			switch(campo)	// asigna cada campo de punto con los
			{			// datos obtenidos
				case 1:	vx[j]=caracter;
					vx[j+1]=0;
					break;
				case 2:	vy[j]=caracter;
					vy[j+1]=0;
					break;
				case 3:	vz[j]=caracter;
					vz[j+1]=0;
					break;
				case 4:	vlum[j]=caracter;
					vlum[j+1]=0;
					break;
			}
			j++;
		}
	}

	pto.x=atof(vx);
	pto.y=atof(vy);
	pto.z=atof(vz);
	pto.lum=atof(vlum);

	return entrada;
}
