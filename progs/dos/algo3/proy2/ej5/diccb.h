//
// Definiciones Clase Diccionario
//

#ifndef _diccionario_
#define _diccionario_
#include "punto.h"
#include "kdtree.h"

class Diccionario
{
public:
	Diccionario();
	void LeerPuntos(char* filename);
	void ProcesarConsultas(char* filename);
private:
	CKDT<Punto> puntos;		// k-d-tree de puntos espaciales

	void Parse(istream&);
					// reconocimiento del archivo
};
#endif
