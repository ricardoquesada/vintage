//
// Implementacion del K-D-TREE
//

#ifndef __CKDT_KDTREE__
#define __CKDT_KDTREE__

#include "kdt_nodo.h"

template <class T> class CKDT
{
public:
     CKDT();
     CKDT(int kdim);
     ~CKDT();
     int  Insertar(const T&t);          // inserta un nodo en el arbol;
					//    devuelve 0 cuando no se pudo
     int  Borrar(const T&t);            // elimina un nodo;
					//    devuelve 0 cuando no se pudo
     int  Buscar(T&t);                  // busca un nodo y lo almacena en t;
					//    devuelve 0 cuando no existe
     void BusRango(T& tinf, int cinf,   // busca los nodos dentro de los
		   T& tsup, int csup,   //    limites inferior y superior
		   char* filename);     //    definidos por tinf y tsup
					//    (respectivamente); cinf y csup
					//    son numeros binarios que indican
					//    que campos se han especificado;
					//    la salida va a archivo
     void Limpiar();                    // elimina todos los nodos
     int  Tamano();                     // cantidad de nodos
     int  Vacio();                      // 0 cuando hay nodos
     CKDT& operator=( const CKDT&);
     CKDT(const CKDT&);
     friend ostream& operator<<(ostream&, const CKDT&);
     friend istream& operator>>(istream&, CKDT&);
     friend void RecorrerTodo( const kdt_nodo<T>*, const CKDT<T>&, ostream& );
private:
//   Esto lo hacen ustedes.
	kdt_nodo<T> *raiz;
	int maxdisc;
	int tamano;
	int nextdisc( int );
	int Insertar_Recur( const T&t, kdt_nodo<T>**, int );
	int Buscar_Recur( T&t, kdt_nodo<T>**, int );
	kdt_nodo<T> ** Borrar_BuscarNodo( const T&, kdt_nodo<T> ** ,int * );
	void           Borrar_Recur( kdt_nodo<T> ** , int);
	kdt_nodo<T> ** Borrar_BuscarDisc( kdt_nodo<T> **, int , int, int * );
	void BusRango_Recur( T&, int, T&, int, ofstream&, kdt_nodo<T>*, int );
	void BorrarTodo( kdt_nodo<T> * );
	int Menor( int , const T& , const T&);
	int Mayor( int , const T& , const T&);
	kdt_nodo<T>* Copiar( kdt_nodo<T>* );
};

#endif
