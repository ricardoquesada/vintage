#include <fstream.h>
#include <math.h>
#include "cabbb.h"
#include "nodoabbb.h"

#define MAYOR 1
#define MENOR -1
#define IGUAL 0
#define IZQ 0
#define DER 1
#define HACIA_IZQ -1
#define HACIA_DER 1

template <class T>
CABBB<T>::CABBB()
{
	raiz=NULL;				//Inicializacion de la variable raiz
	dim=1;					//Almacenamiento de la dimension
	tamano=0;				//Inicializacion del tamano del arbol
}

template <class T>
CABBB<T>::CABBB(int kdim)
{
	Error(kdim<1,"La cantidad de claves debe ser >= 1");
						//Verificacion de dimension valida
	raiz=NULL;				//Inicializacion de la variable raiz
	dim=kdim;				//Almacenamiento de la dimension
	tamano=0;				//Inicializacion del tamano del arbol
}

template <class T>
CABBB<T>::~CABBB(void)			//Destructor
{
	BorrarTodo(raiz);			//Se borran todos los nodos
}

template <class T>
int CABBB<T>::Insertar(const T &t)  //Inserta el elemento t, devuelve 0
						//  si no lo pudo insertar
{
	int cambio = 0;
	return Insertar1(raiz, t, cambio);	//Llama a la funcion Insertar1 la
							//  cual realmente inserta el
							//  elemento
}

template <class T>
int CABBB<T>::Borrar(const T &t)
{
	int cambio = 0;

	return ((NULL==Borrar1(raiz, t, cambio, IGUAL))? 0:1);
}

template <class T>
int CABBB<T>::Buscar(T &t)	//Busca un elemento en el arbol y
						//  lo devuelve en t
{
	return (Buscar1(raiz, t));
}

template <class T>
void CABBB<T>::BusRango(T& tinf, int cinf, T& tsup, int csup, char* filename)
						//Realiza una busqueda por rangos
{
	ofstream handle (filename, ios::out);
	Error(!handle,"Error al crear el archivo");
				// abre el archivo chequeando errores
	BusRango1(tinf, cinf, tsup, csup, handle, raiz);
	handle.close();		// cierra el archivo
}

template <class T>
void CABBB<T>::Limpiar(void)		// Limpieza de todo el arbol
{
	BorrarTodo(raiz);			// se borran todos los nodos
	raiz=NULL;				// se inicializan las variables
	tamano=0;				// primero y siguiente
}

template <class T>
long double CABBB<T>::Tamano()  		//Devuelve el tamano del arbol
{
	return tamano;
}

template <class T>
int CABBB<T>::Vacio()  			//Devuelve 0 cuando hay nodos
{
	return (tamano!=0);
}

template <class T>
CABBB<T>& CABBB<T>::operator=(const CABBB<T> &param)	// operador de asignacion
{
	BorrarTodo(raiz);		// se borran todos los nodos
	tamano=param.tamano;		// se actualizan las variables con las
      dim=param.dim;
	raiz=Copiar(param.raiz);	// se copian todos los nodos
	return *this;		// devuelve puntero al nuevo arbol
}

template <class T>
CABBB<T>::CABBB(const CABBB<T> &param)	// copy constructor
{
	tamano=param.tamano;		// se actualizan las variables con las
      dim=param.dim;
	raiz=Copiar(param.raiz);	// se copian todos los nodos
}

template <class T>
ostream& operator<<(ostream &salida, const CABBB<T> &abbb)
{
	RecorrerTodo(abbb.raiz, abbb, salida);
	return salida;
}

template <class T>
istream& operator>>(istream &entrada, CABBB<T> &abbb)
{
	T elemento;
	int res;

	entrada >> elemento;
	while (entrada)
	{
		res=abbb.Insertar(elemento);
		if (!res)
			cout << "No se pudo insertar el elemento";
		entrada >> elemento;
	}
	return entrada;
}

template <class T>
void RecorrerTodo(const nodo<T>* actual, const CABBB<T>& abbb, ostream& salida)
{
	if(actual!=NULL)
	{
		salida << *(actual->datos);
		RecorrerTodo(actual->hijo[IZQ], abbb, salida);
		RecorrerTodo(actual->hijo[DER], abbb, salida);
	}
}

template <class T>
int CABBB<T>::Insertar1(nodo<T>* &actual, const T& t, int &cambio)
						//Inserta el elemento t, si tiene exito
						//devuelve 1 y 0 en caso contrario, la
						//variable cambio es utilizada para indicar
						//los cambios de altura del arbol actual
{
	int exito;
	int dir;
	T* elem;

	if (actual == NULL)
	{
		actual=new nodo<T>;	//Crea un nuevo nodo
		Error(!actual,"\nNo hay mas memoria");
						//Verificacion de memoria suficiente
		elem=new T;
		Error(!actual,"\nNo hay mas memoria");
		tamano++;
		*elem=t;
		actual->datos=elem;	//Inserta t en el arbol
		cambio=1;			//Indica que la altura del arbol cambio
		return  1;			//Indica que la operacion tuvo exito
	}
	if (*(actual->datos)==t)	//Verifica si el nodo es igual al elemento
						//que se desea insertar
	{
		cambio=0;			//Indica que la altura del arbol no vario
		return  0;			//Indica que la operacion no tuvo exito
	}
	else
	{
		dir=(*(actual->datos)<t) ? DER : IZQ;
						//Se fija en que subarbol debe insertar
		exito=Insertar1(actual->hijo[dir], t, cambio);
						//Lo inserta recursivamente en el subarbol
						//correspondiente, devolviendo el exito o
						//no de la operacion en la variable exito
		if (!exito)			//Verifica si tuvo exito
			return  0;		//Indica que la operacion no tuvo exito
		if (cambio==1) 		//Se fija si cambio la altura en la inser-
						//sion en el subarbol correspondiente
		{

			actual->balance= ((dir==IZQ)? --(actual->balance):
								++(actual->balance));
						//Actualiza el factor de balanceo del nodo
			if (actual->balance==0)
						//Se fija si el cambio del balanceo dejo
						//al nodo balanceado
				cambio=0;   //Indica que no cambio la altura del arbol
			else
				cambio=1-Balancear(actual);
						//Indica si hubo cambios o no en la altura del
						//arbol dependiendo de los cambio en la altura
						//despues de balancer
		}
	}
	return 1;			//Indica que la operacion tuvo exito
}

template <class T>
T* CABBB<T>::Borrar1(nodo<T>* &actual, const T &t, int &cambio, int comp)
{
	nodo<T>* aBorrar = NULL;
	T* esta = NULL;
	int compaux;
	int dir;

	if (actual == NULL)
	{
		cambio = 0;
		return  NULL;
	}

	compaux = Comparar(actual, t, comp);

	if (compaux != IGUAL)
	{
		dir = (compaux == MENOR)? IZQ:DER;
		esta = Borrar1(actual->hijo[dir], t, cambio, comp);
		if (!esta)
			return  NULL;
		if (cambio == 1)
		{
			actual->balance = ((compaux == MENOR)? ++actual->balance:
									   --actual->balance);
			if (actual->balance != 0)
				cambio = Balancear(actual);
		}
	}
	else
	{
		esta = actual->datos;
		if ((actual->hijo[IZQ] == NULL) &&
		    (actual->hijo[DER] == NULL))
		{
			delete actual;
			actual = NULL;
			tamano--;
			cambio = 1;
		}
		else if ((actual->hijo[IZQ] == NULL) ||
			   (actual->hijo[DER] == NULL))
		{
			aBorrar = actual;
			actual = actual->hijo[(actual->hijo[DER])? DER:IZQ];
			aBorrar->hijo[IZQ] = aBorrar->hijo[DER] = NULL;
			delete aBorrar;
			tamano--;
			cambio = 1;
		}
		else
		{
			actual->datos = Borrar1(actual->hijo[IZQ], t,
							cambio, MAYOR);
			if (cambio == 1)
			{
				actual->balance++;
				if (actual->balance != 0)
					cambio = Balancear(actual);
			}

		}
	}
	return  esta;
}

template <class T>
int CABBB<T>::Balancear(nodo<T>* &actual)
{
	int  cambio_altura = 0;

	if (actual->balance < -1)
	{
		if ((actual->hijo)[IZQ]->balance  ==  HACIA_DER)
			cambio_altura = RotarDos(actual, DER);
		else
			cambio_altura = RotarUno(actual, DER);
	}
	else if (actual->balance>1)
	{
		if ((actual->hijo)[DER]->balance  ==  HACIA_IZQ)
			cambio_altura = RotarDos(actual, IZQ);
		else
			cambio_altura = RotarUno(actual, IZQ);
	}
	return  cambio_altura;
}

template <class T>
int CABBB<T>::RotarUno(nodo<T>* &actual, int dir)
{
	int cambio_altura;

	int  otraDir = Opuesta(dir);
	nodo<T>* viejaraiz = actual;
	cambio_altura = (((actual->hijo)[otraDir]->balance == 0)? 0:1);
	actual = (viejaraiz->hijo)[otraDir];
	(viejaraiz->hijo)[otraDir] = (actual->hijo)[dir];
	(actual->hijo)[dir] = viejaraiz;
	viejaraiz->balance = -((dir == IZQ) ? --(actual->balance):
							  ++(actual->balance));
	return  cambio_altura;
}

template <class T>
int CABBB<T>::RotarDos(nodo<T>* &actual, int dir)
{
	int  otraDir = Opuesta(dir);
	nodo<T>* viejaraiz = actual;
	nodo<T>* viejoSubarbol = (actual->hijo)[otraDir];
	actual = ((viejaraiz->hijo)[otraDir]->hijo)[dir];
	(viejaraiz->hijo)[otraDir] = (actual->hijo)[dir];
	(actual->hijo)[dir] = viejaraiz;
	(viejoSubarbol->hijo)[dir] = (actual->hijo)[otraDir];
	(actual->hijo)[otraDir] = viejoSubarbol;
	(actual->hijo)[IZQ]->balance  = -MAX(actual->balance, 0);
	(actual->hijo)[DER]->balance = -MIN(actual->balance, 0);
	actual->balance = 0;
	return  1;
}

template <class T>
int CABBB<T>::Comparar(nodo<T>* actual, T t, int comp)
{
	switch (comp)
	{
		case IGUAL :
			{
			 if (*(actual->datos) == t)
                comp=IGUAL;
			 else if (*(actual->datos) < t)
				comp=MAYOR;
			 else
				comp=MENOR;
			 return comp;
			}
		case MENOR :
			comp=((actual->hijo[IZQ] == NULL)? IGUAL : MENOR);
		case MAYOR :
			comp=((actual->hijo[DER] == NULL)? IGUAL : MAYOR);
	}
	return comp;
}

template <class T>                 	// fc. recursiva que borra
void CABBB<T>::BorrarTodo(nodo<T>* actual)	// todos los nodos del arbol
{
	if(actual != NULL)
	{
		BorrarTodo(actual->hijo[IZQ]);
		BorrarTodo(actual->hijo[DER]);
		delete actual->datos;
		delete actual;
	}
}

template <class T>
int CABBB<T>::Buscar1(nodo<T>* &actual, T &t)
{
	if (actual==NULL)
		return 0;
	if (*(actual->datos)==t)
	{
      	t=*(actual->datos);
		return 1;
      }
	else if (*(actual->datos)<t)
		return Buscar1(actual->hijo[DER], t);
      else
		return Buscar1(actual->hijo[IZQ], t);
}

template <class T>                 	// fc. recursiva que copia
nodo<T>* CABBB<T>::Copiar(nodo<T>* actual)	// todos los nodos del arbol
{
	nodo<T>* nnodo;
	T* dato;

	if(actual != NULL)
	{
		nnodo=new nodo<T>;
		Error(!nnodo,"No hay mas memoria");
		dato=new T;
		Error(!dato,"No hay mas memoria");
		*dato=*(actual->datos);
		nnodo->hijo[IZQ]=Copiar(actual->hijo[IZQ]);
		nnodo->hijo[DER]=Copiar(actual->hijo[DER]);
		return nnodo;
	}
	else
		return NULL;
}

template <class T>
void CABBB<T>::BusRango1(T& tinf, int cinf, T& tsup, int csup, ofstream& handle, nodo<T>* actual)
					// fc. recursiva que verifica para cada clave
					// si actual esta en el rango
					// grabando los puntos que cumplen
					// todas las claves
{
	int mascara=1;		// acumula las potencias de 2
	int agregar=1;
	int i=1;

	if(actual != NULL)
	{
		while (agregar && i<=dim)
		{
			if ( (!(mascara&cinf) || !((*(actual->datos)).Menor(1,tinf))) &&
			     (!(mascara&csup) || ((*(actual->datos)).Menor(1,(tsup)))) )
			{
				mascara=pow(2,i);
                        i++;
			}
			else
				agregar=0;
            }
		if (agregar)
			handle << *(actual->datos);
		if ((1&cinf) && (*(actual->datos) < tinf))
			BusRango1(tinf, cinf, tsup, csup, handle, actual->hijo[DER]);
		else if ((1&csup) && (tsup < *(actual->datos)))
			BusRango1(tinf, cinf, tsup, csup, handle, actual->hijo[IZQ]);
            else
			BusRango1(tinf, cinf, tsup, csup, handle, actual->hijo[IZQ]);
			BusRango1(tinf, cinf, tsup, csup, handle, actual->hijo[DER]);
	}
}