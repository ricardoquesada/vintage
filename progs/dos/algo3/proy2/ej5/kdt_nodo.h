//
// KDT nodo
//
#ifndef __KDT_NODO__
#define __KDT_NODO__

template<class T>
class kdt_nodo
{
public:
	kdt_nodo();
	kdt_nodo & operator=( const kdt_nodo &ip );
	kdt_nodo( const kdt_nodo  &ip );
	kdt_nodo( const T &ip );

	kdt_nodo<T> *izq;
	kdt_nodo<T> *der;
	T datos;
};
#endif
