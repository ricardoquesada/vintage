//
// Definiciones Clase Diccionario
//

#ifndef _diccionario_
#define _diccionario_
#include "punto.h"
#include "cqt.h"

class Diccionario
{
public:
	Diccionario();
	void LeerPuntos(char* filename);
	void ProcesarConsultas(char* filename);
private:
	CQT<Punto> puntos;		// quad-tree de puntos espaciales

	void Parse(istream&);
					// reconocimiento del archivo
};
#endif
