//
// Definiciones Clase Nodo CQT
//

#ifndef _nodo_
#define _nodo_

template <class T> class nodo
{
public:
	T* datos ;			// alberga los datos
	nodo<T>** hijos;		// puntero a array de hijos
	nodo(int kdim=1);
	~nodo();
	nodo& operator=(const nodo&);
	nodo(const nodo&);
private:
	int k;				// cantidad de claves
};

#endif
