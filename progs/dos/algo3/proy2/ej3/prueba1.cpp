#include <iostream.h>
#include <fstream.h>
#include <stdlib.h>
#include "elem.cpp"
#include "nodo_cqt.cpp"
#include "cqt.cpp"

void main(void)
 {
	CQT<Elem> qt(3);

	cout << "Este es un programa de prueba del CQT\n";
	ifstream handle ("salida.txt", ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";
	handle >> qt;
	cout << "\nLISTA: " << qt;
	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
	handle.close();
}