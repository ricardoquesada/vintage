//
// Miembros Clase CQT
//

#include <fstream.h>
#include <math.h>
#include "nodo_cqt.cpp"
#include "cqt.h"

template <class T>		// Constructor de la clase
CQT<T>::CQT()
{
	tamano=0;		// se inicializan las variables
	dim=1;			// guarda la cantidad de claves
	k=pow(2,dim);		// calcula la cantidad de hijos
	raiz=NULL;
}

template <class T>		// Constructor de la clase
CQT<T>::CQT(int kdim)
{
	tamano=0;		// se inicializan las variables
	dim=kdim;		// guarda la cantidad de claves
	k=pow(2,dim);		// calcula la cantidad de hijos
	raiz=NULL;
}

template <class T>
CQT<T>::~CQT(void)		// destructor de la clase
{
	BorrarTodo(raiz);	// se borran todos los nodos
}

template <class T>
int CQT<T>::Insertar(const T& t)       	// Insercion de un elemento
{
	T* tipo;	      	// puntero a elemento
	nodo<T>* ptr;		// puntero al nuevo nodo
	nodo<T>* ultAcc;	// nodo al que se le inserta un hijo
	int hijo;

	ultAcc=Posicionar(t, hijo); // busqueda del lugar en el arbol
	if ((ultAcc != NULL) && (t==*(ultAcc->datos)))
		return 0;	 // si ya existe no se inserta nada
	ptr=new nodo<T>(k);	 // se pide memoria para el nodo nuevo
	Error(!ptr,"No hay mas memoria");
	tipo=new T;		 // se pide memoria para el elemento
	Error(!tipo,"No hay mas memoria");  	// nuevo
	*tipo=t;		 // se copia el elemento al arbol
	ptr->datos=tipo;	 // se enlaza la copia al nodo
	for (int i=0;i<k; i++)
		ptr->hijos[i]=NULL;
				// se inicializan los hijos

	if( ultAcc==NULL )	// se enlaza al elemento nuevo donde
		raiz=ptr ;		// corresponda en el arbol
	else
		ultAcc->hijos[hijo]=ptr;

	tamano++;		// se actualiza el tamano del arbol
	return 1;		// retorna ok
}

template <class T>
int CQT<T>::Borrar(const T& t)	// borrado de un elemento
{
	nodo<T>* actual;	// puntero al elemento a borrar
	nodo<T>* ultAcc;	// puntero al padre del elem. a borrar
	int hijo;		// contiene el nro. de hijo a tomar

	ultAcc=actual=raiz;	// se comienza a buscar el nodo
	while(actual!=NULL && (!(*(actual->datos)==t)))
	{
		ultAcc=actual;
		hijo=Posicion(actual, t);
		actual=actual->hijos[hijo];
	}

	if(actual==NULL || !(*(actual->datos)==t))
		return (0);	// si no lo encontro retorna error
	tamano--;		// actualiza tamano
	if(actual==raiz)      	// se desenlaza del arbol el nodo a borrar
		raiz=NULL;
	else
		ultAcc->hijos[hijo]=NULL;
	for (int i=0; i<k; i++)
		ReInserta(actual->hijos[i]);
				// reinserta todos los nodos hijos y descen-
					// dientes del elemento a borrar

	delete actual->datos;	// se borra fisicamente de memoria
	delete actual;			// el nodo y el elemento
	return(1);		// retorna ok
}

template <class T>
int CQT<T>::Buscar(T& t)	// busqueda de un elemento
{
	nodo<T>* ultAcc;	// puntero al nodo buscado
        int hijo;

	ultAcc=Posicionar(t, hijo);	// busca el elemento
	if((ultAcc!=NULL) && (t==*(ultAcc->datos)))
	{
		t=*(ultAcc->datos);     // si se encontro lo devuelve
		return (1);	// retorna ok
	}
	else
		return (0);	// no se encontro el elemento
}

template <class T>
void CQT<T>::BusRango(T& tinf, int cinf, T& tsup, int csup, char* filename)
				// busqueda de los elementos contenidos en
				// el rango delimitado por tinf y tsup
{
	ofstream handle (filename, ios::out);
	Error(!handle,"Error al crear el archivo");
				// abre el archivo chequeando errores

	SeleccionarPuntos(tinf, cinf, tsup, csup, handle, raiz, 0, 1);
				// se llama a este procedimiento para grabar
					// en el archivo los puntos que estan
					// en el rango especificado

	handle.close();		// cierra el archivo
}

template <class T>
void CQT<T>::Limpiar(void)	// Limpieza de todo el arbol
{
	BorrarTodo(raiz);	// se borran todos los nodos
	raiz=NULL;		// se inicializan las variables
	tamano=0;
}

template <class T>
int CQT<T>::Tamano(void)	// Cantidad de elementos en el arbol
{
	return tamano;
}

template <class T>
int CQT<T>::Vacio(void)		// El arbol esta vacio?
{
	return (tamano!=0);
}

template <class T>
CQT<T>& CQT<T>::operator=(const CQT<T>& qt)	// operador de asignacion
{
	BorrarTodo(raiz);	// se borran todos los nodos
	tamano=qt.tamano;    	// se actualizan las variables con las
	dim=qt.dim;
	k=qt.k;
	raiz=Copiar(qt.raiz);	// se copian todos los nodos
	return *this;		// devuelve puntero al nuevo arbol
}

template <class T>
CQT<T>::CQT(const CQT<T>& qt)	// copy constructor
{
	tamano=qt.tamano;	// se actualizan las variables
	dim=qt.dim;
	k=qt.k;
	raiz=Copiar(qt.raiz);	// se copian todos los nodos
}

template <class T>
ostream& operator<<(ostream& salida, const CQT<T>& qt)
{				// operacion stream de salida
	RecorrerTodo(qt.raiz, qt, salida);
	return salida;		// devuelve todos los elementos del cqt
}

template <class T>
istream& operator>>(istream& entrada, CQT<T>& qt)
{				// operacion stream de entrada
	T elemento;
	int res;

	entrada >> elemento;	// inserta todos los elementos en el cqt
	while (entrada)
	{
		res=qt.Insertar(elemento);
		Error(!res, "No se pudo insertar el elemento");
		entrada >> elemento;
	}
	return entrada;
}

template <class T>           	// Busca un elemento en el arbol o un lugar
nodo<T>* CQT<T>::Posicionar(const T& t, int& hijo) // para insertar t
{
	nodo<T>* actual;	// punteros a nodos
	nodo<T>* ultAcc;

	ultAcc=actual=raiz;	// se busca el lugar en el arbol
	while(actual!=NULL && (!(*(ultAcc->datos)==t)))
	{
		ultAcc=actual ;
		hijo=Posicion(ultAcc, t);	// calcula que hijo se usara
		actual=ultAcc->hijos[hijo];	// se elige el nodo a seguir
	}
	return (ultAcc);
}

template <class T>           	// Calcula en que hijo debera ir t con res-
int CQT<T>::Posicion(nodo<T>* actual, const T& t) // pecto al nodo actual
{
	int resultado=0;

	for(int i=1; i<=dim; i++)
		if ((*(actual->datos)).Menor(i, t))
			resultado=resultado + pow(2,dim-i);
				// si el dato se encuentra a la derecha del
					// nodo le suma a resultado 2**(k-i)

	return (resultado);
}

template <class T>  		// fc. recursiva que inserta todos los nodos
void CQT<T>::ReInserta(nodo<T>* actual) // del arbol apuntado por actual
{
	if (actual!=NULL)
	{
		tamano--;
		Insertar(*(actual->datos));
				// se inserta el nodo en el arbol
		for (int i=0;i<k;i++)
			ReInserta(actual->hijos[i]);
				// llama recursivamente para cada hijo

		delete actual->datos;
				// se borra el dato del nodo
		delete actual;	// se borra el nodo viejo

	}
}

template <class T>
void CQT<T>::SeleccionarPuntos(T& tinf, int cinf, T& tsup, int csup,
		ofstream& handle, nodo<T>* actual, int hijo, int j)

				// fc. recursiva que verifica para cada clave
					// si actual esta en el rango
					// grabando los puntos que cumplen
					// todas las claves
{
	int pot=1;		// acumula las potencias de 2
	int resultado;		// variable logica para chequear multiples
					// condiciones

	if(actual != NULL)
	{
		if (j==1)	// verifica que el elemento actual este en el
		{			// rango pedido para todas las claves
			resultado=1;
			for (int i=1; i<=dim; i++)
			{
				resultado=resultado &&
					((((!((*(actual->datos)).Menor(i, tinf)) ||
					((cinf & pot)==0))) &&
					(((*(actual->datos)).Menor(i, tsup)) ||
					((*(actual->datos)).Igual(i, tsup)) ||
					((csup & pot)==0))));
				pot*=2;
			}
			if (resultado)	// si esta en el rango lo graba
				handle << *(actual->datos);
		}
		if (j==dim+1)
			SeleccionarPuntos(tinf, cinf, tsup, csup, handle,
			actual->hijos[hijo], 0, 1);
			// llama recursivamente al hijo correspondiente
		else
		{
			pot=pow(2, j-1);
			if ((!((*(actual->datos)).Menor(j, tinf)) ||
				((cinf & pot)==0)) &&
				(((*(actual->datos)).Menor(j, tsup) ||
				(*(actual->datos)).Igual(j, tsup)) ||
				((csup & pot)==0)))
			{
				SeleccionarPuntos(tinf, cinf, tsup, csup,
					handle,	actual, hijo+pow(2, dim-j), j+1);
				SeleccionarPuntos(tinf, cinf, tsup, csup,
					handle,	actual, hijo, j+1);
			}
			else
				if ((*(actual->datos)).Menor(j, tinf) &&
					((cinf & pot)>0))
					SeleccionarPuntos(tinf, cinf, tsup,
					csup, handle, actual, hijo+pow(2,dim-j), j+1);
				else
					SeleccionarPuntos(tinf, cinf, tsup,
					csup, handle, actual, hijo, j+1);
		}
		// se verifica cada clave con respecto al rango para determi-
			// nar en que hijo buscar
	}
}

template <class T>            	// fc. recursiva que borra
void CQT<T>::BorrarTodo(nodo<T>* actual) // todos los nodos del arbol
{
	if(actual != NULL)
	{
		for (int i=0; i<k; i++)
			BorrarTodo(actual->hijos[i]);
		delete actual->datos;
		delete actual;
	}
}

template <class T>                 	// fc. recursiva que copia
nodo<T>* CQT<T>::Copiar(nodo<T>* actual)	// todos los nodos del arbol
{
	nodo<T>* ele;
	T* dato;

	if(actual != NULL)
	{
		ele=new nodo<T>(k);
		Error(!ele,"No hay mas memoria");
		dato=new T;
		Error(!dato,"No hay mas memoria");
		ele->datos=dato;
		*dato=*(actual->datos);
		for (int i=0; i<k; i++)
			ele->hijos[i]=Copiar(actual->hijos[i]);
		return ele;
	}
	else
		return NULL;
}

template <class T>
void RecorrerTodo(const nodo<T>* actual, const CQT<T>& qt, ostream& salida)
{                              		// fc. recursiva que pone todos los
	if(actual!=NULL)			// nodos en el stream de sal.
	{
		salida << *(actual->datos);	// pone en el stream cada
							// elemento
		for (int i=0; i<qt.k; i++)	// llama rec. para cada hijo
			RecorrerTodo(actual->hijos[i], qt, salida);
	}
}
