#ifndef _cabbb_
#define _cabbb_

#include "nodoabbb.h"

template <class T>
class CABBB
{
public:
	CABBB();				//Constructor
	CABBB(int kdim);          		//Constructor
	~CABBB();					//Destructor
	int Insertar(const T &t);		//Inserta un nodo en el arbol
							//  devuelve 0 cuando no se pudo
	int Borrar(const T &t);			//Borra un nodo del arbol
							//  devuelve 0 cuando no pudo
	int Buscar(T &t);				//Busca un nodo y lo almacena en t
							//  devuelve 0 cuando no existe
	void BusRango(T& tinf, int cinf, T& tsup, int csup, char* filename);
							//Busca los nodos dentro de los
							//  limites inferior y superior
							//  definidos por tinf y tsup
							//  (respectivemente); cinf y csup
							//  son numeros binarios que indican
							//  que campos se han especificado;
							//  la salida va a archivo
	void Limpiar();				//Elimina todos los nodos
	long double Tamano();			//Cantidad de nodos
	int Vacio();				//0 cuando hay nodos
	CABBB &operator=(const CABBB<T> &t);//Asignacion
	CABBB(const CABBB<T> &t);		//Constructor de copia
	friend ostream& operator<<(ostream&, const CABBB&);
	friend istream& operator>>(istream&, CABBB&);
	friend void RecorrerTodo(const nodo<T>*, const CABBB<T>&, ostream&);

private:
	nodo<T>* raiz;
	long double tamano;
	int dim;

	int Insertar1(nodo<T>* &actual, const T &t, int &cambio);
							//Funcion recursiva para insertar
							//  un nodo en el arbol. Devuelve
							//  0 cuando no se pudo
	T* Borrar1(nodo<T>* &actual, const T &t, int &cambio, int comp);
							//Funcion recursiva para borrar
							//  un elemento del arbol. Devuelve
							//  NULL cuando no se pudo
	int Balancear(nodo<T>* &actual);	//Funcion que balancea. Devuelve 0
							//  si la altuara del arbol no
							//  cambio y 1 en caso contrario
	int RotarUno(nodo<T>* &actual, int dir);
							//Funcion que realiza una rotacion
							//  en el sentido de la direccion
							//  "dir". Devuelve 0 si la altura
							//  del arbol no cambio y 1 en caso
							//  contrario
	int RotarDos(nodo<T>* &actual, int dir);
							//Funcion que realiza 2 rotaciones,
							//  una en el sentido de la direccion
							//  "dir" y la otr en direccion
							//  opuesta. Devuelve 0 si la altura
							//  del arbol no cambio y 1 en caso
							//  contrario
	int Comparar(nodo<T>* actual, T t, int comp);
							//Funcion que en el caso que "comp"
							//  sea IGUAL compara el dato del
							//  nodo "actual" con "t"; en el
							//  caso que "comp" sea MAYOR o
							//  MENOR, si "actual" es NULL,
							//  devuelve IGUAL, si no devuelve
							//  el mimo valor de "comp"
	void BorrarTodo(nodo<T>* actual);	//Borra todos los elementos del
							//  arbol
	int Buscar1(nodo<T>* &actual, T &t);
	nodo<T>* Copiar(nodo<T>* actual);	//Copia un arbol a otro
	void BusRango1(T& tinf, int cinf, T& tsup, int csup, ofstream& handle, nodo<T>* actual);
	inline int Opuesta(int dir)
			{return (1 - dir);}
	inline int MIN(int a, int b)
			{ return  ((a < b) ? a : b); }
	inline int MAX(int a, int b)
			{ return  ((a > b) ? a : b); }

};
# endif