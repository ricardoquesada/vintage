//
// Clase KDT nodo
//

#include "kdt_nodo.h"

template<class T>
kdt_nodo<T>::kdt_nodo()
{
	izq = NULL ;
	der = NULL ;
}

template<class T>
kdt_nodo<T>& kdt_nodo<T>::operator=( const kdt_nodo<T> &ip )
{
	izq = ip.izq ;
	der = ip.der ;
	datos = ip.datos ;
	return *this ;
}

template<class T>
kdt_nodo<T>::kdt_nodo( const kdt_nodo<T>  &ip )
{
	izq = ip.izq ;
	der = ip.der ;
	datos = ip.datos ;
}

template<class T>
kdt_nodo<T>::kdt_nodo( const T &ip )
{
	izq = NULL ;
	der = NULL ;
	datos = ip ;
}
