//
// Definiciones Clase CQT
//

#ifndef _cqt_
#define _cqt_
#include "nodo_cqt.h"

template <class T> class CQT
{
public:
	CQT();
	CQT(int kdim);
	~CQT();
	int Insertar(const T& t);     	// inserta un nodo en el arbol
					// 	devuelve 0 cuando no se pudo
	int Borrar(const T& t);   	// elimina un nodo
					//      devuelve 0 cuando no se pudo
	int Buscar(T& t);               // busca un nodo y lo almacena en t
					//      devuelve 0 cuando no existe
	void BusRango(T& tinf, int cinf, // busca los nodos dentro de los
		     T& tsup, int csup, //  	limites inferior y superior
		     char* filename);	//      definidos por tinf y tsup
					//   	(respectivamente); cinf y csup
					//	son numeros binarios que indican
					//	que campos se han especificado;
					//	la salida va al archivo
	void Limpiar();                 // Elimina todos los nodos
	int Tamano();			// Cantidad de nodos
	int Vacio();			// 0 cuando no hay nodos
	CQT& operator=(const CQT&);
	CQT(const CQT&);
	friend ostream& operator<<(ostream&, const CQT&);
	friend istream& operator>>(istream&, CQT&);
	friend void RecorrerTodo(const nodo<T>*, const CQT<T>&, ostream&);
private:
	nodo<T>* raiz;
	int tamano;
	int dim;
	int k;
	nodo<T>* Posicionar(const T& t, int& hijo);
	int Posicion(nodo<T>* actual, const T& t);
	void ReInserta(nodo<T>* actual);
	void SeleccionarPuntos(T& tinf, int cinf, T& tsup, int csup,
		ofstream& handle, nodo<T>* actual, int hijo, int j);
	void BorrarTodo(nodo<T>* actual);
	nodo<T>* Copiar(nodo<T>* actual);
};
#endif
