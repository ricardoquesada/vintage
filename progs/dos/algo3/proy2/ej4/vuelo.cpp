//
// Miembros Clase Vuelo
//

#include <fstream.h>
#include <string.h>
#include <stdlib.h>
#include "Vuelo.h"

Vuelo::Vuelo()			// constructor default de la clase
{
	strcpy(compania,"");
	strcpy(aero_origen,"");
	strcpy(momento_salida,"");
	strcpy(aero_destino,"");
	strcpy(momento_llegada,"");
	strcpy(modelo_avion,"");
	tripulantes=0;
}

Vuelo::Vuelo(char* cp, char* ao, char* ms, char* ad, char* ml, char* ma, int tr)
{                               // construye la clase con sus datos
	strcpy(compania,cp);
	strcpy(aero_origen,ao);
	strcpy(momento_salida,ms);
	strcpy(aero_destino,ad);
	strcpy(momento_llegada,ml);
	strcpy(modelo_avion,ma);
	tripulantes=tr;
}

char * Vuelo::Compania()	// funciones de observacion de los datos
{
	return compania;
}
char * Vuelo::Aero_Origen()
{
	return aero_origen;
}
char * Vuelo::Momento_Salida()
{
	return momento_salida;
}

char * Vuelo::Aero_Destino()
{
	return aero_destino;
}

char * Vuelo::Momento_Llegada()
{
	return momento_llegada;
}

char * Vuelo::Modelo_Avion()
{
	return modelo_avion;
}

int Vuelo::Tripulantes()
{
	return tripulantes;
}

int Vuelo::operator==(const Vuelo& vue) const
{                               // operator== (verifica que todas las claves
					// sean iguales para dar true)
	return (strcmp(compania, vue.compania)==0 &&
		strcmp(aero_origen, vue.aero_origen)==0 &&
		strcmp(momento_salida, vue.momento_salida)==0 &&
		strcmp(aero_destino, vue.aero_destino)==0 &&
		strcmp(momento_llegada, vue.momento_llegada)==0);
}

int Vuelo::operator<(const Vuelo& vue) const
{                               // operator< (verifica que el elemento sea
					// menor para dar true)

	if (strcmp(compania, vue.compania)<0)
		return 1;
	else if (strcmp(compania, vue.compania)==0 &&
		 strcmp(aero_origen, vue.aero_origen)<0)
		return 1;
	else if (strcmp(compania, vue.compania)==0 &&
		 strcmp(aero_origen, vue.aero_origen)==0 &&
		 strcmp(momento_salida, vue.momento_salida)<0)
		return 1;
	else if (strcmp(compania, vue.compania)==0 &&
		 strcmp(aero_origen, vue.aero_origen)==0 &&
		 strcmp(momento_salida, vue.momento_salida)==0 &&
		 strcmp(aero_destino, vue.aero_destino)<0)
		return 1;
	else if (strcmp(compania, vue.compania)==0 &&
		 strcmp(aero_origen, vue.aero_origen)==0 &&
		 strcmp(momento_salida, vue.momento_salida)==0 &&
		 strcmp(aero_destino, vue.aero_destino)==0 &&
		 strcmp(momento_llegada, vue.momento_llegada)<0)
		return 1;
	else
		return 0;
}

Vuelo& Vuelo::operator=(const Vuelo& vue)	// operador de asignacion
{
	strcpy(compania, vue.compania);
	strcpy(aero_origen, vue.aero_origen);
	strcpy(momento_salida, vue.momento_salida);
	strcpy(aero_destino, vue.aero_destino);
	strcpy(momento_llegada, vue.momento_llegada);
	strcpy(modelo_avion, vue.modelo_avion);
	tripulantes=vue.tripulantes;

	return *this;		// devuelve puntero al nuevo vuelo
}

int Vuelo::Menor(int i, const Vuelo& vue) const
{				// verifica que un vuelo sea menor a otro con
	int res;			// respecto a su clave 'i'

	switch(i)
	{
		case 1: res=(strcmp(compania, vue.compania)<0);
			break;
		case 2: res=(strcmp(aero_origen, vue.aero_origen)<0);
			break;
		case 3: res=(strcmp(momento_salida, vue.momento_salida)<0);
			break;
		case 4: res=(strcmp(aero_destino, vue.aero_destino)<0);
			break;
		case 5: res=(strcmp(momento_llegada, vue.momento_llegada)<0);
			break;
	}
	return (res);
}

int Vuelo::Igual(int i, const Vuelo& vue) const
{				// verifica que un vuelo sea igual a otro con
	int res;			// respecto a su clave 'i'

	switch (i)
	{
		case 1: res=(strcmp(compania, vue.compania)==0);
			break;
		case 2: res=(strcmp(aero_origen, vue.aero_origen)==0);
			break;
		case 3: res=(strcmp(momento_salida, vue.momento_salida)==0);
			break;
		case 4: res=(strcmp(aero_destino, vue.aero_destino)==0);
			break;
		case 5: res=(strcmp(momento_llegada, vue.momento_llegada)==0);
			break;
	}
	return (res);
}

ostream& operator<<(ostream& salida, const Vuelo& vue)
{                               // operacion stream de salida
	salida << '"';
	salida << vue.compania;
	salida << '"' << "," << '"';
	salida << vue.aero_origen;
	salida << '"' << "," << '"';
	salida << vue.momento_salida;
	salida << '"' << "," << '"';
	salida << vue.aero_destino;
	salida << '"' << ","<< '"' ;
	salida << vue.momento_llegada;
	salida << '"' << ","<< '"' ;
	salida << vue.modelo_avion;
	salida << '"' << ","<< '"' ;
	salida << vue.tripulantes;
	salida << '"' << '\n';

	return salida;
}

istream& operator>>(istream& entrada, Vuelo& vue)
{                       	// operacion stream de entrada
	char caracter;
	signed char linea[200];
	char trip[5];
	int comillas=0;
	int campo=1,j=0;

	entrada.getline(linea, 200, '\n');
	for(int i=0;i<strlen(linea);i++)
	{			// analiza la linea tomada del stream
		caracter=linea[i];
		if((caracter==',' && comillas==0) || caracter==34)
		{
			if(caracter==34)
				comillas=!(comillas);
			else
				campo++;
			j=0;
		}
		else
		{
			switch(campo)	// asigna cada campo de vuelo con los
			{			// datos obtenidos
				case 1:	vue.compania[j]=caracter;
					vue.compania[j+1]=0;
					break;
				case 2:	vue.aero_origen[j]=caracter;
					vue.aero_origen[j+1]=0;
					break;
				case 3:	vue.momento_salida[j]=caracter;
					vue.momento_salida[j+1]=0;
					break;
				case 4:	vue.aero_destino[j]=caracter;
					vue.aero_destino[j+1]=0;
					break;
				case 5:	vue.momento_llegada[j]=caracter;
					vue.momento_llegada[j+1]=0;
					break;
				case 6:	vue.modelo_avion[j]=caracter;
					vue.modelo_avion[j+1]=0;
					break;
				case 7:	trip[j]=caracter;
					trip[j+1]=0;
					break;
			}
			j++;
		}
	}

	vue.tripulantes=atoi(trip);

	return entrada;
}
