//
// Definiciones Clase Vuelo
//

#ifndef _Vuelo_
#define _Vuelo_

#define MAXCAMPO 60
class Vuelo
{
public:
	Vuelo();
	Vuelo(char*, char*, char*, char*, char*, char*, int);
	char* Compania();
	char* Aero_Origen();
	char* Momento_Salida();
	char* Aero_Destino();
	char* Momento_Llegada();
	char* Modelo_Avion();
	int Tripulantes();
	int Menor(int, const Vuelo& ) const;
	int Igual(int, const Vuelo& ) const;
	Vuelo& operator=(const Vuelo& );
	int operator==(const Vuelo& ) const;
	int operator<(const Vuelo& ) const;
	friend ostream& operator<<(ostream&, const Vuelo&);
	friend istream& operator>>(istream&, Vuelo&);
private:
	char compania[MAXCAMPO];
	char aero_origen[4];
	char momento_salida[13];
	char aero_destino[4];
	char momento_llegada[13];
	char modelo_avion[MAXCAMPO];
	int tripulantes;
};

#endif