#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "traficoc.cpp"

void main(int nArgCount, char *sArgValues[])
{
	Trafico Tra;

	if (nArgCount != 3)
	{
		cout << "\aError de sintaxis en la linea de comandos!\n\n";
		cout << "Sintaxis: ej4 <infvuelo.ext> <consulta.ext>\n";
	}
	else
	{
		Tra.LeerVuelos(sArgValues[1]);
		Tra.ProcesarConsultas(sArgValues[2]);
	}
}
