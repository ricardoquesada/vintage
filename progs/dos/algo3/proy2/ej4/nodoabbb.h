//
// Definiciones Clase Nodo ABBB
//

#ifndef _nodo_
#define _nodo_

template <class T> class nodo
{
public:
		T* datos ;				//Contiene los datos
		nodo<T>** hijo;			//Puntero al arreglo de hijos
		int balance;			//Indica el factor de balanceo
							//del nodo
		nodo();				//Constructor
		~nodo();				//Destructor
		nodo& operator=(const nodo&); //Asignacion
		nodo(const nodo&);		//Constructor de copia
};

#endif
