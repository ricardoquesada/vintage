//
// Definiciones Clase Trafico
//

#ifndef _trafico_
#define _trafico_
#include "vuelo.h"
#include "cabbb.h"

class Trafico
{
public:
	Trafico();
	void LeerVuelos(char* filename);
	void ProcesarConsultas(char* filename);
private:
	CABBB<Vuelo> vuelos;		// cabbb de vuelos

	void Parse(istream&);
					// reconocimiento del archivo
};
#endif
