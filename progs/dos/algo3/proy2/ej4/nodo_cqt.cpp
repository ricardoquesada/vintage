//
// Miembros Clase Nodo CQT
//

#include <iostream.h>
#include <stdlib.h>
#include "nodo_cqt.h"

template <class T>
nodo<T>::nodo(int kdim=1)	     	//Constructor
{
	Error(kdim<1,"La cantidad de claves debe ser >= 1");
	k=kdim;			        //Inicializa la cantidad de claves
	hijos=new nodo<T>*[k];		//Asignacion de memoria al
						//vector de hijos creado
	Error(!hijos,"No hay mas memoria");
					//Verificacion de memoria
						// suficiente
	for(int i=0; i<k; i++)		//Inicializa los elementos de hijos
		hijos[i]=NULL;
}

template <class T>
nodo<T>::~nodo(void)			//Destructor
{
	delete[] hijos;			//Liberacion de la memoria
						//asignada al vector hijos
}

template <class T>
nodo<T>& nodo<T>::operator=(const nodo<T>& param)
					//Asignacion de nodos
{
	delete[] hijos;			//Liberacion de la memoria
						//asignada a hijos
	k=param.k;			//Asignacion del nuevo tamano
	hijos=new nodo<T>*[k];	//Asignacion de memoria a hijos
	Error(!hijos,"No hay mas memoria");
					//Verificacion de memoria suficiente
	for(int i=0; i<k; i++)		//Copia los elementos de param al
		hijos[i]=param.hijos[i];		//nodo que se esta creando

	return *this;
}

template <class T>
nodo<T>::nodo(const nodo<T>& param)	//Constructor de copia
{
	k=param.k;			//Inicializacion del tamano
	hijos=new nodo<T>*[k];       	//Asignacion de memoria a hijos
	Error(!hijos,"No hay mas memoria");
					//Verificacion de memoria suficiente
	for(int i=0; i<k; i++)		//Copia los elementos de param al
		hijos[i]=param.hijos[i];	//nodo que se esta creando
}

#ifndef _error_
#define _error_
void Error(int cond, char* str)
{
	if (cond)
	{
		cout << str;
		exit(1);
	}
}
#endif

