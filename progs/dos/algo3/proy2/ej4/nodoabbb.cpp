//
// Miembros Clase Nodo ABBB
//

#include <iostream.h>
#include <stdlib.h>
#include "nodoabbb.h"

template <class T>
nodo<T>::nodo()			     		//Constructor
{
	hijo=new nodo<T>* [2];			//Asignacion de memoria a los hijos
							//del nodo
	Error(!hijo,"\nNo hay mas memoria");	//Verificacion de memoria suficiente
	balance=0;
	hijo[0]=NULL;				//Inicializa los elementos de hijos
	hijo[1]=NULL;
	datos=NULL;
}

template <class T>
nodo<T>::~nodo(void)				//Destructor
{
	delete[] hijo;				//Liberacion de la memoria asignada
							//al vector de los hijos
}

template <class T>
nodo<T>& nodo<T>::operator=(const nodo<T>& param)
							//Asignacion de nodos
{
	delete[] hijo;				//Liberacion de la memoria
							//asignada a los hijos
	hijo=new nodo<T>* [2];			//Asignacion de memoria a hijos
	Error(!hijo,"\nNo hay mas memoria");	//Verificacion de memoria suficiente
	hijo[0]=param.hijo[0];			//Copia los hijos
	hijo[1]=param.hijo[1];
	datos=param.datos;                  //Copia los datos

	return *this;
}

template <class T>
nodo<T>::nodo(const nodo<T>& param)		//Constructor de copia
{
	hijo=new nodo<T>*[2];	     		//Asignacion de memoria a hijos
	Error(!hijo,"\nNo hay mas memoria"); 	//Verificacion de memoria suficiente
	hijo[0]=param.hijo[0];			//Copia los hijos
	hijo[1]=param.hijo[1];
	datos=param.datos;
}


void Error(int cond, char* str)
{
	if (cond)
	{
		cout << str;
		exit(1);
	}
}

