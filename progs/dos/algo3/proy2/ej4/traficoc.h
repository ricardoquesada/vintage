//
// Definiciones Clase Trafico
//

#ifndef _trafico_
#define _trafico_
#include "vuelo.h"
#include "cqt.h"

class Trafico
{
public:
	Trafico();
	void LeerVuelos(char* filename);
	void ProcesarConsultas(char* filename);
private:
	CQT<Vuelo> vuelos;		// quad-tree de vuelos

	void Parse(istream&);
					// reconocimiento del archivo
};
#endif
