//
// Definiciones Clase Trafico
//

#ifndef _trafico_
#define _trafico_
#include "vuelo.h"
#include "kdtree.h"

class Trafico
{
public:
	Trafico();
	void LeerVuelos(char* filename);
	void ProcesarConsultas(char* filename);
private:
	CKDT<Vuelo> vuelos;		// k-d-tree de vuelos

	void Parse(istream&);
					// reconocimiento del archivo
};
#endif
