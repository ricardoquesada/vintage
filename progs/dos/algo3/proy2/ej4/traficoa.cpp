//
// Miembros Clase Trafico
//

#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vuelo.cpp"
#include "cabbb.cpp"
#include "traficoa.h"

Trafico::Trafico()		// constructor
{
	CABBB<Vuelo> aux(5);	// prepara la variable vuelos especif. 5 cla-
	vuelos=aux;			// ves
}

void Trafico::LeerVuelos( char* filename )	// se lee de un archivo los
{							// vuelos y se los al-
							// macena en la estruc.
	ifstream handle (filename, ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";
	handle >> vuelos;
	handle.close();
}

void Trafico::ProcesarConsultas(char* entrada)
{					// se ejecuta un archivo de consultas
	ifstream handle (entrada, ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";

	Parse(handle); 		      	// se llama al parse
	handle.close();
}

void Trafico::Parse( istream& handle )
{					// reconoce las consultas y las	ejecuta
	signed char linea[255];
	char accion[20];		// accion a realizar con el registro
	char cai[60],cad[60];		// limites inf. y sup. companias
	char aoi[4],aod[4];		// limites inf. y sup. aero. origen
	char moi[13],mod[13];		// limites inf. y sup. momento salida
	char adi[4],add[4];		// limites inf. y sup. aero. destino
	char mdi[13],mdd[13];		// limites inf. y sup. momento llegada
	char comando[3];		// campo condicion
	char par1[60];			// limite inferior aux.
	char par2[60];			// limite superior aux.
	int corchete;
	int i, j, par, nco=1;
	int cinf,csup;			// campos especificados

	handle.getline(linea, 255, '\n');
	while (handle)			// mientras no haya fin de archivo
	{					// analiza linea por linea
		i=0;			// inicializa variables para nueva
		j=0;				// linea a analizar
		corchete=0;
		cai[0]=0; aoi[0]=0; moi[0]=0; adi[0]=0;	mdi[0]=0;
		cad[0]=0; aod[0]=0; mod[0]=0; add[0]=0;	mdd[0]=0;
		accion[0]=0;
		par1[0]=0; par2[0]=0; comando[0]=0;
		while(i<strlen(linea) && linea[i]!=';')
		{		 		// analiza la condicion
			if(linea[i]!=' ')
			{
				switch(linea[i])
				{
				case '[': corchete=1;	// empiezan los limi-
					  par=0;		// tes
					  j=0;
					  break;
				case ']': corchete=0;	// se termino la con-
					  j=0;			// dicion
					  break;
				case ',': if(corchete)	// se termino el 1er.
					  {			// limite
						par=1;
						j=0;
					  }
					  break;
				default : if(!corchete)
					  {
						if(j<2) // arma el campo
						{
							comando[j]=linea[i];
							comando[j+1]=0;
						}
					  }
					  else
					  {
						if(!par) // arma limite inf.
						{
							par1[j]=linea[i];
							par1[j+1]=0;
						}
						else
						{	 // arma limite sup.
							par2[j]=linea[i];
							par2[j+1]=0;
						}
					  }
					  j++;
					  break;
				}
				if(linea[i]==']')
				{		// verifica que tipo de campo
							// se esta pidiendo
							// y copia los limites
					if(stricmp(comando,"CA")==0)
					{
						strcpy(cai, par1);
						strcpy(cad, par2);
					}
					if(stricmp(comando,"AO")==0)
					{
						strcpy(aoi, par1);
						strcpy(aod, par2);
					}
					if(stricmp(comando,"MO")==0)
					{
						strcpy(moi, par1);
						strcpy(mod, par2);
					}
					if(stricmp(comando,"AD")==0)
					{
						strcpy(adi, par1);
						strcpy(add, par2);
					}
					if(stricmp(comando,"MD")==0)
					{
						strcpy(mdi, par1);
						strcpy(mdd, par2);
					}
					j=0;  	// inicializa variables para
					par=0;		// nueva restriccion
					par1[0]=0;
					par2[0]=0;
					comando[0]=0;
				}
			}
			i++;
		}
		i++;
		j=0;
		while(i<strlen(linea))	// analiza la accion
		{
			if(linea[i]!=' ')
			{
				accion[j]=linea[i];
				accion[j+1]=0;
				j++;
			}
			i++;
		}

					// prepara rango para consultar
		Vuelo inf(cai, aoi, moi, adi, mdi, "",0); // vuelo inferior
		Vuelo sup(cad, aod, mod, add, mdd, "",0); // vuelo superior

		cinf=0;
		csup=0;
		if(cai[0]!=0)	    	// se arma el indicador de claves
			cinf++;			// activas inferiores
		if(aoi[0]!=0)
			cinf+=2;
		if(moi[0]!=0)
			cinf+=4;
		if(adi[0]!=0)
			cinf+=8;
		if(mdi[0]!=0)
			cinf+=16;
		if(cad[0]!=0)		// se arma el indicador de claves
			csup++;			// activas superiores
		if(aod[0]!=0)
			csup+=2;
		if(mod[0]!=0)
			csup+=4;
		if(add[0]!=0)
			csup+=8;
		if(mdd[0]!=0)
			csup+=16;

		if(accion[0]!=0)	// si hay alguna accion se muestra
		{				// consulta efectuada
			cout << "\n" << "Consulta " << nco << ": (" << linea << ")\n";
			nco++;
		}
		if(stricmp(accion,"MCO")==0)
		{			// si la accion es mostrar consultas,
						// las muestra por pantalla
			if(inf==sup && cinf>30 && csup>30)
			{		// si se busca sin rango
				if(vuelos.Buscar(inf))
					cout << inf;
			}
			else
			{
				vuelos.BusRango(inf, cinf, sup, csup,
						"salaux.txt");
					// utiliza como archivo aux. salaux
				ifstream handle1 ("salaux.txt", ios::in);
				if(!handle1)
					cout << "No se pudo generar la consulta\n";

				while (handle1.getline(linea, 255, '\n'))
					cout << linea << "\n";
					// muestra por pantalla los resultados
				handle1.close();
			}
		}
		handle.getline(linea, 255, '\n');
	}
}