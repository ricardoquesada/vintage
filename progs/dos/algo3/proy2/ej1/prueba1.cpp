#include <iostream.h>
#include <fstream.h>
#include <stdlib.h>
#include "elem.cpp"
#include "nodoabbb.cpp"
#include "cabbb.cpp"

void main(void)
 {
	CABBB<Elem> abbb(3);

	cout << "Este es un programa de prueba del CABBB\n";
	ifstream handle ("maria.txt", ios::in);
	if(!handle)
		cout << "Error de apertura del archivo\n";
	handle >> abbb;
	cout << "\nLISTA: \n" << abbb;
	cout << "\nTama�o: " << abbb.Tamano() << "\n";
	cout << "Vacio?: " << (abbb.Vacio()?"NO":"SI") << "\n";
	handle.close();
}