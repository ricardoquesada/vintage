#include <iostream.h>
#include <stdlib.h>
#include "elem.cpp"
#include "CABBB.CPP"
#include "NodoABBB.CPP"

void main(void)
 {
	CABBB<Elem> qt=3;

	cout << "Este es un programa de prueba del KDT\n";
	cout << "Tama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
	cout << "Ahora se insertan 3 elementos en el arbol\n";
	char cd[10];
	randomize();
	Elem f("Ricardo","Quesada","rquesada@dc.uba.ar","805-1247","Estudiante");
	for (int i=1; i<100; i++)
	{
		itoa(random(100), cd, 10);
		cout << cd << ";";
		Elem e(cd, cd, cd, cd, cd);
		qt.Insertar(e);
	}

	qt.Insertar(f);

	cout << "\nLISTA: " << qt;
	Elem eb("10", "10","10","","");
	cout << "\nBUSCO " << (qt.Buscar(eb)?"OK":"ERROR") << " elemento: " << eb;
	Elem f1("Ricardo","Quesada","rquesada@dc.uba.ar","","");
	cout << "\nBorrar " << (qt.Borrar(f1)?"OK":"ERROR") << " elemento: " << f1;
	cout << "\nBuscar " << (qt.Buscar(f1)?"OK":"ERROR") << " elemento: " << f1;
	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
	for (i=1; i<1000; i++)
	{
		itoa(random(100), cd, 10);
		Elem e(cd, cd, cd, "", "");
		cout << e << qt.Borrar(e);
	}
	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";

	Elem eb1("8","8","8","","");
	Elem eb2("z","z","z","","");
	qt.BusRango(eb1, 7, eb2, 7, "salida.txt");
	cout << "Ahora se limpia el arbol\n";
	qt.Limpiar();
	cout << "\nTama�o: " << qt.Tamano() << "\n";
	cout << "Vacio?: " << (qt.Vacio()?"NO":"SI") << "\n";
}