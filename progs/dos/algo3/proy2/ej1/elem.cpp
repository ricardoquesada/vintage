//
// Miembros Clase Elem
//

#include <fstream.h>
#include <string.h>
#include "elem.h"

Elem::Elem()
{
	strcpy(apellido,"");
	strcpy(nombre,"");
	strcpy(telefono,"");
	strcpy(direccion,"");
	strcpy(e_mail,"");
}

Elem::Elem(char* ape, char* nom, char* tel, char* dir, char* ema)
{
	strcpy(apellido, ape);
	strcpy(nombre, nom);
	strcpy(telefono, tel);
	strcpy(direccion, dir);
	strcpy(e_mail, ema);
}

char * Elem::Apellido()
{
	return apellido;
}
char * Elem::Nombre()
{
	return nombre;
}
char * Elem::Telefono()
{
	return telefono;
}

char * Elem::Direccion()
{
	return direccion;
}

char * Elem::E_mail()
{
	return e_mail;
}

int Elem::operator==(const Elem& per) const
{
	return (strcmp(nombre, per.nombre)==0 &&
		strcmp(apellido, per.apellido)==0 &&
		strcmp(telefono, per.telefono)==0);
}

int Elem::operator<(const Elem& per) const
{
	if (strcmp(apellido, per.apellido)< 0)
		return 1;
	else if (strcmp(apellido, per.apellido)==0 && strcmp(nombre, per.nombre)<0)
		return 1;
	else if (strcmp(apellido, per.apellido)==0 && strcmp(nombre, per.nombre)==0
		   && strcmp(telefono, per.telefono)<0)
		return 1;
	else
		return 0;
}


Elem& Elem::operator=(const Elem& ele)	// operador de asignacion
{
	strcpy(apellido, ele.apellido);
	strcpy(nombre, ele.nombre);
	strcpy(telefono, ele.telefono);
	strcpy(direccion, ele.direccion);
	strcpy(e_mail, ele.e_mail);

	return *this;		// devuelve puntero al nuevo arbol
}

int Elem::Menor(int i, const Elem& per) const
{
	int res;

	switch(i)
	{
		case 1: res=(strcmp(apellido, per.apellido)<0);
			break;
		case 2: res=(strcmp(nombre, per.nombre)<0);
			break;
		case 3: res=(strcmp(telefono, per.telefono)<0);
			break;
	}
	return (res);
}

int Elem::Igual(int i, const Elem& per) const
{
	int res;

	switch (i)
	{
		case 1: res=(strcmp(apellido, per.apellido)==0);
			break;
		case 2: res=(strcmp(nombre, per.nombre)==0);
			break;
		case 3: res=(strcmp(telefono, per.telefono)==0);
			break;
	}
	return (res);
}

ostream& operator<<(ostream& salida, const Elem& ele)
{
	salida << '"';
	salida << ele.apellido;
	salida << '"' << "," << '"';
	salida << ele.nombre;
	salida << '"' << "," << '"';
	salida << ele.telefono;
	salida << '"' << "," << '"';
	salida << ele.direccion;
	salida << '"' << ","<< '"' ;
	salida << ele.e_mail;
	salida << '"' << '\n';

	return salida;
}

istream& operator>>(istream& entrada, Elem& ele)
{
	char caracter;
	signed char linea[200];
	int comillas=0;
	int campo=1,j=0;

	entrada.getline(linea, 200, *("\n"));
	for(int i=0;i<strlen(linea);i++)
	{
		caracter=linea[i];
		if((caracter==*(",") && comillas==0) || caracter==34)
		{
			if(caracter==34)
				comillas=!(comillas);
			else
				campo++;
			j=0;
		}
		else
		{
			switch(campo)
			{
				case 1:	ele.apellido[j]=caracter;
					ele.apellido[j+1]=0;
					break;
				case 2:	ele.nombre[j]=caracter;
					ele.nombre[j+1]=0;
					break;
				case 3:	ele.telefono[j]=caracter;
					ele.telefono[j+1]=0;
					break;
				case 4:	ele.direccion[j]=caracter;
					ele.direccion[j+1]=0;
					break;
				case 5:	ele.e_mail[j]=caracter;
					ele.e_mail[j+1]=0;
					break;
			}
			j++;
		}
	}

	return entrada;
}
