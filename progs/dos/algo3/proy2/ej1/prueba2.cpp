#include <iostream.h>
#include <stdlib.h>
#include "elem.cpp"
#include "nodoabbb.cpp"
#include "cabbb.cpp"

void main(void)
 {
	CABBB<Elem> abbb(3);

	cout << "Este es un programa de prueba del ABBB\n";
	cout << "Tama�o: " << abbb.Tamano() << "\n";
	cout << "Vacio?: " << (abbb.Vacio()?"NO":"SI") << "\n";
	cout << "Ahora se insertan 3 elementos en el arbol\n";
	char cd[10];
	randomize();
	for (int i=1; i<2; i++)
	{
		itoa(random(100), cd, 10);
		cout << cd << ";";
		Elem e(cd, cd, cd, cd, cd);
		abbb.Insertar(e);
	}
	cout << "\nLISTA: " << abbb;
	Elem eb("3", "3","3","","");
	cout << "\nBUSCO " << (abbb.Buscar(eb)?"OK":"ERROR") << " elemento: " << eb;
	cout << "\nTama�o: " << abbb.Tamano() << "\n";
	cout << "Vacio?: " << (abbb.Vacio()?"NO":"SI") << "\n";
	Elem eb1("5","5","5","","");
	Elem eb2("7","7","7","","");
	abbb.BusRango(eb1, 7, eb2, 7, "salida.txt");
	cout << "Ahora se limpia el arbol\n";
	abbb.Borrar(e);
//	abbb.Limpiar();
	cout << "\nTama�o: " << abbb.Tamano() << "\n";
	cout << "Vacio?: " << (abbb.Vacio()?"NO":"SI") << "\n";
}