/* Magic v1.2 por Ricardo Quesada */
/* 28-29-30/3/94 */
/* Cambiador de Magic's names para Portal Of Power 0.62   */

#include <stdio.h>
#include <string.h>
#include <io.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <conio.h>
#include <stdlib.h>
#include <dos.h>
#include <ctype.h>

void Register( void );
void LeerConfig( void );
void toArriba( char * );
void About( void );
void Salir( int );
void Abrir( void );
void Leer( void );
void LeerDialog( void );
void Ver( void );
void Write( void );
void Ayuda( void );
void Time( void );
void Default ( void );

struct MagicPortal
{
	unsigned char NodeStatus;
	unsigned char LongMagic;
	char MagicName[20];
	unsigned char LongPath;
	char PathName[79];
	unsigned char LongPassword;
	char Password[10];
	unsigned char Level;
	unsigned char Keys;
	char FreeArea;
	unsigned char TaskNumber;
	char Basura[11];
} Config;

FILE *stream, *config;
int handle;
int nomostrar = 1;

char PortalDir[80];
char LogName[80];
char LogDir[80];
int  nolog;
char RegisterName[60];
int registrado;

char Name[20];
char Path[79];

/*Convierte todo a upper case */
void toArriba( char * Point)
{
   int length, i;

   length = strlen( Point );
   for (i=0; i<length; i++)
   {
	  Point[i] = toupper(Point[i]);
   }

}


/* codigos por default */
void Default()
{
	strcpy(PortalDir,"");
	strcpy(LogName,"MAGIC.LOG");
	strcpy(LogDir,"");
	nolog=1;
	registrado=0;
}


/* escribe en Stream la hora y fecha */
void Time()
{
	struct date d;
	struct time t;

	getdate(&d);
	fprintf(stream," MAGIC - %d/%d/%d ",d.da_day,d.da_mon,d.da_year);
	gettime(&t);
	fprintf(stream,"%2d:%02d:%02d - ",t.ti_hour, t.ti_min, t.ti_sec);
}


/* Lee archivo de configuracion */
void LeerConfig()
{
	int i;
	char buffer[50];
	char names[128];
	char ch;

	for(i=0;i<200;i++)
	{
		ch=fgetc( config );
		if(ch==';')
		{
			do{
				ch=fgetc(config);
			} while(ch != 0x0A ) ;
		}
		if(ch=='$')
		{
			i=0;
			do{
				ch = fgetc(config);
				buffer[i] = ch;
				if(ch==0x0A)
					break;
				i++;
			} while( ch != '=' );

			buffer[i-1] = 0;

			if ( strcmp(buffer,"PORTALPATH" ) == 0)
			{
				i=0;
				do{
					ch=fgetc(config);
					names[i] = ch;
					i++;
				}  while( ch != 0x0A );
				names[i-1] = 0;
				strcpy(PortalDir,names);
			}
			else{
				if ( strcmp(buffer,"LOGLEVEL") == 0 )
				{
					i=0;
					do{
						ch=fgetc(config);
						names[i] = ch;
						i++;
					}  while( ch != 0x0A );
					names[i-1] = 0;
					nolog = atoi( names );
				}

				else{
					if( strcmp(buffer,"LOGPATH") == 0 )
					{
						i=0;
						do{
							ch=fgetc(config);
							names[i] = ch;
							i++;
						} while( ch != 0x0A );
						names[i-1] = 0;
						strcpy(LogDir,names);
					}
					else{
						if( strcmp(buffer,"LOGNAME") == 0)
						{
							i=0;
							do{
								ch=fgetc(config);
								names[i] = ch;
								i++;
							} while( ch != 0x0A );
							names[i-1] = 0;
							strcpy(LogName,names);
						}
						else{
							if( strcmp(buffer,"JURO QUE INVITE UNA CERVEZA A RICARDO QUESADA")== 0)
							{
								registrado = 1;
								i=0;
								do{
									ch=fgetc(config);
									names[i] = ch;
									i++;
								} while( ch != 0x0A );
								names[i-1] = 0;
								if( strlen(names) < 1)
									strcpy(RegisterName,"A nadie. Que raro, no?");
								else { strcpy(RegisterName,names); }

							}
						}
					}
				}
			}
		}
	} while (ch != EOF );
}

/* Abre archivo Portal.okf y Magic.Log */
void Abrir()
{
	strcat(LogDir,LogName);                /* une directorios */
	strcat(PortalDir,"PORTAL.OKF");        /* une directorios */

	if( (stream = fopen( LogDir ,"at") )== NULL )
		nolog = 0;
	if( (nolog) && (registrado) )
	{
		fprintf(stream,"\n\n+");
		Time();
		fprintf(stream,"INICIO.");
	}
	if ( (nolog == 2) && (registrado) )
	{
		fprintf(stream,"\n:");
		Time();
		fprintf(stream,"Log   : %s",LogDir );

		fprintf(stream,"\n:");
		Time();
		fprintf(stream,"Portal: %s",PortalDir );
	}
	if(nomostrar == 2)
	{
		printf("\nLog    :%s",LogDir );
		printf("\nPortal :%s",PortalDir );
	}

	if( (handle = _open( PortalDir ,O_RDWR ) ) == -1)
	{
		if( nomostrar )
			cprintf("\n\rError al abrir PORTAL.OKF");
		if( (nolog) && (registrado) )
		{
			fprintf(stream,"\n!");
			Time();
			fprintf(stream,"Error al abrir PORTAL.OKF");
		}
		Salir(1);
	}
}


/* leer esto. */
void Leer()
{
   const unsigned int sizeofbuffer = sizeof(Config);

   if (_read(handle, &Config , sizeofbuffer ) != sizeofbuffer )
   {
		if(nomostrar)
			cprintf("\r\nNombre Magico no encontrado.");
		if( (nolog) && (registrado) )
		{
			fprintf(stream,"\n!");
			Time();
			fprintf(stream,"Nombre Magico no encontrado (%s)",Name);
		}
		Salir(2);
   }
}

/* write esto. */
void Write()
{
   const unsigned int sizeofbuffer = sizeof(Config);

   lseek( handle , -128L ,SEEK_CUR );

   if (_write(handle, &Config , sizeofbuffer ) != sizeofbuffer )
   {
		if(nomostrar)
			cprintf("\n\rError en la escritura.\n\r");
		if( (nolog) && (registrado) )
		{
			fprintf(stream,"\n!");
			Time();
			fprintf(stream,"Error en la escritura.");
		}
		Salir(1);
   }
}

void Salir( int salir )
{
	if( (nolog) && (registrado) )
	{
		fprintf(stream,"\n+");
		Time();
		fprintf(stream,"FIN");
		fclose ( stream );
	}
	close  ( handle );
	exit   ( salir );
}

/*  acerca del programa */
void About()
{
	if(nomostrar)
	{
		cprintf("\n\rMagic v1.20 - Por Ricardo Quesada (4:900/309.3@fidonet.org)");
		if(registrado)
			cprintf("\r\nVersion registrada a:%s\r\n",RegisterName);
	}
	if(!registrado)
		cprintf("\r\nVersion no registrada. Invitame una cerveza y vemos...\r\n");

}

/* ver archivo */
void Ver()
{
	unsigned char len;
	unsigned char lenn;

	int c;

	Abrir();

	if(nomostrar)
		cprintf("\n\rBuscando Magic:%s",Name );
	do {
		Leer();
		lenn = Config.LongPassword;
		Config.Password[lenn] = 0;
		lenn = Config.LongMagic;
		Config.MagicName[lenn] = 0;
		lenn = Config.LongPath;
		Config.PathName[lenn] = 0;
		if(nomostrar==2)
		{
			cprintf("\r\nMagicName:%s",Config.MagicName);
			cprintf("\r\nPath:%s",Config.PathName);
			cprintf("\r\nPassword:%s",Config.Password);
		}
		c = strcmp( Config.MagicName , Name );
	} while (c != 0);


	if( nomostrar )
	{
		cprintf("\r\nOld path:%s",Config.PathName);
		cprintf("\r\nNew path:%s",Path);
	}
	if( (nolog) && (registrado) )
	{
		fprintf(stream,"\n:");
		Time();
		fprintf(stream,"Nombre Magico %s",Name);
	}
	if( (nolog==2) && (registrado) )
	{
		fprintf(stream,"\n:");
		Time();
		fprintf(stream,"Old path %s",Config.PathName);
	}
	if( (nolog) && (registrado) )
	{
		fprintf(stream,"\n:");
		Time();
		fprintf(stream,"New path %s",Path);
	}
	(len) = strlen( Path );
	Config.LongPath = len;
	strcpy( Config.PathName, Path );

	Write();

}
/* ayuda */
void Ayuda()
{
	About();
	cprintf("\n\rEste programa actualiza los Magic Names para Portal v0.62�"
			"\n\rParametros:"
			"\r\n       MagicName = Nombre Magico que buscara"
			"\n\r       Path      = El path nuevo que tendra el nombre magico"
			"\r\n       [/O][/M]  = /O:Oculta todo /M:Muestra todo por pantalla"
			"\n\r\n\rEjemplo: MAGIC NODEDIFF C:\\FREQ\\NODEDIFF\\NODEDIFF.Z84"
			"\r\nPrograma dedicado a : Alejandro Alvarez (4:900/309)"
	);
	if(!registrado)
		cprintf("\r\nHola, yo soy BIRRAWARE y me registras invitandome una cerveza." );
	exit(0);
}


/* main function */
int main(int argc,char *argv[] )
{
	int i=0;
	char path[128];

	strcpy(path,argv[0]);

	i=strlen(path);  /* en el argv[0] viene el path complete.saco el directorio */
	for(;i>0;i--)    /* y hay agrego el nombre del config y ya tengo el path completo del config. */
	{
		if( path[i] =='\\')
			break;
	}
	path[i+1] = 0;
	strcat(path,"MAGIC.CFG");

	Default();
	if( (config = fopen( path ,"rt" ) ) != NULL )
		LeerConfig();

	for(i=0;i<argc;i++)
		toArriba(argv[i]);

	if( (argc == 4) && (strcmp(argv[3],"/O") == 0)  )
		nomostrar = 0;
	else {
		if( (argc == 4) && (strcmp(argv[3],"/M")==0) )
			nomostrar = 2;
		else {
			if( argc != 3 )
				Ayuda();
		}
	}
	About();

	strcpy( Name , argv[1] );
	strcpy( Path , argv[2] );

	Ver();
	Salir( 0 );

	return 0;  /* el codigo nunca va a llegar aca */
}
