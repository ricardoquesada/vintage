code    segment public byte 'code'
org     100h
VChar   PROC
assume cs:code,ds:code,ss:code,es:code

start:
mov forrex,0

fa:
call dac           ;peque�o ejemplo
mov ah,1
int 21h
cmp al,13
je finala
inc forrex
jmp short fa

finala:
mov ah,4ch
int 21h

dac proc near

mov al,forrex
mov dx,3c8h
out dx,al

inc dx       ;apuntar a 03c9h (el registro de lectura de datos de paleta)

mov al,255
out dx,al
mov al,42
out dx,al
mov al,255
out dx,al
ret
dac endp
forrex db ?

VChar   endp
code    ends
        end VChar

