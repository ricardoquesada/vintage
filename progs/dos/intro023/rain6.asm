.286c
assume cs:cseg,ds:cseg
cseg segment para public 'Rainbow'
org 100h
mproc proc near

                                cli
                                mov ax,3
                                int 10h

                                mov si,offset color
                                mov al,0ffh
                                out 21,al
waiting_for_retrace:
                                call scrollcolors

                                in  al,060h
                                cmp al,01h
                                je  escape

waiting_:                       mov dx,03dah
                                in  al,dx

                                and al,09h
                                or  al,al
                                je  waiting_

                                cmp al,01h
                                je  change_color

                                cmp al,09h
                                je retrace_wait_on

                                jmp waiting_for_retrace

retrace_wait_on:                in  al,dx
                                and al,1
                                jne retrace_wait_on
                                mov si,offset color
                                jmp waiting_for_retrace

change_color:                   mov dx,03c8h
                                mov al,color_offset
                                out dx,al
                                inc dx

                                outsb
                                outsb
                                outsb

                                jmp waiting_

escape:                         sti
                                mov al,000h
                                out 21,al
                                mov ax,3
                                int 010h
                                mov ax,4c00h
                                int 21h

scrollcolors proc near
        pusha

        cmp indice,100*3
        jb adf1
        cmp indice,200*3
        jb adf2
        mov indice,0
        mov indice2,0

adf1:
        mov dx,3
        mov si,offset color
loopdos:
        mov cx,100*3
loopuno:
        mov bx,indice
        add bx,cx
        mov al,[si+bx]
        mov [si+bx+1],al
        dec cx
        or cx,cx
        jne loopuno
        inc indice
        dec dx
        or dx,dx
        jne loopdos
        mov byte ptr [si+bx+0],0
        mov byte ptr [si+bx+1],0
        mov byte ptr [si+bx+2],0

        mov dx,3
loopcuatro:
        xor bx,bx
looptres:
        mov al,[si+bx+100*6+1]
        mov [si+bx+100*6],al
        inc bx
        cmp bx,100*3
        jne looptres
        dec dx
        or dx,dx
        jne loopcuatro
        mov byte ptr [si+bx+100*6+0],0
        mov byte ptr [si+bx+100*6+1],0
        mov byte ptr [si+bx+100*6+2],0
        popa
        ret

adf2:
        mov si,offset color
        mov dx,3
loop2:
        mov cx,100*3
loop1:
        mov bx,indice
        add bx,cx
        mov al,[si+bx+0]
        mov [si+bx+1],al
        dec cx
        or cx,cx
        jne loop1
        inc indice
        dec dx
        or dx,dx
        jne loop2

        mov di,offset color2
        mov dx,indice2
        add di,dx
        mov bx,indice
        mov al,[di+0]
        mov [si+bx+0],al
        mov al,[di+1]
        mov [si+bx+1],al
        mov al,[di+2]
        mov [si+bx+2],al

        mov dx,3
loop4:
        xor bx,bx
loop3:
        mov al,[si+bx+1]
        mov [si+bx+0],al
        inc bx
        cmp bx,100*3
        jb loop3
        dec dx
        or dx,dx
        jne loop4

        mov di,offset color2
        mov dx,indice2
        add di,dx
        mov al,[di+0]
        mov [si+99*3+0],al
        mov al,[di+1]
        mov [si+99*3+1],al
        mov al,[di+2]
        mov [si+99*3+2],al
        add indice2,3
        popa
        ret
scrollcolors endp

color_offset                    equ 0

indice2 dw 0
indice  dw 0

include                         color6.inc

mproc endp
cseg ends
end mproc
