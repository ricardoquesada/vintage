.286c
assume cs:cseg,ds:cseg
cseg segment para public 'Rainbow'
org 100h
mproc proc near
                                cld
                                mov ax,0b800h
                                mov es,ax
                                xor di,di
                                mov si,offset imagedata
                                mov cx,160*23
rep                             movsb

                                mov al,0ffh
                                out 21,al
waiting_for_retrace:            in  al,060h
                                cmp al,01h
                                je  escape

waiting_:                       mov dx,03dah
                                in  al,dx

                                and al,09h

;                                or  al,al
;                                je  waiting_

                                cmp al,01h
                                je  change_color

                                cmp al,09h
                                je retrace_wait_on

                                jmp waiting_

retrace_wait_on:                in  al,dx
                                and al,1
                                jne retrace_wait_on
                                mov si,offset color
                                jmp waiting_for_retrace

change_color:                   mov dx,03c8h
                                mov al,color_offset
                                out dx,al
                                inc dx
                                mov cx,6
rep                             outsb
                                jmp waiting_

escape:                         sti
                                mov al,000h
                                out 21,al
                                mov ax,3
                                int 010h
                                mov ax,4c00h
                                int 21h

color_offset                    equ 1

include                         pantalla.asm

include                         color2.inc

mproc endp
cseg ends
end mproc
