color_offset                    equ 1


.286c
assume cs:cseg,ds:cseg
cseg segment para public 'Rainbow'
org 100h
mproc proc near

        call is286
        call isvga
		cmp al,1ah				;compara si es placa VGA
		je vgaonly

		mov ah,9
        mov dx,offset novgamsg
		int 21h
        mov ax,4cffh
        int 21h
vgaonly:

                                cli
                                mov ax,3
                                int 10h
                                mov ax,01300h
                                mov bx,0001
                                mov cx,15*80
                                mov dx,0300h
                                mov bp,offset tester
                                int 10h
                                mov ah,2
                                xor bh,bh
                                mov dx,0ffffh
                                int 10h

                                mov si,offset color
                                mov al,0ffh
                                out 21,al
waiting_for_retrace:
                                call scrollcolors
                                call scrollcolors

                                in  al,060h
                                cmp al,01h
                                je  escape

waiting_:                       mov dx,03dah
                                in  al,dx

                                and al,09h
                                or  al,al
                                je  waiting_

                                cmp al,01h
                                je  change_color

                                cmp al,09h
                                je retrace_wait_on

                                jmp waiting_for_retrace

retrace_wait_on:                in  al,dx
                                and al,1
                                jne retrace_wait_on
                                mov si,offset color
                                jmp waiting_for_retrace

change_color:
                                mov dx,03c8h
                                mov al,color_offset
                                out dx,al
                                inc dx
                                cld
                                mov cx,3
rep                             outsb
                                cmp si,offset colornulo
                                jb waiting_
                                mov si,offset colornulo
                                jmp waiting_


escape:                         sti
                                mov al,000h
                                out 21,al
                                mov ax,3
                                int 010h

                                mov ax,01300h
                                mov bx,0070h
                                mov cx,1*80
                                mov dx,0000h
                                mov bp,offset finmsg
                                int 10h

                                mov ax,4c00h
                                int 21h

scrollcolors proc near
        pusha

        cmp indice,100*3
        jb adf1
        cmp indice,200*3
        jb adf2
        mov indice,0
        mov indice2,0

adf1:
        std
        mov cx,3 * 100
        mov si,offset color
        mov di,offset color + 3
        mov bx,indice
        add cx,bx
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset color
loop1:
        mov [di],ch
loop    loop1

        cld
        mov si,offset color + 3 * 201 + 3
        mov di,offset color + 3 * 201
        mov cx,3 * 100
rep     movsb

        mov di,offset color + 3 * 200
        mov cx,3
loop2:
        mov [di],ch
loop    loop2

        add indice,3

        popa
        ret

adf2:
        std
        mov cx,3 * 100
        mov si,offset color + 3 * 101
        mov di,offset color + 3 * 101 + 3
        add si,indice
        add di,indice
rep     movsb

        cld
        mov cx,3
        mov di,offset color
        mov si,offset color2
        add si,indice2
        add di,indice
rep     movsb

        cld
        mov si,offset color + 3
        mov di,offset color
        mov cx,3 * 100
rep     movsb

        mov di,offset color + 3 * 100
        mov si,offset color2
        add si,indice2
        mov cx,3
rep     movsb

        add indice2,3
        add indice,3

        popa
        ret
scrollcolors endp

;***********************;
; Is286                 ;
;***********************;
is286 proc near
    xor ax,ax
    push ax
    popf
    pushf
    pop ax
    and ax,0f000h
    cmp ax,0f000h
    je not286
    ret
not286:
    mov dx,offset no286msg
    mov ah,9
    int 21h
    mov ax,4cffh
    int 21h
is286 endp

;***********************;
;		isvga			;
;***********************;
isvga proc near
		mov ax,1a00h
		int 10h
		ret
isvga  endp

tester label byte
db '                         旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�                      '
db '                         �     �     複複�     複    複  �                      '
db '                         �   蔔 白   栢   白   栢�   栢  �                      '
db '                         �  査� 査�  栢   査�  栢査� 栢  �                      '
db '                         �  栢   栢  栢    栢  栢 査朋�  �                      '
db '                         �  栢   栢  栢    栢  栢  査栢  �                      '
db '                         넵輻賽賽賽複賽複複賽複賽複複賽複�                      '
db '                         넴�  栢�  栢  栢栢  栢  栢栢  栢�                      '
db '                         넴�  栢�  栢  栢百 査�  栢栢  栢�                      '
db '                         넴�  栢�  栢  栢� 蔔栢  栢栢  栢�                      '
db '                         넴白蔔栢複栢複複蔔栢栢複栢栢複栢�                      '
db '                         읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�                      '
db '                         旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�                      '
db '                         �     Assembly Demo Network     �                      '
db '                         읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�                      '

finmsg label byte
db 'by El ni쨚 del Sol                       Basado en Rainbow de Maximiliano Kolus.'
db 13,10,13,10,'Hecha entre el 1 y 3 de nov. de 1994 - v1.01  - el ni쨚 del sol -',13,10,13,10
no286msg db'Requiere 286 o compatible para correr.',13,10,'$'
novgamsg db'Requiere VGA o compatible para correr.',13,10,'$'
indice2 dw 0
indice  dw 0

include                         color6.inc

mproc endp
cseg ends
end mproc
