;VChar v3.00b (c) Ricardo Quesada 1994
;Tabla de caracteres generada en assembler
;Cualquier duda acerca del c�digo por favor hagamela llegar.
;Compilar de la siguiente manera:
;TASM nombre
;TLINK /t nombre

code    segment public byte 'code'
org     100h
VChar   PROC
assume cs:code,ds:code,ss:code,es:code

start:

call bit8           ;peque�o ejemplo

mov ah,4ch
int 21h



bit8 proc

mov dx,3cch
in al,dx
and al,00111111b
or  al,10000000b
mov dx,3c2h
out dx,al

cli
;   mov dx,3c4h
;   mov ax,100h
;   out dx,ax
;
;   mov ax,101h
;   out dx,ax
;
;   mov ax,300h
;   out dx,ax
;   sti
;
;   mov ax,1000h
;   mov bl,13h
;   mov bh,0
;   int 10h
ret

bit8 endp



;Funci�n de 9 bits
;bit9 proc

;mov dx,3cch
;in al,dx
;and al,11110011b
;or al,00000100b
;mov dx,3c2h
;out dx,al

;cli
;mov dx,3c4h
;mov ax,100h
;out dx,ax

;mov ax,001h
;out dx,ax

;mov ax,300h
;out dx,ax
;sti

;mov ax,1000h
;mov bl,13h
;mov bh,8
;int 10h
;ret

;bit9 endp



VChar   endp
code    ends
        end VChar
