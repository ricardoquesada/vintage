color_uno  equ 1

.286c
assume cs:cseg,ds:cseg
cseg segment para public 'Ilusion'
org 100h
mproc proc near

        mov bp,sp
        mov [bp],offset truejump
        mov cx,final - empiezaelxor
        mov di,offset final
dexoreo:
        xor [di],di
        dec di
loopnz  dexoreo
        ret
empiezaelxor:
        mov di,0a000h
        push si
        mov si,0b800h
        mov cx,4000
        dec dx
rep     movsb
        nop
        pop ax
        mov bx,offset scrollarriba
        nop
        jmp [bx]

        call $+300
truejump:
        call is286
        nop
        call isvga
		cmp al,1ah				;compara si es placa VGA
		je vgaonly

		mov ah,9
        mov dx,offset novgamsg
		int 21h
        mov ax,4cffh
        int 21h
vgaonly:

        cli
        mov ax,3
        int 10h

        push ds

        mov ax,0b800h
        mov ds,ax
        xor di,di
        mov cx,80 * 25
        xor bl,bl                       ; representa a la columan
primerloop:
        mov ax,01dbh
        cmp bl,40
        jb colorazul
        cmp bl,80
        jb colorverde
        xor bl,bl
        dec ah
colorverde:
        inc ah

colorazul:
        mov [di],ax
        add di,2
        inc bl
loop    primerloop

        pop ds

        mov ah,2
        xor bh,bh
        mov dx,0ffffh
        int 10h

        mov si,offset color
        mov al,0ffh
        out 21h,al
waiting_for_retrace:

        call scrollcolors
        call scrollcolors2
        call scrollall
        call scrollall2
        call cambiacolor

        in  al,060h
        cmp al,01h
        je  escape

waiting_:
        mov dx,03dah
        in  al,dx

        and al,09h
        or  al,al
        je  waiting_

        cmp al,01h
        je  change_color

        cmp al,09h
        je retrace_wait_on

        jmp waiting_for_retrace

retrace_wait_on:
        in  al,dx
        and al,1
        jne retrace_wait_on
        mov si,offset color
        jmp waiting_for_retrace

change_color:
        mov dx,03c8h
        mov al,color_uno
        out dx,al
        inc dx
        cld
        mov cx,3
rep     outsb

        push si
        add si, colordos - color - 2
        inc al
        out dx,al
        mov cx,3
rep     outsb
        pop si

        cmp si,offset colornulo
        jb waiting_
        mov si,offset colornulo
        jmp waiting_


escape:
        sti
        mov al,000h
        out 21h,al
        mov ax,3
        int 010h

        mov ax,01301h
        mov bx,0070h
        mov cx,1*80
        xor dx,dx
        mov bp,offset finmsg
        int 10h

        mov ax,4c00h
        int 21h

scrollcolors proc near
        pusha

        cmp indice, 50*3
        jb adf1
        cmp indice,100*3
        jb adf2
        mov indice,0
        mov indice2,0

adf1:
        std
        mov cx,3 * 50
        mov si,offset color + 2                     ; bug
        mov di,offset color + 3 + 2                 ; bug
        add si,indice3
        add di,indice3
        mov bx,indice
        add cx,bx
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset color
        add di,indice3
loop1:
        mov [di],ch
loop    loop1

        cld
        mov si,offset color + 3 * 50 * 2 + 3 + 3        ; bug
        mov di,offset color + 3 * 50 * 2 + 3            ; bug
        add si,indice3
        add di,indice3
        mov cx,3 * 50
rep     movsb

        mov di,offset color + 3 * 50 * 2
        add di,indice3
        mov cx,3
loop2:
        mov [di],ch
loop    loop2

        add indice,3

        popa
        ret

adf2:
;------parte de abajo
        std
        mov cx,3 * 50
        mov si,offset color + 3 * 50
        mov di,offset color + 3 * 50 + 3
        add si,indice3
        add di,indice3
        add si,indice
        add di,indice
rep     movsb

        cld
        mov cx,3
        mov di,offset color
        mov si,offset color2
        add si,indice2
        add di,indice
        add di,indice3
rep     movsb


;parte de arriba ---------
        cld
        mov si,offset color + 3
        mov di,offset color
        add si,indice3
        add di,indice3
        mov cx,3 * 50
rep     movsb

        mov di,offset color + 3 * 50
        mov si,offset color2
        add si,indice2
        add di,indice3
        mov cx,3
rep     movsb

        add indice2,3
        add indice,3

        popa
        ret
scrollcolors endp


scrollcolors2 proc near
        pusha

        cmp indic, 50*3
        jb adf12
        cmp indic,100*3
        jb adf22
        mov indic,0
        mov indic2,0

adf12:
        std
        mov cx,3 * 50
        mov si,offset colordos + 2                  ; bug
        mov di,offset colordos + 3 + 2              ; bug
        mov bx,indic
        add si,indic3
        add di,indic3
        add cx,bx
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset colordos
        add di,indic3
loop12:
        mov [di],ch
loop    loop12

        cld
        mov si,offset colordos + 3 * 50 * 2 + 3 + 3     ; bug
        mov di,offset colordos + 3 * 50 * 2 + 3         ; bug
        add si,indic3
        add di,indic3
        mov cx,3 * 50
rep     movsb

        mov di,offset colordos + 3 * 50 * 2
        add di,indic3
        mov cx,3
loop22:
        mov [di],ch
loop    loop22

        add indic,3

        popa
        ret

adf22:
;------parte de abajo
        std
        mov cx,3 * 50
        mov si,offset colordos + 3 * 50
        mov di,offset colordos + 3 * 50 + 3
        add si,indic3
        add di,indic3
        add si,indic
        add di,indic
rep     movsb

        cld
        mov cx,3
        mov di,offset colordos
        mov si,offset color2
        add si,indic2
        add di,indic3
        add di,indic
rep     movsb


;parte de arriba ---------
        cld
        mov si,offset colordos + 3
        mov di,offset colordos
        add si,indic3
        add di,indic3
        mov cx,3 * 50
rep     movsb

        mov di,offset colordos + 3 * 50
        mov si,offset color2
        add di,indic3
        add si,indic2
        mov cx,3
rep     movsb

        add indic2,3
        add indic,3

        popa
        ret
scrollcolors2 endp



;
;
scrollall proc near
        pusha

        cmp indice4, 100
        jb scrollabajo
        cmp indice4, 200
        jb scrollarriba
        mov indice4,0

scrollabajo:
        std
        mov cx,3 * 50 * 5
        mov si,offset color
        mov di,offset color + 3
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset color
loopall1:
        mov [di],ch
loop    loopall1

        inc indice4
        add indice3,3
        popa
        ret

scrollarriba:
        cld
        mov si,offset color + 3
        mov di,offset color
        mov cx,3 * 50 * 5
rep     movsb

        mov di,offset color + 3 * 50 * 5
        mov cx,3
loopall2:
        mov [di],ch
loop    loopall2

        sub indice3,3
        inc indice4

        popa
        ret
scrollall endp

;
;

scrollall2 proc near
        pusha

        cmp indic4, 100
        jb scrollabajo2
        cmp indic4, 200
        jb scrollarriba2
        mov indic4,0

scrollabajo2:
        std
        mov cx,3 * 50 * 5
        mov si,offset colordos
        mov di,offset colordos + 3
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset colordos
loopall12:
        mov [di],ch
loop    loopall12

        inc indic4
        add indic3,3
        popa
        ret

scrollarriba2:
        cld
        mov si,offset colordos + 3
        mov di,offset colordos
        mov cx,3 * 50 * 5
rep     movsb

        mov di,offset colordos + 3 * 50 * 5
        mov cx,3
loopall22:
        mov [di],ch
loop    loopall22

        sub indic3,3
        inc indic4

        popa
        ret
scrollall2 endp


cambiacolor proc near
    cmp indic4,50
    je cambiatrue
    ret
cambiatrue:
    pusha
    cmp indice5,3
    jb colores1
    cmp indice5,6
    jb colores2
    mov indice5,0
colores1:
    cld
    mov si,offset color2 + 1
    mov di,offset color2
    mov al,[di]
    mov cx,3 * 50
rep movsb
    mov [di],al
    inc indice5
    popa
    ret

colores2:
    std
    mov si,offset color2 - 1
    mov di,offset color2
    mov cx,3 * 50
    add si,cx
    add di,cx
    mov al,[di]
rep movsb
    mov [di],al
    inc indice5
    popa
    ret

cambiacolor endp


;***********************;
; Is286                 ;
;***********************;
is286 proc near
    xor ax,ax
    push ax
    popf
    pushf
    pop ax
    and ax,0f000h
    cmp ax,0f000h
    je not286
    ret
not286:
    mov dx,offset no286msg
    mov ah,9
    int 21h
    mov ax,4cffh
    int 21h
is286 endp

;***********************;
;		isvga			;
;***********************;
isvga proc near
		mov ax,1a00h
		int 10h
		ret
isvga  endp


finmsg label byte
db' --- ILUSION --- por El ni�o del Sol - Intro n�mero 3 - Hecha el 7,8/11/1994. - '
no286msg db'Only 286+',13,10,'$'
novgamsg db'Only VGA+',13,10,'$'

indice2 dw 0
indice  dw 0                    ;indices del 1er color

indic   dw 0
indic2  dw 0                    ;indices del 2do color

indice3 dw 0
indice4 dw 0                    ;indices del scrollall

indic3  dw 3 * 50 * 2
indic4  dw 100                  ;indices del scrollall2


indice5 db 0                    ;del cambiacolor proc

include color9.inc

final equ this byte

mproc endp
cseg ends
end mproc
