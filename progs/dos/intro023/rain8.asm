color_offset                    equ 0


.286c
assume cs:cseg,ds:cseg
cseg segment para public 'Rainbow'
org 100h
mproc proc near

        call is286
        call isvga
		cmp al,1ah				;compara si es placa VGA
		je vgaonly

		mov ah,9
        mov dx,offset novgamsg
		int 21h
        mov ax,4cffh
        int 21h
vgaonly:

                                cli
                                mov ax,3
                                int 10h
                                mov ah,2
                                xor bh,bh
                                mov dx,0ffffh
                                int 10h

                                mov si,offset color
                                mov al,0ffh
                                out 21,al
waiting_for_retrace:

                                call scrollcolors
                                call scrollall

                                in  al,060h
                                cmp al,01h
                                je  escape

waiting_:                       mov dx,03dah
                                in  al,dx

                                and al,09h
                                or  al,al
                                je  waiting_

                                cmp al,01h
                                je  change_color

                                cmp al,09h
                                je retrace_wait_on

                                jmp waiting_for_retrace

retrace_wait_on:                in  al,dx
                                and al,1
                                jne retrace_wait_on
                                mov si,offset color
                                jmp waiting_for_retrace

change_color:
                                mov dx,03c8h
                                mov al,color_offset
                                out dx,al
                                inc dx
                                cld
                                mov cx,3
rep                             outsb
                                cmp si,offset colornulo
                                jb waiting_
                                mov si,offset colornulo
                                jmp waiting_


escape:                         sti
                                mov al,000h
                                out 21,al
                                mov ax,3
                                int 010h

                                mov ax,01300h
                                mov bx,0070h
                                mov cx,1*80
                                mov dx,0000h
                                mov bp,offset finmsg
                                int 10h

                                mov ax,4c00h
                                int 21h

scrollcolors proc near
        pusha

        cmp indice, 50*3
        jb adf1
        cmp indice,100*3
        jb adf2
        mov indice,0
        mov indice2,0

adf1:
        std
        mov cx,3 * 50
        mov si,offset color
        mov di,offset color + 3
        add si,indice3
        add di,indice3
        mov bx,indice
        add cx,bx
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset color
        add di,indice3
loop1:
        mov [di],ch
loop    loop1

        cld
        mov si,offset color + 3 * 50 * 2 + 3
        mov di,offset color + 3 * 50 * 2
        add si,indice3
        add di,indice3
        mov cx,3 * 50
rep     movsb

        mov di,offset color + 3 * 50 * 2
        add di,indice3
        mov cx,3
loop2:
        mov [di],ch
loop    loop2

        add indice,3

        popa
        ret

adf2:
;------parte de abajo
        std
        mov cx,3 * 50
        mov si,offset color + 3 * 50
        mov di,offset color + 3 * 50 + 3
        add si,indice3
        add di,indice3
        add si,indice
        add di,indice
rep     movsb

        cld
        mov cx,3
        mov di,offset color
        mov si,offset color2
        add si,indice2
        add di,indice
        add di,indice3
rep     movsb


;parte de arriba ---------
        cld
        mov si,offset color + 3
        mov di,offset color
        add si,indice3
        add di,indice3
        mov cx,3 * 50
rep     movsb

        mov di,offset color + 3 * 50
        mov si,offset color2
        add si,indice2
        add di,indice3
        mov cx,3
rep     movsb

        add indice2,3
        add indice,3

        popa
        ret
scrollcolors endp



;
;
scrollall proc near
        pusha

        cmp indice4, 100
        jb scrollabajo
        cmp indice4, 200
        jb scrollarriba
        mov indice4,0

scrollabajo:
        std
        mov cx,3 * 50 * 5
        mov si,offset color
        mov di,offset color + 3
        add si,cx
        add di,cx
rep     movsb

        cld
        mov cx,3
        mov di,offset color
loopall1:
        mov [di],ch
loop    loopall1

        inc indice4
        add indice3,3
        popa
        ret

scrollarriba:
        cld
        mov si,offset color + 3
        mov di,offset color
        mov cx,3 * 50 * 5
rep     movsb

        mov di,offset color + 3 * 50 * 5
        mov cx,3
loopall2:
        mov [di],ch
loop    loopall2

        sub indice3,3
        inc indice4

        popa
        ret
scrollall endp


;***********************;
; Is286                 ;
;***********************;
is286 proc near
    xor ax,ax
    push ax
    popf
    pushf
    pop ax
    and ax,0f000h
    cmp ax,0f000h
    je not286
    ret
not286:
    mov dx,offset no286msg
    mov ah,9
    int 21h
    mov ax,4cffh
    int 21h
is286 endp

;***********************;
;		isvga			;
;***********************;
isvga proc near
		mov ax,1a00h
		int 10h
		ret
isvga  endp


finmsg label byte
db 'by El ni�o del Sol               - Horizontal Retrace: de Rainbow por Max. Kolus'
db 13,10,13,10,'Hecha entre el 1 y 3 de nov. de 1994 - v1.01  - el ni�o del sol -',13,10,13,10
no286msg db'Requiere 286 o compatible para correr.',13,10,'$'
novgamsg db'Requiere VGA o compatible para correr.',13,10,'$'

indice2 dw 0
indice  dw 0

indice3 dw 0
indice4 dw 0

include                         color8.inc

mproc endp
cseg ends
end mproc
