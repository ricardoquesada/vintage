/*                   - Frank Imburgio 4/7/88 -                           */
/*        modificada y mejorada por Ricardo Quesada 22/2/94              */
/*               mirar menu2.c para ver su funcionamiento                */

#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <string.h>
#include <ctype.h>
#include <bios.h>

int  getcursor(void);
int  menu(int menuchoice);
int  getkey(void);
void prompt(int x,int y,char *prompt, char *msg);
void changetotextcolor(void);
void changetobarcolor(void);
void clearmenugets(void);
void setbarcolor(int fg, int bg);
void settextcolor(int fg, int bg);
void setcursor(unsigned int shape);
void setmessageline(int line);

char *PROMPTS[24];
int  PROMPTY[24];
int  PROMPTX[24];
int  NPROMPTS=0;
int  PROMPTLINE=24;
char *PROMPTMSG[24];
int  nocursor = 0x2000;
int  TEXTF = WHITE;
int  TEXTB = BLACK;
int  BARF  = BLACK;
int  BARB  = LIGHTGRAY;

void prompt(int x,int y,char *prompt, char *msg)
{
    changetotextcolor();
    PROMPTX[NPROMPTS] = x;
    PROMPTY[NPROMPTS] = y;
    gotoxy(PROMPTX[NPROMPTS],PROMPTY[NPROMPTS]);
    PROMPTS[NPROMPTS] = prompt;
    PROMPTMSG[NPROMPTS] = msg;
    cprintf("%s",PROMPTS[NPROMPTS]);
    NPROMPTS++;
}

int menu(int menuchoice)
{
    int x=0,k=0,svcurs,i;
    if (menuchoice > 0 && menuchoice <NPROMPTS) x=menuchoice-1;
    svcurs = getcursor();
    setcursor(nocursor);
    while(1) {
       changetotextcolor();
       gotoxy(1,PROMPTLINE);
       clreol();
       gotoxy(40 - (strlen (PROMPTMSG[x]) /2),PROMPTLINE);
       cprintf("%s",PROMPTMSG[x]);
       gotoxy(PROMPTX[x],PROMPTY[x]);
       changetobarcolor();
       cprintf("%s",PROMPTS[x]);
       k = getkey();
       gotoxy(PROMPTX[x],PROMPTY[x]);
       changetotextcolor();
       cprintf("%s",PROMPTS[x]);
       switch (k) {
          case 27 :setcursor(svcurs);return 0;
          case 13 :setcursor(svcurs);return x+1;
	  case 328:x--;break;     /* crs lf  */
          case 333:x++;break;     /* crs up   */
          case 331:x--;break;     /* crs dw   */
          case 336:x++;break;     /* crs rg   */
          default : {
              for (i=0;i<NPROMPTS;i++) {
		 if (toupper(k) == toupper(*PROMPTS[i]) ) return i+1;
              }
          }

       }
       if (x>NPROMPTS-1) x=0;
       if (x<0) x=NPROMPTS-1;
    }

}

int getkey(void)
{
 int key, lo, hi;
 key = bioskey(0);
 lo = key & 0X00FF;
 hi = (key & 0XFF00) >> 8;
 return((lo == 0) ? hi + 256 : lo);
}

void settextcolor(int fg,int bg)
{
 TEXTF = fg;
 TEXTB = bg;
}

void setbarcolor(int fg,int bg)
{
 BARF = fg;
 BARB = bg;
}

void changetotextcolor(void)
{
 textcolor(TEXTF);
 textbackground(TEXTB);
}

void changetobarcolor(void)
{
 textcolor(BARF);
 textbackground(BARB);
}

void setcursor(unsigned int shape)
{
 union REGS reg;
 reg.h.ah = 1;
 reg.x.cx = shape;
 int86(0X10, &reg, &reg);
}

int getcursor(void)
{
 union REGS reg;
 reg.h.ah = 3;
 int86(0X10, &reg, &reg);
 return(reg.x.cx);
}

void setmessageline(int line)
{
 PROMPTLINE=line;
}

void clearmenugets(void)
{
    int x = 0;
    for (x=0;x<NPROMPTS;x++) {
       PROMPTS[x]     = "";          /*originalmente decia   PROMPTS[x] = ''; */
       PROMPTY[x]     = 1 ;
       PROMPTX[x]     = 1 ;
       PROMPTMSG[x]   = "";          /*  "                        "       "   */
       }
    NPROMPTS = 0;
}

