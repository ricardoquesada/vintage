/* FD2BT v1.0 por Ricardo Quesada */
/* 25-08-94 */
/* Conversor de nombres del pointlist al tb-point.usr */

#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <stdlib.h>
#include <ctype.h>

void Convertir( void );
void About( void );
void Ayuda( void );
void Nombre( void );
void toArriba( char * );

/* Variables Globales */
int zz;
FILE *tbpoint, *config;
char nombre[70];
char zone[10]="4";
char host[10]="?";
char node[10]="?";
char point[10]="?";



/*Convierte todo a upper case */
void toArriba( char * Point)
{
   int length, i;

   length = strlen( Point );
   for (i=0; i<length; i++)
   {
	  Point[i] = toupper(Point[i]);
   }

}

/* Nombre */
void Nombre( void )
{
	int i;
	char ch;
	for(i=0;i<2;i++) {
		do{
			ch = fgetc(config);
		} while( ch != ',');
	}

	i=0;
	do{
		ch = fgetc(config);
		if(ch=='_') ch=' ';
		nombre[i] = ch;
		if(ch==0x0A)
			break;
		i++;
	}while( ch != ',' );
	nombre[i-1] = 0;

	strcat(nombre,",");
	strcat(nombre,zone);
	if( (zz==0) || (zz==1) || (zz==2) )
	{
		strcat(nombre,":");
		strcat(nombre,host);
	}
	if( (zz == 0) || (zz == 1) )
	{
		strcat(nombre,"/");
		strcat(nombre,node);
	}
	if(zz == 0)
	{
		strcat(nombre,".");
		strcat(nombre,point);
	}
	strcat(nombre,"\n");
	fprintf(tbpoint,nombre);
	do{
		ch=fgetc(config);
	}while(ch != 0x0A );
}


/* Convertir */
void Convertir()
{
	int i;

	char ch;
	cprintf("\r\nConvirtiendo...");
	fprintf(tbpoint,";\n;FD2TB v1.00 (c) Ricardo Quesada 1994\n;\n");
	do{
		ch = fgetc(config);
		if(ch==';')
		{
			do{
			   ch=fgetc(config);
			}while(ch != 0x0A );
		}

		else if(ch==',')
		{
			i=0;
			if(zz==0) {
				do{
					ch = fgetc(config);
					point[i] = ch;
					i++;
				}while( ch != ',' );
				point[i-1] = 0;
			}
			else if(zz==1) {
				do{
					ch = fgetc(config);
					node[i] = ch;
					i++;
				}while( ch != ',' );
				node[i-1] = 0;
			}
			else if(zz==2) {
				do{
					ch = fgetc(config);
					host[i] = ch;
					i++;
				}while( ch != ',' );
				host[i-1] = 0;
			}
			Nombre();
		}
		else if( toupper(ch)=='B')			/*  Encontro "Boss" */
		{
			if( toupper(fgetc(config))=='O')
			{
				zz=0;
				while(ch!=',') ch=fgetc(config);
				i=0;
				do{
					ch = fgetc(config);
					zone[i] = ch;
					i++;
				}while( ch !=':');
				zone[i-1] = 0;

				i=0;
				do{
					ch = fgetc(config);
					host[i] = ch;
					i++;
				}while( ch !='/');
				host[i-1] = 0;

				i=0;
				do{
					ch = fgetc(config);
					node[i] = ch;
					i++;
				}while( ch !=0x0a);
				node[i-1] = 0;
			}
		}
		else if(toupper(ch)=='H')         	/* **** HOST **** */
		{
			ch = fgetc(config);
			if( toupper(ch)=='O')
			{
				if(toupper(fgetc(config))=='S')
				{
					zz=2;
					while(ch!=',') ch=fgetc(config);
					i=0;
					do{
						ch = fgetc(config);
						host[i] = ch;
						i++;
					}while( ch !=',');
					host[i-1] = 0;

					Nombre();

					zz=1;
					while(ch!=0x0a) ch=fgetc(config);
				}
			}
			else if(toupper(ch)=='U')     				/*  Hub  */
			{
				if(toupper(fgetc(config))=='B')
				{
					zz=1;
					while(ch!=',') ch=fgetc(config);
					i=0;      				/* Poner NODE */
					do{
						ch = fgetc(config);
						node[i] = ch;
						i++;
					}while( ch !=',');
					node[i-1] = 0;

					Nombre();

					while(ch!=0x0a) ch=fgetc(config);
				}
			}
		}
		else if(toupper(ch)=='R')         	/* **** Region **** */
		{
			ch = fgetc(config);
			if(toupper(ch)=='E')
			{
				if(toupper(fgetc(config))=='G')
				{
					zz=2;
					while(ch!=',') ch=fgetc(config);
					i=0;
					do{
						ch = fgetc(config);
						host[i] = ch;
						i++;
					}while( ch !=',');
					host[i-1] = 0;

					Nombre();
					zz=1;
					while(ch!=0x0a) ch=fgetc(config);
				}
			}
		}
		else if(toupper(ch)=='Z')         	/* **** Region **** */
		{
			ch = fgetc(config);
			if(toupper(ch)=='O')
			{
				if(toupper(fgetc(config))=='N')
				{
					zz=3;
					while(ch!=',') ch=fgetc(config);
					i=0;
					do{
						ch = fgetc(config);
						zone[i] = ch;
						i++;
					}while( ch !=',');
					zone[i-1] = 0;

					Nombre();
					zz=2;
					while(ch!=0x0a) ch=fgetc(config);
				}
			}
		}
	} while (ch != EOF );
}


/*  acerca del programa */
void About()
{
	cprintf("\n\rFD2TB v1.00 - Por Ricardo Quesada (4:900/309.3@fidonet.org)\n\r");

}

/* ayuda */
void Ayuda()
{
	cprintf("\n\rConversor de cualquier formato FrontDoor a TB-Point"
			"\n\rFD2TB SOURCE [DESTINATION] [/A]"
			"\r\n       Source = Nombre del archivo a convertir"
			"\r\n  Destination = Nombre del TB-POINT list (default=tb-edit.usr)"
			"\r\n           /a = Append to file."
			"\n\r\n\rEjemplo: FD2TB pointlst.222 c:\\tbpoint\\tb-edit.usr /a"
	);
}


/* main function */
int main(int argc,char *argv[] )
{
	int i;
	int appen = 0;
	char modow[10];
	char destino[128]="tb-edit.usr";

	About();

	for(i=0;i<argc;i++)
		toArriba(argv[i]);

	if( (argc == 4) && (strcmp(argv[3],"/A") == 0)  )
			appen = 1;

	if(argc >= 3)
	{
		if (strcmp(argv[2],"/A") == 0 )
			appen = 1;
		else strcpy(destino,argv[2]);
	}
	if(argc>=2) {
		   if(appen == 1) strcpy(modow,"at");
		   else strcpy(modow,"w+t");
		  if(
			 ( (tbpoint = fopen( destino,modow) ) != NULL ) &&
			 ( (config  = fopen( argv[1] ,"rt") ) != NULL )
		  )
		  Convertir();
		else {
			cprintf("\r\nERROR durante la apertura / creaci�n de archivos!");
			 }
	}
	else{
		Ayuda();
	}
	return 0;  /* el codigo nunca va a llegar aca */
}
