//
// VChar en C++.
//
// Autor: Ricardo Quesada
// Fecha de inicio: 29/11/94
//
// Versi�n de este modulo: 0.90
//

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TKeys
#define Uses_TRect
#define Uses_TStatusLine
#define Uses_TStatusItem
#define Uses_TStatusDef
#define Uses_TMenuBar
#define Uses_TSubMenu
#define Uses_TMenuItem
#define Uses_TWindow
#define Uses_TView
#define Uses_TDeskTop
//#define Uses_TDialog
//#define Uses_TSItem
//#define Uses_TCheckBoxes
//#define Uses_TRadioButtons
//#define Uses_TLabel
//#define Uses_TInputLine


//#include <stdlib.h>
//#include <string.h>
#include <tv.h>

const int cmMakeNewWindow = 100;
const int cmEditChar	  = 101;

class TWinDemoApp : public TApplication
{
public:

	TWinDemoApp();
	static TStatusLine *initStatusLine ( TRect r );
	static TMenuBar *initMenuBar( TRect r );
	virtual void handleEvent ( TEvent& event );
	void makeNewWindow();
	void editChar();

};

static short winNumber  = 0;
static short editNumber = 0;

class TMyWindow : public TWindow
{
	public:
		TMyWindow( const TRect& r, const char *aTitle, short aNumber );
};

class TMyWindow2 : public TWindow
{
	public:
		TMyWindow2( const TRect& r, const char *aTitle, short aNumber );
};


class TWinInterior : public TView
{
	public:
		TWinInterior( const TRect& bounds );
		virtual void draw();
};

class TWinInterior2 : public TView
{
	public:
		TWinInterior2( const TRect& bounds );
		virtual void draw();
};

TWinDemoApp::TWinDemoApp() :
	TProgInit(	&TWinDemoApp::initStatusLine,
				&TWinDemoApp::initMenuBar,
				&TWinDemoApp::initDeskTop
			 )
{
}

TMyWindow::TMyWindow( const TRect& bounds, const char *aTitle, short aNumber ):
	TWindow( bounds, aTitle, aNumber),
	TWindowInit( &TMyWindow::initFrame)
{
	TRect r = getClipRect();
	r.grow( -1, -1);
	insert( new TWinInterior( r ) );
}

TMyWindow2::TMyWindow2( const TRect& bounds, const char *aTitle, short aNumber ):
	TWindow( bounds, aTitle, aNumber),
	TWindowInit( &TMyWindow2::initFrame)
{
	TRect r = getClipRect();
	r.grow( -1, -1);
	insert( new TWinInterior2( r ) );
}

TWinInterior::TWinInterior( const TRect& bounds ) : TView( bounds )
{
	growMode = gfGrowHiX | gfGrowHiY;
	options = options | ofFramed;
}

TWinInterior2::TWinInterior2( const TRect& bounds ) : TView( bounds )
{
	growMode = gfGrowHiX | gfGrowHiY;
	options = options | ofFramed;
}

TStatusLine *TWinDemoApp::initStatusLine( TRect r )
{
	r.a.y = r.b.y - 1;
	return new TStatusLine( r,
		*new TStatusDef( 0, 0xffff ) +
			*new TStatusItem( "~Alt-X~ Exit",   kbAltX,  cmQuit  ) +
			*new TStatusItem( "~Alt-F3~ Close", kbAltF3, cmClose ) +
			*new TStatusItem( "~F-10~ Menu"   , kbF10,   cmMenu  )
	);
}

TMenuBar *TWinDemoApp::initMenuBar ( TRect r )
{
	r.b.y = r.a.y + 1;
	return new TMenuBar ( r,
		*new TSubMenu("~W~indow", kbAltW ) +
			*new TMenuItem("~A~scii", cmMakeNewWindow,	kbF5,
				hcNoContext, "F5" ) +
			*new TMenuItem("~E~dit Char", cmEditChar, kbF6,
				hcNoContext, "F6" ) +
			*new TMenuItem("~C~lose", cmClose,	kbAltF3,
				hcNoContext, "Alt-F3" ) +
			newLine() +
			*new TMenuItem( "E~x~it", cmQuit,	cmQuit,
				hcNoContext, "Alt-X")
		);
}

void TWinDemoApp::handleEvent( TEvent& event )
{
	TApplication::handleEvent( event );
		if( event.what== evCommand )
			{
			switch( event.message.command )
				{
				case cmMakeNewWindow:
					makeNewWindow();
					break;
				case cmEditChar:
					editChar();
					break;
				default:
					return;
				}
			clearEvent( event );
			}
}

void TWinDemoApp::makeNewWindow()
{
	TRect r(0,0,40,20);
	TMyWindow *window = new TMyWindow (r, "Ascii Table", ++winNumber );
	deskTop ->insert( window );
}

void TWinDemoApp::editChar()
{
	TRect r(40,0,80,16);
	TMyWindow2 *window = new TMyWindow2( r,"Editar Caracter", ++editNumber );
	deskTop ->insert( window );
}

void TWinInterior::draw()
{
	int i;
	char letters[ 60 ];
	for ( i = 0; i <= 60 ; i++ )
		letters[i] = 'A' + i;
	ushort color = getColor( 0x0301 );
	TView::draw();
	TDrawBuffer b;
	b.moveStr ( 0,letters,color );
	writeBuf( 4,2,29,2,b );
}

void TWinInterior2::draw()
{
	int i;
	char letters[14]="VChar TV v0.90";
	ushort color = getColor( 0x0301 );
	TView::draw();
	TDrawBuffer b;
	b.moveStr ( 0,letters,color );
	writeBuf( 12,2,14,1,b );
}

int main()
{
	TWinDemoApp demoApp;
	demoApp.run();
	return 0;
}
