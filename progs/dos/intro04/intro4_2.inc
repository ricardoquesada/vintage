;                                               ;
;       Modificadas por El Ni�o del Sol         ;
;                                               ;
;               para la Intro 4                 ;
;                                               ;

p286

;*********************************************************************;
;*    Author         : Michael Tischer                               *;
;*    Developed on   : 09/05/90                                      *;
;*    Last update    : 02/17/92                                      *;
;*********************************************************************;

;== Constants =========================================================

SC_INDEX       = 3c4h             ;Index register for sequencer ctrl.
SC_MAP_MASK    = 2                ;Number of map mask register
SC_MEM_MODE    = 4                ;Number of memory mode register

GC_INDEX       = 3ceh             ;Index register for graphics ctrl.
GC_GRAPH_MODE  = 5                ;Number of graphic mode register

VERT_RESCAN    = 3DAh             ;Input status register #1
PIXX           = 320              ;Horizontal resolution
;== Program ============================================================

additive   dw ?                   ;Local variable
restc      dw ?
movec      dw ?
data_seg   dw ?
frompage   dw ?                   ;From page
fromx      dw ?                   ;From X-coordinate
fromy      dw ?                   ;From Y-coordinate
topage     dw ?                   ;To page
tox        dw ?                   ;To X-coordinate
toy        dw ?                   ;To Y-coordinate
pwidth     dw ?                   ;Pixel width
pheight    dw ?                   ;Pixel height
bmskp      dw ?                   ;Pointer to buffer with bit mask


;-- BLOCKMOVE: Moves a group of pixels in video RAM -------------------
;-- Declaration :
;       In: Parametros pasados en variables de prog.
;      Out: None
;----------------------------------------------------------------------
blockmove proc near

       pusha
       push  ds
       push  es

       mov   data_seg,ds

	   mov   dx,GC_INDEX      ;Get current write mode and
	   mov   al,GC_GRAPH_MODE ;initialize write mode 1
	   out   dx,al
	   inc   dx
	   in    al,dx
	   push  ax               ;Push current mode onto stack
	   and   al,not 3
	   or    al,1
	   out   dx,al

	   mov   al,4             ;Move DS to start of FROM page
       mov   cl,byte ptr frompage
	   mul   cl
	   or    al,0A0h
	   xchg  ah,al
	   mov   ds,ax

	   mov   al,4             ;Move ES to start of TO page
       mov   cl,byte ptr cs:topage
	   mul   cl
	   or    al,0A0h
	   xchg  ah,al
	   mov   es,ax

	   mov   ax,PIXX / 4      ;Move SI to FROM starting position
       mul   cs:fromy
       mov   si,cs:fromx
	   shr   si,1
	   shr   si,1
	   add   si,ax

	   mov   ax,PIXX / 4      ;Move DI to TO position
       mul   cs:toy
       mov   di,cs:tox
	   shr   di,1
	   shr   di,1
	   add   di,ax

       mov   dh,byte ptr cs:pheight ;DH = Pixel lines
       mov   dl,byte ptr cs:pwidth  ;DL = Bytes
	   shr   dl,1
	   shr   dl,1

	   mov   bx,PIXX / 4      ;Move BX as offset to next line
	   sub   bl,dl
	   xor   ch,ch            ;High byte of counter is always 0
       cmp   cs:bmskp,0       ;No background?
	   jne   mt2              ;None, use other copy routine

	   push  dx               ;Push DX onto stack
	   mov   dx,SC_INDEX      ;Get bitplane access
	   mov   ah,0Fh
	   mov   al,SC_MAP_MASK
	   out   dx,ax
	   pop   dx               ;Pop DX

	   ;-- Copy routine for all four bitplanes, 
	   ;-- without checking the background

mt1:   mov   cl,dl            ;Number of bytes to CL

	   rep movsb              ;Copy lines
	   add   di,bx            ;DI and
	   add   si,bx            ;SI in next line
	   dec   dh               ;Still more lines?
	   jne   mt1              ;Yes --> Continue
	   jmp short mtend        ;No --> Get out of routine

	   ;-- Copy routine for individual bitplanes using
	   ;-- the specified bit mask arrays

mt2:
;       mov   byte ptr cs:restc,dh ;First specify variables
;       mov   byte ptr cs:movec,dl ;placed in local variables
;       mov   cs:additive,bx       ;on the stack
;
;       mov   al,SC_MAP_MASK   ;Address permanent
;       mov   dx,SC_INDEX      ;Map mask register
;       out   dx,al
;       inc   dx               ;Increment DX on data register
;
;       mov   bx,cs:bmskp      ;BX is pointer to bit mask array
;       push  ds
;       mov   ds,cs:data_seg
;       mov   al,[bx]          ;Load first byte      tiene que ir [bx]
;       xor   ah,ah            ;Start with an even byte
;       pop   ds
;
;mt3:   mov   cl,cs:byte ptr movec ;Move number of bytes to CL
;
;mt4:   out   dx,al            ;Set bit mask
;       movsb                  ;Copy 4 bytes
;
;       inc   ah               ;Increment odd/even counter
;       test  ah,1             ;Odd again?
;       jne   mt5              ;Yes --> Move nibble
;
;       ;-- Get next byte from buffer on every even byte -----------
;
;       inc   bx               ;BX to next bit mask byte
;       push  ds
;       mov   ds,cs:data_seg
;       mov   al,[bx]          ;Load next byte       tiene que ir [bx]
;       pop   ds
;       loop  mt4              ;Next four latches
;       jmp   short mt6
;
;mt5:   shr   al,1             ;Get odd byte bit mask from
;       shr   al,1             ;low nibble
;       shr   al,1
;       shr   al,1
;       loop  mt4              ;Next four latches
;
;mt6:   add   di,cs:additive      ;Add DI and
;       add   si,cs:additive      ;SI to next line
;       dec   byte ptr cs:restc   ;Still more lines?
;       jne   mt3                 ;Yes --> Continue
;
mtend: mov   dx,GC_INDEX      ;Revert to old
	   pop   ax               ;write mode
	   mov   ah,al
	   mov   al,GC_GRAPH_MODE
	   out   dx,ax

       pop   es
       pop   ds
       popa

	   ret
blockmove endp
