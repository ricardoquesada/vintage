;                                               ;
;                                               ;
;                   Intro 04                    ;
;           El Ni�o del Sol (c) 1994.           ;
;                                               ;
;                                               ;
P286

code    segment public byte 'code'
org     100h
intro04 PROC
assume cs:code,ds:code,ss:code,es:code

VERT_RESCAN    = 3DAh             ;Input status register #1

start:
    call is286
    call isvga
    call init320200
    call letras
    call scrolldown
    call animacion
    call finanimacion

    mov ax,3
    int 10h             ;volver al modo texto.
    mov ax,4c00h
    int 21h

;***********************;
; Is286                 ;
;***********************;
is286 proc near
    xor ax,ax
    push ax
    popf
    pushf
    pop ax
    and ax,0f000h
    cmp ax,0f000h
    je not286
    ret
not286:
    mov dx,offset no286msg
    mov ah,9
    int 21h
    mov ax,4cffh
    int 21h
is286 endp
;***********************;
;		isvga			;
;***********************;
isvga proc near
		mov ax,1a00h
		int 10h
        cmp al,1ah              ;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
        mov ax,4cffh
        int 21h
vgaonly:
        ret
isvga  endp



;               ;
;   animacion   ;
;               ;
animacion proc near
    mov  cl,1
    call showpage
    mov  cl,1
    call setpage

loopanima:
    call newmessages
    call bordesmove
    call lookcuadra
    call lookletras
    call ulaula
    call verticalr
    call getkey
    jne loopanima
    ret
animacion endp

;                   ;
;   finanimacion    ;
;                   ;
finanimacion proc near
    call byelnino
    call borrarpelotas

finanim1:
    call fincuadrados
    call bordesmove
    call verticalr
    cmp finlookactive,1
    jne finanim1
    call scrumble
    ret
finanimacion endp

;               ;
;   newmessages ;
;               ;
newmessages proc near
    inc  newmsgflag
    cmp  newmsgflag,30
    je   newmsghere1
    cmp  newmsgflag,60
    je   newmsghere2
    cmp  newmsgflag,90
    je   newmsghere3
    ret
newmsghere1:
    mov ax,cs                           ; futuro para poner los datos
    mov data_seg,ax
    mov frompage,3
    mov fromx,38
    mov fromy,60
    mov topage,1
    mov tox,38
    mov toy,180
    mov pwidth,248
    mov pheight,16
    mov bmskp,0
    call blockmove
    ret

newmsghere2:
    mov ax,cs                           ; futuro para poner los datos
    mov data_seg,ax
    mov frompage,3
    mov fromx,38
    mov fromy,80
    mov topage,1
    mov tox,38
    mov toy,180
    mov pwidth,248
    mov pheight,16
    mov bmskp,0
    call blockmove
    ret

newmsghere3:
    mov ax,cs                           ; futuro para poner los datos
    mov data_seg,ax
    mov frompage,3
    mov fromx,38
    mov fromy,100
    mov topage,1
    mov tox,38
    mov toy,180
    mov pwidth,248
    mov pheight,16
    mov bmskp,0
    call blockmove
    mov  newmsgflag,0
    ret
newmessages endp

newmsgflag  db 0

;               ;
;   bordesmove  ;
;               ;
bordesmove proc near
    inc brdflag
    cmp brdflag,1
    je exeborder
    ret
exeborder:
    pusha

    mov cx,0
submoverborde:
    push cx
    shl  cx,1
    mov  di,offset bordoff
    add  di,cx
    mov  si,[ di ]

    mov cx,[ si + 2 ]
    add [ si ],cx
    mov cx,[ si ]
    cmp cx,172
    ja  notborder1
    cmp cx,160
    jb  notborder1
    jmp nonotbrd1

notborder1:
    not word ptr [ si + 2 ]          ;incremento
    inc word ptr [ si + 2 ]
nonotbrd1:
    mov ax,cs                           ; futuro para poner los datos
    mov data_seg,ax
    mov frompage,3
    mov fromx,0
    mov fromy,4
    mov topage,1
    mov cx,[ si + 4 ]
    mov tox,cx                          ; x1
    mov cx,[ si ]
    mov toy,cx
    mov pwidth,8
    mov pheight,6
    mov bmskp,0
    call blockmove
    mov cx,[ si + 6 ]                   ; x2
    mov tox,cx
    call blockmove

    mov brdflag,0
    pop cx
    inc cx
    cmp cx,3
    jb submoverborde

    popa
    ret
bordesmove endp

;           ;
;   Letras  ;
;           ;
letras proc near

    mov cl,0
    call showpage

    mov cl,3
    call setpage
    mov ax,38                           ; x0
    mov bx,2                            ; y0
    mov cx,24                           ; Color
    mov si,offset message
    mov zerovalue,0
    call displayletras

    mov ax,38
    mov bx,180
    mov cx,24                           ; Color
    mov si,offset message2
    call displayletras

;letras para newletras!!
    mov ax,38                           ; x0
    mov bx,60                           ; y0
    mov cx,20                           ; Color Inicial
    mov si,offset crisismsg
    call displayletras

    mov ax,38
    mov bx,80
    mov cx,20                           ; Color Inicial
    mov si,offset crisismsg2
    call displayletras

    mov ax,38
    mov bx,100
    mov cx,20                           ; Color Inicial
    mov si,offset crisismsg3
    call displayletras

    mov ax,0
    mov bx,0
    mov cx,24                           ; Color Inicial
    mov si,offset bordemsg
    call displayletras

    call cuadrados                      ; escribir 2 cuadrados

    ret
letras endp

;               ;
;   Cuadrados   ;   ( llamada por letras )
;               ;
cuadrados proc near
    mov cl,2
    call setpage

    mov cx,4                            ; Color Rojo
    mov bx,0                            ; y
cualoopy1:
    mov ax,0                            ; x
cualoopx1:
    call setpix
    inc ax
    cmp ax,28
    jb cualoopx1
    inc bx
    cmp bx,28
    jb cualoopy1

    mov cx,15                           ; Color Blanco
    mov bx,32                           ; y
cualoopy2:
    mov ax,0                            ; x
cualoopx2:
    call setpix
    inc ax
    cmp ax,28
    jb cualoopx2
    inc bx
    cmp bx,64
    jb cualoopy2
    ret
cuadrados endp

;                   ;
;   fincuadrados    ;
;                   ;
fincuadrados proc near
    inc fincountcuadra
    cmp fincountcuadra,5
    je  finherecua1
    ret
finherecua1:

    pusha
    mov fincountcuadra,0           ; reset counter
    mov finlookactive,0            ; do not show letters

    cmp fincuadrx,320
    jb  finherecua2

    mov fincuadrx,0
    add fincuadry,32
    cmp fincuadry,64
    jb  finherecua2
    mov finlookactive,1            ; Finish now

    popa
    ret

finherecua2:
    mov dx,cuadry

    mov frompage,2
    mov fromx,0
    mov fromy,64                ; Negro , borra
    mov topage,1
    mov ax,fincuadrx
    mov tox,ax
    mov ax,fincuadry
    mov toy,ax
    mov pwidth,28
    mov pheight,28
    mov bmskp,0
    call blockmove

    mov dx,fincuadrx
    add dx,32
    mov fincuadrx,dx

    popa
    ret
fincuadrados endp

;               ;
;   lookcuadra  ;
;               ;
lookcuadra proc near
    inc countcuadra
    cmp countcuadra,5
    je  herecua1
endcuadr1:
    ret
herecua1:

    pusha
    mov countcuadra,0           ; reset counter
    mov lookactive,0            ; do not show letters

    cmp cuadrx,320
    jb  herecua2

    mov cuadrx,0
    add cuadry,32
    cmp cuadry,64
    jb  herecuax
    mov cuadry,24
    mov countcuadra,-110
    mov lookactive,1            ; show letters

    popa
    jmp endcuadr1

herecuax:
    xor bx,bx
    mov bl,rojoblanco           ; Fromy
    xor bl,1
    and bl,1
    mov rojoblanco,bl           ; Invierte el color
    shl bx,5                    ; multiplica por 8.

herecua2:
    xor bx,bx
    mov bl,rojoblanco           ; Fromy
    xor bl,1
    and bl,1
    mov rojoblanco,bl           ; Invierte el color
    shl bx,5                    ; multiplica por 8.

herecua3:
    mov dx,cuadry

    mov frompage,2
    mov fromx,0
    mov fromy,bx                ; 0 o 32 ( rojo o blanco )
    mov topage,1
    mov ax,cuadrx
    mov tox,ax
    mov ax,cuadry
    mov toy,ax
    mov pwidth,28
    mov pheight,28
    mov bmskp,0
    call blockmove

    mov dx,cuadrx
    add dx,32
    mov cuadrx,dx

    popa

    ret
lookcuadra endp

;                   ;
;   lookletras      ;
;                   ;
lookletras proc near

    cmp lookactive,1
    je  llnext1
    ret
llnext1:
    inc looklecounter
    cmp looklecounter,5
    je  llnext2
    ret
llnext2:
    pusha
    mov looklecounter,0

    mov si,offset doublemsg
    add si,llmsgcount
    mov ax,llax
    mov bx,llbx
    mov cl,[si]
    or  cl,cl
    je  nodisll2
    call displayletras2
    inc llmsgcount
    add llax,32
    cmp llax,310
    ja  llnext3
    popa
    ret

llnext3:
    mov llax,6
    add llbx,32
    cmp llbx,62
    ja  llnext4
    popa
    ret
llnext4:
    mov llbx,30
    mov lookactive,0
    popa
    ret

nodisll2:
    mov llax,6
    mov llbx,30
    mov llmsgcount,0
    popa
    ret
lookletras endp

;                   ;       In: ax = x    ; bx = y     ;  si = offset message
;   displayletras2  ;      Out: None
;                   ;
displayletras2 proc near
    pusha

    mov x0,ax
    mov y0,bx
    xor ax,ax
    mov al,cl                       ;en cl esta la letra.
    shl  ax,4                       ;multiplica por 16
    add  bp,ax                      ;puntero de la letra.
    mov di,16
loopdl22:
    mov cx,8
    mov dl,[bp]

loopdl21:
    push cx
    shl dl,1
    jc spix2

    mov cl,255
    jmp short cpix2
spix2:
    mov cl,0
cpix2:

    mov ax,x0               ; x
    mov bx,y0               ; y
    call setpix
    inc x0
    mov ax,x0               ; x
    mov bx,y0               ; y
    call setpix             ;real double
    inc x0

    pop cx
    loop loopdl21
    sub x0,16
    inc y0
    inc bp
    dec di
    or di,di
    jne loopdl22

    popa
    ret
displayletras2 endp

;                   ;       In: ax = x    ; bx = y     ;  si = offset message
;   displayletras   ;      Out: None
;                   ;
displayletras proc near

    mov x0,ax
    mov y0,bx
    mov Charcolor,cx

loopdl3:
    mov  bp,offset tabla

    xor  ah,ah
    mov  al,[si]
    or   al,al
    jne  nodl1

    ret

nodl1:
    mov  cl,4
    shl  ax,cl                      ;multiplica por 8
    add  bp,ax                      ;puntero de la letra.

    mov di,16
loopdl2:
    mov cx,8
    mov dl,[bp]

loopdl1:
    push cx
    shl dl,1
    jc spix

    mov cl,zerovalue
    jmp short cpix
spix:
    mov cx,x0
    add cx,y0
    and cx,6
    add cx,CharColor        ; algoritmo para poner el color
cpix:
    mov ax,x0               ; x
    mov bx,y0               ; y
    call setpix
    inc x0
    pop cx
    loop loopdl1
    sub x0,8
    inc y0
    inc bp
    dec di
    or di,di
    jne loopdl2
    inc si
    sub y0,16
    add x0,8
    jmp loopdl3

    ret
displayletras endp


;               ;
;   ScrollDown  ;
;               ;
scrolldown proc near

    mov dx,2
heresd1:
    mov cl,0
    call showpage

    mov ax,cs                           ; futuro para poner los datos
    mov data_seg,ax
    mov frompage,3
    mov fromx,38
    mov fromy,0
    mov topage,1
    mov tox,38
    mov toy,dx
    mov pwidth,248
    mov pheight,16
    mov bmskp,0
    call blockmove

    inc dx
    mov cl,1
    call showpage
    mov ax,cs
    mov data_seg,ax
    mov frompage,3
    mov fromx,38
    mov fromy,0
    mov topage,0
    mov tox,38
    mov toy,dx
    mov pwidth,248
    mov pheight,16
    mov bmskp,0
    call blockmove
    inc dx

    cmp dx,160
    ja heresd2
    jmp heresd1

heresd2:
    ret
scrolldown endp
;               ;
;   byelnino    ;
;               ;
byelnino proc near
    mov cl,1
    call showpage
    mov ax,cs
    mov data_seg,ax
    mov frompage,3
    mov fromx,38
    mov fromy,180
    mov topage,1
    mov tox,38
    mov toy,180
    mov pwidth,248
    mov pheight,16
    mov bmskp,0
    call blockmove
    ret
byelnino endp

;               ;
;   UlaUla      ;
;               ;
ulaula proc near

    mov cx,0
submoverpelotas:
    push cx
    shl  cx,1
    mov  di,offset pelota1
    add  di,cx
    mov  si,[ di ]

    cmp  byte ptr [si + 5 ],1 ; estaba activo.
    jne  pelotanotactive

    mov  ax,[ si ]          ; como estaba activo borra el viejo pixel.
    mov  bx,[si+2]
    mov  cx,0
    call setpix

pelotanotactive:
    call movex
    call movey
    mov  ax,[ si ]          ; posx
    mov  bx,[ si + 2 ]      ; posy
    call getpix             ; pixel a escribir vacio ?
    cmp  pcolor,0
    jne  ulnopix1
    mov  cl,byte ptr [ si + 4 ] ; tomar color de la pelota
    call setpix
    mov  byte ptr [ si + 5 ],1  ; Pelota activa
    jmp short endulaula1
ulnopix1:
    mov  byte ptr [ si + 5 ],0  ; Pelota no activa.
endulaula1:
    pop  cx
    inc  cx
    cmp  cx,6
    jb   submoverpelotas
    ret
ulaula endp

;           ;
;   getkey  ;
;           ;
getkey proc near
    in  al,060h
    cmp al,01h
    ret
getkey endp

;                   ;
;   borrarpelotas   ;
;                   ;
borrarpelotas proc near
    mov cx,0
subborrarpelotas:
    push cx
    shl  cx,1
    mov  di,offset pelota1
    add  di,cx
    mov  si,[ di ]
    mov  ax,[ si ]
    mov  bx,[ si + 2 ]
    xor  cx,cx
    call setpix
    pop  cx
    inc  cx
    cmp  cx,6
    jb   subborrarpelotas
    ret
borrarpelotas endp

;   movex   ; funcion interna del ulaula
movex proc near
    push ax
    mov  ax,[ si ]              ; pos x
    add  ax,[ si + 6 ]          ; incremento x
    cmp  ax,315
    ja   notmx1
    cmp  ax,0
    jb   notmx1
    jmp  short nonotmx1
notmx1:
    not  word ptr [ si + 6 ]    ; incremento x
    inc  word ptr [ si + 6 ]    ; incremento x
nonotmx1:
    mov  [ si ],ax              ; pos x
    pop  ax
    ret
movex endp

;   movey   ; funcion interna del ulaula
movey proc near
    push ax
    push cx
    inc  word ptr [ si + 8 ]    ; incremey
    mov  ax,[ si + 8 ]          ; incremey
    cmp  ax,[ si + 10 ]         ; maximo de y
    je   notmy1
    cmp  ax,0
    je   notmy1
    jmp  short nonotmy1

notmy1:
    not  word ptr [ si + 8 ]    ; incremey
    inc  word ptr [ si + 8 ]    ; incremey
nonotmy1:

    mov  cx,[ si + 8 ]          ; incremey
    cmp  ch,0
    jne  heremy2

    mov  cl, [si + 12 ]
    shr  ax,cl

    jmp  short endmy1
heremy2:
    dec  ax
    not  ax
    inc  ax
    mov  cl, [ si + 12 ]
    shr  ax,cl
    not  ax
    inc  ax
endmy1:
    add  [ si + 2 ],ax          ; pos y
    pop  cx
    pop  ax
    ret
movey endp

;               ;
;   VerticalR   ;
;               ;
verticalr proc near
    push ax
    push dx

    mov   dx,VERT_RESCAN   ;Wait for end of
ula1:
    in    al,dx            ;vertical rescan
	test  al,8
	jne   ula1
ula2:
    in    al,dx            ;Go to start of rescan
    test  al,8
    je    ula2

    pop dx
    pop ax
    ret
verticalr endp

;               ;
;   Scrumble    ;
;               ;
scrumble proc near
    mov  cl,1
    call showpage
    mov cl,1
    call setpage

    mov counter1,0

loopscrum1:
    mov bx,161              ; y0
    xor ax,ax
nextpixel:
    inc ax
    cmp  ax,320
    ja   nextline
    call getpix

    cmp  byte ptr pcolor,0
    jne  nextpixel

    dec  bx
    call getpix
    cmp  byte ptr pcolor,0
    je   nextprev                       ; basado en una linea

loopsd2:
    inc counter1
    dec bx
    call getpix
    cmp byte ptr pcolor,0
    jne loopsd2                         ; contador de las lineas a scrollear.

    mov counter2,0
    add bx,counter1                     ; original BX

heresd3:
    call getpix
    mov  cx,pcolor
    inc  bx
    call setpix

    sub bx,2                            ; 2 para atras.
    inc counter2

    mov dx,counter1
    cmp dx,counter2
    jne heresd3

    inc bx
    mov cx,0
    call setpix
    add bx,counter1
    mov counter1,0
    jmp nextpixel

nextprev:
    inc  bx
    jmp  nextpixel

nextline:
    xor  ax,ax
    inc  bx
    cmp  bx,200
    jb   nextpixel

    ret
scrumble endp

include     intro4_1.inc
include     intro4_2.inc

;usadas por scrumble
counter1    dw ?
counter2    dw ?

;usadas por ulaula


pelota1     dw offset datpelo1
pelota2     dw offset datpelo2
pelota3     dw offset datpelo3
pelota4     dw offset datpelo4
pelota5     dw offset datpelo5
pelota6     dw offset datpelo6

datpelo1    equ this word
inicx1      dw 185                  ; + 0    ( coordenas iniciales )
inicy1      dw 199                  ; + 2
color1      db 15                   ; + 4    ( color )
activ1      db 1                    ; + 5    ( valor anterior aparecia ? 1=si )
incremex1   dw 1                    ; + 6    ( 1=derecha -1=izquierda )
incremey1   dw -25                  ; + 8    ( valores negativos suben )
maxy1       dw 25                   ; + 10   ( maximo que puede subir )
shrs1       db 3                    ; + 12   ( > numero > ancho de onda )

datpelo2    equ this word
inicx2      dw 160                  ; + 0
inicy2      dw 199                  ; + 2
color2      db 14                   ; + 4
activ2      db 1                    ; + 5
incremex2   dw -2                   ; + 6
incremey2   dw -25                  ; + 8
maxy2       dw 25                   ; + 10
shrs2       db 3                    ; + 12

datpelo3    equ this word
inicx3      dw 90                   ; + 0
inicy3      dw 199                  ; + 2
color3      db 13                   ; + 4
activ3      db 1                    ; + 5
incremex3   dw 2                    ; + 6
incremey3   dw -25                  ; + 8
maxy3       dw 25                   ; + 10
shrs3       db 3                    ; + 12

datpelo4    equ this word
inicx4      dw 220                  ; + 0
inicy4      dw 199                  ; + 2
color4      db 12                   ; + 4
activ4      db 1                    ; + 5
incremex4   dw -1                   ; + 6
incremey4   dw -20                  ; + 8
maxy4       dw 20                   ; + 10
shrs4       db 2                    ; + 12

datpelo5    equ this word
inicx5      dw 20                   ; + 0
inicy5      dw 199                  ; + 2
color5      db 11                   ; + 4
activ5      db 1                    ; + 5
incremex5   dw 1                    ; + 6
incremey5   dw -25                  ; + 8
maxy5       dw 25                   ; + 10
shrs5       db 2                    ; + 12

datpelo6    equ this word
            dw 300                  ; + 0
            dw 199                  ; + 2
            db 10                   ; + 4
            db 1                    ; + 5
            dw -3                   ; + 6
            dw -21                  ; + 8
            dw 21                   ; + 10
            db 1                    ; + 12


;   datos de mover borde    ;

bordoff dw offset bordesy1
        dw offset bordesy2
        dw offset bordesy3

bordesy1 dw 160
         dw 1       ;incremento
         dw 46      ;x1
         dw 264     ;x2

bordesy2 dw 166
incbrd2  dw 1
         dw 54      ;x1
         dw 256     ;x2

bordesy3 dw 172
incbrd3  dw 1
         dw 62      ;x1
         dw 248     ;x2

brdflag  db 0


CharColor   dw 0    ; Usada para el Color de los textos (fn interna del Displayletras)
message     db '     Intro N�mero 4 v1.00     ',0
message2    db '      by El Ni�o del Sol      ',0
;
;               1234567890123456789012345678901234567890

; formato:  Texto,0
crisismsg   db '          CRISIS BBS          ',0
crisismsg2  db ' Carlos Jaacks,Victor Idoyaga ',0
crisismsg3  db '       TLD 24 hs  963-6579    ',0
bordemsg    db '-',0

;   usadas por lookletras   ;
looklecounter   dw 0
doublemsg   db'CRISIS BBS','Tld 24 hs.'
            db'  Sysop:  ',' C.Jaacks '
            db' Cosysop: ','V. Idoyaga'
            db' Telefono ',' 963-6579 '
            db'Intro n� 4',' 8 Dic 94 '
            db'El    ni�o','del    Sol'
            db' Saludos: ','Nacho,Toro'
            db' Alessia  ','  Viole   '
            db'Javi , Emy','Santi ,Edy'
            db'� Music ? ','Ja, ja, ja'
            db' No tengo ',' la menor '
            db' idea de  ',' como se  '
            db'hace.. :-(',' Si sabes '
            db'   como   ',' hacerla  '
            db' y queres ',' ayudarme '
            db'bienvenido','- - - - - '
            db'Un saludo ','muy grande'
            db' para  la ','gente  que'
            db'hace demos','  intros  '
            db'Sera hasta','la proxima'
            db'          ','      ciao',0

llmsgcount  dw 0
llax        dw 6
llbx        dw 30

vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'

zerovalue   db 0

;   datos de borrar cuadrado    ;
finlookactive  db 0
fincountcuadra db 0
fincuadrx      dw 0                ; pos x de los cuadrados.
fincuadry      dw 24

;   datos de cuadrado   ;
lookactive  db 0
countcuadra db 0
rojoblanco  db 0                ; 0=rojo    1=blanco.
cuadrx      dw 0                ; pos x de los cuadrados.
cuadry      dw 24



include     font0000.inc

intro04 endp
code    ends
        end intro04
