Comment|
File:    aobt.asm
Version: 1.0
Author:  RICARDO QUESADA
Date:    04/2/94
Purpose: Bootea a determinada hora...
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RRTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE



PRINCIPIO:

        JMP INTRO
        DB '(C)RQ94'

;***************************************************
;                INT 1CH
;***************************************************

NEW1CH: JMP SHORT ALA
        DB 'SE'
ALA:    CLI
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH SI
        PUSH DI

        MOV AH,2
        INT 1AH

        CMP CX,CS:HORA
        JNE FINAL

        MOV AX,0B800H
        MOV DS,AX
        MOV BYTE PTR DS:0,'X'

;        MOV CS:RUTSEG,0FFFFH
;        MOV CS:RUTOFF,0
;        JMP CS:[RUTINA]         ;SALTA A CS:100 DEL PRG ORIGINAL

         xor ax,ax
         mov ds,ax
         mov ds:0472h,1234h
         int 19h

FINAL:  STI
        POP DI
        POP SI
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD1CH]
        IRET


;**********************************************
;**************** NEW 2FH *********************
;**********************************************

NEW2FH:
        JMP SHORT ACA_ACA
        DB'SE'
ACA_ACA:
        CMP AH,0DEH
        JE ES_ESTO
FIN_:
        JMP CS:[OLD2FH]

ES_ESTO:
        CMP AL,40H
        JNE ES_ESTO2
        XOR AL,AL
        IRET

ES_ESTO2:
        CMP AL,41H
        JNE FIN_
        MOV AX,3031H
        MOV BX,3030H
        IRET

;**************************************************************
;**************************************************************
;---------------------------------DATA
OLD1CH  DD 0
OLD2FH  DD 0
ADDR    DW 0
HORA    DW 1441H            ;Cambia la hora... Simple  verdad

RUTINA  EQU THIS DWORD      ;SE LLAMA ACA PARA QUE SALTE
RUTOFF  DW 0                ;AL PRG ORIGINAL
RUTSEG  DW 0FFFFH


;      ******************************************
;           EMPIEZA LA UN/INS-TALACION
;      ******************************************

INTRO:  CLD
        MOV SI,81H
ALOPA:  LODSB
        CMP AL,13
        JNE LULU
        JMP SISIE
LULU:   CMP AL,'/'
        JNE ALOPA

DETEC:  LODSB
        CMP AL,13
        JNE LULA
        JMP SISIE
LULA:   AND AL,11011111B
        CMP AL,'H'              ;HELP
        JE HELP
        CMP AL,'V'              ;VERSION
        JE VERSION_

ERROR_MSG:
        PUSH CS                 ;OPTION ERROR
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;DESPUES DEL ERROR DISPLAY HELP SCREEN

HELP:   PUSH CS                 ;HELP SCREEN
        POP DS
        MOV DX,OFFSET MSGHLP
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H


VERSION_:
        MOV AX,0DE41H           ;AVERIGUA NM VERSION
        INT 2FH
        CMP AH,0DEH
        JE ERRVER_

        PUSH CS
        POP DS
        MOV [NUMBER],AH
        MOV [NUMBER+1],AL
        MOV [NUMBER+3],BH
        MOV [NUMBER+4],BL
        MOV DX,OFFSET VERNUM
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

ERRVER_:
        PUSH CS
        POP DS
        MOV AH,9
        MOV DX,OFFSET ERRVER
        INT 21H
        MOV AH,4CH
        INT 21H

SISIE:
        MOV ADDR,0B800H         ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H

NOMONO:
        MOV AX,0DE40H
        INT 2FH
        OR AL,AL                ;AL=0 INST, AL=30H UNINST
        JE UN_INST                ;NO ES INSTALADO
        JMP INST

;**************************************
;---------------------------------------UNINSTALL
UN_INST:
        MOV AX,351CH
        INT 21H
        CMP ES:[BX+2],'ES'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'ES'
        JE UNLOAD

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNL
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

UNLOAD:
        MOV SI,OFFSET OLD1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        PUSH CS
        POP DS
        MOV AH,9                ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H
        MOV AH,4CH
        INT 21H                 ;FIN DE TSR

;---------------------------------------INSTALL

INST:
        MOV AX,351CH            ;OLD1CH
        INT 21H
        MOV SI,OFFSET OLD1CH
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS                 ;NEW 1CH
        POP DS
        MOV DX,OFFSET NEW1CH
        MOV AX,251CH
        INT 21H

        MOV AX,352FH            ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH    ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV DX,OFFSET INSTAL    ;MENSAJE DE INTALADO
        MOV AH,09H
        INT 21H

        MOV DX,OFFSET INTRO
        INT 27H                 ;TSR

INSTAL  DB 13,10,'AutoBoot v1.0 - (C)Ricardo Quesada.',13,10
        DB'Llame a AoBt de nuevo para desinstalarlo.',13,10
        DB'y AoBt /h para ver el menu de ayuda.',13,10,'$'

UNINST  DB'AoBt desinstalado.',13,10,'$'
VERNUM  DB'Numero de version: '
NUMBER  DB'00.00',13,10,'$'
ERRVER  DB'****ERROR:SeSt no esta instalado.',13,10,'$'
ERRUNL  DB'****ERROR:Imposible desinstalarlo.',13,10,'$'
ERROPT  DB'****ERROR:Opcion invalida.',13,10,'$'


MSGHLP  DB'**** AoBt v1.0 - Menu de ayuda ****',13,10,13,10
        DB'AoBt [opciones]',13,10
        DB'     /h   Muestra esta pantalla.',13,10
        DB'     /v   Muestra la version de SeSt instalado.',13,10,13,10
        DB'AoBt bootea la maquina a determinada hora.',13,10,13,10,'$'


        RRTI    ENDP
CODE    ENDS
        END RRTI
