COMMENT|
NOMBRE:   DAY
VERSION:  2.0
AUTOR:    Ricardo QUESADA
FECHA:    8-3-93
|

CODE    SEGMENT PUBLIC BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE
        JMP INTRO

        DB '(C)RQ93'
;***************************************************
;                INT 1CH
;***************************************************

NEW1CH: JMP SHORT ALA
        DB 'DY'
ALA:    CLI
        PUSH AX
        PUSH BX
        PUSH CX
        PUSH DX
        PUSH DS
        PUSH SI
        PUSH DI

        XOR AH,AH
        MOV AL,CS:DSP
        MOV DI,AX
        MOV AL,CS:FLAG1
        CMP AL,0
        JNE CONTA
        JMP FINAL

CONTA:  MOV AX,CS:ADDR
        MOV DS,AX
        CMP DS:[DI+4],702FH
        JE DISP
        CMP DS:[DI],702AH
        JE DISP

STOBUF: XOR BX,BX
CONA:   MOV SI,OFFSET BUFF
        MOV AX,DS:[DI+BX]
        MOV CS:[SI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE CONA

DISP:   MOV AH,4
        INT 1AH
        JNC LULU     ;NO BATTERY
        XOR BX,BX
        MOV SI,OFFSET NOBAT
ALOPA:  MOV AX,CS:[SI+BX]
        MOV DS:[DI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE ALOPA
        JMP SHORT FINAL
LULU:   MOV BL,CL  ;TRABAJAR CON LOS A�OS
        AND BL,00001111B
        ADD BL,30H
        SHR CL,4
        AND CL,00001111B
        ADD CL,30H
        MOV DS:[DI+12],CL
        MOV DS:[DI+14],BL
        MOV BL,DL  ;TRABAJAR CON LOS DIAS
        AND BL,00001111B
        ADD BL,30H
        SHR DL,4
        AND DL,00001111B
        ADD DL,30H
        MOV DS:[DI],DL
        MOV DS:[DI+2],BL
        MOV BL,DH     ;TRABAJAR CON LOS MESES
        AND BL,00001111B
        ADD BL,30H
        SHR DH,4
        AND DH,00001111B
        ADD DH,30H
        MOV DS:[DI+6],DH
        MOV DS:[DI+8],BL
        MOV AX,702FH
        MOV DS:[DI+4],AX
        MOV DS:[DI+10],AX
        MOV DS:[DI+1],AH
        MOV DS:[DI+3],AH
        MOV DS:[DI+7],AH
        MOV DS:[DI+9],AH
        MOV DS:[DI+13],AH
        MOV DS:[DI+15],AH

FINAL:  STI
        POP DI
        POP SI
        POP DS
        POP DX
        POP CX
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD1CH]
        IRET

;*********************************************************
;               INT 09H
;**********************************************************

NEW09H: PUSHF
        PUSH AX
        PUSH DS
        CLI
        IN AL,60H
        TEST AL,80H
        JNE SEGU
        CMP AL,8      ;TECLA 7 PULSADA
        JNE SEGU
        MOV AX,0040H    ;ALT PULSADA
        MOV DS,AX
        MOV AX,DS:[17H]
        AND AX,8
        CMP AX,8
        JNE SEGU
;---------------------
        MOV AL,255
        XOR CS:FLAG1,AL
        CMP CS:FLAG1,AL
        JE SEGU
;-------------------- DISPLAY BUFFER
        PUSH BX
        PUSH SI
        PUSH DI
        XOR AH,AH
        MOV AL,CS:DSP
        MOV DI,AX
        MOV AX,CS:ADDR
        MOV DS,AX
        XOR BX,BX
        MOV SI,OFFSET BUFF
LOOP1:  MOV AX,CS:[SI+BX]
        MOV DS:[DI+BX],AX
        ADD BX,2
        CMP BX,16
        JNE LOOP1
        POP DI
        POP SI
        POP BX
;------------------
SEGU:   MOV AL,20H
        OUT 20H,AL
        POP DS
        POP AX
        POPF
        PUSHF
        CALL CS:[OLD09H]
        IRET

;**********************************VECTORES AND ETC...**************

OLD1CH  DD 0
OLD09H  DD 0
ADDR    DW 0
FLAG1   DB 255
HOR1    DB 00
HOR2    DB 00
MIN1    DB 00
MIN2    DB 00
SEC1    DB 00
SEC2    DB 00
BUFF    DB  16 DUP(0)
DSP     DB 110
NOBAT   DB '*',70H,'N',112,'O',112,' ',112,'B',112,'A',112,'T',112,'T',112
;*********************************************************************
;               INSTALACION, DES-INSTALACION, ETC...
;*********************************************************************
INTRO:  CLD
        MOV SI,81H
LOD:    LODSB
        CMP AL,13
        JNE ACA1
        JMP INTR
ACA1:   CMP AL,'+'
        JE AP
        CMP AL,'-'
        JE AM
        AND AL,11011111B
        CMP AL,'H'
        JE HELP
        JMP SHORT LOD
HELP:   PUSH CS
        POP DS
        MOV DX,OFFSET MSGHEL
        MOV AH,9
        INT 21H
        MOV AX,4C00H
        INT 21H
; * * * * * * *     CUANDO SE ENTRA DISPLACEMENT
AP:     LODSB               ;DISPLACEMENT
        CMP AL,13           ;CR?
        JE SALTO            ;YES, SO GO INTR
        MOV AH,AL
        LODSB
        CMP AL,13           ;CR?
        JE LUALU            ;ONLY ONE DIGIT
        CMP AL,'/'
        JE LUALU            ;ONLY ONE DIGIT
        CMP AL,20H          ;COMPARA CON SPACE
        JE LUALU            ;ONLY ONE DIGIT
        SUB AL,30H
        AND AH,7            ;2 DIGITO NO MAYOR DE 7
        MOV BL,AL
        MOV AL,10
        MUL AH
        ADD AL,BL
MULT:   MOV AH,2            ;TOTAL * 2 POR LOS ATRIBUTOS
        MUL AH
        MOV CS:DSP,AL
        JMP SHORT LOD       ;SEGUIR POR SI HAY MAS ESPECIFICACIONES
LUALU:  SUB AH,30H
        MOV AL,AH
        JMP SHORT MULT      ;UN DIGITO SOLO Y SIGUE
LUBLU:  SUB AH,30H          ;UN DIGITO Y ACABA
        MOV AL,AH
        MOV AH,2
        MUL AH
        MOV CS:DSP,AL
SALTO:  JMP SHORT INTR
; * * * * * * * *     CUANDO SE ENTRA FECHA
AM:     XOR BX,BX
        MOV DI,OFFSET DIAX
LOD2:   LODSB
        CMP AL,13
        JE INTR
        CMP AL,'/'
        JE SETD
        SUB AL,30H
        MOV CS:[DI+BX],AL
        INC BX
        CMP BX,8
        JB LOD2
SETD:   MOV DH,CS:DIAX
        MOV DL,CS:DIAX+1
        SHL DH,4
        OR DL,DH      ;DIA
        MOV BH,CS:MESX
        MOV DH,CS:MESX+1
        SHL BH,4
        OR DH,BH     ;MES
        MOV CH,CS:ANOX
        MOV CL,CS:ANOX+1
        SHL CH,4
        OR CL,CH     ;A�O
        MOV CH,00011001B ;CENTURY 19 BDC

        MOV AH,5         ;LLAMA PARA SET FECHA
        INT 1AH
        JMP LOD

;+ + + + + + + + + + + + + + + + UN IN S TALACION
INTR:   MOV CS:ADDR,0B800H
        MOV AX,0040H
        MOV ES,AX
        MOV AL,30H
        AND AL,ES:[10H]
        CMP AL,30H
        JNE SIG
        MOV CS:ADDR,0B000H
SIG:    MOV AX,351CH
        INT 21H
        CMP ES:[BX+2],'YD'
        JNE INSTAL

;DES INSTALACION--------------------------------
        MOV SI,OFFSET OLD09H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2509H
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H
        MOV SI,OFFSET OLD1CH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,251CH
        INT 21H
        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H
        PUSH CS
        POP DS
        MOV DX,OFFSET MSGDES
        MOV AH,9
        INT 21H
        MOV AX,4C00H
        INT 21H


;INSTALACION-----------------------------------
INSTAL: MOV SI,OFFSET OLD1CH
        MOV [SI],BX
        MOV [SI+2],ES
        PUSH CS
        POP DS
        MOV DX,OFFSET NEW1CH
        MOV AX,251CH
        INT 21H
        MOV AX,3509H
        INT 21H
        MOV SI,OFFSET OLD09H
        MOV [SI],BX
        MOV [SI+2],ES
        MOV DX,OFFSET NEW09H
        MOV AX,2509H
        INT 21H
        MOV AH,9
        MOV DX,OFFSET MSGINS
        INT 21H
        MOV DX,OFFSET INTRO
        INT 27H
DIAX    DB 0,1,0
MESX    DB 0,1,0
ANOX    DB 8,0
MSGINS  DB 13,10,'DAY V2.0 - (C)Ricardo QUESADA',13,10
        DB 'DAY /H display the HELP SCREEN.',13,10,'$'
MSGDES  DB 'DAY uninstalled',13,10,'$'
MSGHEL  DB '**** DAY V2.0 - HELP SCREEN ****',13,10,13,10
        DB 'DAY [OPTION/][OPTION/]',13,10
        DB '    +XX/  where XX, a number between 0 and 79,',13,10
        DB '          is the offset of the display. Default:55.',13,10
        DB '    -dd.mm.yy/  Day,Month,Year.',13,10,13,10
        DB 'IMPORTANT: Every option must end with and inverted bar ("/").',13,10
        DB ' e.g.: day +15/ -20.03.93/',13,10
        DB 'Alt + 7 toggle the display ON and OFF.',13,10
        DB 'If DAY is installed, call again to uninstall it, and viceversa.',13,10,13,10
        DB 'DAY should not be sold. For personal use only.',13,10
        DB '(C) Copyright 1993 by Ricardo QUESADA.',13,10,'$'
RUTI    ENDP
CODE    ENDS
        END RUTI
