/****************************************************/
/*                                                  */
/*                                                  */
/*                  llamar.exe                      */
/*                                                  */
/*           conec2.c    y    serial2.asm           */
/*                                                  */
/*   llama a los numero que estan en config.ure     */
/*                                                  */
/****************************************************/


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <io.h>
#include <dos.h>


/* functions prototypes */
int DialNum( char*, int );
int Comuniq( int );
int MandarC( char*, int );


/* variables globales */
int handle;
int com;

/*************************************************************/
/************** int abrir ( void ) ***************************/
/*************************************************************/
int abrir(void)
{
   if ( (handle = _open("llamar.cfg",O_RDWR)) == -1) {
       printf("\nArchivo LLAMAR.CFG no encontrado\n");
       return -1;
   }
   return 0;
}

/*************************************************************/
/*************** int leer ( void ) ***************************/
/*************************************************************/
int leer(void)
{
   /* variables */
   unsigned char buf[80];
   char paso;
   int i=0;
   int err;
   int num=1;


   do {
       err = _read(handle,buf+i,1);

       if (buf[i] == ';') {
	   do {
	      err = _read(handle,&paso,1);
	   } while (paso != 10);
	   i--;
       }

       if (buf[i] == 10) {
	    buf[i] = 0;
	    printf("\n\nCOM = %d\n",com);
	    printf("Dato %i:\n%s\n",num,buf);
	    i = -1;
	    num++;


	    if ( DialNum( buf, com ) == 1 ) {
		printf("\nUn error se produjo al intentar la llamada.\nVerifique si su modem esta conectado.\n");
		exit(1);
	    }

	    i = atoi( &buf[10] );


	    for ( ;i--;i>0 ) {
		gotoxy(1,15);
		printf("Esperando comunicacion... %d  \n",i);

		if ( Comuniq( com ) == 0 ) {
			printf("\Se ha logrado la comunicacion.");
			printf("\nAhora no se que hacer.\n");
			exit(0);
		}
		sleep(1);
	    }

       sleep(1);
       clrscr();
       printf("\nIntentando con otro telefono...\n");
       }

       if (err == -1) break;
       if (err == 0) {
	    buf[i] = 0;
	    break;
       }
       i++;

   } while ( i <= 80);

   return 0;
}



/*************************************************************/
/**************        main()      ***************************/
/*************************************************************/
void main(int argc, char* argv[])
{

   clrscr();
   printf("Llamar v0.1a\n"
	  "Hecho por Ricardo Quesada.\n"
	  "con la participacion especial de Juan Janczuk.\n"
	  "Ojo con los bugs!\n\n");


   if (argc == 1) com = 1;          /* por defecto se asume com 1 */
   else {
	if ( (com = atoi( argv[1] )) == 0 ) {
	    printf("Error en linea de comando.\n");
	    com = 1;
	}

   }

   if ( abrir()== -1) exit(1);
   leer();
   printf("\n\nOK.");
   return;
}
