/****************************************************/
/*                                                  */
/*                                                  */
/*                  telefono                        */
/*                                                  */
/*                                                  */
/*               v1.2 Erasoft                       */
/*                                                  */
/*                                                  */
/****************************************************/

#define FALSE 0
#define TRUE not FALSE
#define LLAMAX 15

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <io.h>
#include <dos.h>
#include <asynch_1.h>
#include <asynch_h.h>
#include <process.h>
#include "texto.h"
#include "menu2.h"

extern int com;

/* variables globales */
int handle;

static char buf[400];
extern MODEM **modem;


/*************************************************************/
/************* void error ( int ) ****************************/
/*************************************************************/
/*              reporta el error que se le mande             */
/*************************************************************/
void error( int ercode )
{

    char *ermsg[] =
    {
	   "\nTerminacion normal. De casualidad!...\nPero seguro que aparece \" null pointer assigment \" \n",    /* 0 */
	   "\nError en la lectura de archivo.\n",        /* 1 */
	   "\nError al inicializar el modem.\n",         /* 2 */
	   "\nError desconocido.\n",                     /* 3 */
	   "\nArchivo TELEFONO.TEL no encontrado.\n",      /* 4 */
	   "\nError en comunicacion.\n",                 /* 5 */
	   "\nError en linea de comando.\nFormato: telefono [/COM] .\nDonde COM es el numero de com [de 1 a 8].\nPor defecto se asume el COM = 1.\n",
	   "\nError al cerrar el com.\n",                /* 7 */
	   "\nError al intentar el llamado.\n",          /* 8 */
	   "\n...\n"
    };

    clrscr();
    printf( "%s", ermsg[ercode] );
    exit(ercode);
}

/*************************************************************/
/************* void mreport ( int ) **************************/
/*************************************************************/
/*          reporta la respuesta del modem                   */
/*************************************************************/
int mreport( int code )
{

    char *codemsg[] =
    {
	"OK",                           /* 0 */
	"CONNECT",                      /* 1 */
	"RING",                         /* 2 */
	"NO CARRIER",                   /* 3 */
	"ERROR",                        /* 4 */
	"CONNECT 1200",                 /* 5 */
	"NO DIALTONE",                  /* 6 */
	"BUSY",                         /* 7 */
	"NO ANSWER",                    /* 8 */
	"RESERVED",                      /* 9 */
	"CONNECT 2400"                  /* 10 */
    };

    if (code > 10 || code < 0 )
	centrar("INVALID RESPONSE.",25);

    else centrar( codemsg[code],25 );

    getch();

    return code;
}

/*************************************************************/
/************* void baudios ( int ) **************************/
/*************************************************************/
/*          reporta los baudios del modem                    */
/*************************************************************/
int baudios( int code )
{

    int baudd[] =
    {
    110,150,300,600,1200,2400,4800,9600,14400,19200
    };

    return baudd[code];
}

/*************************************************************/
/************** int  cerrar( void ) **************************/
/*************************************************************/
int  cerrar( void )
{
    clrscr();
    close_a1( com );
    return(0);
}

/*************************************************************/
/************** int  mandar( void ) **************************/
/*************************************************************/
int mandar( void )
{
   /* variables locales */
   int key;
   char ch;

   /* function 1 returns 0 until a key is pressed */
   if (bioskey(1) != 0){

	key = bioskey(0);

	(ch) = key;

	wrtch_a1( com, ch );
   }
   return( 0 );

}

/*************************************************************/
/************** int recibir( void ) **************************/
/*************************************************************/
int recibir( void )
{
    /* variables locales */
    int queue;
    unsigned status;
    char buf2;

    do {
	qsize_a1( com, &queue, &status);

	if (queue != 0) {
		if ( rdch_a1( com, &buf2 ,&queue ,&status ) == 0 )
			printf("%c",buf2);
	}
    } while (queue != 0);

    return ( 0 );
}

/*************************************************************/
/*************** int conect ( void )     *********************/
/*************************************************************/
int conec( void )
{
    int bbb;

    clrscr();

    while(1) {
	isdcd_hm( *modem , &bbb );
	if( bbb != 0 ) {
		recibir();
		mandar();
	}
	else return 0;

    }
}

/*************************************************************/
/*************** int llamar ( int  )     *********************/
/*************************************************************/
int llamar(char *numero)
{
	/* variables locales */
	int bbb;
	int der;

	numero[10] = 0;

	if ( ( der = dial_hm( *modem,numero,&bbb,0 ) ) != 0) {
		centrar("Dial error.",25);
		getch();
		return -1;
	}

	isdcd_hm( *modem, &der );
	if (der != 0)
		conec();
	mreport( bbb );
	return -1;
}

/************************************************************/
/*************      mandarcmd       *************************/
/************************************************************/
int mandarcmd( char *mandar )
{
	/* variables */
	int er;
	int ercode;

	char buffer[50];

	er = query_hm( *modem ,mandar , buffer , &ercode);

	cprintf("%s",buffer);

	if(er)
		return -1;

	return 0;
}

/*************************************************************/
/*************** int inic_modem ( void ) *********************/
/*************************************************************/
int inic_modem( void )
{
 /* variables locales */
    int er;
    int rcode;
    int loop = 1;
    int baud;

    while (loop == 1) {
	er = open_a1( com,200,200,0,0,buf);
	switch (er) {
		case 0: loop = 0;
			break;
		case 9: close_a1( com );
			break;
		default: error(2);
	}
    }


    er = struc_hm( modem );        /* tira null pointer assigment */

    er += reset_hm( *modem, &baud , &rcode );
    er += init_hm( *modem , &rcode , 1);
    er += rcode;
    if (er)           /* not zero */
	return -1;

    return(0);
}

/*************************************************************/
/*************** int leer ( void ) ***************************/
/*************************************************************/
int leer(void)
{
   /* variables locales */
   static char buff[25*80];		/* 25 telefonos */
   char paso;
   int i=0,yg=4;	/* usados por read / usado por prompt */
   int er = 1; 		/* igual 0 es end of file */
   int num =0;          /* valor de telefonos leidos 0==1 */
   int y=0;		/* longitud del telefono */

    clearmenugets();
    do {
	if ( (er =_read(handle,buff+i,1)) == -1 )
	    return -1;   		/* error 1 */

	else if (buff[i] == ';')
	{
	    do
	    {
		 if ( (er =_read(handle,&paso,1)) == -1)
		    return -1;    	/* error 1 */
	    } while (paso != 10);

	}
	else if (buff[i] == 10)
	{
	    if (y>0)
	    {
		buff[i-1] = 0;
		prompt( 1, yg, &buff[i-y], "Telefono a llamar." );
		num++;
		yg++;
		y=0;
	    }
	}
	else
	{
	    i++;
	    y++;
	}

    } while ( er != 0 );

    return num;
}
/*************************************************************/
/************** int  abrir ( void ) **************************/
/*************************************************************/
int abrir(void)
{
    /* variables */
    int choice;
    int result = -1;

    if ( (handle = _open("telefono.tel",O_RDWR)) == -1)
	return -1;

    leer();

    choice = 0;
    choice=menu(choice);

    if ( choice == 0) {
	close( handle );
	return -1;
    }

    result = llamar( PROMPTS[choice -1] );

    _close( handle );

    return(result);
}

