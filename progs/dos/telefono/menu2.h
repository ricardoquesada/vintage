/*                   - Frank Imburgio 4/7/88 -                           */
/*        modificada y mejorada por Ricardo Quesada 22/2/94              */
/*               mirar menu2.c para ver su funcionamiento                */

#include <stdio.h>
#include <conio.h>
#include <dos.h>
#include <string.h>
#include <ctype.h>
#include <bios.h>

int  getcursor(void);
int  menu(int menuchoice);
int  getkey(void);
void prompt(int x,int y,char *prompt, char *msg);
void changetotextcolor(void);
void changetobarcolor(void);
void clearmenugets(void);
void setbarcolor(int fg, int bg);
void settextcolor(int fg, int bg);
void setcursor(unsigned int shape);
void setmessageline(int line);

extern char *PROMPTS[];
extern int  PROMPTY[];
extern int  PROMPTX[];
extern int  NPROMPTS;
extern int  PROMPTLINE;
extern char *PROMPTMSG[];
extern int  nocursor;
extern int  TEXTF;
extern int  TEXTB;
extern int  BARF;
extern int  BARB;