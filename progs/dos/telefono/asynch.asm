page 82,132
Title ASYNCH Serial Interface Driver Package
;==============================================================================
;
;
;	ASYNCH	  Serial Interface Drive Level Zero Functions
;
;		  This code constitutes the level 0 functions of the
;		  ASYNCH Manager Package.
;
;	Version 5.00 (C)Copyright Blaise Computing Inc. 1984-1986, 1988, 1989
;
;==============================================================================
;
;	Equates for current version of package
;
	include beginasm.mac

	beginMod    asynch

version_high	equ	 5
version_low	equ	 0
current_version equ	(256 * version_high) + version_low

;==============================================================================
;
;	User-tunable constants
;

max_com_port	 equ	 4		;Number of data structures allocated,
					;one per port.

;	If debug is set to 1, then variables in the data segment COMDATA are
;	declared public for debugging purposes.
;
debug		equ	0		; Production release

;==============================================================================
;
;	Public And External Declarations
;
public		com_entry		;only callable entry point to this pkg
public		com_swi 		;software interrupt entry point
public		endcode 		;end of code segment
;public 	enddata 		;end of data segment

if debug
include 	asynch.pub
endif

subttl		Useful Equates
page
;==============================================================================
;
;	ASCII Character Set Equates
;
;==============================================================================

NULL	equ	000h		; NULL
SOH	equ	001h		; START OF HEADER
STX	equ	002h		; START OF TRANSMISSION
ETX	equ	003h		; END OF TRANSMISSION
EOT	equ	004h		; END OF TEXT
ENQ	equ	005h		; ENQUE
ACK	equ	006h		; ACKNOWLEDGE
BELL	equ	007h		; BELL CHARACTER
BS	equ	008h		; BACK SPACE
HT	equ	009h		; HORIZONTAL TAB
LF	equ	00Ah		; LINE FEED
VT	equ	00Bh		; VERTICLE TAB
FF	equ	00Ch		; FORM FEED
CR	equ	00Dh		; CARRIAGE RETURN
SO	equ	00Eh
SI_	equ	00Fh
DLE	equ	010h
DC1	equ	011h
DC2	equ	012h
DC3	equ	013h
DC4	equ	014h
NAK	equ	015h		; NEGATIVE ACKNOWLEDGE
SYN	equ	016h
ETB	equ	017h
CAN	equ	018h
EM	equ	019h
;SUB	equ	01Ah
;ESC	equ	01Bh		; ESCAPE
;FS	equ	01Ch
;GS	equ	01Dh
RS	equ	01Eh
US	equ	01Fh
SPACE	equ	020h
DEL	equ	07Fh

TAB	equ	HT		; HORIZONTAL TAB CHARACTER

XON	equ	DC1
XOFF	equ	DC3

BLOCK	equ	219

subttl	Modem Equates
page
;==============================================================================
;
;	Equates defining 8259 (Programmable Interrupt Controller) and 8253
;	(Programmable Interval Timer) ports and constants.
;
inta00		equ	20h			;8259 PORT
inta01		equ	21h			;YET ANOTHER 8259 PORT
tim_cmdreg	equ	43h			;Command register for 8253
tim_ch2 	equ	42h			;Channel two of 8253
tim_gate	equ	61h			;Input gate for channel two
tim_out 	equ	62h			;Output port for channel two

eoi		equ	20h			;END OF INTERRUPT COMMAND
data_seg	equ	40h			;BIOS COMMUNICATION AREA


;==============================================================================
;
;	The PC supports two RS-232 Asynchronous COM ports,
;	and the PS/2 eight or more. They are called
;	COM1:, COM2:, etc. and have the following device addresses:
;
com1_addr	equ	3F8h			;COM1:
com2_addr	equ	2F8h			;COM2:
com3_addr	equ	3220h			;COM3:
com4_addr	equ	3228h			;COM4:
com5_addr	equ	4220h			;COM5:
com6_addr	equ	4228h			;COM6:
com7_addr	equ	5220h			;COM7:
com8_addr	equ	5228h			;COM8:


;==============================================================================
;
;	The various serial interface device registers are represented as
;	offsets from the base register. The two serial interfaces found
;	in most systems are located at 2F8h and 3F8h.
;
com_data	equ	0			;DATA BUFFER (READ & WRITE)
com_dl_lsb	equ	0			;DIVISOR LATCH (LEAST SIG BYTE)
com_dl_msb	equ	1			;DIVISOR LATCH (MOST SIG BYTE)
com_int_enab	equ	1			;INTERRUPT ENABLE REGISTER
com_int_id	equ	2			;INTERRUPT IDENTIFICATION REG
com_line_ctl	equ	3			;LINE CONTROL REG
com_modem_ctl	equ	4			;MODEM CONTROL REG
com_line_status equ	5			;LINE STATUS REG
com_modem_status equ	6			;MODEM STATUS REG

page
;==============================================================================
;
;	These equates correspond to the values expected by the ROM BIOS
;	RS232_IO interrupt (14H) initialization function (AH=0, AH= 5 for the
;	PS/2 series machines).
;
BAUD_110	equ	0
BAUD_150	equ	1
BAUD_300	equ	2
BAUD_600	equ	3
BAUD_1200	equ	4
BAUD_2400	equ	5
BAUD_4800	equ	6
BAUD_9600	equ	7
BAUD_19200	equ	8

PARITY_NONE	equ	0
PARITY_ODD	equ	1
PARITY_EVEN	equ	2
PARITY_MARK	equ	3
PARITY_SPACE	equ	4

DATA_5		equ	0
DATA_6		equ	1
DATA_7		equ	2
DATA_8		equ	3

STOP_1		equ	0
STOP_2		equ	1

;==============================================================================
;
;	Baud Rate Divisors
;
BR_50		equ	900H
BR_75		equ	600H
BR_110		equ	417H
BR_134		equ	359H
BR_150		equ	300H
BR_300		equ	180H
BR_600		equ	0C0H
BR_1200 	equ	060H
BR_1800 	equ	040H
BR_2000 	equ	03AH
BR_2400 	equ	030H
BR_3600 	equ	020H
BR_4800 	equ	018H
BR_7200 	equ	010H
BR_9600 	equ	00CH
BR_19200	equ	006H

DLAB_0		equ	00H		; SET DLAB OFF
DLAB_1		equ	80H		; SET DLAB ON

PAGE
;==============================================================================
;
;	Interrupt Enable Register Values
;
IER_READ	equ	1			;DATA AVAILABLE INTERRUPT
IER_WRITE	equ	2			;TX HOLDING REG EMPTY INTERRUPT
IER_STATUS	equ	4			;RECEIVE LINE STATUS INTERRUPT
IER_ERROR	equ	8			;MODEM STATUS INTERRUPT

;==============================================================================
;
;	Line Control Register Values
;
LCR_WLS0	equ	01H			; WORD LENGTH SELECT BIT 0
LCR_WLS1	equ	02H			; WORD LENGTH SELECT BIT 2
LCR_STB 	equ	04H			; NUMBER OF STOP BITS
LCR_PEN 	equ	08H			; PARITY ENABLE
LCR_EPS 	equ	10H			; EVEN PARITY SELECT
LCR_SP		equ	20H			; STICK PARITY
LCR_SB		equ	40H			; STICK BREAK
LCR_DLAB	equ	80H			; DIVISOR LATCH ACCESS BIT

;==============================================================================
;
;	Line Status Register Values
;
LSR_DR		equ	01H			; DATA READY
LSR_OR		equ	02H			; OVERRUN ERROR
LSR_PE		equ	04H			; PARITY ERROR
LSR_FE		equ	08H			; FRAMING ERROR
LSR_BI		equ	10H			; BREAK INTERRUPT
LSR_THRE	equ	20H			; TRANSMITTER HOLDING REGISTER
LSR_TSRE	equ	40H			; TX SHIFT REGISTER EMPTY

;==============================================================================
;
;	Modem Control Register Values
;
MCR_DTR 	equ	01H			; DATA TERMINAL READY
MCR_RTS 	equ	02H			; REQUEST TO SEND
MCR_OUT1	equ	04H			; OUT1
MCR_OUT2	equ	08H			; OUT2
MCR_LOOP	equ	10H			; LOOPBACK TEST
page
;==============================================================================
;
;	Modem Status Register Values
;
MSR_DCTS	equ	01H			; DELTA CLEAR TO SEND
MSR_DDSR	equ	02H			; DELTA DATA SET READY
MSR_TERI	equ	04H			; TRAILING EDGE RING INDICATOR
MSR_DRLSD	equ	08H			; DELTA RX LINE SIGNAL DETECT
MSR_CTS 	equ	10H			; CLEAR TO SEND
MSR_DSD 	equ	20H			; DATA SET READY
MSR_RI		equ	40H			; RING INDICATOR
MSR_RLSD	equ	80H			; RECEIVE SIGNAL LINE DETECT

MSR_DELTAS	equ	(MSR_DCTS or MSR_DDSR or MSR_TERI or MSR_DRLSD)

subttl		Miscelaneous Other Equates And Structures
page
;==============================================================================
;
;	Error codes returned by COM_ENTRY
;
err_none	equ	0		;no error
err_bad_port	equ	2		;invalid port number
err_not_opened	equ	3		;port is not opened
err_bad_parm	equ	4		;Invalid function or option number.
err_no_port	equ	6		;serial port not found
err_queue_full	equ	7		;output queue is full
err_port_open	equ	9		;port already opened
err_que_empty	equ	10		;input queue empty during read
					;11 reserved for Level Two
					;12 reserved for Level One
					;13 reserved for Level One
err_no_addr	equ	14		;No default port address available.
err_no_irq	equ	15		;No default IRQ line	 available.
err_bad_buf	equ	16		;Bad buffer size or address.
err_unk_div	equ	17		;Unrecognized baud rate divisor.
err_bad_value	equ	18		;Invalid option value.
err_bad_code	equ	255		;program code error

all		equ	0ffffh

;==============================================================================
;
;	Option bits passed to OPEN_PORT in DH register
;
open_no_mcr_ctrl    equ     0001h	;Leave DTR and RTS alone.

open_dh_options     equ     open_no_mcr_ctrl

;==============================================================================
;
;	The stack frame defines the locations of the parameters on the stack
;
stack_frame	struc
p_bp		dw	0
p_dx		dw	0
p_cx		dw	0
p_bx		dw	0
p_ax		dw	0
p_ds		dw	0
p_si		dw	0
p_di		dw	0
p_es		dw	0
p_flags 	dw	0
p_ip		dw	0
p_cs		dw	0
stack_frame	ends

;
;	The return code to be passed back to the COM_ENTRY caller is kept here:
;
return_code	equ	byte ptr [bp-1]

;
;	The port number passed by the caller is kept here:
;
port_number	equ	byte ptr [bp-2]

;
;	The parameter value passed by the caller in BH for SET_OPTION and
;	GET_OPTION and SPEC_FUNCTION is saved here:
;
parameter	equ	byte ptr [bp-3]

;==============================================================================
;
;	Machine model information returned by GET_MACHINE routine.
;
machine_ps2	equ	00001h		    ;PS/2

;
;	System configuration structure returned by PS/2 and other recent
;	BIOS versions.
;
machine_desc	    struc
machine_count	    dw	    ?		    ;Byte count of following data
machine_model	    db	    ?		    ;Model byte
machine_submodel    db	    ?		    ;Submodel byte
machine_rev	    db	    ?		    ;BIOS revision level
machine_feature1    db	    ?		    ;Feature information byte 1
machine_feature2    db	    ?		    ;Feature information byte 2
machine_feature3    db	    ?		    ;Feature information byte 3
machine_feature4    db	    ?		    ;Feature information byte 4
machine_feature5    db	    ?		    ;Feature information byte 5
machine_desc	    ends

model_ps2_30	    equ     0fah	    ;PS/2 Model 25 or 30
model_ps2_50	    equ     0fch	    ;PS/2 Model 50 or 50Z
model_ps2_60	    equ     0fch	    ;PS/2 Model 60
model_ps2_80	    equ     0f8h	    ;PS/2 Model 70 or 80

submodel_ps2_50     equ     004h	    ;PS/2 Model 50
submodel_ps2_60     equ     005h	    ;PS/2 Model 60

					    ;Feature information byte 1:
feat1_mca	    equ     00002h	    ;Micro Channel

;==============================================================================
;
;	Interrupt Sharing chaining structure:  entry point for a handler
;	for a shared interrupt.
;
sharing_signature	equ	424bh
intch_first		equ	80h	    ;Flag for intch_flags entry
					    ;indicating first installed.

int_chaining	struc
intch_entry	db	0ebh,010h	    ;Jump around structure
intch_fptr	dd	0		    ;Forward pointer
intch_sign	dw	sharing_signature   ;Signature
intch_flags	db	intch_first	    ;Flags
intch_reset	db	0ebh,?		    ;Jump to reset routine
intch_res_bytes db	7 dup (0)	    ;Future expansion
int_chaining	ends

int_share_entry macro	entry_name,reset_func
		local	past
		jmp	short past	    ;;Jump around structure
intsh_fptr_&entry_name	dd	0	    ;;Forward pointer
		dw	sharing_signature   ;;Signature
		db	intch_first	    ;;Flags
		jmp	short reset_func    ;;Jump to reset routine
		db	7 dup (0)	    ;;Future expansion
past:
		endm

;==============================================================================
;
;	Option codes for READ_MSR, CHECK_MSR.  These values are used as
;	masks so they must not be changed.
;
READ_MSR_CLEAR	  equ	(not (MSR_DELTAS))  ;Clear simulated delta flags.
READ_MSR_NOCLEAR  equ	(not 0) 	    ;Don't clear delta flags.

subttl		Com Port Information Structures
page
;==============================================================================
;
;	COM queue description:	The COM queues are allocated by the calling
;	program, the address of which is passed to this level.	The queues are
;	used circularly, with the head pointing to the oldest byte in the
;	buffer and the tail pointing to the newest.  When head = tail, the
;	queue is empty.  The size of the buffer (which makes up the two queues)
;	must be no greater than 64K because addressing is done using offset
;	of the segment address of the buffer.
;
com_def 	struc
com_port_numb	dw	?		;relative port number (1 OR more)

com_handler	dd	?		;address of handler entry point

com_dev_addr	dw	?		;address of base device register

com_rearm_addr	dw	?		;global rearm address for sharing

com_dev_busy	dw	?		;nonzero if in interrupt service
com_port_status dw	?		;status of device (see below)
com_port_error	dw	?		;error flags (see below)
com_data_lost	dw	?		;count of lost input characters
com_break_time	dw	?		;delay in MS for BREAK (default 250)
com_irq_enable	db	?		;OCW1 mask to enable IRQ
com_irq_disable db	?		;OCW1 mask to disable IRQ
com_irq_level	dw	?		;interrupt request level

com_lf_state	  dw	?		;State of local flow control
com_lf_hw_mask	  db	?		;MCR bits to control
com_lf_hw_closed  db	?		;MCR value after port closed
com_lf_hw_unready db	?		;MCR value when buffer nearly full
com_lf_hw_ready   db	?		;MCR value when buffer ready

com_q_seg	dw	?		;segment address of queues
com_iq_size	dw	?		;size of input queue in bytes
com_iq_count	dw	?		;number of bytes in input queue now
com_iq_trig_1	dw	?		;XOFF trigger for 50% full
com_iq_trig_2	dw	?		;XOFF trigger for 75% full
com_iq_trig_3	dw	?		;XOFF trigger for 90% full
com_iq_beg	dw	?		;offset address of input queue begining
com_iq_end	dw	?		;offset address of input queue end
com_iq_head	dw	?		;offset address of next input byte
com_iq_tail	dw	?		;offset address of last input byte
com_oq_size	dw	?		;size of output queue in bytes
com_oq_count	dw	?		;number of bytes in output queue now
com_oq_beg	dw	?		;offset address of output queue beg
com_oq_end	dw	?		;offset address of output queue end
com_oq_head	dw	?		;offset of next output byte
com_oq_tail	dw	?		;offset of last output byte

com_msr_sim	db	?		;MSR value with simulated delta flags

com_res1	db	?		;Reserved for future use
com_res2	dd	?		;Reserved for future use
com_def 	ends
;
;==============================================================================
page
;==============================================================================
;
;	The COM_PORT_STATUS word indicates the current device status.
;
com_port_active equ	0001h			;Interrupts enabled
com_error_found equ	0002h			;Error reported (see below)
com_carrier_det equ	0004h			;Carrier detected
com_timer_on	equ	0008h			;ETC timer has been started
com_rem_flow	equ	0010h			;Remote system uses flow ctl
com_rem_xoff	equ	0020h			;Rem sys has sent us an xoff
com_loc_flow	equ	0040h			;Rem sys understands flow ctl
com_xoff_sent	equ	0080h			;We have sent rem sys an xoff
com_send_xoff	equ	0100h			;Tell driver to send an xoff
com_trim_bit_7	equ	0200h			;Set bit 7 input byte to 0
com_write_bit_7 equ	0400h			;Force bit 7 of output to 1
com_buffer_full equ	0800h			;Read buffer is full
com_req_cts	equ	1000h			;Clear To Send is required
com_xon_sent	equ	2000h			;XON sent to remote system
com_send_xon	equ	4000h			;Tell driver to send an XON
;
;==============================================================================
;
;	If the COM_ERROR_FOUND flag is on in the COM_PORT_STATUS word, then
;	the COM_PORT_ERROR word indicates the nature of the error.  Not all
;	things recorded in this word are really errors, but they are all
;	returned to the caller by the READ_PORT and WRITE_PORT functions.
;
com_error_lost	equ	0001h			;WE HAVE LOST INPUT CHARACTERS
com_error_neg	equ	0002h			;READ BUFFER HAS NEGATIVE CNT
com_error_p	equ	0010h			;PARITY ERROR
com_error_o	equ	0020h			;OVERRUN ERROR
com_error_f	equ	0040h			;FRAMING ERROR
com_error_b	equ	0080h			;BREAK INTERRUPT
com_stat_r_off	equ	1000h			;REMOTE SYS SENT XOFF TO US
com_stat_r_on	equ	2000h			;REMOTE SYS SENT XON TO US
com_stat_l_off	equ	4000h			;WE SENT XOFF TO REMOTE SYS
com_stat_l_on	equ	8000h			;WE SENT XON TO REMOTE SYS


;
;	The initial state of the COM port after it has been opened is
;	defined by this equate:
;
com_init_state	equ	com_port_active+com_req_cts

;
;	The COM device flags signal when ASYNCH is servicing an interrupt.
;
com_busy	equ	00FFh			; In the interrupt service
com_free	equ	0000h

;
;	The COM_LF_STATE flags indicate the state of local flow control.
;
com_unready	equ	0001h			;Input queue nearly full
com_mcr_ctrl	equ	0002h			;Control MCR while port open

;
;	Default local hardware flow control options
;
;	Note:  the MCR_OUT2 bit MUST be set to allow interrupts
;	from all known IBM and compatible asynch cards.  For this reason
;	MCR_OUT2 should always be set in the "ready" and "unready" MCR
;	values and probably in the mask value.
;
def_lf_hw_mask	  equ (MCR_DTR+MCR_RTS+MCR_OUT2) ;MCR bits to control
def_lf_hw_closed  equ 0 			 ;MCR value when closed
def_lf_hw_unready equ (MCR_DTR+MCR_RTS+MCR_OUT2) ;MCR value when buffer
						 ;nearly full
def_lf_hw_ready   equ (MCR_DTR+MCR_RTS+MCR_OUT2) ;MCR value when buffer ready

;
;	Restrictions on local hardware flow control.
;
;	The USER_LF_HW_MASK indicates the MCR values that the user may
;	control through the local hardware flow control options.
;	All other MCR bits are inherited from the default values above
;	and cannot be changed by user calls.
;
user_lf_hw_mask     equ (MCR_DTR+MCR_RTS)

subttl		ASYNCH Data segment
page
;==============================================================================
;
;	Data segment for all ASYNCH internal information
;
	beginDSeg COMDATA

;==============================================================================
;
;	These words are useful for debugging. The INT_COUNT word shows the
;	number of times the serial interface routine was awakened.
;
int_count	dw	?			; Initialize to 0.

;
;	The INT_TYPES word contains the value of the interrupt id register
;	for the last two interrupts encountered.
;
int_types	dw	?			;INIT TO 0FFFFh.

page
;==============================================================================
;
;	The COM_CTRL table contains one data structure for each port.
;
	pubSym	com_ctrl,<com_def max_com_port dup (<>)>
;
;==============================================================================

subttl		Miscellaneous Data Structures
page
;==============================================================================
;
;	logical port number of current com port
;
current_port	db	?


;==============================================================================
;
;	The status of the package is reflected in the current_status word.
;
current_status	dw	?		;Initialize to zero.
;
;	defined bit masks for current_status are:
;
cs_initialized	equ	0001h	;initialize has been called
cs_interrupt	equ	0002h	;SWI vector enabled

;
;	When initialize is called, it can be passed an interrupt vector which
;	should be enabled.  The old values from that vector are stored here
;	and restored by the terminate procedure.
;
swi_seg 	dw	?	;segment addr of old SWI vector
swi_loc 	dw	?	;offset addr of old SWI vector
swi_int 	dw	?	;interrupt vector number

;
;	These two variables save the machine identification information
;	so get_machine need only call the BIOS once.
;
machine_known	db	?	;Zero if get_machine has not been called.
machine_save	dw	?	;AX value returned by get_machine.

enddata 	label	far

	endDSeg COMDATA
;
;	This marks the end of the package data segment
;
;==============================================================================

subttl		ASYNCH code segment
page
	beginCSeg   comcode

;==============================================================================
;
;
;		THIS COPYRIGHT MUST REMAIN AT THIS LOCATION IN THE SOURCE CODE.
;
;
copyright:
		db	'Copyright (c)1989 by Blaise Computing Inc.'
		db	'All rights reserved '

;==============================================================================
;
;	Data tables in code segment

;
;
;	The COM_TABLE table contains the address of the COM_DEF control
;	block for each communications port whether active or not.
;
com_table	label	word
port_i		=	offset com_ctrl@
		rept	max_com_port
		dw	port_i
port_i		=	port_i + type com_ctrl@
		endm

;
;	This variable indicates whether the data items have been
;	initialized.  They need be initialized only once.
;
data_initialized    db	0		;Nonzero if already initialized.

;
;	This table is used to validate the possible baud rates and to map
;	those rates into the rates supported by this package.  The 8250
;	chip can support a number of baud rates (even more than are shown
;	in this list), but we only support the popular ones.
;
;	The first value is the baud rate divisor and the second value is
;	either the internal representation for that baud rate (0..7) or
;	a -1 to indicate that this baud rate is known, but not supported.
;
baud_rate	dw	br_50,		-1
		dw	br_75,		-1
		dw	br_110, 	baud_110
		dw	br_134, 	-1
		dw	br_150, 	baud_150
		dw	br_300, 	baud_300
		dw	br_600, 	baud_600
		dw	br_1200,	baud_1200
		dw	br_1800,	-1
		dw	br_2000,	-1
		dw	br_2400,	baud_2400
		dw	br_3600,	-1
		dw	br_4800,	baud_4800
		dw	br_7200,	-1
		dw	br_9600,	baud_9600
		dw	br_19200,	baud_19200
		dw	0,		-1

;
;	This table lists all standard IBM port addresses.
;	It must correspond with the standard_irq table.
;
standard_addr	dw	com1_addr
		dw	com2_addr
		dw	com3_addr
		dw	com4_addr
		dw	com5_addr
		dw	com6_addr
		dw	com7_addr
		dw	com8_addr
max_standard_addr	equ	($ - standard_addr) / 2

;
;	This table lists interrupt request lines used by the
;	standard IBM ports.  The offsets in the table must
;	correspond with the standard_addr table.
;
standard_irq	dw	4		;COM1
		dw	3		;COM2
		dw	3		;COM3
		dw	3		;COM4
		dw	3		;COM5
		dw	3		;COM6
		dw	3		;COM7
		dw	3		;COM8

subttl read_msr, check_msr
page
;==============================================================================
;
;	READ_MSR reads the modem status register on the serial port
;	indicated by SI, recording any changes detected.
;
;	On entry:
;
;	    CL = Option (used as mask internally):
;		 READ_MSR_CLEAR    -	    clear simulated delta flags
;		 READ_MSR_NO_CLEAR - do not clear simulated delta flags
;
;	On exit:
;
;	    AL = current modem status register value.
;	    CL = simulated MSR value (with deltas before clearing).
;	    DX = undefined
;	    Interrupts are enabled.
;
;==============================================================================
read_msr	proc	near

		mov	dx,[si].com_dev_addr	;LOAD ADDRESS OF DEVICE
		add	dx,com_modem_status	;POINT TO MODEM STATUS REGISTER
		cli
		in	al,dx			;READ MODEM STATUS REGISTER
						;(ALSO RESETS INTERRUPT)
		jmp	short $+2		;Give port time to process

		mov	dl,[si].com_msr_sim	;Fetch simulated MSR value.
		and	dl,MSR_DELTAS		;Clear former upper four bits.
		or	dl,al			;Combine new upper bits
						; with accumulated deltas.
		xchg	cl,dl			;CL   = new simulated MSR,
						;  DL = mask.
		and	dl,cl			;DL = new simulated MSR
						;  with flags possibly cleared.
		mov	[si].com_msr_sim,dl	;Save new simulated MSR value.

		test	al,msr_rlsd		;TEST FOR CARRIER DETECT
		jz	read_msr_0		;SKIP IF CARRIER LOST

		;
		;	Turn on flag indicating that CARRIER DETECT was found
		;
		or	[si].com_port_status,com_carrier_det
		jmp	short read_msr_1	;skip to next test

read_msr_0:	;
		;	Turn off CARRIER DETECT flag
		;
		and	[si].com_port_status,all-com_carrier_det ;turn off flag

read_msr_1:
		;
		;	Don't bother to do anything if DATA SET READY
		;	or RING INDICATOR changed.
		;
		sti
		ret
read_msr	endp

;==============================================================================
;
;	CHECK_MSR reads and returns the MSR value, triggering output
;		  data if appropriate.
;
;	On entry:
;
;	    CL = Option (used as mask internally):
;		 READ_MSR_CLEAR    -	    clear simulated delta flags
;		 READ_MSR_NO_CLEAR - do not clear simulated delta flags
;
;	On exit:
;
;	    AL = undefined.
;	    AH = current modem status register value.
;	    CL = simulated MSR value (with deltas before clearing).
;	    DX = undefined.
;	    ES = undefined.
;	    Interrupts are enabled.
;
;==============================================================================
check_msr	proc	near

		call	read_msr
		mov	ah,al			;Save MSR value in AH

		;
		;	Check CTS requirement for output.
		;
		test	[si].com_port_status,com_req_cts
		jz	check_msr_end		;CTS is not an issue.
		test	al,msr_cts		;CTS asserted by remote sys?
		jz	check_msr_end		;No and we need it.
check_msr_10:					;If yes, fall through and
						;attempt a write.
		call	write_try

check_msr_end:
		ret
check_msr	endp

subttl		read_lsr
page
;==============================================================================
;
;	READ_LSR reads the line status register on the serial port
;	indicated by SI, recording any errors detected.
;	The LSR value is returned in AL.  Interrupts are disabled on exit.
;
;==============================================================================
read_lsr	proc	near

		push	dx
		push	cx
		;
		;	Decode type of error and store in global flag.
		;
		mov	dx,[si].com_dev_addr	;LOAD ADDRESS OF DEVICE
		add	dx,com_line_status	;POINT TO LINE STATUS REGISTER
		xor	cx,cx			;CX will record errors found.
		cli
		in	al,dx			;READ LINE STATUS REGISTER
						;(ALSO RESETS INTERRUPT)

		;
		;	Check for break interrupt
		;
		test	al,lsr_bi		;BREAK INTERRUPT?
		jz	read_lsr_00		;SKIP IF NOT
		or	cx,com_error_b

read_lsr_00:	;
		;	Check for overrun error
		;
		test	al,lsr_or		;CHECK FOR OVERRUN
		jz	read_lsr_10		;SKIP IF NO OVERRUN
		inc	[si].com_data_lost	;BUMP LOST CHARACTER COUNT
		OR	cx,com_error_o

read_lsr_10:	;
		;	Check for framing error
		;
		test	al,lsr_fe		;CHECK FOR FRAMING ERROR
		jz	read_lsr_20		;SKIP IF NO ERROR
		or	cx,com_error_f

read_lsr_20:	;
		;	Check for parity error
		;
		test	al,lsr_pe		;CHECK FOR PARITY ERROR
		jz	read_lsr_30		;SKIP IF NO ERROR
		or	cx,com_error_p

read_lsr_30:
		;
		;	Store any errors found
		;
		jcxz	read_lsr_x		;Skip if no error
		or	[si].com_port_error,cx
		or	[si].com_port_status,com_error_found

read_lsr_x:
		;
		;	Exit
		;
		pop	cx
		pop	dx
		ret
read_lsr	endp

subttl		write_try, write_next
page
;==============================================================================
;
;	WRITE_TRY attempts to transmit the next scheduled character,
;		  but assumes that the CTS requirement is satisfied.
;
;	On exit:
;
;		AL = undefined.
;		DX = undefined.
;		ES = undefined.
;		Interrupts are enabled.
;
;==============================================================================
write_try	proc	near

		;
		;	Check whether XOFF or XON scheduled.
		;
		test	[si].com_port_status,com_send_xoff or com_send_xon
		jnz	write_try_15		;If yes, don't care if
						; output blocked by XOFF.

		;
		;	Check whether blocked by XOFF.
		;
		test	[si].com_port_status,com_rem_xoff
		jnz	write_try_x

write_try_15:
		;
		;	Check whether Transmit Holding Register is empty.
		;
		call	read_lsr		;Also disable interrupts.
		test	al,lsr_thre
		jz	write_try_x		;Not empty, so can't transmit.

		;
		;	Now we know that it's possible to transmit.
		;
		;	Check for possible output:  first check XOFF,
		;	and send it if appropriate.
		;
		test	[si].com_port_status,com_send_xoff
		jz	write_try_20		;Don't send XOFF.

		and	[si].com_port_status,not(com_send_xoff or com_xon_sent)
		or	[si].com_port_status,com_xoff_sent or com_error_found
		or	[si].com_port_error,com_stat_l_off
		mov	al,XOFF 		;Character to transmit.
		jmp	short write_try_transmit
write_try_20:

		;
		;	Else check for XON and send it if appropriate.
		;
		test	[si].com_port_status,com_send_xon
		jz	write_try_30		;Don't send XON.

		and	[si].com_port_status,not com_send_xon  ;Turn off flag
						;Set flag to indicate
						;  that an XON has been sent
		or	[si].com_port_status,com_xon_sent or com_error_found
		or	[si].com_port_error,com_stat_l_on
		mov	al,XON			;Character to transmit.
		jmp	short write_try_transmit
write_try_30:

		;
		;	Else check for data in the output queue.
		;
		push	bx
		mov	bx,[si].com_oq_head	;addr of queue header
		cmp	bx,[si].com_oq_tail	;check for queue empty
		je	write_try_none		;Queue is empty.

		;
		;	There is something in the output queue.
		;
		inc	bx			;point to next char
		cmp	bx,[si].com_oq_end	;check for wrap-around
		jne	write_try_40		;skip unless wrap-around
		mov	bx,[si].com_oq_beg	;point to beginning of buffer
write_try_40:

		;
		;	load & write the character
		;
		mov	es,[si].com_q_seg	;load buffer segment addr
		mov	al,es:[bx]		;load byte to send
		dec	[si].com_oq_count	;reduce count of output queue

		;
		;	Store buffer pointer
		;
		mov	[si].com_oq_head,bx	;save pointer
		pop	bx

write_try_transmit:
		;
		;	Transmit the character in AL.
		;
		mov	dx,[si].com_dev_addr	;load address of device
	if com_data
		add	dx,com_data		;point to data register
	endif
		out	dx,al			;write the character

write_try_x:
		sti
		ret

write_try_none:
		pop	bx
		jmp	write_try_x

write_try	endp

;==============================================================================
;
;	WRITE_NEXT attempts to transmit the next scheduled character.
;		   It first checks the CTS requirement.
;
;	On exit:
;
;		AL = undefined.
;		CL = undefined.
;		DX = undefined.
;		ES = undefined.
;		Interrupts are enabled.
;
;==============================================================================
write_next	proc	near

		;
		;	Check CTS requirement.
		;
		test	[si].com_port_status,com_req_cts
		jz	write_next_10		;Skip because CTS not required.
		mov	cl,READ_MSR_NOCLEAR
		call	read_msr
		test	al,msr_cts		;CTS asserted by remote sys?
		jz	write_next_x		;No and we need it.
write_next_10:					;If yes, fall through and check
						;another requirement.
		call	write_try
						;Leave interrupts enabled.
write_next_x:
		ret
write_next	endp

subttl		Handle a Line Status Interrupt
page
;==============================================================================
;
;	Receiver Line status Interrupt
;
;		Possible sources:
;
;			1)  overrun error
;			2)  parity error
;			3)  framing error
;			4)  break interrupt
;
;==============================================================================
si_error:
		or	[si].com_port_status,com_error_found
		call	read_lsr
		sti
		jmp	com_int_x

subttl		Handle Received Data Available Interrupt
page
;==============================================================================
;
;	RECEIVED DATA AVAILABLE INTERRUPT
;
;		Possible sources:
;
;			1)  receiver data available
;
;==============================================================================
si_read:	;
		;	Read character from interface and store it in the
		;	input queue.  If the input queue is fill, then set
		;	error flag.
		;
		mov	dx,[si].com_dev_addr	;LOAD ADDRESS OF DEVICE REG
		add	dx,com_data		;POINT TO DATA REGISTER
		in	al,dx			;READ CHARACTER

		;
		;	The character we just read in in AL.  If we understand
		;	XON/XOFF from the remote system, then see whether the
		;	character just read is an XON or XOFF.	If so, then
		;	set the switch which tells the write interrupt routine
		;	whether it can write;  Otherwise, skip down to the
		;	next major code section.
		;
		test	[si].com_port_status,com_rem_flow ;DO WE SUPPORT XON
						;  OR XOFF FROM REMOTE SYSTEM?
		jz	si_read_1		;DON'T BOTHER WITH THESE TESTS
		cmp	al,XON			;CHECK FOR XON
		je	si_read_0
		cmp	al,XOFF 		;CHECK FOR XOFF
		jne	si_read_1		;DON'T BOTHER

		;
		;	We have recevied an XOFF character from the remote
		;	system, so we should suspend sending anything to it
		;	until it sends us an XON character.
		;
		or	[si].com_port_status,com_rem_xoff+com_error_found
		or	[si].com_port_error,com_stat_r_off
		jmp	com_int_x		;ALL DONE

si_read_0:	;
		;	We have received an XON character from the remote
		;	system, so allow more characters to be transmitted.
		;
		and	[si].com_port_status,all-com_rem_xoff ;TURN off SWITCH
		or	[si].com_port_status,com_error_found
		or	[si].com_port_error,com_stat_r_on
		call	write_next		;Write immediately if possible.
		jmp	com_int_x

si_read_1:	;
		;	Attempt to insert the character just read into AL
		;	into the input queue for this port.  Insertion into
		;	the queue is performed at the tail of the queue.
		;	The queue functions just as a queue in a bank lobby.
		;	People arrive and enter the tail end of the queue and
		;	gradually make their way to the front of the queue
		;	where they get removed from the queue and serviced.
		;
		mov	bx,[si].com_iq_tail	;LOAD ADDR OF NEXT AVAIL BYTE
		inc	bx			;BUMP POINTER
		cmp	bx,[si].com_iq_end	;CHECK FOR WRAP-AROUND
		jne	si_read_2		;SKIP UNLESS WRAP-AROUND
		mov	bx,[si].com_iq_beg	;POINT TO BEGINING OF BUFFER

si_read_2:	;
		;	Check for queue full
		;
		cmp	bx,[si].com_iq_head	;CHECK FOR BUFFER FULL
		jne	si_read_3		;SKIP IF ROOM

		;
		;	There is no roon in the queue to insert this character,
		;	so bump the LOST_CHARACTER counter and set the queue
		;	full error flag.
		;
		inc	[si].com_data_lost	;BUMP LOST CHARACTER COUNT
		or	[si].com_port_status,com_buffer_full+com_error_found
		or	[si].com_port_error,com_error_lost
		jmp	com_int_x

si_read_3:	;
		;	If the input queue is starting to get full and the
		;	remote system understands an XON/XOFF sequence from
		;	us, then send an XOFF.
		;
		test	[si].com_port_status,com_loc_flow ;DOES HOST
						;  UNDERSTAND XON FROM US?
		jz	si_read_5		;DON'T BOTHER WITH THESE TESTS
		mov	dx,[si].com_iq_count	;numb chars in input queue
		cmp	dx,[si].com_iq_trig_1	;compare with 1st stigger
		jb	si_read_5		;skip if count < trigger

		;
		;	The remote system understands an XOFF request from us
		;	(or at least we think it does), so send an XOFF to it
		;	and see what happens.  If the remote system does
		;	understand, then it will stop sending; otherwise, the
		;	buffer will keep getting fuller.
		;
		;	In the event that the remote system just didn't hear
		;	the XOFF request, we will send another one when the
		;	2nd and 3rd trigger values are reached.
		;
		;	The XON is sent by the READ_COM_PORT routine when
		;	the input queue becomes empty.
		;
		test	[si].com_port_status,com_xoff_sent ;have we already
						;  sent an XOFF to the host?
		jz	si_read_4		;skip if we havn't sent XOFF
		cmp	dx,[si].com_iq_trig_2	;compare with 2nd trigger value
		je	si_read_4		;send if match
		cmp	dx,[si].com_iq_trig_3	;compare with 3rd trigger
		jne	si_read_5		;don't send another XOFF now

si_read_4:	;
		;	Set flag which tells output driver to send an XOFF
		;	and wake up the output interrupt driver if it isn't
		;	already active.
		;
		or	[si].com_port_status,com_send_xoff ;TELL WRITE DRIVER
						;  TO SEND AN XOFF
		mov	ah,al
		call	write_next		;Write immediately if possible.
		mov	al,ah

si_read_5:

		;
		;	Handle local hardware flow control
		;
		mov	dx,[si].com_iq_count	;Number of chars in input queue
		cmp	dx,[si].com_iq_trig_3	;Compare with 90% trigger.
		jne	si_read_55		;Skip if count != trigger.

		or	[si].com_lf_state,com_unready

		test	[si].com_lf_state,com_mcr_ctrl
		jz	si_read_55		;Skip if control not permitted

		mov	cx,ax			;Save character in AL.
		mov	dx,[si].com_dev_addr	;Get address of device register
		add	dx,com_modem_ctl	;Point to modem control reg
		in	al,dx			;Current port setting
;		jmp	short $+2		;Give port time to process

						;Build new MCR value in AH:
						;mask indicates bits to use
						;from "unready" value.
		mov	ah,[si].com_lf_hw_unready   ;Indicate port not ready.
		xor	ah,al
		and	ah,[si].com_lf_hw_mask
		xor	ah,al
		cmp	al,ah			;Skip output if no change.
		je	si_read_54
		mov	al,ah
		out	dx,al
si_read_54:
		mov	ax,cx			;Restore character to AL.
si_read_55:

		;
		;	Put character in AL into input queue
		;
		inc	[si].com_iq_count	;BUMP INPUT QUEUE SIZE
		mov	dx,[si].com_q_seg	;LOAD SEG ADDR OF BUFFER
		mov	es,dx			;COPY TO ES
		mov	es:[bx],al		;STORE CHARACTER
		mov	[si].com_iq_tail,bx	;UPDATE POINTER

		;
		;	If the queue full flag is on, then turn it off
		;
		test	[si].com_port_status,com_buffer_full
		jz	si_read_6
		and	[si].com_port_status,all-com_buffer_full

si_read_6:	jmp	com_int_x

subttl		handle Transmitter Holding Register Empty Interrupt
page
;==============================================================================
;
;	TRANSMITTER HOLDING REGISTER EMPTY INTERRUPT
;
;		Possible sources:
;
;			1)  the read routine wants us to send an XOFF to the
;			    remote system.
;
;			2)  The WRITE_PORT proc has inserted a character into
;			    the output queue.
;
;			3)  The modem state has changed to CLEAR TO SEND.
;
;==============================================================================
si_write:
		call	write_next
		jmp	com_int_x
page
;==============================================================================
;
;	MODEM STATUS INTERRUPT
;
;		Possible sources:
;
;			1)  clear to send
;			2)  data set ready
;			3)  ring indicator
;			4)  received line signal (carrier) detect
;
;==============================================================================
si_modem:	;
		;	The status of the modem has changed.  Check the
		;	current status and take appropriate action.
		;
		mov	cl,READ_MSR_NOCLEAR
		call	check_msr
		jmp	com_int_x

subttl		serial interface interrupt handler
page
;==============================================================================
;
;	SERIAL INTERFACE INTERRUPT HANDLER
;
;	This interrupt service routine handles interrupts generated by the
;	8250.  All four types of interrupts are serviced:
;
;		- Change in modem status
;		- Transmitter holding register empty
;		- Received data is ready
;		- Received character error or "break" condition
;
;	The global table COM_TABLE contains the addresses of the port control
;	structures.
;
;==============================================================================

;
;	This table defines the routines which handle the various types of
;	interrupts which can occur on the serial interface.
;
SER_INT_TABLE	LABEL	WORD
		DW	SI_MODEM
		DW	SI_WRITE
		DW	SI_READ
		DW	SI_ERROR
SER_INT_END	EQU	$-SER_INT_TABLE

;
;	The INT_ENTRY table lists the interrupt entry point address for each
;	port.
;
int_entry_table_item	macro	port_num	    ;;Single table item.
			dw	handler&port_num&_entry
			endm

int_entry	label	word
port_i		=			1

		rept			max_com_port
		int_entry_table_item	%port_i
port_i		=			port_i + 1
		endm

;
;	The INT_HANDLER macro defines the interrupt entry point for a
;	single port.  It also defines the following labels (where X
;	is the port number (1..max_com_port)):
;
;		handlerX_entry		Entry point address.
;		intsh_fptr_X		Doubleword pointer to next chained
;					handler.
;
int_handler	macro	    port_num
		local	    reset_routine
handler&port_num&_entry:
		int_share_entry port_num,reset_routine
		push	    bx
		mov	    bx,((port_num - 1) * 2) ;Index into COM_TABLE.
		call	    com_int	    ;COM_INT returns only if sharing.
					    ;On return, interrupts should
					    ; already be disabled.
		pop	    bx
		jmp	    intsh_fptr_&port_num ;Jump to next handler.

reset_routine:				    ;Reset routine.
		push	    bx
		mov	    bx,((port_num - 1) * 2) ;Index into COM_TABLE.
		jmp	    near ptr shutdown_port
		endm

;
;	The following REPT loop constructs max_com_port copies of the
;	entry point structure defined above.
;
port_i		=	    1

		rept	    max_com_port
		int_handler %port_i
port_i		=	    port_i + 1
		endm

;
;	COM_INT is the common code for all ports.  On entry, BX is the
;	index of the current port in the COM_TABLE and INT_ENTRY.
;
;	COM_INT exits in one of two ways:
;
;	    1) If the current port has no interrupt pending and the
;	       interrupt sharing structure indicates that there is another
;	       handler for this interrupt, it returns to the specific code
;	       for this port, which then JMPs to the next handler.
;	    2) Otherwise COM_INT handles the port until all interrupts
;	       on this port are cleared, sends the EOI to the interrupt
;	       controller, and performs an IRET back to the caller.
;
com_int:
		push	ds
		push	si
		mov	si,seg com_ctrl@
		mov	ds,si
		assume	ds:seg com_ctrl@
		mov	si,com_table[bx]
		mov	[si].com_dev_busy,com_busy
		sti
		push	ax
		push	dx
		push	bx			;Index into COM_TABLE etc.
		push	cx
		push	es
		push	di

		xor	bx,bx			;No interrupts detected
		push	bx			; yet on this port.

		test	[si].com_port_status,com_port_active
		jz	com_int_4		;Data in structure isn't
						; initialized.

com_int_2:
		;
		;	Test the device for an interrupt pending.
		;	Write interrupts must be enabled if a higher priority
		;	interrupt is pending, so that a write interrupt can
		;	be serviced if such an interrupt occurs while the
		;	first is serviced. This can happen during, for example,
		;	a line status error.  If the write interrupt is not
		;	processed, interrupts of equal and lower priority
		;	(write and modem) will no longer be processed.
		;
		;
		mov	dx,[si].com_dev_addr	;Load address of device
		add	dx,com_int_id		;Point to IIR
		in	al,dx			;Read IIR
		test	al,1			;Check for interrupt
		jnz	com_int_4		;Exit if no interrupts


com_int_1:	;
		;	There is an interrupt pending for this device, so
		;	decode the interrupt and dispatch to the appropriate
		;	handler logic.
		;
		pop	bx
		or	bx,1			;Interrupt detected.
		push	bx

		push	ax			;Save interrupt type.
		xor	ah,ah			;Move interrupt type into
		mov	bx,ax			;index register
		jmp	ser_int_table[bx]

com_int_x:					;Return location
		pop	ax			;Retrieve interrupt type.
		test	al,4			;Test for a interrupt of higher
		jz	com_int_2		;priority than a write.
						;Have a line status or read
						;which may have masked a write
						;interrupt,
		call	write_next		;so write if possible.
		jmp	com_int_2

						;In case an interrupt occurred
						;during processing, it must
						;also be serviced.
com_int_4:
		;
		;	If this port had no pending interrupts and
		;	this interrupt is shared with another handler,
		;	pass control to next handler.
		;
		pop	bx			;BX includes flag indicating
		test	bx,1			; whether this port had an
						; interrupt.
		pop	di
		pop	es
		pop	cx
		pop	bx			;Index into INT_ENTRY.
		jnz	com_int_5		;Interrupt occurred:
						; exit normally.

						;No interrupt occurred.
		mov	bx,int_entry[bx]
		test	cs:[bx].intch_flags,intch_first
		jnz	com_int_5		;No one to share with.

		pop	dx
		pop	ax
		cli
		mov	[si].com_dev_busy,com_free  ;Clear busy flag.
		pop	si
		pop	ds
		ret				;Return to code that will
						; jump to next handler.

		;
		;	Normal exit from COM_INT.
		;
com_int_5:
						;(BX is undefined.)
		;
		;	Set up for possibly rearming other devices
		;	sharing this interrupt.  (Doing so now minimizes
		;	the time between the CLI and the IRET.)
		;
		mov	dx,[si].com_rearm_addr
		cmp	dx,0

		;
		;	SIGNAL END OF INTERRUPT TO 8259 PIC
		;
		mov	al,EOI
		cli
		out	INTA00,al

		;
		;	Rearm other devices sharing this interrupt (if any).
		;
		je	com_int_6		;DX = 0 if no rearming needed.
		out	dx,al			;Output any byte value.
com_int_6:

		pop	dx
		pop	ax
		mov	[si].com_dev_busy,com_free
		pop	si
		pop	ds
		inc	sp			;Discard return address
		inc	sp			; pushed by CALL COM_INT.
		pop	bx
		iret

subttl	shutdown_port
page
;
;	SHUTDOWN_PORT is the common reset routine for all ports.
;	Future operating systems may shut down a port by performing a
;	far CALL to the reset routine recorded in the interrupt
;	sharing structure.
;
;	SHUTDOWN_PORT is entered from the reset routine defined in
;	the int_handler macro above.  On entry, BX is the index of the
;	current port in COM_TABLE, and the caller's BX value is saved
;	on the top of the stack.
;
;	Warning:  This version of RESET_PORT triggers one additional
;	interrupt when writing to the modem control register.
;
shutdown_port	proc	far

		push	ds
		push	si
		mov	si,seg com_ctrl@
		mov	ds,si
		assume	ds:seg com_ctrl@
		mov	si,com_table[bx]

		push	ax
		push	cx
		push	dx
		push	es
		push	di

		pushf
		call	reset_port
		popff				;Possibly reenable interrupts.

		pop	di
		pop	es
		pop	dx
		pop	cx
		pop	ax
		pop	si
		pop	ds
		pop	bx
		ret
shutdown_port	endp

;
;	This marks the end of the Interrupt Driver code.
;
;==============================================================================

subttl		validate_port
page
;==============================================================================
;
;	Make sure that PORT_NUMBER is valid. If so, return with CARRY off and
;	with SI = offset address of the port description structure; otherwise,
;	return with CARRY on.  Preserve all other registers except the flags.
;
;==============================================================================
validate_port	proc	near

		push	bx			; Save BX
		;
		;	Make sure that port number is valid
		;
		mov	bl,port_number
		cmp	bl,1			; Must be greater than
		jl	val_port_0		; or equal to one, but
		cmp	bl,max_com_port 	; less than or equal to
		jbe	val_port_1		; the maximum number of
						; ports.
val_port_0:	mov	return_code,err_bad_port
		pop	bx			; Restore BX
		stc
		ret

val_port_1:	;
		;	Put address of port definition struct in SI
		;
		xor	bh,bh			; Clear high byte
		dec	bx			; Adjust for zero based index
		shl	bx,1			; Convert to word offset
		mov	si,com_table[bx]	; Load address of port desc
		pop	bx			; Restore BX
		clc
		ret
validate_port	endp

subttl		get_port_addr
page
;==============================================================================
;
;	Given the com port number in AL (1..8), return the address of the
;	I/O port for that device.
;
;	On return, DX will contain the device register address, or zero if
;	no device was found.
;
;==============================================================================

get_port_addr	proc	near
		;
		;	Get com port address for port number specified in AL.
		;
		mov	bl,al			;copy port number to BL
		xor	bh,bh			;clear high byte of index
		dec	bx			;force port val to 0..7
		sal	bx,1			;convert to word offset

		cmp	al,4
		ja	get_port_addr_1 	;Port number exceeds 4

						;Ports 1..4 recorded by BIOS
		push	ds
		mov	ax,data_seg		;POINT TO ROM BIOS DATA SEG
		mov	ds,ax			;SET DS TO BIOS
		mov	dx,[bx] 		;copy I/O port address to DX
		pop	ds			;restore old DS value
		jmp	short get_port_addr_exit

get_port_addr_1:				;Port number exceeds 4
		cmp	al,max_standard_addr
		ja	get_port_addr_bad	;Beyond new_port_addr table
		mov	dx,standard_addr[bx]	;copy I/O port address to DX
		jmp	short get_port_addr_exit

get_port_addr_bad:
		mov	dx,0			;Unknown port

get_port_addr_exit:
		ret
get_port_addr	endp

subttl		read_line_ctl
page
;==============================================================================
;
;	This routine reads the Line Control Register (address in DX), and
;	returns the baud rate in AH and the data bits, parity and stop bits
;	in AL.	The encoding of the data, parity and stop information is
;	as defined in the Technical Reference Manual.  The baud
;	rate returned in AH is in the range baud_110 to baud_19200.
;
;	NOTE: if the DLAB bit is on in the specified LCR, then it will be
;	turned off on exit.  If the value in DX doesn't point to an INS 8250
;	Line Control Register, then the routine might well loop.
;
;==============================================================================
read_line_ctl	proc	near

		;
		;	Read line control register...
		;
		push	bx		; save scratch register
		push	cx		; save another scratch register

		cli			; No interrupts while DLAB altered
read_lc_0:	in	al,dx		; Read line control register
		jmp	short $+2	; Give port time to process
		test	al,lcr_dlab	; See if dlab is on
		jz	read_lc_1	; Skip if it isn't on
		and	al,7fh		; Turn off the dlab bit
		out	dx,al		; Write register
		jmp	short $+2	; Give port time to process
		jmp	read_lc_0	; Go read it again

read_lc_1:	push	ax		; Save value of register
		and	al,3fh		; Isolate parity, data & stop bits
		mov	cl,al		; Store value in scratch register
		pop	ax		; Restore line ctrl register value
		push	ax		; Save it again
		or	al,lcr_dlab	; Set DLAB on
		out	dx,al		; Write to lcr
		sub	dx,3		; Point to lsb of divisor latch
		in	al,dx		; Get lsb of baud rate divisor
		jmp	short $+2	; Give port time to process
		mov	ah,al		; Save lsb
		inc	dx		; Addr of divisor latch msb
		in	al,dx		; Get msb of baud rate divisor
		xchg	al,ah		; Swap the bytes
		mov	bx,ax		; Save divisor value in BX
		pop	ax		; Restore original LCR value
		add	dx,2		; Get address of LCR
		out	dx,al		; Write old value back
		mov	ax,bx		; Restore divisor
		xor	bx,bx		; Clear index
		sti			; Interrupts back on

read_lc_2:	;
		;	Translate the baud rate divisor into a number in the
		;	range 1..16.
		;
		cmp	ax,baud_rate[bx];compare divisor with table entry
		je	read_lc_4	;skip on a match
		cmp	baud_rate[bx],0 ;check for the end of the table
		je	read_lc_3	;skip if at end of table
		add	bx,4		;point to next entry
		jmp	read_lc_2	;loop

read_lc_3:	;
		;	Unknown baud rate divisor
		;
		xor	ax,ax		;set AX to unknown baud rate
		stc			;set CARRY to indicate error
		jmp	short read_lc_x

read_lc_4:	;
		;	Known baud rate divisor - BX is index into BAUD_RATE
		;	table.
		;
		mov	ah,bl		;copy index to AH
		add	bx,2		;point to second word
		mov	bx,baud_rate[bx];load second word
		shr	ah,1		;divide AX by the size of one entry
		shr	ah,1		;  in the BAUD_RATE table
		inc	ah		;

		;
		;	AH now contains a value in the range 1..16 which
		;	indicates the baud rate.  Register BX contains a
		;	number in the range 0..8 or -1 if the baud rate
		;	is unsupported.
		;
		cmp	bx,-1
		jne	read_lc_5

		;
		;	Unsupported baud rate - set carry flag
		;
		stc
		jmp	short read_lc_x

read_lc_5:	;
		;	Known and supported baud rate
		;
		mov	ah,bl		; Move baud rate value to AH, and
		clc			; clear the carry flag (no error).

read_lc_x:	mov	al,cl		; restore low byte values
		pop	cx
		pop	bx
		ret
read_line_ctl	endp

subttl		enable_lcr
page
;==============================================================================
;
;	ENABLE_LCR
;
;	This proc uses the values set in AH to set the baud rate, and the
;	value in AL to set the parity, data and stop bits in the port
;	specified by DX.
;
;	The bits in AL correspond to the settings described in
;	the Technical Reference Manual.
;
;==============================================================================

br_table	label	word
		dw	br_110		; 110 baud
		dw	br_150		; 150 baud
		dw	br_300		; 300 baud
		dw	br_600		; 600 baud
		dw	br_1200 	; 1200 baud
		dw	br_2400 	; 2400 baud
		dw	br_4800 	; 4800 baud
		dw	br_9600 	; 9600 baud
		dw	br_19200	; 19200 baud

enable_lcr	proc	near
		push	bx
		push	cx
		push	dx

		;
		;	DX contains the address of the data register
		;
		;	Set DLAB bit in LSR on so that we can set the
		;	baud rate
		;
		cli				; Disable interrupts
		push	ax
		mov	cx,dx			; Save data reg address
		add	dx,com_line_ctl 	; Point to line control reg
		mov	al,lcr_dlab		; Set dlab bit
		out	dx,al			;   and write it
		pop	ax			; Restore values passed

		;
		;	Now figure out the new value to stuff into the LCR
		;	for the desired baud rate.  The baud rate is in
		;	AH.  This is used as an index into br_table to
		;	find the divisors.
		;
		push	ax

		xor	bx,bx
		mov	bl,ah			; Copy the initialization
		shl	bl,1			; and multipy by 2 for index.
		add	bx,offset br_table	; Point into baud rate table
		mov	dx,cx			; Point to divisor latch lsb
		mov	al,cs:[bx]		; Load divisor lsb
		out	dx,al			; Write to register
		inc	dx			; Point to divisor latch msb
		mov	al,cs:[bx+1]		; Load divisor msb
		out	dx,al			; Write to register
		pop	ax			; Recover initial AL value
		add	dx,2			; Point to line ctrl reg
		and	al,3fh			; Mask just bits of interest
		out	dx,al			; Set parity, stop & data bits
						;  and set dlab off
		sti

		;
		;	Return to caller
		;
		pop	dx
		pop	cx
		pop	bx
		ret
enable_lcr	endp

subttl		update_lcr
page
;==============================================================================
;
;	This routine is a quick way of changing the baud rate,
;	parity, data or stop bits.  The BAUD rate is represented in BL (low
;	order 4 bits), and the parity, data and stop bits are represented
;	in BL as described in the Technical Reference manual.
;
;	If the BH is not zero, BH is interpreted as a bit mask which
;	indicates what bits should be turned off in the byte transferred
;	to the line control register (an BL contains the new values for
;	those bits).  If the BH is zero, BL is the new BAUD rate.
;
;==============================================================================
update_lcr	proc
		mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl
		call	read_line_ctl

		;
		;	AH contains the baud rate and AL the rest of the
		;	information.
		;
		or	bh,bh
		jz	update_lcr_1
		and	al,bh	       ; Replace the masked bits with
		or	al,bl	       ; the new value.
		jmp	short update_lcr_2
update_lcr_1:			       ; BH is zero, so BL is the BAUD rate
		mov	ah,bl
update_lcr_2:
		mov	dx,[si].com_dev_addr
		call	enable_lcr
		ret
update_lcr	endp

subttl		check_com_port
page
;==============================================================================
;
;	Test for the presence of the specified COM port and disable FIFOs
;	if any.
;
;	DX contains the address of the port's base I/O register, and DS:SI
;	points to the port description for that device.
;
;	On return, if port exists, clear CARRY; otherwise, set CARRY.
;		   AX is undefined.
;
;==============================================================================
check_com_port	proc	near
		push	cx			;save CX
		mov	cx,dx			;save DX value in CX
		add	dx,com_int_id		;point TO INTERRUPT ID REG
		mov	al,0
		out	dx,al			;Disable 16550 FIFOs, if any
		jmp	short $+2		;Give port time to process
		in	al,dx			;READ INTERRUPT ID REG
		test	al,0F8h 		;SEE IF PORT IS DEFINED
		jz	ckp_0			;skip if port exists
		stc				;set carry flag
		pop	cx			;restore CX
		ret

ckp_0:
		mov	dx,cx			;RESTORE DEVICE ADDRESS
		pop	cx
		clc
		ret
check_com_port	endp

subttl		check_parm
page
;==============================================================================
;
;	Use the values in BL (low bounds) and BH (high bounds) to check the
;	value or PARAMETER.  Also make sure that the PORT_NUMBER is valid
;	and opened.
;
;==============================================================================
check_parm	proc	near
		cmp	ah,bh
		ja	check_parm_0
		cmp	ah,bl
		jb	check_parm_0
		call	validate_port
		jc	check_parm_1
		test	[si].com_port_status,com_port_active
		jz	check_parm_2
		clc
		ret

check_parm_0:	mov	return_code,err_bad_value
		stc
		ret

check_parm_1:	mov	return_code,err_bad_port
		stc
		ret

check_parm_2:	mov	return_code,err_not_opened
		stc
		ret
check_parm	endp

subttl		check_port
page
;==============================================================================
;
;	Make sure that port is valid and opened.  Set carry if error;
;	preserve all registers.
;
;==============================================================================
check_port	proc	near
		call	validate_port
		jnc	check_port_1
		mov	return_code,err_bad_port
		stc
		ret

check_port_1:	test	[si].com_port_status,com_port_active
		jnz	check_port_2
		mov	return_code,err_not_opened
		stc
		ret

check_port_2:	clc
		ret
check_port	endp

subttl		set_parm
page
;==============================================================================
;
;	Make sure that parameter value is OK and that the
;	port is valid and opened.
;
;==============================================================================
set_parm	proc	near
		mov	bl,0
		mov	bh,1
		call	check_parm
		jnc	set_parm_0
		ret

set_parm_0:	;
		;	Update COM_PORT_STATUS word with new option value
		;
		mov	bl,parameter		;load parameter value
		or	bl,bl			;check for OFF option
		jnz	set_parm_1		;skip if ON option

		;
		;	turn option OFF
		;
		not	cx
		and	[si].com_port_status,cx
		ret

set_parm_1:	;
		;	turn option ON
		;
		or	[si].com_port_status,cx
		ret
set_parm	endp

subttl		set_baud
page
;==============================================================================
;
;	Set new BAUD RATE
;
;==============================================================================
set_baud	proc	near
		mov	bl,0
		mov	bh,8
		call	check_parm
		jnc	set_baud_0
		ret

set_baud_0:	;
		;	put the new baud rate into the appropriate bits
		;	in register BL.
		;
		xor	bh,bh			; Set BH to zero
		mov	bl,parameter		; Load new parameter value
		call	update_lcr		; Update the port
		ret
set_baud	endp

subttl		set_parity
page
;==============================================================================
;
;	Set new PARITY
;
;==============================================================================
set_parity	proc	near
		mov	bl,0
		mov	bh,4
		call	check_parm
		jnc	set_parity_0
		ret

set_parity_0:	;
		;	put the new parity in register BL
		;
		mov	bl,parameter		; Load new parameter value
		cmp	bl,0			; Alter the parameter values
		jz	set_parity_1		; to the BIOS bit settings
		shl	bl,1
		dec	bl

set_parity_1:
		mov	cl,3			; Shift count
		shl	bl,cl			; Move to bits 3..5
		mov	bh,0c7h 		; Turn off bits 3..5
		call	update_lcr		; Update the port
		ret
set_parity	endp

subttl		set_data
page
;==============================================================================
;
;	Set new DATA BITS
;
;==============================================================================
set_data	proc	near
		mov	bl,0
		mov	bh,3
		call	check_parm
		jnc	set_data_0
		ret

set_data_0:	;
		;	put the new data bits value into the appropriate bits
		;	in register BL.
		;
		mov	bh,0fch 		;turn off bits 0..1
		mov	bl,parameter		;load new parameter value
		call	update_lcr		;update the port
		ret
set_data	endp

subttl		set_stop
page
;==============================================================================
;
;	Set new STOP BITS
;
;==============================================================================
set_stop	proc	near
		mov	bl,0
		mov	bh,1
		call	check_parm
		jnc	set_stop_0
		ret

set_stop_0:	;
		;	put the new stop bits value into the appropriate bits
		;	in register BL.
		;
		mov	bh,0fbh 		;turn off bit 2
		mov	bl,parameter		;load new parameter value
		mov	cl,2			;shift count
		shl	bl,cl			;move to bit 2
		call	update_lcr		;update the port
		ret
set_stop	endp

subttl		set option switches
page
;==============================================================================
;
;	Set Transmission Control Switches
;
;==============================================================================
set_rem_flow	proc	near
		mov	cx,com_rem_flow

		; If parameter is 0 (disable remote flow control), then
		; the flag that an XOFF was received must be cleared also.

		mov	bl,parameter
		or	bl,bl
		jnz	set_rem_flow_1
		add	cx,com_rem_xoff
set_rem_flow_1:
		call	set_parm
		call	write_next
		ret
set_rem_flow	endp

set_loc_flow	proc	near
		mov	cx,com_loc_flow
		call	set_parm
		ret
set_loc_flow	endp

set_trimming	proc	near
		mov	cx,com_trim_bit_7
		call	set_parm
		ret
set_trimming	endp

set_forcing	proc	near
		mov	cx,com_write_bit_7
		call	set_parm
		ret
set_forcing	endp

set_cts_option	proc	near
		mov	cx,com_req_cts
		call	set_parm
		call	write_next
		ret
set_cts_option	endp

set_break_time	proc	near
		mov	bl,0
		mov	bh,255
		call	check_parm
		jnc	set_break_0
		ret

set_break_0:	mov	bl,parameter
		xor	bh,bh
		mov	[si].com_break_time,bx
		ret
set_break_time	endp

subttl		set_int_vector
page
;==============================================================================
;
;	The caller of this package can establish an interrupt vector which
;	will be initialized to point to the COM_SWI entry point.  This allows
;	the user to create a small routine which only calls the SET_INT_VECTOR
;	function and then quits, leaving the package resident.
;
;	Note the following:
;
;		1) A nonzero PARAMETER value will initialize the corresponding
;		interrupt vector, but only if a vector has not been initialized
;		already. The previous value of that vector will be remembered
;		locally.
;
;		2) A zero PARAMETER value will restore the locally remembered
;		prior value, but only if it was set previously by a call with
;		a non-zero PARAMETER value.
;
;==============================================================================
set_int_vector	proc	near
		mov	bl,parameter		;Get interrupt number
		or	bl,bl			;Check it for zero
		jz	set_int_1		;Skip if turn-off requested

		test	current_status,cs_interrupt
		jz	set_int_0		;Can proceed
		ret				;Vector already installed
set_int_0:
		xor	bh,bh			;Clear high byte
		mov	swi_int,bx		;Save vector number
		mov	dx,bx			;Copy vector to DX
		mov	ax,offset com_swi	;Point AX to offset
		mov	bx,cs			;  and BX to segment
		call	setinterrupt		;Update the vector
		or	current_status,cs_interrupt
		mov	swi_seg,bx		;Save old interrupt vector
		mov	swi_loc,ax		;  segment and offset values
		ret

set_int_1:	;
		;	Restore old interrupt vector, but only if one
		;	is currently active.
		;
		test	current_status,cs_interrupt
		jnz	set_int_2
		ret

set_int_2:	mov	bx,swi_seg
		mov	ax,swi_loc
		mov	dx,swi_int
		call	setinterrupt
		and	current_status,all-cs_interrupt
		ret
set_int_vector	endp

subttl		set_lf_hw_option
page
;==============================================================================
;
;	Set local hardware flow control MCR value.  The value in AH
;	is installed in the byte at offset BX in the COM_DEF control block.
;
;==============================================================================
set_lf_hw_option proc	 near
		call	check_port
		jnc	set_lf_hw_1
		ret

set_lf_hw_1:
						;AH has new value from user.
		mov	al,[si][bx]		;Former value from structure.
		xor	ah,al
		and	ah,user_lf_hw_mask	;Which bits to use from user.
		xor	ah,al
		mov	[si][bx],ah		;Store new value.
		ret
set_lf_hw_option endp

subttl		load_mcr, control_mcr, update_mcr
page
;==============================================================================
;
;	Load the modem control register using the local hardware flow
;	control mask and the MCR value in the AH register.
;
;==============================================================================
load_mcr	proc	near
		mov	dx,[si].com_dev_addr	;Get address of device register
		add	dx,com_modem_ctl	;Point to modem control reg
		in	al,dx			;Current port setting
;		jmp	short $+2		;Give port time to process
		;
		;	Build new MCR value in AH:  mask indicates bits to use
		;	from initial AH value.
		;
		xor	ah,al
		and	ah,[si].com_lf_hw_mask
		xor	ah,al
		cmp	al,ah			;Skip output if no change
		je	load_mcr_1
		mov	al,ah
		out	dx,al
load_mcr_1:
		ret
load_mcr	endp

;==============================================================================
;
;	Load the modem control register using the local hardware flow
;	control mask and the MCR value in the AH register,
;	but only if com_mcr_ctrl bit is set.
;
;==============================================================================
control_mcr	proc	near

		test	[si].com_lf_state,com_mcr_ctrl
		jz	control_mcr_1

		call	load_mcr
control_mcr_1:
		ret
control_mcr	endp

;==============================================================================
;
;	Update the modem control register depending on the state of the
;	input queue.  (This version assumes that the port is open.)
;
;==============================================================================
update_mcr	proc	near

		test	[si].com_lf_state,com_unready
		jnz	update_unready

		mov	ah,[si].com_lf_hw_ready
		jmp	short update_mcr_1

update_unready:
		mov	ah,[si].com_lf_hw_unready

update_mcr_1:
		call	control_mcr
		ret
update_mcr	endp

subttl		set local hardware flow control options
page
;==============================================================================
;
;	Set Local Hardware Flow Control Options
;
;==============================================================================
set_lf_hw_mask	    proc    near
		    mov     bx,com_lf_hw_mask
		    call    set_lf_hw_option
		    jc	    set_lf_hw_mask_fail
		    or	    [si].com_lf_state,com_mcr_ctrl
		    call    update_mcr
set_lf_hw_mask_fail:
		    ret
set_lf_hw_mask	    endp

set_lf_hw_ready     proc    near
		    mov     bx,com_lf_hw_ready
		    call    set_lf_hw_option
		    jc	    set_lf_hw_ready_fail
		    or	    [si].com_lf_state,com_mcr_ctrl
		    call    update_mcr
set_lf_hw_ready_fail:
		    ret
set_lf_hw_ready     endp

set_lf_hw_unready   proc    near
		    mov     bx,com_lf_hw_unready
		    call    set_lf_hw_option
		    jc	    set_lf_hw_unready_fail
		    or	    [si].com_lf_state,com_mcr_ctrl
		    call    update_mcr
set_lf_hw_unready_fail:
		    ret
set_lf_hw_unready   endp

set_lf_hw_closed    proc    near
		    mov     bx,com_lf_hw_closed
		    call    set_lf_hw_option
		    ret
set_lf_hw_closed    endp

subttl		get_baud
page
;==============================================================================
;
;	Return BAUD RATE parameter value in BH.
;
;==============================================================================
get_baud	proc	near
		call	check_port
		jnc	get_baud_1
		ret

get_baud_1:	mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl
		call	read_line_ctl
		jc	get_baud_err
		mov	bx,[bp].p_bx
		mov	bh,ah
		mov	[bp].p_bx,bx
		ret

get_baud_err:
		mov	return_code,err_unk_div
		ret

get_baud	endp

subttl		get_parity
page
;==============================================================================
;
;	Return PARITY parameter value in BH.
;
;==============================================================================
get_parity	proc	near
		call	check_port
		jnc	get_parity_1
		ret

get_parity_1:	mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl
		call	read_line_ctl

		;
		;	mask out all but the PARITY bits from AL.
		;
		mov	cl,3
		shr	al,cl
		and	al,7

		;
		;	Convert the bit settings to the BIOS values.
		;
		inc	al
		shr	al,1

		mov	bx,[bp].p_bx
		mov	bh,al
		mov	[bp].p_bx,bx
		ret
get_parity	endp

subttl		get_data
page
;==============================================================================
;
;	Return DATA BITS parameter value in BH.
;
;==============================================================================
get_data	proc	near
		call	check_port
		jnc	get_data_1
		ret

get_data_1:	mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl
		call	read_line_ctl

		;
		;	mask out all but the DATA bits from AL.
		;
		and	al,3
		mov	bx,[bp].p_bx
		mov	bh,al
		mov	[bp].p_bx,bx
		ret
get_data	endp

subttl		get_stop
page
;==============================================================================
;
;	Return STOP BITS parameter value in BH.
;
;==============================================================================
get_stop	proc	near
		call	check_port
		jnc	get_stop_1
		ret

get_stop_1:	mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl
		call	read_line_ctl

		;
		;	mask out all but the STOP bit from AL.
		;
		mov	cl,2
		shr	al,cl
		and	al,1
		mov	bx,[bp].p_bx
		mov	bh,al
		mov	[bp].p_bx,bx
		ret
get_stop	endp

subttl		get_parm
page
;==============================================================================
;
;	Get parameter value specified by the mask in CX.  If the corresponding
;	flag bit in COM_PORT_STATUS is on, set a 1 into BH on the parameter
;	stack; otherwise, set a 0 into BH.
;
;==============================================================================
get_parm	proc	near
		call	check_port
		jnc	get_parm_1
		ret

get_parm_1:	xor	al,al
		test	[si].com_port_status,cx
		jz	get_parm_2
		mov	al,1

get_parm_2:	mov	bx,[bp].p_bx
		mov	bh,al
		mov	[bp].p_bx,bx
		ret
get_parm	endp

subttl		get switch values
page
;==============================================================================
;
;		Return Transmission Control Option Values
;
;==============================================================================
get_rem_flow	proc	near
		mov	cx,com_rem_flow
		call	get_parm
		ret
get_rem_flow	endp

get_loc_flow	proc	near
		mov	cx,com_loc_flow
		call	get_parm
		ret
get_loc_flow	endp

get_trimming	proc	near
		mov	cx,com_trim_bit_7
		call	get_parm
		ret
get_trimming	endp

get_forcing	proc	near
		mov	cx,com_write_bit_7
		call	get_parm
		ret
get_forcing	endp

get_cts_option	proc	near
		mov	cx,com_req_cts
		call	get_parm
		ret
get_cts_option	endp

get_break_time	proc	near
		call	check_port
		jnc	get_break_1
		ret

get_break_1:	mov	ax,[si].com_break_time
		mov	bx,[bp].p_bx
		mov	bh,al
		mov	[bp].p_bx,bx
		ret
get_break_time	endp

get_int_vector	proc	near
		mov	ax,swi_int
		mov	bx,[bp].p_bx
		mov	bh,al
		mov	[bp].p_bx,bx
		ret
get_int_vector	endp

subttl		get_lf_hw_option
page
;==============================================================================
;
;	Get parameter value specified by the offset in BX.  The
;	corresponding byte value in the COM_DEF control block is returned
;	to the user in BH.
;
;==============================================================================
get_lf_hw_option proc	 near
		call	check_port
		jnc	get_lf_hw_1
		ret

get_lf_hw_1:
		mov	al,[si][bx]
		and	al,user_lf_hw_mask
		mov	byte ptr [bp].p_bx[1],al
		ret
get_lf_hw_option endp

subttl		get local hardware flow control options
page
;==============================================================================
;
;		Return Local Hardware Flow Control Option Values
;
;==============================================================================
get_lf_hw_mask	    proc    near
		    mov     bx,com_lf_hw_mask
		    call    get_lf_hw_option
		    ret
get_lf_hw_mask	    endp

get_lf_hw_ready     proc    near
		    mov     bx,com_lf_hw_ready
		    call    get_lf_hw_option
		    ret
get_lf_hw_ready     endp

get_lf_hw_unready   proc    near
		    mov     bx,com_lf_hw_unready
		    call    get_lf_hw_option
		    ret
get_lf_hw_unready   endp

get_lf_hw_closed    proc    near
		    mov     bx,com_lf_hw_closed
		    call    get_lf_hw_option
		    ret
get_lf_hw_closed    endp

subttl		get_msr, get_msr_sim_clear, get_msr_sim
page
;==============================================================================
;
;		Return Modem Status Register value three ways
;
;==============================================================================
get_msr 	    proc    near
		    call    check_port
		    jc	    get_msr_exit

		    mov     cl,READ_MSR_NOCLEAR
		    call    check_msr
		    mov     byte ptr [bp].p_bx[1],ah	; Actual MSR value.

get_msr_exit:
		    ret
get_msr 	    endp


get_msr_sim_clear   proc    near
		    call    check_port
		    jc	    get_msr_sim_clear_exit

		    mov     cl,READ_MSR_CLEAR
		    call    check_msr
		    mov     byte ptr [bp].p_bx[1],cl	; Simulated MSR.

get_msr_sim_clear_exit:
		    ret
get_msr_sim_clear   endp


get_msr_sim	    proc    near
		    call    check_port
		    jc	    get_msr_sim_exit

		    mov     cl,READ_MSR_NOCLEAR
		    call    check_msr
		    mov     byte ptr [bp].p_bx[1],cl	; Simulated MSR.

get_msr_sim_exit:
		    ret
get_msr_sim	    endp

subttl		send_break
page
;==============================================================================
;
;	Send a break signal to the the other guy.
;
;==============================================================================
send_break	proc	near
		call	check_port
		jnc	send_break_1
		ret

send_break_1:	mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl
		in	al,dx
		jmp	short $+2		;Give port time to process
		or	al,lcr_sb
		out	dx,al
		push	dx
		mov	dx,[si].com_break_time
		call	delay
		pop	dx
		in	al,dx
		jmp	short $+2		;Give port time to process
		xor	al,lcr_sb
		out	dx,al
		ret
send_break	endp

subttl		drain_iq, drain_oq and critsec
page
;==============================================================================
;
;	Drain input queue
;
;==============================================================================
drain_iq	proc	near
		call	check_port
		jnc	drain_iq_1
		ret

drain_iq_1:	cli
		mov	bx,[si].com_iq_head
		mov	[si].com_iq_tail,bx
		mov	[si].com_iq_count,0	;Reset number of chars in
		sti				;the input queue.

		;
		;	Handle local hardware flow control
		;
		and	[si].com_lf_state,not com_unready
		mov	ah,[si].com_lf_hw_ready
		call	control_mcr

		;	Because the input queue is now empty, we must test
		;	to see if an XOFF was sent.  If so, then an XON
		;	must now be sent as well.

		test	[si].com_port_status,com_xoff_sent  ;XOFF sent?
		jz	drain_iq_2		;Skip unless XOFF was sent.

		;
		;	Set the flag that tells the write interrupt procedure
		;	to output an XON.
		;

		test	[si].com_port_status,com_xon_sent   ;XON already sent?
		jnz	drain_iq_2		;XON has already been sent
		or	[si].com_port_status,com_send_xon   ;Set flag to send
		call	write_next			    ;XON

		;
		;	Turn off the "XOFF SENT" flag, and turn on
		;	the error flag that an XON was sent.
		;
		and	[si].com_port_status,all-com_xoff_sent
		or	[si].com_port_status,com_error_found
		and	[si].com_port_error,all-com_stat_l_off
		or	[si].com_port_error,com_stat_l_on

drain_iq_2:	ret
drain_iq	endp


;==============================================================================
;
;	Drain output queue
;
;==============================================================================
drain_oq	proc	near
		call	check_port
		jnc	drain_oq_1
		ret

drain_oq_1:	cli
		mov	bx,[si].com_oq_head
		mov	[si].com_oq_tail,bx
		mov	[si].com_oq_count,0	;Reset output queue size
		sti
		ret
drain_oq	endp


;==============================================================================
;
;	Return the critical section flag.  If the interrupt service routine
;	is currently processing and interrupt the flag is nonzero; otherwise
;	it is zero.  The location of the flag is returned in ES:BX and the
;	value of the flag is returned in DX.
;
;==============================================================================
critsec 	proc	near
		call	check_port
		jnc	critsec_1
		ret

critsec_1:
		push	ax
		mov	ax,seg com_ctrl@
		mov	[bp].p_es,ax
		lea	ax,[si].com_dev_busy
		mov	[bp].p_bx,ax
		mov	ax,[si].com_dev_busy
		mov	[bp].p_dx,ax
		pop	ax
		ret
critsec 	endp

subttl		report_addr
page
;==============================================================================
;
;	Report miscellaneous address and version information.
;	Register values returned to user:
;
;	    AH	  = number of COM ports supported by Level Zero
;	    BH	  = Level Zero major version number
;	    BL	  = Level Zero minor version number
;	    DX	  = port base address, or 0 if port is closed or invalid.
;	    DS:SI = address of internal control structure for this port,
;		    or 0:0 if port number is invalid.
;	    CX	  = undefined
;	    DI	  = undefined
;	    ES	  = undefined
;
;==============================================================================
report_addr	proc	near

						; Number of ports supported.
		mov	byte ptr [bp].p_ax[1],max_com_port

		mov	[bp].p_bx,current_version ; Level Zero version.

		call	validate_port		; Check for valid port number.
		jnc	report_1

						; Invalid port number.
		mov	[bp].p_dx,0
		mov	[bp].p_si,0
		mov	[bp].p_ds,0
		mov	return_code,err_bad_port
		ret

report_1:					; Valid port number.
		mov	[bp].p_si,si		; Return address of structure.
		mov	[bp].p_ds,ds

		test	[si].com_port_status,com_port_active
		jnz	report_2

						; Port is closed.
		mov	[bp].p_dx,0		; No port address available.
		mov	return_code,err_not_opened
		ret

report_2:					; Port is open.
		mov	dx,[si].com_dev_addr	; Return device address.
		mov	[bp].p_dx,dx

		ret
report_addr	endp

subttl		reset_port
page
;==============================================================================
;
;	Reset the specified communications port, performing all hardware
;	operations necessary to close this port.  (This does not include
;	controlling the 8259, because other devices may be sharing this
;	interrupt request line.)  The register programming is as follows:
;
;		SI = <offset  of control structure>
;		DS = <segment of control structure>
;
;	This routine returns with interrupts disabled.
;
;	One additional interrupt is triggered when the MCR is reloaded.
;	It will occur when interrupts are reenabled, unless masked by
;	the 8259.
;
;==============================================================================
reset_port	proc	near

		;
		;	Disable interrupts
		;
		cli
		mov	[si].com_port_status,0	;clear status word
		mov	dx,[si].com_dev_addr	;get addr of device registers
		add	dx,com_int_enab 	;get addr of interrupt enab reg
		xor	al,al			;set to turn off all interrupts
		out	dx,al			;disable interrupts

		;
		;	Turn off modem control signals.
		;
		mov	ah,[si].com_lf_hw_closed
		call	load_mcr

		ret
reset_port	endp

;==============================================================================
;
;	This marks the end of the support procedures.  The following
;	procedures are called directly from COM_ENTRY.
;
;==============================================================================

subttl		init_data
page
;==============================================================================
;
;	Initialize internal data
;
;==============================================================================
init_data	proc	near

		cld				;Fill COM_DEF structures
		mov	ax,ds			;with zeros.
		mov	es,ax
		xor	ax,ax
		assume	es:seg com_ctrl@
		lea	di,com_ctrl@
		mov	cx,size com_ctrl@
	rep	stosb

						;AX is still zero.

		lea	si,com_ctrl@		;Fill in nonzero entries
init_data_1:					;in COM_DEF structures.
		mov	bx,ax
		inc	ax			;AX = port number (1..4),
						; BX = AX - 1.
		mov	[si].com_port_numb,ax	;(Port I/O address will
		mov	[si].com_break_time,250 ; be supplied when port is
						; opened.)
		shl	bx,1			;BX = index into INT_ENTRY
		mov	di,int_entry[bx]
		mov	word ptr [si].com_handler  ,di
		mov	word ptr [si].com_handler+2,cs
		add	si,type com_ctrl@	;SI -> next COM_DEF structure
		cmp	ax,max_com_port
		jb	init_data_1

		mov	current_status,0
		mov	machine_known,0
		mov	machine_save,0

		ret				;Successful completion.
init_data	endp
		assume	es:nothing

subttl		open_port_0
page
;==============================================================================
;
;	Open the specified communications port. The register programming is
;	as follows:
;
;		AH = <port number>
;		BX = <input queue size>
;		CX = <output queue size>
;		DX = <interrupt level> & <I/O port address>
;		DS:SI = <I/O queue buffer>
;
;	This function (OPEN_PORT_0) has been superceded by the new
;	OPEN_PORT routine.  OPEN_PORT_0 is supplied for use by older
;	applications.  Use OPEN_PORT for all new applications.
;
;==============================================================================
open_port_0	proc	near

		;
		;	Save registers that the user doesn't expect to
		;	change.
		;
		push	[bp].p_dx
		push	[bp].p_di
		push	[bp].p_es

		;
		;	Convert registers to new conventions used by
		;	OPEN_PORT.
		;
		mov	di,[bp].p_dx		;Copy port address into DI.
		and	di,0fffh
		mov	[bp].p_di,di

						;Copy IRQ line into DL,
						; 0 into DH.
		mov	cl,12			;load shift counter
		shr	[bp].p_dx,cl		;isolate IRQ bits

		mov	[bp].p_es,0		;OPEN_PORT may use
						;nonzero ES values for
						;future purposes.

		call	open_port

		;
		;	Restore registers that shouldn't change.
		;
		pop	[bp].p_es
		pop	[bp].p_di
		pop	[bp].p_dx

		ret
open_port_0	endp

subttl		open_port
page
;==============================================================================
;
;	Open the specified communications port. The register programming is
;	as follows:
;
;		AH = <port number>
;		BX = <input queue size>
;		CX = <output queue size>
;		DH = 0		  means enable both DTR and RTS;
;		     1		  means leave DTR and RTS unchanged;
;		     Other values are reserved for future use.
;		DL = <interrupt level>
;		DI = <I/O port address>
;		DS:SI = <I/O queue buffer>
;		ES = 0	    (Nonzero values are reserved for future use.)
;
;==============================================================================
open_port	proc	near

		;
		;	Check for unsupported DH and ES values.
		;
		test	byte ptr [bp].p_dx[1],not open_dh_options ; Check DH.
		jnz	ocp_bad_new_parm
		cmp	[bp].p_es,0		;Check ES.
		je	ocp_ok_new_parm

ocp_bad_new_parm:
		mov	return_code,err_bad_parm
		ret

ocp_ok_new_parm:

		;
		;	Check range of port number in AL.
		;
		call	validate_port		;sets err_bad_port if error
		jnc	ocp_0
		ret

ocp_0:		;
		;	AL is valid - see if port is already opened
		;
		test	[si].com_port_status,com_port_active ;already open?
		jz	ocp_1			;skip if not active
		mov	return_code,err_port_open
		ret				;exit

ocp_1:		;
		;	Check for existence of device
		;
		mov	dx,[bp].p_di		;get I/O port parameter
		or	dx,dx			;check for zero
		jnz	ocp_2			;skip if not zero
		mov	ax,[si].com_port_numb	;get port number
		call	get_port_addr		;get I/O port to DX
		or	dx,dx			;check for no port
		jnz	ocp_2			;skip if port was found
		mov	return_code,err_no_addr ;set error code for return
		ret

ocp_2:		;
		;	Register DX contains what we hope is the I/O address
		;	of one of the communications ports.  Make sure that
		;	it  exists.
		;
		call	check_com_port		;make sure port exists
		jnc	ocp_3			;skip if it exists
		mov	return_code,err_no_port ;set error code
		ret

ocp_3:		;
		;	Establish interrupt request level for this port
		;
		mov	[si].com_dev_addr,dx	;store device address
		mov	al,byte ptr [bp].p_dx	;Get IRQ parameter
		or	al,al			;check for zero
		jnz	ocp_4			;skip if not zero

		;
		;	Interrupt request level was specified as zero,
		;	so look up port address in the standard_addr table
		;	and use corresponding standard IRQ level from the
		;	standard_irq table.
		;
		xor	bx,bx			;Beginning of table.
ocp_35:
		cmp	bx,max_standard_addr*(type standard_addr)
		jae	ocp_37			;Quit if beyond end of table.
		cmp	dx,standard_addr[bx]
		je	ocp_36			;The address matches.
		inc	bx			;Step to next table entry.
		inc	bx
		jmp	ocp_35

ocp_36:
		mov	al,byte ptr standard_irq[bx] ;Load standard IRQ level.
		jmp	short ocp_4

ocp_37: 	;
		;	If the port isn't at a known address, then the
		;	IRQ parm must be specified and can't be zero.
		;
		mov	return_code,err_no_irq
		ret

ocp_4:		;
		;	The interrupt request level is in AL.  Calculate
		;	the mask values to use to enable and disable interrupts
		;	on this level.
		;
		;	To enable interrupts, the bit which corresponds to
		;	the interrupt level must be turned off in the 8259.
		;	For example, to enable IRQ4, turn off bit 4.
		;	To disable interrupts, turn the bit back on.
		;
		mov	cl,al			;copy IRQ to shift register
		xor	ah,ah			;clear AH
		add	al,8			;I/O interrupts start with 8
		mov	[si].com_irq_level,ax	;save interrupt request level
		mov	al,1			;set up mask
		shl	al,cl			;shift mask to correct bit
		mov	[si].com_irq_disable,al ;save DISABLE mask
		not	al			;reverse the bits
		mov	[si].com_irq_enable,al	;save ENABLE mask

		;
		;	Get input and output buffer sizes and addresses
		;
		mov	cx,[bp].p_cx		;get output queue size
		mov	bx,[bp].p_bx		;get input queue size
		mov	di,[bp].p_si		;get offset address of buffer
		mov	es,[bp].p_ds		;get segment address of buffer
		cmp	cx,1			;bad output buffer size?
		jbe	ocp_5			;skip if bad
		cmp	bx,1			;bad input buffer size?
		jbe	ocp_5			;skip if bad
		mov	ax,es			;copy ES to AX
		or	ax,ax			;bad buffer segment?
		jnz	ocp_6			;skip if OK

ocp_5:		;
		;	bad parameter value
		;
		mov	return_code,err_bad_buf
		ret

ocp_6:		;
		;	Clear table variables
		;
		mov	[si].com_data_lost,0	;clear lost character count
		mov	[si].com_iq_count,0	;clear character counts for
		mov	[si].com_oq_count,0	; both queues.

		;
		;	Setup input queue pointers
		;
		mov	[si].com_q_seg,es	;store segment addr of buffer
		mov	[si].com_iq_size,bx	;store size of input queue
		mov	[si].com_iq_beg,di	;store addr of begining of que
		mov	[si].com_iq_head,di	;store addr of queue head
		mov	[si].com_iq_tail,di	;store addr of queue tail
		add	di,bx			;point to end of queue
		jc	ocp_5			;Input queue too big.
		dec	di			;back up one byte
		mov	[si].com_iq_end,di	;store addr of end of que

		;
		;	Setup output queue pointers
		;
		inc	di			;point to next memory location
		mov	[si].com_oq_size,cx	;store size of output queue
		mov	[si].com_oq_beg,di	;store addr of begining of que
		mov	[si].com_oq_head,di	;store addr of queue head
		mov	[si].com_oq_tail,di	;store addr of queue tail
		dec	cx			;Size of queue minus 1.
		add	di,cx			;One byte before end of queue.
		jc	ocp_5			;Output queue too big.
		mov	[si].com_oq_end,di	;store addr of end of queue

		;
		;	Calculate size of input triggers: 50%, 75%, 90%
		;
		mov	ax,bx			;copy input queue size to AX
		shr	ax,1			;divide by 2 to get 50% level
		mov	[si].com_iq_trig_1,ax	;save 1st trigger
		shr	ax,1			;divide by 2 again to get 25%
		add	ax,[si].com_iq_trig_1	;25% + 50% = 75%
		mov	[si].com_iq_trig_2,ax	;save 2nd trigger
		mov	ax,bx			;restore original queue size
		xor	dx,dx			;make ax into double word
		mov	cx,10			;divide input queue size by 10
		div	cx
		sub	bx,ax			;100% - 10% = 90%
		mov	[si].com_iq_trig_3,bx	;save 3rd trigger
		xor	bx,bx			;clear index register

		;
		;	Establish the initial PORT_STATUS and PORT_ERROR
		;	values
		;
		mov	[si].com_port_status,com_init_state
		mov	[si].com_port_error,0

		;
		;	Initialize hardware local flow control values
		;
		mov	[si].com_lf_hw_mask,   def_lf_hw_mask
		mov	[si].com_lf_hw_closed, def_lf_hw_closed
		mov	[si].com_lf_hw_unready,def_lf_hw_unready
		mov	[si].com_lf_hw_ready,  def_lf_hw_ready

		;
		;	Check machine model for interrupt sharing.
		;	(Do it now, before we enter the critical section,
		;	because GET_MACHINE may enable interrupts.)
		;
		call	get_machine
		mov	cx,ax			;Save model information in CX.

		;
		;	Disable interrupts.
		;
		cli

		;
		;	Check former interrupt handler
		;
		mov	dx,[si].com_irq_level
		call	getinterrupt		;Returns BX:AX
		mov	di,ax
		mov	dx,bx			;DX:DI is former vector
		mov	bx,word ptr [si].com_handler
		mov	word ptr cs:[bx].intch_fptr  ,di    ;Save old vector
		mov	word ptr cs:[bx].intch_fptr+2,dx    ;in sharing struct

		;
		;	Set up interrupt sharing if necessary
		;
		test	cx,machine_ps2
		jnz	ocp_share		;Share only on PS/2.

		mov	[si].com_rearm_addr,0	;No global rearm port.
		jmp	short ocp_first 	;Nobody to share with.


ocp_share:
		mov	ax,[si].com_irq_level	;Rearm port is
		add	ax,02f0h - 8		; 2f0h + (IRQ line number).
		mov	[si].com_rearm_addr,ax

						;DX:DI is former vector.
		mov	es,dx
		cmp	byte ptr es:[di],0cfh
		je	ocp_first		;Previous handler is IRET.

		cmp	es:[di].intch_sign,sharing_signature
		je	ocp_notfirst		;Previous handler is a
						; sharing handler.

		cmp	dx,0f000h		;Compare previous handler
		jne	ocp_notfirst		; address against PS/2 dummy
		cmp	di,es:[0ff01h]		; handler.
		jne	ocp_notfirst
						;Fall through if match.

ocp_first:
		mov	al,intch_first		    ;This is first handler
		jmp	short ocp_set_share_flags   ; installed.

ocp_notfirst:
		xor	al,al			    ;Former handler found.

ocp_set_share_flags:
		mov	bx,word ptr [si].com_handler  ;Install correct value
		mov	cs:[bx].intch_flags,al	      ; of "first" flag.

		;
		;	Establish the Interrupt Vector
		;
		mov	ax,word ptr [si].com_handler
		mov	bx,cs			;BX:AX is new vector
		mov	dx,[si].com_irq_level
		call	setinterrupt		;Install new vector

		;
		;	Turn on modem control signals.
		;
		and	[si].com_lf_state,not (com_unready or com_mcr_ctrl)
		test	byte ptr [bp].p_dx[1],open_no_mcr_ctrl
		jnz	ocp_mcr_done
		mov	ah,[si].com_lf_hw_ready
		call	load_mcr
ocp_mcr_done:

		;
		;	Tell 8259 PIC to allow interrupts
		;	(see page A-156 of IAPX 86,88 User's Manual for
		;	description of the Operational Command Word).
		;
		mov	ah,[si].com_irq_enable	;Load OCW1 enable value
		mov	dx,inta01		; address to r/w OCW1 value
		in	al,dx			;Read current OCW1 value
		jmp	short $+2		;Give port time to process
		and	al,ah			;Enable desired irq level
		out	dx,al			;Write new OCW1 value

		;
		;	Clear miscellaneous residue in the port.
		;
		mov	dx,[si].com_dev_addr
		add	dx,com_line_ctl 	;Point at line control reg.
		in	al,dx
		jmp	short $+2		;Give port time to process
		and	al,not lcr_dlab 	;Clear DLAB bit only.
		out	dx,al
		jmp	short $+2		;Give port time to process

		sub	dx,com_line_ctl-com_data ;Point at data register.
		in	al,dx			;Read & discard waiting data.
		jmp	short $+2		;Give port time to process

		add	dx,com_modem_status-com_data ;Point at MSR.
		in	al,dx			;Read & discard MSR value.
		jmp	short $+2		;Give port time to process

		;
		;	Enable interrupts on the com port.
		;
		mov	dx,[si].com_dev_addr
		add	dx,com_line_status
ocp_wait_thre:
		in	al,dx
		jmp	short $+2		;Give port time to process
		test	al,lsr_thre		;Check trans holding register.
		jz	ocp_wait_thre		;Wait if THR not empty.

		sub	dx,com_line_status-com_int_enab
		mov	al,0			;Disable all four interrupts.
		out	dx,al
		jmp	short $+2		;Give port time to process
		mov	al,ier_read+ier_error+ier_status+ier_write
		out	dx,al			;enable interrupts
		jmp	short $+2		;Give port time to process
		out	dx,al			;enable interrupts
		jmp	short $+2		;Give port time to process

		;
		;	Initialize simulated MSR value and enable interrupts.
		;
		mov	cl,READ_MSR_CLEAR
		call	read_msr

		;
		;	Reset the 8259
		;
		mov	al,eoi
		out	inta00,al

		;
		;	Save identity of current port and exit
		;
		mov	ax,[si].com_port_numb
		mov	current_port,al
		ret
open_port	endp

subttl		close_port
page
;==============================================================================
;
;	Close the specified communications port. The register programming is
;	as follows:
;
;		AH = <port number>
;
;==============================================================================
close_port	proc	near
		call	check_port
		jnc	ccp_1
		ret

ccp_1:		;
		;	If the output queue is empty, make sure the last
		;	character (it may be in the 8250) is transmitted.
		;
		cmp	[si].com_oq_count,0	;Is queue empty?
		jnz	ccp_1b			;Queue not empty

		mov	dx,[si].com_dev_addr	;Address of device registers
		add	dx,com_line_status	;Line status register addr

ccp_1a: 					;Loop until the trans shift
		in	al,dx			;and holding registers are
						;empty.
		jmp	short $+2		;Give port time to process
		and	al,LSR_THRE or LSR_TSRE
		cmp	al,LSR_THRE or LSR_TSRE
		jne	ccp_1a
ccp_1b:

		;
		;	Disable interrupts and reset the board.
		;
		call	reset_port		;Returns with interrupts
						; disabled.

		;
		;	Check whether we are the interrupt handler
		;	pointed to by the vector, or whether we are
		;	somewhere else in the chain.
		;
		mov	dx,[si].com_irq_level
		call	getinterrupt
		mov	cx,bx			;CX:AX is current handler.
		mov	di,word ptr [si].com_handler
		mov	dx,word ptr [si].com_handler+2	;DX:DI is we.
		cmp	ax,di
		jne	ccp_check_next
		cmp	cx,dx
		je	ccp_7			;We're the one pointed to,
						; so restore original
						; vector and perhaps reset
						; 8259 mask.

ccp_check_next: 				;Check next handler in chain.
		mov	es,cx
		mov	bx,ax			;ES:BX is current handler
		;
		;	Check whether this handler begins with an
		;	interrupt sharing structure and whether it points
		;	to us.
		;
		cmp	es:[bx].intch_sign,sharing_signature
		jne	ccp_unknown
		mov	ax,word ptr es:[bx].intch_fptr
		mov	cx,word ptr es:[bx].intch_fptr+2 ;CX:AX is next handler
		cmp	ax,di				 ; -- is it we?
		jne	ccp_check_next
		cmp	cx,dx
		jne	ccp_check_next

		;
		;	Unlink our entry point structure from the
		;	middle of the chain.
		;
		;	The handler points to us, so copy our
		;	pointer-to-the-next-handler to the handler
		;	that points to us.
		;
		mov	ax,word ptr cs:[di].intch_fptr
		mov	cx,word ptr cs:[di].intch_fptr+2
		mov	word ptr es:[bx].intch_fptr  ,ax
		mov	word ptr es:[bx].intch_fptr+2,cx

		;
		;	Copy the "first" flag from our sharing structure
		;	to the handler that pointed to us.
		;
		mov	al,cs:[di].intch_flags
		and	al,intch_first		;Isolate FIRST flag.
		or	es:[bx].intch_flags,al
		jmp	short ccp_9

ccp_unknown:	;
		;	We're unable to find our entry point in the
		;	chain of linked handlers, so we can't unlink.
		;
		mov	return_code,err_bad_parm
		jmp	short ccp_9

ccp_7:		;
		;	The interrupt vector points directly to us.
		;	See whether we point to any other handlers.
		;
		test	cs:[di].intch_flags,intch_first
		jz	ccp_8			;If zero, other handlers
						; exist, so don't touch 8259.

		;
		;	Tell 8259 PIC to mask interrupts
		;
		mov	dx,inta01		;OCW1 VALUE ADDRESS
		in	al,dx
		jmp	short $+2		;Give port time to process
		or	al,[si].com_irq_disable
		out	dx,al

ccp_8:
		;
		;	Restore original interrupt vector values
		;
		mov	ax,word ptr cs:[di].intch_fptr
		mov	bx,word ptr cs:[di].intch_fptr+2
		mov	dx,[si].com_irq_level
		call	setinterrupt

ccp_9:
		;
		;	clear port definition
		;
		mov	[si].com_port_status,0	;CLEAR STATUS WORD
		mov	[si].com_port_error,0	;CLEAR ERROR FLAGS
		sti
		ret
close_port	endp

subttl		write_port
page
;==============================================================================
;
;	Write character(s) to the specified communications port. The register
;	programming is as follows:
;
;		AH = <port number>
;		BL = <character>
;		BH = <flags>
;		CX = <write count>
;		DS:SI = address of <character string>
;
;	On output:
;
;		BX = <number of characters remaining in output queue>
;		CX = <count written>
;
;	The <flags> mask is interpreted as follows:
;
;		mask	meaning
;		01h	Interpret <character> as a single character to be
;			transmitted to the specified com port. Ignore the
;			<write count> and <character string> parameters.
;
;		02h	Use the value of <character> to determine when to
;			stop transferring characters to the output queue.
;			If this flag bit is one, then each character taken
;			from the the string will be compared with <character>
;			and the transfer to the output queue will end when
;			the character from the buffer matches <character>
;			or when the number of characters <write count> is
;			exhausted.
;
;==============================================================================
write_port	proc	near
		call	check_port
		jnc	wcp_10
		ret

wcp_10: 	;
		;	Initialize COUNT_WRITTEN returned parameter to zero
		;
		mov	[bp].p_ax,0		;clear temporary count written
		mov	ax,[bp].p_bx		;get flag byte and char to AX
		test	ah,1			;check for 1 char write
		jnz	wcp_40			;skip unless write string

wcp_20: 	;
		;	Check for exhausted write count
		;
		mov	cx,[bp].p_cx		;get count word
		jcxz	wcp_30			;skip if count exhausted

		;
		;	put the byte to be written into AL and update and
		;	store the string pointer and write counter
		;
		mov	es,[bp].p_ds		;get pointer to segment
		mov	di,[bp].p_si		;  and offset of string
		mov	al,es:[di]
		inc	di
		dec	cx
		mov	[bp].p_si,di
		mov	[bp].p_cx,cx
		mov	bx,[bp].p_bx		;get flag byte and char to BX
		test	bh,2			;check to see if we look for
		jz	wcp_40			;a terminator.	No..go on
		cmp	al,bl
		je	wcp_30			;Have a match.
		jmp	short wcp_40

wcp_30: 	;
		;	The write count is exhausted so bail out now...
		;
		mov	ax,[bp].p_ax		;get temporary count written
		mov	[bp].p_cx,ax		;store in correct field
		mov	ax,[si].com_oq_count	;get output queue count
		mov	[bp].p_bx,ax		;and return in BX
		ret

wcp_40: 	;
		;	See if we should force bit 7 to a one
		;
		test	[si].com_port_status,com_write_bit_7
		jz	wcp_50
		or	al,80h			;turn on bit 7
		jmp	short wcp_60		;skip the next test

wcp_50: 	;
		;	See if we should trim bit 7
		;
		test	[si].com_port_status,com_trim_bit_7
		jz	wcp_55
		and	al,7fh			;turn off bit 7
wcp_55:

wcp_60: 	;
		;	Get queue head and tail
		;
		cli
		mov	bx,[si].com_oq_tail	;address of next char
		inc	bx			;point to next byte
		cmp	bx,[si].com_oq_end	;check for wraparound
		jne	wcp_70			;skip if no wraparound
		mov	bx,[si].com_oq_beg	;point to begining of buffer

wcp_70: 	;
		;	Check for output queue full
		;
		cmp	bx,[si].com_oq_head	;see if queue is full
		jne	wcp_80			;skip if room in queue
		sti
		mov	return_code,err_queue_full ;Indicate error, and
		jmp	wcp_30			;set write count and return

wcp_80:
		;
		;	Store AL in the write queue.
		;
		mov	es,[si].com_q_seg	;get segment address
		mov	es:[bx],al		;store character
		mov	[si].com_oq_tail,bx	;update pointer
		inc	[si].com_oq_count	;increase output queue size

		sti				;allow external interrupts

		;
		;	Trigger next output char if necessary.
		;
		call	write_next

		;
		;	Done handling this char, consider continuing.
		;
		mov	ax,[bp].p_bx		;get flag byte and char to AX
		inc	[bp].p_ax		;Increment count written (RAL)
		test	ah,1			;check for 1 char write
		jnz	wcp_90			;skip unless write string
		jmp	wcp_20			;go get next character

wcp_90:
		push	ax
		mov	ax,[si].com_oq_count	;get output queue count, and
		mov	[bp].p_bx,ax		;return in BX
		pop	ax
		ret
write_port	endp

subttl		read_port
page
;==============================================================================
;
;	Read character(s) from the specified communications port. The register
;	programming is as follows:
;
;		AH = <port number>
;		BL = <terminator>
;		BH = <flags>
;		CX = <read count>
;		DX = anything
;		DS:SI = address of <character string>
;
;	On output:
;
;		AH = <character>
;		BX = <queue size>
;		CX = <count read>
;		DX = <port error word>
;
;	The <flags> mask is interpreted as follows:
;
;		mask	meaning
;		01h	Read only a single byte from the input queue and
;			return its value in <character>. Ignore the values
;			of <read count> and <character string>.
;
;		02h	Use the value of <terminator> to determine when to
;			stop transferring characters from the input queue.
;			If this flag bit is on, then each character taken
;			from the input queue will be compared with <terminator>
;			and the transfer will terminate when the character
;			from the queue matches <terminator> or when the number
;			of characters matches <read count>, whichever happens
;			first.
;
;	NOTE: flag mask 01h takes precedence over other flags.
;
;	NOTE: if flag mask 01h is not specified and <read count> = 0, then
;	no characters will be taken from the queue.  The number of characters
;	in the queue will be returned in <queue size>.
;
;==============================================================================
read_port	proc	near
		call	check_port
		jnc	rcp_05
		jmp	rcp_end

rcp_05: 	;
		;	Check to see if the queue is empty and a request
		;	to read has been made.	If so return an error.
		;	Notice that we do not report an error if the queue
		;	is empty, and just a request for the queue size is
		;	made.
		;
		cli
		mov	bx,[si].com_iq_head
		mov	ax,[si].com_iq_tail
		sti
		cmp	bx,ax
		jne	rcp_10			; queue is not empty
		mov	cx,[bp].p_cx		; see if read request
		jcxz	rcp_052 		; No..checking queue size?
rcp_051:	mov	return_code,err_que_empty  ; Read request. Report
						; error, initialize and
		mov	[bp].p_ax,0		; exit.
		jmp	rcp_exit
rcp_052:	mov	bx,[bp].p_bx		; If not reading a char,
		test	bh,1			; it is OK for count to be 0.
		jz	rcp_10
		jmp	rcp_051 		; Reading a character...error

rcp_10: 	;
		;	Check FLAGS parameter to see what kind of read we
		;	are performing.
		;
		mov	[bp].p_ax,0		;clear temporary COUNT READ
		mov	bx,[bp].p_bx		;get FLAGS and TERMINATOR
		test	bh,1			;if on, read only one char
		jz	rcp_20			;skip if not on
		mov	[bp].p_cx,1		;set read count to 1
		jmp	short rcp_30

rcp_20: 	;
		;	Check for READ COUNT = 0 - this would imply that
		;	we should only return the number of characters in
		;	the queue without removing anything from the queue.
		;
		cmp	[bp].p_cx,0
		jne	rcp_30
		jmp	rcp_exit

rcp_30: 	;
		;	See if anything is in the input queue
		;
		cli				;turn off interrupts
		mov	bx,[si].com_iq_head	;address of front of queue
		mov	ax,[si].com_iq_tail	;address of end of queue
		cmp	bx,ax			;head = tail ==> queue empty
		jne	rcp_40			;skip if queue isn't empty
		cmp	[si].com_iq_count,0	;double check - s/b 0
		sti				;Allow interrupts
		jne	rcp_err
		jmp	rcp_exit

rcp_err:	;
		;	We have an error condition here... The input queue
		;	count is either negative or is not zero when it
		;	should be.  Set error flag and bail out.
		;
		or	[si].com_port_error,com_error_neg
		mov	[si].com_iq_count,0
		mov	return_code,err_bad_code
		jmp	rcp_exit

rcp_40: 	;
		;	Get buffer offset of next byte in the input queue
		;
		inc	bx			;POINT TO NEXT BYTE
		cmp	bx,[si].com_iq_end	;CHECK FOR WRAP_AROUND
		jne	rcp_50			;SKIP UNLESS BUFFER WRAPPED
		mov	bx,[si].com_iq_beg	;POINT TO BEGINING OF BUFFER

rcp_50: 	;
		;	Update queue header pointer
		;
		mov	[si].com_iq_head,bx	;store new queue header pointer

		;
		;	Put next character in input queue into AH
		;
		mov	ax,[si].com_q_seg	;LOAD BUFFER SEGMENT ADDRESS
		mov	es,ax			;SET UP ADDRESS OF
		mov	ah,es:[bx]		;GET NEXT BYTE FROM INPUT QUEUE

		;
		;	Decrement count of characters in the input queue.
		;	Make sure that character count doesn't go negative.
		;
		sub	[si].com_iq_count,1	;Decrement count
		sti				;Allow interrupts
		jb	rcp_err 		;Jump if negative
		jnz	rcp_60			;Ignore if not zero

		;
		;	The input buffer count has gone to zero.  See if we
		;	have transmitted and XOFF; if so, then send
		;	an XON.
		;

		test	[si].com_port_status,com_xoff_sent ;HAVE WE SENT XOFF?
		jz	rcp_60			;skip unless XOFF was sent

		;
		;	Set the flag that tells the write interrupt procedure
		;	to output an XON.
		;

		test	[si].com_port_status,com_xon_sent   ;XON already sent?
		jnz	rcp_60			;XON has already been sent
		or	[si].com_port_status,com_send_xon   ;Set flag to send
		call	write_next			    ;XON

		;
		;	Turn off the "XOFF SENT" flag.
		;
		and	[si].com_port_status,all-com_xoff_sent
		or	[si].com_port_status,com_error_found
		and	[si].com_port_error,all-com_stat_l_off
		or	[si].com_port_error,com_stat_l_on

rcp_60:
		;
		;	Handle local hardware flow control
		;
		cmp	[si].com_iq_count,0	;Input queue empty?
		jne	rcp_65			;If not, skip this.
		and	[si].com_lf_state,not com_unready
		push	ax			;Save character in AH.
		mov	ah,[si].com_lf_hw_ready
		call	control_mcr
		pop	ax			;Restore character to AH.
rcp_65:

		;
		;	See if we should set bit 7 to a zero (some machines
		;	always send bit 7 as a 1).
		;
		test	[si].com_port_status,com_trim_bit_7
		jz	rcp_70
		and	ah,07Fh 		;turn off bit 7

rcp_70: 	;
		;	Now check to see if we are reading a single character
		;	or a character string.
		;
		mov	bx,[bp].p_bx
		test	bh,1			;check for 1 character read
		jz	rcp_80			;skip if reading string
		mov	[bp].p_ax,ax		;store character in AX (AH)
		mov	bx,[si].com_iq_count	;get input queue count
		mov	[bp].p_bx,bx		;store queue size in BX
		mov	[bp].p_cx,1		;set COUNT READ to 1
		xor	ax,ax			;New PORT ERROR word := 0.
		xchg	ax,[si].com_port_error	;Get old PORT ERROR word.
		mov	[bp].p_dx,ax		;save PORT ERROR in DX
		ret

rcp_80: 	;
		;	We are reading a string rather than a single char.
		;	See if we are looking for a specific terminator.
		;
		test	bh,2			;check for TERMINATOR flag
		jz	rcp_90			;skip if no terminator
		cmp	ah,bl			;byte read = terminator?
		je	rcp_exit		;exit on a match

rcp_90: 	;
		;	store the value of AH in the target string and bump
		;	the COUNT READ counter.
		;
		inc	[bp].p_ax		;bump COUNT READ
		mov	es,[bp].p_ds		;get segment of string
		mov	di,[bp].p_si		;get offset of string
		mov	al,ah			;copy character to AL
		stosb				;store byte in buffer
		dec	[bp].p_cx		;decrement count
		jz	rcp_exit		;exit if count = 0
		inc	[bp].p_si		;increment string pointer
		jmp	rcp_30

rcp_exit:	;
		;	Load registers with return values
		;
		mov	ax,[si].com_iq_count	;get input queue count
		mov	[bp].p_bx,ax		;store in BX
		mov	ax,[bp].p_ax		;get temporary COUNT READ
		mov	[bp].p_cx,ax		;store in CX
		xor	ax,ax			;New PORT ERROR word := 0.
		xchg	ax,[si].com_port_error	;Get old PORT ERROR word.
		mov	[bp].p_dx,ax		;save PORT ERROR in DX
rcp_end:
		ret
read_port	endp

subttl		set_option
page
;==============================================================================
;
;	Set communications options. The register programming is as follows:
;
;		AH = <port number>
;		BL = <option code>
;		BH = <parameter>
;
;	The <option code> values are as follows:
;
;		1 - set baud rate
;		2 - set parity
;		3 - set data bits
;		4 - set stop bits
;		5 - remote flow control
;		6 - local flow control
;		7 - bit 7 trimming
;		8 - bit 7 forcing
;		9 - CTS required for transmission
;	       10 - break time
;	       11 - software interrupt vector
;
;	       Local flow control MCR values:
;	       12 - mask
;	       13 - ready   value
;	       14 - unready value
;	       15 - closed  value
;
;==============================================================================
soptn_tbl	label	word
		dw	set_baud		; 1 - set baud rate
		dw	set_parity		; 2 - set parity
		dw	set_data		; 3 - set data bits
		dw	set_stop		; 4 - set stop bits
		dw	set_rem_flow		; 5 - remote flow control
		dw	set_loc_flow		; 6 - local flow control
		dw	set_trimming		; 7 - bit 7 trimming
		dw	set_forcing		; 8 - bit 7 forcing
		dw	set_cts_option		; 9 - set CTS option
		dw	set_break_time		;10 - set break time
		dw	set_int_vector		;11 - set interrupt vector
						;
						;Local flow control MCR values:
		dw	set_lf_hw_mask		;12 - set mask
		dw	set_lf_hw_ready 	;13 - set ready   value
		dw	set_lf_hw_unready	;14 - set unready value
		dw	set_lf_hw_closed	;15 - set closed  value
soptn_tbl_end	equ	$-soptn_tbl

set_option	proc	near
		mov	bx,[bp].p_bx
		mov	parameter,bh
		xor	bh,bh
		dec	bx
		shl	bx,1
		clc
		cmp	bx,soptn_tbl_end
		jc	so_1

		;
		;	invalid function code
		;
		mov	return_code,err_bad_parm
		ret

so_1:		;
		;	Call the appropriate procedure
		;
		mov	al,port_number
		mov	ah,parameter
		call	word ptr cs:[bx + offset soptn_tbl]
		ret
set_option	endp

subttl		get_option
page
;==============================================================================
;
;	Read communications options. The register programming is as follows:
;
;		AH = <port number>
;		BL = <option code>
;
;	On output:
;
;		BH = <option value>
;
;	The <option code> values are defined in goptn_tbl below:
;
;
;==============================================================================
goptn_tbl	label	word
		dw	get_baud		; 1 - get baud rate
		dw	get_parity		; 2 - get parity
		dw	get_data		; 3 - get data bits
		dw	get_stop		; 4 - get stop bits
		dw	get_rem_flow		; 5 - remote flow control
		dw	get_loc_flow		; 6 - local flow control
		dw	get_trimming		; 7 - bit 7 trimming
		dw	get_forcing		; 8 - bit 7 forcing
		dw	get_cts_option		; 9 - get CTS option
		dw	get_break_time		;10 - get break time
		dw	get_int_vector		;11 - get interrupt vector
						;
						;Local flow control MCR values:
		dw	get_lf_hw_mask		;12 - get mask
		dw	get_lf_hw_ready 	;13 - get ready   value
		dw	get_lf_hw_unready	;14 - get unready value
		dw	get_lf_hw_closed	;15 - get closed  value
						;
						;Modem status register values:
		dw	get_msr 		;16 - get actual MSR
		dw	get_msr_sim_clear	;17 - get simulated MSR,
						;	clear delta flags
		dw	get_msr_sim		;18 - get simulated MSR
goptn_tbl_end	equ	$-goptn_tbl

get_option	proc	near
		mov	bx,[bp].p_bx
		mov	parameter,bh
		xor	bh,bh
		dec	bx
		shl	bx,1
		clc
		cmp	bx,goptn_tbl_end
		jc	go_1

		;
		;	invalid function code
		;
		mov	return_code,err_bad_parm
		ret

go_1:		;
		;	Call the appropriate procedure
		;
		mov	al,port_number
		mov	ah,parameter
		call	word ptr cs:[bx + offset goptn_tbl]
		ret
get_option	endp

subttl		spec_function
page
;==============================================================================
;
;	Perform special communications function. The register programming
;	is as follows:
;
;		AH = <port number>
;		BL = <function code>
;
;	The <function code> values are as follows:
;
;		1 - send break
;		2 - drain input queue
;		3 - drain output queue
;		4 - return the device busy flag
;		5 - return port address information
;
;==============================================================================
sfunct_tbl	label	word
		dw	send_break		; 1 - send a break
		dw	drain_iq		; 2 - drain input queue
		dw	drain_oq		; 3 - drain output queue
		dw	critsec 		; 4 - device busy flag
		dw	report_addr		; 5 - report address info
sfunct_tbl_end	equ	$-sfunct_tbl

spec_function	proc	near
		mov	bx,[bp].p_bx
		mov	parameter,bh
		xor	bh,bh
		dec	bx
		shl	bx,1
		clc
		cmp	bx,sfunct_tbl_end
		jc	sf_1

		;
		;	invalid function code
		;
		mov	return_code,err_bad_parm
		ret

sf_1:		;
		;	Call the appropriate procedure
		;
		mov	al,port_number
		mov	ah,parameter
		call	word ptr cs:[bx + offset sfunct_tbl]
		ret
spec_function	endp

subttl		unknown_func
page
;==============================================================================
;
;	Dummy function to hold space in table used by COM_ENTRY.
;	It always returns ERR_BAD_PARM as the result code.
;
;==============================================================================
unknown_func	proc	near
		mov	return_code,err_bad_parm
		ret
unknown_func	endp

subttl		com_entry
page
;==============================================================================
;
;	This procedure is the entry point for calls to COM.
;
;	All calls have the following format:
;
;		al = function
;		ah = port number (if used)
;
;		bx,cx,dx,es,ds are call dependent
;
;	On return, the following is always true:
;
;		al = error code:	0 - operation was successful
;					2 - invalid port number
;					3 - port not opened
;					4 - invalid parameter or function value
;					6 - serial port not found
;					7 - output queue is full
;					9 - port already open
;				       10 - input queue is empty on read
;				      255 - Internal error
;
;		ah = port number
;
;==============================================================================
funct_tbl	label	word
		dw	open_port_0	;0
		dw	close_port	;1
		dw	write_port	;2
		dw	read_port	;3
		dw	set_option	;4
		dw	get_option	;5
		dw	spec_function	;6
		dw	unknown_func	;7 reserved for use by COMGATE.
		dw	unknown_func	;8 reserved for use by COMGATE.
		dw	open_port	;9
funct_tbl_end	equ	$-funct_tbl

com_entry	proc	far
		;
		;	Now lets save the working registers on the user's
		;	stack for the moment until we can switch to the
		;	internal stack
		;
		pushf
		push	es
		push	di
		push	si
		push	ds
		push	ax
		push	bx
		push	cx
		push	dx
		push	bp
		mov	bp,sp

		sub	sp,4			;Allocate space for named
						; local variables

		mov	ax,seg com_ctrl@	;point ds
		mov	ds,ax			;  to data segment
		assume	ds:seg com_ctrl@

		;
		;	Make sure data area has been initialized.
		;
		cmp	data_initialized,0
		jne	entry_0
		call	init_data
		mov	data_initialized,1
entry_0:

		;
		;	Decode the function call and dispatch it.
		;
		mov	ax,[bp].p_ax
		mov	return_code,err_none
		mov	port_number,ah

		;
		;	convert AX into an index into the function table
		;
		xor	ah,ah
		shl	ax,1
		clc
		cmp	ax,funct_tbl_end
		jc	entry_1

		;
		;	invalid function code
		;
		mov	return_code,err_bad_parm
		jmp	short entry_2

entry_1:	;
		;	Call the appropriate procedure
		;
		mov	bx,ax
		call	word ptr cs:[bx + offset funct_tbl]

entry_2:	;
		;
		;
		mov	ax,[bp].p_ax
		mov	al,return_code
		mov	[bp].p_ax,ax

		;
		;	exit to caller
		;
		mov	sp,bp
		pop	bp
		pop	dx
		pop	cx
		pop	bx
		pop	ax
		pop	ds
		pop	si
		pop	di
		pop	es
		popff
		ret

;==============================================================================
;
;	This is the point where a software interrupt will arrive.
;	It begins with a two-byte "short JMP" instruction,
;	followed by the doubleword address of the beginning of the
;	copyright notice and four bytes that are reserved for future use.
;
com_swi:
		jmp	short com_swi_1

		dw	offset copyright,seg copyright
		dd	0			;Reserved for future use.

com_swi_1:
		call	com_entry
		iret

com_entry	endp

subttl		getinterrupt and setinterrupt
page
;==============================================================================
;
;	Get an interrupt vector.  The register programming is:
;
;		DX    = interrupt type (number)
;	Upon return, BX:AX contains the previous interrupt vector.
;	This implementation does not require interrupts to be off.
;
;==============================================================================
getinterrupt	proc	near

		push	ds

		xor	bx,bx
		mov	ds,bx			;DS := 0
		mov	bx,dx
		shl	bx,1
		shl	bx,1			;DS:BX is address of vector

		lds	bx,[bx] 		;DS:BX is vector
		mov	ax,bx
		mov	bx,ds			;BX:AX is vector

		pop	ds
		ret

getinterrupt	endp

;==============================================================================
;
;	Set an interrupt vector.  The register programming is:
;
;		BX:AX = CS:IP for the Interrupt Service Routine
;		DX    = interrupt type (number)
;	Upon return, BX:AX contains the previous interrupt vector.
;	This implementation does not require interrupts to be off.
;
;==============================================================================
setinterrupt	proc	near

		push	ds
		push	di

		xor	di,di
		mov	ds,di			;DS := 0
		mov	di,dx
		shl	di,1
		shl	di,1			;DS:DI is address of vector.

		pushf				;Save interrupt flag.
		cli				;Disable interrupts.
		xchg	ax,[di] 		;Exchange BX:AX with vector.
		xchg	bx,[di+2]
		popff				;Possibly reenable interrupts.

		pop	di
		pop	ds
		ret

setinterrupt	endp

subttl		delay
page
;==============================================================================
;
;	Delay for DX milliseconds.  The register programming is:
;
;		DX = Delay time in milliseconds
;
;	The inner loop creates a delay for one millisecond; it is executed
;	DX times.  The overhead for the loop instructions is approximately
;	10% on the PC and XT. For most reasonable values  for the Break signal
;	(it is on the only routine calling delay), this error is acceptable.
;	For the AT, we call the BIOS routine which delays the appropriate
;	number of microseconds (1000 times dx).  The location ffff:000e
;	is checked to determine if the AT is used (it is if the value is
;	0fc).
;
;==============================================================================
delay	proc	near
	push	ax
	push	bx
	push	cx
	push	dx

	; First check to see if the AT is used.  If it is, use BIOS call
	; 15h, subfunction 86h.  See the AT Technical Reference Manual.
	; To determine if the AT is used, we check for the presence of the
	; 80286 processor.  The 286 pushes the current sp, while 86/88
	; processors push the new stack pointer.  IBM does let you query
	; a storage location for the machine type, but the "clones" do
	; not initialize the storage location properly; hence we use this
	; technique

	mov	ax,sp
	push	sp
	pop	bx
	cmp	ax,bx
	jne	xtpc_delay		; Use the XT/PC code

	; Make a call to the AT BIOS

	mov	ax,dx
	mov	bx,1000
	mul	bx
	mov	cx,dx
	mov	dx,ax
	mov	ax,8600h
	int	15h
	jmp	short delay_ret

xtpc_delay:

	mov	cx,dx

	; Now program channel two in mode 1.  We load the latch with 1190
	; set the input gate high and wait until the output goes high.	This
	; takes approximately 1 millisecond.  We loop through CX times.

	mov	ax,00b2h
	out	tim_cmdreg,al
	mov	ax,1190
	out	tim_ch2,al
	jmp	short $+2		;Give port time to process
	mov	al,ah
	out	tim_ch2,al

again:
	in	al,tim_gate
	jmp	short $+2		; Give port time to process
	or	al,1
	out	tim_gate,al		; Set the gate high to start count
waithi:
	in	al,tim_out
	test	al,20h			; Is it high yet?
	jz	waithi
	in	al,tim_gate		; Setting the gate low, resets the
	jmp	short $+2		; Give port time to process
	and	al,0feh 		; latch to the count value
	out	tim_gate,al
	loop	again

delay_ret:
	pop	dx
	pop	cx
	pop	bx
	pop	ax
	ret
delay	endp

subttl		get_machine
page
;==============================================================================
;
;	Return machine model information.   Returned information:
;
;		AX = Bit values:
;			Bit 0 (value 1):  Set if PS/2.
;		All other registers unchanged.
;
;	Beware:  BIOS interrupt 15h enables interrupts!
;
;==============================================================================
get_machine proc    near

	cmp	machine_known,0 	;Check if we checked hardware already.
	je	get_machine_go

	mov	ax,machine_save 	;Return saved information.
	jmp	short get_machine_exit

get_machine_go: 			;Query actual machine information.
	push	es
	push	bx

	mov	ax,0ffffh		;Check model byte.
	mov	es,ax
	mov	al,byte ptr es:0eh
	cmp	al,model_ps2_50
	ja	machine_not_ps2 	;Older model code.
	cmp	al,0f0h
	jb	machine_not_ps2 	;Definitely an unknown model code.

					;If model code is at least 0f0h,
					;take a chance on INT 15h.
	push	ax
	stc
	mov	ah,0c0h 		;Return system configuration
					;parameters.
	int	15h			;System services.
	pop	ax
	jc	machine_not_ps2 	;Older BIOS, hence not a PS/2.

	cmp	al,es:[bx].machine_model ;Confirm model code.
	jne	machine_not_ps2 	;Model code doesn't match.

	test	es:[bx].machine_feature1,feat1_mca
	jnz	machine_ps2_found	;Micro Channel found.

					;Micro Channel not present.
	cmp	al,model_ps2_30 	;Check if PS/2 model 25 or 30.
	jne	machine_not_ps2
					;Fall through if model 25 or 30.

machine_ps2_found:
	mov	ax,machine_ps2		;This is a PS/2.
	jmp	short get_machine_save
machine_not_ps2:
	xor	ax,ax

get_machine_save:
	mov	machine_save,ax 	;Record this information for next call
	mov	machine_known,1
	pop	bx
	pop	es

get_machine_exit:
	ret

get_machine endp

endcode label	far
	endCSeg comcode

	endMod	asynch

		end
