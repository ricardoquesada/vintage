/************************************/
/*      modulo principal            */
/*                                  */
/*                                  */
/*                                  */
/*                                  */
/*                                  */
/************************************/

#include <conio.h>
#include <asynch_1.h>
#include <asynch_h.h>
#include <stdlib.h>
#include "archivos.h"
#include "texto.h"
#include "menu2.h"

#define XMODEM 1
#define YMODEM 2
#define ZMODEM 3

#define ABOUTX 3
#define MODX ABOUTX+3
#define LLAMAX MODX+11
#define ARCHX LLAMAX+12
#define COMAX ARCHX+12
#define LINEAY 3

extern int abrir( void );
extern int cerrar( void );
extern int leercfg( void );
extern int inic_modem( void );
extern int isansi( void );
extern int error( int );
extern int mandarcmd( char *);
extern int llamar( char *);

MODEM **modem;
int modemproto;
int com;
int ansi;

/******************************/
/*   report( int )           */
/*****************************/
void report( int value )
{

    switch( value ){
	case 0: centrar("Todo bien!",25);
		break;
	default: centrar("Todo mal: Error ",25);
		 cprintf("%d",value);
		 break;
    }
    gotoxy(1,25);
    getch();

    return;
}

/****************************************************/
/*            baudrate                              */
/****************************************************/
int baudrate( void )
{
/* variables locales */
    int choice;
    int value;

    retop_a1( com , 1 ,&choice);
    choice++;

    while(1) {
	clearmenugets();

	caja(19,4,26,15);

	prompt(20,5,"110","");
	prompt(20,6,"150","");
	prompt(20,7,"300","");
	prompt(20,8,"600","");
	prompt(20,9,"1200","");
	prompt(20,10,"2400","");
	prompt(20,11,"4800","");
	prompt(20,12,"9600","");
	prompt(20,13,"14400","");
	prompt(20,14,"19200","");

	choice = menu(choice);

	switch( choice ) {
	    case 0:return -1;
	default: value = setop_a1( com , 1, choice-1);
	     report( value );
	     return 0;

	}
   }
}

/****************************************************/
/*            paridad                               */
/****************************************************/
int paridad( void )
{
/* variables locales */
    int choice;
    int value;

    retop_a1( com , 2 ,&choice);
    choice++;

   while(1) {
	clearmenugets();

	caja(19,4,25,8);

	prompt(20,5,"None","");
	prompt(20,6,"Odd","");
	prompt(20,7,"Even","");

	choice = menu(choice);

	switch( choice ) {
	    case 0:return -1;
    default: value = setop_a1( com , 2 ,choice-1);
	     report( value );
	     return 0;

	}

    }
}

/****************************************************/
/*            databits                              */
/****************************************************/
int databits( void )
{
/* variables locales */
    int choice;
    int value;

    retop_a1( com , 3 ,&choice);
    choice++;

   while(1) {
	clearmenugets();

	caja(19,4,21,9);

	prompt(20,5,"5","");
	prompt(20,6,"6","");
	prompt(20,7,"7","");
	prompt(20,8,"8","");

	choice = menu(choice);

	switch( choice ) {
	    case 0:return -1;
	    default: value = setop_a1( com , 3, choice-1);
		report( value );
		return 0;

	}

    }
}

/****************************************************/
/*            stopbits                              */
/****************************************************/
int stopbits( void )
{
/* variables locales */
    int choice;
    int value;

    retop_a1( com , 4 ,&choice);
    choice++;

   while (1) {
	clearmenugets();

	caja(19,4,21,7);

	prompt(20,5,"1","");
	prompt(20,6,"2","");

	choice = menu(choice);

	switch( choice ) {
	    case 0:return -1;
	default: value = setop_a1( com , 4, choice-1);
	     report( value );
	     return 0;

	}

    }
}
/****************************************************/
/*            remote                                */
/****************************************************/
int remote( void )
{
/* variables locales */
    int choice = 0;
    int value;

    retop_a1( com , 5 ,&choice);
    choice++;

   while(1) {
	clearmenugets();

	caja(19,4,33,7);

	prompt(20,5,"Desactivado","Desactiva el Remote Software Flow Control." );
	prompt(20,6,"Activado","y este lo activa, no?. pero para que sirve, YO NO LO SE.!");

	choice = menu(choice);

	switch( choice ) {
		case 0:return -1;
	default: value = setop_a1( com , 5 ,choice-1);
	     report( value );
	     return 0;

	}

    }
}

/****************************************************/
/*            local                                 */
/****************************************************/
int local( void )
{
/* variables locales */
    int choice = 0;
    int value;

    retop_a1( com , 6 ,&choice);
    choice++;

   while(1) {
	clearmenugets();

	caja(19,4,33,7);

	prompt(20,5,"Desactivado","Desctiva el Local Software Flow control." );
	prompt(20,6,"Activado","Activa el Local Software Flow control.");

	choice = menu(choice);

	switch( choice ) {
		case 0:return -1;
	default: value = setop_a1( com , 6 ,choice-1);
	     report( value );
	     return 0;

	}

    }
}




/************************************************/
/*           protocolo                          */
/************************************************/
int protocolo( void )
{
/* variables locales */
    int choice;
    int valor = 100;

    choice = modemproto;

    do {
	clearmenugets();

    caja(19 ,4 ,26 , 7 );
    prompt( 20, 5 ,"Xmodem","Protocolo de transmision Xmodem.");
    prompt( 20, 6 ,"Ymodem","Protocolo de transmision Ymodem.");

    choice = menu(choice);

	switch( choice ) {
	    case 0: return 0;
	case 1: modemproto = XMODEM;
		valor = 0;
		break;
	case 2: modemproto = YMODEM;
		valor = 0;
		break;
	}

    } while( valor == 100 );

    report( valor );

    return 0;
}

/************************************************/
/*        PREJIFO DE LLAMADA                    */
/************************************************/
int prefijo( void )
{
	/* variables locales */
	char buffi[12];
	char *bptr;

	caja(30,12,50,16);
	centrar("Prefijo de llamada",13);
	centrar((**modem).dial_prefix,14);
	gotoxy(34,15);
	buffi[0] = 8;
	bptr = cgets(buffi);

	if(buffi[1] != 0) {
		strcpy( (**modem).dial_prefix, bptr );
		strcpy( (**modem).dial_prefix, bptr );
	}
	return( 0 );
}
/************************************************/
/*     prefijo de comandos.                     */
/************************************************/
int prefijoat( void )
{
	/* variables locales */
	char buffi[12];
	char *ptr;

	caja(30,12,50,16);
	centrar("Prefijo de comandos",13);
	centrar((**modem).cmd_prefix,14);
	gotoxy(34,15);
	buffi[0] = 8;
	ptr = cgets(buffi);

	if(buffi[1] != 0)
	       strcpy( (**modem).cmd_prefix, ptr );
	return( 0 );
}

/************************************************/
/************************************************/
	       /* configurar */
/************************************************/
/************************************************/
int modemcfg( void )
{
/* variables locales */
    int choice = 0;
    int valor = 100;

    do {
	clearmenugets();

	caja(MODX - 1, LINEAY ,MODX + 20 , LINEAY + 11);
	prompt(MODX, LINEAY + 1 ,"Baudios","Configurar los baudios del modem.");
	prompt(MODX, LINEAY + 2 ,"Paridad","Configurar la paridad del modem");
	prompt(MODX, LINEAY + 3 ,"DataBits","Vos sabes... los data bits.");
	prompt(MODX, LINEAY + 4 ,"StopBits","StopBits.");
	prompt(MODX, LINEAY + 5 ,"Remote","Remote Software Flow Control");
	prompt(MODX, LINEAY + 6 ,"Local","Local Software Flow Control");
	prompt(MODX, LINEAY + 7 ,"Protocolo","Seleccionar protocolo.(xmodem o ymodem)");
	prompt(MODX, LINEAY + 8 ,"Prefijo de llamada","Prefijo de la llamada.(DP,DT...");
	prompt(MODX, LINEAY + 9 ,"Prefijo de comando","Para Hayes compatilbes AT.");
	prompt(MODX, LINEAY + 10,"Grabar","Grabar la informacion en disco.");
	choice = menu(choice);

	switch( choice ) {
	    case 0: return 0;
	    case 1: valor = baudrate();
		break;
	    case 2: valor = paridad();
		break;
	    case 3: valor = databits();
		break;
	    case 4: valor = stopbits();
		break;
	    case 5: valor = remote();
		break;
	    case 6: valor = local();
		break;
	    case 7: valor = protocolo();
		break;
	    case 8: valor = prefijo();
		break;
	    case 9: valor = prefijoat();
		break;
	    case 10: valor = grabar();
		report( valor );
		break;
	}

    } while( valor == 100 );

    return 0;
}

/************************************************/
/************************************************/
	       /* archivos   */
/************************************************/
/************************************************/
int archivos( void )
{
/* variables locales */
    int choice = 0;
    int valor = 100;

    do {
	clearmenugets();

	caja(ARCHX -1 , LINEAY,ARCHX + 8, LINEAY + 2 );
	prompt(ARCHX,LINEAY + 1,"Agregar","Agregar un telefono a la lista.");

	choice = menu(choice);

	switch( choice ) {
	    case 0: return 0;
	    case 1: valor = agregar();
		break;
	}

    } while( valor == 100 );

    report( valor );

    return 0;
}

/***************************************/
/************   about ******************/
/***************************************/
void about( void )
{
	centrar("Telefono",10);
	centrar("Version 1.2",11);
	centrar("Hecho por Ricardo Quesada",13);
	gotoxy(37,15);
	cprintf("Com: %d ",com);
	if(!ansi)
		centrar("Ansi activado",16);
	else centrar("Ansi desactivado",16);
	caja(20,9,60,17);
	gotoxy(0,0);
	getch();
}

/************************************************/
/*               mandarman                      */
/************************************************/
int mandarman( void )
{
	/* variables locales */
    char buffi[80];
	char *bptr;
    int er=0;

    gotoxy(1,15);
    cprintf("Escriba comando:\n\r");
    buffi[0] = 75;
    bptr = cgets(buffi);


    if(buffi[1] != 0)
	er = mandarcmd( bptr );
    else cprintf("No se especifico comando.\r\n");
	return( er );
}
/************************************************/
/*       discar un numero DISCARNUM             */
/************************************************/
int discarnum( void )
{
	/* variables locales */
	char buffi[12];
	char *ptr;
	int valor;

	caja(30,12,50,16);
	centrar("Numero a discar",13);
	gotoxy(35,15);
	buffi[0] = 10;
	ptr = cgets(buffi);
	valor = llamar( ptr );

	return valor;
}
/***********************************************/
/*  		hangup			       */
/***********************************************/
int hangup( void )
{
    /* variables locales */
    int valor;

    valor = hngup_hm( * modem );

    return valor;
}

/************************************************/
/************************************************/
	       /* comandos   */
/************************************************/
/************************************************/
int comandos( void )
{
/* variables locales */
    int choice = 0;
    int valor = 100;

    do {
	clearmenugets();

	caja(COMAX -1 , LINEAY,COMAX + 8, LINEAY + 5 );

	prompt(COMAX,LINEAY + 1,"Reset","Resetea e inicializa el modem.");
	prompt(COMAX,LINEAY + 2,"Enviar","Enviar comando sin prefijo.");
	prompt(COMAX,LINEAY + 3,"Discar","Llama a un numero");
	prompt(COMAX,LINEAY + 4,"Hangup","Terminar comunicacion");
	choice = menu(choice);

	switch( choice ) {
	    case 0: return 0;
	    case 1: valor = inic_modem();
		break;
	    case 2: valor = mandarman();
		break;
	    case 3: valor = discarnum();
		break;
	    case 4: valor = hangup();
		break;
	}

    } while( valor == 100 );

    report( valor );

    return 0;
}

/***************************************/
/*****/
/****/
/*************   configurar      *******/
/****/
/****/
/***************************************/
int configurar( void )
{
  /* variables locales */
    int choice = 0;

   while(1) {
	clrscr();
	clearmenugets();

	caja(1,1,80,3);
	prompt(ABOUTX,2,"�","Acerca del programa.");
	prompt(MODX,2,"Config","Configurar los seteos del modem.");
	prompt(LLAMAX,2,"Llamar","Llamar... m�s claro imposible.");
	prompt(ARCHX,2,"Archivos","Agregar, borrar, grabar, etc.");
	prompt(COMAX,2,"Comandos","Comandos para el modem.");
	prompt(70,2,"Salir","Salir del programa");

	choice = menu(choice);
	switch( choice ) {
	    case 0: cerrar();
		    exit(0);
	    case 1: about();
		break;
	    case 2: modemcfg();
		break;
	    case 3: abrir();
		break;
	    case 4: archivos();
		break;
	    case 5: comandos();
		break;
	    case 6: cerrar();
		    exit(0);
	}
   }
}
/*************************************************************/
/**************        main()      ***************************/
/*************************************************************/
void main(int argc, char* argv[])
{
   /* variables */

   com = 1;     	     /* por defecto se asume com 1 */


   if (argc == 2) {
     if ( ( * argv[1] ) != '/')
	error(6);
     if ( (com = atoi( argv[1]+1 ) ) == 0 )
	error(6);
     else if( com < 1 || com > 8 )
	error(6);
   }
   else if (argc > 2)
	error(6);

   ansi = isansi();

   if( leercfg() != 0) {
	cprintf("Se encontraron problemas para leer el archivo de configuracion.\n");
	cprintf("Una tecla para seguir.\n");
	getch();
   }

   textcolor( WHITE );
   textbackground( BLACK );

   clrscr();
   centrar("Inicializando modem...",24);

   if( inic_modem() ) {
	centrar("Error al inicializar el modem.",25);
	getch();
   }
   while(1) {
	clrscr();
	about();
	configurar();
   }
}
