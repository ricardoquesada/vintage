	page 60,132
;=======================================================================
;
;  This procedure provides an interface between C and the ASYNCH
;  MANAGER Level 0 functions.  The calling sequence is:
;
;  retcode = comgate(&reg);
;
;  where reg is a structure (COMREG) of standard registers defined as:
;
;	typedef struct
;	{
;	  unsigned ax,bx,cx,dx,si,di,ds,es;
;	} COMREG;
;
;  The setting of AH determines which functions are accessed:
;
;  AH Value   Description
;  --------   ---------------------------------------------------------
;    0-6      Asynch Manager Level Zero Functions
;      7      BIOS RS232 Functions (0x14)
;				   (CH determines which subfunction)
;      8      BIOS Timer Function (0x1A)
;
;  When assemblying, ccomint.mac should be renamed ccomgate.mac if the
;  gate will use a software interrupt, and ccomfar.mac should be renamed
;  ccomgate.mac if a call to com_entry is used.  Similarly, either
;  the appropriate comp***.mac file must be renamed compiler.mac to
;  specify the compiler (Turbo or Microsoft) for which the gate is to
;  be used.  The file beginasm.mac includes compiler.mac and defines the
;  necessary macros for startup and finish sequences.
;
;  Version 5.00 (C)Copyright Blaise Computing Inc.  1984, 1987
;
;  Version 5.01 Experimental 1/13/88 PLD
;
;=======================================================================

       include	 ccomgate.mac
       ife	 INTVEC
extrn	  com_entry:far 	       ; Entry point to ASYNCH Level 0
       endif

       include	 beginasm.mac
       beginProg comgate


       ; Beginning of actual code

       push	bp		       ; Save the frame pointer, and
       mov	bp,sp		       ; initialize this frame.
       push	di		       ; Save register variables
       push	si
       push	es		       ; Save ES and DS, which we use
       push	ds
       if	longData
	 les	si,dword ptr [bp + stkoff]
       else
	 mov	si,[bp + stkoff]
	 push	ds		       ; Make sure ES and DS are
	 pop	es		       ; the same.
       endif
       mov	ax,es:[si]	       ; Set up the registers for call
       mov	bx,es:[si + 2]	       ; call to Level 0.
       mov	cx,es:[si + 4]
       mov	dx,es:[si + 6]
       mov	di,es:[si + 10]
       mov	ds,es:[si + 12]
       push	ds
       push	es
       pop	ds
       mov	es,ds:[si + 14]
       mov	si,ds:[si + 8]
       pop	ds

       cmp	al,7		       ; Is it a BIOS call?
       je	bios		       ; Yes
       cmp	al,8
       je	bios
       if	INTVEC		       ; ASYNCH called via software int?
	 int	INTVEC		       ; Yes.
       else
	 call	com_entry	       ; Call ASYNCH Level 0 functions
       endif
       jmp	short return

bios:
       cmp	al,8		       ; Calling the Timer?
       je	timer		       ; Yes, AL = 8
rs232:				       ; No, AL = 7
       mov	ax,cx		       ; Set up call to BIOS function
       dec	dx		       ; Port number is 0 or 1
       int	14h		       ; Call BIOS RS232 functions
       jmp	short return
timer:
       mov	ax,0		       ; Set up call to read time
       int	1ah

return:
       if	longData
	 push	si		       ; Save SI
	 push	es		       ; Save ES

	 les	si,dword ptr [bp + stkoff]

	 pop	es:[si + 14]	       ; Return the value of ES
	 pop	es:[si + 8]	       ; Return the value of SI

	 mov	es:[si],ax	       ; Return the remaining parameters
	 mov	es:[si + 2],bx
	 mov	es:[si + 4],cx
	 mov	es:[si + 6],dx
	 mov	es:[si + 10],di
	 mov	es:[si + 12],ds
       else
	 push	si		       ; Save SI
	 push	ds		       ; Save DS

	 mov	ds, [bp - 8]
	 mov	si, [bp + stkoff]

	 pop	[si + 12]	       ; Return the value of DS
	 pop	[si + 8]	       ; Return the value of SI

	 mov	[si],ax 	       ; Return the remaining parameters
	 mov	[si + 2],bx
	 mov	[si + 4],cx
	 mov	[si + 6],dx
	 mov	[si + 10],di
	 mov	[si + 14],es
       endif

       cld
       pop	ds		       ; Restore DS
       pop	es
       pop	si
       pop	di
       pop	bp

       ret

       endProg	comgate
       end
