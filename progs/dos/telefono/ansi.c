/* programa que detecta si ansi esta cargado y si no lo carga. */
/* retorna 0 si ansi esta cargado.                             */
/* retorna -1 si no se pudo cargar                             */

#include <stdio.h>
#include <dos.h>
#include <process.h>

int isansi( void )
{
   union REGS regs;

   regs.x.ax = 0x1a00;  /* is ansi installed  */
   int86(0x2f, &regs, &regs);
   if( regs.h.al != 0xff )
	if(spawnl(P_WAIT,"ansi.com","ansi.com",NULL) == -1)
		return -1;

   return 0;
}