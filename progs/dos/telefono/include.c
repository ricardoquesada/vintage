/* archivos a incluir en telefono.exe */

#include <asynch_1.h>
#include <asynch_h.h>
#include <asynch_f.h>

/* includes de level one */

#include "c:\async\d2\open_a1.c"
#include "c:\async\d2\retop_a1.c"
#include "c:\async\d2\setop_a1.c"
#include "c:\async\d2\init_a1.c"
#include "c:\async\d2\iscom_a1.c"
#include "c:\async\d2\rdch_a1.c"
#include "c:\async\d2\wrtch_a1.c"
#include "c:\async\d2\iflsh_a1.c"
#include "c:\async\d2\delay_a1.c"
#include "c:\async\d2\drain_a1.c"
#include "c:\async\d2\iwait_a1.c"
#include "c:\async\d2\oflsh_a1.c"
#include "c:\async\d2\oqsiz_a1.c"
#include "c:\async\d2\qsize_a1.c"
#include "c:\async\d2\pctrl_a1.c"
#include "c:\async\d2\rdst_a1.c"
#include "c:\async\d2\close_a1.c"
#include "c:\async\d2\wrtst_a1.c"



/* includes de inicializacion de modem ,etc */

#include "c:\async\d2\gocmd_hm.c"
#include "c:\async\d2\reset_hm.c"
#include "c:\async\d2\init_hm.c"
#include "c:\async\d2\cwait_hm.c"
#include "c:\async\d2\struc_hm.c"
#include "c:\async\d2\dial_hm.c"
#include "c:\async\d2\cmd_hm.c"
#include "c:\async\d2\resp_hm.c"
#include "c:\async\d2\hngup_hm.c"
#include "c:\async\d2\isdcd_hm.c"
#include "c:\async\d2\query_hm.c"



/*   include de transferencia de datos          */

#include "c:\async\d2\fmove_ft.c"
#include "c:\async\d2\rinit_ft.c"
#include "c:\async\d2\sinit_ft.c"
#include "c:\async\d2\struc_ft.c"
#include "c:\async\d2\xfer_ft.c"
#include "c:\async\d2\yxfer_ft.c"
