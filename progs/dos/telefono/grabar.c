/********** grabar configuracion **************/
#include <stdio.h>
#include <io.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <asynch_h.h>

extern int com;

int grabar( void )
{
    int i;
    int j;
    int k;
    int para;
    int configuracion[] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0
    };

    k = sizeof( configuracion );

    for(i=1;i<6;i++) {
	 retop_a1( com , i ,&para );
	 configuracion[i-1] = para;         /* porque empieza con zero */
    }

    if( (j = open("telefono.cfg",O_RDWR | O_CREAT , S_IREAD | S_IWRITE )) == -1 )
	return -1;

    if ( write( j, &configuracion , k) != k) {
	close( j );
	return -1;
    }

    close(j);

    return 0;
}
