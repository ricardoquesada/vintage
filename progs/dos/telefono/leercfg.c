/********** leer  configuracion **************/
#include <stdio.h>
#include <io.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <asynch_h.h>

extern int com;

int leercfg( void )
{
    int i;
    int j;
    int k;
    int para;
    int configuracion[] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0
    };

    k = sizeof( configuracion );


    if ( (j = open("telefono.cfg",O_RDWR | O_CREAT , S_IREAD | S_IWRITE ) ) == -1)
	return -1;

    if ( read(j, &configuracion , k) != k ) {
	close ( j );
	return -2;
    }


    for(i=1;i<6;i++) {
	para = configuracion[ i -1 ];      /* empieza con zero */
	setop_a1( com , i , para);
    }

    close(j);

    return 0;
}

