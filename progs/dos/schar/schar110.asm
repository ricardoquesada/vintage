;name: SChar
;verion: 1.10
;start  date: 21/11/94
;finish date: 21/11/94
;author: Ricardo Quesada
;*** Capturador residente de fonts ***

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code


;************ equ *************************;

;buscar por "ada a" para cambiar el nombre de regis.

SHARE EQU 0
key_mask equ 00001101b  ;Alt + Ctrl + Right Shift
verhi    equ '01'
verlo    equ '10'
CARG_SIZE equ findecargador - cargador
TRUE    equ 1
FALSE   equ 0

IF SHARE EQ 0
%OUT Compilando versi�n Registrada (SChar v1.10)
ELSEIF SHARE EQ 1
%OUT Compilando versi�n Shareware (SChar v1.10)
ELSE
%out Compilando versi�n con ERRORES (SChar v1.10)
ENDIF

FILETIME  = 1 * 2048 + 10 * 32           ;versi�n
FILEDATE  =14 *  512 + 11 * 32 + 21      ;fecha
;************* START ********************;
start:
JMP INIT2
db'SChar v1.10 (c) Ricardo Quesada 1994.',13,10
xorvalue db 0

intromsg equ this byte
;*******************************;
;                               ;
;                               ;
;   Rutinas                     ;
;                               ;
;                               ;
;*******************************;

;*******************************;       new on v1.10
;       fulltable               ;
;*******************************;
; Chequea si la tabla tiene info;
fulltable proc near
    push ax
    push bx
    push cx
    push dx
    push di
    push si
    push ds
    push es

    call inicializar

    mov ax,0a000h
    mov ds,ax

    xor bl,bl
    xor cx,cx                       ;compara la tabla completa
    mov di,cs:offfont               ;valor inicial 0

todaviaescero:
    xor ax,ax
    cmp [di],ax
    jne savethistable
    add di,2
    add cx,2
    inc bl
    cmp bl,8                        ;bl=8 == si=16
    jb alloop
    add cx,16
    add si,16
    xor bl,bl
alloop:
    cmp cx,2000h
    jb todaviaescero

    mov cs:savetable,FALSE
    jmp fulltable_
savethistable:
    mov cs:savetable,TRUE

fulltable_:
    call restaurar

    pop es
    pop ds
    pop si
    pop di
    pop dx
    pop cx
    pop bx
    pop ax
    ret
fulltable endp

;*******************************;
;       inicializar             ;
;*******************************;
;use bitplane 2 , seg at a000h  ;
inicializar proc near
        push ax
        push dx
        push ds

        push cs
        pop ds
nopuedo_inic:
        cli                     ;clear interrup flag
        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al

        inc dx                  ; data of Sequencer Controller
        in al,dx                ;
        mov srmap,al            ; restaura valor origianal
        mov al,04h              ; select bitplane 2
        out dx,al               ;
;
        mov dx,3c4h             ; Sequencer Controller
        mov al,4                ; 4th register
        out dx,al

        inc dx
        in al,dx                ;
        mov srmem,al            ;restaura valor original
        mov al,7
        out dx,al               ;256k & linear processing.
;
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register

        inc dx
        in al,dx
        mov grmap,al            ;restaura valor original
        mov al,02h              ;Bitplane 2
        out dx,al
;
        mov dx,3ceh             ;Graphic Controller
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register

        inc dx
        in al,dx
        mov grmod,al            ;restaura valor original
        mov al,0
        out dx,al               ;Write mode 0,linear adresses,16 colors
;
        mov dx,3ceh             ;Graphic controller
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register

        inc dx
        in al,dx
        mov grmis,al            ;restaura valor original
        mov al,04h
        out dx,al               ;Text,linear addresses,64k at A000h
;
        sti                     ;set interrupt flag
        pop ds
        pop dx
        pop ax
        ret
inicializar endp

;******************************;
;       restaurar              ;
;******************************;
restaurar proc near
        push ax
        push dx
        push ds

        push cs
        pop ds
;
        cli
        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al

        inc dx                  ; data of Sequencer Controller
        mov al,srmap            ;
        out dx,al               ; restaura
;
        mov dx,3c4h
        mov al,4                ; 4th register
        out dx,al

        inc dx
        mov al,srmem            ;
        out dx,al               ;restaura
;
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register

        inc dx
        mov al,grmap
        out dx,al               ;restaura
;
        mov dx,3ceh
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register

        inc dx
        mov al,grmod
        out dx,al               ;restaura
;
        mov dx,3ceh
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register

        inc dx
        mov al,grmis
        out dx,al               ;restaura
;
        sti
        pop ds
        pop dx
        pop ax
        ret
restaurar endp

;*******************************;
;                               ;
;           save                ;
;                               ;
;*******************************;
save proc near
        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds
        push es

        call infovga

        push cs
        pop ds
        xor ax,ax
        mov nfont,ah
        mov offfont,ax

        mov savetable,TRUE

        mov ah,totrow5
        mov biteti+1,ah

volveragrabar:

        push cs
        pop ds

        cmp todos,2
        jne todosnot2

        call fulltable          ;la tabla esta completa ?

        cmp savetable,TRUE
        je todosnot2
        jmp vienedeltodos

todosnot2:
        mov ah,3ch              ;create file
        xor cx,cx               ;atributos
        mov dx,offset fname
        int 21h                 ;create a file or truncate
        mov handle,ax

        jnc nohuberror
huberror:
        call sound
        jmp chau_save

nohuberror:
        mov bx,ax               ;get handle
        mov ah,40h
        mov cx,CARG_SIZE        ;number of bytes
        mov dx,offset cargador
        int 21h                 ;write usign handle
        jb huberror

        call inicializar        ;ahora inicializa para esta concha

        mov ax,segme
        mov es,ax
        xor di,di               ;es:di  -> destination
        mov ax,0a000h           ;
        mov ds,ax               ;ds:si  -> source
        mov si,cs:offfont       ;offfont

        mov cx,100h             ;total de veces
seguir__a:
        call mover16
        dec cx
        or cx,cx                ;cx=0?
        jne seguir__a

        call restaurar

        mov cx,cs:totrow6       ;grabar tabla completa
        push es
        pop ds                  ;ds:dx  la tabla
        xor dx,dx
        mov bx,cs:handle        ;file handle
        mov ah,40h
        int 21h                 ;Tabla grabada completa
        jc huberror

        mov ah,3eh              ;close file handle
        int 21h
        jc huberror

        push cs
        pop ds

        cmp todos,0             ;Solo una tabla ?
        je chau_save            ;Yeah...

vienedeltodos:
        mov ch,fname+6          ;proximo sera 0000001?.asc
        cmp ch,'9'              ;Esto es por un nombre es rrrrrrrr
        jbe forrazo2            ;es proximo sera rrrrrrrr0
        mov ch,'0'
        jmp short aboab2
forrazo2:
        inc ch                  ;incrementa 1
        and ch,00000111b        ;maximo permitido en 7
        or ch,30h               ;le agrega 30h
aboab2:
        mov fname+6,ch

        mov ax,2000h
        add offfont,ax

        inc nfont
        mov al,nfont
        cmp al,8
        je chau_save
        jmp volveragrabar

chau_save:
        push cs
        pop ds

        mov ch,'0'
        mov fname+6,ch          ;pone a 0 el fucking name

        mov ch,fname+7          ;proximo sera 00000001.asc
        cmp ch,'9'              ;Esto es por un nombre es rrrrrrrr
        jbe forrazo1            ;es proximo sera rrrrrrrr0
        mov ch,'0'
        jmp short aboab1
forrazo1:
        inc ch                  ;incrementa 1
        and ch,00000111b        ;maximo permitido en 7
        or ch,30h               ;le agrega 30h
aboab1:
        mov fname+7,ch

        call sound

        pop es
        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax
        ret
save endp

cargador:
        jmp short abcd
        db'VChar NI v3.20 (c) Ricardo Quesada 1994.'
abcd:
        cld
        mov si,81h
_alopa: lodsb
        cmp al,13               ;si es intro fin
        je _apartir
        cmp al,20h              ;si es espacio de nuevo
        je _alopa
_lulu:  cmp al,'/'
        je _opcion
        cmp al,'-'
        jne _alopa

_opcion:
        lodsb
        cmp al,13
        je _apartir

        cmp al,'8'
        je _8_
        cmp al,'9'
        je _9_
        jmp _alopa

_8_:                            ;convierte todo a 8 bit width
        mov dx,3cch             ;cambia la frecuencia del monitor
        in al,dx
        and al,11110011b        ;borra lo que halla
        mov dx,3c2h
        out dx,al

        cli
        mov dx,3c4h
        mov ax,100h
        out dx,ax

        mov ax,101h
        out dx,ax

        mov ax,300h             ;Enable the controller
        out dx,ax               ;
        sti

        mov ax,1000h            ;interrupcion
        mov bl,13h
        mov bh,0                ;por default para 8bit
        int 10h                 ;posciciona bien la pantalla
        jmp _apartir

_9_:                            ;convierte todo a 9 bit width
        mov dx,3cch             ;cambia la frecuencia del monitor
        in al,dx
        and al,11110011b        ;borra lo que halla
        or al,00000100b         ;prende el puto bit
        mov dx,3c2h
        out dx,al

        cli
        mov dx,3c4h
        mov ax,100h
        out dx,ax

        mov ax,001h
        out dx,ax

        mov ax,300h             ;Enable the controller
        out dx,ax               ;
        sti

        mov ax,1000h            ;interrupcion
        mov bl,13h
        mov bh,8                ;por default para 8bit
        int 10h                 ;posciciona bien la pantalla

_apartir:
        mov bp,100h + CARG_SIZE ;offset donde estan los chars
biteti  db 0b7h,0               ;mov bh,8 o 10h
        mov cx,100h             ;cantidad de chars
        xor dx,dx               ;offset del block
        xor bl,bl               ;block
        mov ax,1110h            ;write usign 10h
        int 10h

        mov ax,4c00h            ;terminate
        int 21h

findecargador equ this byte

;***************************************;
;***    mover16                      ***;
;***************************************;
mover16 proc near
        push cx

        xor ch,ch
        mov cl,cs:totrow5
repne   movsb

        xor ch,ch
        mov cl,cs:totrow4
        add si,cx                   ;saltea espacios chotos

        pop cx
        ret

mover16 endp

;***    FIN DE SALVAR COM            ***;
;***************************************;
;***************************************;

;*******************************;       new on v2.60
;                               ;
;            infovga            ;
;                               ;
;*******************************;
infovga proc near
    push ds

    mov ax,40h
    mov ds,ax
    mov si,84h
    mov ah,[si]

    push cs
    pop ds

    cmp ah,49                       ;
    je rrr50                        ; REVISION A 24/9/94 V3.00

    mov totrow4,16
    mov totrow5,16
    mov totrow6,1000h
    jmp short _infovga
rrr50:
    mov totrow4,24
    mov totrow5,8
    mov totrow6,800h
_infovga:
    pop ds
    ret
infovga endp

;*******************************;
;       sound                   ;
;*******************************;
sound proc near
        mov al,182
        out 43h,al
        mov ax,01415h
        out 42h,al
        mov al,ah
        out 42h,al

        in al,61h
        or al,11b
        out 61h,al

        mov cx,01000h
aqui2:
        mov dx,0040h
aqui:
        dec dx
        jne aqui
        dec cx
        jne aqui2

        in al,61h
        and al,11111100b
        out 61h,al
        ret
sound endp

;*************************;
;                         ;
;        DOSACTIVE        ;
;Output: Zero flag=1 Dos may be interrupted.
;*************************;
dosactive proc near
        push ds
        push bx
        lds bx,indos            ;ds:bx point to the INDOS flag
        cmp byte ptr [bx],0     ;DOS function active?
        pop bx
        pop ds
        ret
dosactive endp


;******** INTERRUPS ****************************************;


;**************************;        Interrupci�n del tiempo
;******** int 08 **********;
int08:
        jmp short cont08
        db 'SC'
cont08:
        cmp cs:tsrnow,0         ;si es 0 TIME OUT.
        je i8_end               ;volver a intentar

        dec cs:tsrnow           ;decrementa para poder activar.

        cmp cs:in_bios,0        ;se esta usando la int 13
        jne i8_end              ;si =0 no, si <>0 si

        call dosactive
        je i8_tsr
i8_end:
        jmp cs:[old08]
i8_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old08]
        call start_tsr
        iret

;**************************;
;********* int 09 *********;
int09:
        jmp short cont09
        db 'SC'
cont09:
        push ax

        cmp cs:tsractive,0      ;ya esta activo
        jne i9_end              ;si then terminar

        cmp cs:tsrnow,0         ;waiting for activation
        jne i9_end              ;yes so terminar

        push ds                 ;lee si se pulsa el HOTKEY
        mov ax,040h
        mov ds,ax
        mov al,byte ptr ds:[17h]
        and al,key_mask
        cmp al,key_mask
        pop ds
        jne i9_end              ;No se pulsa entonces nada.

        cmp cs:in_bios,0        ;Se pulsa pero int 13 usandose
        jne i9_e1               ;Then poner tiempo de espera

        call dosactive          ;No se esta usando
        je i9_tsr               ;Pero el INDOS lo permite.Si then TSR
i9_e1:
        mov cs:tsrnow,9         ;9/18.2 = 0.5 segundos de espera.
i9_end:
        pop ax
        jmp cs:[old09]
i9_tsr:
        mov cs:tsractive,1
        mov cs:tsrnow,0
        pushf
        call cs:[old09]
        pop ax
        call start_tsr
        iret

;**************************;
;********* int 2f *********;
int2f:
        jmp short cont2f
        db 'SC'
cont2f:
        cmp ax,0d500h
        jne no_d500
        cmp bx,'SC'
        jne no_d500
        xchg al,ah
        mov bx,VERHI
        mov cx,VERLO
        mov dx,offset todos
        push cs
        pop di                                  ;di Segmento.
        iret

no_d500:
        jmp cs:[old2f]


;***************************;
;********** int 13 *********;
int13:
        jmp short cont13
        db 'SC'
cont13:
        inc cs:in_bios

        pushf
        call cs:[old13]
        dec cs:in_bios
        sti
        ret 2

;*******************************;
;********* int 28 **************;

int28:
        jmp short cont28
        db 'SC'
cont28:
        cmp cs:tsrnow,0         ;tsrnow es el tiempo que tiene
        je i28_end              ;para empezar a ejecutarce.
                                ;Si es 0 se ejecuta.

        cmp cs:in_bios,0        ;in_bios es el flag de la int 13
        je i28_tsr              ;si es 0 se puede ejecutar.

i28_end:
        jmp cs:[old28]

i28_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old28]
        call start_tsr
        iret

;*************************************************;
;********** START PROGRAMA RESIDENTE *************;
;*************************************************;
start_tsr proc near

        cli
        mov cs:xx_ss,ss
        mov cs:xx_sp,sp

        mov sp,offset stacks_end
        mov ax,cs
        mov ss,ax
        sti

        push ax
        push bx
        push cx
        push dx
        push ds
        push es
        push si
        push di

        call save

        pop di
        pop si
        pop es
        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        cli

        mov ss,cs:xx_ss
        mov sp,cs:xx_sp

        mov cs:tsractive,0
        sti
        ret
start_tsr endp


;************ data **********************;

xx_ss   dw 0
xx_sp   dw 0

tsrnow  db 0
tsractive db 0
in_bios db 0

dta_off dw 0
dta_seg dw 0

indos equ this dword
indos_off dw 0
indos_seg dw 0

old08 equ this dword
off08 dw 0
seg08 dw 0

old09 equ this dword
off09 dw 0
seg09 dw 0

old13 equ this dword
off13 dw 0
seg13 dw 0

old28 equ this dword
off28 dw 0
seg28 dw 0

old2f equ this dword
off2f dw 0
seg2f dw 0


;
;datos especificos de SChar y no del programa residente
;
segme   dw 0                ;segmento donde alloca memoria
handle  dw 0                ;handle del file a abrir
nfont   db 0                ;usada por save. Para salvar all 7 fonts.
offfont dw 0                ;offset de los fonts
todos   db 2                ;=1 graba todos los fonts. ;=0 solo 1 tabla ;=2 Best fit
savetable db TRUE           ;usada por fulltable ( tabla a grabar , � si o no ?
fname   db'00000000.COM',0
totrow4 db ?            ;usada por Sal_asm (salteador de espacios chotos) y SAVE
totrow5 db ?            ;usada por SAVE
totrow6 dw ?            ;usada por SAVE

srmap   db ?            ;Sequencer Register, Read Map
srmem   db ?            ;                  , Memory Mode
grmap   db ?            ;Graphics Register, Read Map
grmod   db ?            ;                 , Graph Mode
grmis   db ?            ;                 , Miscellaneous


stacks dw 256 dup(0)
stacks_end equ this byte

INIT:
;****** MESSAGES *****
error_inst_msg1 db 'Error: No estoy residente. No me puede desinstalar.',13,10,'$'
error_isnt_msg2 db 'Error: No se puede desinstalar. Trate de remover otros residentes.',13,10,'$'
error_inst_msg3 db 'Error: El SChar residente pertenece a otra versi�n. Use esa versi�n.',13,10,'$'
anotherver_msg  db 'Error: Ya hay un SChar instalado. Desinstalelo para instalar esta versi�n.',13,10,'$'
intro_msg db'SChar 1.10 (c) Ricardo Quesada 1994. Para ayuda: SChar /h',13,10
IF SHARE EQ 1
db'Versi�n Shareware. Please register.                       ',13,10,13,10,'$'
ELSEIF SHARE EQ 0
db'Versi�n registrada a: Sebasti�n Cativa.                   ',13,10,13,10,'$'
ENDIF
reg_not_ok equ this byte
db 'ERROR',13,10,'Verifique que su disco no este protegido.Gracias.',13,10,'$'
prean_msg label byte
db 'Tratando de remover cualquier tipo de virus o compactaci�n...$'
antiv_msg equ this byte
db'OK',13,10,13,10,'SUGERENCIA: Resetea la computadora ahora mismo.',13,10
db'            El virus debe estar residente.',13,10,'$'

installing_msg equ this byte
db'Instalci�n satisfactoria.',13,10
db'HotKey: Shift Derecho + Control + Alt',13,10,'$'
actualing_msg equ this byte
db'Actualizaci�n de parametros satisfactoria.',13,10
db'HotKey: Shift Derecho + Control + Alt',13,10,'$'
font8 db'Fonts a grabar: 8',13,10,'$'
font1 db'Fonts a grabar: 1',13,10,'$'
fontb db'Fonts a grabar: Solo los que tengan algo',13,10,'$'
uninstall_msg db'Desinstalado.',13,10,'$'
nomem_msg db 'Error: No hay suficiente memoria libre. Libere 8k m�s por favor.',13,10,'$'

help_msg equ this byte
db'SChar [opciones]',13,10
db'     /a      - La conocida opci�n de remover los virus.',13,10
db"               ( Built-in Ricardo Quesada's antivirus (c) 1993 ).",13,10
db'     /u      - Desinstala a SChar.',13,10
db'     /n      - Graba una tabla.',13,10
db'     /f      - Graba solo las tablas que tienen algo. ( Recomendada ).',13,10
db'     /t      - Graba toda las tablas ( 8 en total ).',13,10,13,10
db'HotKey: Shift Derecho + Control + Alt',13,10
db'Los archivos se grabar�n con el nombre 000000xx.COM',13,10
db'Para m�s informaci�n lea SChar.doc .',13,10,'$'

INIT2:
;******* MANAGE MEMORY **********

        call xorear             ;desencripta todo

        mov dx,offset intro_msg
        mov ah,9
        int 21h

        mov ah,4ah
        mov bx,offset ende
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        je jmpinst              ;instala
        cmp al,20h              ;es espacio, si then seguir buscando
        je alop
        cmp al,'/'
        je command_line         ;loop until '/' or CR is found
        cmp al,'-'
        je command_line
        jne displayhelp

command_line:
        lodsb
        and al,11011111b
        cmp al,'U'
        je uninstall
        cmp al,'T'
        je stodos
        cmp al,'N'
        je ntodos
        cmp al,'A'
        je anti_
        cmp al,'F'
        je ftodos

;[...] agregar nueva opci�n aca
        jne displayhelp

jmpinst:
        jmp instalar

stodos: mov todos,1
        jmp alop
ntodos: mov todos,0
        jmp alop
ftodos: mov todos,2
        jmp alop
anti_:
        jmp antivirus
displayhelp:                    ;***************** no command line
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***
        mov ax,4c00h
        int 21h

uninstall:                      ;*********************command line U
        mov ax,0d500h
        mov bx,'SC'
        int 2fh
        cmp ax,00d5h
        jne error_inst2

        cmp bx,verhi
        jne error_inst3
        cmp cx,verlo
        jne error_inst3         ;no es.no desinstala

        mov ax,3508h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,3509h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,3513h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,3528h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,352fh
        int 21h
        cmp es:[bx+2],'CS'
        je desinstalar

error_inst:                         ;mensaje de error porque no puede desinstalar
        push cs
        pop ds
        mov dx,offset error_isnt_msg2
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h

error_inst2:                        ;mensaje de error porque no esta residente
        push cs
        pop ds
        mov dx,offset error_inst_msg1
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h

error_inst3:                        ;mensaje de error porque no esta residente
        push cs
        pop ds
        mov dx,offset error_inst_msg3
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h

desinstalar:                            ;*** Rutina de desinstalacion ***
        cli
        mov dx,es:off08
        mov ds,es:seg08
        mov ax,2508h
        int 21h                         ;*** restaurar vector original

        mov ax,2509h
        mov dx,es:off09
        mov ds,es:seg09
        int 21h

        mov ax,2513h
        mov dx,es:off13
        mov ds,es:seg13
        int 21h

        mov ax,2528h
        mov dx,es:off28
        mov ds,es:seg28
        int 21h

        mov ax,252fh
        mov dx,es:off2f
        mov ds,es:seg2f
        int 21h
        sti

        mov bx,es
        mov es,es:[2ch]
        mov ah,49h
        int 21h
        mov es,bx
        mov ah,49h
        int 21h                         ;Release memory

        push es
        pop ds
        mov bx,segme
        mov es,bx
        mov ah,49h
        int 21h                         ;release memory

        push cs
        pop ds
        mov dx,offset uninstall_msg
        mov ah,9
        int 21h

        mov ax,4c00h
        int 21h


;*********************************;
;*****     Instalar        *******;

instalar:
        mov ax,0d500h           ;Is SChar installed?
        mov bx,'SC'
        int 2fh
        cmp ax,00d5h
        jne instalar_           ;No esta instalado, then install please.
        cmp bx,VERHI
        jne anotherversion1
        cmp cx,VERLO
        jne anotherversion1
        mov ds,di               ;del residente saca esta info
        mov al,cs:todos
        mov bx,dx
        mov [bx],al

        push cs
        pop ds                  ;command line actualizado
        mov ah,9                ;print message of installation
        mov dx,offset actualing_msg
        int 21h

        mov dx,offset font1
        cmp todos,0
        je fontmsg2
        mov dx,offset font8
        cmp todos,1
        je fontmsg2
        mov dx,offset fontb
fontmsg2:
        mov ah,9
        int 21h
        mov ax,4c00h
        int 21h                     ;Command line actualizada.

anotherversion1:
        mov dx,offset anotherver_msg
        mov ah,9
        int 21h                 ;Mensaje de error si no es la v2.10
        mov ax,4cffh
        int 21h                 ;Return to DOS

instalar_:
        mov ah,48h              ;Hay suficiente memoria ?
        mov bx,513              ;513 paragraph, 2000h bytes (8k)
        int 21h
        jnc haymem

        mov dx,offset nomem_msg
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h
haymem:
        mov segme,ax
;*******************************;
;                               ;
;       Instalar                ;
;                               ;
;*******************************;
instalar__:
        mov ah,34h              ;get address of INDOS flag
        int 21h
        mov si,offset indos
        mov [si],bx
        mov [si+2],es

        mov ax,3508h            ;get address of int 08
        int 21h
        mov si,offset old08
        mov [si],bx
        mov [si+2],es

        mov dx,offset int08
        mov ax,2508h
        int 21h

        mov ax,3509h            ;get address of int 09
        int 21h
        mov si,offset old09
        mov [si],bx
        mov [si+2],es

        mov dx,offset int09
        mov ax,2509h
        int 21h

        mov ax,3513h            ;get address of int 13
        int 21h
        mov si,offset old13
        mov [si],bx
        mov [si+2],es

        mov dx,offset int13
        mov ax,2513h
        int 21h

        mov ax,3528h            ;get address of int 28
        int 21h
        mov si,offset old28
        mov [si],bx
        mov [si+2],es

        mov dx,offset int28
        mov ax,2528h
        int 21h

        mov ax,352fh            ;get address of int 2f
        int 21h
        mov si,offset old2f
        mov [si],bx
        mov [si+2],es

        mov dx,offset int2f
        mov ax,252fh
        int 21h

        mov ah,9                ;print message of installation
        mov dx,offset installing_msg
        int 21h

        mov dx,offset font1
        cmp todos,0
        je fontmsg
        mov dx,offset font8
        cmp todos,1
        je fontmsg
        mov dx,offset fontb
fontmsg:
        mov ah,9
        int 21h

        mov dx,offset init      ;TSR
        mov ah,31h
        mov cl,4
        shr dx,cl
        inc dx
        int 21h


;***************************************;
;***    antivirus                    ***;
;***************************************;
antivirus:
        mov dx,offset prean_msg
        mov ah,9
        int 21h
        call autosave
        jc irerroras
        call xorear                     ;desencriptar
        push cs
        pop ds
        mov dx,offset antiv_msg
        mov ah,9
        int 21h
irerroras:
        mov ah,4ch
        int 21h                         ;volver a DOS.

;***************************************;
;***    autosave                     ***;
;***************************************;
autosave proc near
        mov cx,0ffffh
        xor ax,ax
        xor di,di
        mov es,cs:[2ch]
scanear:
        repne scasb
        cmp byte ptr es:[di],0
        je ok_todo
        scasb
        jne scanear
ok_todo:
        mov dx,di
        add dx,3
        push es
        pop ds                  ;HASTA ACA DS:DX EL NOMBRE DEL PROG
        call newxor             ;change de value of the xor.
        call xorear             ;invierte los caracteres
        mov ah,3ch              ; Create a File
        xor cx,cx               ; in this case Truncate
        int 21h
        jc derror               ; If error then go to derror
        mov bx,ax               ;file handle
        mov cx,ende - start     ;number of bytes
        push cs
        pop ds
        mov dx,offset start     ;a partir de donde (desde el principio)
        mov ah,40h
        int 21h                 ;write
        jc derror
        mov ax,5701h
        mov cx,FILETIME
        mov dx,FILEDATE
        int 21h                 ;Set Time & Date.
        jc derror

        mov ah,3eh
        int 21h                 ;close
        jc derror

        ret

derror:                         ;error de drive.show message
        call xorear             ;mensaje en estado normal

        push cs
        pop ds
        mov dx,offset reg_not_ok
        mov ah,9
        int 21h
        stc                     ;set clear
        ret

autosave endp

;*********************************;
;***      newxor               ***;
;*********************************;
newxor proc near
        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

xoragain:
        xor ah,ah
        int 1ah
        or dl,dl
        je xoragain

        mov xorvalue,dl

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret
newxor endp

;*********************************;
;***      xorear               ***;
;*********************************;
xorear proc near
        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

        mov cx,init2 - intromsg
        mov si,offset intromsg
 todavia_sigo:
        mov al,xorvalue
        xor [si],al
        inc si
        dec cx
        jne todavia_sigo

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        ret
xorear endp

;********* F I N A L **********;

ende equ this byte

ruti    endp
code    ends
        end ruti

