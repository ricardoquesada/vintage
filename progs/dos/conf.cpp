#include <string.h>
#include <fcntl.h>
#include <io.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <ctype.h>
#include "adc.h"
#include "chanta.h"

/* definicion de funciones */

int preg_dias(void);
char preg_mus(void);
int open_file(void);
int write_file(char *numeros,char *musica);


/* definicion de variables */

int error;
int handle;
char bufday[3];
char bufmus[2];

/* +-------------------------------------------+ */
/* |            MAIN PROGRAM                   | */
/* +-------------------------------------------+ */


int main(int argc,char *argv[])
{
    if (argc >1)                           /* existe el parametro ? */
    {
      int i;
      for (i=1;i<argc;i++)
      {
        if(*argv[i]=='/')
        {
         clrscr();
         textcolor(WHITE);
         cprintf(CHANTA);
         exit(1);
        }
      }
    cprintf("Parametro invalido. Mejor trata con otro... Ja, Ja, Ja.\r\n");
    exit(1);
    }


  bufday[3]=0;
  bufmus[2]=0;
  int num;
  char musica;
  char *numeros;


  clrscr();
  textcolor(LIGHTBLUE);
  window(1,1,80,25);

  gotoxy(1,1);
  cprintf(IMAGEDATA);

  if(open_file()==1)  exit(1);


  textcolor(WHITE);
  num = preg_dias();
  itoa(num,numeros,10);
  if (num<10)
        {
          numeros[1] = numeros[0];
          numeros[0] = ' ';
        }
  musica = preg_mus();

  write_file(numeros,&musica);

  printf("\nGracias por usar ADC.");

  return 0;
}

int preg_dias (void)    /* Toma con cuantos numeros de anticipacion */
{

    char buffer[5];
    buffer[0]=3;
    char *c;
    int num;

    do  {
        gotoxy(57,20);
        printf("  ");
        gotoxy(1,20);
        printf ("Con cuantos dias de anticipacion (entre 1 y 30)? [%s] : ",bufday);
        c = cgets(buffer);
        if (c[0] == 0)                    /* usa el default */
            {
            c=bufday;
            cprintf("%s",bufday);
            }
        num =atoi(c);
        } while ((num>30)||(num<1));

    return num;
}

char preg_mus(void)
{
    int error=1;
    char buffer[4];
    buffer[0]=2;
    char *musica;
    char music;
    do  {
        gotoxy(34,21);
        printf(" ");
        gotoxy(1,21);
        printf("Lo desea con musica (s/n)? [%s] : ",bufmus);
        musica = cgets(buffer);
        if (musica[0]==0)
            {               /* usa el default */
            musica=bufmus;
            cprintf("%s",bufmus);
            }
        music = toupper(musica[0]);
        if ((music == 'N') || (music=='S'))
        error=0;               /* No errors */

        }while (error == 1);    /* If error exist then loop */

   return music;
}

int open_file()
{

   if ((handle = open("ADCCDP.DBF", O_RDWR)) == -1)

   {
      perror("Error");
      return 1;
   }

   lseek(handle,99,SEEK_SET);
   read(handle,bufday,2);

   lseek(handle,101,SEEK_SET);
   read(handle,bufmus,1);

   return 0;
}

int write_file(char *numeros,char *musica)
{

  lseek(handle,101,SEEK_SET);
  write (handle,musica,1);

  lseek(handle,99,SEEK_SET);
  write(handle,numeros,2);

  close(handle);

  return 0;
}
