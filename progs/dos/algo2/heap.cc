///////////////////////////////////////////////////////////////////
// 
//                 Implementacion de heaps
// para el proyecto 2 de Algorimos y Estructuras de Datos II
//
///////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdio.h>

///////////////////////////////////////////////////////////////
// Class Nodo
///////////////////////////////////////////////////////////////
class nodo
{
public:
		int valor;               // Valor
		nodo* izquierdo;         // Puntero al hijo izquiero
		nodo* derecho;           // Puntero al hijo derecho
		nodo* padre;             // Puntero al padre
		nodo();
		nodo( int );
};
// CONSTRUCTORES
nodo::nodo()
{
		valor=999;
		izquierdo=NULL;derecho=NULL;padre=NULL;
}
nodo::nodo( int a )
{
		valor=a;
		izquierdo=NULL;derecho=NULL;padre=NULL;
}

///////////////////////////////////////////////////////////////
// Class heap
///////////////////////////////////////////////////////////////
class heap
{
		nodo* raiz;              // Puntero a la raiz
		nodo* ultimo;            // Puntero al ultimo

		int ordenar_nodo( nodo* );   // Ordena el nodo
		nodo* hermano_derecho( nodo* ); // Hermano derecho del nodo
		nodo* bajar_arbol( nodo* );     // Usada por Hermano derecho
public:
		int insert_nodo( nodo* );    // Insertar nodo
		int borrar_nodo( nodo* );    // Borrar nodo
		int ver_min();           // Ver minimo
		int mostrar_arbol( nodo* );
		nodo* ver_raiz();
		heap();
};
// CONSTRUCTOR
heap::heap()
{
		raiz=NULL;
		ultimo=NULL;
}
// FUNCIONES PUBLICAS
int heap::insert_nodo( nodo* ptr_nodo )
{
		if(ptr_nodo==NULL ) // No se puede insertar nada, no ?
			return -1;
		
		printf("Insertando... %d\n",ptr_nodo->valor);
		if(raiz==NULL) // Es la raiz
		  {
				  raiz=ptr_nodo;
				  ultimo=ptr_nodo;
				  return 0;
		  }
		else
		  {
				  if( ultimo->izquierdo==NULL)
					{
							ptr_nodo->padre=ultimo;
							ultimo->izquierdo=ptr_nodo;
					}
				  else
					{
							ptr_nodo->padre=ultimo;
							ultimo->derecho=ptr_nodo;
							ultimo=hermano_derecho( ultimo );
					}
				  
				  ordenar_nodo( ptr_nodo );
		  }
		return 0;
}

int heap::borrar_nodo( nodo* ptr_nodo )
{
		return 1;
}
int heap::ver_min( )
{
		if( raiz==NULL )
			return -1;
		else
		  {
				  printf("Valor minimo del arbol %d\n", raiz->valor );
				  return ( raiz->valor );
		  }
}
int heap::mostrar_arbol( nodo* ptr_nodo )
{
		if(ptr_nodo==NULL)
			return 1;
		else
		  {
				  printf(" %d -",ptr_nodo->valor);
				  mostrar_arbol( ptr_nodo->izquierdo);
				  mostrar_arbol( ptr_nodo->derecho);
				  return 1;
		  }
}
nodo* heap::ver_raiz()
{
		return raiz;
}
// FUNCIONES PRIVADAS
int heap::ordenar_nodo( nodo* ptr_nodo )
{
		int temp;
		
		if( ptr_nodo->padre==NULL )
			return 0;
		
		else if( ptr_nodo->valor < ptr_nodo->padre->valor )
		  {
				  temp = ptr_nodo->valor;
				  ptr_nodo->valor = ptr_nodo->padre->valor;
				  ptr_nodo->padre->valor = temp;
				  
				  ordenar_nodo( ptr_nodo->padre );
		  }
		return 0;
}
//HERMANO DERECHO
nodo* heap::hermano_derecho( nodo* ptr_nodo )
{
		// Si soy raiz...
		if( ptr_nodo->padre==NULL)
			return( bajar_arbol( ptr_nodo->izquierdo ) );
		
		// Si soy hijo izquierdo...
		else if( ptr_nodo==ptr_nodo->padre->izquierdo )
			return( bajar_arbol( ptr_nodo->padre->derecho ) );
		
		// Si soy hijo derecho...
		else 
			return( hermano_derecho( ptr_nodo->padre ) );
		
}
//BAJAR ARBOL
nodo* heap::bajar_arbol( nodo* ptr_nodo )
{
		if( ptr_nodo->izquierdo==NULL)
			return ptr_nodo;
		
		else 
			return( bajar_arbol( ptr_nodo->izquierdo) );
}
////////////
//
//    MAIN
//
////////////
int main( void )
{
		printf("\n");
		
		class nodo N1;
		class nodo N2(20);
		class nodo N3(40);
		class nodo N4(30);
		class nodo N5(10);
		class nodo N6(15);
		class nodo N7(8);
		class nodo N8(4);
		
		class heap Arbol;
		Arbol.ver_min();

		Arbol.insert_nodo(&N1);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();
		
		Arbol.insert_nodo(&N2);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();
		
		Arbol.insert_nodo(&N3);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();
		
		Arbol.insert_nodo(&N4);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();

		Arbol.insert_nodo(&N5);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();
		
		Arbol.insert_nodo(&N6);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();
		
		Arbol.insert_nodo(&N7);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();
		
		Arbol.insert_nodo(&N8);
		printf("ARBOL: ");
		Arbol.mostrar_arbol( Arbol.ver_raiz() );
		printf("\n");
		Arbol.ver_min();

		return 0;
}
