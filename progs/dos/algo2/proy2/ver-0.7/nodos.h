#ifndef _nodos_
# define _nodos_

# include "infograv.h"
# include "infopac.h"

// Nodo InfoGrav
class nodoig {
public:
		nodoig * izq ;
		nodoig * der ;
		InfoGrav * datos ;
};

// Nodo InfoPac
class nodoip {
public:
		nodoip * izq ;
		nodoip * der ;
		InfoPac * datos ;
};
#endif
