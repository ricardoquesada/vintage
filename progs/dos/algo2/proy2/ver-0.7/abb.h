//
//
//
#ifndef _abb_
# define _abb_

# include "infograv.h"
# include "nodos.h"
# include "miniclas.h"

class ABB {
		nodoig * raiz ;
		nodoig * ultAcc ;
public:
		ABB( void );
		Bool Vacio( void);
		InfoGrav * Raiz( void );
		ABB * Si( void);
		ABB * Sd( void );
		void Insertar( InfoGrav * );
		Bool Esta( int );
		InfoGrav * Obtener( int );
		void Buscar( int );
};

#endif
