//
// Definicion del tipo Diccionario
//

#ifndef _dicc_
# define _dicc_

# include "miniclas.h"
# include "infograv.h"
# include "abb.h"

class Dicc {
		ABB * puntero ;
public:
		Dicc( );
		~Dicc( );
		void Agregar( InfoGrav * ) ;
		Bool Esta( int ) ;
		InfoGrav * Obtener( int ) ;
} ;

#endif
