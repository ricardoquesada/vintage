//
// Miembros del tipo GRATUR ( crear )
//

#include "gratur.h"

// Constructor

Gratur::Gratur( )
{
		Grav = 0;
		Tur = 0;
}
Gratur::Gratur( gravedad g, int i )
{
		Grav = g;
		Tur = i;
}
Gratur::Gratur( const Gratur &G )
{
		Grav = G.Grav ;
		Tur = G.Tur ;
}
Gratur &Gratur::operator=( const Gratur &G )
{
		Grav = G.Grav ;
		Tur = G.Tur ;
		return *this ;
}

gravedad Gratur::Gravedad( void )
{
		return Grav ;
}
int Gratur::Turno( void )
{
		return Tur ;
}
Gratur * Gratur::MasV( Gratur *b)
{
		return (Tur < b->Tur ? this : b );
}

// Miembros de sobrecarga de operadores
Bool Gratur::operator==( Gratur a )
{
		return ( Grav==a.Grav && Tur==a.Tur );
}

Bool Gratur::operator< ( Gratur a )
{
		return ( ( Grav < a.Grav ) ||
				( Grav == a.Grav  && Tur < a.Tur  ) ) ;
}
