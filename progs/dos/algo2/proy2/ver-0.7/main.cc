//
// Funcion principal
//
#include <iostream.h>
#include "abe.h"
#include "infopac.h"

int main( void )
{

		Gratur G1( 20,32 );
		Gratur G2( 15,79 );
		Gratur G3( 25,10 );
		
		Paciente P1( 23328, "Ricardo", "Bill 2444");
		Paciente P2( 22223, "Maria", "Estrada 733");
		Paciente P3( 22224, "Daniel", "Elpidio Gon 5053");
		
		InfoPac *IP1 = new InfoPac( G1, 2, P1 );
		InfoPac *IP2 = new InfoPac( G2, 2, P2 );
		InfoPac *IP3 = new InfoPac( G3, 2, P3 );
		
		ABE A;

		A.Insertar( IP1 );		
		A.Insertar( IP2 );
		A.Insertar( IP3 );

		cout << IP2->VerPaciente()->VerNombre() << "\n" ;
		cout << P2.VerNombre() << "\n";
		cout << A.Obtener( 23328 )->VerPaciente()->VerNombre() << "\n" ;
		cout << A.Obtener( 22223 )->VerPaciente()->VerNombre() << "\n" ;
		cout << A.Obtener( 22224 )->VerPaciente()->VerNombre() << "\n" ;
		cout << A.PrimeroIngresado()->VerPaciente()->VerNombre() << "\n" ;

		return 0 ;
}
