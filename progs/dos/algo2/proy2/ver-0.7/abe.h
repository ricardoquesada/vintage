//
// clase Arbol Binario Extendido (ABE)
//

#ifndef _abe_
# define _abe_

# include "paciente.h"
# include "nodos.h"
# include "infopac.h"
# include "miniclas.h"

class ABE {
		nodoip *raiz ;
		nodoip *ultAcc ;
		int Cantidad;
		
		// Miembros privados
		Bool Esta2( dni, nodoip ** );
		InfoPac * PrimIng( void );
		void Convertir( char *, int );
		// Miembros publicos
public:
		ABE( );
		ABE( const ABE &);
		Bool Vacio( void );
		InfoPac * Raiz( void );
		ABE* Si( void );
		ABE* Sd( void );
		void Insertar( InfoPac * );
		Bool Esta( dni );
		InfoPac * Obtener( dni );
		InfoPac * PrimeroIngresado( void );
		InfoPac * BorrarUltimo( void );
};
#endif
