//
// Definicion del Tipo Gratur
// version 0.01
//

#ifndef _Gratur_
# define _Gratur_

# include "miniclas.h"

typedef int gravedad ;

class Gratur {
		gravedad Grav ;
		int Tur ;
		
public:
		Gratur( );
		Gratur( gravedad, int ) ;
		Gratur( const Gratur & ) ;
		Gratur & operator=( const Gratur &) ;

		gravedad Gravedad( void ) ;
		int Turno( void ) ;
		Gratur* MasV( Gratur * ) ;
		Bool operator==( Gratur ) ;
		Bool operator< ( Gratur ) ;
};

#endif
