//
// Miembros del Tipo Paciente
//

#include "paciente.h"
#include <string.h>

Paciente::Paciente( )
{
		Dni = 0;
		Nombre[0]=0;
		Domicilio[0]=0;
}

Paciente::Paciente( dni id, nombre *n, domicilio *dom )
{
		Dni = id ;
		strcpy( Nombre, n );
		strcpy( Domicilio, dom );
}

Paciente::Paciente( const Paciente &pac )
{
		Dni = pac.Dni;
		strcpy( Nombre, pac.Nombre );
		strcpy( Domicilio, pac.Domicilio );
}

Paciente& Paciente::operator=( const Paciente &pac )
{
		Dni = pac.Dni;
		strcpy( Nombre, pac.Nombre );
		strcpy( Domicilio, pac.Domicilio );
		return *this ;
}

nombre * Paciente::VerNombre( void )
{
		return Nombre ;
}
domicilio * Paciente::VerDomicilio( void )
{
		return Nombre ;
}

dni Paciente::VerDni( void )
{
		return Dni ;
}

