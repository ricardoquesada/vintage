#include "infopac.h"
#include "gratur.h"

InfoPac::InfoPac( )
{
		Ident = Gratur() ;
		TotalAt =  0;
		Pac = Paciente();
}
InfoPac::InfoPac( Gratur g, int n , Paciente p )
{
		Ident = g ;
		TotalAt = n ;
		Pac = p ;
}
InfoPac::InfoPac( const InfoPac &ip)
{
		Ident = ip.Ident ;
		TotalAt = ip.TotalAt ;
		Pac = ip.Pac ;
}
InfoPac & InfoPac::operator=( const InfoPac &ip )
{
		Ident = ip.Ident ;
		TotalAt = ip.TotalAt ;
		Pac = ip.Pac ;
		return *this ;
}

Gratur * InfoPac::Id( void )
{
		return &Ident ;
}

Paciente * InfoPac::VerPaciente( void )
{
		return &Pac ;
}

int InfoPac::IngTotAt( void )
{
		return TotalAt ;
}

dni InfoPac::Dni( void )
{
		return  Pac.VerDni();
}
InfoPac* InfoPac::MasV( InfoPac *B )
{
		Gratur *x;
		
		x = Ident.MasV( &B->Ident ) ;
		if( &Ident == x )
			return this;
		else
			return B;
}
