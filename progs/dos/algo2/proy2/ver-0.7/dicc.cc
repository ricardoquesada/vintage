//
// Miembros del tipo Dicc
//
#include "infograv.h"
#include "dicc.h"
#include "abb.h"

Dicc::Dicc( )
{
		puntero = new ABB ;
}
Dicc::~Dicc()
{
		delete puntero ;
}

void Dicc::Agregar( InfoGrav * n )
{
		puntero->Insertar( n );
}
Bool Dicc::Esta( int n )
{
		return( puntero->Esta( n ) );
}
InfoGrav * Dicc::Obtener( int n )
{
		return( puntero->Obtener( n )  ) ;
};
