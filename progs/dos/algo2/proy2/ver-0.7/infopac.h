// Definicion de la clase Info Pac

#ifndef _InfoPac_
# define _InfoPac_

# include "gratur.h"
# include "paciente.h"

class InfoPac {
		Gratur Ident ;
		int TotalAt ;
		Paciente Pac ;
		
public:
		InfoPac( );                              // Default Constructor
		InfoPac( Gratur , int, Paciente );       // Otro Constructor
		InfoPac( const InfoPac & );              // Copy Constructor
		InfoPac & operator=( const InfoPac & );  // = overload
		
		Gratur * Id( void );
		Paciente * VerPaciente( void );
		int IngTotAt( void );
		dni Dni( void );
		InfoPac * MasV(  InfoPac * );
} ;

#endif
