//
// clase Arbol Binario Extendido (ABE)
//

#include <string.h>
#include <math.h>
#include "abe.h"
#include "infopac.h"
#include "paciente.h"
#include "nodos.h"
#include "miniclas.h"

#define INS_CHAR 100

ABE::ABE( )
{
		raiz = NULL ;
		ultAcc= NULL;
		Cantidad = 0;
}
ABE::ABE( const ABE &abe )
{
		raiz = abe.raiz ;
		ultAcc= abe.ultAcc ;
		Cantidad = abe.Cantidad ;
}

Bool ABE::Vacio( void )
{
		return ( raiz==NULL );
}
ABE* ABE::Si( void )
{
		int a,b,c ;
		ABE *aux ;
		aux = new ABE ;
		aux->raiz=raiz->izq ;
		aux->ultAcc=NULL ;
		a= (int) ( log10( (double) Cantidad ) / log10( (double) 2 ) ) ;
		b= (int) pow(2 ,a+1 );
		c= (int) pow(2 , a  );
		if ( ( (b-c) /2 ) <= Cantidad )
			aux->Cantidad = (b-1)/2  ;
		else
			aux->Cantidad = Cantidad-(((c-1)/2)-1) ;
		return aux ;
}
ABE* ABE::Sd( void )
{
		int a,b,c ;
		ABE *aux ;
		aux = new ABE ;
		aux->raiz=raiz->der ;
		aux->ultAcc=NULL ;
		a= (int) ( log10( (double) Cantidad ) / log10( (double) 2 ) ) ;
		b= (int) pow( 2 , a+1) ;
		c= (int) pow( 2 , a ) ;
		if (( (b-c) /2 ) >= Cantidad )
			aux->Cantidad = (c-1)/2  ;
		else
			aux->Cantidad = Cantidad-(((b-1)/2)-1) ;
		return aux ;
}

void ABE::Insertar( InfoPac *t )
{
		// Para simplificar usamos una cadena de char
		// con un valor fijo, aunque esto se puede modificar
		// para que sea ilimitado. Por ejemplo utilizando una lista.
		
		char seq[INS_CHAR];
		char *seq2;
		nodoip * actual ;
		nodoip * nuevo_nodo ;
		
		Cantidad++;
		Convertir( seq, Cantidad );
		seq2 = &seq[1] ;
		nuevo_nodo = new nodoip ;
		nuevo_nodo->datos = t;
		nuevo_nodo->izq = NULL ;
		nuevo_nodo->der = NULL ;
		if (strlen(seq2)==0)
			raiz=nuevo_nodo ;
		else
		  {
				  actual=raiz ;
				  while( strlen(seq2)> 1 )
					{
							if( seq2[0]=='1' )
								actual=actual->der;
							else
								actual=actual->izq;
							seq2=&seq2[1];
					}
				  if( seq2[0]=='1')
					  actual->der=nuevo_nodo ;
				  else
					  actual->izq=nuevo_nodo ;
		  }
}
void ABE::Convertir( char *s , int c )
{
		int i ;
		int cant ;
		
		cant = (int) (log10(c) / log10(2)) ;
		
		s[cant+1]=0;
		
		for( i=0; i<=cant ; i++ )
		  {
				  if( ( c & 1 ) == 1 )
					  s[cant-i]='1';
				  else
					  s[cant-i]='0';
				  c >>= 1 ;
		  }
}
Bool ABE::Esta( dni d )
{
		nodoip *t;
		Bool a;
		
		t=NULL ;
		
		if( (ultAcc!=NULL) && (ultAcc->datos->Dni() == d) )
			return TRUE;
		else if (raiz==NULL)
			return FALSE;
		else {
				t=NULL ;
				a = Esta2 ( d, &t);
				ultAcc = t;
				return a;
		}
}
Bool ABE::Esta2( dni d, nodoip ** t )
{
		if( raiz==NULL )
		  {
				  return FALSE ;
		  }
		else
		  {
				  if( raiz->datos->Dni() == d )
					{
							t[0] = raiz ;
							return TRUE ;
					}
				  else
					{
							return ( Si()->Esta2(d, t) || Sd()->Esta2(d, t) );
					}
		  }
}


InfoPac* ABE::Obtener( dni d )
{
		if( Esta( d ) )
			return (InfoPac * )ultAcc->datos;
		else
			return (InfoPac * )NULL;
}

InfoPac* ABE::PrimeroIngresado( void )
{
		InfoPac *A;
		
		A=PrimIng( );
		Esta( A->Dni() );
		return A;
}
InfoPac* ABE::PrimIng( void )
{
		InfoPac *A;
		
		if(( raiz->izq==NULL) && ( raiz->der==NULL ))
			A=raiz->datos ;
		else if( raiz->der==NULL )
			A=raiz->datos->MasV( 
								Si()->PrimIng() 
								);
		else if( raiz->izq==NULL )
			A=raiz->datos->MasV( 
								Sd()->PrimIng() 
								);
		else
			A=raiz->datos->MasV( 
								Si()->PrimIng()->MasV( 
													  Sd()->PrimIng() 
													  ) 
								);
		return A;
}
