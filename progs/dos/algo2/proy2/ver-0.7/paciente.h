// Definicion del tipo paciente

#ifndef _Paciente_
#define _Paciente_

typedef int dni ;
typedef char nombre ;
typedef char domicilio ;

class Paciente {
		dni Dni ;
		nombre Nombre[30] ;
		domicilio Domicilio[30] ;
		
public:
		Paciente( );
		Paciente( dni, nombre *, domicilio *) ;
		Paciente( const Paciente & );
		Paciente &operator=( const Paciente & );

		dni VerDni( void ) ;
		domicilio * VerDomicilio( void ) ;
		nombre * VerNombre( void ) ;
} ;
#endif
