//
// Miembros del Arbol Binario
//

#include <iostream.h>
#include "abb.h"

ABB::ABB()
{
		raiz= NULL;
		ultAcc=NULL;
}

Bool ABB::Vacio( void )
{
		return (ultAcc==NULL);
}
InfoGrav * ABB::Raiz( void )
{
		return( raiz->datos );
}
ABB * ABB::Si( void )
{
		ABB *ptr;
		ptr = new ABB ;
		if( !ptr )
		  {
				  cout << "ERROR: fn. ABB.Si() sin memoria\n";
		  }
		ptr->raiz = raiz->izq ;
		ptr->ultAcc = NULL ;
		return ptr ;
}
ABB * ABB::Sd( void )
{
		ABB *ptr;
		ptr = new ABB ;
		if( !ptr )
		  {
				  cout << "ERROR: fn. ABB.Sd() sin memoria\n";
		  }
		ptr->raiz = raiz->der ;
		ptr->ultAcc = NULL ;
		return ptr ;
}

void ABB::Insertar( InfoGrav * t)
{
		nodoig * ptr;
		
		if( (ultAcc==NULL) || ( ultAcc->datos->Id() != t->Id() ) )
			Buscar( t->Id() );
		
		ptr = new nodoig ;
		ptr->datos=t ;
		ptr->izq=NULL;
		ptr->der=NULL;
		
		if( ultAcc==NULL )
			raiz=ptr ;
		else if ( ( ultAcc != NULL ) && ( ultAcc->datos->Id() < t->Id() ) )
			ultAcc->der=ptr ;
		else if ( ( ultAcc != NULL ) && ( ultAcc->datos->Id() > t->Id() ) )
			ultAcc->izq=ptr ;
}
Bool ABB::Esta( int n )
{
		if( (ultAcc==NULL) || (ultAcc->datos->Id() !=n ) )
			Buscar( n );
		return ( ultAcc!=NULL ) && ( ultAcc->datos->Id() == n ) ;
}
InfoGrav * ABB::Obtener( int n )
{
		if( (ultAcc==NULL ) || ( ultAcc->datos->Id() !=n) )
			Buscar( n );
		return ( ultAcc->datos  );
		
}
void ABB::Buscar( int n )
{
		nodoig * actual ;
		
		if( (ultAcc==NULL) || ( (ultAcc!=NULL) && (ultAcc->datos->Id() != n) ))
			actual=raiz ;
		else
			actual=ultAcc ;
		
		ultAcc= actual ;
		
		while( actual!=NULL && (ultAcc->datos->Id()!= n) )
		  {
				  ultAcc=actual ;
				  if(ultAcc->datos->Id() > n)
					  actual=ultAcc->izq ;
				  else
					  actual=ultAcc->der ;
		  }
}
