;======================================;
;                                      ;
;              Rotaciones              ;
;                by Riq                ;
;                                      ;                                              ;
;======================================;
;                                      ;
;         Copyright (c) 1995.          ;
;                                      ;
;======================================;

DOSSEG
MODEL SMALL

;======================================; INCLUDES

;======================================; MACROS

;======================================; DEFINES
VERT_RESCAN = 3DAh                     ; Vertical Rescan
ADD_X       = 160
ADD_Y       = 100

;======================================; STACK
STACK 200H

;======================================; DATA
DATASEG

;SinTable created by Jon Beltran de Heredia
;Sines are in 8.8 fixedpoint, from 0 degree to 360+90 degree (in order to
; calculate the cosines with the same table)
SinTbl	LABEL	WORD
dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256
dw 256,256,256,256,255,255,255,254,254,253,252,251,250,249,248
dw 247,246,245,243,242,241,239,237,236,234,232,230,228,226,224
dw 222,219,217,215,212,210,207,204,202,199,196,193,190,187,184
dw 181,178,175,171,168,165,161,158,154,150,147,143,139,136,132
dw 128,124,120,116,112,108,104,100,96,92,88,83,79,75,71
dw 66,62,58,53,49,44,40,36,31,27,22,18,13,9,4
dw 0,-4,-9,-13,-18,-22,-27,-31,-36,-40,-44,-49,-53,-58,-62
dw -66,-71,-75,-79,-83,-88,-92,-96,-100,-104,-108,-112,-116,-120,-124
dw -128,-132,-136,-139,-143,-147,-150,-154,-158,-161,-165,-168,-171,-175,-178
dw -181,-184,-187,-190,-193,-196,-199,-202,-204,-207,-210,-212,-215,-217,-219
dw -222,-224,-226,-228,-230,-232,-234,-236,-237,-239,-241,-242,-243,-245,-246
dw -247,-248,-249,-250,-251,-252,-253,-254,-254,-255,-255,-255,-256,-256,-256
dw -256,-256,-256,-256,-255,-255,-255,-254,-254,-253,-252,-251,-250,-249,-248
dw -247,-246,-245,-243,-242,-241,-239,-237,-236,-234,-232,-230,-228,-226,-224
dw -222,-219,-217,-215,-212,-210,-207,-204,-202,-199,-196,-193,-190,-187,-184
dw -181,-178,-175,-171,-168,-165,-161,-158,-154,-150,-147,-143,-139,-136,-132
dw -128,-124,-120,-116,-112,-108,-104,-100,-96,-92,-88,-83,-79,-75,-71
dw -66,-62,-58,-53,-49,-44,-40,-36,-31,-27,-22,-18,-13,-9,-4
dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256,256

Dots   label word                      ; X,Y,Angle
       dw   50,-50,1,0
       dw   50,50,1,0
       dw   -50,-50,1,0
       dw   -50,50,1,0
TOTALDOTS = ($-DOTS)/8

Vertex_Rot  label word
       dw   TOTALDOTS dup(0,0,0,0)

Matrix_Rot  label word                 ; Datos usados por Mul_Matrix
       dw   0,0,0
       dw   0,0,0
       dw   0,0,0

_X     dw   0                          ; Datos Temporales de Mul_Matrix
_Y     dw   0

angle  dw 0                            ; 0 - 360
addangle dw 1

;======================================; CODE
CODESEG
       STARTUPCODE
P286
;======================================;
;      Mainprogram                     ;
;======================================;
Mainprogram proc
       cld
       mov  ax,@DATA
       mov  ds,ax
       mov  ax,0a000h
       mov  es,ax
       mov  ax,13h
       int  10h                        ; Sets standard 320x200x256

       Call Main_Loop

       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h
       ret
Mainprogram endp

;======================================;
;      Main_Loop                       ;
;======================================;
Main_Loop   proc

M_Loop1:

       Call New_Angle
       Call Clean_Dots
       Call Rot_Dots
       Call Draw_Dots

       Call VerticalR
       Call WaitEsc
       jne  M_Loop1
       ret
Main_Loop   endp

;======================================;
;      New_Angle                       ;
;======================================;
New_Angle   proc
       mov  bx,angle
       lea  di,Matrix_Rot
       Call Init_Matrix

       mov  bx,addangle
       add  angle,bx
       cmp  angle,360
       jb   New_Angle_
       mov  angle,0

       cmp  addangle,360
       ja   Reset_Angle
       inc  addangle
       jmp  New_Angle_
Reset_Angle:
       mov  addangle,1

New_Angle_:
       ret
New_Angle   endp


;======================================;
;      Draw_Dots                       ;
;======================================;
Draw_Dots   proc
       xor  cx,cx                      ; Dots Counter
DD_loop:
       push cx
       lea  si,Vertex_Rot              ; Donde esta el nuevo valor
       shl  cx,3                       ; Multiplica * 8 ( 4 * 2 )
       add  si,cx
       mov  ax,[ si + 0 ]              ; X
       mov  bx,[ si + 2 ]              ; Y
       mov  cx,15                      ; Color
       add  ax,ADD_X
       add  bx,ADD_Y
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  DD_loop

       ret
Draw_Dots   endp

;======================================;
;      Clean_Dots                      ;
;======================================;
Clean_Dots   proc
       xor  cx,cx                      ; Dots Counter
CD_loop:
       push cx
       lea  si,Vertex_Rot
       shl  cx,3                       ; Multiplica * 8
       add  si,cx
       mov  ax,[ si + 0 ]              ; X
       mov  bx,[ si + 2 ]              ; Y
       add  ax,ADD_X
       add  bx,ADD_Y
       xor  cx,cx                      ; Color
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  CD_loop

       ret
Clean_Dots   endp

;======================================;
;      Rot_Dots                        ;
;======================================;
Rot_Dots   proc
       pusha
       xor  cx,cx                      ; Dots Counter
RD_loop:
       push cx
       lea  di,Dots
       lea  bx,Vertex_Rot
       lea  si,Matrix_Rot
       shl  cx,3                       ; Multiplica * 8
       add  di,cx
       add  bx,cx

       mov  ax,[ di + 0 ]              ; X
       mov  cx,[ di + 2 ]              ; Y
       mov  [ bx + 0 ],ax
       mov  [ bx + 2 ],cx
       mov  cx,1
       mov  [ bx + 4 ],cx
       call Mul_Matrix                 ; [BX] = New Pos

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  RD_loop

       popa
       ret
Rot_Dots   endp


;======================================;
;      Mul_Matrix                      ;
;======================================;
;      In:  bx -> offset Vertex (1x3)  ;
;           si -> offset Matrix (3x3)  ;
;      Out: bx -> offset new Vertex    ;
;======================================;

.386
Mul_Matrix  proc
       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + 0 ]              ; Cos
       imul cx
       shrd ax,dx,8
       mov  _X,ax                      ; Calcule X * cos

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + 6 ]              ; Sin
       imul cx
       shrd ax,dx,8
       add  _X,ax                      ; Calcule Y * sin

       ;;;

       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + 2 ]              ; -Sin
       imul cx
       shrd ax,dx,8
       mov  _Y,ax                      ; Calcule X * cos

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + 8 ]              ; Cos
       imul cx
       shrd ax,dx,8
       add  _Y,ax                      ; Calcule Y * sin

       mov  ax,_X
       mov  [ bx + 0 ],ax
       mov  ax,_Y
       mov  [ bx + 2 ],ax              ;
       ret
Mul_Matrix  endp


;======================================;
;      Init_Matrix                     ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix proc
       pusha
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + 0 ],cx              ;  cos
       mov  [ di + 8 ],cx              ;  cos
       mov  [ di + 6 ],ax              ;  sin
       neg  ax
       mov  [ di + 2 ],ax              ; -sin
       popa
       ret
Init_Matrix endp


;======================================;
;      Put_Dot                         ;
;======================================;
;      In:  ax -> X                    ;
;           bx -> Y                    ;
;           cl -> Color                ;
;======================================;
Put_Dot     proc
       pusha
       mov  dx,bx
       shl  bx,8                       ; = bx * 320 = bx * 256 + bx * 64 =
       shl  dx,6                       ; = bx * 2^8 + bx * 2^6
       add  bx,dx
       add  bx,ax                      ; BX -> Offset del Video
       mov  es:[ bx ],cl               ; Cl
       popa
       ret
Put_Dot     endp

;======================================;
;      WaitEsc                         ;
;======================================;
waitesc proc
       push ax
       in   al,060h
       cmp  al,01h
       pop  ax
       ret
waitesc endp

;======================================;
;      VerticalR                       ;
;======================================;
verticalr proc
       mov  dx,VERT_RESCAN
ula1:
       in   al,dx            ;vertical rescan
       test al,8
       jne  ula1
ula2:
       in   al,dx
       test al,8
       je   ula2
       ret
verticalr endp

END
