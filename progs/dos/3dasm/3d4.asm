;======================================;
;                                      ;
;              Rotaciones              ;
;                by Riq                ;
;                                      ;                                              ;
;======================================;
;                                      ;
;     Fecha de inicio: 12 / 4 / 95     ;
;              Version 1.1             ;
;            Fixed Point 8.8           ;
;======================================;
;                                      ;
;          Copyright (c) 1995.         ;
;                                      ;
;======================================;

DOSSEG
MODEL SMALL
P386
;======================================; INCLUDES


;======================================; DEFINES
VERT_RESCAN = 3DAh                     ; Vertical Rescan
ADD_X       = 160
ADD_Y       = 100
BORDERS     = 0

;======================================; MACROS
setborder MACRO col
       IF BORDERS
       push ax
       push dx
       mov  dx,3dah
       in   al,dx
       mov  dx,3c0h
       mov  al,11h+32
       out  dx,al
       mov  al,col
       out  dx,al
       pop  dx
       pop  ax
       ENDIF
       ENDM

;======================================; STACK
STACK 200H

;======================================; DATA
DATASEG

;SinTable created by Jon Beltran de Heredia
;Sines are in 8.8 fixedpoint, from 0 degree to 360+90 degree (in order to
; calculate the cosines with the same table)
SinTbl	LABEL	WORD
dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256
dw 256,256,256,256,255,255,255,254,254,253,252,251,250,249,248
dw 247,246,245,243,242,241,239,237,236,234,232,230,228,226,224
dw 222,219,217,215,212,210,207,204,202,199,196,193,190,187,184
dw 181,178,175,171,168,165,161,158,154,150,147,143,139,136,132
dw 128,124,120,116,112,108,104,100,96,92,88,83,79,75,71
dw 66,62,58,53,49,44,40,36,31,27,22,18,13,9,4
dw 0,-4,-9,-13,-18,-22,-27,-31,-36,-40,-44,-49,-53,-58,-62
dw -66,-71,-75,-79,-83,-88,-92,-96,-100,-104,-108,-112,-116,-120,-124
dw -128,-132,-136,-139,-143,-147,-150,-154,-158,-161,-165,-168,-171,-175,-178
dw -181,-184,-187,-190,-193,-196,-199,-202,-204,-207,-210,-212,-215,-217,-219
dw -222,-224,-226,-228,-230,-232,-234,-236,-237,-239,-241,-242,-243,-245,-246
dw -247,-248,-249,-250,-251,-252,-253,-254,-254,-255,-255,-255,-256,-256,-256
dw -256,-256,-256,-256,-255,-255,-255,-254,-254,-253,-252,-251,-250,-249,-248
dw -247,-246,-245,-243,-242,-241,-239,-237,-236,-234,-232,-230,-228,-226,-224
dw -222,-219,-217,-215,-212,-210,-207,-204,-202,-199,-196,-193,-190,-187,-184
dw -181,-178,-175,-171,-168,-165,-161,-158,-154,-150,-147,-143,-139,-136,-132
dw -128,-124,-120,-116,-112,-108,-104,-100,-96,-92,-88,-83,-79,-75,-71
dw -66,-62,-58,-53,-49,-44,-40,-36,-31,-27,-22,-18,-13,-9,-4
dw 0,4,9,13,18,22,27,31,36,40,44,49,53,58,62
dw 66,71,75,79,83,88,92,96,100,104,108,112,116,120,124
dw 128,132,136,139,143,147,150,154,158,161,165,168,171,175,178
dw 181,184,187,190,193,196,199,202,204,207,210,212,215,217,219
dw 222,224,226,228,230,232,234,236,237,239,241,242,243,245,246
dw 247,248,249,250,251,252,253,254,254,255,255,255,256,256,256,256

Vertex_Sca   label word                      ; X,Y,Z,1
       dw    30,-30,-30,100h
       dw    30, 30,-30,100h
       dw   -30,-30,-30,100h
       dw   -30, 30,-30,100h
       dw    30,-30, 30,100h
       dw    30, 30, 30,100h
       dw   -30,-30, 30,100h
       dw   -30, 30, 30,100h
       ;;;
       dw    25,-25,-25,100h
       dw    25, 25,-25,100h
       dw   -25,-25,-25,100h
       dw   -25, 25,-25,100h
       dw    25,-25, 25,100h
       dw    25, 25, 25,100h
       dw   -25,-25, 25,100h
       dw   -25, 25, 25,100h
       ;;;
       dw    20,-20,-20,100h
       dw    20, 20,-20,100h
       dw   -20,-20,-20,100h
       dw   -20, 20,-20,100h
       dw    20,-20, 20,100h
       dw    20, 20, 20,100h
       dw   -20,-20, 20,100h
       dw   -20, 20, 20,100h
       ;;;
       dw    15,-15,-15,100h
       dw    15, 15,-15,100h
       dw   -15,-15,-15,100h
       dw   -15, 15,-15,100h
       dw    15,-15, 15,100h
       dw    15, 15, 15,100h
       dw   -15,-15, 15,100h
       dw   -15, 15, 15,100h
       ;;;
       dw    10,-10,-10,100h
       dw    10, 10,-10,100h
       dw   -10,-10,-10,100h
       dw   -10, 10,-10,100h
       dw    10,-10, 10,100h
       dw    10, 10, 10,100h
       dw   -10,-10, 10,100h
       dw   -10, 10, 10,100h
       ;;;

TOTALDOTS = ($-VERTEX_SCA)/8

Vertex_Rot  label word
       dw   TOTALDOTS dup(0,0,0,0)

Matrix_Rot  label word                 ; Datos usados por Mul_Matrix
m00    dw   0,0,0,0
m01    dw   0,0,0,0
m02    dw   0,0,0,0
m03    dw   0,0,0,0

Matrix_Sca  label word                 ; Datos usados por Mul_Matrix
       dw  100h,   0,   0,   0
       dw     0,100h,   0,   0
       dw     0,   0,100h,   0
       dw     0,   0,   0,100h

p00    = 0                             ; Defines inline
_X     dw   0                          ; Datos Temporales de Mul_Matrix
p01    = $ - _X
_Y     dw   0
p02    = $ - _X
_Z     dw   0
p03    = $ - _X

angle  dw 0                            ; 0 - 360
addangle dw 1
addscale dw 0                          ; Contador de New_Scale
out_   db   0                          ; Ver si ya se apreto Esc.
out2   dw   0                          ; Contador del final
scaleNow    dw 0                       ; Contador para empezar la Scale

;======================================; CODE
CODESEG
       STARTUPCODE
;======================================;
;      Mainprogram                     ;
;======================================;
Mainprogram proc
       cld
       mov  ax,@DATA
       mov  ds,ax
       mov  ax,0a000h
       mov  es,ax
       mov  ax,13h
       int  10h                        ; Sets standard 320x200x256

       Call Main_Loop

       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h
       ret
Mainprogram endp

;======================================;
;      Main_Loop                       ;
;======================================;
Main_Loop   proc

       Mov  Out_,0
       Mov  ScaleNow,0
M_Loop1:
       Call New_Angle
       Cmp  ScaleNow,600
       jb   NoScale
       Call New_Scale
       dec  ScaleNow
NoScale:
       inc  ScaleNow
       Call Clean_Dots
       Call Rot_Dots
       Call Sca_Dots
       Call Draw_Dots
       Call VerticalR
       Call WaitEsc
       jne  M_Loop1

       Mov  Out_,1
       Mov  Out2,0
M_Loop2:
       Inc  Out2
       Call New_Angle
       Call New_Scale
       Call Clean_Dots
       Call Rot_Dots
       Call Sca_Dots
       Call Draw_Dots
       Call VerticalR
       Cmp  Out2,100
       jne  M_Loop2

       ret
Main_Loop   endp

;======================================;
;      New_Angle                       ;
;======================================;
New_Angle   proc
       mov  bx,angle
       lea  di,Matrix_Rot
       cmp  addangle,1
       je   a_z
       cmp  addangle,2
       je   a_a
       cmp  addangle,3
       je   a_y
       cmp  addangle,4
       je   a_b
       cmp  addangle,5
       je   a_x

a_c:
       call Init_Matrix_C
       jmp a_fin
a_b:
       call Init_Matrix_B
       jmp a_fin
a_a:
       call Init_Matrix_A
       jmp a_fin

a_x:
       call Init_Matrix_X
       jmp  a_fin
A_Z:
       Call Init_Matrix_Z
       jmp  a_fin
a_Y:
       Call INit_Matrix_Y

a_fin:
       mov  bx,3
       add  angle,bx
       cmp  angle,360
       jb   New_Angle_
       mov  angle,0

       cmp  addangle,6
       je   Reset_Angle
       inc  addangle
       jmp  New_Angle_
Reset_Angle:
       mov  addangle,1

New_Angle_:
       ret
New_Angle   endp

;======================================;
;      New_Scale                       ;
;======================================;
New_Scale   proc
       lea  di,Matrix_Sca
       cmp  out_,1
       jne  No_out

       Call Init_Matrix_Out
       jmp  s_fin
No_out:
       cmp  addscale,20
       jb   s_2
       cmp  addscale,40
       jb   s_3
       cmp  addscale,60
       jb   s_4
       cmp  addscale,80
       jb   s_5
       cmp  addscale,100
       jb   s_6

s_1:
       call Init_Matrix_S1
       jmp  s_fin
s_2:
       call Init_Matrix_S2
       jmp  s_fin
s_3:
       call Init_Matrix_S3
       jmp  s_fin
s_4:
       call Init_Matrix_S4
       jmp  s_fin
s_5:
       Call Init_Matrix_S5
       jmp  s_fin
s_6:
       Call INit_Matrix_S6

s_fin:
       inc  addscale
       cmp  addscale,120
       jb   New_Scale_

       mov  addscale,0

New_Scale_:
       ret
New_Scale   endp



;======================================;
;      Draw_Dots                       ;
;======================================;
Draw_Dots   proc
       xor  cx,cx                      ; Dots Counter
DD_loop:
       push cx
       lea  si,Vertex_Rot              ; Donde esta el nuevo valor
       shl  cx,3                       ; Multiplica * 8 ( 4 * 2 )
       add  si,cx
       mov  ax,[ si + 0 ]              ; X
       mov  bx,[ si + 2 ]              ; Y
       mov  cx,15                      ; Color
       add  ax,ADD_X
       add  bx,ADD_Y
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  DD_loop

       ret
Draw_Dots   endp

;======================================;
;      Clean_Dots                      ;
;======================================;
Clean_Dots   proc
       xor  cx,cx                      ; Dots Counter
CD_loop:
       push cx
       lea  si,Vertex_Rot
       shl  cx,3                       ; Multiplica * 8
       add  si,cx
       mov  ax,[ si + 0 ]              ; X
       mov  bx,[ si + 2 ]              ; Y
       add  ax,ADD_X
       add  bx,ADD_Y
       xor  cx,cx                      ; Color
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  CD_loop

       ret
Clean_Dots   endp

;======================================;
;      Rot_Dots                        ;
;======================================;
Rot_Dots   proc
       pusha
       xor  cx,cx                      ; Dots Counter
RD_loop:
       push cx
       lea  di,Vertex_Sca
       lea  bx,Vertex_Rot
       lea  si,Matrix_Rot
       shl  cx,3                       ; Multiplica * 8
       add  di,cx
       add  bx,cx

       mov  ax,[ di + 0 ]              ; X
       mov  [ bx + 0 ],ax
       mov  ax,[ di + 2 ]              ; Y
       mov  [ bx + 2 ],ax
       mov  ax,[ di + 4 ]              ; Z
       mov  [ bx + 4 ],ax
       mov  ax,[ di + 6 ]              ; 1
       mov  [ bx + 6 ],ax
       call Mul_Matrix                 ; [BX] = New Pos

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  RD_loop

       popa
       ret
Rot_Dots   endp

;======================================;
;      Sca_Dots                        ;
;======================================;
Sca_Dots   proc
       pusha
       xor  cx,cx                      ; Dots Counter
SD_loop:
       push cx
       lea  bx,Vertex_Sca
       lea  si,Matrix_Sca
       shl  cx,3                       ; Multiplica * 8
       add  bx,cx

       call Mul_Matrix                 ; [BX] = New Pos

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  SD_loop

       popa
       ret
Sca_Dots   endp

;======================================;
;      Mul_Matrix                      ;
;======================================;
;      In:  bx -> offset Vertex (1x4)  ;
;           si -> offset Matrix (4x4)  ;
;      Out: bx -> offset new Vertex    ;
;======================================;
;
;  [ x y z 1 ]  *  [ A B C D ]
;                  [ E F G H ]
;                  [ I J K L ]
;                  [ M N O P ]
;
Mul_Matrix  proc
       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + m00 - m00 + 0 ]  ; A...
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       mov  _X,ax                      ; ....

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + m01 - m00 + 0 ]  ; ....
       imul cx                         ; E...
       shrd ax,dx,8                    ; ....
       add  _X,ax                      ; ....

       mov  ax,[ bx + 4 ]              ; AX = Z
       mov  cx,[ si + m02 - m00 + 0 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; I...
       add  _X,ax                      ; ....

       mov  ax,[ bx + 6 ]              ; AX = 1
       mov  cx,[ si + m03 - m00 + 0 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       add  _X,ax                      ; M...

       ;;;

       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + m00 - m00 + 2 ]  ; .B..
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       mov  _Y,ax                      ; ....

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + m01 - m00 + 2 ]  ; ....
       imul cx                         ; .F..
       shrd ax,dx,8                    ; ....
       add  _Y,ax                      ; ....

       mov  ax,[ bx + 4 ]              ; AX = Z
       mov  cx,[ si + m02 - m00 + 2 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; .J..
       add  _Y,ax                      ; ....

       mov  ax,[ bx + 6 ]              ; AX = 1
       mov  cx,[ si + m03 - m00 + 2 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       add  _Y,ax                      ; .N..

       ;;;

       mov  ax,[ bx + 0 ]              ; AX = X
       mov  cx,[ si + m00 - m00 + 4 ]  ; ..C.
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       mov  _Z,ax                      ; ....

       mov  ax,[ bx + 2 ]              ; AX = Y
       mov  cx,[ si + m01 - m00 + 4 ]  ; ....
       imul cx                         ; ..G.
       shrd ax,dx,8                    ; ....
       add  _Z,ax                      ; ....

       mov  ax,[ bx + 4 ]              ; AX = Z
       mov  cx,[ si + m02 - m00 + 4 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ..K.
       add  _Z,ax                      ; ....

       mov  ax,[ bx + 6 ]              ; AX = 1
       mov  cx,[ si + m03 - m00 + 4 ]  ; ....
       imul cx                         ; ....
       shrd ax,dx,8                    ; ....
       add  _Z,ax                      ; ..O.

       mov  ax,_X
       mov  [ bx + 0 ],ax              ;
       mov  ax,_Y
       mov  [ bx + 2 ],ax              ;
       mov  ax,_Z
       mov  [ bx + 4 ],ax              ;

       ret
Mul_Matrix  endp

;======================================;
;      Init_Matrix                     ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix proc
       mov  ax,100h
       xor  cx,cx
       mov  [ di + m00 - m00 + 0 ],ax  ; 1
       mov  [ di + m00 - m00 + 2 ],cx
       mov  [ di + m00 - m00 + 4 ],cx
       mov  [ di + m00 - m00 + 6 ],cx
       ;;;
       mov  [ di + m01 - m00 + 0 ],cx
       mov  [ di + m01 - m00 + 2 ],ax  ; 1
       mov  [ di + m01 - m00 + 4 ],cx
       mov  [ di + m01 - m00 + 6 ],cx
       ;;;
       mov  [ di + m02 - m00 + 0 ],cx
       mov  [ di + m02 - m00 + 2 ],cx
       mov  [ di + m02 - m00 + 4 ],ax  ; 1
       mov  [ di + m02 - m00 + 6 ],cx
       ;;;
       mov  [ di + m03 - m00 + 0 ],cx
       mov  [ di + m03 - m00 + 2 ],cx
       mov  [ di + m03 - m00 + 4 ],cx
       mov  [ di + m03 - m00 + 6 ],ax  ; 1
       ;;;
       ret
Init_Matrix endp

;======================================;
;      Init_Matrix_Out                 ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_Out proc
       pusha
       Call Init_Matrix
       mov  ax,-2h
       mov  bx,-2h
       mov  cx,1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_Out endp

;======================================;
;      Init_Matrix_S1                  ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_S1 proc
       pusha
       Call Init_Matrix
       mov  ax,-2h
       mov  bx,-2h
       mov  cx,1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_S1 endp



;======================================;
;      Init_Matrix_S2                  ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_S2 proc
       pusha

       Call Init_Matrix
       mov  ax,1h
       mov  bx,1h
       mov  cx,1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_S2 endp

;======================================;
;      Init_Matrix_S3                  ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_S3 proc
       pusha

       Call Init_Matrix
       mov  ax,1h
       mov  bx,1h
       mov  cx,1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_S3 endp



;======================================;
;      Init_Matrix_S4                  ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_S4 proc
       pusha

       Call Init_Matrix
       mov  ax,1h
       mov  bx,-1h
       mov  cx,-1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_S4 endp

;======================================;
;      Init_Matrix_S5                  ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_S5 proc
       pusha

       Call Init_Matrix
       mov  ax,1h
       mov  bx,0h
       mov  cx,-1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_S5 endp

;======================================;
;      Init_Matrix_S6                  ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix_S6 proc
       pusha

       Call Init_Matrix
       mov  ax,-2h
       mov  bx,1h
       mov  cx,-1h
       mov  [ di + m03 - m00 + 0 ],ax  ;
       mov  [ di + m03 - m00 + 2 ],bx  ;
       mov  [ di + m03 - m00 + 4 ],cx  ;

       popa
       ret
Init_Matrix_S6 endp



;======================================;
;      Init_Matrix_A                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_A proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m00 - m00 + 2 ],ax  ; -sin
       mov  [ di + m00 - m00 + 4 ],cx  ;  sin

       mov  [ di + m01 - m00 + 4 ],cx  ; -sin

       neg  ax
       mov  [ di + m02 - m00 + 0 ],ax  ;  cos
       mov  [ di + m02 - m00 + 2 ],ax  ;  cos

       popa
       ret
Init_Matrix_A endp

;======================================;
;      Init_Matrix_B                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_B proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin

       mov  [ di + m00 - m00 + 0 ],cx
       mov  [ di + m00 - m00 + 2 ],ax
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       popa
       ret
Init_Matrix_B endp


;======================================;
;      Init_Matrix_C                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_C proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin

       mov  [ di + m00 - m00 + 0 ],cx
       mov  [ di + m00 - m00 + 2 ],ax
       mov  [ di + m01 - m00 + 0 ],cx
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       mov  [ di + m01 - m00 + 0 ],ax
       popa
       ret
Init_Matrix_C endp


;======================================;
;      Init_Matrix_X                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_X proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m01 - m00 + 4 ],ax  ;  sin
       neg  ax
       mov  [ di + m02 - m00 + 2 ],ax  ; -sin
       popa
       ret
Init_Matrix_X endp

;======================================;
;      Init_Matrix_Y                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_Y proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m00 - m00 + 0 ],cx  ;  cos
       mov  [ di + m02 - m00 + 4 ],cx  ;  cos
       mov  [ di + m02 - m00 + 0 ],ax  ;  sin
       neg  ax
       mov  [ di + m00 - m00 + 4 ],ax  ; -sin
       popa
       ret
Init_Matrix_Y endp

;======================================;
;      Init_Matrix_Z                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           bx -> Angle                ;
;======================================;
Init_Matrix_Z proc
       pusha
       Call Init_Matrix
       lea  si,SinTbl
       shl  bx,1                       ; Multiplica por 2. Trabaja con DW
       add  si,bx
       mov  ax,[ si ]                  ; AX = Sin( BX )
       mov  cx,[ si + 180 ]            ; CX = Cos( BX )
       mov  [ di + m00 - m00 + 0 ],cx  ;  cos
       mov  [ di + m01 - m00 + 2 ],cx  ;  cos
       mov  [ di + m00 - m00 + 2 ],ax  ;  sin
       neg  ax
       mov  [ di + m01 - m00 + 0 ],ax  ; -sin
       popa
       ret
Init_Matrix_Z endp


;======================================;
;      Put_Dot                         ;
;======================================;
;      In:  ax -> X                    ;
;           bx -> Y                    ;
;           cl -> Color                ;
;======================================;
Put_Dot     proc
       cmp  ax,320
       jae  Put_Dot_
       Cmp  bx,200
       jae  Put_Dot_
       pusha
       mov  dx,bx
       shl  bx,8                       ; = bx * 320 = bx * 256 + bx * 64 =
       shl  dx,6                       ; = bx * 2^8 + bx * 2^6
       add  bx,dx
       add  bx,ax                      ; BX -> Offset del Video
       mov  es:[ bx ],cl               ; Cl
       popa
Put_Dot_:
       ret
Put_Dot     endp

;======================================;
;      WaitEsc                         ;
;======================================;
waitesc proc
       push ax
       in   al,060h
       cmp  al,01h
       pop  ax
       ret
waitesc endp

;======================================;
;      VerticalR                       ;
;======================================;
verticalr proc
       mov  dx,VERT_RESCAN
ula1:
       in   al,dx            ;vertical rescan
       test al,8
       jne  ula1
ula2:
       in   al,dx
       test al,8
       je   ula2
       ret
verticalr endp

END
