;======================================;
;                                      ;
;              Rotaciones              ;
;                by Riq                ;
;                                      ;                                              ;
;======================================;
;                                      ;
;     Fecha de inicio: 12 / 4 / 95     ;
;              Version 1.1             ;
;                                      ;
;          Floatings de 8.14           ;
;                                      ;
;======================================;
;                                      ;
;          Copyright (c) 1995.         ;
;                                      ;
;======================================;

DOSSEG
MODEL SMALL
P386

;======================================; INCLUDES
INCLUDE     sin.inc
;======================================; DEFINES
VERT_RESCAN = 3DAh                     ; Vertical Rescan
ADD_X       = 160
ADD_Y       = 100
P00         = 000
P01         = 004
P02         = 008
P03         = 012

;======================================; MACROS

;======================================; STACK
STACK 100H

;======================================; DATA
DATASEG

Dots   label word                      ; X,Y,Z,1
       dd   -30,-30, 20,4000h
       dd    30, 30,-30,4000h
       dd    40, 40, 30,4000h
       dd    40, 40, 30,4000h

TOTALDOTS = ($-DOTS)/16

       dw    30,-30, 30,100h
       dw    30, 30, 30,100h
       dw   -30,-30, 30,100h
       dw   -30, 30, 30,100h
       ;;;
       dw    25,-25,-25,100h
       dw    25, 25,-25,100h
       dw   -25,-25,-25,100h
       dw   -25, 25,-25,100h
       dw    25,-25, 25,100h
       dw    25, 25, 25,100h
       dw   -25,-25, 25,100h
       dw   -25, 25, 25,100h
       ;;;
       dw    20,-20,-20,100h
       dw    20, 20,-20,100h
       dw   -20,-20,-20,100h
       dw   -20, 20,-20,100h
       dw    20,-20, 20,100h
       dw    20, 20, 20,100h
       dw   -20,-20, 20,100h
       dw   -20, 20, 20,100h
       ;;;
       dw    15,-15,-15,100h
       dw    15, 15,-15,100h
       dw   -15,-15,-15,100h
       dw   -15, 15,-15,100h
       dw    15,-15, 15,100h
       dw    15, 15, 15,100h
       dw   -15,-15, 15,100h
       dw   -15, 15, 15,100h
       ;;;
       dw    10,-10,-10,100h
       dw    10, 10,-10,100h
       dw   -10,-10,-10,100h
       dw   -10, 10,-10,100h
       dw    10,-10, 10,100h
       dw    10, 10, 10,100h
       dw   -10,-10, 10,100h
       dw   -10, 10, 10,100h
       ;;;


Vertex_Rot  label word
       dd   TOTALDOTS dup(0,0,0,0)

Matrix_Rot  label word                 ; Datos usados por Mul_Matrix
M00    dd   0,0,0,0
M01    dd   0,0,0,0
M02    dd   0,0,0,0
M03    dd   0,0,0,0

_X     dd   0                          ; Datos Temporales de Mul_Matrix
_Y     dd   0
_Z     dd   0

angle  dw 00h                          ; 0 - 65535
addangle dw 1


;======================================; CODE
CODESEG
       STARTUPCODE

;======================================;
;      Mainprogram                     ;
;======================================;
Mainprogram proc
       cld
       mov  ax,@DATA
       mov  ds,ax
       mov  ax,0a000h
       mov  es,ax
       mov  ax,13h
       int  10h                        ; Sets standard 320x200x256

       Call Main_Loop

       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h
       ret
Mainprogram endp

;======================================;
;      Main_Loop                       ;
;======================================;
Main_Loop   proc

M_Loop1:
       Call New_Angle
       Call Clean_Dots
       Call Rot_Dots
       Call Draw_Dots
       Call VerticalR
       Call WaitEsc
       jne  M_Loop1
       ret
Main_Loop   endp

;======================================;
;      New_Angle                       ;
;======================================;
New_Angle   proc
       mov  ax,angle
       lea  di,Matrix_Rot
       Call Init_Matrix_Z
       mov  bx,200h
       add  angle,bx
       cmp  angle,0
       jne  New_Angle_

       cmp  addangle,6
       je   Reset_Angle
       inc  addangle
       jmp  New_Angle_
Reset_Angle:
       mov  addangle,1
New_Angle_:
       ret


New_Angle   endp


;======================================;
;      Draw_Dots                       ;
;======================================;
Draw_Dots   proc
       xor  cx,cx                      ; Dots Counter
DD_loop:
       push cx
       lea  si,Vertex_Rot
       shl  cx,3                       ; Multiplica * 8 ( 4 * 2 )
       add  si,cx
       mov  eax,[ si + p00 ]           ; X
       mov  ebx,[ si + p01 ]           ; Y
       mov  cx,15                      ; Color
       add  eax,ADD_X
       add  ebx,ADD_Y
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  DD_loop

       ret
Draw_Dots   endp

;======================================;
;      Clean_Dots                      ;
;======================================;
Clean_Dots   proc
       xor  cx,cx                      ; Dots Counter
CD_loop:
       push cx
       lea  si,Vertex_Rot
       shl  cx,3                       ; Multiplica * 8
       add  si,cx
       mov  eax,[ si + p00 ]           ; X
       mov  ebx,[ si + p01 ]           ; Y
       add  eax,ADD_X
       add  ebx,ADD_Y
       xor  cx,cx                      ; Color
       Call Put_Dot
       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  CD_loop

       ret
Clean_Dots   endp

;======================================;
;      Rot_Dots                        ;
;======================================;
Rot_Dots   proc
       pusha
       xor  cx,cx                      ; Dots Counter
RD_loop:
       push cx
       lea  di,Dots
       lea  bx,Vertex_Rot
       lea  si,Matrix_Rot
       shl  cx,4                       ; Multiplica * 16
       add  di,cx                      ; ( Cada Vetex {bx}, Dot {di} tiene 8 )
       add  bx,cx

       mov  eax,[ di + p00 ]           ; X
       mov  [ bx + p00 ],eax
       mov  eax,[ di + p01 ]           ; Y
       mov  [ bx + p01 ],eax
       mov  eax,[ di + p02 ]           ; Z
       mov  [ bx + p02 ],eax
       mov  eax,[ di + p03 ]           ; 1
       mov  [ bx + p03 ],eax

       call Mul_Matrix                 ; [BX] = New Pos

;       mov  eax,[ bx + p00 ]
;       mov  [ di + p00 ],eax           ; X
;       mov  eax,[ bx + p01 ]
;       mov  [ di + p01 ],eax           ; Y
;       mov  eax,[ bx + p02 ]
;       mov  [ di + p02 ],eax           ; Z

       pop  cx
       inc  cx
       cmp  cx,TOTALDOTS
       jne  RD_loop

       popa
       ret
Rot_Dots   endp


;======================================;
;      Mul_Matrix                      ;
;======================================;
;      In:  bx -> offset Vertex (1x4)  ;
;           si -> offset Matrix (4x4)  ;
;      Out: bx -> offset new Vertex    ;
;======================================;
;
;  [ x y z 1 ]  *  [ A B C D ]
;                  [ E F G H ]
;                  [ I J K L ]
;                  [ M N O P ]
;
Mul_Matrix  proc
       mov  eax,[ bx + p00 ]           ; AX = X
       mov  ecx,[si + m00 - m00 + P00] ; A...
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ....
       mov  _X,eax                     ; ....

       mov  eax,[ bx + p01 ]           ; AX = Y
       mov  ecx,[si + m01 - m00 + P00] ; ....
       imul ecx                        ; E...
       shrd eax,edx,14                 ; ....
       add  _X,eax                     ; ....

       mov  eax,[ bx + p02 ]           ; AX = Z
       mov  ecx,[si + m02 - m00 + P00] ; ....
       imul ecx                        ; ....
       shrd eax,edx,14                 ; I...
       add  _X,eax                      ; ....

       mov  eax,[ bx + p03 ]           ; AX = 1
       mov  ecx,[si + m03 - m00 + P00] ; ....
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ....
       add  _X,eax                      ; M...

       ;;;

       mov  eax,[ bx + p00 ]           ; AX = X
       mov  ecx,[si + m00 - m00 + P01] ; .B..
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ....
       mov  _Y,eax                      ; ....

       mov  eax,[ bx + p01 ]           ; AX = Y
       mov  ecx,[si + m01 - m00 + P01] ; ....
       imul ecx                        ; .F..
       shrd eax,edx,14                 ; ....
       add  _Y,eax                      ; ....

       mov  eax,[ bx + p02 ]           ; AX = Z
       mov  ecx,[si + m02 - m00 + P01] ; ....
       imul ecx                        ; ....
       shrd eax,edx,14                 ; .J..
       add  _Y,eax                      ; ....

       mov  eax,[ bx + p03 ]           ; AX = 1
       mov  ecx,[si + m03 - m00 + P01] ; ....
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ....
       add  _Y,eax                      ; .N..

       ;;;

       mov  eax,[ bx + p00 ]           ; AX = X
       mov  ecx,[si + m00 - m00 + P02] ; ..C.
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ....
       mov  _Z,eax                      ; ....

       mov  eax,[ bx + p01 ]           ; AX = Y
       mov  ecx,[si + m01 - m00 + P02] ; ....
       imul ecx                        ; ..G.
       shrd eax,edx,14                 ; ....
       add  _Z,eax                      ; ....

       mov  eax,[ bx + p02 ]           ; AX = Z
       mov  ecx,[si + m02 - m00 + P02] ; ....
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ..K.
       add  _Z,eax                      ; ....

       mov  eax,[ bx + p03 ]           ; AX = 1
       mov  ecx,[si + m03 - m00 + P02] ; ....
       imul ecx                        ; ....
       shrd eax,edx,14                 ; ....
       add  _Z,eax                     ; ..O.

       mov  eax,_X
       mov  [ bx + p00 ],eax           ;
       mov  eax,_Y
       mov  [ bx + p01 ],eax           ;
       mov  eax,_Z
       mov  [ bx + p02 ],eax           ;

       ret
Mul_Matrix  endp

;======================================;
;      Init_Matrix                     ;
;======================================;
;      In:  di -> offset Matrix        ;
;======================================;
Init_Matrix proc
       mov  ebx,4000h
       xor  ecx,ecx
       mov  [di + m00 - m00 + p00],ebx ; 1
       mov  [di + m00 - m00 + p01],ecx
       mov  [di + m00 - m00 + p02],ecx
       mov  [di + m00 - m00 + p03],ecx
       ;;;
       mov  [di + m01 - m00 + p00],ecx
       mov  [di + m01 - m00 + p01],ebx ; 1
       mov  [di + m01 - m00 + p02],ecx
       mov  [di + m01 - m00 + p03],ecx
       ;;;
       mov  [di + m02 - m00 + p00],ecx
       mov  [di + m02 - m00 + p01],ecx
       mov  [di + m02 - m00 + p02],ebx ; 1
       mov  [di + m02 - m00 + p03],ecx
       ;;;
       mov  [di + m03 - m00 + p00],ecx
       mov  [di + m03 - m00 + p01],ecx
       mov  [di + m03 - m00 + p02],ecx
       mov  [di + m03 - m00 + p03],ebx ; 1
       ;;;
       ret
Init_Matrix endp

;======================================;
;      Init_Matrix_Z                   ;
;======================================;
;      In:  di -> offset Matrix        ;
;           Ax -> Angle  ( 0-65535 )   ;
;======================================;
Init_Matrix_Z proc
       pusha
       Call Init_Matrix

       push ax

       Call Sin
       mov  [di + m00 - m00 + p01],eax ;  sin
       neg  eax
       mov  [di + m01 - m00 + p00],eax ; -sin

       pop  ax

       Call Cosin
       mov  [di + m00 - m00 + p00],eax ;  cos
       mov  [di + m01 - m00 + p01],eax ;  cos

       popa
       ret
Init_Matrix_Z endp


;======================================;
;      Put_Dot                         ;
;======================================;
;      In:  eax -> X                   ;
;           ebx -> Y                   ;
;           cl -> Color                ;
;======================================;
Put_Dot     proc
       pusha
       mov  dx,bx
       shl  bx,8                      ; = bx * 320 = bx * 256 + bx * 64 =
       shl  dx,6                      ; = bx * 2^8 + bx * 2^6
       add  bx,dx
       add  bx,ax                    ; BX -> Offset del Video

       mov  es:[ bx ],cl              ; Cl
       popa
       ret
Put_Dot     endp

;======================================;
;      WaitEsc                         ;
;======================================;
waitesc proc
       push ax
       in   al,060h
       cmp  al,01h
       pop  ax
       ret
waitesc endp

;======================================;
;      VerticalR                       ;
;======================================;
verticalr proc
       mov  dx,VERT_RESCAN
ula1:
       in   al,dx            ;vertical rescan
       test al,8
       jne  ula1
ula2:
       in   al,dx
       test al,8
       je   ula2
       ret
verticalr endp


;같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
;
;   Sign - 16 bit theta to 32bit sin(@)
; In:
;     AX - theta  0 - 65536 (0-360)
; Out:
;    EAX - sin (@)   (-4000h to 4000h)
;
;같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
;
; CoSign - 16 bit theta to 32bit cos(@)
; In:
;     AX - theta  0 - 65536 (0-360)
; Out:
;    EAX - cos (@)   (-4000h to 4000h)
;
;같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같
;
; Notes:
; calculate sin into eax, from ax, smashes bx
; after imul by sin, shr eax,14 to compensate for decimal factor!
;  eg:
;    mov eax,sin(@)
;    mov ebx,32bitnumber
;    imul ebx
;    shrd eax,edx,14
;    eax = ebx*sin(@)
;
;    mov ax,sin(@)
;    mov bx,16bitnumber
;    imul bx
;    shrd ax,dx,14
;    eax = bx*sin(@)
;
; eax is simply a sign extended ax
;
;같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같

;           align 16
cosin:
           add ax,4000h
sin:
           shr ax,2
           cmp ax,2000h
           jge q3o4         ; quadrant 3 or 4

           cmp ax,1000h
           jl  q0            ; quad 1

           mov ebx,1fffh
           sub bx,ax
           jmp halfsign     ; quad 2
q0:
           movzx ebx,ax
           jmp halfsign
q3o4:
           cmp ax,3000h
           jl  q3
           mov ebx,3fffh
           sub bx,ax
           call halfsign      ; quad 4
           neg eax
           ret
q3:
           and ax,0fffh
           movzx ebx,ax       ; quad 3
           call halfsign
           neg eax
           ret
halfsign:
           xor eax,eax
           mov ax,word ptr sinus[ebx*2]
           ret

END
