	TITLE	p.c
	.386P
include listing.inc
if @Version gt 510
.model FLAT
else
_TEXT	SEGMENT PARA USE32 PUBLIC 'CODE'
_TEXT	ENDS
_DATA	SEGMENT DWORD USE32 PUBLIC 'DATA'
_DATA	ENDS
CONST	SEGMENT DWORD USE32 PUBLIC 'CONST'
CONST	ENDS
_BSS	SEGMENT DWORD USE32 PUBLIC 'BSS'
_BSS	ENDS
_TLS	SEGMENT DWORD USE32 PUBLIC 'TLS'
_TLS	ENDS
FLAT	GROUP _DATA, CONST, _BSS
	ASSUME	CS: FLAT, DS: FLAT, SS: FLAT
endif
PUBLIC	_main
EXTRN	_printf:NEAR
_DATA	SEGMENT
$SG647	DB	'aca no deberia llegar', 0aH, 00H
_DATA	ENDS
_TEXT	SEGMENT
_main	PROC NEAR
; File p.c
; Line 5
	push	ebp
	mov	ebp, esp
	push	ebx
	push	esi
	push	edi
; Line 6
	lea	edx, DWORD PTR [esp]
; Line 7
	mov	eax, 187				; 000000bbH
; Line 8
	int	46					; 0000002eH
; Line 9
	mov	eax, 15					; 0000000fH
; Line 10
	int	46					; 0000002eH
; Line 12
	push	OFFSET FLAT:$SG647
	call	_printf
	add	esp, 4
; Line 14
	mov	eax, 17					; 00000011H
; Line 15
	pop	edi
	pop	esi
	pop	ebx
	pop	ebp
	ret	0
_main	ENDP
_TEXT	ENDS
END
