;name: SChar
;verion: 1.00
;start  date: 16/10/94
;finish date: 16/10/94
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************ equ *************************;

key_mask equ 00000011b  ;Left & Right Shift
verhi    equ '01'
verlo    equ '00'

;************* START ********************;
start:
JMP INIT


;******* DOSACTIVE *******;
;Output: Zero flag=1 Dos may be interrupted.

dosactive proc near
        push ds
        push bx
        lds bx,indos            ;ds:bx point to the INDOS flag
        cmp byte ptr [bx],0     ;DOS function active?
        pop bx
        pop ds
        ret
dosactive endp


;******** INTERRUPS ****************************************;


;**************************;
;******** int 08 **********;
int08:
        jmp short cont08
        db 'SC'
cont08:
        cmp cs:tsrnow,0         ;si es 0 TIME OUT.
        je i8_end               ;volver a intentar

        dec cs:tsrnow           ;decrementa para poder activar.

        cmp cs:in_bios,0        ;se esta usando la int 13
        jne i8_end              ;si =0 no, si <>0 si

        call dosactive
        je i8_tsr
i8_end:
        jmp cs:[old08]
i8_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old08]
        call start_tsr
        iret

;**************************;
;********* int 09 *********;
int09:
        jmp short cont09
        db 'SC'
cont09:
        push ax

        cmp cs:tsractive,0      ;ya esta activo
        jne i9_end              ;si then terminar

        cmp cs:tsrnow,0         ;waiting for activation
        jne i9_end              ;yes so terminar

        push ds                 ;lee si se pulsa el HOTKEY
        mov ax,040h
        mov ds,ax
        mov al,byte ptr ds:[17h]
        and al,key_mask
        cmp al,key_mask
        pop ds
        jne i9_end              ;No se pulsa entonces nada.

        cmp cs:in_bios,0        ;Se pulsa pero int 13 usandose
        jne i9_e1               ;Then poner tiempo de espera

        call dosactive          ;No se esta usando
        je i9_tsr               ;Pero el INDOS lo permite.Si then TSR
i9_e1:
        mov cs:tsrnow,9         ;9/18.2 = 0.5 segundos de espera.
i9_end:
        pop ax
        jmp cs:[old09]
i9_tsr:
        mov cs:tsractive,1
        mov cs:tsrnow,0
        pushf
        call cs:[old09]
        pop ax
        call start_tsr
        iret


;**************************;
;********* int 2f *********;
int2f:
        jmp short cont2f
        db 'SC'
cont2f:
        cmp ax,0d500h
        jne no_d500
        cmp bx,'SC'
        jne no_d500
        xchg al,ah
        mov bx,VERHI
        mov cx,VERLO
        iret

no_d500:
        jmp cs:[old2f]


;***************************;
;********** int 13 *********;
int13:
        jmp short cont13
        db 'SC'
cont13:
        inc cs:in_bios

        pushf
        call cs:[old13]
        dec cs:in_bios
        sti
        ret 2

;*******************************;
;********* int 28 **************;

int28:
        jmp short cont28
        db 'SC'
cont28:
        cmp cs:tsrnow,0         ;tsrnow es el tiempo que tiene
        je i28_end              ;para empezar a ejecutarce.
                                ;Si es 0 se ejecuta.

        cmp cs:in_bios,0        ;in_bios es el flag de la int 13
        je i28_tsr              ;si es 0 se puede ejecutar.

i28_end:
        jmp cs:[old28]

i28_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old28]
        call start_tsr
        iret


;*************************************************;
;********** START PROGRAMA RESIDENTE *************;
;*************************************************;
start_tsr proc near

        cli
        mov cs:xx_ss,ss
        mov cs:xx_sp,sp

        mov sp,offset stacks_end
        mov ax,cs
        mov ss,ax
        sti

        push ax
        push bx
        push cx
        push dx
        push ds
        push es
        push si
        push di


        pop di
        pop si
        pop es
        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        cli

        mov ss,cs:xx_ss
        mov sp,cs:xx_sp

        mov cs:tsractive,0
        sti
        ret
start_tsr endp


;************ data **********************;

xx_ss   dw 0
xx_sp   dw 0

tsrnow  db 0
tsractive db 0
in_bios db 0

dta_off dw 0
dta_seg dw 0

indos equ this dword
indos_off dw 0
indos_seg dw 0

old08 equ this dword
off08 dw 0
seg08 dw 0

old09 equ this dword
off09 dw 0
seg09 dw 0

old13 equ this dword
off13 dw 0
seg13 dw 0

old28 equ this dword
off28 dw 0
seg28 dw 0

old2f equ this dword
off2f dw 0
seg2f dw 0

stacks dw 256 dup(0)
stacks_end equ this byte


INIT:
;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        je jmpinst              ;instala
        cmp al,20h              ;es espacio, si then seguir buscando
        je alop
        cmp al,'/'
        je command_line         ;loop until '/' or CR is found
        cmp al,'-'
        je command_line
        jne displayhelp

command_line:
        lodsb
        cmp al,'?'
        je displayhelp
        and al,11011111b
        cmp al,'U'
        je uninstall
        cmp al,'H'
        je displayhelp

;       [...] para agregar m�s opciones

jmpinst:
        jmp instalar

displayhelp:                    ;***************** no command line
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***
        mov ax,4c00h
        int 21h

uninstall:                      ;*********************command line U
        mov ax,0d500h
        mov bx,'SC'
        int 2fh
        cmp ax,00d5h
        jne error_inst2

        cmp bx,verhi
        jne error_inst2
        cmp cx,verlo
        jne error_inst2         ;no es.no desinstala

        mov ax,3508h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,3509h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,3513h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,3528h
        int 21h
        cmp es:[bx+2],'CS'
        jne error_inst

        mov ax,352fh
        int 21h
        cmp es:[bx+2],'CS'
        je desinstalar

error_inst:                         ;mensaje de error porque no puede desinstalar
        push cs
        pop ds
        mov dx,offset error_isnt_msg2
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h

error_inst2:                        ;mensaje de error porque no esta residente
        push cs
        pop ds
        mov dx,offset error_inst_msg1
        mov ah,9
        int 21h
        mov ax,4cffh
        int 21h

desinstalar:                            ;*** Rutina de desinstalacion ***
        cli
        mov dx,es:off08
        mov ds,es:seg08
        mov ax,2508h
        int 21h                         ;*** restaurar vector original

        mov ax,2509h
        mov dx,es:off09
        mov ds,es:seg09
        int 21h

        mov ax,2513h
        mov dx,es:off13
        mov ds,es:seg13
        int 21h

        mov ax,2528h
        mov dx,es:off28
        mov ds,es:seg28
        int 21h

        mov ax,252fh
        mov dx,es:off2f
        mov ds,es:seg2f
        int 21h
        sti

        mov bx,es
        mov es,es:[2ch]
        mov ah,49h
        int 21h
        mov es,bx
        mov ah,49h
        int 21h                         ;Release memory

        push cs
        pop ds
        mov dx,offset uninstall_msg
        mov ah,9
        int 21h

        mov ax,4c00h
        int 21h


;*********************************;
;*****     Instalar        *******;

instalar:
        mov ax,0d500h           ;Is Savepic installed?
        mov bx,'SC'
        int 2fh
        cmp ax,00d5h
        jne instalar_           ;Si esta instaldo then change name only.

        mov dx,offset alreadyinst_msg
        mov ah,9
        int 21h                 ;Mensaje de error si no es la v2.10
        mov ax,4cffh
        int 21h                 ;Return to DOS


;*******************************;
;                               ;
;       Instalar                ;
;                               ;
;*******************************;
instalar_:
        mov ah,34h              ;get address of INDOS flag
        int 21h
        mov si,offset indos
        mov [si],bx
        mov [si+2],es

        mov ax,3508h            ;get address of int 08
        int 21h
        mov si,offset old08
        mov [si],bx
        mov [si+2],es

        mov dx,offset int08
        mov ax,2508h
        int 21h

        mov ax,3509h            ;get address of int 09
        int 21h
        mov si,offset old09
        mov [si],bx
        mov [si+2],es

        mov dx,offset int09
        mov ax,2509h
        int 21h

        mov ax,3513h            ;get address of int 13
        int 21h
        mov si,offset old13
        mov [si],bx
        mov [si+2],es

        mov dx,offset int13
        mov ax,2513h
        int 21h

        mov ax,3528h            ;get address of int 28
        int 21h
        mov si,offset old28
        mov [si],bx
        mov [si+2],es

        mov dx,offset int28
        mov ax,2528h
        int 21h

        mov ax,352fh            ;get address of int 2f
        int 21h
        mov si,offset old2f
        mov [si],bx
        mov [si+2],es

        mov dx,offset int2f
        mov ax,252fh
        int 21h

        mov ah,9                ;print message of installation
        mov dx,offset installing_msg
        int 21h

        mov dx,offset init      ;TSR
        mov ah,31h
        mov cl,4
        shr dx,cl
        inc dx
        int 21h

;****** MESSAGES *****
error_inst_msg1 db 'Error.',13,10
db 'SChar 1.00 no esta instalda. Por lo tanto no se puede desinstalar.',13,10,'$'

error_isnt_msg2 db 'Error.',13,10
db 'Trate de desinstalar otros TSR para poder desinstalar SChar.',13,10,'$'

alreadyinst_msg db'SChar 1.00 ya esta instalado. No se va a volver a instalar.',13,10,'$'

installing_msg db'SChar 1.00 instalado.',13,10,'$'

uninstall_msg db'SChar 1.00 desinstalado.',13,10,'$'

help_msg equ this byte
db'- SChar v1.00 (c) Ricardo Quesada 1994 -',13,10
db'Versi�n ShareWare. Please register.',13,10,13,10
db'SChar [opciones] [nombre]',13,10
db'     /a      - La conocida opci�n de remover los virus.',13,10
db"               ( Built-in Ricardo Quesada's antivirus (c) 1993 ).",13,10
db'     /u      - Desinstala a SChar.',13,10
db'     nombre  - Nombre con que se grabar�n los fonts.',13,10,13,10
db'Para m�s informaci�n lea SChar.doc .',13,10,'$'

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

