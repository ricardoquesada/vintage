;programa configurador del ADC
;confadc.exe
;prueba ... nadie sabe el  desastre que puede quedar.


code    segment public byte 'code'
        assume cs:code

start   label                           ;inicio del prog

INTRO:  CLD
        MOV SI,81H
SEGU:   LODSB
        cmp al,20h
        je segu
        CMP AL,13       ;AL=CR?
        je segu1

        mov ah,9
        mov dx,offset credits_msg
        int 21h
        mov ah,4ch
        int 21h

SEGU1:
        mov ax,1a00h                    ;detecta si ansi.sys esta
        int 2fh                         ;residente
        cmp ax,1a00h
        je no_ansi

        mov ah,9
        mov ds,data
        mov dx,offset inic_ansi_msg
        int 21h
        jmp short open_file

no_ansi:
        mov ah,9
        mov dx,offset inic_msg
        int 21h

open_file:
        mov ax,3d02h
        mov dx,offset nombre
        int 21h                         ;Abrir un archivo
        jnc aca_aca
        jmp error_detected

aca_aca:
        mov handle,ax                   ;pushea handle

get_char_again2:
        mov ax,1a00h                    ;detecta si ansi.sys esta
        int 2fh                         ;residente
        cmp ax,1a00h
        je no_ansi2

        mov ah,9                        ;ansi detectado
        mov dx,offset musica2_msg
        int 21h
        jmp short get_char_again

no_ansi2:
        mov ah,9
        mov dx,offset musica_msg
        int 21h

get_char_again:
        mov ah,1
        int 21h                         ;get char from device

        and al,11011111b                ;clear bit 6
        cmp al,4eh                      ;'n' o 'N'
        je char_pulsado
        cmp al,53h
        je char_pulsado
        jmp noeschar

char_pulsado:                           ;El char ya esta en mayusculas
        mov char,al                     ;aca esta el char

tutto3:
        mov ax,1a00h                    ;detecta si ansi.sys esta
        int 2fh                         ;residente
        cmp ax,1a00h
        je no_ansi3

        mov ah,9
        mov dx,offset dias3_msg
        int 21h
        jmp short tutto

no_ansi3:
        mov ah,9
        mov dx,offset dias_msg
        int 21h

tutto:
        mov error,0

        mov below,'1'
        call get_char
        cmp al,13
        jne ok_ok

        mov ah,9
        mov dx,offset dias2_msg
        int 21h

        jmp short tutto3


ok_ok:
        cmp error,0
        je todo_ok_
        jmp tutto2

todo_ok_:
        mov dias,al

        mov below,'0'
        call get_char
        cmp al,13
        je ok_ok2                       ;solo el primer char tiene valor

        cmp dias,'3'                    ;toma los 2 char
        jb ok_ok3
        mov ah,9
        mov dx,offset dias2_msg
        int 21h
        jmp tutto3


ok_ok2:
        cmp error,0
        je todo_ok2_
        jmp tutto2

todo_ok2_:
        mov al,dias
        mov dias,20h
        mov dias+1,al
        jmp short save

ok_ok3:
        cmp error,0
        je todo_ok3_
        jmp tutto2

todo_ok3_:
        mov dias+1,al


save:
        mov ax,4200h
        xor cx,cx
        mov dx,99
        mov bx,handle
        int 21h                         ;move file pointer
        jc error_code

        mov ah,40h
        mov bx,handle
        mov cx,3                        ;3 cararc a grabar
        mov dx,offset dias
        int 21h                         ;write using handles
        jc error_code

        mov ah,3eh
        mov bx,handle
        int 21h

        mov ah,9
        mov dx,offset no_error_msg
        int 21h

        mov ax,4c00h
        int 21h                         ;terminar todo OK.


noeschar:
        mov ah,9
        mov dx,offset presione_msg
        int 21h
        jmp get_char_again2

error_detected:
        mov dx,offset error_msg
        mov ah,9
        int 21h

        mov ax,4cffh
        int 21h                         ;terminate with error code ffH

error_code:                             ;otro tipo de error

        mov ah,3eh
        mov bx,handle
        int 21h                         ;close handle

        mov dx,offset error2_msg
        mov ah,9
        int 21h

        mov ax,4cffh
        int 21h                         ;terminar con error


tutto2:
        mov dx,offset dias2_msg         ;mensaje de 1 al 29 error.
        mov ah,9
        int 21h
        jmp tutto3



;******** Subrutinas ************

get_char  proc

get_dias_again:
        mov ah,1
        int 21h
        cmp al,13
        je enter_

        cmp al,below
        jb dias_dias
        cmp al,'9'
        ja dias_dias
enter_:
        ret

dias_dias:
        mov error,1                     ;Error detectado
        ret

get_char  endp



;*********** data *******************

nombre  db 'adccdp.dbf',0              ;asciiz string
handle  dw 0
dias    db '00'
char    db 'S'
below   db 0
error   db 0
;********* msg **************

error_msg  db 'Error.',13,10
           db 'No se encontro el archivo ADCCDP.DBF .Por favor verifique.',13,10,'$'
error2_msg db 'Error en transferencia de datos. Vuelva a intentar.',13,10,'$'

inic_ansi_msg   db 13,10
db '[40m[2J[2H[30C[0;36m���   ����[5C����[3H[29C[1;37;46m��[1C��[40m[s'
db '[u[46m[2C[0;36m�[1;37;46m�[0;36m�[1;37;46m��[40m�  [46m��[2C[40m[s'
db '[u[46m[0;36m��[4H[29C[1;37m�����   � ���  ��[5H[29C[0;36;44m��[40m[s'
db '[u[44m[1C��[2C[40m�����   ������[6H[19C[1;37m������������������������[s'
db '[u����������������ͻ[7H[19C�   CONFIGURADOR   PARA  EL  [32mA. D. C.    [s'
db '[u[37m�[8H[19C�[40C�[9H[19C�[16C[5;31m [D  [D  [D  [D  [D[s'
db '[u[15C[0;1m�[10H[19C�[16C[33m� � � � �[15C[37m�[11H[19C�[15C[s  '
db '[u[0;35m�����������[14C[1;37m�[12H[19C�[15C/\/\/\/\/\/[14C�[13H[19C[s'
db '[u�[15C[0;35m�����������[14C[1;37m�[14H[19C�[14C[0;36m�������������[s'
db '[u[13C[1;37m�[15H[19C�[17CConfAdc[16C�[16H[19C�         <<<Battle So[s'
db '[uft S.A.>>>         �[17H[19C�[19C[36m1993[17C[37m�[18H[19C��������[s'
db '[u��������������������������������ͼ[19H[20H[21H[22H[23H[0m','$'


inic_msg   db 13,10
DB      '                                                   '
DB      '                                                   '
DB      '        ���   ����     ����                        '
DB      '                                    �� ��  ������  '
DB      '��  ��                                             '
DB      '              �����   � ���  ��                    '
DB      '                                           �� ��  �'
DB      '����   ������                                      '
DB      '           ����������������������������������������'
DB      'ͻ                                      �   CONFIGU'
DB      'RADOR   PARA  EL  A. D. C.    �                    '
DB      '                  �                                '
DB      '        �                                      �   '
DB      '                                �             '
DB      '                         �                � � � � �'
DB      '               �                                   '
DB      '   �               �����������              �      '
DB      '                                �               /\/'
DB      '\/\/\/\/              �                            '
DB      '          �               �����������              '
DB      '�                                      �           '
DB      '   �������������             �                     '
DB      '                 �                 ConfAdc         '
DB      '       �                                      �    '
DB      '      <<<Battle Soft S.A.>>>        �              '
DB      '                        �                   1993   '
DB      '              �                                    '
DB      '  ����������������������������������������ͼ',13,10,13,10,'$'

presione_msg    db 13,10,'Debe pulsar "S" o "N"',13,10,'$'
musica_msg      db '�Lo desea con musica (s/n)? $'
musica2_msg     db '[20;0H�Lo desea con musica (s/n)? $'
dias2_msg       db 13,10,'Entre 1 y 29 por favor.',13,10,'$'
dias_msg        db 13,10,'�Con cuantos dias de anticipacion (1 a 29)? $'
dias3_msg       db 13,10,'[22;45H  [22;0H�Con cuantos dias de anticipacion (1 a 29)? $'

no_error_msg    db 13,10,'Gracias por usar ADC.....',13,10,'$'

credits_msg     db 'ConfAdc v1.00-',13,10,13,10
                db 'Programa (come-disco) original por Pablo Rossi.',13,10
                db 'Pantalla presentacion �por Juan Janczuk?.',13,10
                db 'Codigo (no lo entiende ni el autor) en assembler por Ricardo Quesada.',13,10,13,10
                db 'Tipee CONFADC para configurar.',13,10
                db 'Hecho en 1993. Buenos Aires, Argentina.',13,10,'$'

code    ends

data segment public byte 'data'
        assume ds:data





        end start
