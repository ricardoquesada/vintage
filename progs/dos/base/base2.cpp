/*
Filename    : BASE.EXE
Author      : Ricardo Quesada
Version     : 1.10
Description : A cardfile maker
*/



// tabla de macros
#define SIZE sizeof(tito1)
#define SI_HEAD sizeof(cabecera)

// include files
#include <dos.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <dir.h>
#include "base.h"

// type declarations

typedef unsigned char BYTE; 			/* definiendo un byte */

typedef struct {
    BYTE nombre[30];
    BYTE apellido[30];
    BYTE telefono[10];
    BYTE direccion[40];
    BYTE codigop[10];
    BYTE fecha[11];
    BYTE varios[70];
    int num_orden;
} FICHA;

FICHA tito1;

struct {
   char name[10];
} cabecera,cabecera2;

struct text_info ti;
union REGS regs;

// definicion de global variables.
char buffe[4096];
char filename[14];
int handle;


// definicion de funciones

/************************** CENTRA ********************/
void centra(char *buf,int y)
{
    /* variables */
    int lenght;

    lenght = 40 - (strlen(buf) /2);
    gotoxy(lenght,y);
    cprintf("%s",buf);
}

/************************** MENSA **********************/
void mensa(char *buf,int color)
{
    /* variables */
    int lenght;

    gettextinfo(&ti);
    textcolor(color);
    window(1,25,80,25);
    clrscr();
    lenght = 40 - ( strlen(buf) /2);
    gotoxy(lenght,1);
    cprintf("%s",buf);
    window(ti.winleft,ti.wintop,ti.winright,ti.winbottom);
    textattr(ti.attribute);
    return;
}

/************************* GUINDOU ***************************/
void guindou(int x1,int y1,int x2,int y2,int mod)
{

    if (mod == 1) {     // mod=1 set window
        gettext(x1,y1,x2,y2,buffe);
        window(x1,y1,x2,y2);
        clrscr();
    }
    if (mod == 0) {     // mod=0 restore window

        puttext(x1,y1,x2,y2,buffe);
        window(1,1,80,25);
    }
}

/************************* VERDATOS **************************/
void verdatos(void)
{
    /* variables */
    int i;

    gotoxy(1,12);
    textcolor(RED);
    for (i=0;i<80;i++) cprintf("�");
    gotoxy (1,13);
    textcolor(WHITE);
    cprintf( "\r\n  S : Reg. siguiente.                  A : Reg. anterior."
	     "\r\n  M : Modifica este reg.               N : Agrega uno nuevo reg."
	     "\r\n  B : Busca dato en los regs.          I : Ir a registro."
	     "\r\nEsc : para salir.                      E : Eliminar este registro.");
    gotoxy(1,18);
    textcolor(RED);
    for (i=0;i<80;i++) cprintf("�");
    textcolor(LIGHTGRAY);
}

/********************** LINEA ***************/
void linea(void)
{
    /* variables */
    int i;

    gotoxy(1,1);
    textcolor(WHITE);
    cprintf("NOMBRE DE FICHA: %s",filename);
    gotoxy(40,1);
    cprintf("NUMERO DE REGISTRO: %d",tito1.num_orden);
    gotoxy(1,2);
    textcolor(RED);
    for (i=0;i<80;i++) cprintf("�");
    textcolor(LIGHTGRAY);
}

/************************** MOSTRAR **************************/
void mostrar(void)
{
    linea();
    gotoxy(1,3);
    printf("\nNombre: %s",tito1.nombre);
    printf("\nApellido: %s",tito1.apellido);
    printf("\nTelefono: %s",tito1.telefono);
    printf("\nDireccion: %s",tito1.direccion);
    printf("\nCodigo Postal: %s",tito1.codigop);
    printf("\nFecha: %s",tito1.fecha);
    printf("\nVarios: %s",tito1.varios);

}


/************************** OBTENER **************************/
void obtener(void)
{
    /* variables */
    char carac;
    char gral[73];
    char *ptr_gral;
    int i;

    do{
	for(i=0;i<(SIZE-2);i++)
	    tito1.nombre[i] = 0;
	clrscr();
	verdatos();
	linea();

	gotoxy(1,4);
	printf("Nombre: ");
	gral[0] = 30;
	ptr_gral = cgets(gral);
	strcpy(tito1.nombre,ptr_gral);

	printf("\nApellido: ");
	gral[0] = 30;
	ptr_gral = cgets(gral);
	strcpy(tito1.apellido,ptr_gral);
	printf("\nTelefono: ");
	gral[0] = 10;
	ptr_gral = cgets(gral);
	strcpy(tito1.telefono,ptr_gral);

	printf("\nDireccion: ");
	gral[0] = 40;
	ptr_gral = cgets(gral);
	strcpy(tito1.direccion,ptr_gral);

	printf("\nCodigo Postal: ");
	gral[0] = 10;
	ptr_gral = cgets(gral);
	strcpy(tito1.codigop,ptr_gral);

	printf("\nFecha: ");
	gral[0] = 11;
	ptr_gral = cgets(gral);
	strcpy(tito1.fecha,ptr_gral);

	printf("\nVarios: ");
	gral[0] = 70;
	ptr_gral = cgets(gral);
	strcpy(tito1.varios,ptr_gral);

	gotoxy(1,21);
	printf("Todos estos datos son correctos (s/n) ?:");
	carac = toupper(getche());
    } while (carac !='S');
    gotoxy(1,21);
    printf("                                             ");

}

/************************** CABEZA ***************************/
int cabeza(int hand)
{
    /* variables */
    int retorno ;

    if ( (retorno = write(hand,&cabecera,SI_HEAD)) == -1 )
	 perror("\nError");
    return retorno;
}

/************************** ABRIR ****************************/

int abrir(char *ptr_nombre)
{
    /*  variables */
    int hand;

    gotoxy(1,25);

    if ((hand = open ( ptr_nombre,O_RDWR | O_BINARY)) == -1) {
        mensa("Error",WHITE + BLINK);
        getch();
        return hand ;
    }
    if ( read(hand,&cabecera2,SI_HEAD) != SI_HEAD) {
        mensa("Error",WHITE + BLINK);

        getch();
        close(hand);
        return -1;                  /* se pueden hacer aca    */
    }

    if ( strcmp(cabecera.name,cabecera2.name) != 0) {

        mensa("Archivo no grabado por Base 1.00",WHITE + BLINK);

        getch();
        close(hand);
        return -1;
    }
    return hand;
}
/*************************** RECPOS **********************/
void recpos(void)
{
    /* variables */
    unsigned int cucu;

    (cucu) = lseek(handle,0,SEEK_CUR);
    cucu = (cucu - SI_HEAD) / SIZE;
    tito1.num_orden = cucu;

}

/**************************** GRABAR ************************/
int grabar(void)
{

    if ( (write (handle,&tito1,SIZE) ) != SIZE ) {
	perror("Error:");
    return -1;
    }
    return 0;
}

/*********************** LEER ***********************/
int leer(void)
{
    /* variable */
    int res;

    if ( (res = read (handle,&tito1,SIZE) ) != SIZE )
	res = -1;
    return res;
}
/******************** GOREC ********************/
int gorec(int i)
{
    /* variables */
    int retorno;

    retorno = lseek(handle,SI_HEAD + (SIZE * i),SEEK_SET);
    return retorno;
}

/******************* IR ***********************/
int ir(void)
{
    /* variables */
    char reco[6];
    char *ptr_rec;
    int rec;

    gotoxy(1,20);
    reco[0] = 4;
    printf("Ingrese numero de record:");
    ptr_rec = cgets(reco);
    rec = atoi(ptr_rec);

    gorec(rec);
    rec = leer();

    clrscr();
    mostrar();
    verdatos();
    return rec;
}

/******************* ELIMINAR ******************/
void eliminar(void)
{
    gotoxy(1,22);
    textcolor(WHITE);
    cprintf("�Esta seguro? [s/n]");
    textcolor(LIGHTGRAY);
    if (toupper(getche()) != 'S' ) {
	gotoxy(1,22);
	cprintf("                           ");
	return;
    }
    textcolor(WHITE + BLINK);
    gotoxy(1,25);
    cprintf("Funcion no habilitada por el momento.");
    getch();
    gotoxy(1,22);
    cprintf("                           ");
    gotoxy(1,25);
    cprintf("                                              ");
    textcolor(LIGHTGRAY);
    return;
}


/******************* BUSCO *********************/
int busco(char *ptr)
{
    /* variables */
    int i=0;
    int j=0;
    char *old_ptr;
    old_ptr = ptr;


    for(;i <= SIZE;i++) {

	if ( toupper(*ptr) == toupper(tito1.nombre[i]) ) {
	    for (;;j++) {
		ptr++;
		if (*ptr == NULL)
		    return 0;
		i++;
		if (i > SIZE)
		    return -1;
		if ( toupper(*ptr) != toupper(tito1.nombre[i]) )  {
		    i--;
		    ptr = old_ptr;
		    break;
		}
	    }
	}
    }
    return -1;
}

/******************* BUSCAR ********************/
void buscar(void)
{
    /* variables */
    char busc[43];
    char *ptr_busc;
    int record_num = 0;
    char cara;
    int i;

    Denuevo:					/* label */
    clrscr();
    mostrar();
    verdatos();

    gotoxy(1,20);
    printf("Introduzca el dato que quiere buscar.\n:");
    busc[0] = 40;
    ptr_busc = cgets(busc);

    for (;;record_num++) {
	gotoxy(1,22);
	printf("Buscando en record:%d el dato \"%s\".",record_num,ptr_busc);

	if ( gorec(record_num) == -1) {
	    gotoxy(1,23);
	    cprintf("[");
	    textcolor(WHITE);
	    cprintf("A");
	    textcolor(LIGHTGRAY);
	    cprintf("]bando busqueda, [");
	    textcolor(WHITE);
	    cprintf("O");
	    textcolor(LIGHTGRAY);
	    cprintf("]tro dato?(a/o)");
	    gotoxy(1,25);
	    textcolor(WHITE + BLINK);
	    cprintf("Dato no encontrado.");
	    textcolor(LIGHTGRAY);
	    cara = toupper(getche());
	    break;
	}
	if (leer() == -1) {
	    gotoxy(1,23);
	    cprintf("[");
	    textcolor(WHITE);
	    cprintf("A");
	    textcolor(LIGHTGRAY);
	    cprintf("]bando busqueda, [");
	    textcolor(WHITE);
	    cprintf("O");
	    textcolor(LIGHTGRAY);
	    cprintf("]tro dato?(a/o)");
	    gotoxy(1,25);
	    textcolor(WHITE + BLINK);
	    cprintf("Dato no encontrado.");
	    textcolor(LIGHTGRAY);
	    cara = toupper(getche());
	    break;
	}
	if ( (busco(ptr_busc)) == 0) {
	    clrscr();
	    mostrar();
	    verdatos();
	    gotoxy(1,21);
	    cprintf("Busco [");
	    textcolor(WHITE);
	    cprintf("S");
	    textcolor(LIGHTGRAY);
	    cprintf("]iguiente, [");
	    textcolor(WHITE);
	    cprintf("O");
	    textcolor(LIGHTGRAY);
	    cprintf("]tro dato, [");
	    textcolor(WHITE);
	    cprintf("A");
	    textcolor(LIGHTGRAY);
	    cprintf("]bandono busqueda ? (S/O/A):");
	    textcolor(WHITE);

	    cara = toupper(getche());
	    textcolor(LIGHTGRAY);
	    if (cara == 'S') continue;
	    else break;
	}

    }
    if (cara == 'O') {
	record_num = 0;
	goto Denuevo;
    }
    clrscr();
    mostrar();
    return;
}
/****************** LIGHT **********************/
void light(int x,int y,int lenght,int attrib)
{
    /* variables */
    char buf[160];
    int i;

    gettext(x,y,x+lenght,y,buf);

    for(i=1;i<24;) {
        buf[i] = attrib;
        i++;
        i++;
    }
    puttext(x,y,x+lenght,y,buf);
}

/******************* ABAJO *********************/
int abajo(int count,int num,int *y)
{
    /* variables */

    count++;
    if (count >= num)
        return count-1;

    light(34,*y,12,7);

    (*y)++;

    light(34,*y,12,0x70);

    return count;
}

/******************* ARRIBA *********************/
int arriba(int count,int *y)
{
    /* variables */

    count--;
    if (count < 0)
	return count+1;

    light(34,*y,12,7);

    (*y)--;

    light(34,*y,12,0x70);

    return count;
}

/******************* DIRECTORIO ****************/
void directorio(void)
{
    /*	 variables */
    int count;
    int done;
    int hand;
    int num;
    int num2;
    int y;
    char carac;

    struct ffblk ffblk;

    num = 0;
    done = findfirst("*.fic",&ffblk,0);

    guindou(1,14,80,23,1);
    while (!done) {
        num++;
        gotoxy(34,num);
        cprintf("%s",ffblk.ff_name);
        done = findnext(&ffblk);
    }
    y = 14;
    count = 0;

    light(34,14,12,0x70);
    Reintentar:                 /* label */

    mensa("Seleccione archivo para abrir.",WHITE);

    do {
        regs.h.ah = 0;
        int86(0x16,&regs,&regs);            /* llamar a int 16h. */

        carac = regs.h.ah;

        if (carac == 0x48)
            count = arriba(count,&y);

        if (carac == 0x50)
            count = abajo(count,num,&y);

        if (regs.h.al == 27) {
            guindou(1,14,80,23,0);
            mensa("No se abri� ning�n archivo.",WHITE);
            return;
        }
    } while (regs.h.al != 13);

    num2 = 0;
	done = findfirst("*.fic",&ffblk,0);
	while (!done) {
	  if (count == num2) {
	    if ( (hand = abrir(ffblk.ff_name)) == -1)
		goto Reintentar;

	    if (handle != -1)
		close(handle);     /* cierra el archivo que estaba abierto */

        strcpy(filename,ffblk.ff_name);
	    handle = hand;
        guindou(1,14,80,23,0);
        mensa("Archivo abierto.",WHITE);
        return;
	  }
	  done = findnext(&ffblk);
	  num2++;
	}
    getch();
    guindou(1,14,80,23,0);
    mensa("Error",WHITE + BLINK);
    return;
}

/**************** VER ****************/
void ver(void)
{
    /*	variables */
    char char_in;
    int siguiente = 1;
    int orden;
    int bibi;

    if (handle == -1) {
    mensa("Error. Antes debe abrir un archivo. Una tecla por favor.",WHITE + BLINK);
    getch();
    mensa("Utilice opcion 1 para abrir archivo",WHITE);
    return;
    }

    guindou(1,1,80,25,1);

    lseek(handle,SI_HEAD,SEEK_SET);
    do {
	if (siguiente == 1) {
	    clrscr();
	    if ( (leer()) == -1) {
		lseek(handle,SI_HEAD,SEEK_SET);
		leer();
	    }
	    mostrar();
	    siguiente = 0;
	}
	if (siguiente == -1) {
	    clrscr();
	    orden = tito1.num_orden;
	    orden--;
	    lseek(handle,(orden * SIZE)+SI_HEAD,SEEK_SET);
	    if ( (leer()) == -1) {
		bibi = lseek(handle,0,SEEK_END);
		bibi = bibi - SIZE;
		lseek(handle,bibi,SEEK_SET);
		leer();
	    }
	    mostrar();
	    siguiente = 0;
	}

	verdatos();
	char_in = toupper(getch());

	switch (char_in) {
        case 'N':   lseek(handle,0,SEEK_END);
                    recpos();
                    obtener();
                    grabar();
                    break;

        case 'M':   lseek(handle,SI_HEAD +(SIZE * tito1.num_orden),SEEK_SET);
                    recpos();
                    obtener();
                    grabar();
                    break;

        case 'B':   buscar();
                    break;

        case 'I':   ir();
                    break;

	    case 'E':   eliminar();
                    break;

	    case 'S':   siguiente = 1;
                    break;

	    case 'A':   siguiente = -1;
                    break;

	}

    } while (char_in != '');
    guindou(1,1,80,25,0);

}

/********************** CREAR **********************/
void crear(void)
{
    /* variables */
    int hand;
    char nombre[14];
    char *ptr_nombre;
    int i;


    guindou(1,14,80,23,1);

    mensa("Ingrese el nombre del archivo que quiere crear.",WHITE);
    centra("Nombre del archivo a crear.:",1);
    centra("[        ]",2);
    gotoxy(36,2);

    nombre[0] = 9;
    ptr_nombre = cgets(nombre);

    for(i=0;i<8;i++){
	if (nombre[2+i] == '.')
		nombre[2+i] = NULL;
    }

    strcat(ptr_nombre,".fic"+NULL);
    mensa("Creando...",WHITE);
    if ( (hand = open(ptr_nombre,O_CREAT | O_EXCL | O_BINARY | O_RDWR,S_IREAD | S_IWRITE)) == -1) {
    guindou(1,14,80,23,0);
    mensa("Imposible crear archivo",WHITE + BLINK);
    return;
    }

    if ( cabeza(hand) == -1) {
    guindou(1,14,80,23,0);
    mensa("Imposible crear archivo",WHITE + BLINK);
    return;
    }

    strcpy(filename,ptr_nombre);
    if (handle == -1) close(handle);
    handle = hand;

    for (i=0;i <(SIZE-2);i++)          /* para que no aparezca basura */
	tito1.nombre[i] = 0;

    guindou(1,14,80,23,0);
    mensa("Archivo creado y abierto.",WHITE);
    return;
}


/*----------------------------------*/
/* MAIN PROGRAM STARTS HERE.... OK? */
/*----------------------------------*/

int main(void)
{
    /*	variables  */
    char char_in;
    handle = -1;

    textcolor(LIGHTGRAY);
    strcpy (cabecera.name,"01.00BASE"+NULL);

   puttext(1,1,80,25,PRESENTA);

    do {
        gotoxy(80,25);
        char_in = getch();
        switch ( char_in ) {
            case '1': directorio();
                      break;
            case '2': ver();
                      break;
            case '3': crear();
                      break;
        }
    } while (char_in != '');

    clrscr();			/* borra la pantalla antes de irse */
    return 0;
}
