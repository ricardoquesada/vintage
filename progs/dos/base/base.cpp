/*
Filename    : BASE.EXE
Author      : Ricardo Quesada
Version     : 1.00
Description : A cardfile maker
*/



// tabla de macros
#define SIZE sizeof(tito1)
#define SI_HEAD sizeof(cabecera)

// include files
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <ctype.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <dir.h>
#include "base.h"

// type declarations

typedef unsigned char BYTE; 			/* definiendo un byte */

typedef struct {
    BYTE nombre[30];
	BYTE apellido[30];
	BYTE telefono[10];
	BYTE direccion[40];
	BYTE codigop[10];
    BYTE fecha[11];
    BYTE varios[70];
    int num_orden;
} FICHA;

FICHA tito1;

struct {
	char name[10];
} cabecera,cabecera2;

// definicion de variables globales
char filename[14];
int handle;


// definicion de funciones

/************************* VERDATOS **************************/
void verdatos(void)
{
    /* variables */
    int i;

    gotoxy(1,12);
    textcolor(RED);
    for (i=0;i<80;i++) cprintf("�");
    gotoxy (1,13);
    textcolor(WHITE);
    cprintf( "\r\n  + : Siguiente.                       - : Anterior."
             "\r\n  M : Modifica este rec.               A : Agrega uno nuevo rec."
             "\r\n  B : Busca dato en los recs.          I : Ir a record."
             "\r\nEsc : para salir.");
    gotoxy(1,18);
    textcolor(RED);
    for (i=0;i<80;i++) cprintf("�");
    textcolor(LIGHTGRAY);
}

/********************** LINEA ***************/
void linea(void)
{
    /* variables */
    int i;

    gotoxy(1,1);
    textcolor(WHITE);
    cprintf("NOMBRE DE FICHA: %s",filename);
    gotoxy(40,1);
    cprintf("NUMERO DE RECORD: %d",tito1.num_orden);
    gotoxy(1,2);
    textcolor(RED);
    for (i=0;i<80;i++) cprintf("�");
    textcolor(LIGHTGRAY);
}

/************************** MOSTRAR **************************/
void mostrar(void)
{
    linea();
    gotoxy(1,3);
    printf("\nNombre: %s",tito1.nombre);
	printf("\nApellido: %s",tito1.apellido);
	printf("\nTelefono: %s",tito1.telefono);
	printf("\nDireccion: %s",tito1.direccion);
	printf("\nCodigo Postal: %s",tito1.codigop);
    printf("\nFecha: %s",tito1.fecha);
    printf("\nVarios: %s",tito1.varios);

}


/************************** OBTENER **************************/
void obtener(void)
{
	/* variables */
	char carac;
    char gral[73];
    char *ptr_gral;
    int i;

	do{
        for(i=0;i<(SIZE-2);i++)
            tito1.nombre[i] = 0;
        clrscr();
        verdatos();
        linea();

        gotoxy(1,4);
        printf("Nombre: ");
        gral[0] = 30;
        ptr_gral = cgets(gral);
		strcpy(tito1.nombre,ptr_gral);

        printf("\nApellido: ");
        gral[0] = 30;
        ptr_gral = cgets(gral);
		strcpy(tito1.apellido,ptr_gral);

        printf("\nTelefono: ");
        gral[0] = 10;
        ptr_gral = cgets(gral);
        strcpy(tito1.telefono,ptr_gral);

        printf("\nDireccion: ");
        gral[0] = 40;
        ptr_gral = cgets(gral);
		strcpy(tito1.direccion,ptr_gral);

        printf("\nCodigo Postal: ");
        gral[0] = 10;
        ptr_gral = cgets(gral);
		strcpy(tito1.codigop,ptr_gral);

        printf("\nFecha: ");
        gral[0] = 11;
        ptr_gral = cgets(gral);
        strcpy(tito1.fecha,ptr_gral);

        printf("\nVarios: ");
        gral[0] = 70;
        ptr_gral = cgets(gral);
        strcpy(tito1.varios,ptr_gral);

        gotoxy(1,21);
		printf("Todos estos datos son correctos (s/n) ?:");
		carac = toupper(getche());
	} while (carac !='S');
    gotoxy(1,21);
    printf("                                             ");

}

/************************** CABEZA ***************************/
int cabeza(int hand)
{
	/* variables */
	int retorno ;

    if ( (retorno = write(hand,&cabecera,SI_HEAD)) == -1 )
		 perror("\nError");
	return retorno;
}

/************************** ABRIR ****************************/

int abrir(char *ptr_nombre)
{
    /*  variables */
    int hand;

    gotoxy (30,2);

    if ((hand = open ( ptr_nombre,O_RDWR | O_BINARY)) == -1) {
		perror("Error ");
		getch();
        return hand ;
	}
    if ( read(hand,&cabecera2,SI_HEAD) != SI_HEAD) {
		perror("Error ");             /* futuras modificaciones */
		getch();
        close(hand);
        return -1;                  /* se pueden hacer aca    */
	}

	if ( strcmp(cabecera.name,cabecera2.name) != 0) {
		printf("Archivo no grabado por Base 1.00");
		getch();
        close(hand);
        return -1;
	}
	printf("Archivo abierto.");
	getch();
    return hand;
}
/*************************** RECPOS **********************/
void recpos(void)
{
    /* variables */
	unsigned int cucu;

	(cucu) = lseek(handle,0,SEEK_CUR);
	cucu = (cucu - SI_HEAD) / SIZE;
	tito1.num_orden = cucu;

}

/**************************** GRABAR ************************/
int grabar(void)
{

    if ( (write (handle,&tito1,SIZE) ) != SIZE ) {
		perror("Error:");
        return -1;
    }
	return 0;
}

/*********************** LEER ***********************/
int leer(void)
{
	/*	variable  */
	int res;

	if ( (res = read (handle,&tito1,SIZE) ) != SIZE )
		res = -1;
	return res;
}
/******************** GOREC ********************/
int gorec(int i)
{
	/* variables */
	int retorno;

	retorno = lseek(handle,SI_HEAD + (SIZE * i),SEEK_SET);
	return retorno;
}

/******************* IR ***********************/
int ir(void)
{
    /* variables */
    char reco[6];
    char *ptr_rec;
    int rec;

    gotoxy(1,20);
    reco[0] = 4;
    printf("Ingrese numero de record:");
    ptr_rec = cgets(reco);
    rec = atoi(ptr_rec);

    gorec(rec);
    rec = leer();

    clrscr();
    mostrar();
    verdatos();
    return rec;
}

/******************* BUSCO *********************/
int busco(char *ptr)
{
    /* variables */
    int i=0;
    int j=0;
    char *old_ptr;
    old_ptr = ptr;


    for(;i <= SIZE;i++) {

        if ( toupper(*ptr) == toupper(tito1.nombre[i]) ) {
            for (;;j++) {
                ptr++;
                if (*ptr == NULL)
                    return 0;
                i++;
                if (i > SIZE)
                    return -1;
                if ( toupper(*ptr) != toupper(tito1.nombre[i]) )  {
                    i--;
                    ptr = old_ptr;
                    break;
                }
            }
        }
    }
    return -1;
}

/******************* BUSCAR ********************/
void buscar(void)
{
	/* variables */
	char busc[43];
	char *ptr_busc;
	int record_num = 0;
    char cara;
    int i;

	Denuevo:					/* label */
    clrscr();
    mostrar();
    verdatos();

    gotoxy(1,20);
	printf("Introduzca el dato que quiere buscar.\n:");
	busc[0] = 40;
	ptr_busc = cgets(busc);

	for (;;record_num++) {
        gotoxy(1,22);
        printf("Buscando en record:%d el dato \"%s\".",record_num,ptr_busc);

        if ( gorec(record_num) == -1) {
            gotoxy(1,23);
            cprintf("[");
            textcolor(WHITE);
            cprintf("A");
            textcolor(LIGHTGRAY);
            cprintf("]bando busqueda, [");
            textcolor(WHITE);
            cprintf("O");
            textcolor(LIGHTGRAY);
            cprintf("]tro dato?(a/o)");
            gotoxy(1,25);
            textcolor(WHITE + BLINK);
            cprintf("Dato no encontrado.");
            textcolor(LIGHTGRAY);
            cara = toupper(getche());
            break;
		}
        if (leer() == -1) {
            gotoxy(1,23);
            cprintf("[");
            textcolor(WHITE);
            cprintf("A");
            textcolor(LIGHTGRAY);
            cprintf("]bando busqueda, [");
            textcolor(WHITE);
            cprintf("O");
            textcolor(LIGHTGRAY);
            cprintf("]tro dato?(a/o)");
            gotoxy(1,25);
            textcolor(WHITE + BLINK);
            cprintf("Dato no encontrado.");
            textcolor(LIGHTGRAY);
            cara = toupper(getche());
            break;
		}
        if ( (busco(ptr_busc)) == 0) {
            clrscr();
            mostrar();
            verdatos();
            gotoxy(1,21);
            cprintf("Busco [");
            textcolor(WHITE);
            cprintf("S");
            textcolor(LIGHTGRAY);
            cprintf("]iguiente, [");
            textcolor(WHITE);
            cprintf("O");
            textcolor(LIGHTGRAY);
            cprintf("]tro dato, [");
            textcolor(WHITE);
            cprintf("A");
            textcolor(LIGHTGRAY);
            cprintf("]bandono busqueda ? (S/O/A):");
            textcolor(WHITE);

            cara = toupper(getche());
            textcolor(LIGHTGRAY);
            if (cara == 'S') continue;
            else break;
		}

	}
    if (cara == 'O') {
        record_num = 0;
        goto Denuevo;
    }
	clrscr();
	mostrar();
	return;
}


/******************* DIRECTORIO ****************/
void directorio(void)
{
	/*	 variables */
	int count;
	int done;
	int numero;
    int hand;
    char str_numero[5];
	char *ptr_numero;

	struct ffblk ffblk;

	Reintentar: 			/* label Reintentar */

	count = 0;
	clrscr();
    done = findfirst("*.fic",&ffblk,0);
	while (!done)
	{
		count++;
        if ( (count % 23) == 0) {
            printf("\n**** Una tecla por favor. ****");
            getch();
		}
        textcolor(WHITE);
        cprintf("[");
        textcolor(YELLOW);
        cprintf("%d",count);
        textcolor(WHITE);
        cprintf("] - %s\r\n",ffblk.ff_name);
        textcolor(LIGHTGRAY);
        done = findnext(&ffblk);

	}
	if (count == 0) {						/* no hay *.fic */
        textcolor(WHITE);
        cprintf("No se encontraron archivos .fic");
        textcolor(LIGHTGRAY);
        getch();
        return;
	}
	do {
		str_numero[0] = 3;
        gotoxy(30,1);
        printf("Ingrese numero de archivo [C/R ninguno]:");
		ptr_numero = cgets(str_numero);
        if (str_numero[1] == 0) return;
        numero = atoi(ptr_numero);
	} while (numero == 0);

	count = 1;
	done = findfirst("*.fic",&ffblk,0);
	while (!done)
	{
		if (count == numero)
			{
            if ( (hand = abrir(ffblk.ff_name)) == -1)
				goto Reintentar;

            if (handle == -1)
                close(handle);     /* cierra el archivo que estaba abierto */

            strcpy(filename,ffblk.ff_name);
            handle = hand;
            return;
			}

		done = findnext(&ffblk);
		count++;

	}
    gotoxy(30,2);
	printf("Numero incorrecto.");
    gotoxy(30,3);
	printf("Una tecla por favor");
	getch();
	goto Reintentar;
}

/**************** VER ****************/
void ver(void)
{
	/*	variables */
	char char_in;
	int siguiente = 1;
	int orden;
	int bibi;

	if (handle == -1) {
        gotoxy(1,25);
        textcolor(WHITE + BLINK);
        cprintf("             Error. Antes debe abrir un archivo. Una tecla por favor.          ");
        textcolor(LIGHTGRAY);
        getch();
		return;
	}

	lseek(handle,SI_HEAD,SEEK_SET);

	do {
		if (siguiente == 1) {
			clrscr();
            if ( (leer()) == -1) {
				lseek(handle,SI_HEAD,SEEK_SET);
                leer();
			}
			mostrar();
			siguiente = 0;
		}
		if (siguiente == -1) {
			clrscr();
			orden = tito1.num_orden;
			orden--;
			lseek(handle,(orden * SIZE)+SI_HEAD,SEEK_SET);
            if ( (leer()) == -1) {
				bibi = lseek(handle,0,SEEK_END);
				bibi = bibi - SIZE;
				lseek(handle,bibi,SEEK_SET);
                leer();
			}
			mostrar();
			siguiente = 0;
		}

		verdatos();
		char_in = toupper(getch());

		switch (char_in) {
			case 'A':
            lseek(handle,0,SEEK_END);
            recpos();
            obtener();
            grabar();
			break;

			case 'M':
            lseek(handle,SI_HEAD +(SIZE * tito1.num_orden),SEEK_SET);
            recpos();
            obtener();
            grabar();
			break;

			case 'B':
            buscar();
			break;

            case 'I':
            ir();
            break;

            case '+':
			siguiente = 1;
			break;

			case '-':
			siguiente = -1;
			break;

		}

	} while (char_in != '');

}

/********************** CREAR **********************/
void crear(void)
{
	/* variables */
    int hand;
	char nombre[14];
	char *ptr_nombre;
	int i;

    Reintent:           /* label Reintent */
	clrscr();
	printf("Ingrese el nombre del archivo que quiere crear :");
	nombre[0] = 9;
	ptr_nombre = cgets(nombre);

	for(i=0;i<8;i++){
		if (nombre[2+i] == '.')
			nombre[2+i] = NULL;
    }

	strcat(ptr_nombre,".fic"+NULL);
	printf("\nCreando el archivo... %s",ptr_nombre);
    if ( (hand = open(ptr_nombre,O_CREAT | O_EXCL | O_BINARY | O_RDWR,S_IREAD | S_IWRITE)) == -1) {
        perror("\nImposible de crear archivo.\n");
        printf("\nPresione 'r' para reintentar.");
        if (toupper(getch()) == 'R')
            goto Reintent;
        return;
    }

    if ( cabeza(hand) == -1) {
        perror("\nImposible de crear archivo.\n");
        printf("\nPresione 'r' para reintentar.");
        if (toupper(getch()) == 'R')
            goto Reintent;
        return;
    }
    printf("\nArchivo creado y abierto.");
    printf("\nUna tecla para continuar.");

    strcpy(filename,ptr_nombre);
    if (handle == -1) close(handle);
    handle = hand;

    for (i=0;i <(SIZE-2);i++)          /* para que no aparezca basura */
        tito1.nombre[i] = 0;

    getch();
    return;
}


// definicion de global variables.


/*----------------------------------*/
/* MAIN PROGRAM STARTS HERE.... OK? */
/*----------------------------------*/

int main(void)
{
	/*	variables  */
	char char_in;
    handle = -1;

    textcolor(LIGHTGRAY);
    strcpy (cabecera.name,"01.00BASE"+NULL);


	do {
        puttext(1,1,80,25,PRESENTA);
        gotoxy(80,25);
        char_in = getch();
        switch ( char_in )
		{
            case '1': directorio();
				break;
            case '2': ver();
				break;
            case '3': crear();
				break;
		}
	} while (char_in != '');

	clrscr();				/* borra la pantalla antes de irse */
	return 0;
}
