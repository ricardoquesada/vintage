;-----------------------------------------------------------------------------;
;                                                                             ;
;                        Funciones para manejar Graficos                      ;
;  Todas fueron hechas por Ricardo Quesada salvo las que digan lo contrario   ;
;                                                                             ;
;-----------------------------------------------------------------------------;
P286

;---------------------------;
;		VerticalR			;
;---------------------------;
verticalr proc near
    pusha

	mov   dx,VERT_RESCAN   ;Wait for end of
ula1:
	in	  al,dx 		   ;vertical rescan
	test  al,8
	jne   ula1
ula2:
	in	  al,dx 		   ;Go to start of rescan
	test  al,8
	je	  ula2

    popa
	ret
verticalr endp

;---------------------------;
;	displayletras			;
;---------------------------;
;	In: 					;
;		ax	= x 			;
;		bx	= y 			;
;       cx  = Color         ;
;       dx  = Inc del color ;
;       si  = Offset asciiz ;
;---------------------------;
displayletras proc near
    pusha
    push ds

    push cs
    pop ds

	mov x0,ax
	mov y0,bx
	mov Charcolor,cx
    mov varcolor,dx

loopdl3:
	mov  bp,offset tabla

	xor  ah,ah
	mov  al,[si]
	or	 al,al
	jne  nodl1

    pop ds
    popa
    ret

nodl1:
	mov  cl,4
	shl  ax,cl						;multiplica por 8
	add  bp,ax						;puntero de la letra.

	mov di,16
loopdl2:
	mov cx,8
	mov dl,[bp]

loopdl1:
	push cx
	shl dl,1
	jc spix

    mov cl,0
	jmp short cpix
spix:
    xor cx,cx
    cmp varcolor,0          ; Compara
    je  dlet1color          ; Var de color � no ? then no change
    mov cx,x0
	add cx,y0
    and cx,varcolor         ; Recomendabel 6 para buen efecto.
dlet1color:
    add cx,CharColor        ; algoritmo para poner el color
cpix:
	mov ax,x0				; x
	mov bx,y0				; y
	call setpix
	inc x0
	pop cx
	loop loopdl1
	sub x0,8
	inc y0
	inc bp
	dec di
	or di,di
	jne loopdl2
	inc si
	sub y0,16
	add x0,8
	jmp loopdl3

CharColor   dw 0    ; Usada para el Color de los textos (fn interna del Displayletras)
VarColor    dw 0    ; Usada para la variaci�n del color de los chars ( " " " " )

displayletras endp

