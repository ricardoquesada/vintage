;---------------------------;
; Sprites	-	Estructuras ;
;---------------------------;
spritestruc equ this word
    dw  offset spr_e1
    dw  offset spr_l1
    dw  offset spr_n2
    dw  offset spr_i2
    dw  offset spr_m2
    dw  offset spr_o2
    dw  offset spr_d3
    dw  offset spr_e3
    dw  offset spr_l3
    dw  offset spr_s4
    dw  offset spr_o4
    dw  offset spr_l4

spr_e1 equ this word
    dw  0               ; pos original x
    dw  0               ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_e ; datos del sprite 0
    dw  offset sprbmk_e ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_l1 equ this word
    dw  00              ; pos original x
    dw  20              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_l ; datos del sprite 0
    dw  offset sprbmk_l ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_n2 equ this word
    dw  0               ; pos original x
    dw  120             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_n ; datos del sprite 0
    dw  offset sprbmk_n ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_i2 equ this word
    dw  0               ; pos original x
    dw  60              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_i ; datos del sprite 0
    dw  offset sprbmk_i ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_m2 equ this word
    dw  0               ; pos original x
    dw  80              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_m ; datos del sprite 0
    dw  offset sprbmk_m ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_o2 equ this word
    dw  0               ; pos original x
    dw  100             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_o ; datos del sprite 0
    dw  offset sprbmk_o ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_d3 equ this word
    dw  160             ; pos original x
    dw  00              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_d ; datos del sprite 0
    dw  offset sprbmk_d ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_e3 equ this word
    dw  160             ; pos original x
    dw  20              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_e ; datos del sprite 0
    dw  offset sprbmk_e ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_l3 equ this word
    dw  160             ; pos original x
    dw  120             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_l ; datos del sprite 0
    dw  offset sprbmk_l ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_s4 equ this word
    dw  160             ; pos original x
    dw  60              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_s ; datos del sprite 0
    dw  offset sprbmk_s ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_o4 equ this word
    dw  160             ; pos original x
    dw  80              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_o ; datos del sprite 0
    dw  offset sprbmk_o ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)

spr_l4 equ this word
    dw  160             ; pos original x
    dw  100             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_l ; datos del sprite 0
    dw  offset sprbmk_l ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)


;---------------------------;
;   Datos de los sprites    ;
;---------------------------;
; Todos los sprites fueron  ;
; generados por MGraph de   ;
; Sergio Lerner (c) 1992.   ;
;---------------------------;

sprdat_e label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,15,15,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_e  db 20 * 20 / 2 dup ( 0 )

sprdat_l label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_l  db 20 * 20 / 2 dup ( 0 )


sprdat_n label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,15,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,15,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,13,13,15,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,15,13,13,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,15,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_n  db 20 * 20 / 2 dup ( 0 )

sprdat_i label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_i  db 20 * 20 / 2 dup ( 0 )

sprdat_m label byte
db 0,0,0,0,0,0,0,15,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,15,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,15,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,15,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,13,13,15,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,15,13,13,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,15,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_m  db 20 * 20 / 2 dup ( 0 )

sprdat_o label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,15,15,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_o  db 20 * 20 / 2 dup ( 0 )


sprdat_d label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,15,15,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,13,13,13,13,13,13,15,0,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,13,13,13,13,15,0,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,0,0,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_d  db 20 * 20 / 2 dup ( 0 )


sprdat_s label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,15,15,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,13,13,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,15,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,15,13,13,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,13,13,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,13,13,13,15,0,0,0,0,0,0,0
db 0,15,15,15,15,13,13,13,13,13,13,13,15,0,0,0,0,0,0,0
db 0,15,13,13,13,13,13,13,13,15,15,15,15,0,0,0,0,0,0,0
db 0,15,13,13,13,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_s  db 20 * 20 / 2 dup ( 0 )


