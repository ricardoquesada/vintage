;-----------------------------------------------;
;												;
;					Intro 05					;
;			El Ni�o del Sol (c) 1994.			;
;												;
;-----------------------------------------------;
;		Segunda intro en modo gr�fico			;
;		  Hecha para probar sprites.			;
;-----------------------------------------------;
P286

code	segment public byte 'code'
org 	100h
intro05 PROC
assume cs:code,ds:code,ss:code,es:code

VERT_RESCAN    = 3DAh			  ;Input status register #1
TOTSPRITES     = 12

;---------------------------;
start:
;---------------------------;
	call is286
	call isvga
	call init320200
	call mainprogram

	mov ax,3
	int 10h 			;volver al modo texto.

    push cs
    pop  es
    mov bp,offset TABLA
    mov bh,10h           ;cantidad de bytes x char
    mov cx,100h          ;total de chars (256)
    xor dx,dx            ;a partir de que char (0).
    xor bl,bl            ;map 2.Grabar en 1er tabla
    mov ax,1110h         ;Define los carecteres creado por el usuario.
    int 10h

    mov ax,4c00h
	int 21h

;---------------------------;
;		Is286				;
;---------------------------;
is286 proc near
	xor ax,ax
	push ax
	popf
	pushf
	pop ax
	and ax,0f000h
	cmp ax,0f000h
	je not286
	ret
not286:
	mov dx,offset no286msg
	mov ah,9
	int 21h
	mov ax,4cffh
	int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc near
		mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near

    mov  cl,2
	call showpage

    call lineas             ; dibujar Background.
    call letras             ; dibujar background

    mov cl,0
    call showpage           ;

    mov cl,0
contcreat:
    call createspr
    inc cl
    cmp cl,TOTSPRITES        ; Crear 12 sprites.
    jne contcreat

mainhere1:
    call bouncesprites
    call getkey
    jne  mainhere1

    ret
mainprogram endp

;---------------------------;
;         Lineas            ;
;---------------------------;
lineas proc near
    pusha

    mov cx,0
lineasloop1:
    push cx
    call setpage

    xor bx,bx
acahijo2:
    xor ax,ax
acahijo:
    xor cx,cx
    add cx,ax
    add cx,bx
    and cx,255 - 15
    call setpix
    inc ax
    cmp ax,319
    jb  acahijo
    inc bx
    cmp bx,180
    jbe acahijo2

    pop cx
    inc cx
    cmp cx,2
    jne lineasloop1

    popa
    ret
lineas endp

;---------------------------;
;		Letras				;
;---------------------------;
letras proc near
    pusha
    push ds

    push cs
    pop ds

    mov bx,0
letrasloop2:
    mov cl,0
letrasloop1:
    push cx
    call setpage
    mov ax,8                            ; x0
    mov cx,24                           ; Color
    mov dx,7                            ; Variaci�n de color.
    mov di,1                            ; Transparencia, SI!
    mov si,offset message0
	call displayletras
    pop  cx
    inc  cx
    cmp  cx,2
    jne  letrasloop1

    add  bx,16
    cmp  bx,170
    jb   letrasloop2

    mov cl,0
letrasloop3:
    push cx
    call setpage
    mov ax,8                            ; x0
    mov bx,182
    mov cx,86                           ; Color
    mov dx,9                            ; Variaci�n de color.
    mov di,1                            ; Transparencia, SI!
    mov si,offset message1
	call displayletras
    pop  cx
    inc  cx
    cmp  cx,2
    jne  letrasloop3

    pop ds
    popa
    ret
letras	endp

;---------------------------;
;     BounceSprites         ;
;---------------------------;
;  Hace mover a los sprites ;
;---------------------------;
bouncesprites proc near

    mov cx,0
submoverpelotas:

    push cx
    shl  cx,1
    mov  di,offset spritespings
    add  di,cx
    mov  si,[ di ]

    mov  ax,[ si.sping_orgx ]
    mov  bx,[ si.sping_orgy ]

    pop  cx
    push cx
    call posspr                     ; pocisiona Sprite.

    call movex
    call movey

    pop  cx
    inc  cx
    cmp  cx,TOTSPRITES              ; TOTAL DE SPRITES
    jne  submoverpelotas
    call movesprites                ; mover los 12 sprites.

    ret
bouncesprites endp

;---------------------------;
;         movex             ;
;---------------------------;
; fnc int del BounceSprites ;
;---------------------------;
movex proc near
    push ax
    mov  ax,[ si.sping_orgx ]       ; pos x
    add  ax,[ si.sping_incx ]       ; incremento x
    cmp  ax,300
    ja   notmx1
    cmp  ax,1
    jb   notmx1
    jmp  short nonotmx1
notmx1:
    not  word ptr [ si.sping_incx ] ; incremento x
    inc  word ptr [ si.sping_incx ] ; incremento x
nonotmx1:
    mov  [ si.sping_orgx ],ax       ; pos x
    pop  ax
    ret
movex endp

;---------------------------;
;         movey             ;
;---------------------------;
; fnc int del BounceSprites ;
;---------------------------;
movey proc near
    push ax
    push cx
    inc  word ptr [ si.sping_incy ] ; incremey
    mov  ax,[ si.sping_incy ]       ; incremey
    cmp  ax,80                      ; m�ximo que puede subir.
    je   notmy1
    cmp  ax,0
    je   notmy1
    jmp  short nonotmy1

notmy1:
    not  word ptr [ si.sping_incy ] ; incremey
    inc  word ptr [ si.sping_incy ] ; incremey
nonotmy1:

    mov  cx,[ si.sping_incy ]       ; incremey
    cmp  ch,0
    jne  heremy2

    shr  ax,4                       ; Ancho de salto.

    jmp  short endmy1
heremy2:
    dec  ax
    not  ax
    inc  ax
    shr  ax,4                       ; Ancho de salto.
    not  ax
    inc  ax
endmy1:
    add  [ si.sping_orgy ],ax          ; pos y
    pop  cx
    pop  ax
    ret
movey endp


;---------------------------;
;		getkey				;
;---------------------------;
getkey proc near
    push ax
;    mov  ah,1
;    int  16h

    in  al,060h
    cmp al,01h
    pop ax
    ret
getkey endp

;---------------------------;
;       Estructuras         ;
;---------------------------;

sprstruc struc
    sp_orgx dw  ?               ; pos original x
    sp_orgy dw  ?               ; pos original y
    sp_nowx dw  ?               ; ubicaci�n de ahora x
    sp_nowy dw  ?               ; ubicaci�n de ahroa y
    sp_page db  ?               ; p�gina donde esta
    sp_tamx dw  ?               ; tama�o x
    sp_tamy dw  ?               ; tama�o y
    sp_dato dw  ?               ; datos del sprite 1
    sp_mask dw  ?               ; datos de la m�scara.
    sp_oldx dw  ?               ; ubicaci�n old de x
    sp_oldy dw  ?               ; ubicaci�n old de y
    sp_visi db  ?               ; sprite visible al crearse ?
    sp_offs db  ?               ; offset del sprite (0-3)
sprstruc ends

sprping struc
    sping_orgx  dw ?            ; Coordenadas originales    X
    sping_orgy  dw ?            ;                           Y
    sping_incx  dw ?            ; Incremento de X
    sping_incy  dw ?            ;               Y
sprping ends

spritespings    label word
    dw offset sprbounc1
    dw offset sprbounc2
    dw offset sprbounc3
    dw offset sprbounc4
    dw offset sprbounc5
    dw offset sprbounc6
    dw offset sprbounc7
    dw offset sprbounc8
    dw offset sprbounc9
    dw offset sprbounc10
    dw offset sprbounc11
    dw offset sprbounc12


sprbounc1   label word
        dw 10           ; + 0    ( coordenas iniciales )
        dw 180          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw -80          ; + 8    ( valores negativos suben )

sprbounc2   label word
        dw 20           ; + 0    ( coordenas iniciales )
        dw 8bh          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 0ffbah       ; + 8    ( valores negativos suben )

sprbounc3   label word
        dw 40           ; + 0    ( coordenas iniciales )
        dw 48h          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 0ffceh       ; + 8    ( valores negativos suben )

sprbounc4   label word
        dw 50           ; + 0    ( coordenas iniciales )
        dw 31h          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 0ffd8h       ; + 8    ( valores negativos suben )


sprbounc5   label word
        dw 60           ; + 0    ( coordenas iniciales )
        dw 1eh          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 0ffe2h       ; + 8    ( valores negativos suben )


sprbounc6   label word
        dw 70           ; + 0    ( coordenas iniciales )
        dw 14h          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 0ffech       ; + 8    ( valores negativos suben )


sprbounc7   label word
        dw 90           ; + 0    ( coordenas iniciales )
        dw 00fh         ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 00000h       ; + 8    ( valores negativos suben )


sprbounc8   label word
        dw 100          ; + 0    ( coordenas iniciales )
        dw 0fh          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 0000ah       ; + 8    ( valores negativos suben )


sprbounc9   label word
        dw 110          ; + 0    ( coordenas iniciales )
        dw 014h         ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 00014h       ; + 8    ( valores negativos suben )


sprbounc10  label word
        dw 130          ; + 0    ( coordenas iniciales )
        dw 31h          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 00028h       ; + 8    ( valores negativos suben )


sprbounc11  label word
        dw 140          ; + 0    ( coordenas iniciales )
        dw 048h         ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 32h          ; + 8    ( valores negativos suben )


sprbounc12  label word
        dw 150          ; + 0    ( coordenas iniciales )
        dw 66h          ; + 2
        dw 1            ; + 6    ( 1=derecha -1=izquierda )
        dw 3ch          ; + 8    ( valores negativos suben )


message0    db 'PRUEBA DE SPRITES - SUPER BETA VERSION',0
message1    db '     by El Ni�o del Sol     ',0

vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'

;---------------------------;
;       Includes            ;
;---------------------------;
include     320200.inc                  ; Incluye funciones para manejo de pixels.
include     sprites.inc                 ; Incluye las funciones de los sprites.
include     font0001.inc                ; Fonts
include     sprdat.inc                  ; Datos de los sprites.

intro05 endp
code	ends
        end intro05
