;-----------------------------------------------;
;												;
;					Intro 05					;
;			El Ni�o del Sol (c) 1994.			;
;												;
;-----------------------------------------------;
;		Segunda intro en modo gr�fico			;
;		  Hecha para probar sprites.			;
;-----------------------------------------------;
P286

code	segment public byte 'code'
org 	100h
intro05 PROC
assume cs:code,ds:code,ss:code,es:code

VERT_RESCAN    = 3DAh			  ;Input status register #1
TOTSPRITES     = 16               ; Total de sprites reales.

;---------------------------;
start:
;---------------------------;


;---------------------------;
;       Includes            ;
;---------------------------;
include     320200.inc                  ; Incluye funciones para manejo de pixels.
include     sprites.inc                 ; Incluye las funciones de los sprites.
include     font0001.inc                ; Fonts
include     sprdat.inc                  ; Datos de los sprites.

intro05 endp
code	ends
        end intro05
