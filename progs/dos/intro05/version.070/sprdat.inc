;---------------------------;
; Sprites	-	Estructuras ;
;---------------------------;
spritestruc equ this word
    dw  offset spr_e1               ;  0
    dw  offset spr_l1               ;  1
    dw  offset spr_n2               ;  2
    dw  offset spr_i2               ;  3
    dw  offset spr_m2               ;  4
    dw  offset spr_o2               ;  5
    dw  offset spr_d3               ;  6
    dw  offset spr_e3               ;  7
    dw  offset spr_l3               ;  8
    dw  offset spr_s4               ;  9
    dw  offset spr_o4               ; 10
    dw  offset spr_l4               ; 11
    dw  offset spr_com1             ; 12
    dw  offset spr_com2             ; 13
    dw  offset spr_com3             ; 14
    dw  offset spr_com4             ; 15
    dw  offset spr_star             ; 16
    dw  offset spr_e11              ; 17
    dw  offset spr_l11              ; 18
    dw  offset spr_n22              ; 19
    dw  offset spr_i22              ; 20
    dw  offset spr_m22              ; 21
    dw  offset spr_o22              ; 22
    dw  offset spr_d33              ; 23
    dw  offset spr_e33              ; 24
    dw  offset spr_l33              ; 25
    dw  offset spr_s44              ; 26
    dw  offset spr_o44              ; 27
    dw  offset spr_l44              ; 28

spr_e1 equ this word
    dw  0               ; pos original x
    dw  0               ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_e ; datos del sprite 0
    dw  offset sprbmk_e ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_l1 equ this word
    dw  80              ; pos original x
    dw  00              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_l ; datos del sprite 0
    dw  offset sprbmk_l ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_n2 equ this word
    dw  160             ; pos original x
    dw  00              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_n ; datos del sprite 0
    dw  offset sprbmk_n ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_i2 equ this word
    dw  240             ; pos original x
    dw  00              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_i ; datos del sprite 0
    dw  offset sprbmk_i ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_m2 equ this word
    dw  00              ; pos original x
    dw  20              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_m ; datos del sprite 0
    dw  offset sprbmk_m ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_o2 equ this word
    dw  80              ; pos original x
    dw  20              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_o ; datos del sprite 0
    dw  offset sprbmk_o ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_d3 equ this word
    dw  160             ; pos original x
    dw  20              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_d ; datos del sprite 0
    dw  offset sprbmk_d ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_e3 equ this word
    dw  240             ; pos original x
    dw  20              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_e ; datos del sprite 0
    dw  offset sprbmk_e ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_l3 equ this word
    dw  0               ; pos original x
    dw  40              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_l ; datos del sprite 0
    dw  offset sprbmk_l ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_s4 equ this word
    dw  80              ; pos original x
    dw  40              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_s ; datos del sprite 0
    dw  offset sprbmk_s ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_o4 equ this word
    dw  160             ; pos original x
    dw  40              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_o ; datos del sprite 0
    dw  offset sprbmk_o ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_l4 equ this word
    dw  240             ; pos original x
    dw  40              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_l ; datos del sprite 0
    dw  offset sprbmk_l ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_e11 equ this word
    dw  0               ; pos original x
    dw  60              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_e2 ; datos del sprite 0
    dw  offset sprbm_e2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_l11 equ this word
    dw  80              ; pos original x
    dw  60              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_l2 ; datos del sprite 0
    dw  offset sprbm_l2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_n22 equ this word
    dw  160             ; pos original x
    dw  60              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_n2 ; datos del sprite 0
    dw  offset sprbm_n2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_i22 equ this word
    dw  240             ; pos original x
    dw  60              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprdat_i ; datos del sprite 0
    dw  offset sprbmk_i ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_m22 equ this word
    dw  00              ; pos original x
    dw  80              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_m2 ; datos del sprite 0
    dw  offset sprbm_m2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_o22 equ this word
    dw  80              ; pos original x
    dw  80              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_o2 ; datos del sprite 0
    dw  offset sprbm_o2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_d33 equ this word
    dw  160             ; pos original x
    dw  80              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_d2 ; datos del sprite 0
    dw  offset sprbm_d2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_e33 equ this word
    dw  240             ; pos original x
    dw  80              ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_e2 ; datos del sprite 0
    dw  offset sprbm_e2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_l33 equ this word
    dw  0               ; pos original x
    dw  100             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_l2 ; datos del sprite 0
    dw  offset sprbm_l2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_s44 equ this word
    dw  80              ; pos original x
    dw  100             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_s2 ; datos del sprite 0
    dw  offset sprbm_s2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_o44 equ this word
    dw  160             ; pos original x
    dw  100             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_o2 ; datos del sprite 0
    dw  offset sprbm_o2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_l44 equ this word
    dw  240             ; pos original x
    dw  100             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  20              ; tama�o x
    dw  20              ; tama�o y
    dw  offset sprda_l2 ; datos del sprite 0
    dw  offset sprbm_l2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_com1 equ this word
    dw  0               ; pos original x
    dw  180             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  24              ; tama�o x
    dw  20              ; tama�o y
    dw  offset tapa     ; datos del sprite 0
    dw  offset tapabm   ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_com2 equ this word
    dw  296             ; pos original x
    dw  180             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  24              ; tama�o x
    dw  20              ; tama�o y
    dw  offset tapa     ; datos del sprite 0
    dw  offset tapabm   ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_star equ this word
    dw  128             ; pos original x
    dw  120             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  8               ; tama�o x
    dw  6               ; tama�o y
    dw  offset star3    ; datos del sprite 0
    dw  offset star3bm  ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_com3 equ this word
    dw  0               ; pos original x
    dw  120             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  16              ; tama�o x
    dw  08              ; tama�o y
    dw  offset sprd_co1 ; datos del sprite 0
    dw  offset sprb_co1 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  1               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

spr_com4 equ this word
    dw  64              ; pos original x
    dw  120             ; pos original y
    dw  ?               ; ubicaci�n de ahora x
    dw  ?               ; ubicaci�n de ahroa y
    db  1               ; p�gina donde esta
    dw  16              ; tama�o x
    dw  08              ; tama�o y
    dw  offset sprd_co2 ; datos del sprite 0
    dw  offset sprb_co2 ; datos de la m�scara.
    dw  ?               ; ubicaci�n old de x
    dw  ?               ; ubicaci�n old de y
    db  0               ; sprite visible
    db  0               ; offset del sprite (0-3)
    dw  ?,?,?,?,?       ; Copias de orgx,orgy,tamx,tamy,maskk ( SprImage )

;---------------------------;
;   Datos de los sprites    ;
;---------------------------;
; Todos los sprites fueron  ;
; generados por MGraph de   ;
; Sergio Lerner (c) 1992.   ;
;---------------------------;

sprdat_e label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,66,66,66,15,0,0,0,0,0,0,0
db 0,15,15,15,15,67,67,67,67,67,67,67,15,0,0,0,0,0,0,0
db 0,15,68,68,68,68,68,68,68,15,15,15,15,0,0,0,0,0,0,0
db 0,15,69,69,69,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,71,71,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,72,72,15,15,15,15,15,72,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,73,73,73,73,73,73,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,74,74,74,74,74,15,15,15,15,0,0,0,0,0,0,0
db 0,15,75,75,15,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,78,78,15,15,15,15,15,78,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,79,79,79,79,79,79,79,15,0,0,0,0,0,0,0
db 0,15,80,80,80,80,80,80,80,15,15,15,15,0,0,0,0,0,0,0
db 0,15,81,81,81,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_e  db 20 * 20 / 2 dup ( 0 )

sprdat_l label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,68,68,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,69,69,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,71,71,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,72,72,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,78,78,15,15,15,15,15,78,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,79,79,79,79,79,79,79,15,0,0,0,0,0,0,0
db 0,15,80,80,80,80,80,80,80,15,15,15,15,0,0,0,0,0,0,0
db 0,15,81,81,81,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_l  db 20 * 20 / 2 dup ( 0 )


sprdat_n label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,66,66,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,67,67,15,0,0,0,0,0,0,0
db 0,15,68,68,68,15,0,0,0,15,68,68,15,0,0,0,0,0,0,0
db 0,15,69,69,69,69,15,0,0,15,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,15,70,70,15,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,15,71,71,15,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,0,15,72,72,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,15,73,73,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,15,74,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,15,0,0,0,0,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,0,0,0,0,15,79,79,15,0,0,0,0,0,0,0
db 0,15,80,80,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,81,81,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_n  db 20 * 20 / 2 dup ( 0 )

sprdat_i label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,67,67,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,68,68,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,69,69,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,72,72,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,73,73,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,74,74,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,78,78,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,79,79,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,80,80,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_i  db 20 * 20 / 2 dup ( 0 )

sprdat_m label byte
db 0,0,0,0,0,0,0,15,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,15,65,65,65,65,65,15,0,0,0,0,0,0,0
db 0,15,66,66,66,66,66,66,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,0,0,0,15,68,68,15,0,0,0,0,0,0,0
db 0,15,69,69,69,69,15,0,0,15,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,15,70,70,15,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,15,71,71,15,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,0,15,72,72,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,15,73,73,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,15,74,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,15,0,0,0,0,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,80,80,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_m  db 20 * 20 / 2 dup ( 0 )

sprdat_o label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,68,68,68,15,0,0,0,0,0,0,0
db 0,15,15,15,15,69,69,69,69,69,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,70,70,70,70,70,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,71,15,15,15,15,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,0,0,0,0,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,0,0,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,15,0,0,0,0,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,0,0,0,0,15,79,79,15,0,0,0,0,0,0,0
db 0,15,80,80,15,15,15,15,15,80,80,80,15,0,0,0,0,0,0,0
db 0,15,81,81,15,81,81,81,81,81,81,81,15,0,0,0,0,0,0,0
db 0,15,82,82,82,82,82,82,82,15,15,15,15,0,0,0,0,0,0,0
db 0,15,83,83,83,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_o  db 20 * 20 / 2 dup ( 0 )


sprdat_d label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,15,15,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,67,67,67,67,67,67,15,0,0,0,0,0,0,0,0
db 0,15,68,68,68,68,68,68,68,68,68,68,15,0,0,0,0,0,0,0
db 0,15,69,69,69,15,15,15,15,15,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,0,0,0,0,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,0,0,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,15,15,15,15,15,78,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,79,79,79,79,79,79,15,0,0,0,0,0,0,0,0
db 0,15,80,80,80,80,80,80,80,15,15,0,0,0,0,0,0,0,0,0
db 0,15,81,81,81,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_d  db 20 * 20 / 2 dup ( 0 )


sprdat_s label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,66,66,66,15,0,0,0,0,0,0,0
db 0,15,15,15,15,67,67,67,67,67,67,67,15,0,0,0,0,0,0,0
db 0,15,68,68,68,68,68,68,68,15,15,15,15,0,0,0,0,0,0,0
db 0,15,69,69,69,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,71,71,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,72,72,15,15,15,15,15,72,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,73,73,73,73,73,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,74,74,74,74,74,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,75,15,15,15,15,15,75,75,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,78,78,78,15,0,0,0,0,0,0,0
db 0,15,15,15,15,79,79,79,79,79,79,79,15,0,0,0,0,0,0,0
db 0,15,80,80,80,80,80,80,80,15,15,15,15,0,0,0,0,0,0,0
db 0,15,81,81,81,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbmk_s  db 20 * 20 / 2 dup ( 0 )

sprd_co1    label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,12,0,12,0,0,0,0,0,0,0,0,0,0,0
db 0,14,14,14,12,12,14,8,8,0,12,0,14,0,0,0
db 14,14,14,14,14,0,8,12,12,14,8,12,0,0,0,0
db 0,14,14,14,14,8,0,14,12,0,0,0,0,0,0,0
db 0,0,14,0,12,0,12,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprb_co1 db 16 * 08 / 2 dup( 0 )

sprd_co2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,12,0,12,0,0,0,0,0,0,0,0,0,0,0
db 0,14,14,14,12,14,12,8,8,12,14,0,0,0,0,0
db 14,14,14,14,12,0,8,12,12,14,8,12,14,0,0,0
db 0,14,14,14,14,8,0,12,8,0,12,0,0,0,0,0
db 0,0,14,0,12,0,14,0,0,12,0,12,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprb_co2 db 16 * 08 / 2 dup( 0 )


sprda_d2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,15,15,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,0,15,67,67,67,67,67,67,15,15,15,15,0,0,0,0,0,0,0
db 0,15,68,68,68,68,68,68,68,68,68,68,15,0,0,0,0,0,0,0
db 0,15,69,69,15,15,15,15,15,69,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,0,0,0,0,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,0,0,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,78,15,15,15,15,15,78,78,15,0,0,0,0,0,0,0
db 0,0,15,79,79,79,79,79,79,15,79,79,15,0,0,0,0,0,0,0
db 0,0,0,15,15,80,80,80,80,80,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,81,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_d2 db 20 * 20 / 2 dup ( 0 )

sprda_e2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,66,66,66,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,67,67,67,67,67,67,67,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,68,68,68,68,68,68,68,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,69,69,69,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,72,15,15,15,15,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,73,73,73,73,73,73,73,73,15,0,0,0,0,0,0,0
db 0,15,15,15,15,74,74,74,74,74,74,74,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,15,75,75,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,78,15,15,15,15,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,79,79,79,79,79,15,79,79,15,0,0,0,0,0,0,0
db 0,15,15,15,15,80,80,80,80,80,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,81,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_e2 db 20 * 20 / 2 dup ( 0 )


sprda_l2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,68,68,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,69,69,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,72,72,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,73,73,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,78,15,15,15,15,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,79,79,79,79,79,15,79,79,15,0,0,0,0,0,0,0
db 0,15,15,15,15,80,80,80,80,80,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,81,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_l2 db 20 * 20 /2 dup (0)


sprda_n2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,66,66,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,67,67,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,68,68,15,0,0,0,15,68,68,68,15,0,0,0,0,0,0,0
db 0,15,69,69,15,0,0,15,69,69,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,15,0,15,70,70,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,15,71,71,15,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,72,72,15,0,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,73,73,15,0,0,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,74,15,0,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,15,0,0,0,0,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,0,0,0,0,15,79,79,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_n2 db 20 * 20 / 2 dup ( 0 )


sprda_o2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,66,66,66,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,67,67,67,67,67,67,67,15,15,15,15,0,0,0,0,0,0,0
db 0,15,68,68,15,68,68,68,68,68,68,68,15,0,0,0,0,0,0,0
db 0,15,69,69,15,15,15,15,15,69,69,69,15,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,0,0,0,0,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,0,0,0,0,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,0,0,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,0,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,78,15,15,15,15,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,79,79,79,79,79,15,79,79,15,0,0,0,0,0,0,0
db 0,15,15,15,15,80,80,80,80,80,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,81,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_o2 db 20 * 20 / 2 dup ( 0 )


sprda_s2 label byte
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,66,66,66,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,67,67,67,67,67,67,67,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,68,68,68,68,68,68,68,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,69,69,69,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,70,70,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,72,15,15,15,15,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,73,73,73,73,73,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,15,74,74,74,74,74,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,15,15,15,15,15,75,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,78,78,78,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0
db 0,15,79,79,79,79,79,79,79,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,80,80,80,80,80,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,15,15,15,15,81,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_s2 db 20 * 20 / 2 dup ( 0 )

sprda_m2 label byte
db 0,15,15,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,65,65,65,65,65,15,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,15,66,66,66,66,66,66,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,15,15,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
db 0,15,69,69,15,0,0,0,15,15,15,15,15,0,0,0,0,0,0,0
db 0,15,70,70,15,0,0,15,70,70,70,70,15,0,0,0,0,0,0,0
db 0,15,71,71,15,0,15,71,71,15,71,71,15,0,0,0,0,0,0,0
db 0,15,72,72,15,15,72,72,15,15,72,72,15,0,0,0,0,0,0,0
db 0,15,73,73,15,73,73,15,0,15,73,73,15,0,0,0,0,0,0,0
db 0,15,74,74,74,74,15,0,0,15,74,74,15,0,0,0,0,0,0,0
db 0,15,75,75,75,15,0,0,0,15,75,75,15,0,0,0,0,0,0,0
db 0,15,76,76,15,0,0,0,0,15,76,76,15,0,0,0,0,0,0,0
db 0,15,77,77,15,0,0,0,0,15,77,77,15,0,0,0,0,0,0,0
db 0,15,78,78,15,0,0,0,0,15,78,78,15,0,0,0,0,0,0,0
db 0,15,79,79,15,0,0,0,0,15,79,79,15,0,0,0,0,0,0,0
db 0,15,15,15,15,0,0,0,0,15,80,80,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,81,81,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,15,15,15,15,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
sprbm_m2 db 20 * 20 / 2 dup ( 0 )

star1 label byte
db 0,0,0,90,0,0,0,0,0,0,0,0
db 0,0,0,90,0,0,0,0,0,0,0,0
db 0,0,0,90,0,0,0,0,0,0,0,0
db 0,0,90,90,90,0,0,0,0,0,0,0
db 90,90,90,90,90,90,90,0,0,0,0,0
db 0,0,90,90,90,0,0,0,0,0,0,0
db 0,0,0,90,0,0,0,0,0,0,0,0
db 0,0,0,90,0,0,0,0,0,0,0,0
db 0,0,0,90,0,0,0,0,0,0,0,0
db 0,0,0,0,0,0,0,0,0,0,0,0

star2 label byte
db  00,00
db  00,91

star3 label byte
db  00,00,92,00,00,0,0,0
db  00,00,92,00,00,0,0,0
db  00,92,92,92,00,0,0,0
db  00,00,92,00,00,0,0,0
db  00,00,92,00,00,0,0,0
db  00,00,00,00,00,0,0,0
star3bm db 8 * 6 / 2 dup ( 0 )

star4 label byte
db  00,00
db  00,93

star5 label byte
db  00,00
db  00,94

db  0,0,0

tapa   label byte
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
db 24 dup ( 255 )
tapabm  db 24 * 20 /2 dup( 0 )
