P286

;*********************************************************************;
;*                        V 3 2 2 0 C A . A S M                      *;
;*-------------------------------------------------------------------*;
;*    Task           : Contains routines for operating in            *;
;*                     320x200 256 color mode on a VGA card.         *;
;*-------------------------------------------------------------------*;
;*    Author         : Michael Tischer                               *;
;*    Developed on   : 09/05/90                                      *;
;*    Last update    : 02/13/92                                      *;
;*********************************************************************;

;== Constants =========================================================

SC_INDEX       = 3c4h             ;Index register for sequencer ctrl.
SC_MAP_MASK    = 2                ;Number of map mask register
SC_MEM_MODE    = 4                ;Number of memory mode register

GC_INDEX       = 3ceh             ;Index register for graphics ctrl.
GC_READ_MAP    = 4                ;Number of read map register
GC_GRAPH_MODE  = 5                ;Number of graphics mode register
GC_MISCELL     = 6                ;Number of miscellaneous register

CRTC_INDEX     = 3d4h             ;Index register for CRT controller
CC_MAX_SCAN    = 9                ;Number of maximum scan line reg.
CC_START_HI    = 0Ch              ;Number of high start register
CC_UNDERLINE   = 14h              ;Number of underline register
CC_MODE_CTRL   = 17h              ;Number of mode control register

DAC_WRITE_ADR  = 3C8h             ;DAC write address
DAC_READ_ADR   = 3C7h             ;DAC read address
DAC_DATA       = 3C9h             ;DAC data register

VERT_RESCAN    = 3DAh             ;Input status register #1

PIXX           = 320              ;Horizontal resolution


vio_seg    dw 0a000h              ;Video segment with current page

x0         dw ?                   ;X-coordinate
y0         dw ?                   ;Y-coordinate
pcolor     dw ?                   ;Color

init320200 proc near

	   ;-- Sets mode 13H, since BIOS uses this mostly for
	   ;-- initialization.

       mov   ax,0013h         ;Set normal mode 13H
	   int   10h

	   mov   dx,GC_INDEX      ;Memory division
	   mov   al,GC_GRAPH_MODE ;Disable bit 4 of
	   out   dx,al            ;graphics mode register in
	   inc   dx               ;graphics controller
	   in    al,dx
	   and   al,11101111b
	   out   dx,al
	   dec   dx

	   mov   al,GC_MISCELL    ;And change bit 1
	   out   dx,al            ;in the miscellaneous
	   inc   dx               ;register
	   in    al,dx
	   and   al,11111101b
	   out   dx,al

	   mov   dx,SC_INDEX      ;Modify memory mode register in
	   mov   al,SC_MEM_MODE   ;sequencer controller so no further
	   out   dx,al            ;address division follows in
	   inc   dx               ;bitplanes, and set the bitplane
	   in    al,dx            ;currently in the
	   and   al,11110111b     ;bit mask register
	   or    al,4
	   out   dx,al

	   mov   ax,vio_seg       ;Fill all four bitplanes with color
	   mov   es,ax            ;code 00H and clear the screen
	   xor   di,di
	   mov   ax,di
	   mov   cx,8000h
	   rep   stosw

	   mov   dx,CRTC_INDEX    ;Set double word mode using bit 6
	   mov   al,CC_UNDERLINE  ;in underline register of
	   out   dx,al            ;CRT controller
	   inc   dx
	   in    al,dx
	   and   al,10111111b
	   out   dx,al
	   dec   dx

	   mov   al,CC_MODE_CTRL  ;Using bit 6 in mode control reg.
	   out   dx,al            ;of CRT controller, change
	   inc   dx               ;from word mode to byte mode
	   in    al,dx
	   or    al,01000000b
	   out   dx,al

	   ret                    ;Return to caller

init320200 endp                  ;End of procedure

;-- SETPIX: Changes a pixel to a specific color -----------------------
;In:    ax = x
;       bx = y
;       cx = color
;
;Out:   none
;----------------------------------------------------------------------

setpix    proc near
       pusha                  ; push all
       push es
       push ds

       mov x0,ax
       mov y0,bx
       mov pcolor,cx

	   mov   ax,PIXX / 4      ;Compute offset in video RAM
       mul   y0               ;and pass to DI
       mov   cx,x0
	   mov   bx,cx
	   shr   bx,1
	   shr   bx,1
	   add   ax,bx
	   mov   di,ax

	   and   cl,3             ;Compute bit mask for map to be
	   mov   ah,1             ;addressed, move to AH
	   shl   ah,cl
	   mov   al,SC_MAP_MASK   ;Register number to AL
	   mov   dx,SC_INDEX      ;Load sequencer index address
	   out   dx,ax            ;Load bit mask register

	   mov   ax,vio_seg       ;Set ES to video RAM
	   mov   es,ax
       mov   al,byte ptr pcolor ;Load pixel color and
	   stosb                  ;write to selected bitmap

       pop ds
       pop es
       popa                   ;pop all
	   ret                    ;Return to caller
setpix    endp                   ;End of procedure

;-- GETPIX: Returns a pixel color -------------------------------------
;In:    ax = x
;       bx = y
;
;Out:   Pcolor = color
;----------------------------------------------------------------------
getpix    proc near
       pusha
       push ds
       push es

       push cs
       pop ds

       mov x0,ax
       mov y0,bx

	   mov   ax,PIXX / 4      ;Compute offset in video RAM
       mul   y0               ;and pass to SI
       mov   si,x0
	   mov   cx,si
	   shr   si,1
	   shr   si,1
	   add   si,ax

	   and   cl,3             ;Compute bit mask for map to be
	   mov   ah,cl            ;addressed, move to AH
	   mov   al,GC_READ_MAP   ;Register number to AL
	   mov   dx,GC_INDEX      ;Load graphics controller index
	   out   dx,ax            ;Load read map register

	   mov   ax,vio_seg       ;Set ES to video RAM
	   mov   es,ax
       mov   cl,es:[si]       ;Load pixel color

       xor ch,ch
       mov pcolor,cx          ;return value.

       pop es
       pop ds
       popa
       ret                    ;Return to caller
getpix    endp                ;End of procedure


;-- SETPAGE: Sets page for access from setpix and getpix --------------
;-- Declaration :
;   In:     Cl  -> page
;  Out:     None
;----------------------------------------------------------------------
setpage   proc near
       pusha
       push ds
       push cs
       pop ds

       mov   al,4             ;High byte seg. addr = page * 4 + A0H
	   mul   cl
	   or    al,0A0h
	   mov   byte ptr vio_seg + 1,al ;Move new segment address

       pop ds
       popa
	   ret                    ;Return to caller
setpage   endp                   ;End of procedure

;-- SHOWPAGE: Display one of the two screen pages ---------------------
;-- Declaration :
;       In: Cl  -> page
;      Out: None
;----------------------------------------------------------------------
showpage  proc near
       pusha

       mov   al,64            ;High byte of offset = page * 64
	   mul   cl
	   mov   ah,al            ;Move high byte of offset to AH

	   ;-- Load new starting address ------------------------------

	   mov   dx,CRTC_INDEX    ;Address CRT controller
	   mov   al,CC_START_HI   ;Move register number
	   out   dx,ax            ;to AL and exit

	   ;-- Wait to return to starting screen design ---------------

       mov   dx,VERT_RESCAN   ;Wait for end of
sp3:   in    al,dx            ;vertical rescan
       test  al,8
       jne   sp3

sp4:   in    al,dx            ;Go to start of rescan
       test  al,8
       je    sp4

       popa
       ret                    ;Return to caller
showpage  endp                   ;End of procedure

;-----------------------------------------------------------------------------;
;                                                                             ;
;                        Funciones para manejar Graficos                      ;
;  Todas fueron hechas por Ricardo Quesada salvo las que digan lo contrario   ;
;                                                                             ;
;-----------------------------------------------------------------------------;

;---------------------------;
;		VerticalR			;
;---------------------------;
verticalr proc near
    pusha

	mov   dx,VERT_RESCAN   ;Wait for end of
ula1:
	in	  al,dx 		   ;vertical rescan
	test  al,8
	jne   ula1
ula2:
	in	  al,dx 		   ;Go to start of rescan
	test  al,8
	je	  ula2

    popa
	ret
verticalr endp

;---------------------------;
;	displayletras			;
;---------------------------;
;	In: 					;
;		ax	= x 			;
;		bx	= y 			;
;       cx  = Color         ;
;       dx  = Inc del color ;
;       di  = Transparencia ;
;       si  = Offset asciiz ;
;---------------------------;
;displayletras proc near
;    pusha
;    push ds
;
;    push cs
;    pop ds
;
;    mov x0,ax
;    mov y0,bx
;    mov Charcolor,cx
;    mov varcolor,dx
;    mov transpar,di
;
;loopdl3:
;    mov  bp,offset tabla
;
;    xor  ah,ah
;    mov  al,[si]
;    or   al,al
;    jne  nodl1
;
;    pop ds
;    popa
;    ret
;
;nodl1:
;    mov  cl,4
;    shl  ax,cl                      ;multiplica por 8
;    add  bp,ax                      ;puntero de la letra.
;
;    mov di,16
;loopdl2:
;    mov cx,8
;    mov dl,[bp]
;
;loopdl1:
;    push cx
;    shl dl,1
;    jc spix
;
;    cmp transpar,1
;    je  transparente
;    mov cl,0                ;color de fondo.
;    jmp short cpix
;spix:
;    xor cx,cx
;    cmp varcolor,0          ; Compara
;    je  dlet1color          ; Var de color � no ? then no change
;    mov cx,x0
;    add cx,y0
;    and cx,varcolor         ; Recomendabel 6 para buen efecto.
;dlet1color:
;    add cx,CharColor        ; algoritmo para poner el color
;cpix:
;    mov ax,x0               ; x
;    mov bx,y0               ; y
;    call setpix
;
;transparente:
;    inc x0
;    pop cx
;    loop loopdl1
;    sub x0,8
;    inc y0
;    inc bp
;    dec di
;    or di,di
;    jne loopdl2
;    inc si
;    sub y0,16
;    add x0,8
;    jmp loopdl3
;
;CharColor   dw 0    ; Usada para el Color de los textos (fn interna del Displayletras)
;VarColor    dw 0    ; Usada para la variaci�n del color de los chars ( " " " " )
;Transpar    dw 0    ; Transparencia. =0 NO  =1 SI
;
;displayletras endp
