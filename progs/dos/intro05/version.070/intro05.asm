;-----------------------------------------------;
;												;
;					Intro 05					;
;			El Ni¤o del Sol (c) 1994.			;
;												;
;-----------------------------------------------;
;		Segunda intro en modo gr fico			;
;		  Hecha para probar sprites.			;
;-----------------------------------------------;
P286

code	segment public byte 'code'
org 	100h
intro05 PROC
assume cs:code,ds:code,ss:code,es:code

VERT_RESCAN    = 3DAh			  ;Input status register #1
TOTSPRITES     = 29               ; Total de sprites reales.
DIFFSPR        = 17               ; Diferencia de sprites ( E  y E alreves )

;---------------------------;
start:
;---------------------------;
    call releasemem
    call is286
	call isvga
    call issvga
    call init320200
	call mainprogram

	mov ax,3
	int 10h 			;volver al modo texto.

    mov ax,1301h        ; function and subfunction
    mov bx,0030h        ; page and color
    mov cx,fin2msg - fin1msg    ; length
    xor dx,dx           ; position
    lea bp,fin1msg
    int 10h

	mov ax,4c00h
	int 21h

;---------------------------;
;        releasemem         ;
;---------------------------;
releasemem  proc near
    mov ah,4ah
    mov bx,offset final
    add bx,15
    shr bx,4
    inc bx
    int 21h                 ;*** Change memory size ***
    ret
releasemem endp

;---------------------------;
;		Is286				;
;---------------------------;
is286 proc near
	xor ax,ax
	push ax
	popf
	pushf
	pop ax
	and ax,0f000h
	cmp ax,0f000h
	je not286
	ret
not286:
	mov dx,offset no286msg
	mov ah,9
	int 21h
	mov ax,4cffh
	int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc near
		mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp

;---------------------------;
;       issvga              ;
;---------------------------;
issvga proc near
        mov ah,48h              ; allocate memory
        mov bx,256 / 16         ; 256 bytes
        int 21h
        push ax                 ; push segment !

        mov ax,4f00h
        xor di,di
        pop es                  ; pop segment
        int 10h                 ; Is SVGA ( en verdad cheque si soporta VESA )
        or ah,ah
        je itissvga

        mov ah,9
        lea dx,svgamsg
        int 21h                 ; Display not SVGA.

svgahere1:
        mov ah,8
        int 21h                 ; Get Char.
        and al,not 20h
        cmp al,'N'
        je svgahere2
        cmp al,'Y'
        jne svgahere1

itissvga:
        mov ah,49h
        int 21h                 ; Free memory
		ret
svgahere2:
        mov ah,49h
        int 21h                 ; Free memory
        mov ax,4cffh
        int 21h
issvga  endp

;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near

    mov al,0ffh
    out 21h,al

    mov ax,cs
    mov ds,ax
    mov es,ax


    mov dx,3c8h
    mov al,255
    out dx,al
    mov al,0
    inc dx
    out dx,al
    out dx,al
    out dx,al                       ; color 255 = 0


    mov  cl,3
	call showpage

    call stars              ; dibujar estrellas.

    mov cl,2
    call showpage

	mov cl,0
contcreat:
	call createspr
	inc cl
    cmp cl,TOTSPRITES        ; Crear todos los sprites.
	jne contcreat

    mov cx,14
    mov ax,300
    mov bx,180
    call posspr

mainhere1:
    call changepallete2
    call changepallete
    call movestars2
    call movestars
    call bouncesprites
    mov  cx,16                      ; sprites a mover ( + 1 )
	call movesprites				;
    call getkey
	jne  mainhere1

    mov  cx,63 * 8
fadingout:
    push cx
    call movestars
    call movestars2
    call bouncesprites
    mov  cx,16
    call movesprites
    call fadeout
    pop  cx
    loop fadingout

    mov al,0h
    out 21h,al

    ret
mainprogram endp

;---------------------------;
;		 Stars				;
;---------------------------;
stars proc near

    mov ax,cs
    mov ds,ax

    mov di,offset estrellas

starmainloop:
    cmp [ di ],0ffffh
    je  changesi
    cmp word ptr[ di ],0
    jne starhere3
    jmp starend
changesi:
    add di,2
    mov dx,[ di ]
    mov staroff,dx
    add di,2                    ; new si.
    mov dx,[ di ]               ; ancho
    mov starxx,dx
    add di,2
    mov dx,[ di ]               ; largo
    mov staryy,dx
    add di,2

starhere3:
    mov si,staroff
    mov bx,[ di + 2]            ; y
    mov stary,0
starhere2:
    mov ax,[ di ]               ; x
	mov starx,0
starhere1:
	mov cx,[si]

    push cx
    mov  cx,0
    call setpage
    pop  cx
    call setpix

    push cx
    mov  cx,1
    call setpage
    pop  cx
    call setpix

    inc si
	inc ax
	inc starx
    mov dx,starx
    cmp dx,starxx
	jb starhere1
	inc bx
	inc stary
    mov dx,stary
    cmp dx,staryy
	jb	starhere2

    add di,4
    jmp starmainloop

starend:
	ret
stars endp

starx   dw 0
stary   dw 0

estrellas   dw 0ffffh,offset star1,12,10
            dw 50,50,    250,160
            dw 0ffffh,offset star2,2,2
            dw  20,20,  10,140,     298,21,    123,154,    200,158,    58,100
            dw  0ffffh,offset star3,8,5
            dw  20,170, 220,20
            dw 0ffffh,offset star4,2,2
            dw  5,90,   142,48,      290,142,    200,50,      80,190,     64,124
            dw 0ffffh,offset star5,2,2
            dw  45,90,  42,148,     90,142,    20,50,      150,110,     214,134
            dw  0,0

staroff   dw ?
starxx    dw ?
staryy    dw ?

;---------------------------;
;	  ChangePallete 		;
;---------------------------;
;	  Cambiar la paleta 	;
;---------------------------;
changepallete proc near
	inc daccount3
	cmp daccount3,1
	jne  dachere3

	mov daccount3,0

	mov al,64
	mov dx,3c8h
	out dx,al
	inc dx
	mov cx,20 * 3
	mov si,offset colores
	mov bx,daccount1
dacloop:
	mov al,[si+bx]
	out dx,al
	inc bx
	loop dacloop

	mov bx,daccount1
	add bx,3
    cmp bx,245 * 3
	jb	dachere1
	xor bx,bx
dachere1:
	mov daccount1,bx

	mov al,15
	mov dx,3c8h
	out dx,al
	inc dx
	mov cx,3
	mov si,offset colores
	mov bx,daccount2
dacloop2:
	mov al,[si+bx]
	out dx,al
	inc bx
	loop dacloop2

	mov bx,daccount2
	add bx,3
    cmp bx,245 * 3
	jb	dachere4
	xor bx,bx
dachere4:
	mov daccount2,bx

dachere3:
	ret
changepallete endp

daccount1 dw 0
daccount2 dw 120 * 3
daccount3 db 0

;---------------------------;
;     ChangePallete2        ;
;---------------------------;
;	  Cambiar la paleta 	;
;---------------------------;
changepallete2 proc near

    mov al,90
	mov dx,3c8h
	out dx,al
	inc dx
    mov si,offset graydac
    mov cx,3
    mov bx,graycount1
grayloop1:
    mov al,[ si + bx ]
	out dx,al
	inc bx
    loop grayloop1

    mov cx,3
    mov bx,graycount2
grayloop2:
    mov al,[ si + bx ]
	out dx,al
	inc bx
    loop grayloop2

    mov cx,3
    mov bx,graycount3
grayloop3:
    mov al,[ si + bx ]
	out dx,al
	inc bx
    loop grayloop3

    mov cx,3
    mov bx,graycount4
grayloop4:
    mov al,[ si + bx ]
	out dx,al
	inc bx
    loop grayloop4

    mov cx,3
    mov bx,graycount5
grayloop5:
    mov al,[ si + bx ]
	out dx,al
	inc bx
    loop grayloop5



    mov bx,graycount1
	add bx,3
	cmp bx,245 * 3
    jb  grayhere1
	xor bx,bx
grayhere1:
    mov graycount1,bx

    mov bx,graycount2
	add bx,3
	cmp bx,245 * 3
    jb  grayhere2
	xor bx,bx
grayhere2:
    mov graycount2,bx

    mov bx,graycount3
	add bx,3
	cmp bx,245 * 3
    jb  grayhere3
	xor bx,bx
grayhere3:
    mov graycount3,bx

    mov bx,graycount4
	add bx,3
	cmp bx,245 * 3
    jb  grayhere4
	xor bx,bx
grayhere4:
    mov graycount4,bx

    mov bx,graycount5
	add bx,3
	cmp bx,245 * 3
    jb  grayhere5
	xor bx,bx
grayhere5:
    mov graycount5,bx

    ret

    graycount1  dw 0
    graycount2  dw 120 * 3
    graycount3  dw 80  * 3
    graycount4  dw 160 * 3
    graycount5  dw 200 * 3

changepallete2 endp

;---------------------------;
;	  BounceSprites 		;
;---------------------------;
;  Hace mover a los sprites ;
;---------------------------;
bouncesprites proc near

	mov cx,0
submoverpelotas:

	push cx
	shl  cx,1
	mov  di,offset spritespings
	add  di,cx
	mov  si,[ di ]

	mov  ax,[ si.sping_orgx ]
	mov  bx,[ si.sping_orgy ]

	pop  cx
	push cx
	call posspr 					; pocisiona Sprite.

	call movex
	call movey

	pop  cx
	inc  cx
	cmp  cx,12						; TOTAL DE SPRITES A MOVER
	jne  submoverpelotas

	ret
bouncesprites endp

;---------------------------;
;		  movex 			;
;---------------------------;
; fnc int del BounceSprites ;
;---------------------------;
movex proc near
	pusha
	mov  ax,[ si.sping_orgx ]		; pos x
	add  ax,[ si.sping_incx ]		; incremento x
	cmp  ax,300
	ja	 notmx1
	cmp  ax,1
	jb	 notmx1
	jmp  short nonotmx1
notmx1:
	not  word ptr [ si.sping_incx ] ; incremento x
	inc  word ptr [ si.sping_incx ] ; incremento x

	mov  dx, [ si.sping_imag ]		; imagen del sprite
    cmp  dx,DIFFSPR
	jb	 movexhere1

    sub dx,DIFFSPR
	mov cx,dx
	jmp movexhere2

movexhere1:
	mov  cx,dx
    add  dx,DIFFSPR

movexhere2:
	mov  [ si.sping_imag ],dx
	call sprimage					; change sprite image

nonotmx1:
	mov  [ si.sping_orgx ],ax		; pos x
	popa
	ret
movex endp

;---------------------------;
;		  movey 			;
;---------------------------;
; fnc int del BounceSprites ;
;---------------------------;
movey proc near
	push ax
	push cx
	inc  word ptr [ si.sping_incy ] ; incremey
	mov  ax,[ si.sping_incy ]		; incremey
	cmp  ax,80						; m ximo que puede subir.
	je	 notmy1
	cmp  ax,0
	je	 notmy1
	jmp  short nonotmy1

notmy1:
	not  word ptr [ si.sping_incy ] ; incremey
	inc  word ptr [ si.sping_incy ] ; incremey
nonotmy1:

	mov  cx,[ si.sping_incy ]		; incremey
	cmp  ch,0
	jne  heremy2

	shr  ax,4						; Ancho de salto.

	jmp  short endmy1
heremy2:
	dec  ax
	not  ax
	inc  ax
	shr  ax,4						; Ancho de salto.
	not  ax
	inc  ax
endmy1:
	add  [ si.sping_orgy ],ax		   ; pos y
	pop  cx
	pop  ax
	ret
movey endp

;---------------------------;
;        MoveStars          ;
;---------------------------;
;  Hace mover a las stars   ;
;---------------------------;
movestars proc near
    dec mstar
    cmp mstar,0
    je  mstarhere1
    ret

mstarhere1:
    mov mstar,3

    dec mstarcnt1                   ; posx
    cmp mstarcnt1,1
    ja  mstarhere2

    mov mstarcnt1,300
    mov mstar,1000                  ; main delay

mstarhere2:
    inc mstarcnt2
    cmp mstarcnt2,5
    jne mstarhere3

    mov mstarcnt2,0
    mov cx,14
    xor mstarimage,14 xor 15
    mov dx,mstarimage
    call sprimage

mstarhere3:
    mov  cx,14
    mov  bx,180
    mov  ax,mstarcnt1
	call posspr 					; change x position
	ret
movestars endp

mstar       dw  1000                    ; contador delmain delay.
mstarimage  dw  14
mstarcnt1   dw  300
mstarcnt2   dw  0

;---------------------------;
;        MoveStars2         ;
;---------------------------;
;  Hace mover a las stars   ;
;---------------------------;
movestars2 proc near
    dec mstar2
    cmp mstar2,0
    je  mstarhere12
    ret

mstarhere12:
    mov mstar2,6

    mov ax,mstarcnt12
    cmp ax,50
    jb  mstar2xor
    cmp ax,170
    jb  mstar2here1

mstar2xor:
    not mstarx
    inc mstarx
    mov mstar2,500                  ; delay
    mov ax,mstarcnt12
    add ax,mstarx
    mov mstarcnt12,ax
    ret

mstar2here1:
    mov ax,mstarcnt12
    add ax,mstarx
    mov mstarcnt12,ax
    mov  cx,16                      ; number of spr
    mov  bx,120                     ; y
	call posspr 					; change x position
	ret
movestars2 endp

mstar2       dw  800                     ; contador delmain delay.
mstarcnt12   dw  128
mstarx       dw  1

;---------------------------;
;		getkey				;
;---------------------------;
getkey proc near
	push ax
;	 mov  ah,1
;	 int  16h

	in	al,060h
	cmp al,01h
	pop ax
	ret
getkey endp

;---------------------------;
;       fadeout             ;
;---------------------------;
fadeout proc near
    mov ax,dacntx               ; READ DAC
    mov dx,3c7h
    out dx,al                   ; DAC to be read

    mov dx,3c9h                 ; Read DAC from port
    mov di,offset coloresdac
    mov cx,16 * 3
rep insb

    mov di,offset coloresdac
    mov cx,16 * 3
fadeloop1:
    mov al,[di]
    cmp al,1
    jbe fadehere1
    sub al,2
    mov [di],al
fadehere1:
    inc di
    loop fadeloop1              ; Decrementa los colores

    mov ax,dacntx               ; WRITE DAC
    mov dx,3c8h
    out dx,al                   ; DAC to be write
    mov dx,3c9h                 ; Read DAC from port
    mov si,offset coloresdac
    mov cx,16 * 3
rep outsb

    mov ax,dacntx
    add ax,16
    cmp ax,256
    jbe fadeend

    xor ax,ax
fadeend:
    mov dacntx,ax
    ret
fadeout endp

dacntx      dw 0
coloresdac  db 16 * 3 dup( 0 )

;---------------------------;
;		Estructuras 		;
;---------------------------;

sprstruc struc
	sp_orgx dw	?				; pos original x
	sp_orgy dw	?				; pos original y
	sp_nowx dw	?				; ubicaci¢n de ahora x
	sp_nowy dw	?				; ubicaci¢n de ahroa y
	sp_page db	?				; p gina donde esta
	sp_tamx dw	?				; tama¤o x
	sp_tamy dw	?				; tama¤o y
	sp_dato dw	?				; datos del sprite 1
	sp_mask dw	?				; datos de la m scara.
	sp_oldx dw	?				; ubicaci¢n old de x
	sp_oldy dw	?				; ubicaci¢n old de y
	sp_visi db	?				; sprite visible al crearse ?
	sp_offs db	?				; offset del sprite (0-3)
	sp_orgxx dw ?				; Copias de orgx
	sp_orgyy dw ?				;			orgy
	sp_tamxx dw ?				;			tamx
	sp_tamyy dw ?				;			tamy
	sp_maskk dw ?				;			mask
sprstruc ends

sprping struc
	sping_orgx	dw ?			; Coordenadas originales	X
	sping_orgy	dw ?			;							Y
	sping_incx	dw ?			; Incremento de X
	sping_incy	dw ?			;				Y
	sping_imag	dw ?			; Numero de imagen ( SprImage )
sprping ends

spritespings	label word
	dw offset sprbounc1
	dw offset sprbounc2
	dw offset sprbounc3
	dw offset sprbounc4
	dw offset sprbounc5
	dw offset sprbounc6
	dw offset sprbounc7
	dw offset sprbounc8
	dw offset sprbounc9
	dw offset sprbounc10
	dw offset sprbounc11
	dw offset sprbounc12


sprbounc1	label word
		dw 10			; + 0	 ( coordenas iniciales )
		dw 180			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw -80			; + 8	 ( valores negativos suben )
		dw 0			; + 10	 ( SprImage )

sprbounc2	label word
		dw 20			; + 0	 ( coordenas iniciales )
		dw 8bh			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 0ffbah		; + 8	 ( valores negativos suben )
		dw 1			; + 10	 ( SprImage )

sprbounc3	label word
		dw 40			; + 0	 ( coordenas iniciales )
		dw 48h			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 0ffceh		; + 8	 ( valores negativos suben )
		dw 2			; + 10	 ( SprImage )

sprbounc4	label word
		dw 50			; + 0	 ( coordenas iniciales )
		dw 31h			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 0ffd8h		; + 8	 ( valores negativos suben )
		dw 3			; + 10	 ( SprImage )


sprbounc5	label word
		dw 60			; + 0	 ( coordenas iniciales )
		dw 1eh			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 0ffe2h		; + 8	 ( valores negativos suben )
		dw 4			; + 10	 ( SprImage )


sprbounc6	label word
		dw 70			; + 0	 ( coordenas iniciales )
		dw 14h			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 0ffech		; + 8	 ( valores negativos suben )
		dw 5			; + 10	 ( SprImage )


sprbounc7	label word
		dw 90			; + 0	 ( coordenas iniciales )
		dw 00fh 		; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 00000h		; + 8	 ( valores negativos suben )
		dw 6			; + 10	 ( SprImage )


sprbounc8	label word
		dw 100			; + 0	 ( coordenas iniciales )
		dw 0fh			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 0000ah		; + 8	 ( valores negativos suben )
		dw 7			; + 10	 ( SprImage )

sprbounc9	label word
		dw 110			; + 0	 ( coordenas iniciales )
		dw 014h 		; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 00014h		; + 8	 ( valores negativos suben )
		dw 8			; + 10	 ( SprImage )


sprbounc10	label word
		dw 130			; + 0	 ( coordenas iniciales )
		dw 31h			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 00028h		; + 8	 ( valores negativos suben )
		dw 9			; + 10	 ( SprImage )


sprbounc11	label word
		dw 140			; + 0	 ( coordenas iniciales )
		dw 048h 		; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 32h			; + 8	 ( valores negativos suben )
		dw 10			; + 10	 ( SprImage )


sprbounc12	label word
		dw 150			; + 0	 ( coordenas iniciales )
		dw 66h			; + 2
		dw 1			; + 6	 ( 1=derecha -1=izquierda )
		dw 3ch			; + 8	 ( valores negativos suben )
		dw 11			; + 10	 ( SprImage )

vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'
svgamsg  db 13,10,13,10,7,'Your video card is not fast enough !',13,10,'Continue anyway (Y/N) ? $'

fin1msg label byte
db'                                INTRO NUMERO 5                                  '
db'                              by El ni¤o del sol                                '
db'ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ'
db'       Toda esta intro fue compilada con Tasm 3.1 y linkeada con Tlink 5.1 ,    '
db'          los sprites fueron creados con MicroGraphs 3.5 de Sergio Lerner ,     '
db'            ... y las funciones de los sprites fueron creadas por m¡ !          '
db'         ( inspiraci¢n divina, en la Commodore 64 y librerias de M.Tisher )     '
db'ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ'
db'  Saludos: A la gente de ADN, a Archon y a todos los creadores de la misma, a   '
db'     The Last Hackers por sus espectaculares intros, al Toro [ patrocinador     '
db'         oficial del VChar :-) ], a los distintos grupos que hacen demos,       '
db'                             intros y otras maravillas                          '
db'ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ'
db'                                                   14 - 28 de Diciembre de 1994.'


fin2msg equ this byte

;---------------------------;
;		Includes			;
;---------------------------;
include 	320200.inc					; Incluye funciones para manejo de pixels.
include 	sprites.inc 				; Incluye las funciones de los sprites.
include 	sprdat.inc					; Datos de los sprites.
include     gray256.inc                 ; Tabla de grises.
include     fire.inc                    ; Tabla de colores.
include     colores2.inc                ; Tabla de colores originales.

final  equ this byte

intro05 endp
code	ends
		end intro05
