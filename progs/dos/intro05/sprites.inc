;-----------------------------------------------------------------------------;
;                                                                             ;
;                        Funciones para manejar Sprites                       ;
;  Todas fueron hechas por Ricardo Quesada salvo las que digan lo contrario   ;
;                                                                             ;
;-----------------------------------------------------------------------------;

p286

;---------------------------;
;		CreateSpr			;
;---------------------------;
;	 Create a sprite		;
;---------------------------;
;	In: 					;
;		cx	= N of sprite	;
;---------------------------;
createspr proc near
	pusha
	push ds

	push cx
	mov cl,3
	call setpage
	pop  cx 					; Todos los sprites se crean en p�gina 3

	push cs
	pop ds

	shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

	mov di,[ si + 13]			; Donde estan los datos!

	mov ax,[ si + 9 ]			; Max x del sprite
	mov bx,[ si + 11]			; Max y del sprite
	mov cx,[ si + 0 ]			; Pos original del Sprite x
	mov dx,[ si + 2 ]			; Pos original del Sprite y
	mov sprmaxx,ax				;
	mov sprmaxy,bx				; Mueve todos esos datos
	mov sprx,cx 				; a varibles internas de la
	mov spry,dx 				; funci�n

	xor bx,bx	;		y
showloop2:
	xor ax,ax	;		x
showloop1:
	mov cx,[di] 				; Puntero de donde estan los datos.

	push ax
	push bx

	add  bx,spry
	add  ax,sprx
	call setpix

	pop bx
	pop ax

	inc di
	inc ax

	cmp ax,sprmaxx
	jb	showloop1
	inc bx
	cmp bx,sprmaxy
	jb	showloop2


;---------------------------; Crear m�scara.

	push cs
	pop ds

	mov di,[ si + 15 ]			; Offset de la m�scara.
	mov ax,[ si + 9 ]			; Max x del sprite
	mov bx,[ si + 11]			; Max y del sprite
	mov cx,[ si + 0 ]			; Pos original del Sprite x
	mov dx,[ si + 2 ]			; Pos original del Sprite y
	add ax,cx
	mov sprmaxx,ax				; Lo m�ximo
	add bx,dx
	mov sprmaxy,bx				; Mueve todos esos datos
	mov sprx,cx 				; a varibles internas de la
	mov spry,dx 				; funci�n


mskmainloop:
	mov dl,[di] 				; byte de la m�scara.
	mov cx,8
mskloop1:
	mov  ax,sprx
	mov  bx,spry
	call getpix

	push ax

	inc  sprx
	mov  ax,sprx
	cmp  ax,sprmaxx
	jb	 mskhere1

	mov  ax,[ si + 0 ]			; pone en x la pos original
	mov  sprx,ax				;
	inc  spry
	mov  ax,spry
	cmp  ax,sprmaxy
	jb	 mskhere1

	pop  ax 					; acabo la msk
	jmp  mskfin

mskhere1:
	pop  ax

	shr  dl,1

	cmp  byte ptr pcolor,0
	je	 transparent
	or	 dl,80h
transparent:
	loop mskloop1
	mov  [di],dl
	inc  di
	jmp  mskmainloop

mskfin:
	pop ds
	popa
	ret

sprmaxx dw  ?
sprmaxy dw	?
sprx	dw	?
spry	dw	?

createspr endp

;---------------------------;
;		PutSpr				;
;---------------------------;
;	Put sprite n in (x,y)	;
;---------------------------;
;	In: 					;
;		ax	= x 			;
;		bx	= y 			;
;		cx	= Num of Sprite ;
;---------------------------;
putspr proc near
	pusha
	push ds

	push cs
	pop ds

    push cx
    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

	mov [ si + 4 ],ax			; Pone al spr donde se encuentra. X
	mov [ si + 6 ],bx			; Pone al spr donde se encuentra. Y

    pop cx

    mov byte ptr [ si + 8 ],0
    call gsprbkg                ; Get Sprite CX Background page 0
    mov byte ptr [ si + 8 ],1
    call gsprbkg                ; Get Sprite CX Background page 1

    mov tox,ax
    mov toy,bx
    mov frompage,3
	mov ax,[ si + 0 ]
	mov fromx,ax				; x org del spr
	mov ax,[ si + 2 ]
	mov fromy,ax				; y org del spr
    mov topage,0
    mov ax,[ si + 9 ]
	mov pwidth,ax				; Max x del spr
	mov ax,[ si + 11 ]
	mov pheight,ax				; Max y del spr
	mov ax,[ si + 15 ]
	mov bmskp,ax				; M�scara del sprite ( 0 = no mascara )
	call blockmove
    mov topage,1
    call blockmove              ; Lo graba en las 2 p�ginas.

	pop ds
	popa
	ret
putspr endp

;---------------------------;
;		MoveSpr 			;
;---------------------------;
;	   Move sprite			;
;---------------------------;
;	In: 					;
;		ax	= to x			;
;		bx	= to y			;
;		cx	= N of Sprite	;
;---------------------------;
movespr proc near
	pusha
    push ds

    push cs
    pop ds

    push cx
    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]				; Get Sprite Structure

    mov cl,0
    call showpage               ; mostrar p�gina 0

    pop cx

    mov  dx,[ si + 4]
    push dx                     ; ( x , y , sprite )
    mov  dx,[ si + 6]
    push dx                     ; pushear valores antiguos antes de modificar.
    push cx                     ;

    mov  byte ptr [ si + 8 ],1  ; to page 1
    call rsprbkg                ; Restore Sprite BackGround.

	mov  [ si + 4 ],ax			; To x
	mov  [ si + 6 ],bx			; To y

    call gsprbkg                ; Get Sprite Background.

    mov tox,ax                  ;
    mov toy,bx                  ;
    mov frompage,3
	mov ax,[ si + 0 ]
	mov fromx,ax				; x org del spr
	mov ax,[ si + 2 ]
	mov fromy,ax				; y org del spr
    mov topage,1
	mov ax,[ si + 9 ]
	mov pwidth,ax				; Max x del spr
	mov ax,[ si + 11 ]
	mov pheight,ax				; Max y del spr
	mov ax,[ si + 15 ]
	mov bmskp,ax				; M�scara del sprite ( 0 = no mascara )
	call blockmove

    mov cl,1
    call showpage               ; Mostrar p�gina uno.
    mov byte ptr [ si + 8 ],0   ; Trabajar con sprite en p�gina 0

    pop cx
    pop bx
    pop ax

    mov  dx,[ si + 4 ]          ; To x
    push dx
    mov  dx,[ si + 6 ]          ; To y
    push dx

    mov  [ si + 4 ],ax          ; Old x
    mov  [ si + 6 ],bx          ; Old y
    call rsprbkg
    pop bx
    pop ax
    mov  [ si + 4 ],ax          ; To x
	mov  [ si + 6 ],bx			; To y
    call gsprbkg

    mov tox,ax                  ;
    mov toy,bx                  ;
    mov frompage,3
	mov ax,[ si + 0 ]
	mov fromx,ax				; x org del spr
	mov ax,[ si + 2 ]
	mov fromy,ax				; y org del spr
    mov topage,0
	mov ax,[ si + 9 ]
	mov pwidth,ax				; Max x del spr
	mov ax,[ si + 11 ]
	mov pheight,ax				; Max y del spr
	mov ax,[ si + 15 ]
	mov bmskp,ax				; M�scara del sprite ( 0 = no mascara )
	call blockmove

    mov  byte ptr [ si + 8 ],1  ;

    pop ds
    popa
	ret
movespr endp

;---------------------------;
;		GSprBkg 			;
;---------------------------;
;	Get sprite background	;
;---------------------------;
;	In: 					;
;		cx	= N of Sprite	;
;---------------------------;
gsprbkg proc near
	pusha
    push ds

    push cs
    pop ds

    shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

	mov ax,[ si + 4 ]
    mov fromx,ax                ; de donde ? x
	mov ax,[ si + 6 ]
	mov fromy,ax				; de donde ? y
	mov al,[ si + 8 ]			; From pagina
    mov byte ptr frompage,al    ;
	mov topage,2				; To page

    mov bx,[ si + 9 ]
    mov pwidth,bx               ; Max x del spr
	mov ax,[ si + 11 ]
	mov pheight,ax				; Max y del spr

    mov ax,[ si + 0 ]           ;
    cmp byte ptr [ si + 8 ],0
    je frompage0
    add ax,bx                   ; Si es p�gina 1, then tox = orgx + maxx
frompage0:
    mov tox,ax                  ; To x - org
	mov ax,[ si + 2 ]			;
	mov toy,ax					; To y - org
	mov bmskp,0 				; M�scara del sprite ( 0 = no mascara )
	call blockmove

    pop ds
    popa
	ret
gsprbkg endp

;---------------------------;
;		RSprBkg 			;
;---------------------------;
; Restore sprite background ;
;---------------------------;
;	In: 					;
;		cx	= N of Sprite	;
;---------------------------;
rsprbkg proc near
	pusha
    push ds

    push cs
    pop ds

	shl  cx,1
	mov  di,offset spritestruc
	add  di,cx
	mov  si,[ di ]

	mov ax,[ si + 4 ]			;
	mov tox,ax					; a donde ? x
	mov ax,[ si + 6 ]			;
	mov toy,ax					; a donde ? y
	mov al,[ si + 8 ]			; a pagina
    mov byte ptr topage,al      ;
	mov frompage,2				; de page

    mov bx,[ si + 9 ]
    mov pwidth,bx               ; Max x del spr
	mov ax,[ si + 11 ]
	mov pheight,ax				; Max y del spr


    mov ax,[ si + 0 ]           ;
    cmp byte ptr [ si + 8 ],0
    je  frompage02
    add ax,bx
frompage02:
    mov fromx,ax                ; de x - org
	mov ax,[ si + 2 ]			;
	mov fromy,ax				; de y - org
	mov bmskp,0 				; M�scara del sprite ( 0 = no mascara )
	call blockmove

    pop ds
	popa
	ret
rsprbkg endp

;*********************************************************************;
;*	  Author		 : Michael Tischer								 *;
;*	  Developed on	 : 09/05/90 									 *;
;*	  Last update	 : 02/17/92 									 *;
;*********************************************************************;
;                                                                     ;
; Modificada en 1994 por Ricardo Quesada.                             ;
; Esta funci�n fue modifica para su mejor interfase                   ;
;                                                                     ;
;---------------------------------------------------------------------;

;== Constants =========================================================

SC_INDEX	   = 3c4h			  ;Index register for sequencer ctrl.
SC_MAP_MASK    = 2				  ;Number of map mask register
SC_MEM_MODE    = 4				  ;Number of memory mode register

GC_INDEX	   = 3ceh			  ;Index register for graphics ctrl.
GC_GRAPH_MODE  = 5				  ;Number of graphic mode register

VERT_RESCAN    = 3DAh			  ;Input status register #1
PIXX		   = 320			  ;Horizontal resolution
;== Program ============================================================

additive   dw ? 				  ;Local variable
restc	   dw ?
movec	   dw ?
frompage   dw ? 				  ;From page
fromx	   dw ? 				  ;From X-coordinate
fromy	   dw ? 				  ;From Y-coordinate
topage	   dw ? 				  ;To page
tox 	   dw ? 				  ;To X-coordinate
toy 	   dw ? 				  ;To Y-coordinate
pwidth	   dw ? 				  ;Pixel width
pheight    dw ? 				  ;Pixel height
bmskp	   dw ? 				  ;Pointer to buffer with bit mask


;-- BLOCKMOVE: Moves a group of pixels in video RAM -------------------
;-- Declaration :
;		In: Parametros pasados en variables de prog.
;	   Out: None
;----------------------------------------------------------------------
blockmove proc near

	   pusha
	   push  ds
	   push  es

	   mov	 dx,GC_INDEX	  ;Get current write mode and
	   mov	 al,GC_GRAPH_MODE ;initialize write mode 1
	   out	 dx,al
	   inc	 dx
	   in	 al,dx
	   push  ax 			  ;Push current mode onto stack
	   and	 al,not 3
	   or	 al,1
	   out	 dx,al

	   mov	 al,4			  ;Move DS to start of FROM page
	   mov	 cl,byte ptr frompage
	   mul	 cl
	   or	 al,0A0h
	   xchg  ah,al
	   mov	 ds,ax

	   mov	 al,4			  ;Move ES to start of TO page
	   mov	 cl,byte ptr cs:topage
	   mul	 cl
	   or	 al,0A0h
	   xchg  ah,al
	   mov	 es,ax

	   mov	 ax,PIXX / 4	  ;Move SI to FROM starting position
	   mul	 cs:fromy
	   mov	 si,cs:fromx
	   shr	 si,1
	   shr	 si,1
	   add	 si,ax

	   mov	 ax,PIXX / 4	  ;Move DI to TO position
	   mul	 cs:toy
	   mov	 di,cs:tox
	   shr	 di,1
	   shr	 di,1
	   add	 di,ax

	   mov	 dh,byte ptr cs:pheight ;DH = Pixel lines
	   mov	 dl,byte ptr cs:pwidth	;DL = Bytes
	   shr	 dl,1
	   shr	 dl,1

	   mov	 bx,PIXX / 4	  ;Move BX as offset to next line
	   sub	 bl,dl
	   xor	 ch,ch			  ;High byte of counter is always 0
	   cmp	 cs:bmskp,0 	  ;No background?
	   jne	 mt2			  ;None, use other copy routine

	   push  dx 			  ;Push DX onto stack
	   mov	 dx,SC_INDEX	  ;Get bitplane access
	   mov	 ah,0Fh
	   mov	 al,SC_MAP_MASK
	   out	 dx,ax
	   pop	 dx 			  ;Pop DX

	   ;-- Copy routine for all four bitplanes,
	   ;-- without checking the background

mt1:   mov	 cl,dl			  ;Number of bytes to CL

	   rep movsb			  ;Copy lines
	   add	 di,bx			  ;DI and
	   add	 si,bx			  ;SI in next line
	   dec	 dh 			  ;Still more lines?
	   jne	 mt1			  ;Yes --> Continue
	   jmp short mtend		  ;No --> Get out of routine

	   ;-- Copy routine for individual bitplanes using
	   ;-- the specified bit mask arrays

mt2:
	   mov	 byte ptr cs:restc,dh ;First specify variables
	   mov	 byte ptr cs:movec,dl ;placed in local variables
	   mov	 cs:additive,bx 	  ;on the stack

	   mov	 al,SC_MAP_MASK   ;Address permanent
	   mov	 dx,SC_INDEX	  ;Map mask register
	   out	 dx,al
	   inc	 dx 			  ;Increment DX on data register

	   mov	 bx,cs:bmskp	  ;BX is pointer to bit mask array
	   push  ds
	   push  cs
	   pop	 ds 			  ; antiguamente  mov ds,cs:data_seg
	   mov	 al,[bx]		  ;Load first byte		tiene que ir [bx]
	   xor	 ah,ah			  ;Start with an even byte
	   pop	 ds

mt3:   mov	 cl,cs:byte ptr movec ;Move number of bytes to CL

mt4:   out	 dx,al			  ;Set bit mask
	   movsb				  ;Copy 4 bytes

	   inc	 ah 			  ;Increment odd/even counter
	   test  ah,1			  ;Odd again?
	   jne	 mt5			  ;Yes --> Move nibble

	   ;-- Get next byte from buffer on every even byte -----------

	   inc	 bx 			  ;BX to next bit mask byte
	   push  ds
	   push  cs
	   pop	 ds 			  ; antiguamente, mov ds,cs:data_seg
	   mov	 al,[bx]		  ;Load next byte		tiene que ir [bx]
	   pop	 ds
	   loop  mt4			  ;Next four latches
	   jmp	 short mt6

mt5:   shr	 al,1			  ;Get odd byte bit mask from
	   shr	 al,1			  ;low nibble
	   shr	 al,1
	   shr	 al,1
	   loop  mt4			  ;Next four latches

mt6:   add	 di,cs:additive 	 ;Add DI and
	   add	 si,cs:additive 	 ;SI to next line
	   dec	 byte ptr cs:restc	 ;Still more lines?
	   jne	 mt3				 ;Yes --> Continue

mtend: mov	 dx,GC_INDEX	  ;Revert to old
	   pop	 ax 			  ;write mode
	   mov	 ah,al
	   mov	 al,GC_GRAPH_MODE
	   out	 dx,ax

	   pop	 es
	   pop	 ds
	   popa

	   ret
blockmove endp
