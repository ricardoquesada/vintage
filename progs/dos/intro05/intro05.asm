;-----------------------------------------------;
;												;
;					Intro 05					;
;			El Ni�o del Sol (c) 1994.			;
;												;
;-----------------------------------------------;
;		Segunda intro en modo gr�fico			;
;		  Hecha para probar sprites.			;
;-----------------------------------------------;
P286

code	segment public byte 'code'
org 	100h
intro04 PROC
assume cs:code,ds:code,ss:code,es:code

VERT_RESCAN    = 3DAh			  ;Input status register #1

;---------------------------;
start:
;---------------------------;
	call is286
	call isvga
	call init320200
	call mainprogram

	mov ax,3
	int 10h 			;volver al modo texto.
	mov ax,4c00h
	int 21h

;---------------------------;
;		Is286				;
;---------------------------;
is286 proc near
	xor ax,ax
	push ax
	popf
	pushf
	pop ax
	and ax,0f000h
	cmp ax,0f000h
	je not286
	ret
not286:
	mov dx,offset no286msg
	mov ah,9
	int 21h
	mov ax,4cffh
	int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc near
		mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near
	call letras 			; dibujar background

	mov  cl,1
	call showpage

	mov cl,0				; create sprite num. 0
	call createspr

    mov cl,1
    call createspr

    mov cl,2
    call createspr

	mov cl,0				; Num of sprite.
	mov ax,100				; x
    mov bx,100              ; y
	call putspr 			; PutSprite

    mov cl,1                ; Num of sprite.
    mov ax,200              ; x
    mov bx,100              ; y
    call putspr             ; PutSprite

    mov cl,2                ; Num of sprite.
    mov ax,150              ; x
    mov bx,100              ; y
    call putspr             ; PutSprite

    mov bx,100
mploop1:
;    mov ax,125
;    mov cl,0
;    call movespr
    mov ax,150
    mov cl,1
    call movespr
;    mov ax,175
;    mov cl,2
;    call movespr
    dec bx
    cmp bx,-40
    jne mainhere2
    mov bx,140

mainhere2:
;    call verticalr
    call getkey
    je   mploop1
    ret
mainprogram endp

;---------------------------;
;		Letras				;
;---------------------------;
letras proc near
    mov cl,1
	call showpage

    mov cl,0
    call setpage

    mov ax,38                           ; x0
	mov bx,160							; y0
	mov cx,24							; Color
    mov dx,6                            ; Variaci�n de color.
    mov si,offset message0
	call displayletras

    mov ax,38                           ; x0
    mov bx,100                          ; y0
    mov cx,15                           ; Color
    mov dx,0                            ; Variaci�n de color.
    mov si,offset message0
	call displayletras

    mov cl,1
    call setpage

    mov ax,38                           ; x0
	mov bx,160							; y0
	mov cx,24							; Color
    mov dx,6                            ; Variaci�n de color.
    mov si,offset message1
	call displayletras

    mov ax,38                           ; x0
    mov bx,100                          ; y0
    mov cx,15                           ; Color
    mov dx,0                            ; Variaci�n de color.
    mov si,offset message1
	call displayletras

    ret
letras	endp

;---------------------------;
;		getkey				;
;---------------------------;
getkey proc near
     mov ah,1
     int 16h
;    in  al,060h
;    cmp al,01h
	ret
getkey endp

;---------------------------;
; Sprites	-	Estructuras ;
;---------------------------;
spritestruc equ this word
	dw	offset sprite0
	dw	offset sprite1
    dw  offset sprite2

sprite0 equ this word
	dw	0				; pos original x			; + 0
	dw	0				; pos original y			; + 2
	dw	?				; ubicaci�n de ahora x		; + 4
	dw	?				; ubicaci�n de ahroa y		; + 6
	db	1				; p�gina donde esta 		; + 8
	dw	20				; tama�o x					; + 9
	dw	20				; tama�o y					; + 11
	dw	offset spr0dat	; datos del sprite 0		; + 13
	dw	offset spr0bmk	; datos de la m�scara.		; + 15

sprite1 equ this word
    dw  0               ; pos original x            ; + 0
    dw  20              ; pos original y            ; + 2
	dw	?				; ubicaci�n de ahora x		; + 4
	dw	?				; ubicaci�n de ahroa y		; + 6
	db	1				; p�gina donde esta 		; + 8
	dw	24				; tama�o x					; + 9
	dw	40				; tama�o y					; + 11
	dw	offset spr1dat	; datos del sprite 1		; + 13
	dw	offset spr1bmk	; datos de la m�scara.		; + 15

sprite2 equ this word
    dw  0               ; pos original x            ; + 0
    dw  60              ; pos original y            ; + 2
	dw	?				; ubicaci�n de ahora x		; + 4
	dw	?				; ubicaci�n de ahroa y		; + 6
	db	1				; p�gina donde esta 		; + 8
	dw	24				; tama�o x					; + 9
	dw	40				; tama�o y					; + 11
    dw  offset spr2dat  ; datos del sprite 1        ; + 13
    dw  offset spr2bmk  ; datos de la m�scara.      ; + 15


message0    db 'pagina 0 pagina 0 pagina 00000',0
message1    db 'pagina 1 pagina 1 pagina 11111',0

vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'

;---------------------------;
;       Includes            ;
;---------------------------;
include     320200.inc                  ; Incluye funciones para manejo de pixels.
include     sprites.inc                 ; Incluye las funciones de los sprites.
include     grafik.inc                  ;
include     font0001.inc                ; Fonts
include     sprdat.inc                  ; Datos de los sprites.

intro04 endp
code	ends
		end intro04
