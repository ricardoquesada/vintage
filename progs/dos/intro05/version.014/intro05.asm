;-----------------------------------------------;
;												;
;					Intro 05					;
;			El Ni�o del Sol (c) 1994.			;
;												;
;-----------------------------------------------;
;		Segunda intro en modo gr�fico			;
;		  Hecha para probar sprites.			;
;-----------------------------------------------;
P286

code	segment public byte 'code'
org 	100h
intro04 PROC
assume cs:code,ds:code,ss:code,es:code

VERT_RESCAN    = 3DAh			  ;Input status register #1

;---------------------------;
start:
;---------------------------;
	call is286
	call isvga
	call init320200
	call mainprogram

	mov ax,3
	int 10h 			;volver al modo texto.
	mov ax,4c00h
	int 21h

;---------------------------;
;		Is286				;
;---------------------------;
is286 proc near
	xor ax,ax
	push ax
	popf
	pushf
	pop ax
	and ax,0f000h
	cmp ax,0f000h
	je not286
	ret
not286:
	mov dx,offset no286msg
	mov ah,9
	int 21h
	mov ax,4cffh
	int 21h
is286 endp

;---------------------------;
;		isvga				;
;---------------------------;
isvga proc near
		mov ax,1a00h
		int 10h
		cmp al,1ah				;compara si es placa VGA
		je vgaonly
		mov ah,9
		mov dx,offset vga_msg
		int 21h
		mov ax,4cffh
		int 21h
vgaonly:
		ret
isvga  endp


;---------------------------;
;	mainprogram 			;
;---------------------------;
mainprogram proc near
	call letras 			; dibujar background

    mov  cl,0
	call showpage

    mov cl,0
contcreat:
    call createspr
    inc cl
    cmp cl,4
    jne contcreat

    mov  ax,0
mploop1:
    mov  bx,100
    push ax
    shr  ax,1
    mov  cl,0
    call posspr
    pop  ax

    xchg ax,bx
    mov  cl,1
    call posspr

    push ax
    mov  ax,bx
    mov  cl,2
    call posspr
    shr  ax,1
    mov cl,3
    call posspr
    pop  ax


    xchg  ax,bx
    mov  cx,3
    call movesprites

    inc ax
    cmp ax,160
    jne mainhere2
    mov ax,0

mainhere2:
    call getkey
    je   mploop1
    ret
mainprogram endp

;---------------------------;
;		Letras				;
;---------------------------;
letras proc near
    mov cl,1
	call showpage

    mov cl,0
    call setpage

    mov ax,38                           ; x0
	mov bx,160							; y0
	mov cx,24							; Color
    mov dx,6                            ; Variaci�n de color.
    mov si,offset message0
	call displayletras

    mov ax,38                           ; x0
    mov bx,100                          ; y0
    mov cx,15                           ; Color
    mov dx,0                            ; Variaci�n de color.
    mov si,offset message0
	call displayletras

    mov cl,1
    call setpage

    mov ax,38                           ; x0
	mov bx,160							; y0
	mov cx,24							; Color
    mov dx,6                            ; Variaci�n de color.
    mov si,offset message1
	call displayletras

    mov ax,38                           ; x0
    mov bx,100                          ; y0
    mov cx,15                           ; Color
    mov dx,0                            ; Variaci�n de color.
    mov si,offset message1
	call displayletras

    ret
letras	endp

;---------------------------;
;		getkey				;
;---------------------------;
getkey proc near
    push ax
    mov  ah,1
    int  16h

;    in  al,060h
;    cmp al,01h
    pop ax
    ret
getkey endp

;---------------------------;
;       Estructuras         ;
;---------------------------;

sprstruc struc
    sp_orgx dw  ?               ; pos original x
    sp_orgy dw  ?               ; pos original y
    sp_nowx dw  ?               ; ubicaci�n de ahora x
    sp_nowy dw  ?               ; ubicaci�n de ahroa y
    sp_page db  ?               ; p�gina donde esta
    sp_tamx dw  ?               ; tama�o x
    sp_tamy dw  ?               ; tama�o y
    sp_dato dw  ?               ; datos del sprite 1
    sp_mask dw  ?               ; datos de la m�scara.
    sp_oldx dw  ?               ; ubicaci�n old de x
    sp_oldy dw  ?               ; ubicaci�n old de y
    sP_visi db  ?               ; sprite visible al crearse ?
    sp_offs db  ?               ; offset del sprite (0-3)
sprstruc ends


message0    db 'El ni�o del Sol 5 v0.14 - 0',0
message1    db 'El ni�o del Sol 5 v0.14 - 1',0

vga_msg  db'VGA only!',13,10,'$'
no286msg db'Aaaahh! Not in a XT please. At least a 286, thanx.',13,10,'$'

;---------------------------;
;       Includes            ;
;---------------------------;
include     320200.inc                  ; Incluye funciones para manejo de pixels.
include     sprites.inc                 ; Incluye las funciones de los sprites.
include     grafik.inc                  ;
include     font0001.inc                ; Fonts
include     sprdat.inc                  ; Datos de los sprites.

intro04 endp
code	ends
		end intro04
