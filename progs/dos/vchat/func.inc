;======================================;
;                                      ;
;                                      ;
;      FUNCIONES TOKEN                 ;
;                                      ;
;                                      ;
;======================================;

;======================================;
T_HELPOFF   PROC
       push si
       mov  si,OFFSET T_HELPMSG
       mov  cx,T_HELPMSGLEN
       call copy2screen
       pop  si
       ret
T_HELPOFF   ENDP


;======================================;
T_ABOUTOFF   PROC
       push si
       mov  si,OFFSET T_ABOUTMSG
       mov  cx,T_ABOUTMSGLEN
       call copy2screen
       pop  si
       ret
T_ABOUTOFF   ENDP

;======================================;
T_NAOFF   PROC
       push si
       mov  si,OFFSET T_NAMSG
       mov  cx,T_NAMSGLEN
       call copy2screen
       pop  si
       ret
T_NAOFF   ENDP
