;======================================;
;                                      ;
;                                      ;
;      FUNCIONES IPX                   ;
;                                      ;
;                                      ;
;======================================;


;======================================;
;      IPXTSR                          ;
;======================================;
;      Out: Al = $ff if installed      ;
;======================================;
IPXTSR PROC
       push ds
       push es
       mov  ax,@DATA
       mov  ds,ax

       mov  ax,7a00h                   ; Check  if IPX is installed.
       int  2fh

       cmp  al,0ffh
       jne  @@notipx
       mov  si,offset IPXENTRY
       mov  [si+2],di                  ; Far entry point of IPX OFFSET
       mov  [si],es                    ; Far entry point of IPX SEGMENT
       clc                             ; Carry OFF if OK.
       jmp  @@ok
@@notipx:
       stc                             ; Carry ON if any error.
@@ok:
       pop  es
       pop  ds
       ret
IPXTSR ENDP

;======================================;
;      GETLADDRESS                     ;
;======================================;
GETLADDRESS PROC
       pusha                           ; Get local Address
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax
       push ds
       pop  es
       mov  di,offset LocalADDR        ; ES:DI

       mov  bx,9                       ; Get internetwork address
       call DS:[IPXENTRY]              ; Same as INT 7A

       pop  es
       pop  ds
       popa
       ret
GETLADDRESS ENDP

;======================================;
;      OP_SOCKET                       ;
;======================================;
OP_SOCKET   PROC
       pusha
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax

       xor  bx,bx                      ; Open Socket
       xor  al,al                      ; Socket Longevity
                                       ; Al=0 (normal), al=0ffh (TSR)
       xor  dx,dx                      ; Socket number (0=dinamic)
       call ds:[IPXENTRY]              ; Same as int 7a
       or   al,al                      ; Success ?
       jne  @@notok
       clc                             ; Carry OFF if OK.
       mov  MYSOCKET,dx
       jmp  @@ok
@@notok:
       stc                             ; Carry ON if any error
@@ok:
       pop  es
       pop  ds
       popa
       ret
OP_SOCKET   ENDP

;======================================;
;      CL_SOCKET                       ;
;======================================;
CL_SOCKET   PROC
       pusha
       push ds

       mov  ax,@DATA
       mov  ds,ax

       mov  bx,1                       ; Close Socket
       mov  dx,MYSOCKET                ; Socket number
       call ds:[IPXENTRY]              ; Same as int 7a

       pop  ds
       popa
       ret
CL_SOCKET   ENDP


.DATA
;======================================;
;      Local Address                   ;
;======================================;
LocalADDR label byte
db 4 dup(0)                            ; Network number
db 6 dup(0)                            ; Node number


.CODE
