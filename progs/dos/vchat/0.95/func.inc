;======================================;
;                                      ;
;                                      ;
;      FUNCIONES TOKEN                 ;
;                                      ;
;                                      ;
;======================================;

;======================================;
T_HELPOFF   PROC
       push si
       mov  si,OFFSET T_HELPMSG
       mov  cx,T_HELPMSGLEN
       call copy2screen
       pop  si
       ret
T_HELPOFF   ENDP

;======================================;
T_CONTACTOFF   PROC
       push si
       mov  si,OFFSET T_CONTACTMSG
       mov  cx,T_CONTACTMSGLEN
       call copy2screen
       pop  si
       ret
T_CONTACTOFF   ENDP

;======================================;
T_VEROFF    PROC
       push si
       mov  si,OFFSET T_VERMSG
       mov  cx,T_VERMSGLEN
       call copy2screen
       pop  si
       ret
T_VEROFF    ENDP


;======================================;
T_ABOUTOFF   PROC
       push ds
       push es
       push si
       push di

       mov  ax,@DATA
       mov  ds,ax
       mov  ax,0b800h
       mov  es,ax

       mov  di,SCREENOFF
       push di

       mov  si,OFFSET SECRETMSG
       mov  cx,LOGOMSGLEN
       call copy2screen

       pop  di
       xor  bx,bx
       add  di,28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       stosb
       inc  di
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6                       ; total lines.
       jne  @@loop5                    ; Paint me, please

       pop  di
       pop  si
       pop  es
       pop  ds
       ret
T_ABOUTOFF   ENDP

;======================================;
T_NICKOFF   PROC
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax
       mov  es,ax
       mov  si,offset NAME_            ; DS:SI
       mov  di,offset OLDNICK          ; ES:DI

       mov  cx,11                      ; max len name.
rep    movsb                           ; copia el viejo nicknamez.

       mov  si,OLDSI
       mov  di,OFFSET NAME_
       mov  ax,SEGMEM1                 ; ES = DATA
       mov  ds,ax                      ; DS = SEGMEM1

       mov  cx,11
       mov  al,0h
rep    stosb                           ; Clear name

       mov  di,OFFSET NAME_
@@loop:
       lodsb
       cmp  al,20h
       je   @@loop
       xor  cx,cx
@@loop1:
       or   al,al
       je   @@end
       stosb
       lodsb
       inc  cx
       cmp  cx,10
       jae  @@end                      ; no more than 11 chars
       jmp  @@loop1
@@end:
       mov  al,0
       stosb                           ; name is an ASCIIZ
       Call DrawBar

       push ds
       push es
       pop  ds                         ; XCHG DS,ES
       pop  es                         ; ES=SEGMEM1, DS=DATA

       xor  dx,dx                      ; reset counter
       xor  di,di
       mov  si,offset OLDNICK-1
       mov  ax,'**'
       stosw
       mov  ax,' *'
       stosw
       add  dx,4                       ; update counter

@@looping2:                            ; Copy old nick to message.
       lodsb
       or   al,al
       je   @@ending
       stosb
       inc  dx
       jmp  @@looping2
@@ending:

       mov  si,OFFSET NEWNICK
       mov  cx,NEWNICKLEN
       add  dx,cx
rep    movsb

       mov  si,OFFSET NAME_            ; new nick
@@looping3:                            ; Copy new nick to message.
       lodsb
       or   al,al
       je   @@ending2
       stosb
       inc  dx
       jmp  @@looping3
@@ending2:
       mov  al,'>'
       stosb
       inc  dx
       mov  cx,dx                      ; real counter.

       push es
       pop  ds
       xor  si,si
       call copy2screen                ; Print changes.

       pop  es
       pop  ds
       ret
T_NICKOFF   ENDP

;======================================;
T_CONSOLEOFF PROC
       push ds
       mov  si,OLDSI
       mov  di,OFFSET REQMSG
       mov  ax,SEGMEM1
       mov  ds,ax

       xor  cx,cx                      ; Cantidad de letras
@@loop:
       lodsb
       cmp  al,20h
       je   @@loop
@@loop1:
;       cmp  al,20h
;       je   @@end
       or   al,al
       je   @@end
       inc  cx                         ; Cantidad total
       stosb
       lodsb
       jmp  @@loop1
@@end:
       mov  ES:REQLEN,cl
       add  cl,3
       mov  BYTE PTR ES:REQBUF,cl
       mov  ah,0e1h

       push es
       pop  ds
       mov  si,offset reqbuf           ; Request Buffer
       mov  di,offset repbuf           ; Reply Buffer
       int  21h

       pop  ds
       ret
T_CONSOLEOFF   ENDP


;======================================;
T_NAOFF   PROC
       push si
       mov  si,OFFSET T_NAMSG
       mov  cx,T_NAMSGLEN
       call copy2screen
       pop  si
       ret
T_NAOFF   ENDP

;======================================;
T_CLEAROFF  PROC
       push es
       push ds

       cld
       mov  ax,@data
       mov  ds,ax
       mov  ax,0b800h
       mov  es,ax

       mov  SCREENOFF,SCREENOFF_       ; SCREENOFF_ = MACRO
       mov  STRADR,50h*5
       mov  di,50h*6
       mov  ax,0720h
       mov  cx,4000h-50h*6
rep    stosw

       pop  ds
       pop  es
       ret
T_CLEAROFF  ENDP

;======================================;
T_SERROROFF   PROC
       push si
       mov  si,OFFSET T_SERRORMSG
       mov  cx,T_SERRORMSGLEN
       call copy2screen
       pop  si
       ret
T_SERROROFF   ENDP

;======================================;
T_QUITOFF   PROC
       jmp  realend                    ; finish boy!
T_QUITOFF   ENDP

;======================================;
NOIPX  PROC
       push si
       mov  si,OFFSET NOIPXMSG
       mov  cx,NOIPXMSGLEN
       call copy2screen
       pop  si
       ret
NOIPX  ENDP
