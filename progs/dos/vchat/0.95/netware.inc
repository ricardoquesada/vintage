;======================================;
;                                      ;
;                                      ;
;      NETWARE FUNCTIONS               ;
;                                      ;
;                                      ;
;======================================;

;======================================;
;      GETCONNUMBER                    ;
;======================================;
;      OUT: AL <- CONN NUMBER          ;
;           AL == 0, NOT LOGGED IN     ;
;           CX <- ASCII CONN . CL 1ST  ;
;======================================;
GETCONNUMBER PROC
       mov  ah,0dch
       int  21h
       ret
GETCONNUMBER ENDP

;======================================;
;      GETNAME                         ;
;======================================;
;      IN: AL -> CONNECTION NUMBER     ;
;          ES:DI  -> DESTINATION       ;
;======================================;
GETNAME     PROC
       pusha
       push ds
       push es
       push di

       mov  bx,@data
       mov  ds,bx
       mov  es,bx
       mov  di,offset repbuf           ; ES:DI = REP BUF
       mov  si,offset reqbuf           ; DS:SI = REQ BUF
       mov  word ptr [si],2
       mov  byte ptr [si+2],16h        ; Get connection information
       mov  [si+3],al                  ; Logical connection number.
       mov  ah,0e3h
       int  21h                        ;
       mov  si,di                      ; DS:SI
       add  si,2                       ; Logged in ?
       mov  cx,[si]
       mov  bx,[si+2]
       or   bx,cx                      ; =0 ?
       je   @@notok                    ; yes, so not logged in.

       add  si,6                       ; ok, now point to name.
       pop  di
       pop  es                         ; ES:DI
       or   al,al                      ; error on int 21h, 0e3h ?
       jne  @@notok                    ; yes.

@@loop:
       lodsb
       stosb                           ; Copy DS:SI -> ES:DI
       or   al,al                      ; Name is a ASCIIZ
       jne  @@loop

@@notok:
       pop  es
       pop  ds
       popa
       ret
GETNAME     ENDP

;======================================;
;      SENDBROADCASTN                  ;
;======================================;
;      IN: AL <- NUM OF CONN           ;
;          DS:SI <- OFFSET MSG ASCIIZ  ;
;      OUT: CARRY CLEAR OK             ;
;======================================;
SENDBROADCASTN PROC
       pusha
       push ds
       push es

       mov  dl,al                      ; dl is now connection number.
       mov  bx,@DATA
       mov  es,bx
       mov  di,OFFSET REQBUF+6         ; A partir de aca va el mensaje.
       xor  cx,cx                      ; contador.
@@loop1:
       lodsb                           ;
       stosb
       inc  cx                         ; incrementa el contador.
       or   al,al                      ; AsciiZ ?
       jne  @@loop1

       mov  ds,bx                      ; DS=DATA, ES=DATA
       mov  si,offset REQBUF
       mov  di,offset REPBUF           ;
       mov  [si+5],cl                  ; lenght of message.
       add  cx,5
       mov  [si],cx                    ; lenght of packet.
       mov  byte ptr [si+2],0          ; function: send broadcast message. (otherwise, try 04. Personal msg)
       mov  byte ptr [si+3],1          ; to one connection.
       mov  [si+4],dl                  ; Connection number.

       mov  ah,0e1h                    ;
       int  21h                        ; Ok.

       clc                             ; default succesful.
       cmp  byte ptr [di+3],0          ; Succesful ?
       je   @@end
       stc                             ; else set carry.
@@end:
       pop  es
       pop  ds
       popa
       ret
SENDBROADCASTN ENDP


.DATA
;======================================;
;      GENERAL REQUEST BUFFER          ;
;          & REPLY BUFFER              ;
;======================================;
reqbuf dw ?                            ; Lenght
       db ?                            ; subf
reqlen db ?                            ; Lenght - 3
reqmsg db 100 dup(?)                   ; Mensaje y otras cosas.

repbuf db 100 dup(?)

.CODE

