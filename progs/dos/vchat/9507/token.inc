;======================================;
;                                      ;
;      ESTRUCTURA                      ;
;======================================;
;                                      ;
; 0   DB LENGHT OF TOKEN               ;
; 1   DB ASCII                         ;
; ?   DW OFFSET EXE                    ;
;                                      ;
; Last record:                         ;
; 0   DB 0                             ;
;                                      ;
;======================================;

TOKENMSG    LABEL BYTE

db     1,'?' AND 0dfH                  ; lo convierte a may�sculas.
dw     OFFSET T_HELPOFF

db     5,'ABOUT'
dw     OFFSET T_ABOUTOFF

db     5,'CLEAR'
dw     OFFSET T_CLEAROFF

db     7,'CONSOLE'
dw     OFFSET T_CONSOLEOFF

db     7,'CONTACT'
dw     OFFSET T_CONTACTOFF

db     4,'HELP'
ENDTOK_ EQU THIS BYTE
dw     OFFSET T_HELPOFF
NEXTRECORD = $-ENDTOK_

db     6,'INVITE'
dw     OFFSET T_NAOFF

db     3,'MSG'
dw     OFFSET T_NAOFF

db     4,'NICK'
dw     OFFSET T_NICKOFF

db     8,'NICKLIST'
dw     OFFSET T_NAOFF

db     4,'QUIT'
dw     OFFSET T_QUITOFF

db     4,'SAVE'
dw     OFFSET T_NAOFF

db     4,'SEND'
dw     OFFSET T_SENDOFF

db     8,'USERLIST'
dw     OFFSET T_NAOFF

db     3,'VER'
dw     OFFSET T_VEROFF

db     3,'WHO'
dw     OFFSET T_NAOFF

db     0                               ; last token

T_HELPMSG   LABEL BYTE
db'                             VCHAT ON LINE HELP                                 '
db'type...                             to...                                       '
db'/ABOUT                              Print info about this program.              '
db'/CLEAR                              Clear screen & buffer.                      '
db'/CONSOLE MSG               *        Send a message to the console.              '
db'/CONTACT                            Print addresses of the author.              '
db'/HELP  or  /?                       Print this info.                            '
db'/INVITE USR                *        Invite USER to VCHAT.                       '
db'/MSG NICK MSG                       Send a private message to this NICKNAME.    '
db'/NICK NICKNAME                      Change your NICKNAME.                       '
db'/NICKLIST                           Print NICKNAMES using VCHAT.                '
db'/QUIT                               Quit to D.O.S.                              '
db'/SAVE FILE                          Save buffer to file.                        '
db'/SEND USER MSG             *        Send a message to a user.                   '
db'/USERLIST                  *        Print logged in USERS.                      '
db'/VER                                Print VCHAT version.                        '
db'/WHO NICKNAME                       Print real name of this NICKNAME.           '
db' (*) this commands are available only if you are logged in to a Netware server. '
;db'                                                                                '
T_HELPMSGLEN = $-T_HELPMSG

T_VERMSG    LABEL BYTE
db'VCHAT BETA VERSION 0.9507 - IF YOU ARE INTERESTED IN BEING A TESTER, CONTACT ME.'
T_VERMSGLEN = $-T_VERMSG

T_NAMSG   LABEL BYTE
db'Sorry, this command is not available yet.'
T_NAMSGLEN  = $-T_NAMSG

T_SERRORMSG   LABEL BYTE
db'*** SYNTAX ERROR: Type /HELP for help.'
T_SERRORMSGLEN  = $-T_SERRORMSG

NOIPXMSG   LABEL BYTE
db'*** ERROR: IPX not loaded. Press any key to quit.'
NOIPXMSGLEN  = $-NOIPXMSG
