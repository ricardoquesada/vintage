;======================================;
;                                      ;
;                                      ;
;      FUNCIONES IPX                   ;
;                                      ;
;                                      ;
;======================================;


;======================================;
;      IPXTSR                          ;
;======================================;
;      Out: Al = $ff if installed      ;
;======================================;
IPXTSR PROC
       push ds
       push es
       mov  ax,@DATA
       mov  ds,ax

       mov  ax,7a00h                   ; Check  if IPX is installed.
       int  2fh

       cmp  al,0ffh
       jne  @@notipx
       mov  si,offset IPXENTRY
       mov  [si+2],di                  ; Far entry point of IPX OFFSET
       mov  [si],es                    ; Far entry point of IPX SEGMENT
       clc                             ; Carry OFF if OK.
       jmp  @@ok
@@notipx:
       stc                             ; Carry ON if any error.
@@ok:
       pop  es
       pop  ds
       ret
IPXTSR ENDP

;======================================;
;      GETLADDRESS                     ;
;======================================;
GETLADDRESS PROC
       pusha                           ; Get local Address
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax
       push ds
       pop  es
       mov  di,offset SourAddr         ; ES:DI (source address = local address )

       mov  bx,9                       ; Get internetwork address
       call DS:[IPXENTRY]              ; Same as INT 7A

       pop  es
       pop  ds
       popa
       ret
GETLADDRESS ENDP

;======================================;
;      OP_SOCKET                       ;
;======================================;
OP_SOCKET   PROC
       pusha
       push ds
       push es

       mov  ax,@DATA
       mov  ds,ax

       xor  bx,bx                      ; Open Socket
       xor  al,al                      ; Socket Longevity
                                       ; Al=0 (normal), al=0ffh (TSR)
       mov  dx,VCHATSOCK               ; Socket number (0=dinamic)
       call ds:[IPXENTRY]              ; Same as int 7a
       or   al,al                      ; Success ?
       jne  @@notok
       clc                             ; Carry OFF if OK.
       mov  mysock,VCHATSOCK
       jmp  @@ok
@@notok:
       stc                             ; Carry ON if any error
@@ok:
       pop  es
       pop  ds
       popa
       ret
OP_SOCKET   ENDP

;======================================;
;      CL_SOCKET                       ;
;======================================;
CL_SOCKET   PROC
       pusha
       push ds

       mov  ax,@DATA
       mov  ds,ax

       mov  bx,1                       ; Close Socket
       mov  dx,VCHATSOCK               ; Socket number
       call ds:[IPXENTRY]              ; Same as int 7a

       pop  ds
       popa
       ret
CL_SOCKET   ENDP


;======================================;
;      SEND_PACKET                     ;
;======================================;
SEND_PACKET PROC
       pusha
       push ds
       push es

       mov  ax,@DATA
       mov  es,ax
       mov  si,offset ECBheader        ; es:si -> ecb header
       mov  bx,3                       ; Send packet
       call ds:[IPXENTRY]              ; Same as int 7a
       or   al,al                      ; Success ?
       jne  @@notok
       clc                             ; Carry OFF if OK.
       jmp  @@ok
@@notok:
       stc                             ; Carry ON if any error
@@ok:
       pop  es
       pop  ds
       popa
       ret
SEND_PACKET ENDP


.DATA
;======================================;
;      ECB header                      ;
;======================================;

EBCheader   label byte
Link   db 4 dup(0)                     ; Pointer to next ECB
ESR    db 4 dup(0)                     ; Pointer to Event Service Routine (ESR)
Inuse  db 1 dup(0)                     ; Flag telling the ECB status
Comp   db 1 dup(0)                     ; Flag telling the ECB completion code
socket db 2 dup(0)                     ; Bigendian socket number for ECB
IPXwork db 4 dup(0)                    ; Work space for IPX
Dwork  db 12 dup(0)                    ; Work space for driver
iAddr  db 12 dup(0)                    ; Address to send to
frCnt  db 2 dup(0)                     ; Number of fragments
frDat  db 4 dup(0)                     ; Pointer to data fragment
frSz   db 2 dup(0)                     ; Size of data fragment

IPXENTRY DD ?                          ; Used in IPX func

;======================================;
;      IPX header                      ;
;======================================;
IPXheader   label byte
Check  dw   0                          ; Bigendian checksum
Lenght dw   0                          ; Bigendian Lenght of packet
tc     db   0                          ; Transport control
pType  db   0                          ; Packet Type

DestAddr    label byte
db 4 dup(0)                            ; Network number
db 6 dup(0)                            ; Node number
db 2 dup(0)                            ; Socket number

SourAddr    label byte
db 4 dup(0)                            ; Network number
db 6 dup(0)                            ; Node number
mysock dw 0                            ; Socket number

;======================================;
;      message goes here.              ;
;======================================;
msghere     label byte
db 'Esto es una prueba de mi primer paquete.... espero que funcione.'
msglenght = $-IPXheader

.CODE
