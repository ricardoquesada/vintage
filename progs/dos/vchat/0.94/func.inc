;======================================;
;                                      ;
;                                      ;
;      FUNCIONES TOKEN                 ;
;                                      ;
;                                      ;
;======================================;

;======================================;
T_HELPOFF   PROC
       push si
       mov  si,OFFSET T_HELPMSG
       mov  cx,T_HELPMSGLEN
       call copy2screen
       pop  si
       ret
T_HELPOFF   ENDP

;======================================;
T_VEROFF    PROC
       push si
       mov  si,OFFSET T_VERMSG
       mov  cx,T_VERMSGLEN
       call copy2screen
       pop  si
       ret
T_VEROFF    ENDP


;======================================;
T_ABOUTOFF   PROC
       push ds
       push es
       push si
       push di

       mov  ax,@DATA
       mov  ds,ax
       mov  ax,0b800h
       mov  es,ax

       mov  di,SCREENOFF
       push di

       mov  si,OFFSET SECRETMSG
       mov  cx,LOGOMSGLEN
       call copy2screen

       pop  di
       xor  bx,bx
       add  di,28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       stosb
       inc  di
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6                       ; total lines.
       jne  @@loop5                    ; Paint me, please

       pop  di
       pop  si
       pop  es
       pop  ds
       ret
T_ABOUTOFF   ENDP

;======================================;
T_NICKOFF   PROC
       push ds
       mov  si,OLDSI
       mov  di,OFFSET NAME_
       mov  ax,SEGMEM1
       mov  ds,ax

       mov  cx,11
       mov  al,20h
rep    stosb                           ; Clear name

       mov  di,OFFSET NAME_

@@loop:
       lodsb
       cmp  al,20h
       je   @@loop
@@loop1:
       cmp  al,20h
       je   @@end
       or   al,al
       je   @@end
       stosb
       lodsb
       jmp  @@loop1
@@end:
       Call DrawBar
       pop  ds
       ret
T_NICKOFF   ENDP

;======================================;
T_NAOFF   PROC
       push si
       mov  si,OFFSET T_NAMSG
       mov  cx,T_NAMSGLEN
       call copy2screen
       pop  si
       ret
T_NAOFF   ENDP

;======================================;
T_CLEAROFF  PROC
       push es
       push ds

       cld
       mov  ax,@data
       mov  ds,ax
       mov  ax,0b800h
       mov  es,ax

       mov  SCREENOFF,SCREENOFF_       ; SCREENOFF_ = MACRO
       mov  STRADR,50h*5
       mov  di,50h*6
       mov  ax,0720h
       mov  cx,4000h-50h*6
rep    stosw

       pop  ds
       pop  es
       ret
T_CLEAROFF  ENDP

