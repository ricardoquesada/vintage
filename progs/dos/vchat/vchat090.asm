;======================================;
;                                      ;
;              V C h a t               ;
;                by Riq                ;
;                                      ;
;                                      ;
;            Chater for IPX            ;
;                                      ;
;======================================;
;                                      ;
;      Start: 7-7-95                   ;
;      Version: 0.90                   ;
;      Revision:                       ;
;      Finish:                         ;
;      Size:                           ;
;      State: in developing            ;
;======================================;

DOSSEG
MODEL SMALL
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0090h

;======================================; MACROS

;======================================;
;                                      ;
;      STACK                           ;
;                                      ;
;======================================;
STACK 200H


;======================================;
;                                      ;
;      DATA                            ;
;                                      ;
;======================================;
DATASEG

;======================================; Colores de la pantalla principal

Color_BF    db 7fh
Color_Li    db 30h

AboutMsg    label byte
db'                                  ',9,'                                     '
db'                              ',13,'                                          '
db'                        ',10,'  Ricardo Quesada.                                 '
LENGHTMSG   EQU $-AboutMsg


;======================================;
;                                      ;
;      CODE                            ;
;                                      ;
;======================================;
CODESEG
       STARTUPCODE

       Call InitScreen

       mov  ax,4c00h
       int  21h


;======================================;
;      Init Screen                     ;
;======================================;
InitScreen  proc
       mov  ax,0b800h
       mov  es,ax
       mov  ax,@DATA
       mov  ds,ax
       xor  di,di
       mov  cx,80*25
       mov  ah,Color_BF
       mov  al,20h
       cld
rep    stosw

       mov  di,80*23*2
       mov  cx,80
       mov  ah,Color_Li
rep    stosw

       xor  di,di
       mov  si,offset AboutMsg
       mov  cx,LENGHTMSG
@@here:
       movsb
       inc  di
       loop @@here

       ret
InitScreen  endp

END
