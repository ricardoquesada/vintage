;======================================;
;                                      ;
;              V C h a t               ;
;                by Riq                ;
;                                      ;
;                                      ;
;            Chater for IPX            ;
;                                      ;
;======================================;
;                                      ;
;      Start: 7-7-95                   ;
;      This Version: 12-7-95           ;
;      Version: 0.92                   ;
;      Revision:                       ;
;      Finish:                         ;
;      Size:                           ;
;      State: in developing            ;
;======================================;

DOSSEG
MODEL SMALL
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0092h

INCLUDE     KEYB.INC

;======================================; MACROS

;======================================;
;                                      ;
;      STACK                           ;
;                                      ;
;======================================;
STACK 200H


;======================================;
;                                      ;
;      DATA                            ;
;                                      ;
;======================================;
DATASEG

PANN   DB 0                            ; Used in main Scroll
DELAY  DB 0                            ; Used in main scroll
STRADR DW 0050h                        ; Used in main scroll

SECRET DB 0                            ; Used in command line

SECRETMSG LABEL BYTE
db'                                                     '
db'                                      ',9,'                               '
db'                                  ',13,'                                    '
db'                                                                              '
db'                             ',10,'  Ricardo Quesada.                          '
db'                                                     '
db'                                                                                '
db'                                 Beta Version 0.92                              '
LOGOMSGLEN  = $-SECRETMSG
db'                                                                                '
db'                Otro Chat para redes IPX, ( en especial Netware :) )            '
db'                                                                                '
db'          Requeremientos: ganas de chatear, VGA, 286+ ( 386 recomendado ),      '
db'                                   y una red IPX.                               '
db'                                                                                '
db'                                                                                '
db'    Agradecimientos: Pricipalmente a la gente de la facu (UBA-Exactas,Richie,   '
db'     Pablo,Mar�a,Sebas,Nicolai,Nicol�s,a un gran profe:Leandro), a la gente     '
db'         del laburo y al principal BetaTester (la querida Netware v3.12),       '
db'                  a la gente inolvidable: Nacho, Lole, John, Pablito,           '
db'                    a grandes tipos: a Toro y su querido CRISIS BBS,            '
db'                                a los chicos del cole,                          '
db'              al mejor compositor del mundo: Sir ANDREW LLOYD WEBER             '
db'                              ( gracias por la m�sica )                         '
db'                       a una persona que me ayudo en el comienzo:               '
db'                                 Gustavo Zacarias                               '
db'                   y a todos los que hacen Demos / Intros...                    '
db'                                                                                '
db'                                                                                '
db'                                       Tools:                                   '
db'                                                                                '
db'                                Turbo Assembler 3.0                             '
db'                                  Turbo Linker 5.0                              '
db'                                    Q-Edit v3.0                                 '
db'                                  VChar NI v3.33                                '
db'                                                                                '
db'                                                                                '
db'                                       Info:                                    '
db'                                                                                '
db'                          Un doc de Daniel Parnell de IPX                       '
db'                               Ralf Brown Int list v46                          '
db'                                 PC Intern ( a must!)                           '
db'                               Prog. Guide to EGA/VGA                           '
SECRETMSGLEN   EQU $-SECRETMSG

COPYMSG label byte
db 13,10
db'VChat v0.92 (c) Ricardo Quesada 1995.',13,10
db'Beta copy. For personal use only. Not for sale.',13,10,13,10
COPYMSGLEN EQU $-COPYMSG

HELPMSG label byte
db 13,10
db'Use:       VChat',13,10
db'Purpose:   Utility that allow users to chat in an IPX network.',13,10
db'Greetings: UBA Exactas, Chapadmamlal pals, SMT guys, Commodore 128 boys.',13,10
db'Credits:   Idea & programming, by Riq.',13,10
db'Secret:    Try VCHAT /S',13,10
db 13,10
db'Find me in:',13,10
db'            INTERNET                FIDONET                ADN',13,10
db'       rquesada@dc.uba.ar         4:900/309.3          52:800/101.5',13,10
db 13,10
HELPMSGLEN  = $-HELPMSG


INCLUDE     FONTDATA.INC               ; Datas de los chars.

;======================================;
;                                      ;
;      CODE                            ;
;                                      ;
;======================================;
CODESEG
       STARTUPCODE

       mov  ax,@data
       mov  ds,ax

       mov  bx,2                       ; Std Error
       mov  cx,COPYMSGLEN
       mov  dx,OFFSET COPYMSG
       mov  ah,40h
       int  21h                        ; Write Copyright Msg.

       Call CommandLine                ; Check command line

       mov  ax,0b800h
       mov  es,ax

       Call InitScroll
       Call SetFont
       CAll Bit8

       cmp  secret,1
       jne  nosecret
       Jmp  InitSecret                 ; Go Secret, and finish
nosecret:

       xor  di,di                      ; Offset de donde va MSG q voy writting
@@mainloop:
       mov  ah,PANN
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax
       mov  PANN,ah

       add  PANN,2
       cmp  PANN,10h                   ; finish panning (10h or 0 the same )
       jb   @@here

       mov  bx,STRADR
       add  bx,50h                     ; one row more
       mov  STRADR,bx
       cmp  bx,LOGOMSGLEN+15*80
       jb   @@here
       jmp  superstart

@@here:
       mov  bx,STRADR
       mov  ah,bh
       mov  al,0ch
       out  dx,ax                      ; Start Address High

       mov  ah,bl
       inc  al
       out  dx,ax                      ; Start Address low

       mov  ax,8018h                   ; Line Compare
       out  dx,ax

       mov  dx,3dah                    ;Wait for end of
       in   al,dx                      ;vertical rescan
       test al,8
       jne  $-3
       in   al,dx                      ;Go to start of rescan
       test al,8
       je   $-3

       jmp  @@mainloop

superstart:
       mov  ah,1
       int  16h
       xor  ah,ah
       int  16h
       cmp  ax,011bh
       je   @@end
       Call CheckKbd
       jmp  superstart

@@end:
       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h


;======================================;
;      SETFONT                         ;
;======================================;
SETFONT PROC
       push es
       mov  ax,@DATA
       mov  es,ax
       mov  bp,offset FONTDATA
       mov  bh,10h                     ; cantidad de bytes x char
       mov  cx,100h                    ; total de chars (256)
       xor  dx,dx                      ; a partir de que char (0).
       xor  bl,bl                      ; map 2.Grabar en 1er tabla
       mov  ax,1110h                   ; Define los carecteres creado por el usuario.
       int  10h
       pop  es
       ret
SETFONT ENDP

;======================================;
;      BIT8                            ;
;======================================;
BIT8   PROC
       mov dx,3cch                     ; Put Chars in 8 bit width
       in al,dx
       and al,11110011b
       mov dx,3c2h
       out dx,al
       cli
       mov dx,3c4h
       mov ax,100h
       out dx,ax
       mov ax,101h
       out dx,ax
       mov ax,300h
       out dx,ax
       sti
       mov ax,1000h
       mov bl,13h
       mov bh,0
       int 10h
       ret
BIT8   ENDP

;======================================;
;      CheckKbd                        ;
;======================================;
CheckKbd    PROC
       mov  es:[di],al
       add  di,2
       ret
CheckKbd    ENDP

;======================================;
;      COMMANDLINE                     ;
;======================================;
COMMANDLINE PROC
       push ds
       mov  ax,es
       mov  ds,ax                      ; DS = PSP segment
       mov  ax,@DATA
       mov  es,ax                      ; ES = DATA

       cld
       mov  si,81h
@@loop:lodsb
       cmp  al,13                      ; si es intro fin
       je   @@fin
       cmp  al,20h                     ; si es SPACE
       je   @@loop
       cmp  al,'/'
       je   opcion
       cmp  al,'-'
       je   opcion

minihelp:
       mov  ax,@DATA
       mov  ds,ax
       mov  bx,2                       ; Strerr
       mov  cx,HELPMSGLEN
       mov  dx,OFFSET HELPMSG
       mov  ah,40h
       int  21h                        ; Write.

       mov  ax,4c01h
       int  21h                        ; END

opcion:
       lodsb

       cmp  al,20h
       je   @@loop                     ; Restart loop
       cmp  al,13
       je   @@fin                      ; Fin

       and  al,0dfh                    ; convierte a todos a mayusculas

       cmp  al,'S'                     ; Secret
       je   secreton
; Agregar funci�n.

       jmp  minihelp                   ; Si no es nada, es minihelp

secreton:
       mov  es:SECRET,1
       jmp  opcion

@@fin:                                 ; End This procedure
       pop  ds
       RET
COMMANDLINE ENDP

;======================================;
;      INITSECRET                      ;
;======================================;
INITSECRET  PROC

       mov  cx,SECRETMSGLEN
       mov  di,80*2*25
       mov  si,offset SECRETMSG
@@loop3:
       mov  al,[si]
       mov  es:[di],al
       inc  si
       add  di,2
       loop @@loop3

       xor  bx,bx
       mov  di,80*2*25+28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       mov  es:[di],al
       add  di,2
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6
       jne  @@loop5

       mov  dx,3d4h
       mov  ax,800eh                   ; Hide cursor
       out  dx,ax

;      SECRET SCROLL                   ;

secretloop:
       mov  ah,PANN
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax
       mov  PANN,ah

       inc  DELAY
       cmp  DELAY,5
       jne  @@here

       mov  DELAY,0
       inc  PANN
       cmp  PANN,10h
       jne  @@here

       mov  bx,STRADR
       add  bx,50h                     ; one row more
       mov  STRADR,bx
       cmp  bx,SECRETMSGLEN+25*80
       jb   @@here
       mov  bx,0050h                   ; To first Row
       mov  STRADR,bx

@@here:
       mov  bx,STRADR
       mov  ah,bh
       mov  al,0ch
       out  dx,ax                      ; Start Address High

       mov  ah,bl
       inc  al
       out  dx,ax                      ; Start Address low

       mov  ax,8018h                   ; Line Compare
       out  dx,ax

       mov  dx,3dah                    ; Wait for vertical retrace
       in   al,dx
       test al,8
       je   $-3
       in   al,dx
       test al,8
       jnz  $-3

       mov  ah,1
       int  16h
       jne  @@here2
       jmp  secretloop

@@here2:
       xor  ah,ah
       int  16h
       cmp  ax,011bh
       je   @@end
       jmp  secretloop

@@end:
       mov  ah,1
       int  16h                        ; Clear keyboard buffer
       mov  ax,3
       int  10h
       mov  ax,4c02h
       int  21h

INITSECRET  ENDP


;======================================;
;      INITSCROLL                      ;
;======================================;
INITSCROLL  PROC
       xor  di,di
       mov  ax,0f20h                   ; Color para escribir
       cld
       mov  cx,80*1
@@loop1:
       mov  es:[di],ax
       add  di,2
       loop @@loop1

       mov  cx,80*25                   ; Clear scroll space
       mov  ax,0700h
@@loop2:
       mov  es:[di],ax
       add  di,2
       loop @@loop2

       mov  cx,LOGOMSGLEN
       mov  di,80*2*25
       mov  si,offset SECRETMSG
@@loop3:
       mov  al,[si]
       mov  es:[di],al
       inc  si
       add  di,2
       loop @@loop3

       xor  bx,bx
       mov  di,80*2*25+28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       mov  es:[di],al
       add  di,2
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6
       jne  @@loop5                    ; Paint me, please

       mov  dx,3dah                    ; Set PEL resetting on line compare
       in   al,dx
       mov  dx,3c0h
       mov  al,30h
       out  dx,al
       mov  al,2ch
       out  dx,al

       mov  dx,3d4h                    ; Set up for line compare split
       mov  ax,0e11h
       out  dx,ax
       mov  ax,0f09h
       out  dx,ax
       mov  ax,1f07h
       out  dx,ax

       mov  ah,2
       xor  bh,bh
       xor  dx,dx
       int  10h                        ; Set Cursor position

       ret
INITSCROLL ENDP


END
