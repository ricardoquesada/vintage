;======================================;
;                                      ;
;      ESTRUCTURA                      ;
;======================================;
;                                      ;
; 0   DB LENGHT OF TOKEN               ;
; 1   DB ASCII                         ;
; ?   DW OFFSET MSG                    ;
; ?+2 DW LENGHT OF MSG                 ;
;                                      ;
; Last record:                         ;
; 0   DB 0                             ;
;                                      ;
;======================================;

TOKENMSG    LABEL BYTE

db     4,'HELP'
ENDTOK_ EQU THIS BYTE
dw     OFFSET T_HELPOFF
NEXTRECORD = $-ENDTOK_

db     5,'ABOUT'
dw     OFFSET T_ABOUTOFF

db     6,'SNDMSG'
dw     OFFSET T_NAOFF

db     7,'CONSOLE'
dw     OFFSET T_NAOFF

db     4,'NICK'
dw     OFFSET T_NAOFF

db     5,'USERS'
dw     OFFSET T_NAOFF

db     0                               ; last token

T_HELPMSG   LABEL BYTE
db'                             VCHAT ON LINE HELP                                 '
db'/ABOUT                              Print info about this program.              '
db'/CONSOLE MSG                        Send a message to the console               '
db'/HELP                               Print this info                             '
db'/NICK NAME                          Change your name                            '
db'/SNDMSG USER                        Send a private message to a user            '
db'/USERS                              Print logged in users.                      '
;db'                                                                                '
T_HELPMSGLEN = $-T_HELPMSG

T_ABOUTMSG  LABEL BYTE
db'                        VChat v0.94 (c) Ricardo Quesada 1995.                   '
db' El proyecto fue comenzado el 7 de julio de 1995, cuando quize saber como eran  '
db' paquetes IPX. Decidí hacer un programa para intercambiar paquetes. Pero debia  '
db'ser distinto a los demas, entonces decidi ponerle scroll fino, y otros secretos '
db'                            que ya van a ir descubriendo.                       '
db'Greetings: UBA Exactas ( en especial a la gente de computación ),a los muchachos'
db' de Chapadmalal ( Ud. sabe quien ), a la gente del SMT, a los fanaticos del ASM '
db'                      y a los grandes lectores de ABOUTS... :-)'
T_ABOUTMSGLEN = $-T_ABOUTMSG

T_NAMSG   LABEL BYTE
db'Sorry, this command is not available yet.'
T_NAMSGLEN  = $-T_NAMSG
