;======================================;
;                                      ;
;              V C h a t               ;
;                by Riq                ;
;                                      ;
;                                      ;
;            Chater for IPX            ;
;                                      ;
;======================================;
;                                      ;
;      Start: 7-7-95                   ;
;      Version: 0.91                   ;
;      Revision:                       ;
;      Finish:                         ;
;      Size:                           ;
;      State: in developing            ;
;======================================;

DOSSEG
MODEL SMALL
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0091h

INCLUDE     KEYB.INC

;======================================; MACROS

;======================================;
;                                      ;
;      STACK                           ;
;                                      ;
;======================================;
STACK 200H


;======================================;
;                                      ;
;      DATA                            ;
;                                      ;
;======================================;
DATASEG

PANN   DB 0
DELAY  DB 0
STRADR DW 0050h                        ; Una fila antes.

TXTMSG LABEL BYTE
db'                                                     '
db'                                      ',9,'                               '
db'                                  ',13,'                                    '
db'                                                                              '
db'                             ',10,'  Ricardo Quesada.                          '
db'                                                     '
db'                                                                                '
db'                                   Versi�n 0.91                                 '
db'                                                                                '
db'                Otro Chat para redes IPX, ( en especial Netware :) )            '
db'                                                                                '
db'          Requeremientos: ganas de chatear, VGA, 286+ ( 386 recomendado ),      '
db'                                   y una red IPX.                               '
db'                                                                                '
db'                                                                                '
db'    Agradecimientos: Pricipalmente a la gente de la facu (UBA-Exactas,Richie,   '
db'     Pablo,Mar�a,Sebas,Nicolai,Nicol�s,a un gran profe:Leandro), a la gente     '
db'         del laburo y al principal BetaTester (la querida Netware v3.12),       '
db'                  a la gente inolvidable: Nacho, Lole, John, Pablito,           '
db'                    a grandes tipos: a Toro y su querido CRISIS BBS,            '
db'                                a los chicos del cole,                          '
db'              al mejor compositor del mundo: Sir ANDREW LLOYD WEBER             '
db'                              ( gracias por la m�sica )                         '
db'                       a una persona que me ayudo en el comienzo:               '
db'                                 Gustavo Zacarias                               '
db'                   y a todos los que hacen Demos / Intros...                    '
db'                                                                                '
db'                                                                                '
db'                                       Tools:                                   '
db'                                                                                '
db'                                Turbo Assembler 3.0                             '
db'                                  Turbo Linker 5.0                              '
db'                                    Q-Edit v3.0                                 '
db'                                  VChar NI v3.33                                '
db'                                                                                '
db'                                                                                '
db'                                       Info:                                    '
db'                                                                                '
db'                          Un doc de Daniel Parnell de IPX                       '
db'                               Ralf Brown Int list v46                          '
db'                                 PC Intern ( a must!)                           '
db'                               Prog. Guide to EGA/VGA                           '
TXTMSGLEN   EQU $-TXTMSG

INCLUDE     FONTDATA.INC               ; Datas de los chars.

;======================================;
;                                      ;
;      CODE                            ;
;                                      ;
;======================================;
CODESEG
       STARTUPCODE

       mov  ax,@data
       mov  ds,ax
       xor  cx,cx

       mov  ax,0b800h
       mov  es,ax
       xor  di,di
       mov  ax,4041h
       cld

       mov  cx,80*1
@@loop1:
       mov  es:[di],ax
       add  di,2
       inc  al
       loop @@loop1

       mov  cx,80*25                   ; Clear scroll space
       mov  ax,0700h
@@loop2:
       mov  es:[di],ax
       add  di,2
       loop @@loop2

       mov  cx,TXTMSGLEN
       mov  di,80*2*25
       mov  si,offset TXTMSG
@@loop3:
       mov  al,[si]
       mov  es:[di],al
       inc  si
       add  di,2
       loop @@loop3


       xor  bx,bx
       mov  di,80*2*25+28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       mov  es:[di],al
       add  di,2
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6
       jne  @@loop5

       mov  dx,3dah                    ; Set PEL resetting on line compare
       in   al,dx
       mov  dx,3c0h
       mov  al,30h
       out  dx,al
       mov  al,2ch
       out  dx,al

       mov  dx,3d4h                    ; Set up for line compare split
       mov  ax,0e11h
       out  dx,ax
       mov  ax,0f09h
       out  dx,ax
       mov  ax,1f07h
       out  dx,ax
       mov  ax,800eh                   ; Hide cursor
       out  dx,ax

       Call SetFont
       CAll Bit8

       xor  di,di                      ; Offset de donde va MSG

@@mainloop:
       mov  ah,PANN
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax
       mov  PANN,ah


       inc  DELAY
       cmp  DELAY,5
       jne  @@here

       mov  DELAY,0
       inc  PANN
       cmp  PANN,10h
       jne  @@here

       mov  bx,STRADR
       add  bx,50h                     ; one row more
       mov  STRADR,bx
       cmp  bx,TXTMSGLEN+25*80
       jb   @@here
       mov  bx,0050h                   ; To first Row
       mov  STRADR,bx

@@here:
       mov  bx,STRADR
       mov  ah,bh
       mov  al,0ch
       out  dx,ax                      ; Start Address High

       mov  ah,bl
       inc  al
       out  dx,ax                      ; Start Address low

       mov  ax,8018h                   ; Line Compare
       out  dx,ax

       mov  dx,3dah                    ;Wait for end of
       in   al,dx                      ;vertical rescan
       test al,8
       jne  $-3
       in   al,dx                      ;Go to start of rescan
       test al,8
       je   $-3

       mov  ah,1
       int  16h
       je   @@mainloop

       xor  ah,ah
       int  16h
       cmp  ax,011bh
       je   @@end
       Call CheckKbd
       jmp  @@mainloop

@@end:
       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h


;======================================;
;      SETFONT                         ;
;======================================;
SETFONT PROC
       push es
       mov  ax,@DATA
       mov  es,ax
       mov  bp,offset FONTDATA
       mov  bh,10h                     ; cantidad de bytes x char
       mov  cx,100h                    ; total de chars (256)
       xor  dx,dx                      ; a partir de que char (0).
       xor  bl,bl                      ; map 2.Grabar en 1er tabla
       mov  ax,1110h                   ; Define los carecteres creado por el usuario.
       int  10h
       pop  es
       ret
SETFONT ENDP

;======================================;
;      BIT8                            ;
;======================================;
BIT8   PROC
       mov dx,3cch                     ; Put Chars in 8 bit width
       in al,dx
       and al,11110011b
       mov dx,3c2h
       out dx,al
       cli
       mov dx,3c4h
       mov ax,100h
       out dx,ax
       mov ax,101h
       out dx,ax
       mov ax,300h
       out dx,ax
       sti
       mov ax,1000h
       mov bl,13h
       mov bh,0
       int 10h
       ret
BIT8   ENDP

;======================================;
;      CheckKbd                        ;
;======================================;
CheckKbd    PROC
       mov  es:[di],al
       add  di,2
       ret
CheckKbd    ENDP

END

