;======================================;
;                                      ;
;              V C h a t               ;
;                by Riq                ;
;                                      ;
;                                      ;
;            Chater for IPX            ;
;                                      ;
;======================================;
;                                      ;
;      Start: 7-7-95                   ;
;      Version: 0.90                   ;
;      Revision:                       ;
;      Finish:                         ;
;      Size:                           ;
;      State: in developing            ;
;======================================;

DOSSEG
MODEL SMALL
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0090h

;======================================; MACROS

;======================================;
;                                      ;
;      STACK                           ;
;                                      ;
;======================================;
STACK 200H


;======================================;
;                                      ;
;      DATA                            ;
;                                      ;
;======================================;
DATASEG

TITO   DB 0

;======================================;
;                                      ;
;      CODE                            ;
;                                      ;
;======================================;
CODESEG
       STARTUPCODE

       mov  ax,@data
       mov  ds,ax
       xor  cx,cx

       mov  ax,0b800h
       mov  es,ax
       xor  di,di
       mov  ax,4041h
       cld

       mov  cx,80*1
@@loop1:
       mov  es:[di],ax
       add  di,2
       inc  al
       loop @@loop1

       mov  cx,80*25
       mov  ax,3041h
@@loop2:
       mov  es:[di],ax
       add  di,2
       loop @@loop2

        mov dx,3dah                     ; Set PEL resetting on line compare
        in al,dx
        mov dx,3c0h
        mov al,30h
        out dx,al
        mov al,2ch
        out dx,al

        mov dx,3d4h                     ; Set up for line compare split
        mov ax,0e11h
        out dx,ax
        mov ax,0f09h
        out dx,ax
        mov ax,1f07h
        out dx,ax


@@mainloop:
       mov  ah,TITO
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax

       inc  TITO

       mov  ax,000ch
       out  dx,ax                      ; Start Address High
       mov  ax,500dh
       out  dx,ax                      ; Start Address low

       mov  ax,8018h                   ; Line Compare
       out  dx,ax

       mov  dx,3dah                    ;Wait for end of
@@here1:
       in   al,dx                      ;vertical rescan
       test al,8
       jne  @@here1
@@here2:
       in   al,dx                      ;Go to start of rescan
       test al,8
       je   @@here2

       in   al,060h
       cmp  al,01h
       jne  @@mainloop

       mov  ax,4c00h
       int  21h

END
