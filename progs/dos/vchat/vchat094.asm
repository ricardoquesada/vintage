;======================================;
;                                      ;
;              V C h a t               ;
;                by Riq                ;
;                                      ;
;                                      ;
;            Chater for IPX            ;
;                                      ;
;======================================;
;                                      ;
;      Start: 7-7-95                   ;
;      This Version: 14-7-95           ;
;      Version: 0.94                   ;
;      Revision:                       ;
;      Finish:                         ;
;      Size:                           ;
;      State: in developing            ;
;======================================;

DOSSEG
MODEL SMALL
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0094h
DOSHEAP     = 1                        ; 0 = internal, 1 = DOS
INCLUDE     KEYB.INC

;======================================; MACROS

;======================================;
;                                      ;
;      STACK                           ;
;                                      ;
;======================================;
STACK 200H


;======================================;
;                                      ;
;      DATA                            ;
;                                      ;
;======================================;
DATASEG

PANN   DB 0                            ; Used in main Scroll
DELAY  DB 0                            ; Used in main scroll
STRADR DW 0050h*6                      ; Used in main scroll

CRSR_ROW DB 0                          ; Used in main scroll : Coord
CRSR_FIL DB 1                          ; Used in main scroll : Coord

SEGMEM1 DW 0                           ; Used in processmsg
BUFFER1 DB 40 DUP (0)                  ; Used in processmsg -> token

SCREENOFF DW 00A0h*45                  ; Used in PrintMsg
VECES  DB 0                            ; Used in PrintMsg

SECRET DB 0                            ; Used in command line


BARMSG LABEL BYTE
db'Name: RICARDO    � Users: 02   � Netware ver: 3.12  � Secret: OFF  � VChat 0.94 '
BARMSGLEN = $-BARMSG

SECRETMSG LABEL BYTE
db'                                                     '
db'                                      ',9,'                               '
db'                                  ',13,'                                    '
db'                                                                              '
db'                             ',10,'  Ricardo Quesada.                          '
db'                                                     '
db'                                                                                '
db'                                 Beta Version 0.94                              '
LOGOMSGLEN  = $-SECRETMSG
db'                                                                                '
db'                Otro Chat para redes IPX, ( en especial Netware :) )            '
db'                                                                                '
db'          Requeremientos: ganas de chatear, VGA, 286+ ( 386 recomendado ),      '
db'                                   y una red IPX.                               '
db'                                                                                '
db'                                                                                '
db'    Agradecimientos: Pricipalmente a la gente de la facu (UBA-Exactas,Richie,   '
db'     Pablo,Mar�a,Sebas,Nicolai,Nicol�s,a un gran profe:Leandro), a la gente     '
db'         del laburo y al principal BetaTester (la querida Netware v3.12),       '
db'                  a la gente inolvidable: Nacho, Lole, John, Pablito,           '
db'                    a grandes tipos: a Toro y su querido CRISIS BBS,            '
db'                                a los chicos del cole,                          '
db'              al mejor compositor del mundo: Sir ANDREW LLOYD WEBER             '
db'                              ( gracias por la m�sica )                         '
db'                       a una persona que me ayudo en el comienzo:               '
db'                                 Gustavo Zacarias                               '
db'                   y a todos los que hacen Demos / Intros...                    '
db'                                                                                '
db'                                                                                '
db'                                       Tools:                                   '
db'                                                                                '
db'                                Turbo Assembler 3.0                             '
db'                                  Turbo Linker 5.0                              '
db'                                    Q-Edit v3.0                                 '
db'                                  VChar NI v3.33                                '
db'                                                                                '
db'                                                                                '
db'                                       Info:                                    '
db'                                                                                '
db'                          Un doc de Daniel Parnell de IPX                       '
db'                               Ralf Brown Int list v46                          '
db'                                 PC Intern ( a must!)                           '
db'                               Prog. Guide to EGA/VGA                           '
SECRETMSGLEN   EQU $-SECRETMSG

COPYMSG label byte
db 13,10
db'VChat v0.94 (c) Ricardo Quesada 1995.',13,10
db'Beta copy. For personal use only. Not for sale.',13,10,13,10
COPYMSGLEN EQU $-COPYMSG

HELPMSG label byte
db 13,10
db'Use:       VChat',13,10
db'Purpose:   Utility that allow users to chat in an IPX network.',13,10
db'Greetings: UBA Exactas, Chapadmamlal pals, SMT guys, Commodore 128 boys.',13,10
db'Credits:   Idea & programming, by Riq.',13,10
db'Secret:    Try VCHAT /S',13,10
db 13,10
db'Find me in:',13,10
db'            INTERNET                FIDONET                ADN',13,10
db'       rquesada@dc.uba.ar         4:900/309.3          52:800/101.5',13,10
db 13,10
HELPMSGLEN  = $-HELPMSG

INCLUDE     FONTDATA.INC               ; Datas de los chars.
INCLUDE     TOKEN.INC                  ; Comandos.

;======================================;
;                                      ;
;      CODE                            ;
;                                      ;
;======================================;
CODESEG
       STARTUPCODE

IF DOSHEAP
                                       ;Release all memory except the amount currently being used
                                       ;End of stack is end of non-heap portion of program
       mov  bx,sp
       add  bx,15                      ;convert SP into paragraphs
       shr  bx,4
       mov  ax,ss                      ;calculate size of program using ES PSP address
       add  bx,ax
       mov  ax,es
       sub  bx,ax
       mov  ah,4ah                     ;resize memory block with PSP
       int  21h                        ;address in ES
ENDIF

       mov  ax,@data
       mov  ds,ax

       mov  bx,2                       ; Std Error
       mov  cx,COPYMSGLEN
       mov  dx,OFFSET COPYMSG
       mov  ah,40h
       int  21h                        ; Write Copyright Msg.

       Call CommandLine                ; Check command line

       mov  ax,0b800h
       mov  es,ax

       Call InitScroll
       Call SetFont
       CAll Bit8

       cmp  secret,1
       jne  nosecret
       Jmp  InitSecret                 ; Go Secret, and finish
nosecret:
       mov  ah,48h
       mov  bx,1024/16                 ; 1k to allocate
       int  21h
       mov  SEGMEM1,ax                 ; save for later

       mov  di,0050h*2                 ; Offset de donde va MSG q voy writting
@@mainloop:
       mov  ah,PANN
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax
       mov  PANN,ah

       add  PANN,2
       cmp  PANN,10h                   ; finish panning (10h or 0 the same )
       jb   @@here

       mov  bx,STRADR
       add  bx,50h                     ; one row more
       mov  STRADR,bx
       cmp  bx,LOGOMSGLEN+15*80
       jb   @@here
       jmp  superstart

@@here:
       mov  bx,STRADR
       mov  ah,bh
       mov  al,0ch
       out  dx,ax                      ; Start Address High

       mov  ah,bl
       inc  al
       out  dx,ax                      ; Start Address low

       mov  ax,6018h                   ; Line Compare
       out  dx,ax

       mov  dx,3dah                    ;Wait for end of
       in   al,dx                      ;vertical rescan
       test al,8
       jne  $-3
       in   al,dx                      ;Go to start of rescan
       test al,8
       je   $-3

       jmp  @@mainloop

superstart:
       mov  ah,1
       int  16h
       xor  ah,ah
       int  16h
       cmp  ax,011bh
       je   @@end
       Call CheckKbd
       jmp  superstart

@@end:
       mov  ah,49h
       mov  bx,SEGMEM1
       mov  es,bx
       int  21h                        ; Free allocated memory.

       mov  ax,3
       int  10h
       mov  ax,4c00h
       int  21h


;======================================;
;      SETFONT                         ;
;======================================;
SETFONT PROC
       push es
       mov  ax,@DATA
       mov  es,ax
       mov  bp,offset FONTDATA
       mov  bh,10h                     ; cantidad de bytes x char
       mov  cx,100h                    ; total de chars (256)
       xor  dx,dx                      ; a partir de que char (0).
       xor  bl,bl                      ; map 2.Grabar en 1er tabla
       mov  ax,1110h                   ; Define los carecteres creado por el usuario.
       int  10h
       pop  es
       ret
SETFONT ENDP

;======================================;
;      BIT8                            ;
;======================================;
BIT8   PROC
       mov dx,3cch                     ; Put Chars in 8 bit width
       in al,dx
       and al,11110011b
       mov dx,3c2h
       out dx,al
       cli
       mov dx,3c4h
       mov ax,100h
       out dx,ax
       mov ax,101h
       out dx,ax
       mov ax,300h
       out dx,ax
       sti
       mov ax,1000h
       mov bl,13h
       mov bh,0
       int 10h
       ret
BIT8   ENDP

;======================================;
;      CheckKbd                        ;
;======================================;
CheckKbd    PROC
       cmp  ax,BACKS
       je   @@backs
       cmp  ax,INTRO
       je   @@intro
       cmp  al,0
       je   @@set_crsr
       mov  es:[di],al
       add  di,2
       inc  CRSR_ROW

@@set_crsr:
       mov  dl,CRSR_ROW
       mov  dh,CRSR_FIL
       mov  ah,2
       xor  bh,bh
       int  10h                        ; Set Cursor Position
       ret

@@backs:
       cmp  CRSR_ROW,0
       jne  @@backsok
       add  di,2                       ; Para que se cancele.
       inc  CRSR_ROW                   ; Para que se cancele.
@@backsok:
       mov  byte ptr es:[di-2],20h     ; Backspace.
       dec  CRSR_ROW
       sub  di,2
       jmp  @@set_crsr

@@intro:
       cmp  CRSR_ROW,0
       je   @@nointro
       mov  byte ptr es:[di],0

       call printmsg                   ; Print Msg
       call processmsg

       call scrolling                  ; Scroll screen

       mov  CRSR_ROW,0                 ; Borrar pantalla donde se escribe
       mov  si,0a0h                    ; e inicializar sus valores.
       mov  di,si
       mov  al,20h
       cld
       mov  cx,0a0h * 2
@@loopc:
       mov  es:[si],al
       add  si,2
       loop @@loopc
       jmp  @@set_crsr
@@nointro:
       ret

CheckKbd    ENDP

;======================================;
;      SCROLLING                       ;
;======================================;
SCROLLING   PROC
       push ds
       mov  ax,@DATA
       mov  ds,ax

@@mainloop:
       mov  ah,PANN
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax
       mov  PANN,ah

       add  PANN,2
       cmp  PANN,10h                   ; finish panning (10h or 0 the same )
       jb   @@here

       add  STRADR,50h

       dec  VECES
       cmp  VECES,0
       je   @@endscroll                ; Tantas linas a procesar.

@@here:
       mov  bx,STRADR
       mov  ah,bh
       mov  al,0ch
       out  dx,ax                      ; Start Address High

       mov  ah,bl
       inc  al
       out  dx,ax                      ; Start Address low

       mov  ax,6018h                   ; Line Compare
       out  dx,ax

       mov  dx,3dah                    ;Wait for end of
       in   al,dx                      ;vertical rescan
       test al,8
       jne  $-3
       in   al,dx                      ;Go to start of rescan
       test al,8
       je   $-3
       jmp  @@mainloop

@@endscroll:
       pop  ds
       ret
SCROLLING   ENDP

;======================================;
;      PRINTMSG                        ;
;======================================;
PRINTMSG    PROC
       pusha
       push ds
       push es

       mov  di,SCREENOFF               ; donde van las letras
       mov  ax,SEGMEM1                 ; <��������������������������Ŀ
                                       ;                             �
       push es                         ;                             �
       pop  ds                         ;                             �
                                       ;                             �
       mov  es,ax                      ; ES = allocated memory  <�����


       mov  si,0050h*2

       xor  bx,bx                      ; offset del segement alocado.
       xor  cx,cx                      ; letras a contar
@@loop:
       mov  al,[si]
       or   al,al
       je   @@end
       mov  es:[bx],al
       mov  byte ptr es:[bx+1],0       ; Por la dudas que termine en el paso anterior

       mov  [di],al
       inc  bx                         ; offset del buffer alocado.
       add  si,2
       add  di,2
       inc  cx                         ; Cantidad de letras procesadas.
       jmp  @@loop

@@end:
       pop  es                         ; = b800
       pop  ds                         ; = DATA

       mov  al,1                       ; Default una vez.
       dec  cx                         ; una menos a precesar.
       or   cx,cx
       je   @@unavez                   ; evita division by zero
       xchg ax,cx
       mov  cl,80
       div  cl
       inc  al
@@unavez:
       add  VECES,al                   ; Cantidad de lineas a scrollear
                                       ; "ADD" por si habia acumulado alguno.

       mov  bx,00a0h                   ; 160d
       xor  dx,dx
       mov  ax,di
       div  bx                         ; dx:ax/160
       or   dx,dx
       je   @@noremainder
       sub  bx,dx                      ;
       add  di,bx                      ; di+remainder
@@noremainder:
       mov  SCREENOFF,di               ; para la proxima vez.

       popa
       ret
PRINTMSG    ENDP

;======================================;
;      PROCESSMSG                      ;
;======================================;
PROCESSMSG  PROC
       push ds
       mov  ax,SEGMEM1
       mov  ds,ax                      ; Allocated memory.
       xor  si,si
       cld

@@loop:
       lodsb
       cmp  al,'/'
       je   @@token
       cmp  al,20h
       je   @@loop
       or   al,al
       je   @@fin

       jne  @@fin                      ; si no es nada de nada fin

@@token:
       call checktoken

@@fin:
       pop  ds
       ret
PROCESSMSG  ENDP

;======================================;
;      CHECKTOKEN                      ;
;======================================;
CHECKTOKEN  PROC
       push es
       push di

       mov  ax,@DATA
       mov  es,ax

       xor  bx,bx                      ; Counter < 80
       mov  di,OFFSET BUFFER1          ; offset de buffer1
@@loop:
       lodsb

       and  al,0dfh                    ; convierte a todos a mayusculas
       mov  es:[di],al                 ; BUFFER1[DI] va el token.
       inc  di

       cmp  al,20h                     ;
       je   @@endoftoken
       or   al,al                      ;
       je   @@endoftoken

       inc  bx
       cmp  bx,80
       jae  @@endoftoken               ; syntax error.
       jmp  @@loop

@@endoftoken:
       push ds
       push si                         ;

       push es
       pop  ds                         ; DS = DATA

       mov  di,OFFSET BUFFER1          ;
       mov  si,OFFSET TOKENMSG
       xor  cx,cx                      ; lenght

@@nexttoken:
       mov  cl,[si]                    ; Lenght of input data
       or   cl,cl                      ; Last Record ?
       je   @@finish

       push si                         ; to check las si.
       push di                         ; must do this.

       inc  si                         ; First letter.
rep    cmpsb
       jne  @@notequal

       mov  bx,[si+0]                  ; OFFSET MSG
       call bx                         ; Call function

@@notequal:
       xor  cx,cx
       pop  di                         ; I wont use now, but later
       pop  si
       mov  cl,[si]
       add  si,cx
       inc  si                         ; Fix a bug this.
       add  si,NEXTRECORD              ; Now SI, points to next token.
       jmp  @@nexttoken                ; So, check next token.

@@finish:
       pop  si
       pop  ds

       pop  di
       pop  es
       ret
CHECKTOKEN  ENDP

INCLUDE     FUNC.INC

;======================================;
;      COPY2SCREEN                     ;
;======================================;
;      si -> offset msg                ;
;      cx -> lenght of msg             ;
;======================================;
COPY2SCREEN PROC
       pusha
       push ds
       push es

       push cx

       cld
       mov  ax,@DATA
       mov  ds,ax                      ; DS:SI
       mov  ax,0b800h
       mov  es,ax
       mov  di,SCREENOFF               ; ES:DI
@@loop:
       movsb
       inc  di
       loop @@loop
       pop  cx                         ; Cantidad de letras movidas.

       mov  al,1                       ; Default una vez.
       dec  cx                         ; una menos a procesar.
       or   cx,cx
       je   @@unavez                   ; evita division by zero
       xchg ax,cx
       mov  cl,80
       div  cl
       inc  al
@@unavez:
       add  VECES,al                   ; Cantidad de lineas a scrollear

       mov  bx,00a0h                   ; 160d
       xor  dx,dx
       mov  ax,di
       div  bx                         ; dx:ax/160
       or   dx,dx
       je   @@noremainder
       sub  bx,dx                      ;
       add  di,bx                      ; di+remainder
@@noremainder:
       mov  SCREENOFF,di               ; para la proxima vez.

       pop  es
       pop  ds
       popa
       ret
COPY2SCREEN ENDP

;======================================;
;      COMMANDLINE                     ;
;======================================;
COMMANDLINE PROC
       push ds
       mov  ax,es
       mov  ds,ax                      ; DS = PSP segment
       mov  ax,@DATA
       mov  es,ax                      ; ES = DATA

       cld
       mov  si,81h
@@loop:lodsb
       cmp  al,13                      ; si es intro fin
       je   @@fin
       cmp  al,20h                     ; si es SPACE
       je   @@loop
       cmp  al,'/'
       je   opcion
       cmp  al,'-'
       je   opcion

minihelp:
       mov  ax,@DATA
       mov  ds,ax
       mov  bx,2                       ; Strerr
       mov  cx,HELPMSGLEN
       mov  dx,OFFSET HELPMSG
       mov  ah,40h
       int  21h                        ; Write.

       mov  ax,4c01h
       int  21h                        ; END

opcion:
       lodsb

       cmp  al,20h
       je   @@loop                     ; Restart loop
       cmp  al,13
       je   @@fin                      ; Fin

       and  al,0dfh                    ; convierte a todos a mayusculas

       cmp  al,'S'                     ; Secret
       je   secreton
; Agregar funci�n.

       jmp  minihelp                   ; Si no es nada, es minihelp

secreton:
       mov  es:SECRET,1
       jmp  opcion

@@fin:                                 ; End This procedure
       pop  ds
       RET
COMMANDLINE ENDP

;======================================;
;      INITSECRET                      ;
;======================================;
INITSECRET  PROC

       mov  cx,SECRETMSGLEN
       mov  di,80*2*30
       mov  si,offset SECRETMSG
@@loop3:
       mov  al,[si]
       mov  es:[di],al
       inc  si
       add  di,2
       loop @@loop3

       xor  bx,bx
       mov  di,80*2*30+28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       mov  es:[di],al
       add  di,2
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6
       jne  @@loop5

       mov  dx,3d4h
       mov  ax,800eh                   ; Hide cursor
       out  dx,ax

;      SECRET SCROLL                   ;

secretloop:
       mov  ah,PANN
       and  ah,0fh
       mov  al,8                       ; Scroll vertical
       mov  dx,3d4h
       out  dx,ax
       mov  PANN,ah

       inc  DELAY
       cmp  DELAY,5
       jne  @@here

       mov  DELAY,0
       inc  PANN
       cmp  PANN,10h
       jne  @@here

       mov  bx,STRADR
       add  bx,50h                     ; one row more
       mov  STRADR,bx
       cmp  bx,SECRETMSGLEN+25*80
       jb   @@here
       mov  bx,0050h*6                 ; To first Row (sixth)
       mov  STRADR,bx

@@here:
       mov  bx,STRADR
       mov  ah,bh
       mov  al,0ch
       out  dx,ax                      ; Start Address High

       mov  ah,bl
       inc  al
       out  dx,ax                      ; Start Address low

       mov  ax,9018h                   ; Line Compare (90=no line)
       out  dx,ax

       mov  dx,3dah                    ; Vertical Retrace
       in   al,dx
       test al,8
       je   $-3
       in   al,dx
       test al,8
       jne  $-3

       mov  ah,1
       int  16h
       jne  @@here2
       jmp  secretloop

@@here2:
       xor  ah,ah
       int  16h
       cmp  ax,011bh
       je   @@end
       jmp  secretloop

@@end:
       mov  ah,1
       int  16h                        ; Clear keyboard buffer
       mov  ax,3
       int  10h
       mov  ax,4c02h
       int  21h

INITSECRET  ENDP


;======================================;
;      INITSCROLL                      ;
;======================================;
INITSCROLL  PROC
       xor  di,di
       mov  cx,80*50                   ; Clear scroll space
       mov  ax,0700h
@@loop2:
       mov  es:[di],ax
       add  di,2
       loop @@loop2

       xor  di,di
       mov  ax,7f20h                   ; Color para escribir
       cld
       mov  cx,80*1
@@loop1:
       mov  es:[di],ax
       add  di,2
       loop @@loop1
       mov  ax,3020h                   ; Color de barra de info.
       mov  cx,80*2
@@loopa:
       mov  es:[di],ax
       add  di,2
       loop @@loopa

       xor  di,di
       mov  si,OFFSET BARMSG
       mov  cx,BARMSGLEN
@@loopb:
       mov  al,[si]
       mov  es:[di],al
       inc  si
       add  di,2
       loop @@loopb

       mov  cx,LOGOMSGLEN
       mov  di,80*2*30
       mov  si,offset SECRETMSG
@@loop3:
       mov  al,[si]
       mov  es:[di],al
       inc  si
       add  di,2
       loop @@loop3

       xor  bx,bx
       mov  di,80*2*30+28*2+1
@@loop5:
       mov  cx,27
       mov  al,4fh
@@loop4:
       mov  es:[di],al
       add  di,2
       loop @@loop4
       add  di,23*2+30*2
       inc  bx
       cmp  bx,6
       jne  @@loop5                    ; Paint me, please

       mov  dx,3dah                    ; Set PEL resetting on line compare
       in   al,dx
       mov  dx,3c0h
       mov  al,30h
       out  dx,al
       mov  al,2ch
       out  dx,al

       mov  dx,3d4h                    ; Set up for line compare split
       mov  ax,0e11h
       out  dx,ax
       mov  ax,0f09h
       out  dx,ax
       mov  ax,1f07h
       out  dx,ax

       mov  ah,2
       xor  bh,bh
       mov  dh,CRSR_FIL                ; dh=file
       mov  dl,CRSR_ROW                ; dl=row
       int  10h                        ; Set Cursor position

       ret
INITSCROLL ENDP


END
