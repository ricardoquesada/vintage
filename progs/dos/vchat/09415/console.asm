;======================================;
;                                      ;
;              V C h a t               ;
;                by Riq                ;
;                                      ;
;                                      ;
;            Chater for IPX            ;
;                                      ;
;======================================;
;                                      ;
;      Start: 7-7-95                   ;
;      Version: 0.90                   ;
;      Revision:                       ;
;      Finish:                         ;
;      Size:                           ;
;      State: in developing            ;
;======================================;

DOSSEG
MODEL SMALL
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0090h

;======================================; MACROS

;======================================;
;                                      ;
;      STACK                           ;
;                                      ;
;======================================;
STACK 200H


;======================================;
;                                      ;
;      DATA                            ;
;                                      ;
;======================================;
DATASEG


reqbuf dw 23
       db 9                            ; subf
       db 20
       db 'HOLA COMO TE VA, Riq was here'

repbuf db 50 dup(0)

;======================================;
;                                      ;
;      CODE                            ;
;                                      ;
;======================================;
CODESEG
       STARTUPCODE

       mov  ax,@data
       mov  ds,ax
       mov  es,ax

       mov  ah,0e1h
       mov  si,offset reqbuf           ; Request Buffer
       mov  di,offset repbuf           ; Reply Buffer
       int  21h

       mov  ax,4c00h
       int  21h

END

