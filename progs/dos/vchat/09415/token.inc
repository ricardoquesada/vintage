;======================================;
;                                      ;
;      ESTRUCTURA                      ;
;======================================;
;                                      ;
; 0   DB LENGHT OF TOKEN               ;
; 1   DB ASCII                         ;
; ?   DW OFFSET EXE                    ;
;                                      ;
; Last record:                         ;
; 0   DB 0                             ;
;                                      ;
;======================================;

TOKENMSG    LABEL BYTE

db     4,'HELP'
ENDTOK_ EQU THIS BYTE
dw     OFFSET T_HELPOFF
NEXTRECORD = $-ENDTOK_

db     5,'ABOUT'
dw     OFFSET T_ABOUTOFF

db     6,'SNDMSG'
dw     OFFSET T_NAOFF

db     7,'CONSOLE'
dw     OFFSET T_CONSOLEOFF

db     4,'NICK'
dw     OFFSET T_NICKOFF

db     5,'USERS'
dw     OFFSET T_NAOFF

db     5,'CLEAR'
dw     OFFSET T_CLEAROFF

db     4,'QUIT'
dw     OFFSET T_NAOFF

db     4,'SAVE'
dw     OFFSET T_NAOFF

db     3,'VER'
dw     OFFSET T_VEROFF

db     3,'WHO'
dw     OFFSET T_NAOFF

db     0                               ; last token

T_HELPMSG   LABEL BYTE
db'                             VCHAT ON LINE HELP                                 '
db'type...                             to...                                       '
db'/ABOUT                              Print info about this program.              '
db'/CLEAR                              Clear screen & buffer.                      '
db'/CONSOLE MSG                        Send a message to the console.              '
db'/HELP                               Print this info.                            '
db'/MSG USER MSG                       Send a private message to a user.           '
db'/NICK NICKNAME                      Change your nickname.                       '
db'/QUIT                               Quit to D.O.S.                              '
db'/SAVE FILE                          Save buffer to FILE.                        '
db'/USERS                              Print logged in users.                      '
db'/VER                                Print VCHAT version.                        '
db'/WHO NICKNAME                       Print real name of user                     '
;db'                                                                                '
T_HELPMSGLEN = $-T_HELPMSG

T_VERMSG    LABEL BYTE
db'VCHAT BETA VERSION 0.9500 - IF YOU ARE INTERESTED IN BEING A TESTER, CONTACT ME.'
T_VERMSGLEN = $-T_VERMSG

T_NAMSG   LABEL BYTE
db'Sorry, this command is not available yet.'
T_NAMSGLEN  = $-T_NAMSG

T_SERRORMSG   LABEL BYTE
db'*** SYNTAX ERROR: Type /HELP for help.'
T_SERRORMSGLEN  = $-T_SERRORMSG
