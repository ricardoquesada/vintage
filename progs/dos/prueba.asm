;name: modelo.com
;verion: 1.00
;start  date: 15/11/93
;finish date:
;author: Ricardo Quesada

include mmbios.mac

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:
getchar

ruti    endp
code    ends
        end ruti
