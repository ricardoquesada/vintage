;VWR V1.0
;HECHO POR RICARDO QUESADA EL DIA 6.5.93
;

CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

START:
        JMP INTRO

NEW21H:
        JMP SHORT MAIN21_
        DB'VW'
MAIN21_:
        CMP AH,4BH
        JE EXEC_

        JMP CS:[OLD21H]                 ;se puede cambiar por int 21 (15/11/93)
        
EXEC_:
        PUSH ES
        PUSH SI
        PUSH DI
        PUSH AX

        MOV AX,CS:MODE_C
        MOV ES,AX
        MOV DI,0                        ;xor di,di
        MOV SI,DX

LOOP_:
        MOV AL,DS:[SI]
        OR AL,AL
        JE FIN_
        MOV ES:[DI],AL
        INC SI
        ADD DI,2
        JMP LOOP_                       ;jmp short loop_

FIN_:
        POP AX
        POP DI
        POP SI
        POP ES

        JMP CS:[OLD21H]

;**************************NEW 2FH**************************

NEW2FH:
        JMP SHORT ACA_1
        DB'VW'
ACA_1:
        CMP AH,0DEH
        JE ACA_2
FINA_:   JMP CS:[OLD2FH]

ACA_2:  CMP AL,30H
        JNE ACA_3
        XOR AL,AL
        IRET

ACA_3:  CMP AL,31H
        JNE FINA_
        MOV AX,0001H
        XOR BX,BX
        IRET

;************************************************

MODE_C  DW 0
OLD2FH  DD 0
OLD21H  DD 0


;***********************************************

INTRO:
        CLD
        MOV SI,81H
LOOP1_:
        LODSB
        CMP AL,13       ;CR
        JE FINAL_
        CMP AL,'/'
        JE OPTIONS
        CMP AL,20H
        JE LOOP1_
        JMP ERR_OPT

OPTIONS:
        LODSB
        CMP AL,13
        JE FINAL_
        CMP AL,20H
        JE OPTIONS
        AND AL,11011111B
        CMP AL,'V'
        JE VERSION_
        CMP AL,'H'
        JE HELP_

ERR_OPT:
        PUSH CS
        POP DS
        MOV DX,OFFSET MSG_ERR_OPT
        MOV AH,9
        INT 21H

HELP_:
        PUSH CS
        POP DS
        MOV DX,OFFSET MSG_HLP
        MOV AH,9
        INT 21H

        MOV AH,4CH
        INT 21H

VERSION_:
        MOV AX,0DE31H
        INT 2FH

        PUSH CS
        POP DS

        CMP AH,0DEH
        JE ERR_VER

        ADD AH,30H
        ADD AL,30H
        ADD BL,30H
        ADD BH,30H
        MOV [MSG_NUM],AH
        MOV [MSG_NUM+1],AL
        MOV [MSG_NUM+3],BH
        MOV [MSG_NUM+4],BL
        MOV DX,OFFSET MSG_VER

        JMP SHORT S_GUIR

        MOV AH,9
        INT 21H

ERR_VER:
        MOV DX,OFFSET MSG_ERR_VER

S_GUIR:
        MOV AH,9
        INT 21H

        MOV AH,4CH
        INT 21H

FINAL_:

        MOV CS:MODE_C,0B800H
        MOV AX,0040H
        MOV ES,AX
        MOV AX,ES:[10H]
        AND AX,30H
        CMP AX,30H
        JNE NOT_MONO
        MOV CS:MODE_C,0B000H

NOT_MONO:
        MOV AX,0DE30H
        INT 2FH
        OR AL,AL
        JNE INSTALL

UNLOAD:
        MOV AX,3521H
        INT 21H
        CMP ES:[BX+2],'WV'
        JNE UNLOAD_ERR

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'WV'
        JE UNLOAD_OK

UNLOAD_ERR:
        PUSH CS
        POP DS
        MOV DX,OFFSET MSG_ERR_UNL
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

UNLOAD_OK:
        MOV SI,OFFSET OLD21H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2521H
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        PUSH CS
        POP DS

        MOV DX,OFFSET MSG_UNL
        MOV AH,9
        INT 21H

        MOV AH,4CH
        INT 21H

INSTALL:
        MOV AX,3521H            ;OLD 21H
        INT 21H
        MOV SI,OFFSET OLD21H
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS
        POP DS

        MOV DX,OFFSET NEW21H    ;NEW 21H
        MOV AX,2521H
        INT 21H

        MOV AX,352FH            ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH    ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV DX,OFFSET MSG_INT   ;DISPLAY MSG_INT
        MOV AH,9
        INT 21H

        MOV DX,OFFSET INTRO
        INT 27H

MSG_INT DB 13,10,'VWR v1.0 - (C) 1993 by Ricardo Quesada.',13,10
DB'Call again to unload it.',13,10
db'Type VWR /h to view the HELP SCREEN.',13,10,'$'

MSG_HLP DB'VWR v1.0 - HELP SCREEN.',13,10,13,10
db'VWR [option]',13,10
db'    /h  displays this screen.',13,10
db'    /v  version number of loaded VWR.',13,10,13,10
db'VWR displays the program with its path.',13,10,13,10
db'Should not be sold. For personal use only.',13,10
db'(C) 1993 by Ricardo Quesada.',13,10,'$'

MSG_VER DB'Version number: '
MSG_NUM DB'00.00',13,10,'$'
MSG_ERR_VER DB'****ERROR: VWR not loaded.',13,10,'$'
MSG_ERR_OPT DB'****ERROR: Invalid option.',13,10,'$'
MSG_ERR_UNL DB'****ERROR: unable to unload.',13,10,'$'
MSG_UNL DB'VWR unloaded.',13,10,'$'
RUTI    ENDP
CODE    ENDS
        END RUTI

