;======================================;
;                                      ;
;             Antiencrypt              ;
;                by Riq                ;
;                                      ;
;                                      ;
;       Viewer of Intruder 0.95+       ;
;                                      ;
;======================================;
;                                      ;
;      Start: 1-7-95                   ;
;      Version: 0.95                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

MODEL TINY
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0095h
XORVALUE    = 21h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

       lea  dx,BUFFER2
       mov  ah,3Fh                     ; Aca bx=0, no hace falta ponerlo
       mov  cx,0FFFFh                  ; Lee el archivo
       int  21h                        ;

       push ax                         ; leght
       xchg ax,cx                      ; cx=lenght (1 byte ocupa esta instruccion)
       mov  di,dx                      ; offset
@@here2:
       xor  byte ptr[di],XORVALUE
       inc  di
       loop @@here2

       mov  ah,40h                     ; Write
       inc  bx                         ; Stdout (BX=1)
       pop  cx                         ; Lenght
       int  21h

       ret
BUFFER2     EQU THIS BYTE

ENDP
END
