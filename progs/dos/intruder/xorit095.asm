;======================================;
;                                      ;
;                XOR IT                ;
;                by Riq                ;
;                                      ;
;                                      ;
;         Para xorear archivos         ;
;                                      ;
;======================================;
;                                      ;
;      Start: 1-7-95                   ;
;      Version: 0.95                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

MODEL TINY
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0095h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

       lea  dx,BUFFER2
       mov  ah,3Fh                     ; bx=0 no hace falta ponerlo
       mov  cx,0FFFFh                  ; Lee el archivo
       int  21h                        ;

       push ax                         ; leght
       xchg ax,cx                      ; cx=lenght (1 byte ocupa esta instruccion)
       mov  di,dx                      ; offset
       mov  al,[di]
       inc  di                         ; sig byte es el primero
       dec  cx                         ; uno menos a xorear
@@here2:
       xor  byte ptr[di],al
       inc  di
       loop @@here2

       mov  ah,40h                     ; Write
       inc  bx                         ; Stdout (BX=1)
       pop  cx                         ; Lenght
       int  21h

       ret
BUFFER2     EQU THIS BYTE

ENDP
END
