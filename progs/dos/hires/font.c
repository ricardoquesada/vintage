
 #include <graphics.h>
 #include <stdlib.h>
 #include <stdio.h>
 #include <conio.h>

 int main(void)
 {
	/* request auto detection */
	int gdriver = DETECT, gmode, errorcode;
	int midx, midy;


	registerbgifont(triplex_font);
	registerbgidriver(EGAVGA_driver);


	/* initialize graphics and local variables */
	initgraph(&gdriver, &gmode, "");

	/* read result of initialization */
	errorcode = graphresult();
	if (errorcode != grOk)  /* an error occurred */
	{
	   printf("Graphics error: %s\n", grapherrormsg(errorcode));
	   printf("Press any key to halt:");
	   getch();
	   exit(1); /* terminate with an error code */
	}

	midx = getmaxx() / 2;
	midy = getmaxy() / 2;

	/* select the registered font */
	settextstyle(TRIPLEX_FONT, HORIZ_DIR, 6);

	/* output some text */
	settextjustify(CENTER_TEXT, CENTER_TEXT);
	outtextxy(midx, midy, "The TRIPLEX FONT");

	/* clean up */
	getch();
	closegraph();
	return 0;
 }

