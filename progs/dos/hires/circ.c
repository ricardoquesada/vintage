
#include <graphics.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

int main(void)
{
   /* request autodect */
   int gdriver = DETECT, gmode, errorcode;
   int midx, midy;
   int x,y;
   double i;
   int color=0;
   /* initialize graphics and local variables */

   if( registerbgidriver(EGAVGA_driver)<0)
   {
		printf("Error!\n");
		exit(1);
   }

   initgraph(&gdriver, &gmode, "");

   /* read result of initialization */
   errorcode = graphresult();
   if (errorcode != grOk)  /* an error occurred */
   {
	  printf("Graphics error: %s\n", grapherrormsg(errorcode));
	  printf("Press any key to halt:");
	  getch();
	  exit(1); /* terminate with an error code */
   }

   midx = getmaxx() / 2;
   midy = getmaxy() / 2;

   do{
	 for(i=0;i<2*M_PI;i=i+0.01)
	 {
		(x) = cos(i) * 100;
		(y) = sin(i) * 100;
		putpixel(x+midx, y+midy, getmaxcolor() );
		(x) = cos(i-0.4) * 100;
		(y) = sin(i-0.4) * 100;
		putpixel(x+midx, y+midy, color );
	 }
   color++;
   } while( !kbhit() );

   /* clean up */
   closegraph();
   return 0;
}

