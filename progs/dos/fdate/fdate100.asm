Comment|
File:    FDate
Version: 1.00
Author:  Ricardo QUESADA
Date:    20/11/94
Purpose: Le pone fecha a los archivos.
|

%OUT Compilando FDate v1.00  - Freeware

TRUE  = 1
FALSE = 0

CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

start:
        JMP init
db'FDate v1.00 (c) Ricardo Quesada 1994.',13,10
xorvalue db 0

;== PROG =======================;
init:
    MOV DX,OFFSET INTRO_MSG
    MOV AH,9
    INT 21H

    cld                     ;command line detection
    mov si,81h
alop:
    lodsb
    cmp al,13               ;es CR,si then display help
    je cfecha
    cmp al,20h
    je alop

    cmp al,'-'
    je opcion2
    cmp al,'/'
    je opcion2
    jmp newname

opcion2:
;    lodsb
;    and al,11011111b
;    cmp al,'A'
;    je antivirus               ;
;    cmp al,'T'
;    je settime_
;    cmp al,'D'
;    je setdate                ; si no es nada


displayhelp:                    ;***************** no command line
    mov ah,9
    mov dx,offset help_msg
    int 21h                 ;*** output message ***

    mov ax,4c00h
    int 21h                 ;*** terminate with 00 ***

newname:                        ;Get open name
    mov bx,offset nombre1
buscanombre:
    cmp al,13
    je endname
    cmp al,32
    je alop

    mov nombre,TRUE
    mov [bx],al
    lodsb
    inc bx
    jmp short buscanombre

endname:
    mov [bx],2400h          ;24h=$ 0=nul.Uso el $ para usar la func 9 int 21


cfecha:
    cmp nombre,TRUE
    je cfecha_

    lea dx,help_msg
    mov ah,9
    int 21h
    mov ax,4c00h
    int 21h

cfecha_:
    lea dx,cabecera
    mov ah,9
    int 21h

    lea dx,dta_table
    mov ah,1ah
    int 21h                 ;set DTA table.

    mov ah,4eh
    mov cx,00111111b        ;atributos
    lea dx,nombre1
    int 21h
    jc error                ;
    call displayfile        ;
shownext:
    mov ah,4fh
    int 21h                 ;
    jc error
    call displayfile
    jmp shownext

error:
    mov ax,4c00h
    int 21h


displayfile proc near
    lea bx,dta_name
displayloop:
    mov dl,[bx]
    or dl,dl
    je displayfile_
    mov ah,2
    int 21h
    inc bx
    jmp displayloop

displayfile_:

    call openfile
    call changedate
    call closefile

    call displaysize
    call displayattrib

    lea dx,crlf
    mov ah,9
    int 21h
    ret
displayfile endp

displaysize proc near
    lea si,filesize
    mov ax,word ptr dta_size
    mov dx,word ptr dta_size + 2
    call toint

    mov dl,31
    call newcolumn

    mov cx,6
    lea bx,filesize
    call displaycx

    ret
displaysize endp

displayattrib proc near
    lea di,attribmsg
    mov al,dta_atrib
    shl al,1
    shl al,1                ;los 2 primeros atributos no importan

    shl al,1                ;archive
    mov ah,'a'
    jc archive
    mov ah,'-'
archive:
    mov [di],ah
    inc di

    shl al,1                ;directory
    mov ah,'d'
    jc directory
    mov ah,'-'
directory:
    mov [di],ah
    inc di

    shl al,1                ;volume
    mov ah,'v'
    jc volume
    mov ah,'-'
volume:
    mov [di],ah
    inc di

    shl al,1                ;system
    mov ah,'s'
    jc system
    mov ah,'-'
system:
    mov [di],ah
    inc di

    shl al,1                ;hidden
    mov ah,'h'
    jc hidden
    mov ah,'-'
hidden:
    mov [di],ah
    inc di

    shl al,1                ;read only
    mov ah,'r'
    jc readonly
    mov ah,'-'
readonly:
    mov [di],ah
    inc di

    mov dl,41
    call newcolumn
    lea bx,attribmsg
    mov cx,6
    call displaycx
    ret
displayattrib endp

newcolumn proc near
    mov al,dl
    mov ah,3
    xor bh,bh
    int 10h
    mov dl,al
    dec ah
    int 10h
    ret
newcolumn endp

displaycx proc near
disizeloop:
    mov dl,[bx]
    mov ah,2
    int 21h
    inc bx
    loop disizeloop
    ret
displaycx endp

toint proc near             ;convierte binary to int
    push dx
    push bx
    push ax

    mov word ptr [si+0],32 shl 8 + 32
    mov word ptr [si+2],32 shl 8 + 32
    mov word ptr [si+4],32 shl 8 + 32

    add si,6
    cmp dx,9
    ja sizerror
    mov bx,10

ti1:
    dec si
    div bx
    or dl,'0'
    mov [si],dl
    xor dx,dx
    or ax,ax
    jne ti1

    pop ax
    pop bx
    pop dx
    ret

sizerror:
    dec si
    mov byte ptr [si],'?'
    pop ax
    pop bx
    pop dx
    ret
toint endp

openfile proc near
    mov ax,3d02h
    lea dx,dta_name
    int 21h
    mov handle1,ax
    ret
openfile endp

closefile proc near
    mov ah,3eh
    mov bx,handle1
    int 21h
closefile endp

changedate proc near
    mov ax,5701h
    mov bx,handle1
    mov cx,filetime
    mov dx,filedate
    int 21h
    ret
changedate endp

;== DATOS   ====================;
intro_msg DB 13,10,'FDate v1.00 (c) Ricardo Quesada 1994 - FreeWare version -',13,10,13,10,'$'

;reg_not_ok equ this byte
;db 'ERROR',13,10,'Verifique que su disco no este protegido.Gracias.',13,10,'$'
;prean_msg label byte
;db 13,10,'Tratando de remover cualquier tipo de virus o compactaci�n...$'
;antiv_msg equ this byte
;db'OK',13,10,13,10,'SUGERENCIA: Resetea la computadora ahora mismo.',13,10
;db'            El virus debe estar residente.',13,10,'$'

cabecera label byte
db'Filename         Date    Time    Size    Attrib      New Date    New Time',13,10
db'-------------------------------------------------------------------------',13,10,'$'

attribmsg db'------'

help_msg label byte
db 'Uso: FDate filespec [/t 01:00:00] [/d 20-11-94] [/a]',13,10
db '      filespec     - El nombre del archivo a poner al fecha.',13,10
db '                     Puede ser *.*, prog???.exe ,*.dat , etc.',13,10
db '      /t ??:??:??  - El Time que van a tener los archivos.',13,10
db '      /d ??-??-??  - El Date que van a tener los archivos.',13,10
db '      /a           - La conocida opci�n de remover virus.',13,10
db "                     (Built-in Ricardo Quesada's antivirus).",13,10,13,10
db 'Este programa no debe venderse ni incluirse en ning�n soft comercial',13,10,'$'

nombre1 db 128 dup(0)           ;reservo 128 por si las moscas
handle1 dw 0                    ;1 para cada nombre

filetime  dw  1 * 2048 + 00 * 32
filedate  dw 14 * 512 + 11 * 32 + 20
filesize  db 6 dup(32)            ;ascii size del file

nombre    db FALSE                ;se puso nombre
crlf db 13,10,'$'

dta_table equ this byte
            db 21 dup(?)
dta_atrib   db ?            ;atributo
dta_time    dw ?            ;time
dta_date    dw ?            ;date
dta_size    dd ?            ;size
dta_name    db 13 dup(?)    ;name


;== END ============================;

        RUTI    ENDP
CODE    ENDS
        END RUTI
