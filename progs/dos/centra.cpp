// programa que centra el texto entrado
// vamos a ver que sale
// hecho en diciembre de 1993
// por Ricardo Quesada.

// include files

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>


// variables


// declaracion de funciones


int main(void)
{
   char linea[80];
   int col_linea = 0;
   int cent;

   char ch;
   FILE *in, *out;
   char nombre_out[80];
   char nombre_in[80];

   clrscr();

   printf("Nombre del archivo a centrar (path completo):");
   scanf("%s",nombre_in);
   printf("Nombre del archivo centrado (path completo):");
   scanf("%s",nombre_out);
   if ((in = fopen(nombre_in,"rt")) == NULL)
   {
    fprintf(stderr,"Can not open file %s \n",nombre_in);
    return 1;
   }
   if ((out = fopen(nombre_out,"wt")) == NULL)
   {
    fprintf(stderr,"Can not open file %s \n",nombre_out);
    return 1;
   }
   do
   {
     do
     {
        ch = fgetc(in);
        linea[col_linea] = ch;
        col_linea ++;

     } while ( (ch != 10) && (ch != EOF) && (col_linea < 80) );


     if (col_linea==80)
      {
        linea[col_linea-1] = 10;
        linea[col_linea] = 32;
      }
      else
        linea[col_linea] = 10;

     cent = 40 - col_linea/2;

     col_linea = 0;

     for (;cent!=0;cent--)
     {
        printf(" ");
        fprintf(out," ");
     }
     printf("%s",linea);
     fprintf(out,linea);

   } while (ch != EOF);

return 0;
}
