Comment|
File:    DRV.ASM
Version: 2.51
Author:  Ricardo QUESADA
Date:    6/5/93
Purpose: DRIVE UTILITY
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE
        JMP INTRO
        DB '(C)RQ93'
;-------------------------------------int 13h
NEW13H: JMP SHORT ALA
        DB 'DR'
ALA:    CMP DL,0
        JNE CONT1
        MOV CS:LETTER,'A'
CONT1:  CMP DL,1
        JNE CONT2
        MOV CS:LETTER,'B'
CONT2:  CMP DL,128
        JNE CONT3
        MOV CS:LETTER,'C'
CONT3:  CMP DL,129
        JNE CONT
        MOV CS:LETTER,'D'
CONT:   CMP AH,3
        JB R
        JE W
        CMP AH,4
        JE V
        CMP AH,8
        JB F
        JE R
        CMP AH,0AH
        JB I
        JE R
        CMP AH,0CH
        JB W
        JE R
        CMP AH,15H
        JB D
        JE R
        CMP AH,18H
        JA Y
        MOV CS:LETTER2,'S'
        JMP SHORT SEGUIR
R:      MOV CS:LETTER2,'R'
        JMP SHORT SEGUIR
D:      MOV CS:LETTER2,'D'
        JMP SHORT SEGUIR
F:      MOV CS:LETTER2,'F'
        JMP SHORT SEGUIR
W:      MOV CS:LETTER2,'W'
        JMP SHORT SEGUIR
I:      MOV CS:LETTER2,'I'
        JMP SHORT SEGUIR
Y:      MOV CS:LETTER2,'?'
        JMP SHORT SEGUIR
V:      MOV CS:LETTER2,'V'
SEGUIR: PUSH AX
        PUSH BX
        PUSH DS
        PUSH DI

        XOR AH,AH        ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX

        MOV AX,CS:ADDR
        MOV DS,AX
        MOV AX,CS:LETTER2
        MOV AH,70H
        MOV BX,CS:LETTER
        MOV BH,70H
        XCHG DS:[DI],AX
        XCHG DS:[DI+2],BX
        MOV CS:LETTER2,AX
        MOV CS:LETTER,BX
        POP DI
        POP DS
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD13H]
        PUSHF
        PUSH AX
        PUSH BX
        PUSH DS
        PUSH DI

        XOR AH,AH        ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX

        MOV AX,CS:ADDR
        MOV DS,AX
        MOV AX,CS:LETTER2
        MOV BX,CS:LETTER
        MOV DS:[DI],AX
        MOV DS:[DI+2],BX
        POP DI
        POP DS
        POP BX
        POP AX
        POPF
        RET 2
;**********************************************
;**************** NEW 2FH *********************
;**********************************************

NEW2FH:
        JMP SHORT ACA_ACA
        DB'DR'
ACA_ACA:
        CMP AH,0DEH
        JE ES_ESTO
FIN_:
        JMP CS:[OLD2FH]

ES_ESTO:
        CMP AL,20H
        JNE ES_ESTO2
        XOR AL,AL
        IRET

ES_ESTO2:
        CMP AL,21H
        JNE FIN_
        MOV AX,0002H
        MOV BX,0501H
        IRET
;**************************************************************
;**************************************************************
;---------------------------------DATA
OLD13H  DD 0
OLD2FH  DD 0
LETTER  DW 0
LETTER2 DW 0
ADDR    DW 0
DSP     DB 156
;------------------------------------------
;       EMPIEZA LA UN/INS-TALACION
;------------------------------------------
INTRO:  CLD
        MOV SI,81H
ALOPA:  LODSB
        CMP AL,13
        JNE LULU
        JMP SISIE
LULU:   CMP AL,'/'
        JNE ALOPA

DETEC:  LODSB
        CMP AL,13
        JNE LULA
        JMP SISIE
LULA:   AND AL,11011111B
        CMP AL,'D'     ;DISPLAY
        JE LOFFS
        CMP AL,'H'     ;HELP
        JE HELP
        CMP AL,'V'      ;VERSION
        JE VERSION_
        CMP AL,'S'         ;SAVE
        JNE ERROR_MSG
        JMP SOFFS

ERROR_MSG:
        PUSH CS                 ;OPTION ERROR
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;DESPUES DEL ERROR DISPLAY HELP SCREEN

HELP:   PUSH CS           ;HELP SCREEN
        POP DS
        MOV DX,OFFSET MSGHLP
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

LOFFS:
        LODSB               ;CALCULA EL OFFSET DEL DISPLAY
        CMP AL,13
        JE ERROR_MSG
        MOV AH,AL
        LODSB
        CMP AL,13
        JNE ACA_1
        CALL CS:[ALOA]      ;ONE DIGIT
        JMP SISIE           ;FIN DEL LOOP
ACA_1:
        CMP AL,20H           ;ONE DIGIT
        JNE ACA_2
        CALL CS:[ALOA]
        JMP ALOPA           ;CONTINUE LOOP
ACA_2:
        AND AH,7
        SUB AL,30H
        MOV BL,AL
        MOV AL,10
        MUL AH
        ADD AL,BL

        SHL AL,1           ;MUL X 2
        MOV CS:DSP,AL
        JMP SHORT ALOPA

ALOA    PROC NEAR
        SUB AH,30H
        SHL AH,1            ;MUL X 2
        MOV CS:DSP,AH
        RET
ALOA    ENDP

VERSION_:
        MOV AX,0DE21H      ;AVERIGUA NM VERSION
        INT 2FH
        CMP AH,0DEH
        JE ERRVER_

        ADD AH,30H
        ADD AL,30H
        ADD BL,30H
        ADD BH,30H
        PUSH CS
        POP DS
        MOV [NUMBER],AH
        MOV [NUMBER+1],AL
        MOV [NUMBER+3],BH
        MOV [NUMBER+4],BL
        MOV DX,OFFSET VERNUM
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

ERRVER_:
        PUSH CS
        POP DS
        MOV AH,9
        MOV DX,OFFSET ERRVER
        INT 21H
        MOV AH,4CH
        INT 21H

SOFFS:                       ;OPTION /S
        MOV CX,0FFFFH
        XOR AX,AX
        XOR DI,DI
        MOV ES,CS:[2CH]
SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR
OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS             ;HASTA ACA DS:DX EL NOMBRE DEL PROG

        MOV AX,3D02H
        INT 21H             ;OPEN
        JC DERROR           ;JUMP ON ERROR

        MOV CX,1668         ;NUMBER OF BYTES
        PUSH CS
        POP DS
        MOV DX,100H         ;A PARTIR DE DONDE
        MOV BX,AX           ;HANDLER
        MOV AH,40H
        INT 21H             ;WRITE
        JC DERROR

        MOV AH,3EH
        INT 21H             ;CLOSE
        JC DERROR

        JMP ALOPA

DERROR:                      ;ERROR DE DRIVE.SHOW MESSAGE
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRDRV
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

AISE:
        SUB AH,30H
        MOV AL,AH
        MOV AH,2
        MUL AH
        MOV CS:DSP,AL

SISIE:
        MOV ADDR,0B800H       ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H

NOMONO:
        MOV AX,0DE20H
        INT 2FH
        OR AL,AL         ;AL=0 INST, AL=20H UNINST
        JNE INST         ;NO ES INSTALADO

;**************************************
;---------------------------------------UNINSTALL
        MOV AX,3513H
        INT 21H
        CMP ES:[BX+2],'RD'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'RD'
        JE UNLOAD

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNL
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

UNLOAD:
        MOV SI,OFFSET OLD13H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2513H
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        PUSH CS
        POP DS
        MOV AH,9          ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H
        MOV AH,4CH
        INT 21H             ;FIN DE TSR

;---------------------------------------INSTALL

INST:
        MOV AX,3513H            ;OLD13H
        INT 21H
        MOV SI,OFFSET OLD13H
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS                  ;NEW 13H
        POP DS
        MOV DX,OFFSET NEW13H
        MOV AX,2513H
        INT 21H

        MOV AX,352FH              ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH        ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV DX,OFFSET INSTAL     ;MENSAJE DE INTALADO
        MOV AH,09H
        INT 21H

        MOV DX,OFFSET INTRO
        INT 27H               ;TSR

INSTAL  DB 13,10,'DRV v2.51 - (C)Ricardo Quesada.',13,10
        DB'Call DRV again to unload it.',13,10
        DB'Type DRV /h to view the HELP SCREEN.',13,10,'$'

UNINST  DB'DRV unloaded.',13,10,'$'
VERNUM  DB'Version number: '
NUMBER  DB'00.00',13,10,'$'
ERRVER  DB'****ERROR:DRV not loaded or Version less than 02.51',13,10,'$'
ERRUNL  DB'****ERROR:Unable to unload.',13,10,'$'
ERRDRV  DB'****ERROR:Drive failure.',13,10,'$'
ERROPT  DB'****ERROR:Invalid option.',13,10,'$'

MSGHLP  DB'**** DRV v2.51 - HELP SCREEN ****',13,10,13,10
        DB'DRV [options]',13,10
        DB'    /dNN where NN, a number between 0 and 79,',13,10
        DB'         is the offset of the display.',13,10
        DB'    /s   save the offset of the display.',13,10
        DB'    /v   view version number of loaded DRV.',13,10
        DB'    /h   display this screen.',13,10,13,10
        DB'DRV displays the action that the drive is performing.',13,10
        DB'  1st Char: R:Read  W:Write  F:Format  I:Initialize  V:Verify',13,10
        DB'            D:Diagnostic  S:Disk format  ?:Unknown(1992).',13,10
        DB'  2nd Char: A & B = floppy drives. C & D = hard drives.',13,10,13,10
        DB'Should not be sold. For personal use only.',13,10
        DB'(C) 1993 by Ricardo Quesada.',13,10,'$'

        RUTI    ENDP
CODE    ENDS
        END RUTI
