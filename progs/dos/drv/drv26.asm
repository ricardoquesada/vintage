Comment|
File:    DRV.ASM
Version: 2.6
Author:  Ricardo QUESADA
Date:    10/5/93
Purpose: DRIVE UTILITY
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE
start:
        JMP INTRO
        DB '(C)RQ93'
;-------------------------------------int 13h
NEW13H: JMP SHORT ALA
        DB 'DR'
ALA:    CMP DL,0
        JNE CONT1
        MOV CS:LETTER,'A'
CONT1:  CMP DL,1
        JNE CONT2
        MOV CS:LETTER,'B'
CONT2:  CMP DL,128
        JNE CONT3
        MOV CS:LETTER,'C'
CONT3:  CMP DL,129
        JNE CONT
        MOV CS:LETTER,'D'
CONT:   CMP AH,3
        JB R
        JE W
        CMP AH,4
        JE V
        CMP AH,8
        JB F
        JE R
        CMP AH,0AH
        JB I
        JE R
        CMP AH,0CH
        JB W
        JE R
        CMP AH,15H
        JB D
        JE R
        CMP AH,18H
        JA Y
        MOV CS:LETTER2,'S'
        JMP SHORT SEGUIR
R:      MOV CS:LETTER2,'R'
        JMP SHORT SEGUIR
D:      MOV CS:LETTER2,'D'
        JMP SHORT SEGUIR
F:      MOV CS:LETTER2,'F'
        JMP SHORT SEGUIR
W:      MOV CS:LETTER2,'W'
        JMP SHORT SEGUIR
I:      MOV CS:LETTER2,'I'
        JMP SHORT SEGUIR
Y:      MOV CS:LETTER2,'?'
        JMP SHORT SEGUIR
V:      MOV CS:LETTER2,'V'
SEGUIR: PUSH AX
        PUSH BX
        PUSH DS
        PUSH DI

        XOR AH,AH               ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX

        MOV AX,CS:ADDR
        MOV DS,AX
        MOV AX,CS:LETTER2
        MOV AH,70H
        MOV BX,CS:LETTER
        MOV BH,70H
        XCHG DS:[DI],AX
        XCHG DS:[DI+2],BX
        MOV CS:LETTER2,AX
        MOV CS:LETTER,BX
        POP DI
        POP DS
        POP BX
        POP AX
        PUSHF
        CALL CS:[OLD13H]
        PUSHF
        PUSH AX
        PUSH BX
        PUSH DS
        PUSH DI

        XOR AH,AH               ;DISPLACEMENT
        MOV AL,CS:DSP
        MOV DI,AX

        MOV AX,CS:ADDR
        MOV DS,AX
        MOV AX,CS:LETTER2
        MOV BX,CS:LETTER
        MOV DS:[DI],AX
        MOV DS:[DI+2],BX
        POP DI
        POP DS
        POP BX
        POP AX
        POPF
        RET 2
;**********************************************
;**************** NEW 2FH *********************
;**********************************************

NEW2FH:
        JMP SHORT ACA_ACA
        DB'DR'
ACA_ACA:
        CMP AH,0DEH
        JE ES_ESTO
FIN_:
        JMP CS:[OLD2FH]

ES_ESTO:
        CMP AL,20H
        JNE ES_ESTO2
        XOR AL,AL
        IRET

ES_ESTO2:
        CMP AL,21H
        JNE FIN_
        MOV AX,3032H
        MOV BX,3630H
        IRET
;**************************************************************
;**************************************************************
;---------------------------------DATA
OLD13H  DD 0
OLD2FH  DD 0
LETTER  DW 0
LETTER2 DW 0
ADDR    DW 0
DSP     DB 156
;------------------------------------------
;       EMPIEZA LA UN/INS-TALACION
;------------------------------------------
INTRO:  CLD
        MOV SI,81H
ALOPA:  LODSB
        CMP AL,13
        JNE LULU
        JMP SISIE
LULU:   CMP AL,'/'
        JNE ALOPA

DETEC:  LODSB
        CMP AL,13
        JNE LULA
        JMP SISIE
LULA:   AND AL,11011111B
        CMP AL,'D'              ;DISPLAY
        JE LOFFS
        CMP AL,'H'              ;HELP
        JE HELP
        CMP AL,'V'              ;VERSION
        JE VERSION_
        CMP AL,'A'
        JE ANTIVIRUS_
        CMP AL,'S'              ;SAVE
        JNE ERROR_MSG
        JMP SOFFS

ERROR_MSG:
        PUSH CS                 ;OPTION ERROR
        POP DS
        MOV DX,OFFSET ERROPT
        MOV AH,9
        INT 21H                 ;DESPUES DEL ERROR DISPLAY HELP SCREEN

HELP:   PUSH CS                 ;HELP SCREEN
        POP DS
        MOV DX,OFFSET MSGHLP
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

LOFFS:
        LODSB                   ;CALCULA EL OFFSET DEL DISPLAY
        CMP AL,13
        JE ERROR_MSG
        MOV AH,AL
        LODSB
        CMP AL,13
        JNE ACA_1
        CALL CS:[ALOA]          ;ONE DIGIT
        JMP SISIE               ;FIN DEL LOOP
ACA_1:
        CMP AL,20H              ;ONE DIGIT
        JNE ACA_2
        CALL CS:[ALOA]
        JMP ALOPA               ;CONTINUE LOOP
ACA_2:
        AND AH,7
        SUB AL,30H
        MOV BL,AL
        MOV AL,10
        MUL AH
        ADD AL,BL

        SHL AL,1                ;MUL X 2
        MOV CS:DSP,AL
        JMP SHORT ALOPA

ALOA    PROC NEAR
        SUB AH,30H
        SHL AH,1                ;MUL X 2
        MOV CS:DSP,AH
        RET
ALOA    ENDP

ANTIVIRUS_:
        MOV CS:FLAG,1
        JMP JIUPI


VERSION_:
        MOV AX,0DE21H           ;AVERIGUA NM VERSION
        INT 2FH
        CMP AH,0DEH
        JE ERRVER_

        PUSH CS
        POP DS
        MOV [NUMBER],AH
        MOV [NUMBER+1],AL
        MOV [NUMBER+3],BH
        MOV [NUMBER+4],BL
        MOV DX,OFFSET VERNUM
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

ERRVER_:
        PUSH CS
        POP DS
        MOV AH,9
        MOV DX,OFFSET ERRVER
        INT 21H
        MOV AH,4CH
        INT 21H

SOFFS:                          ;OPTION /S
        MOV CS:FLAG,0
JIUPI:                          ;PARA ACA.
        MOV CX,0FFFFH
        XOR AX,AX
        XOR DI,DI
        MOV ES,CS:[2CH]
SCANEAR:
        REPNE SCASB
        CMP BYTE PTR ES:[DI],0
        JE OK_TODO
        SCASB
        JNZ SCANEAR
OK_TODO:
        MOV DX,DI
        ADD DX,3
        PUSH ES
        POP DS                  ;HASTA ACA DS:DX EL NOMBRE DEL PROG

        CMP CS:FLAG,1
        JNE NO_ANTI

        XOR CX,CX
        MOV AH,3CH
        INT 21H
        JC DERROR

NO_ANTI:
        MOV AX,3D02H
        INT 21H                 ;OPEN
        JC DERROR               ;JUMP ON ERROR

        MOV CX,final - start    ;NUMBER OF BYTES
        PUSH CS
        POP DS
        MOV DX,100H             ;A PARTIR DE DONDE
        MOV BX,AX               ;HANDLER
        MOV AH,40H
        INT 21H                 ;WRITE
        JC DERROR

        MOV AH,3EH
        INT 21H                 ;CLOSE
        JC DERROR

       MOV DX,OFFSET MSG_ERROR_NO
       MOV AH,9
       INT 21H
       MOV AH,4CH
       INT 21H

DERROR:                         ;ERROR DE DRIVE.SHOW MESSAGE
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRDRV
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

AISE:
        SUB AH,30H
        MOV AL,AH
        MOV AH,2
        MUL AH
        MOV CS:DSP,AL

SISIE:
        MOV ADDR,0B800H         ;DETECTA EN QUE MODO SE ESTA USANDO
        MOV BX,0040H
        MOV ES,BX
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOMONO
        MOV ADDR,0B000H

NOMONO:
        MOV AX,0DE20H
        INT 2FH
        OR AL,AL                ;AL=0 INST, AL=20H UNINST
        JNE INST                ;NO ES INSTALADO

;**************************************
;---------------------------------------UNINSTALL
        MOV AX,3513H
        INT 21H
        CMP ES:[BX+2],'RD'
        JNE ERRUNLOAD

        MOV AX,352FH
        INT 21H
        CMP ES:[BX+2],'RD'
        JE UNLOAD

ERRUNLOAD:
        PUSH CS
        POP DS
        MOV DX,OFFSET ERRUNL
        MOV AH,9
        INT 21H
        MOV AH,4CH
        INT 21H

UNLOAD:
        MOV SI,OFFSET OLD13H
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,2513H
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        MOV SI,OFFSET OLD2FH
        MOV DX,ES:[SI]
        MOV AX,ES:[SI+2]
        MOV DS,AX
        MOV AX,252FH
        INT 21H

        MOV BX,ES
        MOV ES,ES:[2CH]
        MOV AH,49H
        INT 21H
        MOV ES,BX
        MOV AH,49H
        INT 21H

        PUSH CS
        POP DS
        MOV AH,9                ;MESAJE DE UNINSTALL
        MOV DX,OFFSET UNINST
        INT 21H
        MOV AH,4CH
        INT 21H                 ;FIN DE TSR

;---------------------------------------INSTALL

INST:
        MOV AX,3513H            ;OLD13H
        INT 21H
        MOV SI,OFFSET OLD13H
        MOV [SI],BX
        MOV [SI+2],ES

        PUSH CS                 ;NEW 13H
        POP DS
        MOV DX,OFFSET NEW13H
        MOV AX,2513H
        INT 21H

        MOV AX,352FH            ;OLD 2FH
        INT 21H
        MOV SI,OFFSET OLD2FH
        MOV [SI],BX
        MOV [SI+2],ES

        MOV DX,OFFSET NEW2FH    ;NEW 2FH
        MOV AX,252FH
        INT 21H

        MOV DX,OFFSET INSTAL    ;MENSAJE DE INTALADO
        MOV AH,09H
        INT 21H

        MOV DX,OFFSET INTRO
        INT 27H                 ;TSR

INSTAL  DB 13,10,'DRV v2.6 - (c) Ricardo Quesada 1993.',13,10
        DB'Llam� a DRV denuevo para desinstalarlo.',13,10
        DB'DRV /H presenta ayuda adicional.',13,10,'$'

UNINST  DB'DRV v2.6 desinstalado.',13,10,'$'
VERNUM  DB'Numero de versi�n: '
NUMBER  DB'00.00',13,10,'$'
ERRVER  DB'****ERROR:DRV no esta instalado o la versi�n es menor que la 2.51',13,10,'$'
ERRUNL  DB'****ERROR:Imposible desinstalar.',13,10,'$'
ERRDRV  DB'****ERROR:Falla de disco.',13,10,'$'
ERROPT  DB'****ERROR:Operaci�n invalida.',13,10,'$'

MSG_ERROR_NO DB'Operaci�n OK.',13,10,'$'

MSGHLP  DB'**** DRV v2.6 ****',13,10,13,10
        DB'DRV [opciones]',13,10
        DB'    /dNN Donde NN, un numero entre 0 y 79,',13,10
        DB'         es el offset del display.',13,10
        DB'    /s   Salva el offset del display.',13,10
        DB'    /a   Borra cualquier tipo de virus que tenga este programa.',13,10
        DB'    /v   Ver la version de DRV instalado.',13,10
        DB'    /h   Muestra esta pantalla.',13,10,13,10
DB'Nota: Recuerde que /a y /s no desinstalan a DRV.',13,10,13,10
        DB'DRV muestra la acci�n que ejecutan los drives.',13,10
        DB'  1er Char: R:Read  W:Write  F:Format  I:Initialize  V:Verify',13,10
        DB'            D:Diagnostic  S:Disk format  ?:Desconocido(1992).',13,10
        DB'  2do Char: A & B = floppy drives. C & D = hard drives.',13,10,13,10
        DB'Freeware version.',13,10
        DB'(C) Ricardo Quesada 1993.',13,10,'$'
FLAG    DB 0

final equ this byte
        RUTI    ENDP
CODE    ENDS
        END RUTI
