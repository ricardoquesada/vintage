// programa que centra el texto entrado
// vamos a ver que sale
// hecho en diciembre de 1993
// por Ricardo Quesada.

// include files

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>


// variables


// declaracion de funciones


int main(void)
{
   char ch;
   int i;
   int wrap_num;
   FILE *in, *out;
   char nombre_out[80];
   char nombre_in[80];
   char wrap_str[10];

   clrscr();

   printf("Nombre del archivo a centrar (path completo):");
   scanf("%s",nombre_in);
   printf("Nombre del archivo centrado (path completo):");
   scanf("%s",nombre_out);
   do
   {
        printf("En que columna se corta:");
        scanf("%s",wrap_str);
        wrap_num = atoi(wrap_str);
   } while (wrap_num == 0);

   if ((in = fopen(nombre_in,"rt")) == NULL)
   {
    fprintf(stderr,"Can not open file %s \n",nombre_in);
    return 1;
   }
   if ((out = fopen(nombre_out,"wt")) == NULL)
   {
    fprintf(stderr,"Can not open file %s \n",nombre_out);
    return 1;
   }

   i=0;
   do
   {

        ch = fgetc(in);
        fputc(ch,stdout);
        fputc(ch,out);
        if (ch == 10)
            i = 0;

        if ( (i % wrap_num == 0) && (i != 0))
        {
            fputc(10,out);
            fputc(10,stdout);
        }

        i++;

   }  while (ch != EOF);

return 0;
}
