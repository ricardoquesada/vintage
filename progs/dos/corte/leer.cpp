//
// Lector de claves.
//

#include <stdio.h>

void Encrypt(char passwd[] ){
	int i;
	for(i=0;;i++){
		if(passwd[i]==0) break;
		passwd[i]^=0x03;
	}
}

int main(void)
{
	FILE *in;
	int i;char ch,ch1;
	char usuario[40],password[40],label[40];

	if ((in = fopen("passwd", "rt"))
		 == NULL)
	{
		fprintf(stderr, "Cannot open input file.\n");
		return 1;
	}

	for(;;){
		i=0;
		ch=fgetc(in);
		if(ch==EOF)
			break;
		if(feof(in)){
			printf("\nReached end of file");
			break;
		}
		if(ch>=0x40){
			do{
				usuario[i]=ch;
				ch=fgetc(in);
				i++;
				usuario[i]=0;
			} while( ch!=':');
			i=0;
			for(;;){
				ch=fgetc(in);
				if(ch==':')
					break;
				password[i]=ch;
				i++;
				password[i]=0;
			}
			i=0;
			for(;;){
				ch=fgetc(in);
				if(ch==':')
					break;
				label[i]=ch;
				i++;
				label[i]=0;
			}
			Encrypt(password);
			fprintf(stdout,"\nUsuario=%s\nPassword=%s\nLabel=%s",usuario,password,label);
		}
		if(feof(in)){
			printf("\nReached end of file");
			break;
		}
		if(ch==EOF)
			break;
		}
	fclose(in);
	return 0;
}
