/***************************************************
 *																	*
 *              CORTE LOGIN v0.02                  *
 *             por Ricardo Quesada                 *
 *                                                 *
 ***************************************************/

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TDialog
#define Uses_TButton
#define Uses_TLabel
#define Uses_TInputLine
#define Uses_TCheckBoxes
#define Uses_TSItem
#define Uses_TView
#define Uses_TRadioButtons
#define Uses_TMenuBar
#define Uses_TStatusLine
#define Uses_TRect
#define Uses_TStatusDef
#define Uses_TSubMenu
#define Uses_TKeys
#define Uses_TStatusItem
#define Uses_TDeskTop
#define Uses_TMenuItem
#define Uses_MsgBox
#define Uses_TInputPasswd

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tvtools.h>       // librerias adicionales para Turbo Vision
#include <tv.h>

const int cmAbout   = 100;
const int cmSalir   = 101;
const int cmPasswd  = 102;
const int cmCPasswd = 103;
const int cmWelco	  = 104;
const int cmAdd	  = 105;

int rvalue;

class MyProg : public TApplication
{
public:

	struct Datos {
		char usuario[40];
		char password[40];
		char level[4];
		int intentos;
	} datos;

	void About();
	void Welcome();
	void Default();
	void Salir();
	int  InputPasswd();
	int  CheckUsr();
	void Encrypt(char *);
	void AddPass();
	void AddPassword();

	MyProg();
	static TStatusLine *initStatusLine( TRect r );
	static TMenuBar *initMenuBar( TRect r );
	virtual void handleEvent( TEvent& event );

};

MyProg::MyProg() :
	 TProgInit( MyProg::initStatusLine,
				  MyProg::initMenuBar,
				  MyProg::initDeskTop
			 )
{
	datos.intentos=0;
}

TStatusLine *MyProg::initStatusLine( TRect r)
{
	r.a.y = r.b.y - 1;
	return new TStatusLine( r,
		*new TStatusDef( 0, 0xFFFF ) +
			*new TStatusItem( "~Alt-X~ Salir",kbAltX, cmSalir ) +
			*new TStatusItem( "~F-10~ Menu",kbF10, cmMenu)
	);
}

TMenuBar *MyProg::initMenuBar( TRect r )
{
	r.b.y = r.a.y + 1;
	return new TMenuBar( r,
		*new TSubMenu( "~�~",kbAltSpace ) +
			*new TMenuItem("~A~cerca",cmAbout, kbF1, hcNoContext,"F1" ) +
			*new TMenuItem("~B~ienvenido",cmWelco,kbNoKey,hcNoContext ) +
		*new TSubMenu("~C~onsulta",kbAltC ) +
			*new TMenuItem("~P~assword",cmPasswd,kbNoKey,hcNoContext ) +
			*new TMenuItem("~A~dd User",cmAdd,kbNoKey,hcNoContext) +
			newLine() +
			*new TMenuItem("~S~alir",cmSalir,kbAltX, hcNoContext,"Alt-X")
	);
}

void MyProg::handleEvent ( TEvent& event )
{
	TApplication::handleEvent( event );
	if ( event.what == evCommand )
		{
		switch( event.message.command)
			{
			case cmAbout:
				About();
				break;
			case cmWelco:
				Welcome();
				break;
			case cmSalir:
				Salir();
				break;
			case cmPasswd:
				InputPasswd();
				break;
			case cmAdd:
				AddPass();
				break;
			default:
				return;
			}
		clearEvent( event );
		}
}


// About
void MyProg::Welcome()
{
	TDialog *aboutBox = new TDialog(TRect(0, 0, 40, 13), "Bienvenidos");

	aboutBox->insert(
	  new TStaticText(TRect(2, 2, 38, 11),
		"\003Bienvenido a la red\n\003\n"
		"\003de la\n\003\n"
		"\003Secretaria Letrada de Informatica\n\003\n"
		"\003\n\003\n"
	  )
	);
	aboutBox->insert(
	  new TButton(TRect(2, 10,38 , 12), "OK", cmOK, bfDefault)
	);
	aboutBox->options |= ofCentered;
	deskTop->execView(aboutBox);
	destroy( aboutBox );
	Default();
	for(;;){
		if(InputPasswd()==0)
			break;
	}
}

// Input del Password
int MyProg::InputPasswd(){
	int a;
	TDialog *dptr = new TDialog ( TRect ( 0, 1, 50, 20),"Password" );
	if ( dptr )
	{
		dptr->insert(
		  new TStaticText(TRect(2, 2, 48, 7),
			"\003Por favor ingrese usuario y password\n"
			"\003Si Ud. no conoce su usuario apriete ENTER\n"
			"\003Con la tecla TAB o con el mouse cambia de campo.\n"
			)
		);
		TInputLine *b = new TInputLine( TRect( 4, 9, 46, 10 ), 40 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 8, 44, 9 ),"~U~suario", b ) );

		b = new TInputPasswd( TRect( 4, 12, 46, 13 ), 40 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 11, 44 ,12 ),"~P~assword", b ) );


		dptr->insert( new TButton( TRect( 3, 16, 47, 18) ,"~O~K",
						cmOK, bfDefault ) );

		dptr->options |= ofCentered;
		dptr->setData(&datos.usuario);
		ushort control = deskTop->execView( dptr );

		if ( control == cmOK )  // si pulso OK, entonces...
		{
			dptr->getData(&datos.usuario);
			switch(CheckUsr()){
				case 0: a=0;
					break;
				case 1: a=1;
					messageBox("\003Usuario o password invalido",mfOKButton | mfWarning );
					datos.intentos++;
					if(datos.intentos>=3){
						a=0;
						rvalue=255;
					}
					break;
				default: a=1;
					break;
			}
		}
	}
	destroy( dptr );
	return a;
}

void MyProg::AddPass(){
	TDialog *dptr = new TDialog ( TRect ( 0, 1, 50, 20),"Add Password" );
	if ( dptr )
	{
		TInputLine *b = new TInputLine( TRect( 4, 5, 46, 6 ), 40 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 4, 44, 5 ),"~U~suario", b ) );

		b = new TInputPasswd( TRect( 4, 9, 46, 10 ), 40 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 8, 44 ,9 ),"~P~assword", b ) );

		b = new TInputLine( TRect( 4, 13, 46, 14 ), 4 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 12, 44 ,13 ),"~E~rror level", b ) );


		dptr->insert( new TButton( TRect( 3, 16, 47, 18) ,"~O~K",
						cmOK, bfDefault ) );

		dptr->options |= ofCentered;
		dptr->setData(&datos.usuario);
		ushort control = deskTop->execView( dptr );

		if ( control == cmOK )  // si pulso OK, entonces...
		{
			dptr->getData(&datos.usuario);
			AddPassword();
		}
	}
	destroy( dptr );
}

void MyProg::About()
{
	TDialog *aboutBox = new TDialog(TRect(0, 0, 35, 11), "Acerca del programa");

	aboutBox->insert(
	  new TStaticText(TRect(2, 2, 33, 7),
		"\003Corte Login v0.02a\n\003\n"
		"\003por Ricardo Quesada\n\003\n"
		"\003(c) 1995,1996\n\003\n"
	  )
	);
	aboutBox->insert(
	  new TButton(TRect(2, 8,33 , 10), "OK", cmOK, bfDefault)
	);
	aboutBox->options |= ofCentered;
	deskTop->execView(aboutBox);
	destroy( aboutBox );
}

void MyProg::Default()
{
	strcpy( datos.usuario,"Icaro" );
	strcpy( datos.password,"Icaro" );
}

void MyProg::Encrypt(char *passwd ){
	int i;
	for(i=0;;i++){
		if(passwd[i]==0) break;
		passwd[i]^=0x03;
	}
}

void MyProg::AddPassword(){
	FILE *in;
	Encrypt(datos.password);
	if ((in = fopen("passwd", "at")) == NULL)	return;
	fseek(in, 0L, SEEK_END);
	fprintf(in,"%s:%s:%s:\n",datos.usuario,datos.password,datos.level);
	fclose(in);
}
int MyProg::CheckUsr(){
	FILE *in;
	int i,a;char ch;
	char usuario[40],password[40],label[40];

	if ((in = fopen("passwd", "rt")) == NULL)	return 1;
	a=1;
	for(;;){
		i=0;
		ch=fgetc(in);
		if(ch==EOF)	break;
		if(feof(in)) break;
		if(ch>=0x40){
			do{
				usuario[i]=ch;
				ch=fgetc(in);
				i++;
				usuario[i]=0;
			} while( ch!=':');
			i=0;
			for(;;){
				ch=fgetc(in);
				if(ch==':')
					break;
				password[i]=ch;
				i++;
				password[i]=0;
			}
			i=0;
			for(;;){
				ch=fgetc(in);
				if(ch==':')
					break;
				label[i]=ch;
				i++;
				label[i]=0;
			}
			Encrypt(password);
			if( (strcmpi( datos.usuario,usuario ) == 0) &&
				 (strcmpi( datos.password,password) == 0) ){
				 a=0;
				 rvalue=atoi(label);
			}
		}
		if(feof(in)) break;
		if(ch==EOF) break;
		}
	fclose(in);
	return a;
}

//++++++++++++++++
//  Salir a Dos.
//++++++++++++++++

// DialogBox de salir
void MyProg::Salir()
{
	if(cmYes == messageBox("\003� Seguro que quiere salir ?",mfYesButton | mfNoButton | mfConfirmation ) )
	{
		endModal( cmQuit);
	}
}


// ++++++++++++++
// Funcion Principal  MAIN
// ++++++++++++++

int main(int argc,char *argv[])
{
	MyProg myprog;

	if( (argc>1) && (argv[1][0]=='a'))
		myprog.run();
	else
		myprog.Welcome();

	return rvalue;
}