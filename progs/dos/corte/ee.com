��          O  O                   Graham Systems Environment Edit (EE)
       Copyright June 1, 1987 by Keith P. Graham






















F1=Help,F2=Save,F3=Quit,F4=Clear,F5=Insert,F6=Delete,F7=0DH,F8=08H,F10=READ ME
  The primary purpose of this program is to interactively
  alter the path and perhaps some other parts of the Environment.
  Altering the COMSPEC= or other parts of the environment is dangerous!
     There is no guarantee that this program will work
     as described under all conditions. I cannot be
     held responsible for the results of running this
     program. The DOS environment is not something to
     fool with.
  You can change any item in the environment or add a new
  item by simply moving you cursor to where you want
  and typing the letters. Although order is not very
  important, try to keep the COMSPEC= statement first,
  because this program sometimes looks for that statement
  to verify that it has indeed found the environment.
  This program has been tested with DOS 3.3, but I can not
  say if it will work with all version of DOS. Worst case is
  that EE will not be able to find a valid environment and
  either give up or be fooled into thinking that something
  other than the environment is the environment.
Press any key to continue..............
  The environment cannot handle blank lines so this program
  automatically deletes blank lines. The Environment almost
  always deals in upper case letters. The caps lock key is
  pushed on by the program for you convenience. PATH= and 
  most of the standard DOS environment items MUST be entirely
  in upper case. Dos cannot find the b: drive, only the B:
  drive. In order to change enviroment lines longer than
  80 characters, EE splits long lines and puts a ~ in
  to show the split line. Most Dos functions can not use
  lines any longer than 80 bytes.
  You may have "Inherited" Dos in which case the Environment
  is of limited length. EE checks the size of the environment
  and will not let you expand beyond the size of the original
  environment. EE does not know the actual environment size
  and only makes a guess. It is possible to hang the system
  in which case you will get the following message:
  "Memory allocation error - cannot load COMMAND, System halted"
   You must reboot to get the system going again.
   The default environmentsize is 160 bytes.
Press any key to continue..............
  Operation of Keys.
        F1 - Gives this display
        F2 - Updates the Environment and leaves EE.
        F3 - Quits EE without updating the Environment.
        F4 - Clears the Environment completely.
        F5 - Inserts a blank line above the cursor line.
        F6 - Deletes the current line.
        F7 - The 0DH key - allows you to enter the
             carriage return character directly.
        F8 - The 08H key - allows you to enter the
             backspace character without backspacing.
        F9 - Redisplays screen. You may have entered more
             characters than the enviroment can handle.
             This shows the expected results of EE.
        F10- Copyright and Advertising
        ~  - (tilde) joins current line with next line
             when the screen is saved to get lines longer
             then 80 bytes

Press any key to continue..............

                Graham Systems Environment Edit (EE)
                   Special Ed Shoemaker Edition 
       (C)Copyright June 1, 1987 by Keith P. Graham
       Please address any comments and an occasional
       $5.00 for those guilt ridden souls to:
              Keith P. Graham
              238 Germonds Rd.
              W. Nyack, New York 10994





                       TANSTAAFL


Press any key to continue..............
Wrong DOS version. Must be 2.0 or 2.1

Invalid COMSPEC or environment not found

EE completed
$                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                d�T�D�4�$��|�l�\�L�<�,��0�!<}	���	�\�	���Ȏ؎�����������������O�> u�n��
<u��;}��� ��������<u�< t,�����}���> t�}O t������� tK�9 u� ��놠
<Ru�6�w�<Su	��6�j�<Gu�� <Ou�� <Hu��:�|�&�q����l�<Pu��:�|�&�V����Q�<Mu"��:�|� :�|�&�5����0����+�<Ku/��:��:��&���̠����������<=u����S�	�!�<<u�����S�	�!�h<;uc����]���u���	����E���]�8�	���-���E��
�	�k�������*�� <Du0���� ����?�	�7�]�� ����� �� <Au��
�;�<Bu��
�/�<Cu� ���Z���� � <?uM�����6���.�����Ћ����� +���� �i��«��� �P ���\ �	���q �F�<@uA���w�װ �P ���j�>����FF���� +���«� �����, ���,���� P� �� X�PR��� � �	�2ZX�PSQR��  ��ZY[X�PSQ��;}&�
�
� � ���;}:|��� �������Y[X�PSQR�>�t�> t��
���5��
 ����!�
����!u���
��
�> t����ZY[Xô� ô� �SQR���>� �ZY[��, ��&�  ��p�p ��9  uQ�  �  � ��Yt@����������&�	��� �H�؋ ���&�@�ؾ  ��&�?&�&�  �  Q��< u�< t)����~��  CC&�?���t����@�B&�Y��Q&�&�YÍ��7�II���  �W�Q�  �B��P~� ���< t	�
�O��-��  CC�7���t%���t;}��� �������Y���Y�����  �7�II�  Q��O��< t<~u$�  CC�7���t���  CC�7���t�< t� �BY��Q� ��Y�PSQRVW�O +˃�O��N��
����  �F�P +ˊ
�< t�
����
���`�_^ZY[X�P�@ ��&� �	@&� X�P�@ ���	&� X�PSQRVW�P +����F���^V�P +ˊ
��
�����
�����_^ZY[X�R��޷ KKۋˋ���ڷ Z�PSRV����<t4<u
� ����<
u�:&|���������΢
����^Z[X�