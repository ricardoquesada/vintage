/*
		Header de Corte
*/

class MyProg : public TApplication
{

public:
	struct CONFIG
	{
		ushort radioImpre;
		char   inputImpre[50];
	};

private:
	void About();
	void Default();
	void SetEnvi();
	void Salir();
	void SelecImpre();
public:

	CONFIG *Config;

	MyProg();
	static TStatusLine *initStatusLine( TRect r );
	static TMenuBar *initMenuBar( TRect r );
	virtual void handleEvent( TEvent& event );

};
