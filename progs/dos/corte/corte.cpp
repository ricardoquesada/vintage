/***************************************************
 *																	*
 *		Programa para la Seleccion de Impresoras     *
 *													            *
 *        para la Corte Suprema de Justicia        *
 *                                                 *
 ***************************************************/

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TDialog
#define Uses_TButton
#define Uses_TLabel
#define Uses_TInputLine
#define Uses_TCheckBoxes
#define Uses_TSItem
#define Uses_TView
#define Uses_TRadioButtons
#define Uses_TMenuBar
#define Uses_TStatusLine
#define Uses_TRect
#define Uses_TStatusDef
#define Uses_TSubMenu
#define Uses_TKeys
#define Uses_TStatusItem
#define Uses_TDeskTop
#define Uses_TMenuItem
#define Uses_MsgBox

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tv.h>

const int cmAbout   = 100;
const int cmSalir   = 101;
const int cmImpre   = 102;
const int cmSuge	  = 103;
const int cmWelco	  = 104;

class MyProg : public TApplication
{
public:

	struct Datos {
		ushort t_impresora;
		char n_impresora[52];
	} datos;

	struct Sugerir {
		char nombre[62];
		char juzgado[62];
		char telefono[62];
		char texto1[62];
		char texto2[62];
	} sugerir;

	void About();
	void Welcome();
	void Default();
	void SetEnvi();
	void Salir();
	void SelecImpre();
	void Sugerencia();

	MyProg();
	static TStatusLine *initStatusLine( TRect r );
	static TMenuBar *initMenuBar( TRect r );
	virtual void handleEvent( TEvent& event );

};

MyProg::MyProg() :
	TProgInit( MyProg::initStatusLine,
				  MyProg::initMenuBar,
				  MyProg::initDeskTop
			 )
{
	Welcome();
	Default();
	SelecImpre();
}

TStatusLine *MyProg::initStatusLine( TRect r)
{
	r.a.y = r.b.y - 1;
	return new TStatusLine( r,
		*new TStatusDef( 0, 0xFFFF ) +
			*new TStatusItem( "~Alt-X~ Salir",kbAltX, cmSalir ) +
			*new TStatusItem( "~F-10~ Menu",kbF10, cmMenu)
	);
}

TMenuBar *MyProg::initMenuBar( TRect r )
{
	r.b.y = r.a.y + 1;
	return new TMenuBar( r,
		*new TSubMenu( "~�~",kbAltSpace ) +
			*new TMenuItem("~A~cerca",cmAbout, kbF1, hcNoContext,"F1" ) +
			*new TMenuItem("~B~ienvenido",cmWelco,kbNoKey,hcNoContext ) +
		*new TSubMenu("~A~rchivos",kbAltA ) +
			*new TMenuItem("~I~mpresora",cmImpre,kbNoKey,hcNoContext ) +
			*new TMenuItem("S~u~gerencia",cmSuge,kbNoKey,hcNoContext ) +
			newLine() +
			*new TMenuItem("~S~alir",cmSalir,kbAltX, hcNoContext,"Alt-X")
	);
}

void MyProg::handleEvent ( TEvent& event )
{
	TApplication::handleEvent( event );
	if ( event.what == evCommand )
		{
		switch( event.message.command)
			{
			case cmAbout:
				About();
				break;
			case cmWelco:
				Welcome();
				break;
			case cmSalir:
				Salir();
				break;
			case cmImpre:
				SelecImpre();
				break;
			case cmSuge:
				Sugerencia();
				break;
			default:
				return;
			}
		clearEvent( event );
		}
}


// acerca del programa
void MyProg::Welcome()
{
	TDialog *aboutBox = new TDialog(TRect(0, 0, 40, 13), "Bienvenidos");

	aboutBox->insert(
	  new TStaticText(TRect(2, 2, 38, 11),
		"\003Bienvenido a la red\n\003\n"
		"\003de la\n\003\n"
		"\003Secretaria Letrada de Informatica\n\003\n"
		"\003\n\003\n"
	  )
	);
	aboutBox->insert(
	  new TButton(TRect(2, 10,38 , 12), "OK", cmOK, bfDefault)
	);
	aboutBox->options |= ofCentered;
	deskTop->execView(aboutBox);
	destroy( aboutBox );
}

void MyProg::About()
{
	TDialog *aboutBox = new TDialog(TRect(0, 0, 35, 11), "Acerca del programa");

	aboutBox->insert(
	  new TStaticText(TRect(2, 2, 33, 7),
		"\003Corte v0.01a\n\003\n"
		"\003por Ricardo Quesada\n\003\n"
		"\003(c) 1995\n\003\n"
	  )
	);
	aboutBox->insert(
	  new TButton(TRect(2, 8,33 , 10), "OK", cmOK, bfDefault)
	);
	aboutBox->options |= ofCentered;
	deskTop->execView(aboutBox);
	destroy( aboutBox );
}

/***************************************
 *                                     *
 *    Funciones de entrada de datos    *
 *                                     *
 ***************************************/
// carga los valores por defecto
void MyProg::Default()
{
	datos.t_impresora = 0;				// Epson LX - 810
	strcpy( datos.n_impresora ,"Marca / Modelo" );
	strcpy( sugerir.nombre,"" );
	strcpy( sugerir.juzgado,"" );
	strcpy( sugerir.telefono,"" );
	strcpy( sugerir.texto1,"" );
	strcpy( sugerir.texto2,"" );
}


void MyProg::SelecImpre()
{
	TDialog *dptr = new TDialog ( TRect ( 0, 0, 60, 14),"AS400 - Impresoras" );
	if ( dptr )
	{
		TView *b = new TRadioButtons( TRect (5 ,3 ,26, 7 ),
			new TSItem( "~E~pson LX-810",
			new TSItem( "~H~P LaserJet 4",
			new TSItem( "I~B~M Proprinter",
			new TSItem( "~N~one",0 ) ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 2, 26, 3 ),"I~m~presoras", b) );

		b = new TInputLine( TRect( 30, 6, 55, 7 ), 50 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 29,5,55,6),"~I~mpresora",b));
		dptr->insert( new TStaticText( TRect( 30, 2, 55, 5 ),"\003Si su impresora no esta\n\003en la lista por favor\n\003agreguela aca." ) );

		dptr->insert( new TButton( TRect( 4, 9, 56, 11) ,"~O~K",
						cmOK, bfDefault ) );

		dptr->insert( new TButton( TRect( 4,11, 56,13)," ~S~ugerencias",
						cmYes,bfNormal ) );

		dptr->options |= ofCentered;

		dptr->setData( &datos.t_impresora );

		ushort control = deskTop->execView( dptr );

		switch( control )
		{
			case cmOK:
				dptr->getData( &datos.t_impresora );
				SetEnvi();
				destroy( dptr );
				break;
			case cmYes:
				destroy( dptr );
				Sugerencia();
				break;
			default:
				destroy( dptr );
				break;
		}
	}
//	destroy( dptr );
}

void MyProg::Sugerencia()
{
	TDialog *dptr = new TDialog ( TRect ( 0, 1, 70, 20),"Sugerencia" );
	if ( dptr )
	{
		TInputLine *b = new TInputLine( TRect( 4, 4, 66, 5 ), 62 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 3, 34, 4 ),"~N~ombre y Apellido", b ) );

		b = new TInputLine( TRect( 4, 7, 66, 8 ), 62 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 6, 34 ,7 ),"~D~ependencia", b ) );

		b = new TInputLine( TRect( 4, 10, 66, 11 ), 62 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 9, 34, 10 ),"~T~elefono", b ) );

		b = new TInputLine( TRect( 4, 13, 66, 14 ), 62 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 3, 12, 34, 13 ),"~S~ugerencia", b ) );
		dptr->insert( new TInputLine( TRect( 4, 14, 66, 15 ), 62 ) );

		dptr->insert( new TButton( TRect( 3, 16, 34, 18) ,"~O~K",
						cmOK, bfDefault ) );

		dptr->insert( new TButton( TRect( 36, 16, 66, 18) ,"~C~ancelar",
						cmCancel, bfNormal ) );

		dptr->options |= ofCentered;

		dptr->setData(&sugerir.nombre);

		ushort control = deskTop->execView( dptr );

		if ( control != cmCancel )  // si pulso OK, entonces...
			{
				dptr->getData(&sugerir.nombre);
			}
	}
	destroy( dptr );
}

void MyProg::SetEnvi()
{
	switch( datos.t_impresora ){
		case 0:
			putenv("ICARO=LX810");
			break;
		case 1:
			putenv("ICARO=HPLASER");
			break;
		case 2:
			putenv("ICARO=IBMPRO");
			break;
		case 3:
			putenv("ICARO=NONE");
			break;
		default:
			putenv("ICARO=NONE");
		}
}

//++++++++++++++++
//  Salir a Dos.
//++++++++++++++++

// DialogBox de salir
void MyProg::Salir()
{
	if(cmYes == messageBox("\003� Seguro que quiere salir ?",mfYesButton | mfNoButton | mfConfirmation ) )
	{
		endModal( cmQuit);
	}
}


// ++++++++++++++
// Funcion Principal  MAIN
// ++++++++++++++

int main()
{
	MyProg myprog;
	myprog.run();
	return 0;
}
