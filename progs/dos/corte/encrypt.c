// encriptador de claves.
#include <stdlib.h>
#include <stdio.h>

void Encrypt(char passwd[] ){
	int i;
	for(i=0;;i++){
		if(passwd[i]==0) break;
		passwd[i]^=0x03;
	}
}

void main(int argc,char *argv[]){
	if(argc>2){
		Encrypt(argv[2]);
		printf("%s:%s:",argv[1],argv[2]);
		}
	else{
		printf("\nGenerador de claves para el CORTE LOGIN"
				"\nModo de uso:"
				"\nencrypt USERNAME PASSWORD");
		}
}
