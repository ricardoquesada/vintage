{Implementaci�n de Colas con Arreglos (Circular).}
{Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a}

unit Ucolas2;

interface

uses Udatos;

const MaxCola = 20;

type
	TipoCola = record
		frente, final: integer;
		elementos: array[1..MaxCola] of TipoDato;
	end;

function PosSig(i: integer): integer;
function ColaVacia(C: TipoCola): boolean;
function ColaLlena(C: TipoCola): boolean;
procedure CrearCola(var C: TipoCola);
procedure SacarDeCola(var C: TipoCola; var x: TipoDato);
procedure PonerEnCola(var C: TipoCola; x: TipoDato);

implementation

function PosSig(i: integer): integer;
begin
	PosSig := (i Mod MaxCola) + 1
end;

procedure CrearCola(var C: TipoCola);
begin
	C.frente := 1;
	C.final := MaxCola;
end;

function ColaVacia(C: TipoCola): boolean;
begin
	ColaVacia := PosSig(C.final) = C.frente;
end;

function ColaLlena(C: TipoCola): boolean;
begin
	ColaLlena := PosSig(PosSig(C.final)) = C.frente;
end;

procedure SacarDeCola(var C: TipoCola; var x: TipoDato);
begin
	x := C.elementos[C.frente];
	C.frente := PosSig(C.frente);
end;

procedure PonerEnCola(var C: TipoCola; x: TipoDato);
begin
	C.final := PosSig(C.final);
	C.elementos[C.final] := x;
end;

end.

