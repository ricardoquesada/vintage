{Implementaci�n de Colas con Arreglos (Lineal).}
{Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a}

unit Ucolas1;

interface

uses Udatos;

const MaxCola = 20;

type
	TipoCola = record
		final: 0..MaxCola;
		elementos: array[1..MaxCola] of TipoDato;
	end;

function ColaVacia(C: TipoCola): boolean;
function ColaLlena(C: TipoCola): boolean;
procedure CrearCola(var C: TipoCola);
procedure SacarDeCola(var C: TipoCola; var x: TipoDato);
procedure PonerEnCola(var C: TipoCola; x: TipoDato);

implementation

procedure CrearCola(var C: TipoCola);
begin
	C.final := 0;
end;

function ColaVacia(C: TipoCola): boolean;
begin
	ColaVacia := C.final = 0;
end;

function ColaLlena(C: TipoCola): boolean;
begin
	ColaLlena := C.final = MaxCola;
end;

procedure SacarDeCola(var C: TipoCola; var x: TipoDato);
var
	i: integer;
begin
	x := C.elementos[1];
	for i := 1 to C.final - 1 do C.elementos[i] := C.elementos[i+1];
	C.final := C.final - 1;
end;

procedure PonerEnCola(var C: TipoCola; x: TipoDato);
begin
	C.final := C.final + 1;
	C.elementos[C.final] := x;
end;

end.

