{Implementaci�n de Listas con Arreglos (Implementaci�n en Memoria Continua).}
{Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a}

program Listas;

uses Crt, Udatos, Upilas, Ucolas1, Ulistas1;

var
	C: TipoCola;
	A, L: TipoLista;

procedure IniLista(var L: TipoLista);
{Comentario: }{Los elementos de las pilas son enteros positivos. Si alguna de las pilas
se encuentra vac�a, este estado ser� puesto de manifiesto poniendo un cero en la cola
resultante. Para el caso dado a continuaci�n la respuesta es: 0, 5, 6.}
var
	P: TipoPila;
begin
	CrearPila(P);
	PonerEnPila(P, 6); PonerEnPila(P, 2); PonerEnPila(P, 3);
	InserFin(L, P);
	CrearPila(P);
	InserFin(L, P);
	PonerEnPila(P, 4); PonerEnPila(P, 5); PonerEnPila(P, 1); PonerEnPila(P, 5);
	InserFin(L, P);
end;

function MaxEnPila(P: TipoPila): TipoDato;
{Precondici�n: }{La pila P no puede estar vac�a.}
var
	x, max: TipoDato;
begin
	SacarDePila(P, x);
	max := x;
	while not PilaVacia(P) do begin
		SacarDePila(P, x);
		if x > max then max := x;
	end;
	MaxEnPila := max;
end;

procedure GenListaAux(L: TipoLista; var A: TipoLista);
var
	P, X: TipoPila;
begin
	if not ListaVacia(L) then begin
		Primero(L);
		while not FinLista(L) do begin
			CrearPila(X);
			Info(L, P);
			if not PilaVacia(P) then begin
				PonerEnPila(X, MaxEnPila(P));
				InserOrden(A, X, 'a');
			end
			else begin
				PonerEnPila(X, Nulo);
				InserOrden(A, X, 'a');
			end;
			Siguiente(L);
		end;
	end;
end;

procedure GenCola(A: TipoLista; var C: TipoCola);
var
	X: TipoPila;
begin
	if not ListaVacia(A) then begin
		Primero(A);
		while not FinLista(A) do begin
			Info(A, X);
			PonerEnCola(C, Top(X));
			Siguiente(A);
		end;
	end;
end;

procedure MostCola(C: TipoCola);
var
	x: TipoDato;
begin
	while not ColaVacia(C) do begin
		SacarDeCola(C, x);
		write(x); write(' ');
	end;
end;

procedure Titulo;
begin
	writeln;
	write('Algoritmos y Estructuras de Datos I - Universidad CAECE');
	writeln; writeln;
	write('[1999] Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a');
	writeln; writeln; writeln;
	write('IMPLEMENTACION DE LISTAS BASADA EN ARREGLOS');
	writeln; writeln;
	write('Parcial 2, Tema 3, Ejercicio 1: Se muestra la soluci�n de un caso.');
	writeln; writeln; writeln; writeln;
end;

begin
	clrscr;
        Titulo;
	CrearCola(C); CrearLista(L); CrearLista(A);
	IniLista(L);
	if not ListaVacia(L) then begin
		GenListaAux(L, A);
		GenCola(A, C);
		MostCola(C);
	end;
end.

