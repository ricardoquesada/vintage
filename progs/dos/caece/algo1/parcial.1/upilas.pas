{Implementaci�n de Pilas con Arreglos (Implementaci�n en Memoria Continua).}
{Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a}

unit Upilas;

interface

uses Udatos;

const MaxPila = 20;

type
	TipoPila = record
		cima: 0..MaxPila;
		elementos: array[1..MaxPila] of TipoDato;
	end;

function PilaVacia(P: TipoPila): boolean;
function PilaLlena(P: TipoPila): boolean;
function Top(P: TipoPila): TipoDato;
procedure CrearPila(var P: TipoPila);
procedure SacarDePila(var P: TipoPila; var x: TipoDato);
procedure PonerEnPila(var P: TipoPila; x: TipoDato);

implementation

procedure CrearPila(var P: TipoPila);
begin
	P.cima := 0;
end;

function PilaVacia(P: TipoPila): boolean;
begin
	PilaVacia := P.cima = 0;
end;

function PilaLlena(P: TipoPila): boolean;
begin
	PilaLlena := P.cima = MaxPila;
end;

function Top(P: TipoPila): TipoDato;
begin
	Top := P.elementos[P.cima];
end;

procedure SacarDePila(var P: TipoPila; var x: TipoDato);
begin
	x := P.elementos[P.cima];
	P.cima := P.cima - 1;
end;

procedure PonerEnPila(var P: TipoPila; x: TipoDato);
begin
	P.cima := P.cima + 1;
	P.elementos[P.cima] := x;
end;

end.

