{Implementaci�n de Listas con Arreglos (Implementaci�n en Memoria Continua).}
{Maximiliano Sosa; Ricardo Quesada; Gonzalo J. Garc�a}

unit Ulistas1;

interface

uses Upilas;

const MaxLista = 20;

type
	TipoLista = record
		tope: 0..MaxLista;
		actual: 0..MaxLista + 1;
		elementos: array[1..MaxLista] of TipoPila;
	end;

function ListaVacia(L: TipoLista): boolean;
function ListaLlena(L: TipoLista): boolean;
function FinLista(L: TipoLista): boolean;
procedure CrearLista(var L: TipoLista);
procedure DestruirLista(var L: TipoLista);
procedure Primero(var L: TipoLista);
procedure Siguiente(var L: TipoLista);
procedure Ubicar(var L: TipoLista; x: TipoPila; var b: boolean);
procedure Info(L: TipoLista; var x: TipoPila);
procedure Borrar(var L: TipoLista; p: integer);
procedure Insertar(var L: TipoLista; x: TipoPila; p: integer);
procedure InserCabeza(var L: TipoLista; x: TipoPila);
procedure InserFin(var L: TipoLista; x: TipoPila);
procedure InserOrden(var L: TipoLista; x: TipoPila; ord: char);
procedure Modificar(var L: TipoLista; x: TipoPila);

implementation

procedure CrearLista(var L: TipoLista);
begin
	L.tope := 0;
end;

procedure DestruirLista(var L: TipoLista);
begin
	L.tope := 0;
end;

function ListaVacia(L: TipoLista): boolean;
begin
	ListaVacia := L.tope = 0;
end;

function ListaLlena(L: TipoLista): boolean;
begin
	ListaLlena := L.tope = MaxLista;
end;

function FinLista(L: TipoLista): boolean;
begin
	FinLista := L.actual = L.tope + 1;
end;

procedure Primero(var L: TipoLista);
begin
	L.actual := 1;
end;

procedure Siguiente(var L: TipoLista);
begin
	L.actual := L.actual + 1;
end;

procedure Ubicar(var L: TipoLista; x: TipoPila; var b: boolean);
var
	i: integer;
begin
	i := L.actual;
	while (i <= L.tope) and (Top(L.elementos[i]) <> Top(x)) do i := i +1;
	if i > L.tope then begin
		L.actual := 0;
		b := False;
	end
	else begin
		L.actual := i;
		b := True;
	end
end;

procedure Info(L: TipoLista; var x: TipoPila);
begin
	x := L.elementos[L.actual];
end;

procedure Borrar(var L: TipoLista; p: integer);
var
	i: integer;
begin
	L.tope := L.tope - 1;
	for i := p to L.tope do L.elementos[i] := L.elementos[i+1];
end;

procedure Insertar(var L: TipoLista; x: TipoPila; p: integer);
var
	i: integer;
begin
	for i := L.tope downto p do L.elementos[i+1] := L.elementos[i];
	L.tope := L.tope + 1;
	L.elementos[p] := x;
end;

procedure InserCabeza(var L: TipoLista; x: TipoPila);
var
	i: integer;
begin
	for i := L.tope downto 1 do L.elementos[i+1] := L.elementos[i];
	L.tope := L.tope + 1;
	L.elementos[1] := x;
end;

procedure InserFin(var L: TipoLista; x: TipoPila);
begin
	L.tope := L.tope + 1;
	L.elementos[L.tope] := x;
end;

procedure InserOrden(var L: TipoLista; x: TipoPila; ord: char);
var
	i: integer;
begin
	if ord = 'a' then begin
		i := L.tope;
		while (i >= 1) and (Top(x) < Top(L.elementos[i])) do begin
			L.elementos[i+1] := L.elementos[i];
			i := i - 1;
		end;
		L.elementos[i+1] := x;
	end
	else if ord = 'd' then begin
		i := L.tope;
		while (i >= 1) and (Top(x) > Top(L.elementos[i])) do begin
			L.elementos[i+1] := L.elementos[i];
			i := i - 1;
		end;
		L.elementos[i+1] := x;
	end
end;

procedure Modificar(var L: TipoLista; x: TipoPila);
begin
	L.elementos[L.actual] := x;
end;

end.

