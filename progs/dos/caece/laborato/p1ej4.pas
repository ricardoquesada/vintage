{ Practica 1 Ejercicio 4 }
program p1e4;
uses strings;

function es_vocal( a: char ): Boolean ;
begin
     es_vocal := false ;
     if(a='a') or (a='A') or
       (a='e') or (a='E') or
       (a='i') or (a='I') or
       (a='o') or (a='O') or
       (a='u') or (a='U') then
         es_vocal := true;
end;


var
  i,j,vocales : integer;
  s_temp,s : String;
begin
  Write('Entre un texto por favor: ');
  Readln(s);
  Writeln('Texto entrado: ',s);
  Write('Palabras con mas de 2 vocales: ');
  j:=0;
  vocales := 0;

  for i:=1 to Length(S) do
  begin
       if( S[i] = ' ') or ( S[i]=',' )then
       begin
            if( vocales > 2 ) then
            begin
                 write( s_temp );
                  write(' ');
            end;
            j:=0;
            vocales := 0;
       end
       else
       begin
           j := j+1;
           s_temp[j] := s[i];
           s_temp[0] := char(j);
           if( es_vocal( S[i] ) ) then
               vocales := vocales + 1;
       end;
  end;
  if( vocales > 2 ) then
    write( s_temp );

  Writeln('');
  Writeln('Pulse <ENTER> para salir');
  Readln;
end.