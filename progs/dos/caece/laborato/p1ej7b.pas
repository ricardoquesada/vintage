{ Practica 1 Ejercicio 7b }
program p1e7b;
uses strings,Crt;

type arreglo100 = array[1..100] of integer;

procedure intercambio( var a,b: integer );
var
   aux :integer;
begin
     aux := a;
     a := b;
     b := aux;
end;

procedure busca_minimo_y_reemplaza(var s: arreglo100; a: integer);
var
   i,pos_min: Integer;
begin
     pos_min := 1;
     for I := 2 to 100 do
     begin
          if s[i] < s[pos_min] then
             pos_min := i ;
     end;
     writeln('minimo: ',s[pos_min] );
     intercambio( s[pos_min], a );
end;


var
  i: integer;
  nuevo: integer;
  arreglo : arreglo100;

begin
  Randomize;
  for i:=1 to 100 do
    arreglo[i] := Random(1000);
  nuevo := Random(1000);
  busca_minimo_y_reemplaza(arreglo,nuevo);
end.
