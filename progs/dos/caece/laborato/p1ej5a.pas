{ Practica 1 Ejercicio 5a }
program p1e5a;
uses strings;

var
  i,j : integer;
  s_temp,s : String;
begin
  Write('Entre un texto por favor: ');
  Readln(s);
  Writeln('Texto entrado: ',s);
  j:=0;

  for i:=1 to Length(S) do
  begin
       if( S[i] = ' ') or ( S[i]=',' ) or (S[i]='.') then
       begin
            if( j > 2 ) then
            begin
                 write( s_temp );
                 write(' ');
            end;
            j:=0;
       end
       else
       begin
           j := j+1;
           s_temp[j] := s[i];
           s_temp[0] := char(j);
       end;
  end;

  Writeln('');
  Writeln('Pulse <ENTER> para salir');
  Readln;
end.