{ Practica 1 Ejercicio 7a }
program p1e7a;
uses strings,Crt;

type arreglo100 = array[1..100] of integer;

procedure intercambio( var a,b: integer );
var
   aux :integer;
begin
     aux := a;
     a := b;
     b := aux;
end;

function Pos_del_Menor( S: arreglo100; I: integer ): integer;
var
   pos,j: Integer;
begin
     pos := i ;
     for j:= i+1 to 100 do
         if( S[j] < S[pos] ) then
             pos := j;
     pos_del_menor := pos;
end;

procedure Ordenar(var s: arreglo100 );
var
   I,J: Integer;
begin
     for I := 1 to 99 do
     begin
          j := Pos_del_Menor( S, i );
          if( i <> j ) then
              Intercambio( S[i], S[j] );
     end;
end;


var
  i: integer;
  arreglo : arreglo100;

begin
  Randomize;
  for i:=1 to 100 do
    arreglo[i] := Random(1000);
  ordenar(arreglo);
  for i:=1 to 100 do
      write(arreglo[i],' ');
end.
