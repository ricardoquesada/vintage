{ Practica 1 Ejercicio 6c }
program p1e6c;
uses strings;

function es_vocal( a: char ): Boolean ;
begin
     es_vocal := false ;
     if(a='a') or (a='A') or
       (a='e') or (a='E') or
       (a='i') or (a='I') or
       (a='o') or (a='O') or
       (a='u') or (a='U') then
         es_vocal := true;
end;

var
  i,j : integer;
  s_temp,s : String;
  palabra_va: Boolean;


begin
  Write('Entre un texto por favor: ');
  Readln(s);
  Writeln('Texto entrado: ',s);
  j:=0;
  palabra_va := False;

  for i:=1 to Length(S) do
  begin
       if( S[i] = ' ') or ( S[i]=',') or (S[i]='.') then
       begin
           j:=0;
           if palabra_va then
           begin
              write(' ',s_temp);
              palabra_va := false ;
           end;
       end
       else
       begin
           j := j+1;
           s_temp[j] := s[i];
           s_temp[0] := char(j);
           if( j <= 3 ) and ( es_vocal(s[i] )) then
               palabra_va := True;
       end;
  end;

  Writeln('');
  Writeln('Pulse <ENTER> para salir');
  Readln;
end.