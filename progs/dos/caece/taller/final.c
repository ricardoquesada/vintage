#include <stdlib.h>
#include <stdio.h>
#include <string.h>


typedef struct nodo {
	struct nodo *next_a;	/* next alfabetico */
	struct nodo *next_c;	/* next carnet */
	struct nodo *next_n;	/* next nota */
	char nombre[40];
	long carnet;
	int nota;
} NODO, *pNODO;

pNODO ga_nodo=NULL;		/* ptr global alfa */
pNODO gc_nodo=NULL;		/* ptr global carnet */
pNODO gn_nodo=NULL;		/* ptr global nota */

/* Insertar alfabeticamente */
void insertar_a(pNODO nodo_new)
{
	pNODO prev,n;

	if(!ga_nodo) {
		ga_nodo = nodo_new;
		return;
	}

	prev = NULL;

	for(n=ga_nodo;n;n=n->next_a) {
		if(strcmp(nodo_new->nombre,n->nombre)<0)
			break;
		prev=n;
	}

	if(n)
		nodo_new->next_a = n;
	if(prev)
		prev->next_a = nodo_new;
	else 
		ga_nodo = nodo_new;
}

/* Insertar por carnet */
void insertar_c(pNODO nodo_new)
{
	pNODO prev,n;

	if(!gc_nodo) {
		gc_nodo = nodo_new;
		return;
	}

	prev = NULL;

	for(n=gc_nodo;n;n=n->next_c) {
		if(nodo_new->carnet < n->carnet)
			break;
		prev=n;
	}

	if(n)
		nodo_new->next_c = n;
	if(prev) 
		prev->next_c = nodo_new;
	else
		gc_nodo = nodo_new;
}


/* Insertar por nota */
void insertar_n(pNODO nodo_new)
{
	pNODO prev,n;

	if(!gn_nodo) {
		gn_nodo = nodo_new;
		return;
	}

	prev = NULL;

	for(n=gn_nodo;n;n=n->next_n) {
		if(nodo_new->nota < n->nota)
			break;
		prev=n;
	}

	if(n)
		nodo_new->next_n = n;
	if(prev)
		prev->next_n = nodo_new;
	else
		gn_nodo = nodo_new;
}


void insertar(pNODO nodo_new)
{
	insertar_a(nodo_new);
	insertar_c(nodo_new);
	insertar_n(nodo_new);
}

void borrar_a(long carnet)
{
	pNODO n,prev;

	prev=NULL;
	
	for(n=ga_nodo;n;n=n->next_a) {
		if(n->carnet == carnet)
			break;
		prev = n;
	}
	if(!prev)
		ga_nodo = ga_nodo->next_a;
	else  {
		if(n)
			prev->next_a = n->next_a;
		else
			prev->next_a = NULL;
	}
}

void borrar_c(long carnet)
{
	pNODO n,prev;

	prev=NULL;

	
	for(n=gc_nodo;n;n=n->next_c) {
		if(n->carnet == carnet)
			break;
		prev = n;
	}
	if(!prev)
		gc_nodo = gc_nodo->next_c;
	else {
		if(n)
			prev->next_c = n->next_c;
		else
			prev->next_c = NULL;
	}
}
void borrar_n(long carnet)
{
	pNODO n;
	pNODO prev=NULL;

	
	for(n=gn_nodo;n;n=n->next_n) {
		if(n->carnet == carnet)
			break;
		prev = n;
	}
	if(!prev)
		gn_nodo = gn_nodo->next_n;
	else {
		if(n)
			prev->next_n = n->next_n;
		else
			prev->next_n = NULL;
	}
}

pNODO devolver(long carnet)
{
	pNODO n;

	for(n=gc_nodo;n;n=n->next_c){
		if(n->carnet == carnet)
			break;
	}
	if(!n)
		printf("Error: Nodo no encontrado!\n");

	return(n);
}

void borrar()
{
	pNODO n;
	long carnet;

	printf("Ingrese el numero de carnet a borrar:\n");
	scanf("%d",&carnet);

	n = devolver(carnet);
	borrar_a(carnet);
	borrar_c(carnet);
	borrar_n(carnet);

	free(n);
}

void crear()
{
	pNODO n;

	n = (pNODO) malloc(sizeof(NODO));
	memset(n,0,sizeof(NODO));

	printf("Ingrese nombre:\n");
	scanf("%s",n->nombre);
	printf("Ingrese carnet:\n");
	scanf("%d",&n->carnet);
	printf("Ingrese nota:\n");
	scanf("%d",&n->nota);

	insertar(n);
}

void listar_a()
{
	pNODO n;
	for(n=ga_nodo;n;n=n->next_a) {
		printf("Nombre: %s\n",n->nombre);
		printf("Carnet: %d\n",n->carnet);
		printf("Nota: %d\n",n->nota);
	}
}
void listar_c()
{
	pNODO n;
	for(n=gc_nodo;n;n=n->next_c) {
		printf("Nombre: %s\n",n->nombre);
		printf("Carnet: %d\n",n->carnet);
		printf("Nota: %d\n",n->nota);
	}
}
void listar_n()
{
	pNODO n;
	for(n=gn_nodo;n;n=n->next_n) {
		printf("Nombre: %s\n",n->nombre);
		printf("Carnet: %d\n",n->carnet);
		printf("Nota: %d\n",n->nota);
	}
}

char menu()
{
	printf("\n1-Creacion e Insercion de un nodo\n");
	printf("2-Eliminacion de un nodo\n");
	printf("3-Listar por orden de nombre\n");
	printf("4-Listar por orden de carnet\n");
	printf("5-Listar por orden de notas\n");
	printf("s-Salir\n");

	return( (char) getchar() );
}
int main()
{
	printf("Programa final para Taller de Programacion\n");


	while(1) {
		switch(menu())
		{
			case '1':
				crear();
				break;
			case '2':
				borrar();
				break;
			case '3':
				listar_a();
				break;
			case '4':
				listar_c();
				break;
			case '5':
				listar_n();
				break;
			case 's':
			case 'S':
				return 0;
		}
		getchar();
	}
	return 0;
}
