#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define A_SIZE	50
struct agenda {
	char nombre[30];
	char telefono[15];
	char direccion[50];
};

struct	agenda Agenda[A_SIZE];
int	i_a=0;

void init()
{
	int i;

	for(i=0;i<A_SIZE;i++)
		memset( &Agenda[i],0,sizeof(struct agenda));
}

void ingresar()
{
	struct agenda a;

	printf("Ingrese un nombre: ");
	scanf("%s",a.nombre);
	printf("Ingrese un telefono: ");
	scanf("%s",a.telefono);
	printf("Ingrese una direccion: ");
	scanf("%s",a.direccion);

	memmove( &Agenda[i_a], &a, sizeof(struct agenda));
	i_a++;
}

void reportar()
{
	int i;
	for(i=0;i<i_a;i++) {
		if((strncmp(Agenda[i].telefono,"801",3)==0) && Agenda[i].nombre[0]=='a')
			printf("Nombre: %s\nTelefono: %s\nDireccion: %s\n"
					,Agenda[i].nombre
					,Agenda[i].telefono
					,Agenda[i].direccion
					);
	}
}

int main()
{
	int a;
	do {
		printf("\n1 - Ingresar datos\n2 - Para mostrar datos\ns - Para salir\n");
		a = getchar();
		if((char)a=='1') ingresar();
		if((char)a=='2') reportar();
	} while ( (char) a!='s');

	return 0;
}
