// Garcha para leer garcha (echovol.dat)

/* Program to create backup of the AUTOEXEC.BAT file */

#include <stdio.h>

int main(void)
{
	int out=0,i=0;
	char nombre[40];
//	char bytes[4],msgs[4];
//	unsigned long bytes,msgs;
//	unsigned long prueba;

	FILE *in;

	if ((in = fopen("ECHOVOL.DAT", "rb"))
		 == NULL)
	{
		fprintf(stderr, "Cannot open input file.\n");
		return 1;
	}

	fseek(in,5L,SEEK_CUR);

	do{
		i=0;
		while ( (nombre[i++]=fgetc(in)) !=0 );
		printf("\n%s",nombre);
		fseek(in,9L,SEEK_CUR);
		if (feof(in)) out=1;
	} while ( out==0 );

	fclose(in);
	return 0;
}
