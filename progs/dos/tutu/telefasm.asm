;rutinas de assembler para telefono.exe
;Hechas por Ricardo Quesada
;para funcionar con serial...
;Para el terminal dedicado

	 DOSSEG
	 .MODEL large   ; Select small model (near code and data)
	 .CODE

PUBLIC _mandarstr_asm

;***********************************;
;                                   ;
;_mandarstr_asm                     ;
;Manda un cadena al com             ;
;C prototype:                       ;
;int mandarstr_asm( int , char far * ) ;
;                                   ;
;***********************************;

_mandarstr_asm PROC

		push bp
		mov bp,sp
		push ds
		push si
		push di

		mov dx,[bp+6]           ;numero de com
		mov si,[bp+8]           ;offset of string
		mov di,si
		mov ax,[bp+10]          ;segment of string
		mov ds,ax


loop1:
		cmp byte ptr [si],0     ;lenguaje C termina los strings
		je bien
		cmp [si],4D5Eh          ;control + M
		je linea

		mov ah,1
		mov al,[si]             ;send one char to com
		int 14h

		and ah,10000000b
		cmp ah,10000000b
		je error

		inc si
		jmp short loop1

linea:
		mov al,13
		mov ah,1
		int 14h

		inc si
		inc si

		jmp loop1

error:
		mov byte ptr ax,1
		jmp final

bien:
		xor ax,ax

final:
		pop di
		pop si
		pop ds
		pop bp
		ret

_mandarstr_asm ENDP


PUBLIC _mandarchar_asm
;***********************************;
;                                   ;
;_mandarchar_asm                    ;
;Manda un char   al com             ;
;C prototype:                       ;
;int mandarchar_asm( int , char )   ;
;                                   ;
;***********************************;

_mandarchar_asm PROC

		push bp
		mov bp,sp
		push ds
		push si
		push di

		mov dx,[bp+6]           ;numero de com
		mov al,[bp+8]           ;char


		mov ah,1
		int 14h                 ;send one char to com X

		and ah,10000000b
		cmp ah,10000000b
		jne bien2


		mov byte ptr ax,1
		jmp short final2

bien2:
		xor ax,ax

final2:
		pop di
		pop si
		pop ds
		pop bp
		ret

_mandarchar_asm ENDP


PUBLIC _recivirchar_asm
;*****************************************
;
;C prototye
; unsigned recivirchar_asm( int com )
;
;devulve el ax=ffff si el char no esta available o si hubo error
;else en AL esta el verdadero char...
;******************************************
_recivirchar_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
		mov ah,02h
		int 14h                         ;devuelve en ax el char available

		and ah,10000000b
		cmp ah,10000000b
		jne si_recivi                   ;char bien recivido

		mov ax,0ffffh                   ;devuelve char not available

si_recivi:
		pop bp
		ret                             ;devuelve el char

_recivirchar_asm ENDP

PUBLIC _dcd_asm
;***********************************
;
;detecta si se conecto
;C prototype
;int dcd_asm( int );
;ax=0 si conecto
;
;**************************************
_dcd_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]
		mov ah,3
		int 14h                 ;detect carrier

		and al,10000000b
		cmp al,10000000b
		je carrier

		mov byte ptr ax,1
		jmp fin_dcd

carrier:
		xor ax,ax
fin_dcd:
		pop bp
		ret

_dcd_asm ENDP



PUBLIC _init_asm
;***********************************
;
;Inicializa el modem a nivel Bios.
;C prototype
;int init_asm( int baudios,int paridad,int stopbit,int databit, int com);
;ax=0 OK
;
;**************************************

_init_asm PROC

		push bp
		mov bp,sp
		push ds

		xor ax,ax

		mov dx,[bp+6]                           ;baudios
		shl dl,5                                ;
		or al,dl

		mov dx,[bp+8]                           ;paridad
		shl dl,3                                ;
		or al,dl

		mov dx,[bp+10]                          ;stopbit
		shl dl,2
		or al,dl

		mov dx,[bp+12]                          ;databit
		or al,dl
		mov dx,[bp+14]                          ;com
		mov ah,0

		int 14h

		and ah,10000000b
		cmp ah,10000000b

		jne noerrorah

grosoerror:
		mov ax,0ffffh

		jmp findeinit
noerrorah:

		mov bx,[bp+14]
		shl bx,1		;multiplica por 2

		push cs
		pop ds

		mov dx,0040h
		mov ds,dx

		cmp byte ptr ds:[bx],0000h
		je grosoerror

		xor ax,ax

findeinit:
		pop ds
		pop bp
		ret

_init_asm ENDP

PUBLIC _detectcom_asm
;***********************************
;
;Detecta el com en el que esta el modem
;C prototype
;int detectcom_asm( void );
;ax=com.
;
;**************************************
_detectcom_asm PROC

		xor si,si  			;empieza con el com1.
mainl:
		call cs:prueba1
		cmp ax,0000h		;AX=0 ERROR
		je malpr

		call cs:prueba2
		cmp ax,0000h        ;AX=0 OK
		je comokok

malpr:
		inc si
		cmp si,4
		jb mainl

		mov ax,1234h
		jmp findepruebas

comokok:

		mov ax,si
		inc ax

findepruebas:
		ret


_detectcom_asm ENDP

prueba1 PROC

		push ds

		mov bx,si
		shl bx,1		;multiplica por 2

		push cs
		pop ds

		mov dx,0040h
		mov ds,dx

		mov ax,ds:[bx]

		pop ds
		ret

prueba1 ENDP

prueba2 PROC

		mov dx,si
		mov ah,1
		mov al,13
		int 14h

		xor al,al
		and ah,10000000b

		ret

prueba2 ENDP

PUBLIC _dtr_asm
;***********************************
;
;DTR. NIVEL PORT
;C prototype
;int dtroff_asm( int com-1 , int cmd);
;cmd = 0: off
;    = 1: on
;
;devuelve ax=0 todo OK.
;**************************************
_dtr_asm PROC

		push bp
		mov bp,sp

		push ds

		mov bx,[bp+6]                           ;Com
		mov cx,[bp+8]

		xor bh,bh
		shl bl,1                    ;multiplica por 2
		mov ax,40h
		mov ds,ax
		mov ax,[bx]
		push cs
		pop ds
		cmp ax,0
		jne loc_10                  ; Jump if not equal

		mov ax,0ffffh               ; -1
		jmp findtr

loc_10:
		mov dx,ax
		add dx,4
		in al,dx        ; port 3, DMA-1 bas&cnt ch 1

		cmp cl,0
		je dtroff

dtron:
		or al,3          	;DTR ON
		out dx,al
		jmp todo_dtr_bien

dtroff:
		and al,0fch         ;DTR OFF
		out dx,al       ;

todo_dtr_bien:
		xor ax,ax
findtr:

		pop ds
		pop bp

		ret

_dtr_asm ENDP

end
