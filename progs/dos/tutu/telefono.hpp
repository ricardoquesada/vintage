#define Uses_TApplication
#include <tv.h>

class MyProg : public TApplication
{

	public:

	struct CONFIG
	{
		char   Cabecera[8];
		char   Usuario[30];
		ushort radioBaudio;
		ushort radioCom;
		ushort radioParidad;
		ushort radioStopbit;
		ushort radioDatabit;
		char   inputPrefijo[20];
		char   inputSufijo[20];
		char   inputInit[40];
		char   inputHang[40];
		char   inputTiempo[4];
		char   inputTelefono[20];
	};


	private:

	void About();
	void Modem();
	void Linea();
	void Salir();
	void Abrir();
	void Salvar();
	void Default();
	void Inic();
	void Hang();
	void Dial(int);
	void Connect();
	void Recibir();
	void Mandar();
	void Terminal();
	int Esperar();
	void DetectCom();
	void Shell();

public:

	CONFIG *Config;

	MyProg();
	static TStatusLine *initStatusLine( TRect r );
	static TMenuBar *initMenuBar( TRect r );
	virtual void handleEvent( TEvent& event );

};
