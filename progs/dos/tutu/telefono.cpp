//
// Tu - Tu
// version Beta 1.00
//
// Hecho en C , C++ , Librerias TVision
// Borland C++ 3.0 & Application Frameworks
//
// Ricardo Quesada 1994
//
// por precauci�n compilar sin QEMM...
// vio, por las dudas...
//

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TDialog
#define Uses_TButton
#define Uses_TLabel
#define Uses_TInputLine
#define Uses_TCheckBoxes
#define Uses_TSItem
#define Uses_TView
#define Uses_TRadioButtons
#define Uses_TMenuBar
#define Uses_TStatusLine
#define Uses_TRect
#define Uses_TStatusDef
#define Uses_TSubMenu
#define Uses_TKeys
#define Uses_TStatusItem
#define Uses_TDeskTop
#define Uses_TMenuItem
#define Uses_MsgBox

#define HEADER "T:1.00"

#include <stdlib.h>
#include <string.h>
#include <io.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <dos.h>
#include <conio.h>
#include "telefono.hpp"

const int cmAbout   = 100;
const int cmLinea   = 101;
const int cmModem   = 102;
const int cmSalir   = 103;
const int cmSalvar  = 104;
const int cmAbrir	= 105;
const int cmInit    = 106;
const int cmHang   	= 107;
const int cmDial	= 108;
const int cmTerm	= 109;
const int cmCom		= 110;
const int cmRedial  = 111;
const int cmShell	= 112;

extern "C" {
	int mandarstr_asm( int , char far * );
	unsigned recivirchar_asm( int );
	int init_asm( int , int , int , int , int );
	int dcd_asm( int );
	int dtr_asm( int , int );
	int detectcom_asm( void );
}

void Cucu(int,int,int,int,int,int);

MyProg::MyProg() :
	TProgInit( &MyProg::initStatusLine,
			   &MyProg::initMenuBar,
			   &MyProg::initDeskTop
			 )
{
	About();
	Abrir();
	Inic();
}

TStatusLine *MyProg::initStatusLine( TRect r)
{
	r.a.y = r.b.y - 1;
	return new TStatusLine( r,
		*new TStatusDef( 0, 0xFFFF ) +
			*new TStatusItem( "~Alt-X~ Salir",kbAltX, cmSalir ) +
			*new TStatusItem( "~F-10~ Menu",kbF10, cmMenu)
	);
}

TMenuBar *MyProg::initMenuBar( TRect r )
{
	r.b.y = r.a.y + 1;
	return new TMenuBar( r,
		*new TSubMenu( "~�~",kbAltSpace ) +
			*new TMenuItem("~A~cerca",cmAbout, kbF1, hcNoContext,"F1" ) +
		*new TSubMenu("~A~rchivos",kbAltA ) +
			*new TMenuItem("S~h~ell al DOS",cmShell,kbNoKey,hcNoContext ) +
			newLine() +
			*new TMenuItem("~S~alir",cmSalir,kbAltX, hcNoContext,"Alt-X") +
		*new TSubMenu("~S~eteos",kbAltS ) +
			*new TMenuItem("~L~inea...",cmLinea, kbAltL, hcNoContext,"Alt-L" ) +
			*new TMenuItem("~M~odem...",cmModem, kbNoKey, hcNoContext ) +
			newLine() +
			*new TMenuItem("~S~alvar",cmSalvar, kbNoKey, hcNoContext ) +
		*new TSubMenu("~C~omandos",kbAltC ) +
			*new TMenuItem("~I~nicializar",cmInit, kbNoKey, hcNoContext ) +
			*new TMenuItem("~H~angup",cmHang, kbAltH, hcNoContext,"Alt-H" ) +
			*new TMenuItem("~D~iscar",cmDial,kbAltD,hcNoContext,"Alt-D")  +
			*new TMenuItem("~R~ediscar",cmRedial,kbAltR,hcNoContext,"Alt-R") +
			newLine() +
			*new TMenuItem("~T~erminal",cmTerm,kbAltT,hcNoContext,"Alt-T") +
			newLine() +
			*new TMenuItem("~D~etectar Com",cmCom,kbNoKey,hcNoContext )
	);
}

void MyProg::handleEvent ( TEvent& event )
{
	TApplication::handleEvent( event );
	if ( event.what == evCommand )
		{
		switch( event.message.command)
			{
			case cmAbout:
				About();
				break;
			case cmModem:
				Modem();
				break;
			case cmLinea:
				Linea();
				break;
			case cmSalvar:
				Salvar();
				break;
			case cmSalir:
				Salir();
				break;
			case cmInit:
				Inic();
				break;
			case cmHang:
				Hang();
				break;
			case cmDial:
				Dial(0);
				break;
			case cmRedial:
				Dial(1);
				break;
			case cmTerm:
				Terminal();
				break;
			case cmCom:
				DetectCom();
				break;
			case cmShell:
				Shell();
				break;
			default:
				return;
			}
		clearEvent( event );
		}
}

// carga los valores por defecto
void MyProg::Default()
{
	strcpy( Config->Cabecera , HEADER );
	strcpy( Config->Usuario ,"*** NO REGISTRADO ***" );
	Config->radioBaudio  = 5;    			//2400
	Config->radioCom 	 = 0;               //COM1
	Config->radioParidad = 0;               //NONE
	Config->radioStopbit = 0;               //1
	Config->radioDatabit = 3;               //8
	strcpy( Config->inputPrefijo ,"ATDP");
	strcpy( Config->inputSufijo ,"^M");
	strcpy( Config->inputInit ,"ATZ^M");
	strcpy( Config->inputHang ,"^#~~~+++~~~ATH^M");
	strcpy( Config->inputTiempo,"45" );
	strcpy( Config->inputTelefono ,"746-6666" );
}

// Manda cadena de inicializacion al modem.
void MyProg::Inic()
{
	if ( init_asm( Config->radioBaudio , Config->radioParidad , Config->radioStopbit , Config->radioDatabit , Config->radioCom ) == 0 )
	{
		if ( mandarstr_asm( Config->radioCom , Config->inputInit ) == 0)
		{
			messageBox("\003Modem inicializado",mfOKButton | mfInformation );
			return;
		}
	}
	messageBox("\003Error al inicializar el modem.",mfOKButton | mfError );
}

// Cuelga la comunicacion
void MyProg::Hang()
{
	if ( dtr_asm( Config->radioCom , 0 ) == 0 ) 	 // DTR OFF.
		messageBox("\003DTR off",mfOKButton | mfInformation );
	else { messageBox("\003No se pudo quitar el DTR.",mfOKButton | mfError ); }

	// mandarstr_asm( Config->radioCom , Config->inputHang );
}




// +++++++++++++++++++++++++
//
// Funciones de Disk
//
// +++++++++++++++++++++++++

// Lee archivo de configuracion
void MyProg::Abrir()
{
   const unsigned int sizeofbuffer = sizeof( *Config );
   int handle;

   Default();  										// Si no se encuentra, se cargan los valores por defecto

   if ( (handle = _open("TUTU.cfg",O_RDWR ) ) == -1)
   {
		messageBox("\003El archivo TUTU.CFG no se encontr�. Se creara uno.",mfInformation | mfOKButton);
		if ( (handle = _creat("tutu.cfg", 0 )) == -1 )
		{
			messageBox("\003No se pudo crear el archivo",mfError | mfOKButton );
			return;
		}

			DetectCom();
			Salvar();
			close(handle);
			return;
   }

   if (_read(handle, Config , sizeofbuffer ) != sizeofbuffer )
   {
			messageBox("\003Se actualizara el archivo de configuracion.",mfInformation | mfOKButton);
			Salvar();
			close(handle);
			return;
   }

   if( 0 != strcmp( Config->Cabecera , HEADER ) )
   {
		messageBox("\003El archivo pertenece a otra version\n\003Se actualizara",mfError | mfOKButton );
		int a = Config->radioCom;
		Default();
		Config->radioCom = a;
		Salvar();
		close(handle);
		return;
   }
}

// Salva valores en archivo de configuracion
void MyProg::Salvar()
{
   const unsigned int sizeofbuffer = sizeof( *Config );
   int handle;

   if ( (handle = _creat("tutu.cfg",0 ) ) == -1)
   {
		messageBox("\003No se pudo abrir el archivo\n",mfError | mfOKButton );
		return;
   }

   if (_write(handle, Config , sizeofbuffer ) != sizeofbuffer )
   {
		close(handle);
		messageBox("\003No se pudo salvar satisfactoriamente la informacion.",mfError | mfOKButton);
		return;
   }

   close(handle);
   messageBox("\003Configuraci�n salvada.",mfInformation | mfOKButton);
   return ;
}


// ++++++++++++++++++
//
// ++++++++++++++++++

// acerca del programa
void MyProg::About()
{
	TDialog *aboutBox = new TDialog(TRect(0, 0, 39, 13), "Acerca del Programa");

	aboutBox->insert(
	  new TStaticText(TRect(2, 2, 37, 9),
		"\003Tu - Tu\n\003\n"
		"\003Version Beta 1.00c\n\003\n"
		"\003por Ricardo Quesada\n\003\n"
		"\003(c) 1994"
	  )

	);

	aboutBox->insert(
	  new TButton(TRect(14, 10, 26, 12), "OK", cmOK, bfDefault)
	  );

	aboutBox->options |= ofCentered;

	deskTop->execView(aboutBox);

	destroy( aboutBox );
}


// ++++++++++++++++++
// Funciones de entrada de datos
// +++++++++++++++++++++

void MyProg::Linea()
{
	TDialog *dptr = new TDialog ( TRect ( 0, 0, 46, 18),"Seteos de la linea" );
	if ( dptr )
	{
		TView *b = new TRadioButtons( TRect (5 ,3 ,26, 7 ),
			new TSItem( "110",
			new TSItem( "150",
			new TSItem( "300",
			new TSItem( "600",
			new TSItem( "1200",
			new TSItem( "2400",
			new TSItem( "4800",
			new TSItem( "9600", 0 ) ) ) ) ) ) ) )

		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 2, 26, 3 ),"~B~audios", b) );


	   b = new TRadioButtons( TRect ( 30, 3, 41, 7 ),
			new TSItem( "COM1",
			new TSItem( "COM2",
			new TSItem( "COM3",
			new TSItem( "COM4",0 ) ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 30, 2, 41, 3),"Co~m~", b) ) ;


	   b = new TRadioButtons( TRect ( 5, 10, 15, 13),
			new TSItem( "None",
			new TSItem( "Even",
			new TSItem( "Odd", 0 ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 9, 15, 10),"~P~aridad", b) );

		b = new TRadioButtons( TRect ( 18, 10, 28, 12 ),
			new TSItem( "1",
			new TSItem( "2",0 ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 18, 9, 28, 10),"~S~topBit", b) );

		b = new TRadioButtons( TRect ( 31, 10, 41, 14 ),
			new TSItem( "5",
			new TSItem( "6",
			new TSItem( "7",
			new TSItem( "8",0 ) ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 31, 9, 41, 10),"~D~atabit", b) );


		dptr->insert( new TButton( TRect( 4, 15, 20, 17) ,"~O~K",
						cmOK, bfDefault ) );


		dptr->insert( new TButton( TRect( 26, 15, 42, 17) ,"~C~ancelar",
						cmCancel, bfNormal ) );

		dptr->options |= ofCentered;
		dptr->setData( &Config->radioBaudio );

		ushort control = deskTop->execView( dptr );

		if ( control != cmCancel )
			dptr->getData( &Config->radioBaudio );
	}
	destroy( dptr );

}

// Dialog Box del seteo del Modem
void MyProg::Modem()
{

	TDialog *dptr = new TDialog ( TRect ( 0, 0, 67, 15),"Seteos del modem" );
	if ( dptr )
	{
		TInputLine *b = new TInputLine( TRect( 35, 4, 57, 5 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 4, 35, 5 ),"~P~refijo de llamada", b ) );

		b = new TInputLine( TRect( 35, 5, 57, 6 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 5, 35, 6 ),"~S~ufijo de llamada", b ) );

		b = new TInputLine( TRect( 35, 6, 57, 7 ), 40 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 6, 35, 7 ),"~I~nicializar", b ) );

		b = new TInputLine( TRect( 35, 7, 57, 8 ), 40 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 7, 35, 8 ),"~H~angup", b ) );

		b = new TInputLine( TRect( 35, 8, 57 , 9), 4 );
		dptr->insert( b);
		dptr->insert( new TLabel( TRect( 10, 8, 35, 9 ),"~T~iempo de espera", b ) );


		dptr->insert( new TButton( TRect( 10, 12, 25, 14) ,"~O~K",
						cmOK, bfDefault ) );

		dptr->insert( new TButton( TRect( 30, 12, 45, 14) ,"~C~ancelar",
						cmCancel, bfNormal ) );

		dptr->options |= ofCentered;
		dptr->setData( &Config->inputPrefijo );

		ushort control = deskTop->execView( dptr );

		if ( control != cmCancel )  // si pulso OK, entonces...
		{
			if( atoi(Config->inputTiempo) == 0 )
				strcpy(Config->inputTiempo,"45");
			dptr->getData( &Config->inputPrefijo );
		}
	}
	destroy( dptr );

}
//++++++++++++++++
//  Modo terminal.
//++++++++++++++++

void MyProg::Terminal()
{
	suspend();
	clrscr();
	::Cucu(Config->radioCom,Config->radioBaudio,Config->radioParidad,Config->radioDatabit,Config->radioStopbit,1);
	resume();
	redraw();
	mandarstr_asm( Config->radioCom, Config->inputHang );
}

//**************//
// Detectar Com //
//**************//
void MyProg::DetectCom()
{
	int quecom;
	char numero[3];
	char cadena[30]="\003Modem encontrado en COM ";

	quecom = detectcom_asm();

	itoa( quecom,numero,10 );

	strcat( cadena , numero);
	if( quecom < 5 )
	{
		messageBox( cadena ,mfOKButton | mfInformation);
		Config->radioCom = quecom -1;
	}
	else
		messageBox("\003El modem no ha sido encontrado",mfOKButton | mfError);
}

//++++++++++++++
//  Shell al DOS
//++++++++++++++
void MyProg::Shell()
{
	suspend();
	system("cls");
	cout << "Tipee EXIT para volver a Tu - Tu.";
	system( getenv( "COMSPEC"));
	resume();
	redraw();
}

//++++++++++++++++
//  Salir a Dos.
//++++++++++++++++

// DialogBox de salir
void MyProg::Salir()
{
	if(cmYes == messageBox("\003� Seguro que quiere salir ?",mfYesButton | mfNoButton | mfConfirmation ) )
	{
		endModal( cmQuit);
	}
}


// ++++++++++++++
// Funcion Principal  MAIN
// ++++++++++++++

int main()
{
	MyProg myprog;
	myprog.run();
	return 0;
}
