//
// este modulo pertenece a telefono.
//
// Ricardo Quesada 1994
//

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TDialog
#define Uses_TButton
#define Uses_TLabel
#define Uses_TInputLine
#define Uses_TCheckBoxes
#define Uses_TSItem
#define Uses_TView
#define Uses_TRadioButtons
#define Uses_TMenuBar
#define Uses_TStatusLine
#define Uses_TRect
#define Uses_TStatusDef
#define Uses_TSubMenu
#define Uses_TKeys
#define Uses_TStatusItem
#define Uses_TDeskTop
#define Uses_TMenuItem
#define Uses_MsgBox
#define Uses_TDialog


#include <conio.h>
#include <bios.h>
#include <string.h>
#include <dos.h>
#include "telefono.hpp"
#include <stdlib.h>
#include "serial.h"

extern "C" {
	int mandarstr_asm( int , char far * );
	int mandarchar_asm( int , char );
	unsigned recivirchar_asm( int );
	int init_asm( int , int , int , int , int );
	int dcd_asm( int );
	int dtr_asm( int, int);
}

//
// definicion de Classes
//
class Conexion : public TView
{
	char cadena[30];

public:

	Conexion( TRect& r );
	virtual void update( int );
	virtual void draw();
};
class Conexion *chart;

void Cucu(int,int,int,int,int,int);

//
// funcion Dial
//

void MyProg::Dial(int redial)
{

	char telefono[60];
	int a = 0;

	if(redial!=1)
	{
		TDialog *dptr = new TDialog ( TRect ( 0, 0, 35, 10),"Llamar a un telefono" );
		if ( dptr )
		{
			TInputLine *b = new TInputLine( TRect( 5, 4, 30, 5 ), 20 );
			dptr->insert( b );
			dptr->insert( new TLabel( TRect( 5, 3, 30, 4 ),"~N~umero a llamar", b ) );

			dptr->insert( new TButton( TRect( 4, 7, 16, 9 ) ,"~L~lamar",
						cmOK, bfDefault ) );

			dptr->insert( new TButton( TRect( 19, 7, 31, 9 ) ,"~C~ancelar",
						cmCancel, bfNormal ) );

			dptr->options |= ofCentered;
			dptr->setData( &Config->inputTelefono );

			ushort control = deskTop->execView( dptr );

			if ( control != cmCancel )
			{
				dptr->getData( &Config->inputTelefono );
				a = 1;
			}
		}
		destroy( dptr );

		if(a==0) return;
	}

	strcpy( telefono , Config->inputPrefijo );
	strcat( telefono , Config->inputTelefono );
	strcat( telefono , Config->inputSufijo );

	if ( mandarstr_asm( Config->radioCom , (char far *) telefono ) == 0)
	{

		TDialog *dptr = new TDialog ( TRect ( 0, 0, 35, 6),"Esperando" );
		dptr->options = ofCentered;
		deskTop->insert(dptr);

		chart = new Conexion( TRect(0,5,30,6) );
		chart->options = ofCentered;

		deskTop->insert(chart);

		a = Esperar();

		destroy( dptr  );
		destroy( chart );
		if(a==1) // conecto
			Connect();
	}

	else{  // perteneciente al     if(manda... )==0
		messageBox("\003Se ha producido un error al intentar el llamado.",mfOKButton | mfError );
	}
}

void MyProg::Connect()
{
	suspend();
	clrscr();
	::Cucu(Config->radioCom,Config->radioBaudio,Config->radioParidad,Config->radioDatabit,Config->radioStopbit,0);
	resume();
	redraw();
	mandarstr_asm( Config->radioCom, Config->inputHang );
	messageBox("\003Comunicación terminada.",mfOKButton | mfInformation );
}


// +++++++++++++++++++++
//
//
// funciones de Conexion
//
//
// +++++++++++++++++++++

Conexion::Conexion(TRect& r) : TView( r )
{
	strcpy(cadena,"Espere...");
}


void Conexion::draw()
{
	TDrawBuffer buf;

	char cad2[]="Apretar ESC para cancelar:";
	char c = getColor(2);
	int largo=strlen(cad2);

	buf.moveChar(0, ' ', c, size.x);
	buf.moveStr(0,cad2,c );
	buf.moveStr(largo + 1 , cadena, c);
	writeLine(0, 0, size.x, 1, buf);
}

void Conexion::update(int juju)
{
	itoa(juju, cadena, 10);
	drawView();
}

// devuelve 1 si conecta

int MyProg::Esperar()
{
	char mensaje[20];
	char       c;
	int i,a,m;
	int u=0;

	int port = Config->radioCom + 1 ;
	int stopbits = Config->radioStopbit + 1 ;
	int bits = Config->radioDatabit + 5 ;
	int speed = Config->radioBaudio ;
	int parity = Config->radioParidad ;

	switch(speed)
	{
	case 0:
		speed = 110;
		break;
	case 1:
		speed = 150;
		break;
	case 2:
		speed = 300;
		break;
	case 3:
		speed = 600;
		break;
	case 4:
		speed = 1200;
		break;
	case 5:
		speed = 2400;
		break;
	case 6:
		speed = 4800;
		break;
	case 7:
		speed = 9600;
		break;
	}

	switch(parity)
	{
	case 0:
		parity = 0;
		break;
	case 1:
		parity = 0x18;
		break;
	case 2:
		parity = 0x08;
		break;
	}

	serial comport(port, speed, parity, bits, stopbits);


	i=atoi(Config->inputTiempo);

	do {              		// vacia el buffer.
		comport >> c;
	} while (c!=-1);

	for(i;i>0;i--)
	{
		chart->update(i);

		a=dcd_asm( Config->radioCom );
		if(a==0)
		{
			return 1;
		}

		if( bioskey(1)!=0)
		{
			if (bioskey(0) == 0x011B ) // ESC
			{
				i=0;
				m=0;
			}
		}

		do{
			comport >> c;
			if(c!=-1)
			{
				if( (c==10) || (c==13))
				{
					mensaje[u]=0;
					if( strncmp(mensaje,"BU",2) == 0)
					{
						i=0;
						m=1;    // BUSY
					}
					if( strncmp(mensaje,"NO C",4) == 0)
					{
						i=0;
						m=2; // NO CARRIER
					}
					if( strncmp(mensaje,"NO D",4) == 0)
					{
						i=0;
						m=3;  // NO DIALTONE
					}
				}
				else {
					mensaje[u]=c;
					u++;
				}
			}

		} while(c!=-1);

		sleep(1);
	}  // este pertenece al for

	dtr_asm( Config->radioCom, 0);		//DTR OFF
	mandarstr_asm( Config->radioCom , Config->inputHang );  // cuelga por las dudas, vio ?

	switch(m){
	case 0:
		messageBox("\003Cancelado por usuario.",mfOKButton | mfInformation );
		break;
	case 1:
		messageBox("\003Ocupado.",mfOKButton | mfInformation );
		break;
	case 2:
		messageBox("\003No DialTone.",mfOKButton | mfInformation );
		break;
	case 3:
		messageBox("\003No Carrier.",mfOKButton | mfInformation );
		break;
	default:
		messageBox("\003Tiempo terminado.",mfOKButton | mfInformation );
		break;
	}

	return 0;
}
