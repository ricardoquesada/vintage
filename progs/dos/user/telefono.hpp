#define Uses_TApplication
#include <tv.h>

class MyProg : public TApplication
{

	struct CONFIG
	{
		char   Cabecera[8];
		char   Usuario[30];
		ushort radioBaudio;
		ushort radioCom;
		ushort radioParidad;
		ushort radioStopbit;
		ushort radioDatabit;
		ushort boxXonXoff;
		ushort boxRtsCts;
		char   inputPrefijo[20];
		char   inputSufijo[20];
		char   inputInit[20];
		char   inputHang[20];
		char   inputTelefono[20];
	};
	CONFIG *Config;

	void About();
	void Modem();
	void Linea();
	void Fossil();
	void Salir();
	void Abrir();
	void Salvar();
	void Default();
	void Inic();
	void Hang();
	void Dial();
	void Conecto();
	void Recivir();
	void Mandar();

public:

	MyProg();
	static TStatusLine *initStatusLine( TRect r );
	static TMenuBar *initMenuBar( TRect r );
	virtual void handleEvent( TEvent& event );
};
