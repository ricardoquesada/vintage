//
// Telefono
// Hecho en C , C++ , Librerias TVision
// Borland C++ 3.0 & Application Frameworks
//
// Ricardo Quesada 1994

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TDialog
#define Uses_TButton
#define Uses_TLabel
#define Uses_TInputLine
#define Uses_TCheckBoxes
#define Uses_TSItem
#define Uses_TView
#define Uses_TRadioButtons
#define Uses_TMenuBar
#define Uses_TStatusLine
#define Uses_TRect
#define Uses_TStatusDef
#define Uses_TSubMenu
#define Uses_TKeys
#define Uses_TStatusItem
#define Uses_TDeskTop
#define Uses_TMenuItem
#define Uses_MsgBox

#define HEADER "Tel2.00"

#include <stdlib.h>
#include <string.h>
#include <io.h>
#include <sys\stat.h>
#include <fcntl.h>
#include <dos.h>
#include <conio.h>
#include "telefono.hpp"

const int cmAbout   = 100;
const int cmLinea   = 101;
const int cmModem   = 102;
const int cmSalir   = 103;
const int cmSalvar  = 104;
const int cmAbrir	= 105;
const int cmInit    = 106;
const int cmHang	= 107;
const int cmDial    = 108;

extern "C" {
	int fossil_asm( void );
	int mandar_asm( int , char far * );
	char charwait_asm( int );
	unsigned charnowait_asm( int );
	unsigned keycharwait_asm( int );
	unsigned keycharnowait_asm( int );
	unsigned hay_char_asm( int );
	int init_asm( int , int , int , int , int );
	int init_fossil_asm( int );
	int dcd_asm( int );
}

MyProg::MyProg() :
	TProgInit( &MyProg::initStatusLine,
			   &MyProg::initMenuBar,
			   &MyProg::initDeskTop
			 )
{
	About();
	Fossil();
	Abrir();
	Inic();
}

TStatusLine *MyProg::initStatusLine( TRect r)
{
	r.a.y = r.b.y - 1;
	return new TStatusLine( r,
		*new TStatusDef( 0, 0xFFFF ) +
			*new TStatusItem( "~Alt-X~ Salir",kbAltX, cmSalir ) +
			*new TStatusItem( "~F-10~ Menu",kbF10, cmMenu)
	);
}

TMenuBar *MyProg::initMenuBar( TRect r )
{
	r.b.y = r.a.y + 1;
	return new TMenuBar( r,
		*new TSubMenu( "~�~",kbAltSpace ) +
			*new TMenuItem("~A~cerca",cmAbout, kbNoKey, hcNoContext ) +
		*new TSubMenu("~A~rchivos",kbAltA ) +
			*new TMenuItem("~S~alir",cmSalir,kbAltX, hcNoContext,"Alt-X") +
		*new TSubMenu("~S~eteos",kbAltS ) +
			*new TMenuItem("~L~inea...",cmLinea, kbAltL, hcNoContext,"Alt-L" ) +
			*new TMenuItem("~M~odem...",cmModem, kbNoKey, hcNoContext ) +
			newLine() +
			*new TMenuItem("~S~alvar",cmSalvar, kbNoKey, hcNoContext ) +
		*new TSubMenu("~C~omandos",kbAltC ) +
			*new TMenuItem("~I~nicializar",cmInit, kbNoKey, hcNoContext ) +
			*new TMenuItem("~H~angup~",cmHang, kbAltH, hcNoContext,"Alt-H" ) +
			*new TMenuItem("~D~iscar",cmDial, kbAltD, hcNoContext,"Alt-D" )
	);
}

void MyProg::handleEvent ( TEvent& event )
{
	TApplication::handleEvent( event );
	if ( event.what == evCommand )
		{
		switch( event.message.command)
			{
			case cmAbout:
				About();
				break;
			case cmModem:
				Modem();
				break;
			case cmLinea:
				Linea();
				break;
			case cmSalvar:
				Salvar();
				break;
			case cmSalir:
				Salir();
				break;
			case cmInit:
				Inic();
				break;
			case cmHang:
				Hang();
				break;
			case cmDial:
				Dial();
				break;
			default:
				return;
			}
		clearEvent( event );
		}
}

// carga los valores por defecto
void MyProg::Default()
{
	strcpy( Config->Cabecera , HEADER );
	strcpy( Config->Usuario ,"*** NO REGISTRADO ***" );
	Config->radioBaudio  = 3;    			//2400
	Config->radioCom 	 = 0;               //COM1
	Config->radioParidad = 0;               //NONE
	Config->radioStopbit = 0;               //1
	Config->radioDatabit = 3;               //8
	Config->boxXonXoff   = 0;               //NO
	Config->boxRtsCts    = 0;               //NO
	strcpy( Config->inputPrefijo ,"ATDP");
	strcpy( Config->inputSufijo ,"^M");
	strcpy( Config->inputInit ,"ATZ^M");
	strcpy( Config->inputHang ,"~~~+++~~~ATH^M");
	strcpy( Config->inputTelefono ,"Telefono Corte" );
}


// Detecta si hay algun FOSSIL instalado
void MyProg::Fossil()
{
	if( fossil_asm() == 1 )
	{
		messageBox("\003Fossil no instalado\n\003Instalelo y cargue nuevamente el programa",mfError | mfCancelButton );
		exit(1);
	}
	else return;
}

// Manda cadena de inicializacion al modem.
void MyProg::Inic()
{
	gotoxy(1,3);
	if ( init_fossil_asm( Config->radioCom ) ==  0)
	{
		init_asm( Config->radioBaudio , Config->radioParidad , Config->radioStopbit , Config->radioDatabit , Config->radioCom );

		if ( mandar_asm( Config->radioCom , Config->inputInit ) == 0)
		{
			messageBox("\003Modem inicializado",mfOKButton | mfInformation );
			return;
		}
	}
	messageBox("\003Error al inicializar el modem.",mfOKButton | mfError );
}

// Cuelga la comunicacion
void MyProg::Hang()
{
	mandar_asm( Config->radioCom , Config->inputHang );
}


// Lee archivo de configuracion
void MyProg::Abrir()
{
   const unsigned int sizeofbuffer = sizeof( *Config );
   int handle;

   if ( (handle = _open("telefono.cfg",O_RDWR ) ) == -1)
   {
		messageBox("\003El archivo TELEFONO.CFG no se encontr�. Se creara uno.",mfInformation | mfOKButton);
		if ( (handle = _creat("telefono.cfg", 0 )) == -1 )
		{
			messageBox("\003No se pudo crear el archivo",mfError | mfOKButton );
			return;
		}
		else {
			Default();
			Salvar();
			close(handle);
			return;
		}
   }

   if (_read(handle, Config , sizeofbuffer ) != sizeofbuffer )
   {
			messageBox("\003Se actualizara el archivo de configuracion.",mfInformation | mfOKButton);
			Default();
			Salvar();
			close(handle);
			return;
   }

   if( 0 != strcmp( Config->Cabecera , HEADER ) )
   {
		messageBox("\003El archivo pertenece a otra version\n\003Se actualizara",mfError | mfOKButton );
		Default();
		Salvar();
		close(handle);
		return;
   }
}

// Graba valores en archivo de configuracion
void MyProg::Salvar()
{
   const unsigned int sizeofbuffer = sizeof( *Config );
   int handle;

   if ( (handle = _open("telefono.cfg",O_RDWR ) ) == -1)
   {
		messageBox("\003No se pudo abrir el archivo\n",mfError | mfOKButton );
		return;
   }

   if (_write(handle, Config , sizeofbuffer ) != sizeofbuffer )
   {
		close(handle);
		messageBox("\003No se pudo salvar satisfactoriamente la informacion.",mfError | mfOKButton);
		return;
   }

   close(handle);
   messageBox("\003Configuraci�n salvada.",mfInformation | mfOKButton);
   return ;
}

// acerca del programa
void MyProg::About()
{
	TDialog *aboutBox = new TDialog(TRect(0, 0, 39, 13), "Acerca del Programa");

	aboutBox->insert(
	  new TStaticText(TRect(2, 2, 37, 9),
		"\003Telefono\n\003\n"
		"\003Version 2.00\n\003\n"
		"\003por Ricardo Quesada\n\003\n"
		"\003(c) 1994"
		)

	  );

	aboutBox->insert(
	  new TButton(TRect(14, 10, 25, 12), " OK", cmOK, bfDefault)
	  );

	aboutBox->options |= ofCentered;

	deskTop->execView(aboutBox);

	destroy( aboutBox );
}


void MyProg::Linea()
{
	TDialog *dptr = new TDialog ( TRect ( 0, 0, 45, 22),"Seteos de la linea" );
	if ( dptr )
	{
		TView *b = new TRadioButtons( TRect (5 ,3 ,26, 7 ),
			new TSItem( "300",
			new TSItem( "600",
			new TSItem( "1200",
			new TSItem( "2400",
			new TSItem( "4800",
			new TSItem( "9600",
			new TSItem( "19200",
			new TSItem( "38400", 0 ) ) ) ) ) ) ) )

		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 2, 26, 3 ),"~B~audios", b) );


	   b = new TRadioButtons( TRect ( 5, 9, 26, 13 ),
			new TSItem( "COM1",
			new TSItem( "COM2",
			new TSItem( "COM3",
			new TSItem( "COM4",0 ) ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 8, 26, 9),"Co~m~", b) ) ;


	   b = new TRadioButtons( TRect ( 30, 3, 40, 6),
			new TSItem( "None",
			new TSItem( "Even",
			new TSItem( "Odd", 0 ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 30, 2, 40, 3),"~P~aridad", b) );

		b = new TRadioButtons( TRect ( 30, 8, 40, 10 ),
			new TSItem( "1",
			new TSItem( "2",0 ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 30, 7, 40, 8),"~S~topBit", b) );

		b = new TRadioButtons( TRect ( 30, 12, 40, 16 ),
			new TSItem( "5",
			new TSItem( "6",
			new TSItem( "7",
			new TSItem( "8",0 ) ) ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 30, 11, 40, 12),"~D~atabit", b) );

		b = new TCheckBoxes( TRect( 5, 15 ,26 , 17),
			new TSItem( "Xon/Xoff",
			new TSItem( "RTS/CTS", 0 ) )
		);
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 14, 26, 15),"~F~lowcontrol",b ));

		dptr->insert( new TButton( TRect( 4, 19, 20, 21) ,"~O~K",
						cmOK, bfDefault ) );


		dptr->insert( new TButton( TRect( 25, 19, 41, 21) ,"~C~ancelar",
						cmCancel, bfNormal ) );

		dptr->options |= ofCentered;
		dptr->setData( &Config->radioBaudio );

		ushort control = deskTop->execView( dptr );

		if ( control != cmCancel )
			dptr->getData( &Config->radioBaudio );
	}
	destroy( dptr );

}

// Dialog Box del seteo del Modem
void MyProg::Modem()
{

	TDialog *dptr = new TDialog ( TRect ( 0, 0, 67, 15),"Seteos del modem" );
	if ( dptr )
	{
		TInputLine *b = new TInputLine( TRect( 35, 4, 57, 5 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 4, 35, 5 ),"~P~refijo de llamada", b ) );

		b = new TInputLine( TRect( 35, 5, 57, 6 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 5, 35, 6 ),"~S~ufijo de llamada", b ) );

		b = new TInputLine( TRect( 35, 6, 57, 7 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 6, 35, 7 ),"~I~nicializar", b ) );

		b = new TInputLine( TRect( 35, 7, 57, 8 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 10, 7, 35, 8 ),"~H~angup", b ) );


		dptr->insert( new TButton( TRect( 10, 12, 25, 14) ,"~O~K",
						cmOK, bfDefault ) );

		dptr->insert( new TButton( TRect( 30, 12, 45, 14) ,"~C~ancelar",
						cmCancel, bfNormal ) );

		dptr->options |= ofCentered;
		dptr->setData( &Config->inputPrefijo );

		ushort control = deskTop->execView( dptr );

		if ( control != cmCancel )
			dptr->getData( &Config->inputPrefijo );
	}
	destroy( dptr );

}


// DialogBox de salir
void MyProg::Salir()
{
	if(cmYes == messageBox("\003Seguro que quiere salir ?",mfYesButton | mfNoButton | mfConfirmation ) )
		endModal( cmQuit);
}

int main()
{
	MyProg myprog;
	myprog.run();
	return 0;
}
