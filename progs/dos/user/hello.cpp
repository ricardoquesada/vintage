/*---------------------------------------------------------*/
/*                                                         */
/*   Turbo Vision 1.0                                      */
/*   Copyright (c) 1991 by Borland International           */
/*                                                         */
/*   Turbo Vision Hello World Demo Source File             */
/*---------------------------------------------------------*/

#define Uses_TKeys
#define Uses_TApplication
#define Uses_TEvent
#define Uses_TRect
#define Uses_TDialog
#define Uses_TStaticText
#define Uses_TButton
#define Uses_TMenuBar
#define Uses_TSubMenu
#define Uses_TMenuItem
#define Uses_TStatusLine
#define Uses_TStatusItem
#define Uses_TStatusDef
#define Uses_TDeskTop
#define Uses_MsgBox
#define Uses_TDrawBuffer

#include <stdlib.h>
#include <tv.h>
#include <string.h>
#include <dos.h>

const GreetThemCmd = 100;
const PruebaCmd    = 101;
const cmSi		   = 102;

class TiempoDisplay : public TView
{

public:

	~TiempoDisplay();
	TiempoDisplay( TRect& r );
	virtual void draw();
	virtual void handleEvent(TEvent& event);

	void updat();

private:
	char tiemp[17];
	int tio;
};

class TiempoCrear : public TDialog
{

public:

	TiempoDisplay *pipi;
	TiempoCrear();
	TiempoCrear( StreamableInit ) :
		TDialog(streamableInit), TWindowInit(&TiempoCrear::initFrame) { };

	void perro();

};

class THelloApp : public TApplication
{

public:

	THelloApp();

	virtual void handleEvent( TEvent& event );
	static TMenuBar *initMenuBar( TRect );
	static TStatusLine *initStatusLine( TRect );

private:
	TiempoCrear *cucu;
	void greetingBox();
	void Prueba();
};

THelloApp::THelloApp() :
	TProgInit( &THelloApp::initStatusLine,
			   &THelloApp::initMenuBar,
			   &THelloApp::initDeskTop
			 )
{
}

void THelloApp::greetingBox()
{
	TDialog *d = new TDialog(TRect( 25, 5, 55, 16 ), "Hello, World!" );

	d->insert( new TStaticText( TRect( 3, 5, 15, 6 ), "How are you?" ) );
	d->insert( new TButton( TRect( 16, 2, 28, 4 ), "Terrific", cmCancel, bfNormal ) );
	d->insert( new TButton( TRect( 16, 4, 28, 6 ), "Ok", cmCancel, bfNormal ) );
	d->insert( new TButton( TRect( 16, 6, 28, 8 ), "Lousy", cmCancel, bfNormal ) );
	d->insert( new TButton( TRect( 16, 8, 28, 10 ), "Cancel", cmCancel, bfNormal ) );

	deskTop->execView( d );
}
void THelloApp::Prueba()
{
	TiempoCrear *cucu = (TiempoCrear *) validView(new TiempoCrear );

	if(cucu != 0)
		deskTop->insert(cucu);
	cucu->perro();

}

void THelloApp::handleEvent( TEvent& event )
{
	TApplication::handleEvent( event );
	if( event.what == evCommand )
		{
		switch( event.message.command )
			{
			case GreetThemCmd:
				greetingBox();
				break;
			case PruebaCmd:
				Prueba();
				break;
			default:
				return;
			}
		clearEvent( event );
		}
}

TMenuBar *THelloApp::initMenuBar( TRect r )
{

	r.b.y = r.a.y+1;

	return new TMenuBar( r,
	  *new TSubMenu( "~H~ello", kbAltH ) +
		*new TMenuItem( "~G~reeting...", GreetThemCmd, kbAltG ) +
		*new TMenuItem( "~P~rueba", PruebaCmd,kbAltP ) +
		 newLine() +
		*new TMenuItem( "E~x~it", cmQuit, cmQuit, hcNoContext, "Alt-X" )
		);

}

TStatusLine *THelloApp::initStatusLine( TRect r )
{
	r.a.y = r.b.y-1;
	return new TStatusLine( r,
		*new TStatusDef( 0, 0xFFFF ) +
			*new TStatusItem( "~Alt-X~ Exit", kbAltX, cmQuit ) +
			*new TStatusItem( 0, kbF10, cmMenu )
			);
}

int main()
{
	THelloApp helloWorld;
	helloWorld.run();
	return 0;
}


//
// -------------- Tiempo Viewer functions
//

TiempoDisplay::TiempoDisplay( TRect& r ) : TView( r )
{
	tio = 10;
}
TiempoDisplay::~TiempoDisplay()
{
}

void TiempoDisplay::draw()
{
	TDrawBuffer buf;
	char c = getColor(2);

	itoa(tio,tiemp,10);
	char tipi[]="Tiempo queda:";
	int a =strlen(tipi);

	buf.moveChar(0,' ',c,size.x);
	buf.moveStr(0,tipi,c);
	buf.moveStr(a+1, tiemp, c);
	writeLine(0, 0, size.x, 1, buf);
}


void TiempoDisplay::updat()
{
		tio--;
		drawView();
}

TiempoCrear::TiempoCrear() :
	TDialog( TRect(5, 3, 35, 10), "Tiempo" ),
	TWindowInit( &TiempoCrear::initFrame )
{
	TView *tv;
	TRect r;

	options |= ofFirstClick || ofCentered;

	tv = new TButton(TRect(3,4,10,6),"~S~i",cmSi,bfDefault);
	insert( tv );

	r = TRect( 3, 2, 21, 3 );
	insert( new TiempoDisplay(r) );
}

void TiempoDisplay::handleEvent(TEvent& event)
{
	TView::handleEvent(event);
	if( event.what == evCommand )
		{
		switch( event.message.command )
			{
			case cmSi:
				updat();
				break;
			default:
				return;
			}
		clearEvent( event );
		}
}

void TiempoCrear::perro()
{
	pipi->updat();
}