//
// este modulo pertenece a telefono.
//
// Ricardo Quesada 1994

#define Uses_TApplication
#define Uses_TEventQueue
#define Uses_TEvent
#define Uses_TDialog
#define Uses_TButton
#define Uses_TLabel
#define Uses_TInputLine
#define Uses_TCheckBoxes
#define Uses_TSItem
#define Uses_TView
#define Uses_TRadioButtons
#define Uses_TMenuBar
#define Uses_TStatusLine
#define Uses_TRect
#define Uses_TStatusDef
#define Uses_TSubMenu
#define Uses_TKeys
#define Uses_TStatusItem
#define Uses_TDeskTop
#define Uses_TMenuItem
#define Uses_MsgBox
#define Uses_TDialog

#include <conio.h>
#include <bios.h>
#include <string.h>
#include <dos.h>
#include "telefono.hpp"

extern "C" {
	int fossil_asm( void );
	int mandar_asm( int , char far * );
	int mandarchar_asm( int , char );
	char charwait_asm( int );
	unsigned charnowait_asm( int );
	int read_asm( int );
	unsigned keycharwait_asm( int );
	unsigned keycharnowait_asm( int );
	unsigned hay_char_asm( int );
	int init_asm( int , int , int , int , int );
	int init_fossil_asm( int );
	int dcd_asm( int );
	void ansi_asm( int , char );
	int sendchar_asm( int , char );
}

void MyProg::Mandar()
{
	char ch;
	if( bioskey(1) != 0)
	{
		(ch) = bioskey(0);
		sendchar_asm( Config->radioCom , ch );
		// cprintf("%c",ch);
	}

}

void MyProg::Recivir()
{
	int ch;
	char cc;

	ch = read_asm ( Config->radioCom );
	if( ch != -1 )
	{
		(cc) = ch;
		ansi_asm( Config->radioCom , cc );
	}
}

void MyProg::Conecto()
{
	int a=0;
	do {
		a=dcd_asm( Config->radioCom );
		Mandar();
		Recivir();
	}while (a==0);

}

// Llama a un numero de telefono
void MyProg::Dial()
{
	char telefono[60];
	char mensaje[20];
	int i,a=0;
	int ch;
	char cc;
	int num=0;

	TDialog *dptr = new TDialog ( TRect ( 0, 0, 35, 10),"Llamar a un telefono" );
	if ( dptr )
	{
		TInputLine *b = new TInputLine( TRect( 5, 4, 30, 5 ), 20 );
		dptr->insert( b );
		dptr->insert( new TLabel( TRect( 5, 3, 30, 4 ),"~N~umero a llamar", b ) );

		dptr->insert( new TButton( TRect( 4, 7, 16, 9 ) ,"~L~lamar",
						cmOK, bfDefault ) );

		dptr->insert( new TButton( TRect( 19, 7, 31, 9 ) ,"~C~ancelar",
						cmCancel, bfNormal ) );

		dptr->options |= ofCentered;
		dptr->setData( &Config->inputTelefono );

		ushort control = deskTop->execView( dptr );

		if ( control != cmCancel )
		{
			dptr->getData( &Config->inputTelefono );
			a = 1;
		}
	}
	destroy( dptr );

	if(a==0) return;

	strcpy( telefono , Config->inputPrefijo );
	strcat( telefono , Config->inputTelefono );
	strcat( telefono , Config->inputSufijo );

	do{
		ch = read_asm( Config->radioCom );
	} while(ch != -1);			// vacia el buffer de entrada. Metodo algo extra�o, Verdad ?


	if ( mandar_asm( Config->radioCom , (char far *) telefono ) == 0)
	{
		clrscr();
		for(i=30;i>0;i--)
		{
			a=dcd_asm( Config->radioCom );
			if(a==0)
				{
					Conecto();
					return;
				}
		gotoxy(1,1);
		cprintf("Esperando conexi�n: %i \r\n",i);
		cprintf("Apretar SPACE para cancelar\r\n");

		if( bioskey(1) != 0)
		{
			(cc) = bioskey(0);
			if(cc==32)
			{
				i=1;
				mandar_asm( Config->radioCom , Config->inputHang );
			}
		}
		do{
			ch = read_asm ( Config->radioCom );
			if( ch != -1 )
			{
				(cc) = ch;
				if( (cc == 13) || (cc ==10) )
				{
					mensaje[num]=0;
					num=0;
					if( (strcmp(mensaje,"BUSY") == 0) || (strcmp(mensaje,"NO DIALTONE") == 0) || (strcmp(mensaje,"NO CARRIER") == 0) )
						i=1;
				}
				else
				{
					ansi_asm( Config->radioCom , cc );
					mensaje[num]=cc;
					num++;
				}
			}   // este al if(ch != -1)
		} while ( ch != -1 ); // este pertenece al do
		sleep(1);
		}  // este pertenece al for
		messageBox("\003No conecto",mfOKButton | mfInformation );
	}
}
