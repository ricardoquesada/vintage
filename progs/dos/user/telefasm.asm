;rutinas de assembler para telefono.exe
;Hechas por Ricardo Quesada

	 DOSSEG
	 .MODEL large   ; Select small model (near code and data)
	 .CODE


PUBLIC _fossil_asm

;**********************************************;
;                                              ;
;detecta si el fossil esta cargado.            ;
;C prototype:                                  ;
;int fossil_asm( void )                        ;
;                                              ;
;devuelve en ax=0 si fossil instalado          ;
;            ax=1 si fossil no instalado       ;
;**********************************************;
_fossil_asm  PROC

		push es
		push bx

		mov ax,3514h
		int 21h

		xor ax,ax				;devuelve 0 por default

		cmp es:[bx+6],1954h

		pop bx
		pop es

		jne nofossil
		ret

nofossil:
		mov byte ptr ax,1
		ret

_fossil_asm  ENDP

PUBLIC _mandar_asm

;***********************************;
;                                   ;
;_mandar_asm                        ;
;Manda un cadena al com             ;
;C prototype:                       ;
;int mandar_asm( int , char far * ) ;
;                                   ;
;***********************************;

_mandar_asm PROC

		push bp
		mov bp,sp
		push ds
		push si
		push di

		mov dx,[bp+6]           ;numero de com
		mov si,[bp+8]           ;offset of string
		mov di,si
		mov ax,[bp+10]          ;segment of string
		mov ds,ax


loop1:
		cmp byte ptr [si],0     ;lenguaje C termina los strings
		je bien
		cmp [si],4D5Eh          ;control + M
		je linea

		mov ah,1
		mov al,[si]             ;send one char to com
		int 14h

		and ah,10000000b
		cmp ah,10000000b
		je error

		inc si
		jmp short loop1

linea:
		mov al,13
		mov ah,1
		int 14h

		inc si
		inc si

		jmp loop1

error:
		mov byte ptr ax,1
		jmp final

bien:
		xor ax,ax

final:
		pop di
		pop si
		pop ds
		pop bp
		ret

_mandar_asm ENDP


PUBLIC _mandarchar_asm
;***********************************;
;                                   ;
;_mandarchar_asm                    ;
;Manda un char   al com             ;
;C prototype:                       ;
;int mandarchar_asm( int , char )   ;
;                                   ;
;***********************************;

_mandarchar_asm PROC

		push bp
		mov bp,sp
		push ds
		push si
		push di

		mov dx,[bp+6]           ;numero de com
                mov al,[bp+8]           ;char


		mov ah,1
                int 14h                 ;send one char to com X

		and ah,10000000b
		cmp ah,10000000b
                jne bien2


                mov byte ptr ax,1
                jmp short final2

bien2:
		xor ax,ax

final2:
		pop di
		pop si
		pop ds
		pop bp
		ret

_mandarchar_asm ENDP


PUBLIC _sendchar_asm
;***********************************;
;                                   ;
;_sendchar_asm                    ;
;Manda un char   al com             ;
;C prototype:                       ;
;int sendchar_asm( int , char )   ;
;                                   ;
;***********************************;

_sendchar_asm PROC

		push bp
		mov bp,sp
		push ds
		push si
		push di

		mov dx,[bp+6]           ;numero de com
                mov al,[bp+8]           ;char

                mov ah,0bh
                int 14h                 ;send one char to com X

                or ax,ax
                jne bien3

                mov byte ptr ax,1
                jmp short final3

bien3:
		xor ax,ax

final3:
		pop di
		pop si
		pop ds
		pop bp
		ret

_sendchar_asm ENDP


PUBLIC _charwait_asm
;*****************************************
;
;C prototye
; char charwait_asm( int com )
;
;******************************************
_charwait_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
		mov ah,2
		int 14h                         ;devuelve en ax el char available

		pop bp
		ret                             ;devuelve el char

_charwait_asm ENDP


PUBLIC _charnowait_asm
;*****************************************
;
;C prototye
; unsigned charnowait_asm( int com )
;
;******************************************
_charnowait_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
		mov ah,0ch
		int 14h                         ;devuelve en ax el char available

		pop bp
		ret                             ;devuelve el char

_charnowait_asm ENDP

PUBLIC _keycharwait_asm
;*****************************************
;
;C prototye
; char keycharwait_asm( int com )
;
;******************************************
_keycharwait_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
		mov ah,0eh
		int 14h                         ;devuelve en ax el char available

		pop bp
		ret                             ;devuelve el char


_keycharwait_asm ENDP

PUBLIC _keycharnowait_asm
;*****************************************
;
;C prototye
; char keycharnowait_asm( int com )
;
;******************************************
_keycharnowait_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
		mov ah,0dh
		int 14h                         ;devuelve en ax el char available

		pop bp
		ret						;devuelve el char

_keycharnowait_asm ENDP

PUBLIC _init_asm
;***********************************;
;                                   ;
;_init_asm                          ;
;Inicializa usando fossil           ;
;C prototype:                       ;
;int mandar_asm( int , int , int , int , int )
;                                   ;
;***********************************;

_init_asm PROC

				push bp
				mov bp,sp

				xor ax,ax

				mov dx,[bp+6]                           ;baudios
				add dl,2
				shl dl,5                                ;
				or al,dl

				mov dx,[bp+8]                           ;paridad
				shl dl,3                                ;
				or al,dl

				mov dx,[bp+10]                          ;stopbit
				shl dl,2
				or al,dl

				mov dx,[bp+12]                          ;databit
				or al,dl

				mov dx,[bp+14]                          ;com
				mov ah,0

				int 14h

				pop bp
				ret

_init_asm ENDP

PUBLIC _dcd_asm
;***********************************
;
;detecta si se conecto
;C prototype
;int dcd_asm( int );
;ax=0 si conecto
;
;**************************************
_dcd_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]
		mov ah,3
		int 14h                 ;detect carrier

		and al,10000000b
		cmp al,10000000b
		je carrier

		mov byte ptr ax,1
		jmp fin_dcd

carrier:
		xor ax,ax
fin_dcd:
		pop bp
		ret

_dcd_asm ENDP


PUBLIC _init_fossil_asm
;********************
;
;inicializa el driver
;C prototype
; int init_fossil_asm( int com )
;
;*********************
_init_fossil_asm PROC

				push bp
				mov bp,sp

				mov dx,[bp+6]
				mov bx,4f50h
				mov ah,4
				int 14h

				cmp ax,1954h
				je initfossil

				mov byte ptr ax,1
				jmp fin_initfossil
initfossil:
				xor ax,ax

fin_initfossil:
				pop bp
		ret

_init_fossil_asm ENDP


PUBLIC _hay_char_asm
;*************************
;
;detecta si hay char el el input buffer
;c prototype
;unsigned hay_char_asm( int com );
;
;**********************************
_hay_char_asm PROC

				push bp
				mov bp,sp

				push ds
				push di
				push cx

				mov dx,[bp+6]
				mov ah,1bh
				mov cx,200
				mov di,offset buffe
				push cs
				pop ds

				int 14h

				mov di,offset buffe
				mov ax,[di+10]

				pop cx
				pop di
				pop ds
				pop bp

				ret

buffe           db 200 dup (?)

_hay_char_asm   ENDP


PUBLIC _hangup_asm
;*****************************************
;
;C prototye
; void hangup_asm( int com )
;
;******************************************
_hangup_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
                mov ah,6
                mov al,0                        ;lower DTR
                int 14h                         ;devuelve en ax el char available

		pop bp
		ret                             ;devuelve el char

_hangup_asm ENDP

PUBLIC _ansi_asm
;*****************************************
;
;C prototye
; void ansi_asm( int com , char ch )
;
;******************************************
_ansi_asm PROC

		push bp
		mov bp,sp

		mov dx,[bp+6]			;numero de com
                mov al,[bp+8]
                mov ah,13h
                int 14h                         ;write ansi to screen

		pop bp
		ret                             ;devuelve el char

_ansi_asm ENDP

PUBLIC _read_asm
;*****************************************
;
;C prototye
; int read_asm( int com )
;
;******************************************
_read_asm PROC

		push bp
		mov bp,sp
                push es
                push di

		mov dx,[bp+6]			;numero de com
                mov cx,1                        ;one char
                push cs
                pop es
                mov di,offset mozart
                mov ah,18h
                int 14h                         ;write ansi to screen

                cmp ax,0001h
                je beethoven

                mov ax,0ffffh
                jmp short chopin

beethoven:
                xor ax,ax
                mov al,cs:[di]
chopin:
                pop di
                pop es
                pop bp
		ret                             ;devuelve el char

mozart          db 20 dup(?)

_read_asm ENDP



                                end
