	ifndef	??version
?debug	macro
	endm
publicdll macro	name
	public	name
	endm
	endif
	?debug	V 300h
	?debug	S "COS.C"
	?debug	C E9BAAE891E05434F532E43
	?debug	C E900186B1714433A5C42435C494E434C5544455C4D4154482E48
	?debug	C E900186B1715433A5C42435C494E434C5544455C5F444546532E48
_TEXT	segment byte public 'CODE'
_TEXT	ends
DGROUP	group	_DATA,_BSS
	assume	cs:_TEXT,ds:DGROUP
_DATA	segment word public 'DATA'
d@	label	byte
d@w	label	word
_DATA	ends
_BSS	segment word public 'BSS'
b@	label	byte
b@w	label	word
_BSS	ends
_TEXT	segment byte public 'CODE'
	?debug	C E80105434F532E43BAAE891E
   ;	
   ;	main()
   ;	
	?debug	L 4
	assume	cs:_TEXT
_main	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	sub	sp,4
	?debug	B
   ;	
   ;	{
   ;		float a;
   ;		a = cos (1);
   ;	
	?debug	L 7
	fld1	
	sub	sp,8
	fstp	qword ptr [bp-12]
	fwait	
	call	near ptr _cos
	add	sp,8
	fstp	dword ptr [bp-4]
	fwait	
   ;	
   ;	}
   ;	
	?debug	L 8
	mov	sp,bp
	pop	bp
	ret	
	?debug	C E601610E02FCFF00
	?debug	E
	?debug	E
_main	endp
	?debug	C E9
	?debug	C FA00000000
_TEXT	ends
_DATA	segment word public 'DATA'
s@	label	byte
_DATA	ends
_TEXT	segment byte public 'CODE'
_TEXT	ends
	public	_main
	extrn	_cos:near
_s@	equ	s@
	?debug	C EA0109
	?debug	C E31800000023040000
	?debug	C EC055F6D61696E181800
	?debug	C E319000000230F0000
	?debug	C EB045F636F731900
	end
