	ifndef	??version
?debug	macro
	endm
publicdll macro	name
	public	name
	endm
	endif
	?debug	V 300h
	?debug	S "3D3.C"
	?debug	C E94391961E053344332E43
	?debug	C E900186B1718433A5C42435C494E434C5544455C47524150484943+
	?debug	C 532E48
	?debug	C E900186B1715433A5C42435C494E434C5544455C5F444546532E48
	?debug	C E900186B1714433A5C42435C494E434C5544455C4D4154482E48
_TEXT	segment byte public 'CODE'
_TEXT	ends
DGROUP	group	_DATA,_BSS
	assume	cs:_TEXT,ds:DGROUP
_DATA	segment word public 'DATA'
d@	label	byte
d@w	label	word
_DATA	ends
_BSS	segment word public 'BSS'
b@	label	byte
b@w	label	word
_BSS	ends
_TEXT	segment byte public 'CODE'
	?debug	C E801053344332E434391961E
   ;	
   ;	void Delay( int t)
   ;	
	?debug	L 62
	assume	cs:_TEXT
_Delay	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	sub	sp,4
	push	si
	?debug	C E60174040A040000
	mov	si,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		float x = 1;
   ;	
	?debug	L 64
	mov	word ptr [bp-2],16256
	mov	word ptr [bp-4],0
	jmp	short @1@86
@1@58:
   ;	
   ;		while( t-- >0)
   ;			x=cos(x);
   ;	
	?debug	L 66
	fld	dword ptr [bp-4]
	sub	sp,8
	fstp	qword ptr [bp-14]
	fwait	
	call	near ptr _cos
	add	sp,8
	fstp	dword ptr [bp-4]
	fwait	
@1@86:
	?debug	L 65
	mov	ax,si
	dec	si
	or	ax,ax
	jg	short @1@58
   ;	
   ;	} // end Delay
   ;	
	?debug	L 67
	pop	si
	mov	sp,bp
	pop	bp
	ret	
	?debug	C E601780E02FCFF000174040C0600
	?debug	E
	?debug	E
_Delay	endp
   ;	
   ;	void Make_Identity( matrix_ptr i)
   ;	
	?debug	L 70
	assume	cs:_TEXT
_Make_Identity	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	?debug	C E3190A6D61747269785F74797040001E01
	?debug	C E31B0010001A0E
	?debug	C E31A0040001A1B
	?debug	C E318000200151904
	?debug	C E60169180A040000
	mov	si,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		i->elem[0][0] = i->elem[1][1] = i->elem[2][2] = i->elem[3][3] =1;
   ;	
	?debug	L 72
	fld1	
	fst	dword ptr [si+60]
	fst	dword ptr [si+40]
	fst	dword ptr [si+20]
	fstp	dword ptr [si]
   ;	
   ;		i->elem[0][1] = i->elem[0][2] = i->elem[0][3] = i->elem[1][2] = i->elem[1][3] = i->elem[2][3] = 0;
   ;	
	?debug	L 73
	fldz	
	fst	dword ptr [si+44]
	fst	dword ptr [si+28]
	fst	dword ptr [si+24]
	fst	dword ptr [si+12]
	fst	dword ptr [si+8]
	fstp	dword ptr [si+4]
   ;	
   ;		i->elem[1][0] = i->elem[2][0] = i->elem[3][0] = i->elem[2][1] = i->elem[3][1] = i->elem[3][2] = 0;
   ;	
	?debug	L 74
	fldz	
	fst	dword ptr [si+56]
	fst	dword ptr [si+52]
	fst	dword ptr [si+36]
	fst	dword ptr [si+48]
	fst	dword ptr [si+32]
	fstp	dword ptr [si+16]
	fwait	
   ;	
   ;	}	// end Make_Identity
   ;	
	?debug	L 75
	pop	si
	pop	bp
	ret	
	?debug	C E60169180C0600
	?debug	E
	?debug	E
_Make_Identity	endp
   ;	
   ;	void Clear_Matrix( matrix_ptr i)
   ;	
	?debug	L 77
	assume	cs:_TEXT
_Clear_Matrix	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	?debug	C E60169180A040000
	mov	si,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		i->elem[0][0] = i->elem[1][1] = i->elem[2][2] = i->elem[3][3] =0;
   ;	
	?debug	L 79
	fldz	
	fst	dword ptr [si+60]
	fst	dword ptr [si+40]
	fst	dword ptr [si+20]
	fstp	dword ptr [si]
   ;	
   ;		i->elem[0][1] = i->elem[0][2] = i->elem[0][3] = i->elem[1][2] = i->elem[1][3] = i->elem[2][3] = 0;
   ;	
	?debug	L 80
	fldz	
	fst	dword ptr [si+44]
	fst	dword ptr [si+28]
	fst	dword ptr [si+24]
	fst	dword ptr [si+12]
	fst	dword ptr [si+8]
	fstp	dword ptr [si+4]
   ;	
   ;		i->elem[1][0] = i->elem[2][0] = i->elem[3][0] = i->elem[2][1] = i->elem[3][1] = i->elem[3][2] = 0;
   ;	
	?debug	L 81
	fldz	
	fst	dword ptr [si+56]
	fst	dword ptr [si+52]
	fst	dword ptr [si+36]
	fst	dword ptr [si+48]
	fst	dword ptr [si+32]
	fstp	dword ptr [si+16]
	fwait	
   ;	
   ;	}
   ;	
	?debug	L 82
	pop	si
	pop	bp
	ret	
	?debug	C E60169180C0600
	?debug	E
	?debug	E
_Clear_Matrix	endp
   ;	
   ;	void Mat_Mul( vertex_ptr v, matrix_ptr m)
   ;	
	?debug	L 84
	assume	cs:_TEXT
_Mat_Mul	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	sub	sp,12
	push	si
	push	di
	?debug	C E31D0A7665727465785F74797010001E03
	?debug	C E31E0010001A0E
	?debug	C E31C000200151D04
	?debug	C E6016D180A06000001761C0A040000
	mov	si,word ptr [bp+4]
	mov	di,word ptr [bp+6]
	?debug	B
   ;	
   ;	{
   ;		float x_new, y_new, z_new;
   ;		x_new = v->p[0] *m->elem[0][0] + v->p[1] *m->elem[1][0] + v->p[2] * m->elem[2][0] + v->p[3] * m->elem[3][0];
   ;	
	?debug	L 87
	fld	dword ptr [si]
	fmul	dword ptr [di]
	fld	dword ptr [si+4]
	fmul	dword ptr [di+16]
	fadd	
	fld	dword ptr [si+8]
	fmul	dword ptr [di+32]
	fadd	
	fld	dword ptr [si+12]
	fmul	dword ptr [di+48]
	fadd	
	fstp	dword ptr [bp-4]
   ;	
   ;		y_new = v->p[0] *m->elem[0][1] + v->p[1] *m->elem[1][1] + v->p[2] * m->elem[2][1] + v->p[3] * m->elem[3][1];
   ;	
	?debug	L 88
	fld	dword ptr [si]
	fmul	dword ptr [di+4]
	fld	dword ptr [si+4]
	fmul	dword ptr [di+20]
	fadd	
	fld	dword ptr [si+8]
	fmul	dword ptr [di+36]
	fadd	
	fld	dword ptr [si+12]
	fmul	dword ptr [di+52]
	fadd	
	fstp	dword ptr [bp-8]
   ;	
   ;		z_new = v->p[0] *m->elem[0][2] + v->p[1] *m->elem[1][2] + v->p[2] * m->elem[2][2] + v->p[3] * m->elem[3][2];
   ;	
	?debug	L 89
	fld	dword ptr [si]
	fmul	dword ptr [di+8]
	fld	dword ptr [si+4]
	fmul	dword ptr [di+24]
	fadd	
	fld	dword ptr [si+8]
	fmul	dword ptr [di+40]
	fadd	
	fld	dword ptr [si+12]
	fmul	dword ptr [di+56]
	fadd	
	fstp	dword ptr [bp-12]
   ;	
   ;	
   ;		v->p[X_COMP] = x_new;
   ;	
	?debug	L 91
	fwait	
	mov	ax,word ptr [bp-2]
	mov	dx,word ptr [bp-4]
	mov	word ptr [si+2],ax
	mov	word ptr [si],dx
   ;	
   ;		v->p[Y_COMP] = y_new;
   ;	
	?debug	L 92
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr [si+6],ax
	mov	word ptr [si+4],dx
   ;	
   ;		v->p[Z_COMP] = z_new;
   ;	
	?debug	L 93
	mov	ax,word ptr [bp-10]
	mov	dx,word ptr [bp-12]
	mov	word ptr [si+10],ax
	mov	word ptr [si+8],dx
   ;	
   ;	}
   ;	
	?debug	L 94
	pop	di
	pop	si
	mov	sp,bp
	pop	bp
	ret	
	?debug	C E6057A5F6E65770E02F4FF0005795F6E65770E02+
	?debug	C F8FF0005785F6E65770E02FCFF0001761C0C0600+
	?debug	C 016D180C0700
	?debug	E
	?debug	E
_Mat_Mul	endp
   ;	
   ;	void Scale_Object_Mat( object_ptr obj )
   ;	
	?debug	L 96
	assume	cs:_TEXT
_Scale_Object_Mat	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	push	di
	?debug	C E3200A6F626A6563745F74797018011E05
	?debug	C E3210000011A1D
	?debug	C E31F000200152004
	?debug	C E6036F626A1F0A040000
	mov	di,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;		for( index=0; index < obj->num_vertices; index++ )
   ;	
	?debug	L 99
	xor	si,si
	jmp	short @5@114
@5@58:
   ;	
   ;		{
   ;			Mat_Mul( &obj->vertices[index], &scale);
   ;	
	?debug	L 101
	mov	ax,offset DGROUP:_scale
	push	ax
	mov	ax,si
	mov	cl,4
	shl	ax,cl
	mov	dx,di
	add	dx,ax
	add	dx,24
	push	dx
	call	near ptr _Mat_Mul
	pop	cx
	pop	cx
	?debug	L 99
	inc	si
@5@114:
	cmp	word ptr [di],si
	jg	short @5@58
   ;	
   ;		}
   ;	}
   ;	
	?debug	L 103
	pop	di
	pop	si
	pop	bp
	ret	
	?debug	C E605696E64657804040600036F626A1F0C0700
	?debug	E
	?debug	E
_Scale_Object_Mat	endp
   ;	
   ;	void Rotate_X_Object_Mat( object_ptr obj)
   ;	
	?debug	L 105
	assume	cs:_TEXT
_Rotate_X_Object_Mat	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	push	di
	?debug	C E6036F626A1F0A040000
	mov	di,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;		for( index=0; index< obj->num_vertices; index++)
   ;	
	?debug	L 108
	xor	si,si
	jmp	short @6@114
@6@58:
   ;	
   ;		{
   ;			Mat_Mul( &obj->vertices[ index ], &rotation_x);
   ;	
	?debug	L 110
	mov	ax,offset DGROUP:_rotation_x
	push	ax
	mov	ax,si
	mov	cl,4
	shl	ax,cl
	mov	dx,di
	add	dx,ax
	add	dx,24
	push	dx
	call	near ptr _Mat_Mul
	pop	cx
	pop	cx
	?debug	L 108
	inc	si
@6@114:
	cmp	word ptr [di],si
	jg	short @6@58
   ;	
   ;		}
   ;	}
   ;	
	?debug	L 112
	pop	di
	pop	si
	pop	bp
	ret	
	?debug	C E605696E64657804040600036F626A1F0C0700
	?debug	E
	?debug	E
_Rotate_X_Object_Mat	endp
   ;	
   ;	void Rotate_Y_Object_Mat( object_ptr obj)
   ;	
	?debug	L 114
	assume	cs:_TEXT
_Rotate_Y_Object_Mat	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	push	di
	?debug	C E6036F626A1F0A040000
	mov	di,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;		for( index=0; index< obj->num_vertices; index++)
   ;	
	?debug	L 117
	xor	si,si
	jmp	short @7@114
@7@58:
   ;	
   ;		{
   ;			Mat_Mul( &obj->vertices[ index ], &rotation_y);
   ;	
	?debug	L 119
	mov	ax,offset DGROUP:_rotation_y
	push	ax
	mov	ax,si
	mov	cl,4
	shl	ax,cl
	mov	dx,di
	add	dx,ax
	add	dx,24
	push	dx
	call	near ptr _Mat_Mul
	pop	cx
	pop	cx
	?debug	L 117
	inc	si
@7@114:
	cmp	word ptr [di],si
	jg	short @7@58
   ;	
   ;		}
   ;	}
   ;	
	?debug	L 121
	pop	di
	pop	si
	pop	bp
	ret	
	?debug	C E605696E64657804040600036F626A1F0C0700
	?debug	E
	?debug	E
_Rotate_Y_Object_Mat	endp
   ;	
   ;	void Rotate_Z_Object_Mat( object_ptr obj)
   ;	
	?debug	L 122
	assume	cs:_TEXT
_Rotate_Z_Object_Mat	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	push	di
	?debug	C E6036F626A1F0A040000
	mov	di,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;		for( index=0; index< obj->num_vertices; index++)
   ;	
	?debug	L 125
	xor	si,si
	jmp	short @8@114
@8@58:
   ;	
   ;		{
   ;			Mat_Mul( &obj->vertices[ index ], &rotation_z);
   ;	
	?debug	L 127
	mov	ax,offset DGROUP:_rotation_z
	push	ax
	mov	ax,si
	mov	cl,4
	shl	ax,cl
	mov	dx,di
	add	dx,ax
	add	dx,24
	push	dx
	call	near ptr _Mat_Mul
	pop	cx
	pop	cx
	?debug	L 125
	inc	si
@8@114:
	cmp	word ptr [di],si
	jg	short @8@58
   ;	
   ;		}
   ;	}
   ;	
	?debug	L 129
	pop	di
	pop	si
	pop	bp
	ret	
	?debug	C E605696E64657804040600036F626A1F0C0700
	?debug	E
	?debug	E
_Rotate_Z_Object_Mat	endp
   ;	
   ;	void Create_Field( void )
   ;	
	?debug	L 131
	assume	cs:_TEXT
_Create_Field	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	sub	sp,14
	push	si
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;		float angle,c,s;
   ;	
   ;		angle = 0.1;
   ;	
	?debug	L 136
	mov	word ptr [bp-2],15820
	mov	word ptr [bp-4],52429
   ;	
   ;		c = cos(angle);
   ;	
	?debug	L 137
	fld	dword ptr [bp-4]
	sub	sp,8
	fstp	qword ptr [bp-24]
	fwait	
	call	near ptr _cos
	add	sp,8
	fstp	dword ptr [bp-8]
   ;	
   ;		s = sin(angle);
   ;	
	?debug	L 138
	fld	dword ptr [bp-4]
	sub	sp,8
	fstp	qword ptr [bp-24]
	fwait	
	call	near ptr _sin
	add	sp,8
	fstp	dword ptr [bp-12]
   ;	
   ;	
   ;		// Rotation_X
   ;		Make_Identity( &rotation_x );
   ;	
	?debug	L 141
	mov	ax,offset DGROUP:_rotation_x
	push	ax
	fwait	
	call	near ptr _Make_Identity
	pop	cx
   ;	
   ;		rotation_x.elem[1][1] = c;
   ;	
	?debug	L 142
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr DGROUP:_rotation_x+22,ax
	mov	word ptr DGROUP:_rotation_x+20,dx
   ;	
   ;		rotation_x.elem[1][2] = -s;
   ;	
	?debug	L 143
	fld	dword ptr [bp-12]
	fchs	
	fstp	dword ptr DGROUP:_rotation_x+24
   ;	
   ;		rotation_x.elem[2][1] = s;
   ;	
	?debug	L 144
	fwait	
	mov	ax,word ptr [bp-10]
	mov	dx,word ptr [bp-12]
	mov	word ptr DGROUP:_rotation_x+38,ax
	mov	word ptr DGROUP:_rotation_x+36,dx
   ;	
   ;		rotation_x.elem[2][2] = c;
   ;	
	?debug	L 145
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr DGROUP:_rotation_x+42,ax
	mov	word ptr DGROUP:_rotation_x+40,dx
   ;	
   ;	
   ;		// Rotation_Y
   ;		Make_Identity( &rotation_y);
   ;	
	?debug	L 148
	mov	ax,offset DGROUP:_rotation_y
	push	ax
	call	near ptr _Make_Identity
	pop	cx
   ;	
   ;		rotation_y.elem[0][0] = c;
   ;	
	?debug	L 149
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr DGROUP:_rotation_y+2,ax
	mov	word ptr DGROUP:_rotation_y,dx
   ;	
   ;		rotation_y.elem[0][2] = s;
   ;	
	?debug	L 150
	mov	ax,word ptr [bp-10]
	mov	dx,word ptr [bp-12]
	mov	word ptr DGROUP:_rotation_y+10,ax
	mov	word ptr DGROUP:_rotation_y+8,dx
   ;	
   ;		rotation_y.elem[2][0] = -s;
   ;	
	?debug	L 151
	fld	dword ptr [bp-12]
	fchs	
	fstp	dword ptr DGROUP:_rotation_y+32
   ;	
   ;		rotation_y.elem[2][2] = c;
   ;	
	?debug	L 152
	fwait	
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr DGROUP:_rotation_y+42,ax
	mov	word ptr DGROUP:_rotation_y+40,dx
   ;	
   ;	
   ;		// Rotation_Z
   ;		Make_Identity( &rotation_z);
   ;	
	?debug	L 155
	mov	ax,offset DGROUP:_rotation_z
	push	ax
	call	near ptr _Make_Identity
	pop	cx
   ;	
   ;		rotation_z.elem[0][0] = c;
   ;	
	?debug	L 156
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr DGROUP:_rotation_z+2,ax
	mov	word ptr DGROUP:_rotation_z,dx
   ;	
   ;		rotation_z.elem[0][1] = -s;
   ;	
	?debug	L 157
	fld	dword ptr [bp-12]
	fchs	
	fstp	dword ptr DGROUP:_rotation_z+4
   ;	
   ;		rotation_z.elem[1][0] = s;
   ;	
	?debug	L 158
	fwait	
	mov	ax,word ptr [bp-10]
	mov	dx,word ptr [bp-12]
	mov	word ptr DGROUP:_rotation_z+18,ax
	mov	word ptr DGROUP:_rotation_z+16,dx
   ;	
   ;		rotation_z.elem[1][1] = c;
   ;	
	?debug	L 159
	mov	ax,word ptr [bp-6]
	mov	dx,word ptr [bp-8]
	mov	word ptr DGROUP:_rotation_z+22,ax
	mov	word ptr DGROUP:_rotation_z+20,dx
   ;	
   ;	
   ;		// Scale
   ;		Make_Identity( &scale);
   ;	
	?debug	L 162
	mov	ax,offset DGROUP:_scale
	push	ax
	call	near ptr _Make_Identity
	pop	cx
   ;	
   ;		scale.elem[0][0] = 35;
   ;	
	?debug	L 163
	mov	word ptr DGROUP:_scale+2,16908
	mov	word ptr DGROUP:_scale,0
   ;	
   ;		scale.elem[1][1] = 35;
   ;	
	?debug	L 164
	mov	word ptr DGROUP:_scale+22,16908
	mov	word ptr DGROUP:_scale+20,0
   ;	
   ;		scale.elem[2][2] = 35;
   ;	
	?debug	L 165
	mov	word ptr DGROUP:_scale+42,16908
	mov	word ptr DGROUP:_scale+40,0
   ;	
   ;	
   ;	
   ;		for( index=0; index<NUM_OBJECTS; index++)
   ;	
	?debug	L 168
	xor	si,si
	jmp	@9@114
@9@58:
   ;	
   ;		{
   ;			asteroids[ index ].num_vertices = 8;
   ;	
	?debug	L 170
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx],8
   ;	
   ;			asteroids[ index ].color  = 15;
   ;	
	?debug	L 171
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+2],15
   ;	
   ;			asteroids[ index ].x0	  = 50;
   ;	
	?debug	L 172
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+6],16968
	mov	word ptr DGROUP:_asteroids[bx+4],0
   ;	
   ;			asteroids[ index ].y0	  = 200;
   ;	
	?debug	L 173
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+10],17224
	mov	word ptr DGROUP:_asteroids[bx+8],0
   ;	
   ;			asteroids[ index ].z0	  = 20 ;
   ;	
	?debug	L 174
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+14],16800
	mov	word ptr DGROUP:_asteroids[bx+12],0
   ;	
   ;			asteroids[ index ].x_velocity = 0;
   ;	
	?debug	L 175
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+18],0
	mov	word ptr DGROUP:_asteroids[bx+16],0
   ;	
   ;			asteroids[ index ].y_velocity = 0;
   ;	
	?debug	L 176
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+22],0
	mov	word ptr DGROUP:_asteroids[bx+20],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 0 ].p[ X_COMP ] = -1.0 + 3 * index ;
   ;	
	?debug	L 178
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fadd	qword ptr DGROUP:s@
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+24]
   ;	
   ;			asteroids[ index ].vertices[ 0 ].p[ Y_COMP ] = -1.0 ;
   ;	
	?debug	L 179
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+30],49024
	mov	word ptr DGROUP:_asteroids[bx+28],0
   ;	
   ;			asteroids[ index ].vertices[ 0 ].p[ Z_COMP ] = 0.0 ;
   ;	
	?debug	L 180
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+34],0
	mov	word ptr DGROUP:_asteroids[bx+32],0
   ;	
   ;			asteroids[ index ].vertices[ 0 ].p[ N_COMP ] = 1.0;
   ;	
	?debug	L 181
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+38],16256
	mov	word ptr DGROUP:_asteroids[bx+36],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 1 ].p[ X_COMP ] = -1.0 + 3 * index ;
   ;	
	?debug	L 183
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fadd	qword ptr DGROUP:s@
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+40]
   ;	
   ;			asteroids[ index ].vertices[ 1 ].p[ Y_COMP ] =  1.0 ;
   ;	
	?debug	L 184
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+46],16256
	mov	word ptr DGROUP:_asteroids[bx+44],0
   ;	
   ;			asteroids[ index ].vertices[ 1 ].p[ Z_COMP ] = 0.0 ;
   ;	
	?debug	L 185
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+50],0
	mov	word ptr DGROUP:_asteroids[bx+48],0
   ;	
   ;			asteroids[ index ].vertices[ 1 ].p[ N_COMP ] = 1.0;
   ;	
	?debug	L 186
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+54],16256
	mov	word ptr DGROUP:_asteroids[bx+52],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 2 ].p[ X_COMP ] = 1.0 + 3 * index ;
   ;	
	?debug	L 188
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fld1	
	fadd	
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+56]
   ;	
   ;			asteroids[ index ].vertices[ 2 ].p[ Y_COMP ] = 1.0 ;
   ;	
	?debug	L 189
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+62],16256
	mov	word ptr DGROUP:_asteroids[bx+60],0
   ;	
   ;			asteroids[ index ].vertices[ 2 ].p[ Z_COMP ] = 0.0 ;
   ;	
	?debug	L 190
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+66],0
	mov	word ptr DGROUP:_asteroids[bx+64],0
   ;	
   ;			asteroids[ index ].vertices[ 2 ].p[ N_COMP ] = 1.0 ;
   ;	
	?debug	L 191
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+70],16256
	mov	word ptr DGROUP:_asteroids[bx+68],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 3 ].p[ X_COMP ] = 1.0 + 3 * index ;
   ;	
	?debug	L 193
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fld1	
	fadd	
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+72]
   ;	
   ;			asteroids[ index ].vertices[ 3 ].p[ Y_COMP ] = -1.0 ;
   ;	
	?debug	L 194
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+78],49024
	mov	word ptr DGROUP:_asteroids[bx+76],0
   ;	
   ;			asteroids[ index ].vertices[ 3 ].p[ Z_COMP ] = 0.0 ;
   ;	
	?debug	L 195
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+82],0
	mov	word ptr DGROUP:_asteroids[bx+80],0
   ;	
   ;			asteroids[ index ].vertices[ 3 ].p[ N_COMP ] = 1.0 ;
   ;	
	?debug	L 196
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+86],16256
	mov	word ptr DGROUP:_asteroids[bx+84],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 4 ].p[ X_COMP ] = 1.0 + 3 * index ;
   ;	
	?debug	L 198
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fld1	
	fadd	
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+88]
   ;	
   ;			asteroids[ index ].vertices[ 4 ].p[ Y_COMP ] = -1.0 ;
   ;	
	?debug	L 199
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+94],49024
	mov	word ptr DGROUP:_asteroids[bx+92],0
   ;	
   ;			asteroids[ index ].vertices[ 4 ].p[ Z_COMP ] = 1.0 ;
   ;	
	?debug	L 200
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+98],16256
	mov	word ptr DGROUP:_asteroids[bx+96],0
   ;	
   ;			asteroids[ index ].vertices[ 4 ].p[ N_COMP ] = 1.0 ;
   ;	
	?debug	L 201
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+102],16256
	mov	word ptr DGROUP:_asteroids[bx+100],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 5 ].p[ X_COMP ] = -1.0 + 3 * index ;
   ;	
	?debug	L 203
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fadd	qword ptr DGROUP:s@
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+104]
   ;	
   ;			asteroids[ index ].vertices[ 5 ].p[ Y_COMP ] = -1.0 ;
   ;	
	?debug	L 204
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+110],49024
	mov	word ptr DGROUP:_asteroids[bx+108],0
   ;	
   ;			asteroids[ index ].vertices[ 5 ].p[ Z_COMP ] = 1.0 ;
   ;	
	?debug	L 205
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+114],16256
	mov	word ptr DGROUP:_asteroids[bx+112],0
   ;	
   ;			asteroids[ index ].vertices[ 5 ].p[ N_COMP ] = 1.0 ;
   ;	
	?debug	L 206
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+118],16256
	mov	word ptr DGROUP:_asteroids[bx+116],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 6 ].p[ X_COMP ] = -1.0 + 3 * index ;
   ;	
	?debug	L 208
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fadd	qword ptr DGROUP:s@
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+120]
   ;	
   ;			asteroids[ index ].vertices[ 6 ].p[ Y_COMP ] = 1.0 ;
   ;	
	?debug	L 209
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+126],16256
	mov	word ptr DGROUP:_asteroids[bx+124],0
   ;	
   ;			asteroids[ index ].vertices[ 6 ].p[ Z_COMP ] = 1.0 ;
   ;	
	?debug	L 210
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+130],16256
	mov	word ptr DGROUP:_asteroids[bx+128],0
   ;	
   ;			asteroids[ index ].vertices[ 6 ].p[ N_COMP ] = 1.0 ;
   ;	
	?debug	L 211
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+134],16256
	mov	word ptr DGROUP:_asteroids[bx+132],0
   ;	
   ;	
   ;			asteroids[ index ].vertices[ 7 ].p[ X_COMP ] = 1.0 + 3 * index ;
   ;	
	?debug	L 213
	mov	ax,si
	mov	dx,3
	imul	dx
	mov	word ptr [bp-14],ax
	fild	word ptr [bp-14]
	fld1	
	fadd	
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fstp	dword ptr DGROUP:_asteroids[bx+136]
   ;	
   ;			asteroids[ index ].vertices[ 7 ].p[ Y_COMP ] = 1.0 ;
   ;	
	?debug	L 214
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fwait	
	mov	word ptr DGROUP:_asteroids[bx+142],16256
	mov	word ptr DGROUP:_asteroids[bx+140],0
   ;	
   ;			asteroids[ index ].vertices[ 7 ].p[ Z_COMP ] = 1.0 ;
   ;	
	?debug	L 215
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+146],16256
	mov	word ptr DGROUP:_asteroids[bx+144],0
   ;	
   ;			asteroids[ index ].vertices[ 7 ].p[ N_COMP ] = 1.0 ;
   ;	
	?debug	L 216
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	mov	word ptr DGROUP:_asteroids[bx+150],16256
	mov	word ptr DGROUP:_asteroids[bx+148],0
   ;	
   ;	
   ;			Scale_Object_Mat( &asteroids[ index ] );
   ;	
	?debug	L 218
	mov	ax,si
	mov	dx,280
	imul	dx
	add	ax,offset DGROUP:_asteroids
	push	ax
	call	near ptr _Scale_Object_Mat
	pop	cx
	?debug	L 168
	inc	si
@9@114:
	cmp	si,5
	jge	@@3
	jmp	@9@58
@@3:
   ;	
   ;	
   ;		} // end for index
   ;	} // end Create_Field
   ;	
	?debug	L 221
	pop	si
	mov	sp,bp
	pop	bp
	ret	
	?debug	C E601730E02F4FF0001630E02F8FF0005616E676C+
	?debug	C 650E02FCFF0005696E64657804040600
	?debug	E
	?debug	E
_Create_Field	endp
   ;	
   ;	void Draw_Asteroids( int erase)
   ;	
	?debug	L 223
	assume	cs:_TEXT
_Draw_Asteroids	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	sub	sp,14
	push	si
	push	di
	?debug	C E6056572617365040A040000
	?debug	B
   ;	
   ;	{
   ;		int index,vertex;
   ;		float x0,y0,zz;
   ;	
   ;		for( index=0; index< NUM_OBJECTS; index++)
   ;	
	?debug	L 228
	xor	si,si
	jmp	@10@506
@10@58:
   ;	
   ;		{
   ;			if(erase==ERASE)
   ;	
	?debug	L 230
	cmp	word ptr [bp+4],0
	jne	short @10@114
   ;	
   ;				setcolor(0);
   ;	
	?debug	L 231
	xor	ax,ax
	push	ax
	jmp	short @10@142
@10@114:
   ;	
   ;			else
   ;				setcolor(asteroids[ index ].color);
   ;	
	?debug	L 233
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	push	word ptr DGROUP:_asteroids[bx+2]
@10@142:
	call	far ptr _setcolor
	pop	cx
   ;	
   ;	
   ;			x0 = asteroids[ index ].x0;
   ;	
	?debug	L 235
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+4]
	fstp	dword ptr [bp-4]
   ;	
   ;			y0 = asteroids[ index ].y0;
   ;	
	?debug	L 236
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+8]
	fstp	dword ptr [bp-8]
   ;	
   ;			zz = asteroids[ index ].vertices[ 0 ].p[Z_COMP];
   ;	
	?debug	L 237
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+32]
	fstp	dword ptr [bp-12]
   ;	
   ;			if (zz) zz=1;
   ;	
	?debug	L 238
	fld	dword ptr [bp-12]
	fldz	
	fcompp
	fstsw	word ptr [bp-14]
	fwait	
	mov	ax,word ptr [bp-14]
	sahf	
	je	short @10@226
	mov	word ptr [bp-10],16256
	mov	word ptr [bp-12],0
@10@226:
   ;	
   ;	
   ;			moveto( (int) ( DISTANCE * (x0 + asteroids[ index ].vertices[ 0 ].p[X_COMP]) / zz ),
   ;	
	?debug	L 240
   ;	
   ;					(int) ( DISTANCE * (y0 + asteroids[ index ].vertices[ 0 ].p[Y_COMP]) / zz ) );
   ;	
	?debug	L 241
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+28]
	fadd	dword ptr [bp-8]
	fmul	dword ptr DGROUP:s@+8
	fdiv	dword ptr [bp-12]
	call	near ptr N_FTOL@
	push	ax
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+24]
	fadd	dword ptr [bp-4]
	fmul	dword ptr DGROUP:s@+8
	fdiv	dword ptr [bp-12]
	call	near ptr N_FTOL@
	push	ax
	call	far ptr _moveto
	pop	cx
	pop	cx
   ;	
   ;	
   ;			for( vertex=1; vertex< asteroids[ index ].num_vertices; vertex++ )
   ;	
	?debug	L 243
	mov	di,1
	jmp	@10@366
@10@254:
   ;	
   ;			{
   ;				zz = asteroids[ index ].vertices[ vertex ].p[Z_COMP];
   ;	
	?debug	L 245
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	dx,di
	mov	cl,4
	shl	dx,cl
	add	ax,dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+32]
	fstp	dword ptr [bp-12]
   ;	
   ;				if (zz) zz=1;
   ;	
	?debug	L 246
	fld	dword ptr [bp-12]
	fldz	
	fcompp
	fstsw	word ptr [bp-14]
	fwait	
	mov	ax,word ptr [bp-14]
	sahf	
	je	short @10@310
	mov	word ptr [bp-10],16256
	mov	word ptr [bp-12],0
@10@310:
   ;	
   ;	
   ;				lineto( (int) ( DISTANCE * (x0 + asteroids[ index ].vertices[ vertex ].p[X_COMP]) / zz ),
   ;	
	?debug	L 248
   ;	
   ;						(int) ( DISTANCE * (y0 + asteroids[ index ].vertices[ vertex ].p[Y_COMP]) / zz ) );
   ;	
	?debug	L 249
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	dx,di
	mov	cl,4
	shl	dx,cl
	add	ax,dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+28]
	fadd	dword ptr [bp-8]
	fmul	dword ptr DGROUP:s@+8
	fdiv	dword ptr [bp-12]
	call	near ptr N_FTOL@
	push	ax
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	dx,di
	mov	cl,4
	shl	dx,cl
	add	ax,dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+24]
	fadd	dword ptr [bp-4]
	fmul	dword ptr DGROUP:s@+8
	fdiv	dword ptr [bp-12]
	call	near ptr N_FTOL@
	push	ax
	call	far ptr _lineto
	pop	cx
	pop	cx
	?debug	L 243
	inc	di
@10@366:
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	cmp	word ptr DGROUP:_asteroids[bx],di
	jle	@@4
	jmp	@10@254
@@4:
   ;	
   ;			} // end for vertex
   ;	
   ;			zz = asteroids[ index ].vertices[ 0 ].p[Z_COMP];
   ;	
	?debug	L 252
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+32]
	fstp	dword ptr [bp-12]
   ;	
   ;			if (zz) zz=1;
   ;	
	?debug	L 253
	fld	dword ptr [bp-12]
	fldz	
	fcompp
	fstsw	word ptr [bp-14]
	fwait	
	mov	ax,word ptr [bp-14]
	sahf	
	je	short @10@450
	mov	word ptr [bp-10],16256
	mov	word ptr [bp-12],0
@10@450:
   ;	
   ;	
   ;			lineto( (int) ( DISTANCE * (x0 + asteroids[ index ].vertices[ 0 ].p[X_COMP]) / zz ),
   ;	
	?debug	L 255
   ;	
   ;					(int) ( DISTANCE * (y0 + asteroids[ index ].vertices[ 0 ].p[Y_COMP]) / zz ) );
   ;	
	?debug	L 256
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+28]
	fadd	dword ptr [bp-8]
	fmul	dword ptr DGROUP:s@+8
	fdiv	dword ptr [bp-12]
	call	near ptr N_FTOL@
	push	ax
	mov	ax,si
	mov	dx,280
	imul	dx
	mov	bx,ax
	fld	dword ptr DGROUP:_asteroids[bx+24]
	fadd	dword ptr [bp-4]
	fmul	dword ptr DGROUP:s@+8
	fdiv	dword ptr [bp-12]
	call	near ptr N_FTOL@
	push	ax
	call	far ptr _lineto
	pop	cx
	pop	cx
	?debug	L 228
	inc	si
@10@506:
	cmp	si,5
	jge	@@5
	jmp	@10@58
@@5:
   ;	
   ;	
   ;		} // end for index
   ;	} // end Draw_Asteroids
   ;	
	?debug	L 259
	pop	di
	pop	si
	mov	sp,bp
	pop	bp
	ret	
	?debug	C E6027A7A0E02F4FF000279300E02F8FF00027830+
	?debug	C 0E02FCFF00067665727465780404070005696E64+
	?debug	C 657804040600056572617365040A040000
	?debug	E
	?debug	E
_Draw_Asteroids	endp
   ;	
   ;	void Rotate_Asteroids( int x )
   ;	
	?debug	L 262
	assume	cs:_TEXT
_Rotate_Asteroids	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	push	di
	?debug	C E60178040A040000
	mov	di,word ptr [bp+4]
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;		for( index=0; index< NUM_OBJECTS; index++)
   ;	
	?debug	L 265
	xor	si,si
	jmp	short @11@254
@11@58:
   ;	
   ;		{
   ;			if(x==0)
   ;	
	?debug	L 267
	or	di,di
	jne	short @11@114
   ;	
   ;				Rotate_X_Object_Mat( &asteroids[ index ] );
   ;	
	?debug	L 268
	mov	ax,si
	mov	dx,280
	imul	dx
	add	ax,offset DGROUP:_asteroids
	push	ax
	call	near ptr _Rotate_X_Object_Mat
	pop	cx
@11@114:
   ;	
   ;			if(x==1)
   ;	
	?debug	L 269
	cmp	di,1
	jne	short @11@170
   ;	
   ;				Rotate_Y_Object_Mat( &asteroids[ index ] );
   ;	
	?debug	L 270
	mov	ax,si
	mov	dx,280
	imul	dx
	add	ax,offset DGROUP:_asteroids
	push	ax
	call	near ptr _Rotate_Y_Object_Mat
	pop	cx
@11@170:
   ;	
   ;			if(x==2)
   ;	
	?debug	L 271
	cmp	di,2
	jne	short @11@226
   ;	
   ;				Rotate_Z_Object_Mat( &asteroids[ index ] );
   ;	
	?debug	L 272
	mov	ax,si
	mov	dx,280
	imul	dx
	add	ax,offset DGROUP:_asteroids
	push	ax
	call	near ptr _Rotate_Z_Object_Mat
	pop	cx
@11@226:
	?debug	L 265
	inc	si
@11@254:
	cmp	si,5
	jl	short @11@58
   ;	
   ;		}
   ;	}	// End Rotate_Asteroids
   ;	
	?debug	L 274
	pop	di
	pop	si
	pop	bp
	ret	
	?debug	C E605696E646578040406000178040C0700
	?debug	E
	?debug	E
_Rotate_Asteroids	endp
   ;	
   ;	void Scale_P_Asteroids( void )
   ;	
	?debug	L 277
	assume	cs:_TEXT
_Scale_P_Asteroids	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;	
   ;	
   ;		for( index=0; index< NUM_OBJECTS; index++)
   ;	
	?debug	L 282
	xor	si,si
	jmp	short @12@114
@12@58:
   ;	
   ;		{
   ;			Make_Identity( &scale);
   ;	
	?debug	L 284
	mov	ax,offset DGROUP:_scale
	push	ax
	call	near ptr _Make_Identity
	pop	cx
   ;	
   ;			scale.elem[0][0]+= 0.2;
   ;	
	?debug	L 285
	fld	dword ptr DGROUP:_scale
	fadd	qword ptr DGROUP:s@+12
	fstp	dword ptr DGROUP:_scale
   ;	
   ;			scale.elem[1][1]+= 0.2;
   ;	
	?debug	L 286
	fld	dword ptr DGROUP:_scale+20
	fadd	qword ptr DGROUP:s@+12
	fstp	dword ptr DGROUP:_scale+20
   ;	
   ;			scale.elem[2][2]+= 0.2;
   ;	
	?debug	L 287
	fld	dword ptr DGROUP:_scale+40
	fadd	qword ptr DGROUP:s@+12
	fstp	dword ptr DGROUP:_scale+40
   ;	
   ;			Scale_Object_Mat( &asteroids[ index ] );
   ;	
	?debug	L 288
	mov	ax,si
	mov	dx,280
	imul	dx
	add	ax,offset DGROUP:_asteroids
	push	ax
	fwait	
	call	near ptr _Scale_Object_Mat
	pop	cx
	?debug	L 282
	inc	si
@12@114:
	cmp	si,5
	jl	short @12@58
   ;	
   ;		}
   ;	}	// End Scale_+_Asteroids
   ;	
	?debug	L 290
	pop	si
	pop	bp
	ret	
	?debug	C E605696E64657804040600
	?debug	E
	?debug	E
_Scale_P_Asteroids	endp
   ;	
   ;	void Scale_M_Asteroids( void )
   ;	
	?debug	L 292
	assume	cs:_TEXT
_Scale_M_Asteroids	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	push	si
	?debug	B
   ;	
   ;	{
   ;		int index;
   ;	
   ;	
   ;		for( index=0; index< NUM_OBJECTS; index++)
   ;	
	?debug	L 297
	xor	si,si
	jmp	short @13@114
@13@58:
   ;	
   ;		{
   ;			Make_Identity( &scale);
   ;	
	?debug	L 299
	mov	ax,offset DGROUP:_scale
	push	ax
	call	near ptr _Make_Identity
	pop	cx
   ;	
   ;			scale.elem[0][0]-= 0.2;
   ;	
	?debug	L 300
	fld	dword ptr DGROUP:_scale
	fsub	qword ptr DGROUP:s@+12
	fstp	dword ptr DGROUP:_scale
   ;	
   ;			scale.elem[1][1]-= 0.2;
   ;	
	?debug	L 301
	fld	dword ptr DGROUP:_scale+20
	fsub	qword ptr DGROUP:s@+12
	fstp	dword ptr DGROUP:_scale+20
   ;	
   ;			scale.elem[2][2]-= 0.2;
   ;	
	?debug	L 302
	fld	dword ptr DGROUP:_scale+40
	fsub	qword ptr DGROUP:s@+12
	fstp	dword ptr DGROUP:_scale+40
   ;	
   ;			Scale_Object_Mat( &asteroids[ index ] );
   ;	
	?debug	L 303
	mov	ax,si
	mov	dx,280
	imul	dx
	add	ax,offset DGROUP:_asteroids
	push	ax
	fwait	
	call	near ptr _Scale_Object_Mat
	pop	cx
	?debug	L 297
	inc	si
@13@114:
	cmp	si,5
	jl	short @13@58
   ;	
   ;		}
   ;	}	// End Scale_-_Asteroids
   ;	
	?debug	L 305
	pop	si
	pop	bp
	ret	
	?debug	C E605696E64657804040600
	?debug	E
	?debug	E
_Scale_M_Asteroids	endp
   ;	
   ;	void main( void )
   ;	
	?debug	L 307
	assume	cs:_TEXT
_main	proc	near
	?debug	B
	push	bp
	mov	bp,sp
	sub	sp,6
	push	si
	push	di
	?debug	B
   ;	
   ;	{
   ;		int out = 0;
   ;	
	?debug	L 309
	xor	si,si
   ;	
   ;	
   ;	   /* request auto detection */
   ;	   int gdriver = DETECT, gmode, errorcode;
   ;	
	?debug	L 312
	mov	word ptr [bp-2],0
   ;	
   ;	
   ;	   /* initialize graphics and local variables */
   ;	   initgraph(&gdriver, &gmode, "");
   ;	
	?debug	L 315
	push	ds
	mov	ax,offset DGROUP:s@+20
	push	ax
	push	ss
	lea	ax,word ptr [bp-4]
	push	ax
	push	ss
	lea	ax,word ptr [bp-2]
	push	ax
	call	far ptr _initgraph
	add	sp,12
   ;	
   ;	
   ;	   /* read result of initialization */
   ;	   errorcode = graphresult();
   ;	
	?debug	L 318
	call	far ptr _graphresult
	mov	di,ax
   ;	
   ;	   if (errorcode != grOk)
   ;	
	?debug	L 319
	or	di,di
	je	short @14@86
   ;	
   ;	   {
   ;		  printf("Graphics error: %s\n", grapherrormsg(errorcode));
   ;	
	?debug	L 321
	push	di
	call	far ptr _grapherrormsg
	pop	cx
	push	ax
	mov	ax,offset DGROUP:s@+21
	push	ax
	call	near ptr _printf
	pop	cx
	pop	cx
   ;	
   ;		  printf("Press any key to halt:");
   ;	
	?debug	L 322
	mov	ax,offset DGROUP:s@+41
	push	ax
	call	near ptr _printf
	pop	cx
   ;	
   ;		  getch();
   ;	
	?debug	L 323
	call	near ptr _getch
   ;	
   ;		  exit(1);
   ;	
	?debug	L 324
	mov	ax,1
	push	ax
	call	near ptr _exit
	pop	cx
@14@86:
   ;	
   ;	   }
   ;	
   ;		Create_Field();
   ;	
	?debug	L 327
	call	near ptr _Create_Field
	jmp	short @14@506
@14@114:
   ;	
   ;	
   ;		while(!out){
   ;			switch( getch() ) {
   ;	
	?debug	L 330
	call	near ptr _getch
	mov	word ptr [bp-6],ax
	mov	cx,5
	mov	bx,offset @14@C418
@14@170:
	mov	ax,word ptr cs:[bx]
	cmp	ax,word ptr [bp-6]
	je	short @14@254
	add	bx,2
	loop	short @14@170
	jmp	short @14@478
@14@254:
	jmp	word ptr cs:[bx+10]
@14@282:
   ;	
   ;				case 'x':
   ;					Draw_Asteroids( ERASE );
   ;	
	?debug	L 332
	xor	ax,ax
	push	ax
	call	near ptr _Draw_Asteroids
	pop	cx
   ;	
   ;					Rotate_Asteroids( 0 );
   ;	
	?debug	L 333
	xor	ax,ax
@14@310:
	push	ax
	call	near ptr _Rotate_Asteroids
	pop	cx
@14@338:
   ;	
   ;					Draw_Asteroids( DRAW );
   ;	
	?debug	L 334
	mov	ax,1
	push	ax
	call	near ptr _Draw_Asteroids
	pop	cx
   ;	
   ;					Delay( 100 );
   ;	
	?debug	L 335
	mov	ax,100
	push	ax
	call	near ptr _Delay
	pop	cx
   ;	
   ;					break;
   ;	
	?debug	L 336
	jmp	short @14@506
@14@366:
   ;	
   ;				case 'y':
   ;					Draw_Asteroids( ERASE );
   ;	
	?debug	L 338
	xor	ax,ax
	push	ax
	call	near ptr _Draw_Asteroids
	pop	cx
   ;	
   ;					Rotate_Asteroids( 1 );
   ;	
	?debug	L 339
	mov	ax,1
	jmp	short @14@310
@14@394:
   ;	
   ;					Draw_Asteroids( DRAW );
   ;					Delay( 100 );
   ;					break;
   ;				case 'z':
   ;					Draw_Asteroids( ERASE );
   ;	
	?debug	L 344
	xor	ax,ax
	push	ax
	call	near ptr _Draw_Asteroids
	pop	cx
   ;	
   ;					Rotate_Asteroids( 2 );
   ;	
	?debug	L 345
	mov	ax,2
	jmp	short @14@310
@14@422:
   ;	
   ;					Draw_Asteroids( DRAW );
   ;					Delay( 100 );
   ;					break;
   ;				case '+':
   ;					Draw_Asteroids( ERASE );
   ;	
	?debug	L 350
	xor	ax,ax
	push	ax
	call	near ptr _Draw_Asteroids
	pop	cx
   ;	
   ;					Scale_P_Asteroids();
   ;	
	?debug	L 351
	call	near ptr _Scale_P_Asteroids
	jmp	short @14@338
@14@450:
   ;	
   ;					Draw_Asteroids( DRAW );
   ;					Delay( 100 );
   ;					break;
   ;				case '-':
   ;					Draw_Asteroids( ERASE );
   ;	
	?debug	L 356
	xor	ax,ax
	push	ax
	call	near ptr _Draw_Asteroids
	pop	cx
   ;	
   ;					Scale_M_Asteroids();
   ;	
	?debug	L 357
	call	near ptr _Scale_M_Asteroids
	jmp	short @14@338
@14@478:
   ;	
   ;					Draw_Asteroids( DRAW );
   ;					Delay( 100 );
   ;					break;
   ;				default:
   ;					out = 1;
   ;	
	?debug	L 362
	mov	si,1
@14@506:
	?debug	L 329
	or	si,si
	je	short @14@114
   ;	
   ;			} // End of Switch
   ;		} // End of While
   ;	
   ;		closegraph();
   ;	
	?debug	L 366
	call	far ptr _closegraph
   ;	
   ;	}
   ;	
	?debug	L 367
	pop	di
	pop	si
	mov	sp,bp
	pop	bp
	ret	
	?debug	C E6096572726F72636F64650404070005676D6F64+
	?debug	C 650402FCFF0007676472697665720402FEFF0003+
	?debug	C 6F757404040600
	?debug	E
	?debug	E
_main	endp
@14@C418	label	word
	db	43
	db	0
	db	45
	db	0
	db	120
	db	0
	db	121
	db	0
	db	122
	db	0
	dw	@14@422
	dw	@14@450
	dw	@14@282
	dw	@14@366
	dw	@14@394
_TEXT	ends
_BSS	segment word public 'BSS'
_rotation_z	label	word
	db	64 dup (?)
_rotation_y	label	word
	db	64 dup (?)
_rotation_x	label	word
	db	64 dup (?)
_scale	label	word
	db	64 dup (?)
_asteroids	label	word
	db	1400 dup (?)
	?debug	C E9
	?debug	C FA00000000
_BSS	ends
_DATA	segment word public 'DATA'
s@	label	byte
	db	0
	db	0
	db	0
	db	0
	db	0
	db	0
	db	240
	db	191
	db	0
	db	0
	db	224
	db	'@'
	db	154
	db	153
	db	153
	db	153
	db	153
	db	153
	db	201
	db	'?'
	db	0
	db	'Graphics error: %s'
	db	10
	db	0
	db	'Press any key to halt:'
	db	0
_DATA	ends
_TEXT	segment byte public 'CODE'
_TEXT	ends
	extrn	_exit:near
	extrn	_getch:near
	extrn	_printf:near
	public	_main
	public	_Scale_M_Asteroids
	public	_Scale_P_Asteroids
	public	_Rotate_Asteroids
	extrn	N_FTOL@:far
	public	_Draw_Asteroids
	public	_Create_Field
	public	_Rotate_Z_Object_Mat
	public	_Rotate_Y_Object_Mat
	public	_Rotate_X_Object_Mat
	public	_Scale_Object_Mat
	public	_Mat_Mul
	public	_Clear_Matrix
	public	_Make_Identity
	public	_Delay
	public	_rotation_z
	public	_rotation_y
	public	_rotation_x
	public	_scale
	public	_asteroids
	extrn	_sin:near
	extrn	_cos:near
	extrn	_setcolor:far
	extrn	_moveto:far
	extrn	_lineto:far
	extrn	_initgraph:far
	extrn	_graphresult:far
	extrn	_grapherrormsg:far
	extrn	_closegraph:far
_s@	equ	s@
	?debug	C EA0109
	?debug	C E32200000023040000
	?debug	C EB055F657869742200
	?debug	C EB065F67657463682200
	?debug	C EB075F7072696E74662200
	?debug	C E32300000023010000
	?debug	C EC055F6D61696E231800
	?debug	C E32400000023010000
	?debug	C EC125F5363616C655F4D5F41737465726F696473+
	?debug	C 241800
	?debug	C E32500000023010000
	?debug	C EC125F5363616C655F505F41737465726F696473+
	?debug	C 251800
	?debug	C E32600000023010000
	?debug	C EC115F526F746174655F41737465726F69647326+
	?debug	C 1800
	?debug	C E32700000023040500
	?debug	C EB074E5F46544F4C402700
	?debug	C E32800000023010000
	?debug	C EC0F5F447261775F41737465726F696473281800
	?debug	C E32900000023010000
	?debug	C EC0D5F4372656174655F4669656C64291800
	?debug	C E32A00000023010000
	?debug	C EC145F526F746174655F5A5F4F626A6563745F4D+
	?debug	C 61742A1800
	?debug	C E32B00000023010000
	?debug	C EC145F526F746174655F595F4F626A6563745F4D+
	?debug	C 61742B1800
	?debug	C E32C00000023010000
	?debug	C EC145F526F746174655F585F4F626A6563745F4D+
	?debug	C 61742C1800
	?debug	C E32D00000023010000
	?debug	C EC115F5363616C655F4F626A6563745F4D61742D+
	?debug	C 1800
	?debug	C E32E00000023010000
	?debug	C EC085F4D61745F4D756C2E1800
	?debug	C E32F00000023010000
	?debug	C EC0D5F436C6561725F4D61747269782F1800
	?debug	C E33000000023010000
	?debug	C EC0E5F4D616B655F4964656E74697479301800
	?debug	C E33100000023010000
	?debug	C EC065F44656C6179311800
	?debug	C EC0B5F726F746174696F6E5F7A190000
	?debug	C EC0B5F726F746174696F6E5F79190000
	?debug	C EC0B5F726F746174696F6E5F78190000
	?debug	C EC065F7363616C65190000
	?debug	C E3320078051A20
	?debug	C EC0A5F61737465726F696473320000
	?debug	C E333000000230F0000
	?debug	C EB045F73696E3300
	?debug	C E334000000230F0000
	?debug	C EB045F636F733400
	?debug	C E33500000023010400
	?debug	C EB095F736574636F6C6F723500
	?debug	C E33600000023010400
	?debug	C EB075F6D6F7665746F3600
	?debug	C E33700000023010400
	?debug	C EB075F6C696E65746F3700
	?debug	C E33800000023010400
	?debug	C EB0A5F696E697467726170683800
	?debug	C E33900000023040400
	?debug	C EB0C5F6772617068726573756C743900
	?debug	C E33B000200150204
	?debug	C E33A000000233B0400
	?debug	C EB0E5F67726170686572726F726D73673A00
	?debug	C E33C00000023010400
	?debug	C EB0B5F636C6F736567726170683C00
	?debug	C E60A6F626A6563745F7074721F0600066F626A65+
	?debug	C 63742006000A6D61747269785F70747218060006+
	?debug	C 6D61747269781906000A7665727465785F707472+
	?debug	C 1C0600067665727465781D06000A6F626A656374+
	?debug	C 5F7479702007000A6D61747269785F7479701907+
	?debug	C 000A7665727465785F7479701D0700
	?debug	C E20004656C656D1AC040000000
	?debug	C E20001701EC010000000
	?debug	C E2000C6E756D5F7665727469636573040005636F+
	?debug	C 6C6F7204000278300E000279300E00027A300E00+
	?debug	C 0A785F76656C6F636974790E000A795F76656C6F+
	?debug	C 636974790E0008766572746963657321C0180100+
	?debug	C 00
	end
