////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                           Prueba de 3D                                     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// Autor: Ricardo Quesada                                                     //
// Fecha: 13/3/95,8/4/95                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


////////////////////////     INCLUDES      /////////////////////////////////////
#include <stdio.h>
#include <graphics.h>
#include <math.h>

////////////////////////     DEFINES       /////////////////////////////////////
#define NUM_OBJECTS 1
#define ERASE 0
#define DRAW  1

#define X_COMP 0
#define Y_COMP 1
#define Z_COMP 2
#define N_COMP 3
#define DISTANCE 1



////////////////////////     TYPEDEFS      /////////////////////////////////////
typedef struct vertex_typ
	{
		float p[4];
	} vertex, *vertex_ptr;


typedef struct matrix_typ
	{
		float elem[4][4];
	} matrix, *matrix_ptr;

typedef struct object_typ
	{
		int num_vertices;
		int color;
		float x0,y0,z0;
		float x_velocity;
		float y_velocity;
		matrix scale;
		matrix rotation_x;
		matrix rotation_y;
		matrix rotation_z;
		vertex vertices[16];
	} object, *object_ptr;


////////////////////////       GLOBALS      ////////////////////////////////////

object  asteroids[ NUM_OBJECTS ];


////////////////////////      FUNCIONES     ////////////////////////////////////

void Delay( int t)
{
	float x = 1;
	while( t-- >0)
		x=cos(x);
} // end Delay


void Make_Identity( matrix_ptr i)
{
	i->elem[0][0] = i->elem[1][1] = i->elem[2][2] = i->elem[3][3] =1;
	i->elem[0][1] = i->elem[0][2] = i->elem[0][3] = i->elem[1][2] = i->elem[1][3] = i->elem[2][3] = 0;
	i->elem[1][0] = i->elem[2][0] = i->elem[3][0] = i->elem[2][1] = i->elem[3][1] = i->elem[3][2] = 0;
}	// end Make_Identity

void Clear_Matrix( matrix_ptr i)
{
	i->elem[0][0] = i->elem[1][1] = i->elem[2][2] = i->elem[3][3] =0;
	i->elem[0][1] = i->elem[0][2] = i->elem[0][3] = i->elem[1][2] = i->elem[1][3] = i->elem[2][3] = 0;
	i->elem[1][0] = i->elem[2][0] = i->elem[3][0] = i->elem[2][1] = i->elem[3][1] = i->elem[3][2] = 0;
}

void Mat_Mul( vertex_ptr v, matrix_ptr m)
{
	float x_new, y_new, z_new;
	x_new = v->p[0] *m->elem[0][0] + v->p[1] *m->elem[1][0] + v->p[2] * m->elem[2][0] + v->p[3] * m->elem[3][0];
	y_new = v->p[0] *m->elem[0][1] + v->p[1] *m->elem[1][1] + v->p[2] * m->elem[2][1] + v->p[3] * m->elem[3][1];
	z_new = v->p[0] *m->elem[0][2] + v->p[1] *m->elem[1][2] + v->p[2] * m->elem[2][2] + v->p[3] * m->elem[3][2];

	v->p[X_COMP] = x_new;
	v->p[Y_COMP] = y_new;
	v->p[Z_COMP] = z_new;
}

void Scale_Object_Mat( object_ptr obj )
{
	int index;
	for( index=0; index < obj->num_vertices; index++ )
	{
		Mat_Mul( (vertex_ptr)&obj->vertices[index], (matrix_ptr)&obj->scale);
	}
}

void Rotate_X_Object_Mat( object_ptr obj)
{
	int index;
	for( index=0; index< obj->num_vertices; index++)
	{
		Mat_Mul( (vertex_ptr)&obj->vertices[ index ], (matrix_ptr)&obj->rotation_x);
	}
}

void Rotate_Y_Object_Mat( object_ptr obj)
{
	int index;
	for( index=0; index< obj->num_vertices; index++)
	{
		Mat_Mul( (vertex_ptr)&obj->vertices[ index ], (matrix_ptr)&obj->rotation_y);
	}
}
void Rotate_Z_Object_Mat( object_ptr obj)
{
	int index;
	for( index=0; index< obj->num_vertices; index++)
	{
		Mat_Mul( (vertex_ptr)&obj->vertices[ index ], (matrix_ptr)&obj->rotation_z);
	}
}

void Create_Field( void )
{
	int index;
	float angle,c,s;

	for( index=0; index<NUM_OBJECTS; index++)
	{
		asteroids[ index ].num_vertices = 8;
		asteroids[ index ].color  = 15;
		asteroids[ index ].x0	  = 300;
		asteroids[ index ].y0	  = 200;
		asteroids[ index ].z0	  = 20 ;
		asteroids[ index ].x_velocity = 0;
		asteroids[ index ].y_velocity = 0;

		// Rotation_X

		Make_Identity( (matrix_ptr)&asteroids[index].rotation_x);

		angle = 0.1;
		c = cos(angle);
		s = sin(angle);

		asteroids[ index ].rotation_x.elem[1][1] = c;
		asteroids[ index ].rotation_x.elem[1][2] = -s;
		asteroids[ index ].rotation_x.elem[2][1] = s;
		asteroids[ index ].rotation_x.elem[2][2] = c;

		// Rotation_Y

		Make_Identity( (matrix_ptr)&asteroids[index].rotation_y);

		angle = 0.1;
		c = cos(angle);
		s = sin(angle);

		asteroids[ index ].rotation_y.elem[0][0] = c;
		asteroids[ index ].rotation_y.elem[0][2] = s;
		asteroids[ index ].rotation_y.elem[2][0] = -s;
		asteroids[ index ].rotation_y.elem[2][2] = c;

		// Rotation_Z

		Make_Identity( (matrix_ptr)&asteroids[index].rotation_z);

		angle = 0.1;
		c = cos(angle);
		s = sin(angle);

		asteroids[ index ].rotation_z.elem[0][0] = c;
		asteroids[ index ].rotation_z.elem[0][1] = -s;
		asteroids[ index ].rotation_z.elem[1][0] = s;
		asteroids[ index ].rotation_z.elem[1][1] = c;


		// Scale

		Make_Identity( (matrix_ptr)&asteroids[index].scale);

		asteroids[ index ].scale.elem[0][0] = 35;
		asteroids[ index ].scale.elem[1][1] = 35;
		asteroids[ index ].scale.elem[2][2] = 35;

		asteroids[ index ].vertices[ 0 ].p[ X_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 0 ].p[ Y_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 0 ].p[ Z_COMP ] = 0.0 ;
		asteroids[ index ].vertices[ 0 ].p[ N_COMP ] = 1.0;

		asteroids[ index ].vertices[ 1 ].p[ X_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 1 ].p[ Y_COMP ] =  1.0 ;
		asteroids[ index ].vertices[ 1 ].p[ Z_COMP ] = 0.0 ;
		asteroids[ index ].vertices[ 1 ].p[ N_COMP ] = 1.0;

		asteroids[ index ].vertices[ 2 ].p[ X_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 2 ].p[ Y_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 2 ].p[ Z_COMP ] = 0.0 ;
		asteroids[ index ].vertices[ 2 ].p[ N_COMP ] = 1.0 ;

		asteroids[ index ].vertices[ 3 ].p[ X_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 3 ].p[ Y_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 3 ].p[ Z_COMP ] = 0.0 ;
		asteroids[ index ].vertices[ 3 ].p[ N_COMP ] = 1.0 ;

		asteroids[ index ].vertices[ 4 ].p[ X_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 4 ].p[ Y_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 4 ].p[ Z_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 4 ].p[ N_COMP ] = 1.0 ;

		asteroids[ index ].vertices[ 5 ].p[ X_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 5 ].p[ Y_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 5 ].p[ Z_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 5 ].p[ N_COMP ] = 1.0 ;

		asteroids[ index ].vertices[ 6 ].p[ X_COMP ] = -1.0 ;
		asteroids[ index ].vertices[ 6 ].p[ Y_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 6 ].p[ Z_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 6 ].p[ N_COMP ] = 1.0 ;

		asteroids[ index ].vertices[ 7 ].p[ X_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 7 ].p[ Y_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 7 ].p[ Z_COMP ] = 1.0 ;
		asteroids[ index ].vertices[ 7 ].p[ N_COMP ] = 1.0 ;


		Scale_Object_Mat( (object_ptr)&asteroids[ index ] );

	} // end for index
} // end Create_Field

void Draw_Asteroids( int erase)
{
	int index,vertex;
	float x0,y0;

	for( index=0; index< NUM_OBJECTS; index++)
	{
		if(erase==ERASE)
			setcolor(0);
		else
			setcolor(asteroids[ index ].color);

		x0 = asteroids[ index ].x0;
		y0 = asteroids[ index ].y0;

		moveto( (int) (  x0 + asteroids[ index ].vertices[ 0 ].p[X_COMP] ),
				(int) (  y0 + asteroids[ index ].vertices[ 0 ].p[Y_COMP] ) );

		for( vertex=1; vertex< asteroids[ index ].num_vertices; vertex++ )
		{
			lineto( (int) (  x0 + asteroids[ index ].vertices[ vertex ].p[X_COMP] ),
					(int) (  y0 + asteroids[ index ].vertices[ vertex ].p[Y_COMP] ) );
		} // end for vertex

		lineto( (int) (  x0 + asteroids[ index ].vertices[ 0 ].p[X_COMP] ),
				(int) (  y0 + asteroids[ index ].vertices[ 0 ].p[Y_COMP] ) );

	} // end for index
} // end Draw_Asteroids


void Rotate_Asteroids( int x )
{
	int index;
	for( index=0; index< NUM_OBJECTS; index++)
	{
		if(x==0)
			Rotate_X_Object_Mat( (object_ptr)&asteroids[ index ] );
		if(x==1)
			Rotate_Y_Object_Mat( (object_ptr)&asteroids[ index ] );
		if(x==2)
			Rotate_Z_Object_Mat( (object_ptr)&asteroids[ index ] );
	}
}	// End Rotate_Asteroids


void Scale_P_Asteroids( void )
{
	int index;
	for( index=0; index< NUM_OBJECTS; index++)
	{
		Make_Identity( (matrix_ptr)&asteroids[index].scale);
		asteroids[ index ].scale.elem[0][0]+= 0.2;
		asteroids[ index ].scale.elem[1][1]+= 0.2;
		asteroids[ index ].scale.elem[2][2]+= 0.2;
		Scale_Object_Mat( (object_ptr)&asteroids[ index ] );
	}
}	// End Scale_+_Asteroids

void Scale_M_Asteroids( void )
{
	int index;
	for( index=0; index< NUM_OBJECTS; index++)
	{
		Make_Identity( (matrix_ptr)&asteroids[index].scale);
		asteroids[ index ].scale.elem[0][0]-= 0.2;
		asteroids[ index ].scale.elem[1][1]-= 0.2;
		asteroids[ index ].scale.elem[2][2]-= 0.2;
		Scale_Object_Mat( (object_ptr)&asteroids[ index ] );
	}
}	// End Scale_-_Asteroids

void main( void )
{
	int out = 0;

   /* request auto detection */
   int gdriver = DETECT, gmode, errorcode;

   /* initialize graphics and local variables */
   initgraph(&gdriver, &gmode, "");

   /* read result of initialization */
   errorcode = graphresult();
   if (errorcode != grOk)
   {
	  printf("Graphics error: %s\n", grapherrormsg(errorcode));
	  printf("Press any key to halt:");
	  getch();
	  exit(1);
   }

	Create_Field();

	while(!out){
		switch( getch() ) {
			case 'x':
				Draw_Asteroids( ERASE );
				Rotate_Asteroids( 0 );
				Draw_Asteroids( DRAW );
				Delay( 100 );
				break;
			case 'y':
				Draw_Asteroids( ERASE );
				Rotate_Asteroids( 1 );
				Draw_Asteroids( DRAW );
				Delay( 100 );
				break;
			case 'z':
				Draw_Asteroids( ERASE );
				Rotate_Asteroids( 2 );
				Draw_Asteroids( DRAW );
				Delay( 100 );
				break;
			case '+':
				Draw_Asteroids( ERASE );
				Scale_P_Asteroids();
				Draw_Asteroids( DRAW );
				Delay( 100 );
				break;
			case '-':
				Draw_Asteroids( ERASE );
				Scale_M_Asteroids();
				Draw_Asteroids( DRAW );
				Delay( 100 );
				break;
			default:
				out = 1;
		} // End of Switch
	} // End of While

	closegraph();
}


