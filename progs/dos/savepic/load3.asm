;name: loadpic
;verion: 1.00
;start  date: 14/10/93
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        je nopasanada

        cmp al,'/'
        jne alop                ;loop until '/' or CR is found

        lodsb
        cmp al,'?'
        je displayhelp
        and al,11011111b
        cmp al,'N'
        je  newname

displayhelp:                    ;*****************command line H
        push cs
        pop ds
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***


nopasanada:                     ;****sin command line ****
        mov dx,offset installing_msg
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,4c00h            ;*** terminate ***
        int 21h


newname:                        ;******************** command line N
        lea bx,nombre1
        lodsb
buscanombre:
        lodsb
        mov cs:[bx],al
        cmp al,13
        je endname
        inc bx
        mov cs:[bx],al
        jmp short buscanombre

endname:
        mov byte ptr ds:[bx],0

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,2000h            ;128k
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        mov cs:seg_mem,ax


;********** drive user ***************;

        mov ah,0fh
        int 10h
        mov cs:oldvideo,al

        push cs
        pop ds
        mov dx,offset loading_msg  ;loading messagge
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,3d02h            ;open file with al=2 (W/R access)
        mov dx,offset nombre1
        int 21h
        jnc no_error

        jmp error               ;salta porque hubo error

no_error:
        mov cs:handle1,ax
        mov cs:bitplane,0

        mov bx,cs:handle1
        mov cx,2000             ;2000 bytes
        mov ah,3fh              ;read
        push cs
        pop ds
        mov dx,offset cabeza
        int 21h

        mov si,offset compare
        mov di,offset cabeza
        mov cx,15
        mov al,[si]
        cmp [di],al
        jne no_pic
        inc si
        inc di
        rep movsb
        jmp short pic_ok

no_pic:
        mov dx,offset pic_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

pic_ok:
        mov ah,0
        mov al,cs:videomode
        int 10h                 ;*** set video mode ***

        mov ah,5
        mov al,cs:displaypage
        int 10h                 ;*** set active display page ***

        mov ah,12h
        mov bl,32h
        mov al,0
        int 10h                 ;*** Enable direct CPU acces to VideoRam ***

        mov ah,1ch
        mov al,0
        mov cx,2
        int 10h                 ;*** Return restore status

        mov ah,1ch
        mov al,2
        mov cx,2
        mov bx,offset dac_colors
        int 10h                 ;*** Restore info about Dac colors ***

       ;mov ah,42h              ;file pointer at the end
       ;mov al,2
       ;mov bx,cs:handle1       ;bx=handle
       ;xor cx,cx               ;cx:dx=offset
       ;xor dx,dx
       ;int 21h                 ;*** move file pointer ***

read_again:
        mov cx,0ffffh           ;number of bytes
        mov ax,0a000h           ;ds:dx=buffer
        mov ds,ax
        xor dx,dx
        mov bx,cs:handle1       ;bx=handle
        mov ah,3fh
        int 21h                 ;*** read from file using handle ***

       ;mov ax,0a000h           ;es:di target
       ;mov es,ax
       ;xor di,di

       ;mov ax,cs:seg_mem       ;ds:si source
       ;mov ds,ax
       ;xor si,si
       ;mov cx,8000h           ;64k

       ;mov dx,3ceh             ;Address graphic controller
       ;mov ah,cs:bitplane      ;Number of Bitplane (0=first)
       ;mov al,4                ;Select Bitplane
       ;out dx,ax
       ;mov ax,0105h            ;Write mode 1/Read mode 0
       ;out dx,ax

       ;rep movsb               ;Copy 64k

       ;inc cs:bitplane
       ;cmp cs:bitplane,4
       ;jne read_again

        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        mov ah,8
        int 21h                 ;*** wait until a key is pressed ***

        mov al,cs:oldvideo      ;*** old video mode ***
        mov ah,0
        int 10h

        push cs
        pop ds
        mov dx,offset listo_msg
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 0 ***

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        push cs
        pop ds
        mov dx,offset drv_error_msg
        mov ah,9
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***


;************ data **********************;

;****** MESSAGES *****

help_msg        db 13,10,'Loadpic v1.00 por Ricardo Quesada.',13,10
                db 'Pantalla de ayuda.',13,10
db 13,10,'Tipee Loadpic /comando',13,10
db 'Comandos:',13,10
db '    /?            muestra esta pantalla.',13,10
db '    /n=Nombre     Donde "Nombre" es el archivo que tiene el grafico.',13,10
db '                  Se puede incluir un path completo.Es NECESARIO poner un',13,10
db '                  nombre.',13,10,13,10
db 'Ejemplo:',13,10
db '    savepic /n=c:\dibujos\dibujo.pic',13,10,13,10
db 'Esa linea de comando busca el archivo "dibujo.pic" en el directorio',13,10
db '"c:\dibujos\" .',13,10,13,10
db 'Recuerde que necesita por lo menos 70k de memoria RAM libre.',13,10,'$'

installing_msg  db 13,10,'Loadpic v1.00 por Ricardo Quesada.',13,10
                db 'Escriba Savepic /? para ayuda.',13,10,'$'

loading_msg     db 'Loadpic v1.00 por Ricardo Quesada.',13,10
                db 'Cargando grafico... Un momento por favor.',13,10,'$'

listo_msg       db 'Carga terminada.',13,10,'$'

mem_error_msg   db 13,10,'Memory error.',13,10
                db 'Necesita por lo menos 70k RAM de memoria libre.',13,10,'$'

drv_error_msg   db 13,10,'Drive error.',13,10
                db 'Verifique si el archivo que quiere cargar se encuentra en el directorio.',13,10,'$'

pic_error_msg   db 13,10,'Error.El archivo no fue grabado por Savepic.',13,10
                db 'Por favor verifique.',13,10,'$'

handle1         dw 0
seg_mem         dw 0
bitplane        db 0
oldvideo        db 0

nombre1         db 'picture.pic',0,'espacios dejados para poner otro nombre'

compare         db '2SavepicRQuesada'
cabeza          db '2Savepic'
autor           db 'RQuesada'
videomode       db 0
displaypage     db 0
dac_colors      db 1024 dup (0)

basura          db 1000 dup (1)

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

