;name: loadpic
;verion: 2.10
;start  date: 30/10/93
;finish date: 30/10/93
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13               ;es CR,si then display help
        je displayhelp

        cmp al,20h              ;es espacio,si then le de nuevo
        je alop

        jne newname             ;si es otra cosa es el nombre.Ve alli.

displayhelp:                    ;***************** no command line
        push cs
        pop ds
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***


newname:                        ;******************** command line Name
        lea bx,nombre1
buscanombre:
        mov cs:[bx],al
        cmp al,13
        je endname
        lodsb
        inc bx
        jmp short buscanombre

endname:
        mov cs:[bx],2400h       ;24h=$ 0=nul.Uso el $ para usar la func 9 int 21

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,0ea6h            ;59k    (m5b 800x600x16)
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        mov cs:seg_mem,ax


;********** drive user ***************;

        mov ah,0fh
        int 10h
        mov cs:oldvideo,al

        push cs
        pop ds
        mov dx,offset loading_msg  ;loading messagge
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,3d02h            ;open file with al=2 (W/R access)
        mov dx,offset nombre1
        int 21h
        jnc no_error

        jmp error               ;salta porque hubo error

no_error:

        mov cs:handle1,ax

        mov bx,cs:handle1
        mov cx,1000             ;1000 bytes
        mov ah,3fh              ;read
        push cs
        pop ds
        mov dx,offset cabeza
        int 21h

        push cs
        pop ds
        push cs
        pop es
        mov si,offset compare
        mov di,offset cabeza
        mov cx,16
        rep cmpsb
        je pic_ok

        mov dx,offset picver_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

pic_ok:
        mov si,offset compare_ver
        mov di,offset ver
        mov cx,4                ;5 si se chequea con centecimos 02.00
        rep cmpsb               ;4 si se chequea con decimos 02.0x
        je ver_ok

        mov dx,offset picver_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

ver_ok:

        mov ah,0
        mov al,cs:videomode
        int 10h                 ;*** set video mode ***

        push cs
        pop ds

        cmp al,5bh
        jae svga

        xor ah,ah
        shl al,1                ;multiplica x 2
        mov si,offset m00
        add si,ax
        mov ax,[si]
        mov number_bytes,ax     ;cantidad de bytes a grabar.
        jmp short common

svga:
        xor ah,ah
        sub al,5bh
        shl al,1
        mov si,offset m5b
        add si,ax
        mov ax,[si]
        mov number_bytes,ax

common:
        mov ah,5
        mov al,cs:displaypage
        int 10h                 ;*** set active display page ***

        mov ah,10h
        mov al,2
        push cs
        pop es
        mov dx,offset palette
        int 10h                 ;*** Set all palette registers ***

        mov ah,10h
        mov al,12h
        xor bx,bx
        mov cx,255
        push cs
        pop es
        mov dx,offset dac_color
        int 10h                 ;*** Load multiple DAC color registers ***

        mov ax,cs:number_bytes  ;De cuantos colores es?
        and al,16
        cmp al,16
        je mode16color          ;de 16
        mov ax,cs:number_bytes
        cmp al,0
        je mode256color         ;de 256
        and al,2
        cmp al,2
        je mode2color           ;de 2

mode256color:                   ;empieza lo 256 colores ********************

        mov cx,64000            ;number of bytes
        mov ax,0a000h           ;ds:dx=buffer
        mov ds,ax
        xor dx,dx
        mov bx,cs:handle1       ;bx=handle
        mov ah,3fh
        int 21h                 ;*** read from file using handle ***

        jmp listo               ;Se termino lo 256 colores


;***************    empieza lo dificil de codigo...
mode2color:
mode16color:
        mov ax,cs:number_bytes
        sub al,16
        mov cs:number_bytes,ax  ;resta 16 por los colores de chequeo


        mov cs:readers,1

        mov ax,0a000h           ;segment video memory
        mov es,ax
        xor di,di               ;offset video memory

        mov cs:bitmask,128      ;set initial bitmask (bit7=1)

read_again:
        mov cx,cs:number_bytes  ;
        mov ax,cs:seg_mem
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov bx,cs:handle1       ;handle
        mov ah,3Fh
        int 21h                 ;*** reade from file (handle) ***

        mov ax,cs:seg_mem       ;segment source code
        mov ds,ax
        xor si,si               ;offset source code

read_read:
        mov dx,3ceh             ;GC_INDEX
        mov al,8                ;Bit mask
        mov ah,cs:bitmask
        out dx,ax
        mov ax,0205h            ;read mode 0 ; write mode 2
        out dx,ax

        mov al,es:[di]          ;dummy read
        mov al,ds:[si]          ;read color
        mov bl,al               ;
        shr al,4                ;first hi nibble
        mov es:[di],al          ;set new color

        shr cs:bitmask,1        ;rotate bitmask one bit
        mov ah,cs:bitmask
        mov al,8
        out dx,ax               ;set new bitmask

        mov al,es:[di]          ;dummy read
        mov al,bl               ;
        mov es:[di],al          ;set new color for another pixel

        shr cs:bitmask,1        ;rotate bitmask one bit
        cmp cs:bitmask,0
        jne same_byte

        mov cs:bitmask,128
        inc di

same_byte:
        inc si
        cmp si,cs:number_bytes
        jne read_read

        inc cs:readers
        cmp cs:readers,5
        jne read_again

;****************** termino lo dificil de codigo...
listo:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        mov ah,8
        int 21h                 ;*** wait until a key is pressed ***

        mov al,cs:oldvideo      ;*** old video mode ***
        mov ah,0
        int 10h

        push cs
        pop ds
        mov dx,offset listo_msg
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 0 ***

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        push cs
        pop ds
        mov dx,offset drv_error_msg
        mov ah,9
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***


;************ data **********************;

m00     dw 0                    ;modos que soporta.
m01     dw 0
m02     dw 0
m03     dw 0
m04     dw 0
m05     dw 0
m06     dw 0
m07     dw 0
m08     dw 0
m09     dw 0
m0a     dw 0
m0b     dw 0
m0c     dw 0
m0d     dw 8000 or 16           ;320x200x16 color
m0e     dw 16000 or 16          ;640x200x16
m0f     dw 28000 or 2           ;640x350x2
m10     dw 28000 or 16          ;640x350x16
m11     dw 38400 or 2           ;640x480x2
m12     dw 38400 or 16          ;640x200x16
m13     dw 64000 or 0           ;320x200x256

m5b     dw 60000 or 16          ;800x600x16

number_bytes dw 0               ;cantidad de bytes a grabar depende del modo

;****** MESSAGES *****

help_msg db 13,10
db '                           *** Loadpic v2.10 ***',13,10
db '                            por Ricardo Quesada',13,10
db 13,10,'LOADPIC comando',13,10
db 'Comando:',13,10
db '    path\name  Busca en el "path" el archivo  "name" y muestra',13,10
db '                  el grafico en pantalla',13,10,13,10
db 'Ejemplo:',13,10
db '    loadpic c:\dibujos\paisaje',13,10,13,10
db 'Esa linea de comando busca el archivo "paisaje" en el directorio',13,10
db '"c:\dibujos\" y muestra el grafico.',13,10,13,10
db 'Recuerde que necesita por lo menos 64k de memoria RAM libre.',13,10,'$'

loading_msg     db 'Loadpic v2.10 por Ricardo Quesada.',13,10
                db 'Cargando grafico...   ','$'

listo_msg       db 'OK.',13,10,'$'

mem_error_msg   db 13,10,'Error.',13,10
db 'Necesita por lo menos 64k RAM de memoria libre.',13,10,'$'

drv_error_msg   db 13,10,'Error.',13,10
db 'El siguiente archivo no fue encontrdo.',13,10

nombre1    db '***** Io ti amo Alessia, Richi *****',13,10,'Curioso que miras'

picver_error_msg db 13,10,'Error.',13,10
db 'El archivo no fue grabado por Savepic v2.1x.',13,10,'$'

handle1         dw 0
seg_mem         dw 0
bitplane        db 0            ;number of bitplane you will write
bitmask         db 0            ;primero el primer bit.
oldvideo        db 0
readers         db 0

compare         db 'Savepic.RQuesada'
compare_ver     db '02.1x'      ;la 'x' esta demas.Only check '02.0'
cabeza          db 'Ricardo '   ;tiene que ir 'Savepic.'
autor           db 'Quesada '   ;tiene que ir 'RQuesada'
ver             db '1993 '      ;tiene que ir '02.1?' ?=varia segun version.
videomode       db 0
displaypage     db 0
palette         db 17 dup(0)
dac_color       db 256*3 dup(0)

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

