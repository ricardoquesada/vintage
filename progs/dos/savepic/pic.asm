;name: savepic
;verion: 1.00
;start  date: 11/10/93
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************ equ *************************;

key_mask equ 00000011b  ;Left & Right Shift


;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;***** Already installed ? *******

        mov ax,0d200h            ;Is Savepic installed?
        int 2fh
        cmp ax,00d2h
        jne not_installed       ;No, so install it.

        mov ah,9
        mov dx,offset already_ins_msg ;Yes, so do not install it again.
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***

not_installed:                  ;Install SavePic

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,1000h            ;64k
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        push cs
        pop ds
        mov seg_mem,ax

;******* GET ADDRESSES **********

        mov ah,34h              ;get address of INDOS flag
        int 21h
        mov si,offset indos
        mov [si],bx
        mov [si+2],es


        mov ax,3508h            ;get address of int 08
        int 21h
        mov si,offset old08
        mov [si],bx
        mov [si+2],es

        mov dx,offset int08
        mov ax,2508h
        int 21h


        mov ax,3509h            ;get address of int 09
        int 21h
        mov si,offset old09
        mov [si],bx
        mov [si+2],es

        mov dx,offset int09
        mov ax,2509h
        int 21h


        mov ax,3513h            ;get address of int 13
        int 21h
        mov si,offset old13
        mov [si],bx
        mov [si+2],es

        mov dx,offset int13
        mov ax,2513h
        int 21h


        mov ax,3528h            ;get address of int 28
        int 21h
        mov si,offset old28
        mov [si],bx
        mov [si+2],es

        mov dx,offset int28
        mov ax,2528h
        int 21h


        mov ax,352fh            ;get address of int 2f
        int 21h
        mov si,offset old2f
        mov [si],bx
        mov [si+2],es

        mov dx,offset int2f
        mov ax,252fh
        int 21h


        mov ah,9                ;print message of installation
        mov dx,offset installing_msg
        int 21h

        mov dx,offset final     ;TSR
        inc dx
        int 27h

;******** RUTINES *****************************************;

;******* DOSACTIVE *******;

;Output: Zero flag=1 Dos may be interrupted.

dosactive proc near

        push ds
        push bx
        lds bx,indos            ;ds:bx point to the INDOS flag
        cmp byte ptr [bx],0     ;DOS function active?
        pop bx
        pop ds

        ret

dosactive endp


;******** INTERRUPS ****************************************;

;******** int 08 **********;

int08:

        cmp cs:tsrnow,0         ;si es 0 TIME OUT.
        je i8_end               ;volver a intentar

        dec cs:tsrnow           ;decrementa para poder activar.

        cmp cs:in_bios,0        ;se esta usando la int 13
        jne i8_end              ;si =0 no, si <>0 si

        call dosactive
        je i8_tsr

i8_end:
        jmp cs:[old08]

i8_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old08]
        call start_tsr
        iret


;********* int 09 *********;

int09:

        push ax

        cmp cs:tsractive,0      ;ya esta activo
        jne i9_end              ;si then terminar

        cmp cs:tsrnow,0         ;waiting for activation
        jne i9_end              ;yes so terminar

        push ds                 ;lee si se pulsa el HOTKEY
        mov ax,040h
        mov ds,ax
        mov al,byte ptr ds:[17h]
        and al,key_mask
        cmp al,key_mask
        pop ds
        jne i9_end              ;No se pulsa entonces nada.

        cmp cs:in_bios,0        ;Se pulsa pero int 13 usandose
        jne i9_e1               ;Then poner tiempo de espera

        call dosactive          ;No se esta usando
        je i9_tsr               ;Pero el INDOS lo permite.Si then TSR

i9_e1:
        mov cs:tsrnow,9         ;9/18.2 = 0.5 segundos de espera.

i9_end:
        pop ax
        jmp cs:[old09]

i9_tsr:
        mov cs:tsractive,1
        mov cs:tsrnow,0
        pushf
        call cs:[old09]
        pop ax
        call start_tsr
        iret


;********* int 2f *********;

int2f:
        cmp ax,0d200h
        jne no_d200
        xchg al,ah
        iret

no_d200:
        jmp cs:[old2f]


;********** int 13 *********;

int13:

        inc cs:in_bios

        pushf
        call cs:[old13]

        dec cs:in_bios

        sti
        ret 2


;********* int 28 **************;

int28:
        cmp cs:tsrnow,0         ;tsrnow es el tiempo que tiene
        je i28_end              ;para empezar a ejecutarce.
                                ;Si es 0 se ejecuta.

        cmp cs:in_bios,0        ;in_bios es el flag de la int 13
        je i28_tsr              ;si es 0 se puede ejecutar.

i28_end:
        jmp cs:[old28]

i28_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old28]
        call start_tsr
        iret

;********** START PROGRAMA RESIDENTE *************;

start_tsr proc near

        cli
        mov cs:xx_ss,ss
        mov cs:xx_sp,sp

        mov sp,offset stacks_end
        mov ax,cs
        mov ss,ax

        sti

        push ax
        push bx
        push cx
        push dx
        push ds
        push es
        push si
        push di


        mov ah,0fh
        int 10h                 ;*** get video mode ***
        push cs
        pop ds
        mov cs:videomode,al

        mov ah,0
        mov al,12h
        int 10h

        xor dx,dx
        xor al,al
volver2:
        xor cx,cx
volver:
        mov ah,0ch
        mov bh,0
        int 10h                 ;*** write pixel ***

        inc al
        inc cx
        cmp cx,200
        jne volver
        inc dx
        cmp dx,200
        jne volver2

        mov cs:bitplane,0

volver3:

        mov ax,0a000h           ;ds:si source
        mov ds,ax
        xor si,si
        mov ax,cs:seg_mem
        mov es,ax
        xor di,di
        mov cx,0ffffh           ;64k

        mov dx,3ceh             ;Address graphic controller
        mov ah,cs:bitplane      ;Number of Bitplane (0=first)
        mov al,4                ;Select Bitplane
        out dx,ax
        mov ax,0105h            ;Write mode 1/Read mode 0
        out dx,ax

        rep movsb               ;Copy 8k


        push cs
        pop ds

        cmp cs:bitplane,0
        jne no_truncate

        mov ah,3ch
        xor cx,cx               ;File attribute
        mov dx,offset nombre1
        int 21h                 ;*** create or truncate a file ***

        jc error               ;Si hubo error.

        mov cs:handle1,ax

no_truncate:

        mov ah,42h
        mov al,2
        xor dx,dx
        xor cx,cx
        mov bx,cs:handle1
        int 21h                 ;*** move file pointer to end of file ***

        mov cx,0ffffh           ;64k se escriben
        mov ax,cs:seg_mem
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        inc cs:bitplane
        cmp cs:bitplane,4
        jne volver3

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov al,cs:videomode
        mov ah,00h
        int 10h                 ;*** set video mode ***

        pop di
        pop si
        pop es
        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        cli

        mov ss,cs:xx_ss
        mov sp,cs:xx_sp

        mov cs:tsractive,0
        sti

        ret

start_tsr endp

;************ data **********************;

xx_ss   dw 0
xx_sp   dw 0

tsrnow  db 0
tsractive db 0
in_bios db 0

dta_off dw 0
dta_seg dw 0

indos equ this dword
indos_off dw 0
indos_seg dw 0

old08 equ this dword
off08 dw 0
seg08 dw 0

old09 equ this dword
off09 dw 0
seg09 dw 0

old13 equ this dword
off13 dw 0
seg13 dw 0

old28 equ this dword
off28 dw 0
seg28 dw 0

old2f equ this dword
off2f dw 0
seg2f dw 0

;****** MESSAGES *****
already_ins_msg db 13,10,'Savepic ya esta residente.',13,10,'$'

installing_msg  db 13,10,'Savepic v1.00 por Ricardo Quesada.',13,10
                db 'Escriba Savepic /? para mas ayuda.',13,10
                db 'Hotkey = Shift+Shift',13,10,'$'

mem_error_msg   db 13,10,'Memory error.',13,10
                db 'Necesita por lo menos 64k libres.',13,10,'$'

prueba_msg      db 'Usted acaba de apretar el hot key',13,10,'$'

nombre1         db 'picture.pic',0
videomode       db 0
handle1         dw 0
seg_mem         dw 0
bitplane        db 0

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

