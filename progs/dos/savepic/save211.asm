;name: Savepic
;verion: 2.11
;start  date: 4/11/93
;finish date: 4/11/93
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************ equ *************************;

key_mask equ 00000011b  ;Left & Right Shift
verhi   equ '20'        ;02 Hi number of version
verlo   equ '11'        ;11 Lo number of version

;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        je displayhelp
        cmp al,20h              ;es espacio, si then seguir buscando
        je alop
        cmp al,'/'
        je command_line         ;loop until '/' or CR is found
        jmp newname             ;si no es nada de lo anterior then es name

command_line:
        lodsb
        and al,11011111b
        cmp al,'U'
        jne displayhelp         ;si no es alguna reconocido go displayhelp
        jmp uninstall           ;es 'u' then goto uninstall

displayhelp:                    ;***************** no command line
        push cs
        pop ds

        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,0d200h
        int 2fh
        cmp ax,00d2h
        jne no_inst             ;detecta si esta instalado.

        mov ax,0d202h           ;detect version number
        int 2fh
        cmp ax,02d2h            ;habilitada esta funcion
        jne not_supported_version

        mov si,offset numberh_msg
        mov [si],bx
        mov [si+3],cx

        push cs
        pop ds
        mov dx,offset version_msg
        mov ah,9
        int 21h

        cmp bx,verhi            ;version not 2.11 then don't display path
        jne no_disp_path        ;dont print path
        cmp cx,verlo
        jne no_disp_path

        mov dx,offset path_msg
        mov ah,9
        int 21h                 ;display path only if v2.11 is loaded.

        mov ax,0d201h
        int 2fh
        push es
        pop ds
        mov dx,offset nombre1
        mov ah,9
        int 21h                 ;display path name

        mov dl,13
        mov ah,6
        int 21h                 ;output CR

        mov dl,10
        mov ah,6
        int 21h                 ;output LF

no_disp_path:
        jmp short fin_fin

not_supported_version:
        mov dx,offset no_su_ver_msg
        mov ah,9
        int 21h
        jmp short fin_fin

no_inst:
        mov dx,offset status_u_msg
        mov ah,9
        int 21h

fin_fin:                        ;mentira.A partir de aca detecta el Vesa.
        mov ah,4fh              ;VESA
        mov al,00               ;
        push cs
        pop es
        mov di,offset vesa_buffer
        int 10h

        push cs
        pop ds

        cmp ax,004fh            ;hi-lo inverted.cmp?ok,yes vesa supported.
        jne vesa_not

        mov dx,offset vesa_i_msg        ;existe VESA.display version
        mov al,vesa_hi
        add al,30h
        mov vesa_ver_msg,al
        mov al,vesa_lo
        add al,30h
        mov vesa_ver_msg+2,al

        mov ah,9
        int 21h

        mov dx,offset vesa_fab_msg
        mov ah,9
        int 21h                         ;display 'FABRICANTE:

        mov si,vesa_off                 ;vesa.display manufacturer
        mov ax,vesa_seg
        mov ds,ax
de_vuelta_output:
        mov dl,[si]
        cmp dl,0
        je fin_de_output
        mov ah,6
        int 21h
        inc si
        jmp short de_vuelta_output

fin_de_output:
        jmp short iralfin

vesa_not:                       ;vesa not supported message
        mov dx,offset vesa_u_msg
        mov ah,9
        int 21h                 ;output message to screen

iralfin:
        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***

uninstall:                      ;*********************command line U
        mov ax,0d200h
        int 2fh
        cmp ax,00d2h
        jne error_inst2

        mov ax,0d202h
        int 2fh                 ;ve si es la v2.11
        cmp bx,verhi
        jne error_inst2
        cmp cx,verlo
        jne error_inst2         ;no es.no desinstala

        mov ax,3508h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3509h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3513h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3528h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,352fh
        int 21h
        cmp es:[bx+2],'ps'
        je desinstalar

error_inst:
        push cs
        pop ds
        mov dx,offset error_isnt_msg2
        mov ah,9
        int 21h

        mov ax,4cffh
        int 21h

error_inst2:
        push cs
        pop ds
        mov dx,offset error_inst_msg1
        mov ah,9
        int 21h

        mov ax,4cfeh
        int 21h

desinstalar:                            ;*** Rutina de desinstalacion ***
        cli
        mov dx,es:off08
        mov ds,es:seg08
        mov ax,2508h
        int 21h                         ;*** restaurar vector original

        mov ax,2509h
        mov dx,es:off09
        mov ds,es:seg09
        int 21h

        mov ax,2513h
        mov dx,es:off13
        mov ds,es:seg13
        int 21h

        mov ax,2528h
        mov dx,es:off28
        mov ds,es:seg28
        int 21h

        mov ax,252fh
        mov dx,es:off2f
        mov ds,es:seg2f
        int 21h
        sti

        mov bx,es
        mov es,es:[2ch]
        mov ah,49h
        int 21h
        mov es,bx
        mov ah,49h
        int 21h                         ;Release memory

        mov ah,49h
        mov es,bx
        mov es,es:seg_mem
        int 21h

        push cs
        pop ds
        mov dx,offset uninstall_msg
        mov ah,9
        int 21h

        mov ax,4c00h
        int 21h

newname:                        ;******************** command line paht/name
        lea bx,nombre1
buscanombre:
        mov cs:[bx],al
        cmp al,13
        je endname
        lodsb
        inc bx
        jmp short buscanombre

endname:
        mov cs:[bx],2400h       ;24h=$ 0=nul/ $ usado para func9 del int21

;***** Already installed ? *******

installed:
        mov ax,0d200h            ;Is Savepic installed?
        int 2fh
        cmp ax,00d2h
        jne instalar_            ;Si esta instaldo then change name only.

        mov ax,0d202h
        int 2fh
        cmp bx,verhi
        jne no_chg_name
        cmp cx,verlo
        je si_chg_name          ;si no es la version 2.11 no se puede chg_name

no_chg_name:
        mov dx,offset name_error_msg
        mov ah,9
        int 21h                 ;Mensaje de error si no es la v2.10

        mov ax,4cffh
        int 21h


si_chg_name:
        mov ah,9
        mov dx,offset nombre_chg_msg ;mensaje de cambiando nombre
        int 21h                 ;*** output message ***

        jmp short chg_name

instalar_:

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,0ea6h            ;59k que es lo que necesita el modo mas grande
        int 21h                 ;*** allocate memory ***   (800x600x16)
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        push cs
        pop ds
        mov seg_mem,ax

chg_name:

        mov ah,3ch
        xor cx,cx               ;file attribute
        push cs
        pop ds                  ;ds:dx=nombre1
        mov dx,offset nombre1
        int 21h                 ;*** create or truncate a file ***
        jnc todo_ok

        mov ah,9
        mov dx,offset path_error_msg
        int 21h                 ;*** display error msg ***

        mov ax,4cffh
        int 21h                 ;*** terminate with error ***

todo_ok:
        mov bx,ax
        mov ah,3eh
        int 21h                 ;*** close handle ***

        mov ax,0d200h           ;Este nuevo control de instalacion se hace
        int 2fh                 ;para ver si luego de cambiado el nombre
        cmp ax,00d2h            ;tiene que seguir a instalacion
        jne not_installed_continue   ;si esta instalado se termina
                                     ;si no sigue con la instalacion
        ;porque aca se llama cuando se cambia el nombre para verificar el path

        mov dx,offset satisfacto_msg
        mov ah,9
        int 21h                 ;display 'OK' luego de cambiado el nombre

        mov ax,0d201h           ;*** Esta rutina es la que cambia el nombre  ***
        int 2fh                 ;en ES vuelve el segmento donde esta residente

        lea bx,nombre1

no_termino_nombre:
        mov al,cs:[bx]
        mov es:[bx],al
        inc bx
        cmp al,'$'              ;simbolo de que termino el nombre
        jne no_termino_nombre

        mov ax,4c00h            ;termina satisfactoriamente el cambio
        int 21h                 ;de nombre


not_installed_continue:

;******* GET ADDRESSES **********

        mov ah,34h              ;get address of INDOS flag
        int 21h
        mov si,offset indos
        mov [si],bx
        mov [si+2],es


        mov ax,3508h            ;get address of int 08
        int 21h
        mov si,offset old08
        mov [si],bx
        mov [si+2],es

        mov dx,offset int08
        mov ax,2508h
        int 21h


        mov ax,3509h            ;get address of int 09
        int 21h
        mov si,offset old09
        mov [si],bx
        mov [si+2],es

        mov dx,offset int09
        mov ax,2509h
        int 21h


        mov ax,3513h            ;get address of int 13
        int 21h
        mov si,offset old13
        mov [si],bx
        mov [si+2],es

        mov dx,offset int13
        mov ax,2513h
        int 21h


        mov ax,3528h            ;get address of int 28
        int 21h
        mov si,offset old28
        mov [si],bx
        mov [si+2],es

        mov dx,offset int28
        mov ax,2528h
        int 21h


        mov ax,352fh            ;get address of int 2f
        int 21h
        mov si,offset old2f
        mov [si],bx
        mov [si+2],es

        mov dx,offset int2f
        mov ax,252fh
        int 21h


        mov ah,9                ;print message of installation
        mov dx,offset installing_msg
        int 21h

        mov dx,offset final     ;TSR
        inc dx
        int 27h

;******** RUTINES *****************************************;

;******* DOSACTIVE *******;

;Output: Zero flag=1 Dos may be interrupted.

dosactive proc near

        push ds
        push bx
        lds bx,indos            ;ds:bx point to the INDOS flag
        cmp byte ptr [bx],0     ;DOS function active?
        pop bx
        pop ds

        ret

dosactive endp


;******** INTERRUPS ****************************************;

;******** int 08 **********;

int08:
        jmp short cont08
        db 'sp'
cont08:

        dec cs:timetime

        cmp cs:beep_act,0       ;detecta si se tiene que activar el beep
        je continue_08
        call beep


continue_08:
        cmp cs:tsrnow,0         ;si es 0 TIME OUT.
        je i8_end               ;volver a intentar

        dec cs:tsrnow           ;decrementa para poder activar.

        cmp cs:in_bios,0        ;se esta usando la int 13
        jne i8_end              ;si =0 no, si <>0 si

        call dosactive
        je i8_tsr

i8_end:
        jmp cs:[old08]

i8_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old08]
        call start_tsr
        iret


;********* int 09 *********;

int09:
        jmp short cont09
        db 'sp'
cont09:
        push ax

        cmp cs:tsractive,0      ;ya esta activo
        jne i9_end              ;si then terminar

        cmp cs:tsrnow,0         ;waiting for activation
        jne i9_end              ;yes so terminar

        push ds                 ;lee si se pulsa el HOTKEY
        mov ax,040h
        mov ds,ax
        mov al,byte ptr ds:[17h]
        and al,key_mask
        cmp al,key_mask
        pop ds
        jne i9_end              ;No se pulsa entonces nada.

        cmp cs:in_bios,0        ;Se pulsa pero int 13 usandose
        jne i9_e1               ;Then poner tiempo de espera

        call dosactive          ;No se esta usando
        je i9_tsr               ;Pero el INDOS lo permite.Si then TSR

i9_e1:
        mov cs:tsrnow,9         ;9/18.2 = 0.5 segundos de espera.

i9_end:
        pop ax
        jmp cs:[old09]

i9_tsr:
        mov cs:tsractive,1
        mov cs:tsrnow,0
        pushf
        call cs:[old09]
        pop ax
        call start_tsr
        iret


;********* int 2f *********;

int2f:
        jmp short cont2f
        db 'sp'
cont2f:
        cmp ax,0d200h
        jne no_d200
        xchg al,ah
        iret

no_d200:
        cmp ax,0d201h
        jne no_d201
        push cs
        pop es
        iret

no_d201:
        cmp ax,0d202h
        jne no_d202
        xchg al,ah              ;soporta la opcion de la version
        mov bx,verhi
        mov cx,verlo
        iret

no_d202:
        jmp cs:[old2f]


;********** int 13 *********;

int13:
        jmp short cont13
        db 'sp'
cont13:
        inc cs:in_bios

        pushf
        call cs:[old13]

        dec cs:in_bios

        sti
        ret 2


;********* int 28 **************;

int28:
        jmp short cont28
        db 'sp'
cont28:
        cmp cs:tsrnow,0         ;tsrnow es el tiempo que tiene
        je i28_end              ;para empezar a ejecutarce.
                                ;Si es 0 se ejecuta.

        cmp cs:in_bios,0        ;in_bios es el flag de la int 13
        je i28_tsr              ;si es 0 se puede ejecutar.

i28_end:
        jmp cs:[old28]

i28_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old28]
        call start_tsr
        iret

;********** START PROGRAMA RESIDENTE *************;

start_tsr proc near

        cli
        mov cs:xx_ss,ss
        mov cs:xx_sp,sp

        mov sp,offset stacks_end
        mov ax,cs
        mov ss,ax

        sti

        push ax
        push bx
        push cx
        push dx
        push ds
        push es
        push si
        push di

        mov ah,4fh              ;Primero.Probar si es vesa
        mov al,3
        int 10h                 ;Is Vesa installed?
        cmp ah,4fh              ;ah=4f vesa installed and bx=videomode
        jne vesa_not_inst
        mov cs:video_mode,bx
        and bh,1                ;bh_bit0(bx_bit8)=1
        je vesa_mode



vesa_mode:
        xor bh,bh
        shl bl,1                ;multiplica x 2
        mov si,offset v100
semetesvga:
        add si,bx
        mov ax,[si]
        cmp ax,0
        jne esvesamode
        jmp error2
esvesamode:
        mov number_bytes,ax   ;cantidad de bytes a grabar.



vesa_not_inst:
        mov ah,0fh
        int 10h                 ;*** get video mode ***
        push cs
        pop ds
        xor ah,ah
        mov cs:videomode,ax     ;al=videomode
        mov cs:displaypage,bh

        cmp al,13h              ;es mayor que 13h
        jbe check_again
        jmp error2              ;modo no soportado

check_again:
        xor ah,ah
        shl al,1                ;multiplica x 2
        mov si,offset m00
semetesvga:
        add si,ax
        mov ax,[si]
        cmp ax,0
        jne check_again2
jmp_error2:
        jmp error2
check_again2:
        mov number_bytes,ax   ;cantidad de bytes a grabar.

mode_supp:                      ;Mode supported

        mov ah,10h
        mov al,9
        push cs
        pop es
        mov dx,offset palette   ;es:dx=buffer
        int 10h                 ;*** Read contents of palette and overscan ***

        mov ah,10h              ;func number
        mov al,17h              ;sub-func number
        push cs                 ;es:dx=address of buffer
        pop es
        mov dx,offset dac_color
        mov cx,255              ;number of regs to be loaded
        xor bx,bx               ;number of the 1st DAC color to be loaded
        int 10h                 ;*** Load contents of multiple DAC color reg ***

        mov ah,3ch
        xor cx,cx               ;File attribute
        push cs
        pop ds                  ;ds:dx=nombre1
        mov dx,offset nombre1
        int 21h                 ;*** create or truncate a file ***
        jnc no_error_1
        jmp error

no_error_1:
        mov cs:handle1,ax

        mov bx,cs:handle1
        mov cx,1000             ;1000 bytes de cabecera
        mov dx,offset cabeza
        push cs
        pop ds                  ;ds:dx=Source code
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        jnc no_error_2
        jmp error

no_error_2:
        mov ax,cs:number_bytes  ;De cuantos colores es?
        cmp al,0                ;0=256 color
        je  mode256color
        cmp al,2                ;bit 2=1 2colormode
        and al,2                ;de 2 colores
        je mode2color
        mov ax,cs:number_bytes  ;
        and al,16
        cmp al,16               ;bit 5=1 16colormode
        je mode16color          ;de 16 colores
        jmp error

mode256color:                   ;**********Aca empieza lo referente a 256 col

        mov ah,42h
        mov al,2                ;mode 2=to the end of file
        xor dx,dx
        xor cx,cx
        mov bx,cs:handle1
        int 21h                 ;*** move file pointer to end of file ***

        mov cx,cs:number_bytes  ;depende del mode
        mov ax,0a000h           ;escribe directamente de la video ram
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        jmp beep_

mode2color:                     ;usa la misma rutina.Por compatibilidad y fiaca
mode16color:                    ;**************Aca empieza lo referente a 16 col
        mov ax,cs:number_bytes          ;esto lo que hace es sacar el '16'
        sub al,16                       ;que indica cuantos colores tiene.
        mov cs:number_bytes,ax          ;se lo saca xor al,al el ax.

        mov cs:leidas,1

        mov ax,0a000h           ;es:di=seg:off video mem
        mov es,ax
        xor di,di

tutti_again:
        mov ax,cs:seg_mem
        mov ds,ax
        xor si,si

        mov dx,3ceh             ;gc_index
        mov ax,0005h            ;read write mode=0
        out dx,ax
        mov ch,128              ;ch=bitmask

repetir_1:
        xor bl,bl

        mov dx,3ceh
        mov al,4                ;select read bitplane
        mov ah,3                ;ah=bitplane to read

same_bitplane:
        out dx,ax

        mov bh,es:[di]          ;es:di seg:off video memory
        and bh,ch               ;solo el bit requerido
        neg bh                  ;set to bit 7 el bit si es que hay
        rol bx,1

        sub ah,1                ;ah =readmap
        jnc same_bitplane

        shr ch,1                ;shift right bitmask one bit
        mov ah,3

same_bitplane2:
        out dx,ax

        mov bh,es:[di]
        and bh,ch
        neg bh
        rol bx,1

        sub ah,1
        jnc same_bitplane2

        mov ds:[si],bl          ;escribe el color de los 2 primeros pixels
        inc si

        shr ch,1
        jnc aqui_aqui
        mov ch,128
        inc di

aqui_aqui:
        mov ah,3
        cmp si,cs:number_bytes
        je  salvar
        jmp repetir_1

salvar:
        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,42h
        mov al,2                ;mode 2=to the end of file
        xor dx,dx
        xor cx,cx
        mov bx,cs:handle1
        int 21h                 ;*** move file pointer to end of file ***

        mov cx,cs:number_bytes  ;depende del mode
        mov ax,cs:seg_mem
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        inc cs:leidas
        cmp cs:leidas,5
        je beep_                ;error es igual a seguir
        jmp tutti_again

beep_:
        mov cs:nota,0f00h       ;se manda la nota
        mov cs:beep_act,1       ;Todo OK 1 beep + beep on error (2beeps)

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

error2:
        mov cs:nota,0909h       ;se manda la nota y se activa el sonido.
        mov cs:beep_act,1

        pop di
        pop si
        pop es
        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        cli

        mov ss,cs:xx_ss
        mov sp,cs:xx_sp

        mov cs:tsractive,0
        sti

        ret

start_tsr endp

;********* Subroutines ************
beep    proc                    ;en CX number of times

        cmp cs:beep_act,1
        jne check_time

        mov al,182
        out 43h,al
        mov ax,cs:nota          ;bx valor externo.es la nota
        out 42h,al
        mov al,ah
        out 42h,al

        in al,61h
        or al,11b
        out 61h,al

        mov cs:timetime,4       ;18=1 segundo

check_time:
        cmp cs:timetime,0       ;paso el segundo
        jne return_

        in al,16h
        and al,11111100b
        out 61h,al

        mov cs:beep_act,0       ;disable calls to beep

        ret

return_:
        mov cs:beep_act,2       ;ya esta act. Solo chequear tiempo
        ret

beep    endp

;************ data **********************;

xx_ss   dw 0
xx_sp   dw 0

tsrnow  db 0
tsractive db 0
in_bios db 0

dta_off dw 0
dta_seg dw 0

indos equ this dword
indos_off dw 0
indos_seg dw 0

old08 equ this dword
off08 dw 0
seg08 dw 0

old09 equ this dword
off09 dw 0
seg09 dw 0

old13 equ this dword
off13 dw 0
seg13 dw 0

old28 equ this dword
off28 dw 0
seg28 dw 0

old2f equ this dword
off2f dw 0
seg2f dw 0

;****** TABLES ******

m00     dw 0                    ;hola como te va
m01     dw 0                    ;fmt: hi_lo
m02     dw 0                    ;hi_lo-lo=number of bytes required.
m03     dw 0                    ;lo=colors required.
m04     dw 0
m05     dw 0
m06     dw 0
m07     dw 0
m08     dw 0
m09     dw 0
m0a     dw 0
m0b     dw 0
m0c     dw 0
m0d     dw 8000 or 16           ;320x200x16 color
m0e     dw 16000 or 16          ;640x200x16
m0f     dw 28000 or 2           ;640x350x2
m10     dw 28000 or 16          ;640x350x16
m11     dw 38400 or 2           ;640x480x2
m12     dw 38400 or 16          ;640x200x16
m13     dw 64000 or 0           ;320x200x256

v100    dw 0                    ;640x400x256
v101    dw 0                    ;640x480x256
v102    dw 60000 or 16          ;800x600x16
v103    dw 0                    ;800x600x256
v104    dw 0                    ;1024x768x16
v105    dw 0                    ;1024x768x256
v106    dw 0                    ;1280x1024x16
v107    dw 0                    ;1280x1024x256

number_bytes dw 0               ;cantidad de bytes a grabar depende del modo
;**** more date *****

handle1         dw 0
seg_mem         dw 0
leidas          db 0
beep_act        db 0            ;usada por int08 y beep.
timetime        db 0
vesa_act        db 0            ;1 vesa installed.0 not supported.
nota            dw 0            ;valor de la nota.

;****** MESSAGES *****
error_inst_msg1 db 'Error.',13,10
db 'La v2.11 no esta instalada. Por lo tanto no se puede desinstalar.',13,10,'$'

error_isnt_msg2 db 'Error.',13,10
db 'Trate de desinstalar otros TSR para poder desinstalar Savepic.',13,10,'$'

mem_error_msg   db 13,10,'Error.',13,10
db 'Necesita por lo menos 64k RAM de memoria libre.',13,10,'$'

path_error_msg  db 13,10,'Error.',13,10
db 'El siguiente path y nombre no son correctos.Por favor verifique.',13,10

nombre1         db '******* Alessia, I will always love you - Richi *********'

name_error_msg  db 13,10,'Error.',13,10
db 'La v2.11 no esta instalada. No se puede cambiar el nombre a otra.',13,10,'$'

help_msg equ this byte
db '                          *** Savepic v2.11 ***',13,10
db '                           por Ricardo Quesada',13,10,13,10
db 'SAVEPIC /comando',13,10
db 'Comandos:',13,10
db '    /u            Desinstala Savepic.',13,10
db '    path\name     Deja residente Savepic para que se pueda grabar el grafico',13,10
db '                  en el "path" con el "name".',13,10,13,10
db 'Ejemplo:',13,10
db '    savepic c:\dibujos\paisaje',13,10,13,10
db 'Esa linea de comando deja residente a Savepic y cuando se pulse el Hotkey',13,10
db 'el dibujo se grabara en "c:\dibujos\" como "paisaje".',13,10,13,10
db 'HotKey = Shift izquiero + Shift derecho.',13,10
db 'Recuerde que necesita por lo menos 64k RAM de memoria y entre',13,10
db '32k y 236k en disco por cada archivo que se grabe.',13,10,13,10,'$'


no_su_ver_msg   db'Hay una version de Savepic instalada menor que la v2.07.',13,10,'$'

status_u_msg    db'SAVEPIC: no esta instalado.',13,10,'$'
version_msg     db'SAVEPIC VERSION: '
numberh_msg     db'??.??',13,10,'$'
path_msg        db'PATH: $'

vesa_i_msg      db'VESA VERSION: '
vesa_ver_msg    db'0.0',13,10,'$'
vesa_u_msg      db'VESA: No esta disponible.',13,10,'$'
vesa_fab_msg    db'FABRICANTE: $'

installing_msg  db 13,10,'Savepic v2.11 por Ricardo Quesada.',13,10
                db 'Instalado.',13,10
                db 'HotKey = Shift izquierdo + Shift derecho.',13,10,'$'

uninstall_msg   db 13,10,'Savepic v2.11 por Ricardo Quesada.',13,10
                db 'Desinstalado.',13,10,'$'

nombre_chg_msg db 'Cambiando el nombre...   ','$'
satisfacto_msg db 'OK.',13,10,'$'

cabeza          db 'Savepic.'
autor           db 'RQuesada'
ver             db '02.11'
videomode       dw 0                    ;bit8=1 vesa mode.bit8=0 normal mode
displaypage     db 0
palette         db 17 dup (0)
dac_color       db 256*3 dup(0)

vesa_buffer     db 4 dup(0)             ;'VESA'.Empieza el buffer de VESA.
vesa_hi         db 0                    ;version hi number
vesa_lo         db 0                    ;        lo number
vesa_manufac    equ this dword          ;far pointer to string(manufacturer name)
vesa_off        dw 0
vesa_seg        dw 0
vesa_resta      db 246 dup (0)          ;lo que resta nomas.Total 256 bytes

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti
