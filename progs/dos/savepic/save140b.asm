;name: savepic
;verion: 1.40b
;start  date: 20/10/93
;finish date: 20/10/93
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************ equ *************************;

key_mask equ 00000011b  ;Left & Right Shift


;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        je displayhelp
seguir_:
        cmp al,'/'
        jne alop                ;loop until '/' or CR is found

        lodsb
        and al,11011111b
        cmp al,'N'
        jne seguir
        jmp newname
seguir:
        cmp al,'U'
        je uninstall

displayhelp:                    ;*****************command line H
        push cs
        pop ds
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***

uninstall:                      ;*********************command line U
        mov ax,0d200h
        int 2fh
        cmp ax,00d2h
        jne error_inst2

        mov ax,3508h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3509h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3513h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3528h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,352fh
        int 21h
        cmp es:[bx+2],'ps'
        je desinstalar

error_inst:
        push cs
        pop ds
        mov dx,offset error_isnt_msg2
        mov ah,9
        int 21h

        mov ax,4cffh
        int 21h

error_inst2:
        push cs
        pop ds
        mov dx,offset error_inst_msg1
        mov ah,9
        int 21h

        mov ax,4cfeh
        int 21h

desinstalar:                            ;*** Rutina de desinstalacion ***
        cli
        mov dx,es:off08
        mov ds,es:seg08
        mov ax,2508h
        int 21h                         ;*** restaurar vector original

        mov ax,2509h
        mov dx,es:off09
        mov ds,es:seg09
        int 21h

        mov ax,2513h
        mov dx,es:off13
        mov ds,es:seg13
        int 21h

        mov ax,2528h
        mov dx,es:off28
        mov ds,es:seg28
        int 21h

        mov ax,252fh
        mov dx,es:off2f
        mov ds,es:seg2f
        int 21h
        sti

        mov bx,es
        mov es,es:[2ch]
        mov ah,49h
        int 21h
        mov es,bx
        mov ah,49h
        int 21h                         ;Release memory

        mov ah,49h
        mov es,bx
        mov es,es:seg_mem
        int 21h

        push cs
        pop ds
        mov dx,offset uninstall_msg
        mov ah,9
        int 21h

        mov ax,4c00h
        int 21h

newname:                        ;******************** command line N
        lea bx,nombre1
        lodsb
buscanombre:
        lodsb
        mov ds:[bx],al
        cmp al,13
        je endname
        inc bx
        mov ds:[bx],al
        jmp short buscanombre

endname:
        mov byte ptr ds:[bx],0

;***** Already installed ? *******

installed:
        mov ax,0d200h            ;Is Savepic installed?
        int 2fh
        cmp ax,00d2h
        jne not_installed       ;No, so install it.

        mov ah,9
        mov dx,offset already_ins_msg ;Yes, so do not install it again.
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***

not_installed:                  ;Install SavePic

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,1000h            ;64k
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        push cs
        pop ds
        mov seg_mem,ax

        mov ah,3ch
        xor cx,cx               ;file attribute
        push cs
        pop ds                  ;ds:dx=nombre1
        mov dx,offset nombre1
        int 21h                 ;*** create or truncate a file ***
        jnc todo_ok

        mov ah,9
        mov dx,offset path_error_msg
        int 21h                 ;*** display error msg ***

        mov ax,4cffh
        int 21h                 ;*** terminate with error ***

todo_ok:
        mov bx,ax
        mov ah,3eh
        int 21h                 ;*** close handle ***

;******* GET ADDRESSES **********

        mov ah,34h              ;get address of INDOS flag
        int 21h
        mov si,offset indos
        mov [si],bx
        mov [si+2],es


        mov ax,3508h            ;get address of int 08
        int 21h
        mov si,offset old08
        mov [si],bx
        mov [si+2],es

        mov dx,offset int08
        mov ax,2508h
        int 21h


        mov ax,3509h            ;get address of int 09
        int 21h
        mov si,offset old09
        mov [si],bx
        mov [si+2],es

        mov dx,offset int09
        mov ax,2509h
        int 21h


        mov ax,3513h            ;get address of int 13
        int 21h
        mov si,offset old13
        mov [si],bx
        mov [si+2],es

        mov dx,offset int13
        mov ax,2513h
        int 21h


        mov ax,3528h            ;get address of int 28
        int 21h
        mov si,offset old28
        mov [si],bx
        mov [si+2],es

        mov dx,offset int28
        mov ax,2528h
        int 21h


        mov ax,352fh            ;get address of int 2f
        int 21h
        mov si,offset old2f
        mov [si],bx
        mov [si+2],es

        mov dx,offset int2f
        mov ax,252fh
        int 21h


        mov ah,9                ;print message of installation
        mov dx,offset installing_msg
        int 21h

        mov dx,offset final     ;TSR
        inc dx
        int 27h

;******** RUTINES *****************************************;

;******* DOSACTIVE *******;

;Output: Zero flag=1 Dos may be interrupted.

dosactive proc near

        push ds
        push bx
        lds bx,indos            ;ds:bx point to the INDOS flag
        cmp byte ptr [bx],0     ;DOS function active?
        pop bx
        pop ds

        ret

dosactive endp


;******** INTERRUPS ****************************************;

;******** int 08 **********;

int08:
        jmp short cont08
        db 'sp'
cont08:
        cmp cs:tsrnow,0         ;si es 0 TIME OUT.
        je i8_end               ;volver a intentar

        dec cs:tsrnow           ;decrementa para poder activar.

        cmp cs:in_bios,0        ;se esta usando la int 13
        jne i8_end              ;si =0 no, si <>0 si

        call dosactive
        je i8_tsr

i8_end:
        jmp cs:[old08]

i8_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old08]
        call start_tsr
        iret


;********* int 09 *********;

int09:
        jmp short cont09
        db 'sp'
cont09:
        push ax

        cmp cs:tsractive,0      ;ya esta activo
        jne i9_end              ;si then terminar

        cmp cs:tsrnow,0         ;waiting for activation
        jne i9_end              ;yes so terminar

        push ds                 ;lee si se pulsa el HOTKEY
        mov ax,040h
        mov ds,ax
        mov al,byte ptr ds:[17h]
        and al,key_mask
        cmp al,key_mask
        pop ds
        jne i9_end              ;No se pulsa entonces nada.

        cmp cs:in_bios,0        ;Se pulsa pero int 13 usandose
        jne i9_e1               ;Then poner tiempo de espera

        call dosactive          ;No se esta usando
        je i9_tsr               ;Pero el INDOS lo permite.Si then TSR

i9_e1:
        mov cs:tsrnow,9         ;9/18.2 = 0.5 segundos de espera.

i9_end:
        pop ax
        jmp cs:[old09]

i9_tsr:
        mov cs:tsractive,1
        mov cs:tsrnow,0
        pushf
        call cs:[old09]
        pop ax
        call start_tsr
        iret


;********* int 2f *********;

int2f:
        jmp short cont2f
        db 'sp'
cont2f:
        cmp ax,0d200h
        jne no_d200
        xchg al,ah
        iret

no_d200:
        jmp cs:[old2f]


;********** int 13 *********;

int13:
        jmp short cont13
        db 'sp'
cont13:
        inc cs:in_bios

        pushf
        call cs:[old13]

        dec cs:in_bios

        sti
        ret 2


;********* int 28 **************;

int28:
        jmp short cont28
        db 'sp'
cont28:
        cmp cs:tsrnow,0         ;tsrnow es el tiempo que tiene
        je i28_end              ;para empezar a ejecutarce.
                                ;Si es 0 se ejecuta.

        cmp cs:in_bios,0        ;in_bios es el flag de la int 13
        je i28_tsr              ;si es 0 se puede ejecutar.

i28_end:
        jmp cs:[old28]

i28_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old28]
        call start_tsr
        iret

;********** START PROGRAMA RESIDENTE *************;

start_tsr proc near

        cli
        mov cs:xx_ss,ss
        mov cs:xx_sp,sp

        mov sp,offset stacks_end
        mov ax,cs
        mov ss,ax

        sti

        push ax
        push bx
        push cx
        push dx
        push ds
        push es
        push si
        push di

        mov ah,0fh
        int 10h                 ;*** get video mode ***
        push cs
        pop ds
        and al,01111111b        ;apaga el bit 7 por las dudas.
        mov cs:videomode,al
        mov cs:displaypage,bh

        mov byte ptr cs:mode,16 ;Default 16 colors

        xor ah,ah               ;calcula cuantos bytes se deben grabar
        mov bx,offset m_00h     ;depende el mode grafico que se use
        shl al,1
        add bx,ax
        mov ax,cs:[bx]
        mov cs:number_bytes,ax
        cmp ax,0
        jne mode_supported      ;si number_bytes=0 mode not supported
        jmp error2

mode_supported:
        mov cs:bitplane,3       ;default 4 pages

        mov ah,12h
        mov bl,32h
        mov al,0
        int 10h                 ;*** Enable CPU access to video RAM ***

        mov ah,10h              ;func number
        mov al,17h              ;sub-func number
        push cs                 ;es:dx=address of buffer
        pop es
        mov dx,offset dac_color
        mov cx,255              ;number of regs to be loaded
        xor bx,bx               ;number of the 1st DAC color to be loaded
        int 10h                 ;*** Load contents of multiple DAC color reg ***

        mov ah,10h
        mov al,9
        push cs
        pop es
        mov dx,offset palette   ;es:dx=buffer
        int 10h                 ;*** Read contents of palette and overscan ***

        mov ah,3ch
        xor cx,cx               ;File attribute
        push cs
        pop ds                  ;ds:dx=nombre1
        mov dx,offset nombre1
        int 21h                 ;*** create or truncate a file ***
        jnc no_error_1
        jmp error

no_error_1:
        mov cs:handle1,ax

        mov bx,cs:handle1
        mov cx,1000             ;1000 bytes de cabecera
        mov dx,offset cabeza
        push cs
        pop ds                  ;ds:dx=Source code
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        jnc no_error_2
        jmp error

no_error_2:
        cmp byte ptr cs:videomode,13h ;es mode 13h? (256 colores)
        jne volver3              ;No, entonces 16 colores

mode13h:                        ;**********Aca empieza lo referente a 256 col
        mov ax,0a000h           ;source ds:si
        mov ds,ax
        xor si,si
        mov ax,cs:seg_mem       ;destination es:di
        mov es,ax
        xor di,di
        mov cx,cs:number_bytes  ;cantidad de bytes a trasladar

        mov al,ds:[si]          ;translada Data de fuente a destino
        mov es:[di],al

        rep movsb

        mov cs:bitplane,0
        jmp short saltar_aca    ;ir a grabar informacion.Solo un page


volver3:                        ;**************Aca empieza lo referente a 16 col

        mov ax,0a000h           ;ds:si source
        mov ds,ax
        xor si,si
        mov ax,cs:seg_mem       ;es:di target
        mov es,ax
        xor di,di
        mov cx,cs:number_bytes  ;Lo necesario para el mode 12h

        mov dx,3ceh             ;Address graphic controller
        mov ax,0105h            ;Write mode 1/Read mode 0
        out dx,ax
        mov ah,cs:bitplane      ;Number of Bitplane (0=first)
        mov al,4                ;Select Bitplane
        out dx,ax

        rep movsb               ;cantidad de bytes dependiendo del mode

saltar_aca:                     ;a esta rutina se engancha el mode 256 col
        mov ah,42h
        mov al,2                ;mode 2=to the end of file
        xor dx,dx
        xor cx,cx
        mov bx,cs:handle1
        int 21h                 ;*** move file pointer to end of file ***

        mov cx,cs:number_bytes  ;depende del mode
        mov ax,cs:seg_mem
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        cmp cs:bitplane,0
        je listo
        dec cs:bitplane
        jmp short volver3

listo:
error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

error2:
        pop di
        pop si
        pop es
        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        cli

        mov ss,cs:xx_ss
        mov sp,cs:xx_sp

        mov cs:tsractive,0
        sti

        ret

start_tsr endp

;************ data **********************;

xx_ss   dw 0
xx_sp   dw 0

tsrnow  db 0
tsractive db 0
in_bios db 0

dta_off dw 0
dta_seg dw 0

indos equ this dword
indos_off dw 0
indos_seg dw 0

old08 equ this dword
off08 dw 0
seg08 dw 0

old09 equ this dword
off09 dw 0
seg09 dw 0

old13 equ this dword
off13 dw 0
seg13 dw 0

old28 equ this dword
off28 dw 0
seg28 dw 0

old2f equ this dword
off2f dw 0
seg2f dw 0

;****** TABLES ******

;only saves the 16-color-graphics

m_00h           dw 0            ;=0 not supported
m_01h           dw 0
m_02h           dw 0
m_03h           dw 0
m_04h           dw 0
m_05h           dw 0
m_06h           dw 0
m_07h           dw 0
m_08h           dw 0
m_09h           dw 0
m_0ah           dw 0
m_0bh           dw 0
m_0ch           dw 0
m_0dh           dw 8000
m_0eh           dw 16000
m_0fh           dw 0
m_10h           dw 28000
m_11h           dw 0
m_12h           dw 38400
m_13h           dw 64000

number_bytes    dw 0

;**** more date *****

handle1         dw 0
seg_mem         dw 0
bitplane        db 0
mode            db 0

;****** MESSAGES *****
error_inst_msg1 db 'Error.',13,10
db 'Imposible desinstalarlo si no esta residente.',13,10,'$'

error_isnt_msg2 db 'Error.',13,10
db 'Trate de desinstalar otros TSR para poder desinstalar Savepic.',13,10,'$'

mem_error_msg   db 13,10,'Error.',13,10
db 'Necesita por lo menos 65k RAM de memoria libre.',13,10,'$'

path_error_msg  db 13,10,'Error.',13,10
db 'Verifique que el path y el nombre del archivo sean correctos.',13,10,'$'

already_ins_msg db 'Error.',13,10
db 'Ya hay una version instalada de SAVEPIC.',13,10
db 'Desinstale la que esta residente usando el comando "/u",',13,10
db 'y luego de eso, trate de instalar SAVEPIC.',13,10,'$'

help_msg        db 13,10,'Savepic v1.40b por Ricardo Quesada.',13,10,13,10
db 'SAVEPIC /comando',13,10
db 'Comandos:',13,10
db '    /u            Desinstala Savepic.',13,10
db '    /n=path\name  Deja residente Savepic para que se pueda grabar el grafico',13,10
db '                  en el "path" con el "name".',13,10,13,10
db 'Ejemplo:',13,10
db '    savepic /n=c:\dibujos\paisaje',13,10,13,10
db 'Esa linea de comando deja residente a Savepic y cuando se pulse el Hotkey',13,10
db 'el dibujo se grabara en "c:\dibujos\" como "paisaje".',13,10,13,10
db 'HotKey = Shift izquiero + Shift derecho.',13,10
db 'Recuerde que necesita por lo menos 65k de memoria RAM libre',13,10
db 'y entre 32k y 160k de espacio en disco dependiendo del archivo que se grabe.',13,10,13,10,'$'

installing_msg  db 13,10,'Savepic v1.40b por Ricardo Quesada.',13,10
                db 'Instalado.',13,10
                db 'HotKey = Shift izquierdo + Shift derecho.',13,10,'$'

uninstall_msg   db 13,10,'Savepic v1.40b por Ricardo Quesada.',13,10
                db 'Desinstalado.',13,10,'$'


nombre1         db 'alessia te amo.........................................'

cabeza          db 'Savepic.'
autor           db 'RQuesada'
ver             db '01.40'
videomode       db 0
displaypage     db 0
dac_color       db 256*3 dup(0)
palette         db 17 dup (0)

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

