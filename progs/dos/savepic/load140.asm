;name: loadpic
;verion: 1.40
;start  date: 14/10/93
;finish date: 20/10/93
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        je displayhelp

        cmp al,'/'
        jne alop                ;loop until '/' or CR is found

        lodsb
        cmp al,'?'
        je displayhelp
        and al,11011111b
        cmp al,'N'
        je  newname

displayhelp:                    ;*****************command line H
        push cs
        pop ds
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***


newname:                        ;******************** command line N
        lea bx,nombre1
        lodsb
buscanombre:
        lodsb
        mov cs:[bx],al
        cmp al,13
        je endname
        inc bx
        mov cs:[bx],al
        jmp short buscanombre

endname:
        mov byte ptr ds:[bx],0

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,1000h            ;64k
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        mov cs:seg_mem,ax


;********** drive user ***************;

        mov ah,0fh
        int 10h
        mov cs:oldvideo,al

        push cs
        pop ds
        mov dx,offset loading_msg  ;loading messagge
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,3d02h            ;open file with al=2 (W/R access)
        mov dx,offset nombre1
        int 21h
        jnc no_error

        jmp error               ;salta porque hubo error

no_error:
        mov cs:handle1,ax
        mov cs:bitplane,0

        mov bx,cs:handle1
        mov cx,1000             ;1000 bytes
        mov ah,3fh              ;read
        push cs
        pop ds
        mov dx,offset cabeza
        int 21h

        push cs
        pop ds
        push cs
        pop es
        mov si,offset compare
        mov di,offset cabeza
        mov cx,16
        rep cmpsb
        je pic_ok

        mov dx,offset pic_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

pic_ok:
        mov si,offset compare_ver
        mov di,offset ver
        mov cx,5
        rep cmpsb
        je ver_ok

        mov dx,offset ver_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

ver_ok:
        mov ah,0
        mov al,cs:videomode
        int 10h                 ;*** set video mode ***

        xor ah,ah               ;calcula cuantos bytes se deben grabar
        mov bx,offset m_00h     ;depende el mode grafico que se use
        shl al,1
        add bx,ax
        mov ax,cs:[bx]
        mov cs:number_bytes,ax

        mov ah,5
        mov al,cs:displaypage
        int 10h                 ;*** set active display page ***

        mov ah,10h
        mov al,12h
        xor bx,bx
        mov cx,255
        push cs
        pop es
        mov dx,offset dac_color
        int 10h                 ;*** Load multiple DAC color registers ***

        mov ah,10h
        mov al,2
        push cs
        pop es
        mov dx,offset palette
        int 10h                 ;*** Set all palette registers ***

        mov cs:bitplane,3

        mov ah,12h
        mov bl,32h
        mov al,0
        int 10h                 ;*** Enable CPU access to video Ram ***

        cmp byte ptr cs:videomode,13h
        jne no_video13h         ;es mode 13h (256 col).No then 16 col

video13h:                       ;empieza lo 256 colores ********************

        mov cx,64000            ;number of bytes
        mov ax,0a000h           ;ds:dx=buffer
        mov ds,ax
        xor dx,dx
        mov bx,cs:handle1       ;bx=handle
        mov ah,3fh
        int 21h                 ;*** read from file using handle ***

        jmp short listo         ;Se termino lo 256 colores


no_video13h:                    ;empieza lo 16 colores *****************
        mov cs:plane,8

read_again:
        mov cx,cs:number_bytes  ;number of bytes
        mov ax,cs:seg_mem       ;ds:dx=buffer
        mov ds,ax
        xor dx,dx
        mov bx,cs:handle1       ;bx=handle
        mov ah,3fh
        int 21h                 ;*** read from file using handle ***

        mov ax,0a000h
        mov ds,ax
        xor si,si

        mov ax,cs:seg_mem
        mov es,ax
        xor di,di

seguir_loop:

        mov dx,3c4h
        mov al,2                ;map mask register with bitplane 1
        mov ah,cs:plane
        out dx,ax
        mov dx,3ceh
        mov ax,0005h            ;read mode 0,write mode 2
        out dx,ax
        mov ax,0003h            ;replace mode 0
        out dx,ax
        mov ax,0ff08h           ;bit mask to bit mask register 0
        out dx,ax

        mov ax,es:[di]
        mov ds:[si],ax

        inc si
        inc di
        cmp si,cs:number_bytes
        jne seguir_loop

        shr cs:plane,1

        cmp cs:bitplane,0
        je listo
        dec cs:bitplane
        jmp short read_again

listo:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        mov ah,8
        int 21h                 ;*** wait until a key is pressed ***

        mov al,cs:oldvideo      ;*** old video mode ***
        mov ah,0
        int 10h

        push cs
        pop ds
        mov dx,offset listo_msg
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 0 ***

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        push cs
        pop ds
        mov dx,offset drv_error_msg
        mov ah,9
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***


;************ data **********************;

;**** tables ****

;only saves the 16-color-graphics

m_00h           dw 0
m_01h           dw 0
m_02h           dw 0
m_03h           dw 0
m_04h           dw 0
m_05h           dw 0
m_06h           dw 0
m_07h           dw 0
m_08h           dw 0
m_09h           dw 0
m_0ah           dw 0
m_0bh           dw 0
m_0ch           dw 0
m_0dh           dw 8000
m_0eh           dw 16000
m_0fh           dw 0
m_10h           dw 28000
m_11h           dw 0
m_12h           dw 38400
m_13h           dw 0

number_bytes    dw 0


;****** MESSAGES *****

help_msg        db 13,10,'Loadpic v1.40 por Ricardo Quesada.',13,10,13,10
db 13,10,'LOADPIC /comando',13,10
db 'Comando:',13,10
db '    /n=path\name  Busca el en el "path" el archivo  "name" y muestra',13,10
db '                  el grafico en pantalla',13,10,13,10
db 'Ejemplo:',13,10
db '    loadpic /n=c:\dibujos\paisaje',13,10,13,10
db 'Esa linea de comando busca el archivo "paisaje" en el directorio',13,10
db '"c:\dibujos\" y muestra el grafico.',13,10,13,10
db 'Recuerde que necesita por lo menos 65k de memoria RAM libre.',13,10,'$'

loading_msg     db 'Loadpic v1.40 por Ricardo Quesada.',13,10
                db 'Cargando grafico... Un momento por favor.',13,10,'$'

listo_msg       db 'Carga terminada.',13,10,'$'

mem_error_msg   db 13,10,'Error.',13,10
db 'Necesita por lo menos 65k RAM de memoria libre.',13,10,'$'

drv_error_msg   db 13,10,'Error.',13,10
db 'Archivo no encontrado. Verifique path por favor.',13,10,'$'

pic_error_msg   db 13,10,'Error.',13,10
db 'El no archivo fue grabado por Savepic.Por favor verifique.',13,10,'$'

ver_error_msg   db 13,10,'Error.',13,10
db 'El archivo no fue grabado por la version de Savepic 1.40.',13,10,'$'

handle1         dw 0
seg_mem         dw 0
bitplane        db 0
plane           db 0            ;donde se escribe (bitmap)
oldvideo        db 0

nombre1         db 'alessia te amo.........................................'

compare         db 'Savepic.RQuesada'
compare_ver     db '01.40'
cabeza          db 'Savepic.'
autor           db 'RQuesada'
ver             db '01.40'
videomode       db 0
displaypage     db 0
dac_color       db 256*3 dup(0)
palette         db 17 dup(0)

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

