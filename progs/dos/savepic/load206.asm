;name: loadpic
;verion: 2.06
;start  date: 28/10/93
;finish date: 28/10/93
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13               ;es CR,si then display help
        je displayhelp

        cmp al,20h              ;es espacio,si then le de nuevo
        je alop

        jne newname             ;si es otra cosa es el nombre.Ve alli.

displayhelp:                    ;***************** no command line
        push cs
        pop ds
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***


newname:                        ;******************** command line Name
        lea bx,nombre1
buscanombre:
        mov cs:[bx],al
        cmp al,13
        je endname
        lodsb
        inc bx
        jmp short buscanombre

endname:
        mov cs:[bx],2400h       ;24h=$ 0=nul.Uso el $ para usar la func 9 int 21

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,961h             ;64k
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        mov cs:seg_mem,ax


;********** drive user ***************;

        mov ah,0fh
        int 10h
        mov cs:oldvideo,al

        push cs
        pop ds
        mov dx,offset loading_msg  ;loading messagge
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,3d02h            ;open file with al=2 (W/R access)
        mov dx,offset nombre1
        int 21h
        jnc no_error

        jmp error               ;salta porque hubo error

no_error:

        mov cs:handle1,ax

        mov bx,cs:handle1
        mov cx,1000             ;1000 bytes
        mov ah,3fh              ;read
        push cs
        pop ds
        mov dx,offset cabeza
        int 21h

        push cs
        pop ds
        push cs
        pop es
        mov si,offset compare
        mov di,offset cabeza
        mov cx,16
        rep cmpsb
        je pic_ok

        mov dx,offset picver_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

pic_ok:
        mov si,offset compare_ver
        mov di,offset ver
        mov cx,4                ;5 si se chequea con centecimos 02.00
        rep cmpsb               ;4 si se chequea con decimos 02.0x
        je ver_ok

        mov dx,offset picver_error_msg
        mov ah,9
        int 21h                 ;*** output error message ***

        mov ax,4cfah
        int 21h                 ;*** terminate with fah ***

ver_ok:

        mov ah,0
        mov al,cs:videomode
        int 10h                 ;*** set video mode ***

        mov ah,5
        mov al,cs:displaypage
        int 10h                 ;*** set active display page ***

        mov ah,10h
        mov al,2
        push cs
        pop es
        mov dx,offset palette
        int 10h                 ;*** Set all palette registers ***

        mov ah,10h
        mov al,12h
        xor bx,bx
        mov cx,255
        push cs
        pop es
        mov dx,offset dac_color
        int 10h                 ;*** Load multiple DAC color registers ***

;***************    empieza lo dificil de codigo...

        mov cs:readers,1

        mov ax,0a000h           ;segment video memory
        mov es,ax
        xor di,di               ;offset video memory

        mov cs:bitmask,128      ;set initial bitmask (bit7=1)

read_again:
        mov cx,38400            ;
        mov ax,cs:seg_mem
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov bx,cs:handle1       ;handle
        mov ah,3Fh
        int 21h                 ;*** reade from file (handle) ***

        mov ax,cs:seg_mem       ;segment source code
        mov ds,ax
        xor si,si               ;offset source code

read_read:
        mov dx,3ceh             ;GC_INDEX
        mov al,8                ;Bit mask
        mov ah,cs:bitmask
        out dx,ax
        mov ax,0205h            ;read mode 0 ; write mode 2
        out dx,ax

        mov al,es:[di]          ;dummy read
        mov al,ds:[si]          ;read color
        mov bl,al               ;
        shr al,4                ;first hi nibble
        mov es:[di],al          ;set new color

        shr cs:bitmask,1        ;rotate bitmask one bit
        mov ah,cs:bitmask
        mov al,8
        out dx,ax               ;set new bitmask

        mov al,es:[di]          ;dummy read
        mov al,bl               ;
        mov es:[di],al          ;set new color for another pixel

        shr cs:bitmask,1        ;rotate bitmask one bit
        cmp cs:bitmask,0
        jne same_byte

        mov cs:bitmask,128
        inc di

same_byte:
        inc si
        cmp si,38400
        jne read_read

        inc cs:readers
        cmp cs:readers,5
        jne read_again

;****************** termino lo dificil de codigo...
listo:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        mov ah,8
        int 21h                 ;*** wait until a key is pressed ***

        mov al,cs:oldvideo      ;*** old video mode ***
        mov ah,0
        int 10h

        push cs
        pop ds
        mov dx,offset listo_msg
        mov ah,9
        int 21h                 ;*** output messagge ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 0 ***

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        mov ah,49h
        mov es,cs:seg_mem
        int 21h                 ;*** free allocated memory ***

        push cs
        pop ds
        mov dx,offset drv_error_msg
        mov ah,9
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***


;************ data **********************;

;****** MESSAGES *****

help_msg db 13,10
db '                           *** Loadpic v2.06 ***',13,10
db '                            por Ricardo Quesada',13,10
db 13,10,'LOADPIC comando',13,10
db 'Comando:',13,10
db '    path\name  Busca en el "path" el archivo  "name" y muestra',13,10
db '                  el grafico en pantalla',13,10,13,10
db 'Ejemplo:',13,10
db '    loadpic c:\dibujos\paisaje',13,10,13,10
db 'Esa linea de comando busca el archivo "paisaje" en el directorio',13,10
db '"c:\dibujos\" y muestra el grafico.',13,10,13,10
db 'Recuerde que necesita por lo menos 45k de memoria RAM libre.',13,10,'$'

loading_msg     db 'Loadpic v2.06 por Ricardo Quesada.',13,10
                db 'Cargando grafico...   ','$'

listo_msg       db 'OK.',13,10,'$'

mem_error_msg   db 13,10,'Error.',13,10
db 'Necesita por lo menos 45k RAM de memoria libre.',13,10,'$'

drv_error_msg   db 13,10,'Error.',13,10
db 'El siguiente archivo no fue encontrdo.',13,10

nombre1    db '***** Io ti amo Alessia, Richi *****',13,10,'Curioso que miras'

picver_error_msg db 13,10,'Error.',13,10
db 'El archivo no fue grabado por Savepic v2.0x.',13,10,'$'

handle1         dw 0
seg_mem         dw 0
bitplane        db 0            ;number of bitplane you will write
bitmask         db 0            ;primero el primer bit.
oldvideo        db 0
readers         db 0

compare         db 'Savepic.RQuesada'
compare_ver     db '02.0x'      ;la 'x' esta demas.Only check '02.0'
cabeza          db 'Ricardo '   ;tiene que ir 'Savepic.'
autor           db 'Quesada '   ;tiene que ir 'RQuesada'
ver             db '1993 '      ;tiene que ir '02.0?' ?=varia segun version.
videomode       db 0
displaypage     db 0
palette         db 17 dup(0)
dac_color       db 256*3 dup(0)

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

