;name: savepic
;verion: 1.00
;start  date: 11/10/93
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************ equ *************************;

key_mask equ 00000011b  ;Left & Right Shift


;************* START ********************;
start:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******* command line detection ********

        cld
        mov si,81h
alop:   lodsb
        cmp al,13
        jne seguir_
        jmp installed
seguir_:
        cmp al,'/'
        jne alop                ;loop until '/' or CR is found

        lodsb
        and al,11011111b
        cmp al,'N'
        jne seguir
        jmp newname
seguir:
        cmp al,'U'
        je uninstall

displayhelp:                    ;*****************command line H
        push cs
        pop ds
        mov ah,9
        mov dx,offset help_msg
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***

uninstall:                      ;*********************command line U
        mov ax,0d200h
        int 2fh
        cmp ax,00d2h
        jne error_inst2

        mov ax,3508h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3509h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3513h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,3528h
        int 21h
        cmp es:[bx+2],'ps'
        jne error_inst

        mov ax,352fh
        int 21h
        cmp es:[bx+2],'ps'
        je desinstalar

error_inst:
        push cs
        pop ds
        mov dx,offset error_isnt_msg2
        mov ah,9
        int 21h

        mov ax,4cffh
        int 21h

error_inst2:
        push cs
        pop ds
        mov dx,offset error_inst_msg1
        mov ah,9
        int 21h

        mov ax,4cfeh
        int 21h

desinstalar:                            ;*** Rutina de desinstalacion ***
        cli
        mov dx,es:off08
        mov ds,es:seg08
        mov ax,2508h
        int 21h                         ;*** restaurar vector original

        mov ax,2509h
        mov dx,es:off09
        mov ds,es:seg09
        int 21h

        mov ax,2513h
        mov dx,es:off13
        mov ds,es:seg13
        int 21h

        mov ax,2528h
        mov dx,es:off28
        mov ds,es:seg28
        int 21h

        mov ax,252fh
        mov dx,es:off2f
        mov ds,es:seg2f
        int 21h
        sti

        mov bx,es
        mov es,es:[2ch]
        mov ah,49h
        int 21h
        mov es,bx
        mov ah,49h
        int 21h                         ;Release memory

        mov ah,49h
        mov es,bx
        mov es,es:seg_mem
        int 21h

        push cs
        pop ds
        mov dx,offset uninstall_msg
        mov ah,9
        int 21h

        mov ax,4c00h
        int 21h

newname:                        ;******************** command line N
        lea bx,nombre1
        lodsb
buscanombre:
        lodsb
        mov ds:[bx],al
        cmp al,13
        je endname
        inc bx
        mov ds:[bx],al
        jmp short buscanombre

endname:
        mov byte ptr ds:[bx],0

;***** Already installed ? *******

installed:
        mov ax,0d200h            ;Is Savepic installed?
        int 2fh
        cmp ax,00d2h
        jne not_installed       ;No, so install it.

        mov ah,9
        mov dx,offset already_ins_msg ;Yes, so do not install it again.
        int 21h                 ;*** output message ***

        mov ax,4c00h
        int 21h                 ;*** terminate with 00 ***

not_installed:                  ;Install SavePic

;******* MEMORY MANAGE II *******

        mov ah,48h
        mov bx,1000h            ;64k
        int 21h                 ;*** allocate memory ***
        jnc no_mem_error

        mov ah,9                ;viene aca si hay memory error
        mov dx,offset mem_error_msg
        int 21h                 ;*** output message ***

        mov ax,4cffh
        int 21h                 ;*** terminate with ffh ***

no_mem_error:

        push cs
        pop ds
        mov seg_mem,ax

;******* GET ADDRESSES **********

        mov ah,34h              ;get address of INDOS flag
        int 21h
        mov si,offset indos
        mov [si],bx
        mov [si+2],es

        mov ah,1ch
        mov al,0
        mov cx,2
        int 10h                 ;*** Return save/restore status ***
        cmp ah,1ch
        je supported

        mov ah,9
        mov dx,offset support_error_msg
        push cs
        pop ds
        int 21h                 ;*** display error msg ***

        mov ax,4cffh
        int 21h                 ;*** Terminate with error ***

supported:
        mov ax,3508h            ;get address of int 08
        int 21h
        mov si,offset old08
        mov [si],bx
        mov [si+2],es

        mov dx,offset int08
        mov ax,2508h
        int 21h


        mov ax,3509h            ;get address of int 09
        int 21h
        mov si,offset old09
        mov [si],bx
        mov [si+2],es

        mov dx,offset int09
        mov ax,2509h
        int 21h


        mov ax,3513h            ;get address of int 13
        int 21h
        mov si,offset old13
        mov [si],bx
        mov [si+2],es

        mov dx,offset int13
        mov ax,2513h
        int 21h


        mov ax,3528h            ;get address of int 28
        int 21h
        mov si,offset old28
        mov [si],bx
        mov [si+2],es

        mov dx,offset int28
        mov ax,2528h
        int 21h


        mov ax,352fh            ;get address of int 2f
        int 21h
        mov si,offset old2f
        mov [si],bx
        mov [si+2],es

        mov dx,offset int2f
        mov ax,252fh
        int 21h


        mov ah,9                ;print message of installation
        mov dx,offset installing_msg
        int 21h

        mov dx,offset final     ;TSR
        inc dx
        int 27h

;******** RUTINES *****************************************;

;******* DOSACTIVE *******;

;Output: Zero flag=1 Dos may be interrupted.

dosactive proc near

        push ds
        push bx
        lds bx,indos            ;ds:bx point to the INDOS flag
        cmp byte ptr [bx],0     ;DOS function active?
        pop bx
        pop ds

        ret

dosactive endp


;******** INTERRUPS ****************************************;

;******** int 08 **********;

int08:
        jmp short cont08
        db 'sp'
cont08:
        cmp cs:tsrnow,0         ;si es 0 TIME OUT.
        je i8_end               ;volver a intentar

        dec cs:tsrnow           ;decrementa para poder activar.

        cmp cs:in_bios,0        ;se esta usando la int 13
        jne i8_end              ;si =0 no, si <>0 si

        call dosactive
        je i8_tsr

i8_end:
        jmp cs:[old08]

i8_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old08]
        call start_tsr
        iret


;********* int 09 *********;

int09:
        jmp short cont09
        db 'sp'
cont09:
        push ax

        cmp cs:tsractive,0      ;ya esta activo
        jne i9_end              ;si then terminar

        cmp cs:tsrnow,0         ;waiting for activation
        jne i9_end              ;yes so terminar

        push ds                 ;lee si se pulsa el HOTKEY
        mov ax,040h
        mov ds,ax
        mov al,byte ptr ds:[17h]
        and al,key_mask
        cmp al,key_mask
        pop ds
        jne i9_end              ;No se pulsa entonces nada.

        cmp cs:in_bios,0        ;Se pulsa pero int 13 usandose
        jne i9_e1               ;Then poner tiempo de espera

        call dosactive          ;No se esta usando
        je i9_tsr               ;Pero el INDOS lo permite.Si then TSR

i9_e1:
        mov cs:tsrnow,9         ;9/18.2 = 0.5 segundos de espera.

i9_end:
        pop ax
        jmp cs:[old09]

i9_tsr:
        mov cs:tsractive,1
        mov cs:tsrnow,0
        pushf
        call cs:[old09]
        pop ax
        call start_tsr
        iret


;********* int 2f *********;

int2f:
        jmp short cont2f
        db 'sp'
cont2f:
        cmp ax,0d200h
        jne no_d200
        xchg al,ah
        iret

no_d200:
        jmp cs:[old2f]


;********** int 13 *********;

int13:
        jmp short cont13
        db 'sp'
cont13:
        inc cs:in_bios

        pushf
        call cs:[old13]

        dec cs:in_bios

        sti
        ret 2


;********* int 28 **************;

int28:
        jmp short cont28
        db 'sp'
cont28:
        cmp cs:tsrnow,0         ;tsrnow es el tiempo que tiene
        je i28_end              ;para empezar a ejecutarce.
                                ;Si es 0 se ejecuta.

        cmp cs:in_bios,0        ;in_bios es el flag de la int 13
        je i28_tsr              ;si es 0 se puede ejecutar.

i28_end:
        jmp cs:[old28]

i28_tsr:
        mov cs:tsrnow,0
        mov cs:tsractive,1
        pushf
        call cs:[old28]
        call start_tsr
        iret

;********** START PROGRAMA RESIDENTE *************;

start_tsr proc near

        cli
        mov cs:xx_ss,ss
        mov cs:xx_sp,sp

        mov sp,offset stacks_end
        mov ax,cs
        mov ss,ax

        sti

        push ax
        push bx
        push cx
        push dx
        push ds
        push es
        push si
        push di

        mov ah,12h
        mov bl,32h
        mov al,0
        int 10h                 ;*** Enable direct CPU acces to VideoRam ***


        mov ah,0fh
        int 10h                 ;*** get video mode ***
        push cs
        pop ds
        mov cs:videomode,al
        mov cs:displaypage,bh

        mov ah,1ch
        mov al,0
        mov cx,2
        mov bx,offset dac_colors
        int 10h                 ;*** return save status of Dac colors ***

        mov ah,1ch
        mov al,1
        mov cx,2
        push cs
        pop es
        mov bx,offset dac_colors
        int 10h                 ;*** Save info about Dac colors ***

        mov cs:pages,4          ;default 4 pages

        mov ah,3ch
        xor cx,cx               ;File attribute
        push cs
        pop ds
        mov dx,offset nombre1
        int 21h                 ;*** create or truncate a file ***

        jc error               ;Si hubo error.

        mov cs:handle1,ax

        mov bx,ax
        mov cx,2000             ;1000 bytes de cabecera
        mov dx,offset cabeza
        push cs
        pop ds                  ;ds:dx=Source code
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        jc error

        mov cs:bitplane,0

volver3:

        mov ax,0a000h           ;ds:si source
        mov ds,ax
        xor si,si
        mov ax,cs:seg_mem       ;es:di target
        mov es,ax
        xor di,di
        mov cx,0ffffh           ;64k

        mov dx,3ceh             ;Address graphic controller
        mov ah,cs:bitplane      ;Number of Bitplane (0=first)
        mov al,4                ;Select Bitplane
        out dx,ax
        mov ax,0105h            ;Write mode 1/Read mode 0
        out dx,ax

        rep movsb               ;Copy 64k

        mov ah,42h
        mov al,2                ;mode 2=to the end of file
        xor dx,dx
        xor cx,cx
        mov bx,cs:handle1
        int 21h                 ;*** move file pointer to end of file ***

        mov cx,0ffffh           ;64k se escriben
        mov ax,cs:seg_mem
        mov ds,ax               ;ds:dx  Segment allocated w/info the bitplane
        xor dx,dx
        mov ah,40h
        int 21h                 ;*** write to file (handle) ***

        inc cs:bitplane
        mov al,cs:pages
        cmp cs:bitplane,al
        jb volver3

error:
        mov bx,cs:handle1
        mov ah,3eh
        int 21h                 ;*** close handle1 ***

        pop di
        pop si
        pop es
        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        cli

        mov ss,cs:xx_ss
        mov sp,cs:xx_sp

        mov cs:tsractive,0
        sti

        ret

start_tsr endp

;************ data **********************;

xx_ss   dw 0
xx_sp   dw 0

tsrnow  db 0
tsractive db 0
in_bios db 0

dta_off dw 0
dta_seg dw 0

indos equ this dword
indos_off dw 0
indos_seg dw 0

old08 equ this dword
off08 dw 0
seg08 dw 0

old09 equ this dword
off09 dw 0
seg09 dw 0

old13 equ this dword
off13 dw 0
seg13 dw 0

old28 equ this dword
off28 dw 0
seg28 dw 0

old2f equ this dword
off2f dw 0
seg2f dw 0

;****** MESSAGES *****
error_inst_msg1 db 'Error.',13,10
db 'Imposible desinstalarlo si no esta residente.',13,10,'$'

error_isnt_msg2 db 'Error.',13,10
db 'Trate de desinstalar otros TSR para poder desinstalar Savepic.',13,10,'$'

support_error_msg     db 'Error.',13,10
db 'La funcion 1ch de la int 10h no se puede ejecutar.',13,10,'$'

mem_error_msg   db 13,10,'Error.',13,10
                db 'Necesita por lo menos 70k RAM de memoria libre.',13,10,'$'

help_msg        db 13,10,'Savepic v1.00 por Ricardo Quesada.',13,10
                db 'Pantalla de ayuda.',13,10
db 13,10,'Tipee Savepic [/{comando}]',13,10
db 'Comandos:',13,10
db '    /h            muestra esta pantalla.',13,10
db '    /u            Desinstala Savepic.',13,10
db '    /n=Nombre     Donde "Nombre" es el nombre con que se grabara el grafico.',13,10
db '                  Se puede incluir un path completo.El nombre por defecto',13,10
db '                  es PICTURE.PIC y se graba en el directorio actual.',13,10,13,10
db 'Ejemplo:',13,10
db '    savepic /n=c:\dibujos\dibujo.pic',13,10,13,10
db 'Esa linea de comando deja residente a Savepic y cuando se apriete el Hotkey',13,10
db 'el dibujo se grabara en "c:\dibujos\" como "dibujo.pic".',13,10,13,10
db 'Hotkey:Shift izquiero + Shift derecho.',13,10
db 'Recuerde que necesita por lo meno 70k de memoria RAM libre',13,10
db 'y 256k de espacio libre en disco por cada archivo que se grabe.',13,10,13,10,'$'

installing_msg  db 13,10,'Savepic v1.00 por Ricardo Quesada.',13,10
                db 'Escriba Savepic /? para mas ayuda.',13,10
                db 'Hotkey = Shift izq + Shift der.',13,10
                db 'Instalado.',13,10,'$'

already_ins_msg db 13,10,'Savepic v1.00 por Ricardo Quesada.',13,10
                db 'Escriba Savepic /? para mas ayuda.',13,10
                db 'Hotkey = Shift izq + Shift der.',13,10
                db 'Ya hay una version instalada.No se debe instalar otra.',13,10,'$'

uninstall_msg   db 13,10,'Savepic v1.00 por Ricardo Quesada.',13,10
                db 'Desinstalado.',13,10,'$'

handle1         dw 0
seg_mem         dw 0
bitplane        db 0
pages           db 0

nombre1         db 'picture.pic',0,'espacios dejados para poner otro nombre'

cabeza          db '2Savepic'
autor           db 'RQuesada'
videomode       db 0
displaypage     db 0
dac_colors      db 1024 dup (0)

stacks  dw 256 dup(0)
stacks_end equ this byte

;********* F I N A L **********;

final equ this byte

ruti    endp
code    ends
        end ruti

