/* ESTAS SON LIBRERIAS CREADAS POR MI PARA EL MEJOR MANEJO DE TEXTO */
/* NO OLVIDARSE DE INCLUIR EL texto.h EN EL PROGRAMA PRINCIPAL      */
/* CREADA EL 25/2/94						    */


#include <conio.h>
#include <string.h>

/******************************************************/
/*************************  CENTRAR  ******************/
/******************************************************/
void centrar( char *buf, int y )
{
    /* variables */
    int lenght;

    lenght = 40 - (strlen(buf) /2);
    if (y==0)
	y = wherey();
    gotoxy(lenght,y);
    cprintf("%s",buf);
}

/******************************************************/
/*******************     caja      ********************/
/******************************************************/
void caja( int x1, int y1, int x2, int y2 )
{
	/* variables */
	int p1=x1,q1=y1;

	for(;x1!=x2;x1++) {
		gotoxy(x1,y1);
		cprintf("�");
		gotoxy(x1,y2);
		cprintf("�");
	}
	x1=p1;
	for(;y1!=y2;y1++) {
		gotoxy(x1,y1);
		cprintf("�");
		gotoxy(x2,y1);
		cprintf("�");
	}
	y1=q1;

	gotoxy(x1,y1);
	cprintf("�");
	gotoxy(x2,y1);
	cprintf("�");
	gotoxy(x1,y2);
	cprintf("�");
	gotoxy(x2,y2);
	cprintf("�");
}

/******************************************************/
/*******************     linea     ********************/
/******************************************************/
void linea( int x1, int x2, char a, int y1 )
{
	if (y1 == 0)
		y1 = wherey();
	gotoxy( x1 , y1);
	for(;x1<=x2;x1++) {
		gotoxy( x1 , y1 );
		cprintf("%c",a );
	}
}