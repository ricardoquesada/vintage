;name: beep
;verion: 1.00
;start  date: 28/10/93
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;************* START ********************;
start:
        mov al,182
        out 43h,al
        mov ax,0909h
        out 42h,al
        mov al,ah
        out 42h,al

        in al,61h
        or al,11b
        out 61h,al

        mov cx,01000h
aqui2:
        mov dx,0040h
aqui:
        dec dx
        jne aqui
        dec cx
        jne aqui2

        in al,16h
        and al,11111100b
        out 61h,al

        mov ax,4c00h
        int 21h


ruti    endp
code    ends
        end ruti
