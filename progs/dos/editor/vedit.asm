;name: VEdit
;version: 0.10
;revision:
;start	date: 25/09/94
;finish date:
;author: Ricardo Quesada

code	segment public byte 'code'
org 	100h
ruti	PROC FAR
		assume cs:code,ds:code,ss:code,es:code

;defines

ESQ equ 011Bh			;equivale a ESC, no poner ESC con tasm 5.0
SPACE = 3920h
BACKS = 0e08h
TAB   = 0f09h
UP    = 48h
RIGHT = 4dh
DOWN  = 50h
LEFT  = 4bh
INTRO = 1c0dh
INSS  = 52h
DEL   = 53h
HOME  = 47h
KEND  = 4Fh

MAX_ARR = 13
MAX_ABJ = 23
MAX_IZQ = 1
MAX_DER = 78

start:
;*******************************;
;Inicializaci�n 				;
;*******************************;
	mov ah,4ah
	mov bx,offset ende
	add bx,15
	mov cl,4
	inc bx
	int 21h

	mov sp,offset ende

;*****************************;
;*****************************;

		call monocolor			;es mono o color
		call allocmem			;Alloc mem for editor purpose
		call savescr			;Salva info de pantalla
        call clrscr             ;borrar pantalla del editor
		call drawbox			;dibujar caja
		call titulo 			;muestra el titulo.
		call tipear 			;llamar al editor
		call grabar 			;Graba toda la info que hay mem
		call restscr			;Restaura info de pantalla.
		mov ax,4c00h			;terminar
		int 21h


;*******************************;
;*******************************;
;			tipear				;
;*******************************;
;*******************************;

tipear proc near

		push ax
		push bx
		push cx
		push dx
		push di
		push si
		push ds
		push es

		push cs
		pop ds

		mov flaga,0

		mov ecol,MAX_IZQ
		mov erow,MAX_ARR

tipear_loop:
		call irpos
		call colrow
		call getchar2
		call chartomem			;Write char to mem.

		mov flaga,0 			;Flag usado tambien por GetChar
		mov ax,escan			;
		cmp al,0				;
		je flagggon 			;
		cmp al,0e0h 			;	 detecta si se apreto tecla especial
		jne flagggoff			;
flagggon:						;
		mov flaga,1 			;
flagggoff:
		cmp escan,ESQ
		je endtipear

        cmp hican,UP
		je goarr
        cmp hican,DOWN
		je goaba
        cmp hican,LEFT
		je goizq
        cmp hican,RIGHT
		je goder
		cmp escan,BACKS
		je bspace
		cmp escan,TAB
		je gotab
		cmp escan,INTRO
		je genter
        cmp hican,INSS
        je esins
        cmp hican,HOME
        je ehome
;						;cualquier otra tecla la escribe
		cmp flaga,1
		jne acapibe 	;tecla especial, no mostrar ir al comienzo
		jmp tipear_loop
acapibe:
		mov ax,escan
		mov ah,0ah
		mov cx,1
		xor bh,bh				;pagina 0
		int 10h
		call go_der
		jmp tipear_loop

endtipear: jmp end_tipear
goarr:	call go_arr
		jmp tipear_loop
goaba:	call go_aba
		jmp tipear_loop
goizq:	call go_izq
		jmp tipear_loop
goder:	call go_der
		jmp tipear_loop
bspace: call b_space
		jmp tipear_loop
gotab:	call go_tab
		jmp tipear_loop
genter: call g_enter
		jmp tipear_loop
esins:  call g_ins
        jmp tipear_loop
ehome:  call g_home
        jmp tipear_loop

go_arr:
		dec erow
		cmp erow,MAX_ARR			 ;te pasastes
		jae go_arr_
		mov erow,MAX_ABJ			 ;principio
go_arr_:
		ret
go_izq:
        dec offmem                      ;offset de memoria
        dec ecol
        cmp ecol,MAX_IZQ
		jae go_izq_
		mov ecol,MAX_DER
        jmp short go_arr
go_izq_:
		ret
go_aba:
		inc erow
		cmp erow,MAX_ABJ
		jbe go_aba_
		mov erow,MAX_ARR			 ;principio
go_aba_:
		ret
go_der:
		inc ecol
		cmp ecol,MAX_DER
		jbe go_der_
		mov ecol,MAX_IZQ
		jmp short go_aba
go_der_:
		ret
b_space:
		call go_izq
		call irpos
		mov ah,0ah
		mov al,32				;espacio
		mov cx,1
		xor bh,bh				;pagina 0
		int 10h
		ret

go_tab:
		push cx
		mov cl,8
again_tab:
		call go_der
		dec cl
		or cl,cl
		jne again_tab
		pop cx
		ret

;***********
g_ins:
        push di
        push si
        push ds
        push es

        mov al,flagins
        xor al,1
        and al,1
        mov flagins,al

        cmp al,1
        je inson
insoff:
        mov cl,4
        mov ch,2
        mov ah,1
        int 10h
        lea si,supmsg
        jmp short finins
inson:
        mov cl,8
        mov ch,2
        mov ah,1
        int 10h
        lea si,insmsg
finins:
        mov ax,cs
        mov es,ax
        mov ds,ax
        lea di,iin
        mov cx,3
        rep movsb

        pop es
        pop es
        pop si
        pop di
        ret

;*****************

g_home: mov ecol,MAX_IZQ
        ret
g_enter:
		mov ecol,MAX_IZQ
		jmp go_aba


end_tipear:                     ;******************************* TIPEAR END
		pop es
		pop ds
		pop si
		pop di
		pop dx
		pop cx
		pop bx
		pop ax

		ret

tipear endp

;******************************;
;		getchar2			   ;
;******************************;
getchar2 proc near
		push ax
		push ds

		push cs
		pop ds
        xor ah,ah
        int 16h                 ;Keaboard Read
		mov escan,ax
        mov hican,ah
        mov locan,al

		pop ds
		pop ax
		ret
getchar2 endp

;*******************************;
;	ColRow						;
;*******************************;
colrow proc near
	push ax
	push bx
	push cx
	push dx
	push ds

	mov ah,3
	xor bh,bh
	int 10h

	mov al,dl		;Columa
	lea bx,col
	call verasc
	mov al,dh		;Row
    sub al,MAX_ARR
    inc al
    lea bx,row
	call verasc

   call titulo

	pop ds
	pop dx
	pop cx
	pop bx
	pop ax
	ret
colrow endp

;*******************************;
;	verasc						;
;*******************************;
verasc proc near
	push ax
	push bx
	push cx
	push dx
	push ds

	push cs
	pop ds

	mov ch,'0'
	mov [bx],ch
	mov [bx+1],ch
	mov [bx+2],ch

asc0:
	or al,al
	je verasc_

	dec al
	inc byte ptr [bx+2]
	cmp byte ptr [bx+2],3ah
	je asc1
	jmp short asc0

asc1:
	mov byte ptr [bx+2],'0'
	inc byte ptr [bx+1]
	cmp byte ptr [bx+1],3ah
	je asc2
	jmp short asc0

asc2:
	mov byte ptr [bx+1],'0'
	inc byte ptr [bx]
	cmp byte ptr [bx],3ah
	jmp short asc0


verasc_:
	pop ds
	pop dx
	pop cx
	pop bx
	pop ax
    ret
verasc endp

;*******************************;
;	Chartomem					;
;*******************************;
chartomem proc near
	push ds
	push es
	push si
	push di
	push ax

	push cs
	pop ds

	mov es,segmem
	mov di,offmem
    cmp al,0
    je chartomem_
    cmp al,0e0h
    je chartomem_

    mov es:[di],al
	inc di
    mov offmem,di

    cmp al,13               ;Se apret� Return
    jne chartomem_
    mov al,10
    mov es:[di],al
    inc di
    mov offmem,di

chartomem_:
    pop ax
	pop di
	pop si
	pop es
	pop ds
	ret
chartomem endp

;*******************************;
;		irpos					;
;*******************************;
irpos proc near

		push ax
		push bx
		push dx
		push ds

		push cs
		pop ds

		mov ah,2
		xor bh,bh				;pagina 0
		mov dh,erow
		mov dl,ecol
		int 10h 				;posciciona el cursor

		pop ds
		pop dx
		pop bx
		pop ax

		ret

irpos endp

;***********************;
;		clrscr			;
;***********************;
clrscr proc near

		mov ah,7
		mov al,0
		mov bh,07h
        mov ch,MAX_ARR
        mov cl,MAX_IZQ
        mov dh,MAX_ABJ
        mov dl,MAX_DER
		int 10h 				;scroll window

		mov ax,500h
		int 10h 				;Select active display page

		mov ah,0Fh
		int 10h 				;Get video mode

		mov ah,3				;es el numero 3
		cmp al,ah
		je loc_3
		mov al,ah
		xor ah,ah
		int 10h 				;no entonces lo pone

loc_3:
		mov ah,2
		mov bh,0
		xor dx,dx
		int 10h 				;set cursos position

		ret

clrscr	endp

;***************************************;
;		MONOCOLOR						;
;***************************************;
monocolor proc near

		mov addr,0b800h
		mov ax,0040h
		mov es,ax
		mov al,30h
		and al,es:[10h]
		cmp al,30h
		jne sig
		mov addr,0b000h
sig:
		ret
monocolor endp

;**************************************;
;		DRAWBOX 					   ;
;**************************************;
drawbox proc near
		mov erow,MAX_ARR-1
		mov ecol,MAX_IZQ-1
		call irpos
		mov al,'�'
		call putchar
volverabox:
		inc ecol
		call irpos
		mov al,'�'
		call putchar					;write a char at cursor position
		cmp ecol,MAX_DER
		jbe volverabox
		mov al,'�'
		call putchar

		mov erow,MAX_ABJ+1
		mov ecol,MAX_IZQ-1
		call irpos
		mov al,'�'
		call putchar
volverabox2:
		inc ecol
		call irpos
		mov al,'�'
		call putchar
		cmp ecol,MAX_DER
		jbe volverabox2
		mov al,'�'
		call putchar

		mov erow,MAX_ARR-1
		mov ecol,MAX_IZQ-1
volverabox3:
		inc erow
		call irpos
		mov al,'�'
		call putchar					;write a char at cursor position
		cmp erow,MAX_ABJ
		jb volverabox3

		mov erow,MAX_ARR-1
		mov ecol,MAX_DER+1
volverabox4:
		inc erow
		call irpos
		mov al,'�'
		call putchar
		cmp erow,MAX_ABJ
		jb volverabox4

		ret
drawbox endp

;**************************************;
;		putchar 					   ;
;**************************************;
putchar proc near
		mov ah,0ah
		xor bh,bh
		mov cx,1
		int 10h
		ret
putchar endp

;**************************************;
;		allocmem					   ;
;**************************************;
allocmem proc near
	mov ah,48h
	mov bx,1000h			;64k
	int 21h
	jc allocerror
	mov segmem,ax
	ret
allocerror:
	lea dx,memerror
	mov ah,9
	int 21h
	mov ax,4cffh
	int 21h 			;Return to DOS.

allocmem endp

;**************************************;
;	Titulo							   ;
;**************************************;
titulo proc near
	push ax
	push ds
	push es
	push si
	push di
	push bp

	mov ax,cs
	mov ds,ax
	mov es,ax

	mov ah,13h
	mov al,0					;Subservice 0
	xor bh,bh					;Display page
	mov bl,70h					;Attribute
	mov cx,80					;lenght of string
    mov dh,11
    xor dl,dl
    lea bp,titul
	int 10h 					;Write a string

	pop bp
	pop di
	pop si
	pop es
	pop ds
	pop ax
	ret
titulo endp

;**************************************;
;	   Grabar						   ;
;**************************************;
grabar proc near
	push ax
	push bx
	push cx
	push dx
	push ds
	push es
	push di
	push si

	push cs
	pop ds

	mov ah,3ch
	xor cx,cx
	lea dx,nombre
	int 21h 				;Create File
	mov filehand,ax

	mov bx,ax				;File handle
	mov ah,40h
	mov cx,offmem			;Number of bytes.
	mov ds,segmem
	xor dx,dx				;DS:DX -> Buffer.
	int 21h 				;Write, using handle

	mov ah,3eh
	int 21h 				;Close file handle

	pop si
	pop di
	pop es
	pop ds
	pop dx
	pop cx
	pop bx
	pop ax
	ret
grabar endp

;**************************************;
;		savescr 					   ;
;**************************************;
savescr proc near
	push ds
	push es
	push di
	push si

	push cs 					;Es:di	-> Destination
	pop es
	lea di,area1
	mov ax,addr
	mov ds,ax					;Ds:Si	-> Source
	xor si,si
	mov cx,2000 				;salvar info
	cld
	rep movsw

	mov ah,3
	xor bh,bh					;Number of page
	int 10h 					;Read Cursor position
    mov cs:crspos,dx            ; Posicion
    mov cs:crsize,cx            ; Tama�o
	pop si
	pop di
	pop es
	pop ds
	ret
savescr endp

;**************************************;
;		restscr 					   ;
;**************************************;
restscr proc near
	push ds
	push es
	push di
	push si

	push cs 					;ds:si	-> Source
	pop ds
	lea si,area1
	mov ax,cs:addr
	mov es,ax					;es:di	-> Destination
	xor di,di
	mov cx,2000 				;salvar info
	cld
	rep movsw

	mov ah,2
	xor bh,bh
	mov dx,cs:crspos
	int 10h 					;Set cursor position

    mov ah,1
    mov cx,cs:crsize
    int 10h

	pop si
	pop di
	pop es
	pop ds
	ret
restscr endp


;********** DATOS *************

erow	db 0			;coordenadas
ecol	db 0
escan	dw 0			;valor de tecla pulsada
hican   db 0            ;Hi de escan
locan   db 0            ;Low de escan
flaga   db 0            ;flag
flagins db 0            ;Default = no insertar
addr    dw 0            ;segmento de color o blanco y negro
segmem	dw 0			;Segmento de alloc mem.
offmem	dw 0			;Offset de alloc mem.
area1	dw 2000 dup (0) ;Infor de la pantalla
crspos	dw 0			;Poscicion del cursor
crsize  dw 0            ;Tama�o del cursor
filehand dw 0           ;Handle del archivo.

nombre	db 'texto.txt',0

memerror db'No hay suficiente memoria. Se necesitan 64k libres.',13,10,'$'

titul  label byte
	db'Linea: '
row db'000    Columna: '
col db'000    � '
iin db'    �                            - VEdit v0.10 -'

insmsg db'Ins'
supmsg db'   '

endoffile db 255,0
endofmsg    db 13,10,'<*** Fin de Archivo ***>'

;Stack
	dw 256 dup (?)

ende label byte

ruti	endp
code	ends
		end ruti

