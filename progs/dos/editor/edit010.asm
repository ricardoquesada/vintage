;name: EDIT
;version: 0.02
;revision:
;start  date: 23/08/94
;finish date:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    PROC FAR
        assume cs:code,ds:code,ss:code,es:code

;defines

ESQ equ 011Bh           ;equivale a ESC, no poner ESC con tasm 5.0
F1  equ 3b00h
F2  equ 3c00h
F3  equ 3d00h
F4  equ 3e00h
F5  equ 3f00h
F6  equ 4000h
F7  equ 4100h
F8  equ 4200h
F9  equ 4300h
F10 equ 4400h
F11 equ 8500h
F12 equ 8600h
AltF1 = 6800h
AltF3 = 6a00h
AltF11 =8b00h
AltF12 =8c00h
CtrlF3 =6000h
CtrlF1 =5e00h
_UP   = 4800h
_LEFT = 4b00h
_RIGHT= 4d00h
_DOWN = 5000h
SPACE = 3920h
BACKS = 0e08h
TAB   = 0f09h
UP    = 48e0h
RIGHT = 4de0h
DOWN  = 50e0h
LEFT  = 4be0h
INTRO = 1c0dh

MAX_ARR = 5
MAX_ABJ = 20
MAX_IZQ = 5
MAX_DER = 70

start:
        call monocolor          ;es mono o color
        call clrscr             ;borrar pantalla
        call drawbox            ;dibujar caja
        call tipear             ;llamar al editor

        mov ah,4ch              ;terminar
        int 21h


;*******************************;
;*******************************;
;           tipear              ;
;*******************************;
;*******************************;

tipear proc near

        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds
        push es

        push cs
        pop ds

        mov flaga,0

        mov ecol,MAX_IZQ
        mov erow,MAX_ARR

tipear_loop:
        call irpos
        call getchar2

        mov flaga,0             ;Flag usado tambien por GetChar
        mov ax,escan            ;
        cmp al,0                ;
        je flagggon             ;
        cmp al,0e0h             ;    detecta si se apreto tecla especial
        jne flagggoff           ;
flagggon:                       ;
        mov flaga,1             ;
flagggoff:
        cmp escan,ESQ
        je endtipear
        cmp escan,AltF1
        je endtipear
        cmp escan,F1
        je endtipear
        cmp escan,UP
        je goarr
        cmp escan,DOWN
        je goaba
        cmp escan,LEFT
        je goizq
        cmp escan,RIGHT
        je goder
        cmp escan,BACKS
        je bspace
        cmp escan,TAB
        je gotab
        cmp escan,INTRO
        je genter
        cmp escan,CtrlF1
        je clearwin

;                       ;cualquier otra tecla la escribe
        cmp flaga,1
        jne acapibe     ;tecla especial, no mostrar ir al comienzo
        jmp tipear_loop
acapibe:
        mov ax,escan
        mov ah,0ah
        mov cx,1
        xor bh,bh               ;pagina 0
        int 10h
        call go_der
        jmp tipear_loop

endtipear: jmp end_tipear
goarr:  call go_arr
        jmp tipear_loop
goaba:  call go_aba
        jmp tipear_loop
goizq:  call go_izq
        jmp tipear_loop
goder:  call go_der
        jmp tipear_loop
bspace: call b_space
        jmp tipear_loop
gotab:  call go_tab
        jmp tipear_loop
genter: call g_enter
        jmp tipear_loop
clearwin: call clear_win
        jmp tipear_loop

go_arr:
        dec erow
        cmp erow,MAX_ARR             ;te pasastes
        jae go_arr_
        mov erow,MAX_ABJ             ;principio
go_arr_:
        ret
go_izq:
        dec ecol
        cmp ecol,MAX_IZQ
        jae go_izq_
        mov ecol,MAX_DER
        jmp short go_arr
go_izq_:
        ret
go_aba:
        inc erow
        cmp erow,MAX_ABJ
        jbe go_aba_
        mov erow,MAX_ARR             ;principio
go_aba_:
        ret
go_der:
        inc ecol
        cmp ecol,MAX_DER
        jbe go_der_
        mov ecol,MAX_IZQ
        jmp short go_aba
go_der_:
        ret
b_space:
        call go_izq
        call irpos
        mov ah,0ah
        mov al,32               ;espacio
        mov cx,1
        xor bh,bh               ;pagina 0
        int 10h
        ret

go_tab:
        push cx
        mov cl,8
again_tab:
        call go_der
        dec cl
        or cl,cl
        jne again_tab
        pop cx
        ret

g_enter:
        mov ecol,MAX_IZQ
        jmp go_aba

clear_win:
        push ds
        push si

        mov cx,400
        mov ax,addr
        mov ds,ax
        mov si,0be0h
win_loop:
        mov byte ptr [si],20h
        add si,2
        dec cx
        or cx,cx
        jne win_loop

        pop si
        pop ds
        ret

end_tipear:                     ;******************************* TIPEAR END

        pop es
        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax

        ret

tipear endp

;******************************;
;       getchar2               ;
;******************************;

getchar2 proc near

        push ax
        push ds

        push cs
        pop ds

        mov ah,10h
        int 16h                 ;Keaboard Read

        mov escan,ax

        pop ds
        pop ax

        ret

getchar2 endp

;*******************************;
;       irpos                   ;
;*******************************;

irpos proc near

        push ax
        push bx
        push dx
        push ds

        push cs
        pop ds

        mov ah,2
        xor bh,bh               ;pagina 0
        mov dh,erow
        mov dl,ecol
        int 10h                 ;posciciona el cursor

        pop ds
        pop dx
        pop bx
        pop ax

        ret

irpos endp

;***********************;
;       clrscr          ;
;***********************;

clrscr proc near

        mov ah,7
        mov al,0
        mov bh,07h
        mov ch,0
        mov cl,0
        mov dh,25
        mov dl,80
        int 10h                 ;scroll window

        mov ax,500h
        int 10h                 ;Select active display page

        mov ah,0Fh
        int 10h                 ;Get video mode

        mov ah,3                ;es el numero 3
        cmp al,ah
        je loc_3
        mov al,ah
        xor ah,ah
        int 10h                 ;no entonces lo pone

loc_3:
        mov ah,2
        mov bh,0
        xor dx,dx
        int 10h                 ;set cursos position

        ret

clrscr  endp

;***************************************;
;       MONOCOLOR                       ;
;***************************************;
monocolor proc near

        mov addr,0b800h
        mov ax,0040h
        mov es,ax
        mov al,30h
        and al,es:[10h]
        cmp al,30h
        jne sig
        mov addr,0b000h
sig:
        ret
monocolor endp

;**************************************;
;       DRAWBOX                        ;
;**************************************;
drawbox proc near
        mov erow,MAX_ARR-1
        mov ecol,MAX_IZQ-1
        call irpos
        mov al,'�'
        call putchar
volverabox:
        inc ecol
        call irpos
        mov al,'�'
        call putchar                    ;write a char at cursor position
        cmp ecol,MAX_DER
        jbe volverabox
        mov al,'�'
        call putchar

        mov erow,MAX_ABJ+1
        mov ecol,MAX_IZQ-1
        call irpos
        mov al,'�'
        call putchar
volverabox2:
        inc ecol
        call irpos
        mov al,'�'
        call putchar
        cmp ecol,MAX_DER
        jbe volverabox2
        mov al,'�'
        call putchar

        mov erow,MAX_ARR-1
        mov ecol,MAX_IZQ-1
volverabox3:
        inc erow
        call irpos
        mov al,'�'
        call putchar                    ;write a char at cursor position
        cmp erow,MAX_ABJ
        jb volverabox3

        mov erow,MAX_ARR-1
        mov ecol,MAX_DER+1
volverabox4:
        inc erow
        call irpos
        mov al,'�'
        call putchar
        cmp erow,MAX_ABJ
        jb volverabox4

        ret

drawbox endp

;**************************************;
;       putchar                        ;
;**************************************;
putchar proc near
        mov ah,0ah
        xor bh,bh
        mov cx,1
        int 10h
        ret
putchar endp

;********** DATOS *************

erow    db 0            ;coordenadas
ecol    db 0
escan   dw 0            ;valor de tecla pulsada
flaga   db 0            ;flag
addr    dw 0            ;segmento de color o blanco y negro
ruti    endp
code    ends
        end ruti

