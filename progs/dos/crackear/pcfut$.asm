;======================================;
;                                      ;
;           PC F�tbol Crack            ;
;               by Riq                 ;
;                                      ;
;                                      ;
;    TSR that cracks PC F�tbol v3.4    ;
;                                      ;
;======================================;
;                                      ;
;      Start: 8-7-95                   ;
;      Version: 1.00                   ;
;      Revision:                       ;
;      Finish:                         ;
;                                      ;
;======================================;

MODEL TINY
P286
LOCALS

;======================================; DEFINES
TRUE        = 1
FALSE       = 0
THISVERSION = 0100h

;======================================; MACROS

;======================================; CODE
CODESEG
       STARTUPCODE

       mov  cx,FINAL-FROMHERE
       lea  si,FROMHERE
@@here:
       xor  byte ptr [si],0f1h
       inc  si
       loop  @@here
       ret

       Jmp  Main_Program

FROMHERE    EQU THIS BYTE
;======================================; NEW 16H
New16H PROC
       cmp  ax,'PC'
       jne  @@here
       cmp  bx,'F�'
       je   @@AmIResident

@@here:
       cmp  ax,100h
       jne  @@end
       cmp  si,5eceh
       jne  @@end
       cmp  di,5eech
       jne  @@end
       cmp  bp,0f60h
       jne  @@end

       pushf
       pusha
       push ds
       push es

       push cs
       pop  ds                         ; Sour Seg
       lea  si,NEWVAL                  ; Sour Off
       mov  ax,ss:[bp+36h]
       mov  es,ax                      ; Dest Seg
       mov  di,3c7h                    ; Dest Off
       cld
       mov  cx,3
rep    movsb                           ; Prog crackeado!

       pop  es
       pop  ds
       popa
       popf

@@end:
       Jmp  cs:[OLD16h]                ; Original int 21

@@AmIResident:
       xchg bx,ax
       iret

New16h ENDP

;======================================; TSR DATA
OLD16H DD 0                            ; Original interrupt 16h

NEWVAL DB   75h,48h,90h
XORMSG LABEL BYTE
INCLUDE     XOR.MSG
LENXORMSG EQU $-XORMSG

;======================================;
;      Main_Program                    ;
;======================================;
Main_Program PROC
       cld
       mov  ah,xormsg
       lea  di,xormsg+1
       mov  cx,lenxormsg-1
@@here:
       xor  [di],ah
       inc  di
       loop @@here

       lea  dx,xormsg+1
       mov  ah,9
       int  21h

       mov  ax,'PC'
       mov  bx,'F�'
       int  16h                        ; Am i installed ?
       cmp  ax,'F�'
       jne  Install
       cmp  bx,'PC'
       jne  Install                    ; If not Installed

       mov  ah,4ch                     ; Yes, i am installed, so finish.
       int  21h                        ; Finish Please.

Install:
       mov  ax,3516h                   ; Old int 16h
       Int  21h
       lea  si,OLD16h
       mov  [ si ],bx
       mov  [ si+2 ],es

       push cs
       pop  ds
       lea  dx,NEW16h                  ; New int 16h
       mov  ax,2516h
       int  21h

       lea  dx,Main_Program
       inc  dx
       int  27h                        ; TSR

ENDP

FINAL  EQU THIS BYTE

END

