#include <graphics.h>
#include <stdio.h>
#include <dos.h>
#include <string.h>
#include <conio.h>
#include <process.h>

struct ty_icon{
	int	estado;
	int x,y,x1,y1;
	char *texto;
} icon[10];

void wbox		    (int x, int y, int x1, int y1,char *titulo);
void lineas_hv		(int x, int y, int x1, int y1);
void barra_titulo 	(int x, int y, int x1, char *titulo);
void pintaborde		(int x, int y, int color);
void mouse_on		(void);
void mouse_off		(void);
void botonr (int x, int y, int x1, int y1, char *texto);
void botonp (int x, int y, int x1, int y1, char *texto);
//--------------------------------------------------------------------//
int main(void)
{
   int i;
   int gdriver = DETECT, gmode, errorcode;
   initgraph(&gdriver, &gmode, "c:\\bc\\bgi\\ibm8514.bgi");
   errorcode = graphresult();
   if (errorcode != grOk)
   {
	  printf("Error Grafico: %s\n", grapherrormsg(errorcode));
	  printf("Presione cualquier Tecla:");
	  getch();
	  exit(1); 					// terminar con codigo de error
   }

   cleardevice();					//color fondo NEGRO, borro pantalla.

   botonr ( 30,60,120,90,"Boton-1");
   botonr (130,60,220,90,"Boton-2");
   botonr (230,60,320,90,"boton-3");
   botonr (330,60,420,90,"Boton-4");
   botonr (430,60,520,90,"Boton-5");

   mouse_on();

   botonr (130,100,420,125,"Salir");
   wbox (30 ,135,270,450,"Menu-001");
   wbox (280,135,520,450,"Menu-002");
   wbox (1,1,130,120,"M");
   wbox (1,130,130,220,"asdasdM");
   wbox (60,60,190,180,"Varios");
   wbox (400,12,510,310,"4");
   getch();
   wbox (150,150,500,300,"Menu-1024");
   botonr (100,220,360,240,"Exit");
   for (i=1; i<=9; ++i)
   {

	botonr (10,100+i*20,50+i*10,200+i*15,"Exit");
	delay(200);
	botonp (10,100+i*20,50+i*10,200+i*15,"Exit");
	if (getch()==27)			//Escape
		break;
   }

   closegraph();
   return 0;
}

void wbox(int x, int y, int x1, int y1, char *titulo)
{
//guardar attributos de todo
	mouse_off();
	setfillstyle(SOLID_FILL, LIGHTGRAY);
	bar(x+1,y+1,x1-1,y1-1);
	setcolor(BLACK);
	setlinestyle(SOLID_LINE,1,1);
	rectangle(x,y,x1,y1);        	    //rectangulo de afuera
	rectangle(x+3,y+3,x1-3,y1-3);       //rectangulo de adentro

	pintaborde(x+1,y+1,LIGHTGRAY);				//tercer arg color
	lineas_hv(x,y,x1,y1);

	barra_titulo(x+3,y+3,x1-3, titulo);
	setfillstyle(SOLID_FILL,WHITE);		//lleno de blanco el interior del box
	floodfill(x+4,y+24,BLACK);
	mouse_on();
//retornar attributios de todo
}
//***********************************************************************
void barra_titulo (int x, int y, int x1, char *titulo)		//falta 3 parametro titulo
		//no hace falta desplazarse x ni y.
{
void cuadradito_close(int x, int y);
	int medio=x+ (x1-x)/2 - strlen(titulo)*8/2;

	cuadradito_close(x,y);

	rectangle (x,y,x1,y+20);				//linea separadora del titulo
	setfillstyle(SOLID_FILL,BLUE);
	floodfill(x+21,y+1,BLACK);
	setcolor(WHITE);
	outtextxy(medio,y+6,titulo);

}
//***********************************************************************
void lineas_hv(int x,int y, int x1, int y1)
{
	line(x ,y+23 ,x+3 ,y+23);			//---- horizontales
	line(x1,y+23 ,x1-3,y+23);
	line(x ,y1-23,x+3 ,y1-23);
	line(x1,y1-23,x1-3,y1-23);

	line(x+23,y ,x+23,y+3);             // |||| verticales
	line(x+23,y1,x+23,y1-3);
	line(x1-23,y ,x1-23,y+3);
	line(x1-23,y1,x1-23,y1-3);
}
//***********************************************************************
void pintaborde(int x, int y, int color)
{
	setfillstyle(SOLID_FILL,color);
	floodfill(x,y,BLACK);
}
//***********************************************************************
void cuadradito_close(int x, int y)
{
	int color_ant=getcolor();

	line (x+20,y,x+20,y+20);			   	//linea vertical rectangulito
	rectangle (x+6,y+8,x+14,y+10);			//rectangulito close
	//setcolorblack
	setfillstyle(SOLID_FILL,WHITE);         //
	floodfill(x+7,y+9,BLACK);
	setcolor(DARKGRAY);
	moveto(x+7,y+11);                       //me muevo abajo izquierda
	lineto(x+15,y+11);						//dibujo
	lineto(x+15,y+9);                       //la sombra en DARKGRAY
	setcolor(color_ant);
}
//***********************************************************************
void mouse_on(void)
{
  union	REGS regs;

  regs.x.ax=1;
  int86(0x33,&regs,&regs);
}

//***********************************************************************
void mouse_off(void)
{
  union	REGS regs;

  regs.x.ax=2;
  int86(0x33,&regs,&regs);
}
//***********************************************************************
void botonr (int x, int y, int x1, int y1, char *texto)  	// falta  titulo
{
	int color_ant=getcolor();
	int mediox	 =x-2+(x1-x)/2 - strlen(texto)*8/2;
	int medioy	 =y+ (y1-y-8)/2+1;
	int userpat  =1+4+16+64+256+1024+4096+16384;	//21845=01010101 01010101

	setlinestyle(SOLID_LINE, userpat, 2);	   		//el 2 es puntapincel
	setcolor(BLACK);
	rectangle(x,y,x1,y1);
	rectangle(++x,y-1,--x1,y1+1);

	setfillstyle(SOLID_FILL,LIGHTGRAY);
	bar (++x,++y,--x1,--y1);					//linea separadora del titulo

	setlinestyle(SOLID_LINE, userpat, 1);
	setcolor(WHITE);
	line(x  ,y  ,x1  ,y   );
	line(x  ,y+1,x1-1,y+1 );
	line(x  ,y  ,x   ,y1  );
	line(x+1,y  ,x+1 ,y1-1);

	setcolor(DARKGRAY);
	line(x1  ,y   ,x1  ,y1);
	line(x1-1,y+1 ,x1-1,y1);
	line(x1	 ,y1  ,x   ,y1);
	line(x1-1,y1-1,x+1 ,y1-1);

	setcolor(BLACK);
	setlinestyle(USERBIT_LINE, userpat, 1);
	rectangle((mediox-4),medioy-3,mediox+strlen(texto)*8+7,medioy+9);
	outtextxy(mediox+3,medioy,texto);

	setcolor(color_ant);
}

//***********************************************************************
void botonp (int x, int y, int x1, int y1, char *texto)  	// falta  titulo
{
	int color_ant=getcolor();
	int mediox	 =x-2+(x1-x)/2 - strlen(texto)*8/2;
	int medioy	 =y+ (y1-y-8)/2+1;
	int userpat  =1+4+16+64+256+1024+4096+16384;	//21845=01010101 01010101

	setlinestyle(SOLID_LINE, userpat, 2);	   		//el 2 es puntapincel
	setcolor(BLACK);
	rectangle(x,y,x1,y1);
	rectangle(++x,y-1,--x1,y1+1);

	setfillstyle(SOLID_FILL,LIGHTGRAY);
	bar (++x,++y,--x1,--y1);					//linea separadora del titulo

	setlinestyle(SOLID_LINE, userpat, 1);
	setcolor(DARKGRAY);
	line(x  ,y  ,x1  ,y   );
	line(x  ,y+1,x1-1,y+1 );
	line(x  ,y  ,x   ,y1  );
	line(x+1,y  ,x+1 ,y1-1);

	setcolor(WHITE);
	line(x1  ,y   ,x1  ,y1);
	line(x1-1,y+1 ,x1-1,y1);
	line(x1	 ,y1  ,x   ,y1);
	line(x1-1,y1-1,x+1 ,y1-1);

	++mediox;++medioy;
	setcolor(BLACK);
	setlinestyle(USERBIT_LINE, userpat, 1);
	rectangle((mediox-4),medioy-3,mediox+strlen(texto)*8+7,medioy+9);
	outtextxy(mediox+3,medioy,texto);

	setcolor(color_ant);
}


