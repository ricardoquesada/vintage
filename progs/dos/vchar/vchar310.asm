;name: VCHAR
;version: 3.10
;revision:
;start  date: 1/10/94
;finish date:
;revsion dat:
;author: Ricardo Quesada

code    segment public byte 'code'
org     100h
ruti    proc far
        assume cs:code,ds:code,ss:code,es:code


;*****************************;
;       definiciones          ;
;*****************************;

FILETIME  = 3 * 2048 + 10 * 32
FILEDATE  =14 * 512 + 10 * 32 + 4
XOR_VALUE equ 178 xor 1
CARG_SIZE equ findecargador - cargador
PROG_SIZE equ final - inicio
DIFER     equ ascname - fname
DIFER2    equ asmname - fname
MODOX     equ 35
MODOY     equ 8

;scancodes
ESQ equ 011Bh           ;equivale a ESC, no poner ESC con tasm 5.0
TECLA1 =0231h
TECLA2 =0332h
F1  equ 3b00h
F2  equ 3c00h
F3  equ 3d00h
F4  equ 3e00h
F5  equ 3f00h
F6  equ 4000h
F7  equ 4100h
F8  equ 4200h
F9  equ 4300h
F10 equ 4400h
F11 equ 8500h
F12 equ 8600h
ShiftF1=5400h
AltF1 = 6800h
AltF3 = 6a00h
AltF5 = 6c00h
AltF6 = 6d00h
AltF9 = 7000h
AltF10 =7100h
AltF11 =8b00h
AltF12 =8c00h
CtrlF3 =6000h
CtrlF1 =5e00h
_UP   = 4800h
_LEFT = 4b00h
_RIGHT= 4d00h
_DOWN = 5000h
SPACE = 3920h
BACKS = 0e08h
TAB   = 0f09h
UP    = 48e0h
RIGHT = 4de0h
DOWN  = 50e0h
LEFT  = 4be0h
INTRO = 1c0dh

ATCOLOR=08h

;************* START ********************;
start:
        jmp short copyright
        db 'VChar NI 3.10 (c) Ricardo Quesada 1994.'

copyright:

        call xorear
        int 20h

inicio:

;******* MANAGE MEMORY **********

        mov ah,4ah
        mov bx,offset final
        add bx,15
        shr bx,4
        inc bx
        int 21h                 ;*** Change memory size ***

;******************************:
; ***   display cabecera   *** ;
;******************************;

        mov ah,9
        mov dx,offset head_msg
        int 21h

;*********************************
;       get command line
;*********************************

        cld
        mov si,81h
alopa:  lodsb
        cmp al,13               ;si es intro fin
        je apartir
        cmp al,20h              ;si es espacio de nuevo
        je alopa
lulu:   cmp al,'/'
        je opcion
        cmp al,'-'
        je opcion

newname:
        xor cl,cl
        lea bx,fname            ;nombre del .com
buscanombre:
        cmp al,13
        je endname
        cmp al,20h
        je endname
        cmp al,'.'
        je endname
        cmp al,'/'
        je endname
        mov [bx],al
        mov [bx + DIFER],al
        mov [bx + DIFER2],al
        lodsb
        inc bx
        inc cl
        cmp cl,8                ;un nombre max (12345678.012)
        jb buscanombre

endname:
        cmp al,13
        je apartir
        jne alopa
opcion:
        lodsb
        cmp al,13
        je apartir

        and al,0dfh             ;convierte a todos a mayusculas

        cmp al,'S'
        je sound_
        cmp al,15
        je registr
        cmp al,'A'
        je antivir
        cmp al,'M'               ;new on version 3.00 rev a 24.9.94
        je mouse_
        jmp short display_help

registr:
        jmp registracion        ;saltar a registrar el programa
antivir:
        jmp antivirus           ;salir a antivirus
mouse_:
        jmp mouse
sound_:
        jmp soundd

display_help:
        mov ah,9
        lea dx,help_msg
        int 21h

        mov ax,4c10h            ;error level 10h (help)
        int 21h                 ;fin del prog

apartir:
        call isvga
        cmp al,1ah
        je esvga

        mov ah,9
        lea dx,vga_msg
        int 21h
        mov ax,4c01h            ;Error code 1. Finish - VGA wasnt found .
        int 21h

esvga:
        call ansiplus            ;Detecta si esta el Ansiplus cargado.

;***********     reservar memoria   ***************  new on 2.79�
        mov ah,48h
        mov bx,81h              ;2k
        int 21h
        mov macro_seg,ax

        mov ah,48h
        mov bx,200h             ;8192 bytes
        int 21h
        mov segme50,ax          ;

        mov ah,48h
        mov bx,200h
        int 21h
        mov segme25,ax          ;allocate memory for 2 x 8192 bytes
        jnc allocaOk

        mov ah,9
        lea dx,alloc_error
        int 21h
        mov ax,4c01h
        int 21h

allocaOk:
        mov macro_off,0         ;
        xor si,si               ;
        mov ax,macro_seg        ;- Inicializa el puto macro
        mov es,ax               ;
        xor ax,ax               ;
        mov es:[si],ax          ;
        add si,2                ;
        mov es:[si],ax          ;

        push cs
        pop es

        call clrscr             ;borra pantalla

        mov ah,9
        mov dx,offset menu_msg  ;
        int 21h                 ;el de las teclas

        mov erow,16             ;fila
        mov ecol,7              ;columna
        call irpos              ;posciciona cursor

        mov ah,9
        mov dx,offset char_msg  ;
        int 21h                 ;el peque�o char (char ->)

        call viewfile           ;mostrar archivos(nombre,etc)

; * esto es el titulo * ;
        mov ax,1300h            ;Write Character String
        mov bl,30h              ;atributo
        mov cx,240              ;longitud
        mov dh,0                ;fila
        mov dl,0                ;columna
        mov bp,offset msg3
        int 10h

; * animacion * ;
        mov ax,1300h            ;Write Character String
        xor bh,bh               ;pagina 0
        mov bl,39h              ;atributo
        mov cx,80               ;longitud
        mov dh,18               ;fila
        mov dl,0                ;columna
        mov bp,offset msg5
        int 10h

; * todo esto muestra el mensaje de saludos de abajo de la pantalla * ;
        mov ax,1300h            ;Write Character String
        xor bh,bh               ;pagina 0
        mov bl,7fh              ;atributo
        mov cx,400              ;longitud
        mov dh,19               ;fila
        mov dl,0                ;columna
        mov bp,offset msg1
        int 10h

        mov bp,offset regmsg    ;mensaje de no registrado
        cmp flagc,1             ;Estoy registrado ?
        je saltea_1

        mov bp,offset msg4      ;mensaje de registrado
saltea_1:
        mov ax,1300h            ;Write Character String
        mov bl,30h              ;atributo
        mov cx,78               ;longitud
        mov dh,24               ;fila
        mov dl,1                ;columna
        int 10h

        call i_int1c            ;interrupcion 1ch

; * muestra la A grande en pantalla * ;

        mov inic,0              ;puede inicializar
        mov carac,'A'
        mov carr,'A'

        call infovga            ;funcion v2.60
        call verchar
        call verascii           ;funcion v1.60
        call mouse_init         ;funcion v2.50
        call dacolor            ;funcion v4.00
        call doublefont         ;funcion v3.00b

;*******************************;
;*******************************;
;       MODE CARACTER           ;
;*******************************;
;*******************************;
caracter:

        mov dh,MODOY
        mov dl,MODOX
        mov ah,2
        xor bh,bh
        int 10h
        mov dx,offset carac_msg
        mov ah,9
        int 21h

loop1:
        call getchar

        cmp scancod,ESQ
        je finale               ;fin if ESC
        cmp scancod,F1
        je editar
        cmp scancod,UP
        je charup
        cmp scancod,DOWN
        je chardown
        cmp scancod,LEFT
        je speedd
        cmp scancod,RIGHT
        je speedu
        cmp scancod,ALTF1
        je gotipear

        mov ax,scancod
        mov escan,ax            ;por compatibilidad con comun
        call comun

mostrar_char:

        cmp flaga,0
        je nomostrara

        mov ah,09h
        mov al,carac            ;el carac apretado, OK ?
        xor bh,bh
        mov bl,ATCOLOR          ;escribe con color
        mov cx,1
        int 10h                 ;write char at cursor position

        call verchar
        call verascii           ;funcion nueva a partir de la v1.60

nomostrara:
        jmp loop1

finale: jmp finalisimo
editar: jmp editar_
charup: jmp charup_
chardown: jmp chardown_
speedd: jmp speeddown
speedu: jmp speedup
gotipear: jmp tipear

;****************************** FUNCIONES ***************************;

;***************************************;
;***                                 ***;
;***    MOUSE_INIT                   ***;
;***                                 ***;
;***************************************;
mouse_init proc near

        cmp flagf,0             ;Mouse deshabilitado ?
        je endeset              ;v3.00 rev a 24/9/94

        xor ax,ax
        int 33h                 ;reset mouse driver and status
        cmp ax,0ffffh
        je mouse_set_handler
        ret                     ;mouse not installed

mouse_set_handler:

        mov ax,0ch              ;set handler interrupt
        mov cx,11               ;00001011  :left,right & movement
        push cs
        pop es                  ;ES:DX -> FAR ROUTINE
        mov dx,offset mouse_event
        int 33h

        call mouse_show

        mov ax,4
        mov cx,41 * 8           ;en el centro centro del scroller
        mov dx,10 * 8
        int 33h                 ;position mouse cursor

endeset:
        ret
mouse_init endp

;***************************************;
;***                                 ***;
;***    MOUSE_SHOW                   ***;
;***                                 ***;
;***************************************;
mouse_show proc near
        mov ax,1h       ;
        int 33h
        ret             ;show mouse cursor
mouse_show endp

;***************************************;
;***                                 ***;
;***    MOUSE_HIDE                   ***;
;***                                 ***;
;***************************************;
mouse_hide proc near
        mov ax,2h       ;
        int 33h
        ret             ;hide mouse cursor

mouse_hide endp

;***************************************;
;***                                 ***;
;***    MOUSE_EVENT                  ***;
;***                                 ***;
;***************************************;

mouse_event proc far

        cmp cs:macroW,1             ;si se esta ejecutando un macro
        je ende                     ;que no funcione el mouse

        cmp cs:active,0             ;Call still not finished?
        jne ende                    ;NO --> Do not exit call

        mov cs:active,1             ;No more calls

        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds
        push es
        push bp
        pushf

        push cs
        pop ds

        call mcoordxy           ;actualiza las coordenadas del XY

        call mouse_what

        popf
        pop bp
        pop es
        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax

        mov cs:active,0          ;Re-enable call

ende:   ret

mouse_event endp

;***************************************;
;***                                 ***;
;***    MOUSE_WHAT                   ***;
;***                                 ***;
;***************************************;
mouse_what proc near

        mov bx,mouseb           ;Button pressed ?
        and bl,1
        or bl,bl                ;new on v3.00b
        je nobuttonl2           ;button left button

        call mouse_ediscreen    ;new on v3.00b

nobuttonl2:
        mov ah,3
        xor bh,bh
        int 10h                 ;get cursor position
        push dx

        mov bx,mouseb           ;Button pressed ?
        and bl,1
        or bl,bl
        je nobuttonl            ;button left button

        mov mousechar,178

        call mouse_ediscreen2   ;new on v4.00
        call mouse_bigscreen
        call mouse_sup
        call mouse_sdown
        call mouse_sleft
        call mouse_sright
        call mouse_rotate
        call mouse_flip
        call mouse_invert
        call mouse_clean
        call mouse_trota
        call mouse_anim
        call mouse_copy
        call mouse_paste
        call mouse_89
        call mouse_charup
        call mouse_chardw
        call mouse_salvar
        call mouse_shiftF1
        call mouse_ctrlF1
        call mouse_F3
        call mouse_CtrlF3
        call mouse_AltF3
        call mouse_AltF11
        call mouse_AltF12
        call mouse_F12
        call mouse_ESC              ;new on v3.00b
        call mouse_grid             ;new on v4.00
nobuttonl:
        mov bx,mouseb
        and bl,2
        or bl,bl
        je nobuttonr

        mov mousechar,1
        call mouse_bigscreen

nobuttonr:
        mov bx,mouseb
        or bx,bx
        jne nomovement

        call mmouse_sup
        call mmouse_sdown
        call mmouse_sleft
        call mmouse_sright
        call mmouse_rotate
        call mmouse_flip
        call mmouse_invert
        call mmouse_clean
        call mmouse_trota
        call mmouse_anim
        call mmouse_copy
        call mmouse_paste
        call mmouse_89
        call mmouse_charup
        call mmouse_chardw
        call mmouse_salvar
        call mmouse_shiftF1
        call mmouse_ctrlF1
        call mmouse_F3
        call mmouse_CtrlF3
        call mmouse_AltF3
        call mmouse_AltF11
        call mmouse_AltF12
        call mmouse_F12
        call mmouse_ESC         ;new on v3.00b
        call mmouse_grid        ;new on v4.00
nomovement:
        pop dx
        xor bh,bh
        mov ah,2
        int 10h                 ;set cursor position

        ret

mouse_what endp

;***************************************;
;***    MOUSE_BIGSCREEN              ***;
;***************************************;
mouse_bigscreen proc near

        call mcoordxy

        mov mousex1,0
        mov mousex2,7
        mov mousey1,0
        mov ah,totrow1
        mov mousey2,ah              ;total de rows
        call mouse_xy
        jc end_bigscreen

        mov ax,7
        xor cx,cx
        mov dx,7*8
        int 33h                 ;define horizontal movement (0-7)
        inc ax
        mov dx,totrow2          ;rows ?
        int 33h                 ;define vertical movement (0-15)

_bigscreen:
        call mcoordxy           ;para saber status del button
        call mouse_hide

        mov dh,mousey
        mov dl,mousex
        mov ah,2
        xor bh,bh
        int 10h                 ;set cursor position

        mov ah,8
        xor bh,bh
        int 10h                 ;read char and attributte

        mov al,mousechar
        mov bl,ah
        mov cx,1
        mov ah,9
        int 10h                 ;write char and attribute

        call mouse_show

        mov bx,mouseb
        and bl,00000011         ;left or right
        or bl,bl
        jne _bigscreen

        mov ax,7
        xor cx,cx
        mov dx,79*8
        int 33h                 ;define horizontal movement (0-79)
        inc ax
        mov dx,24*8
        int 33h                 ;define horizontal movement (0-24)

end_bigscreen:
        ret

mouse_bigscreen endp

;***************************************; NEW ON V3.00b
;***    MOUSE_EDISCREEN              ***;
;***************************************;
mouse_ediscreen proc near
        call mcoordxy
        mov mousex1,0
        mov mousex2,79
        mov mousey1,19
        mov mousey2,23              ;total de rows
        call mouse_xy
        jc end_ediscreen

        cmp ftipear,1
        je _ediscreen
        jne end_ediscreen           ;
_ediscreen:
        mov dh,mousey
        mov dl,mousex
        mov row2,dh
        mov col2,dl
        call irpos2
end_ediscreen:
        ret

mouse_ediscreen endp

;***************************************; NEW ON V4.00
;***    MOUSE_EDISCREEN              ***;
;***************************************;
mouse_ediscreen2 proc near
        call mcoordxy
        mov mousex1,0
        mov mousex2,79
        mov mousey1,19
        mov mousey2,23              ;total de rows
        call mouse_xy
        jc end_ediscreen2

        mov dh,mousey
        mov dl,mousex
        mov ah,2
        xor bh,bh
        int 10h                     ;set cursor position

        mov ah,8
        xor bh,bh
        int 10h                     ;Read char & attr at cursor

        mov ah,2
        mov dh,16
        mov dl,7
        xor bh,bh
        int 10h                 ;set cursor position

        mov ah,0ah
        xor bh,bh
        mov cx,1
        int 10h                 ;write char at cursor position.

        mov carac,al
        mov carr,al
        call verchar
        call verascii

end_ediscreen2:
        ret

mouse_ediscreen2 endp


;***************************************;
;***    MOUSE_SUP                    ***;
;***************************************;
mouse_sup proc near

        mov mousex1,40
        mov mousex2,42
        mov mousey1,9
        mov mousey2,9
        call mouse_xy
        jc m_end_sup

        mov mcolor,47h
        call pintar

        call gup
        mov bl,4
        call pausa
        call leftbu
        jnc __sup

_sup:
        call gup
        mov bl,1
        call pausa
        call leftbu
        jc _sup

__sup:
        call mrestorexy                 ;restaura la pos del mouse
        call mmouse_sup                 ;restaurar color

m_end_sup:
        ret

mouse_sup endp

;***************************************;
;***    MOUSE_SDOWN                  ***;
;***************************************;
mouse_sdown proc near

        mov mousex1,40
        mov mousex2,42
        mov mousey1,11
        mov mousey2,11
        call mouse_xy
        jc m_end_sdown

        mov mcolor,47h
        call pintar

        call gdown
        mov bl,4
        call pausa
        call leftbu
        jnc __sdown

_sdown:
        call gdown
        mov bl,1
        call pausa
        call leftbu
        jc _sdown

__sdown:
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_sdown

m_end_sdown:
        ret

mouse_sdown endp

;***************************************;
;***    MOUSE_SRIGHT                 ***;
;***************************************;
mouse_sright proc near

        mov mousex1,42
        mov mousex2,44
        mov mousey1,10
        mov mousey2,10
        call mouse_xy
        jc m_end_sright

        mov mcolor,47h
        call pintar

        call gright
        mov bl,4
        call pausa
        call leftbu
        jnc __sright

_sright:
        call gright
        mov bl,1
        call pausa
        call leftbu
        jc _sright

__sright:
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_sright

m_end_sright:
        ret

mouse_sright endp

;***************************************;
;***    MOUSE_SLEFT                  ***;
;***************************************;
mouse_sleft proc near

        mov mousex1,38
        mov mousex2,40
        mov mousey1,10
        mov mousey2,10
        call mouse_xy
        jc m_end_sleft

        mov mcolor,47h
        call pintar

        call gleft
        mov bl,4
        call pausa
        call leftbu
        jnc __sleft

_sleft:
        call gleft
        mov bl,1
        call pausa
        call leftbu
        jc _sleft

__sleft:
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_sleft

m_end_sleft:
        ret

mouse_sleft endp

;***************************************;
;***    MOUSE_ROTATE                 ***;
;***************************************;
mouse_rotate proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,9
        mov mousey2,9
        call mouse_xy

        jc m_end_rotate

        mov mcolor,47h                  ;red... pressed
        call pintar

        call rotate

        mov bl,3
        call pausa
_rotate:
        call leftbu
        jc _rotate

        call mrestorexy                 ;restaurar posciciones del mouse
        call mmouse_rotate              ;restaurar color

m_end_rotate:

        ret

mouse_rotate endp

;***************************************;
;***    MOUSE_FLIP                   ***;
;***************************************;
mouse_flip proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,10
        mov mousey2,10
        call mouse_xy

        jc m_end_flip

        mov mcolor,47h                  ;red... pressed
        call pintar

        call flip

        mov bl,3
        call pausa

loopa_2:
        call leftbu
        jc loopa_2

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_flip                ;restaurar color

m_end_flip:
        ret

mouse_flip endp

;***************************************;
;***    MOUSE_INVERT                 ***;
;***************************************;
mouse_invert proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,11
        mov mousey2,11
        call mouse_xy

        jc m_end_invert

        mov mcolor,47h                  ;red... pressed
        call pintar

        call invert

        mov bl,3
        call pausa

loopa_3:
        call leftbu
        jc loopa_3

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_invert

m_end_invert:
        ret

mouse_invert endp


;***************************************;
;***    MOUSE_CLEAN                  ***;
;***************************************;
mouse_clean proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,12
        mov mousey2,12
        call mouse_xy

        jc m_end_clean

        mov mcolor,47h                  ;red... pressed
        call pintar

        call clean

        mov bl,3
        call pausa

loopa_4:
        call leftbu
        jc loopa_4

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_clean

m_end_clean:
        ret

mouse_clean endp

;***************************************;
;***    MOUSE_TROTA                  ***;
;***************************************;
mouse_trota proc near

        mov mousex1,23
        mov mousex2,34
        mov mousey1,9
        mov mousey2,9
        call mouse_xy

        jc m_end_trota

        mov mcolor,47h                  ;red... pressed
        call pintar

        call trota

        mov bl,3
        call pausa

loopa_a:
        call leftbu
        jc loopa_a

        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_trota

m_end_trota:
        ret

mouse_trota endp

;***************************************;
;***    MOUSE_GRID                   ***;
;***************************************;
mouse_grid proc near
        mov mousex1,23
        mov mousex2,34
        mov mousey1,10
        mov mousey2,10
        call mouse_xy
        jc m_end_grid
        mov mcolor,47h                  ;red... pressed
        call pintar
        call grid
        mov bl,3
        call pausa
loopa_ag:
        call leftbu
        jc loopa_ag
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_grid
m_end_grid:
        ret
mouse_grid endp

;***************************************;
;***    MOUSE_ANIM                   ***;
;***************************************;
mouse_anim proc near

        mov mousex1,10
        mov mousex2,34
        mov mousey1,7
        mov mousey2,7
        call mouse_xy

        jc m_end_anim

        mov mcolor,47h                  ;red... pressed
        call pintar

        call toggle_anim
        call mouse_show                 ;NEW ON REVISON A V3.00 23/9/94

        mov bl,3
        call pausa

loopa_5:
        call leftbu
        jc loopa_5

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_anim

m_end_anim:
        ret

mouse_anim endp

;***************************************;
;***    MOUSE_COPY                   ***;
;***************************************;
mouse_copy proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,9
        mov mousey2,9
        call mouse_xy

        jc m_end_copy

        mov mcolor,47h                  ;red... pressed
        call pintar

        call copiar

        mov bl,3
        call pausa

loopa_6:
        call leftbu
        jc loopa_6

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_copy

m_end_copy:
        ret

mouse_copy endp


;***************************************;
;***    MOUSE_PASTE                  ***;
;***************************************;
mouse_paste proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,10
        mov mousey2,10
        call mouse_xy

        jc m_end_paste

        mov mcolor,47h                  ;red... pressed
        call pintar

        call paste

        mov bl,3
        call pausa

loopa_7:
        call leftbu
        jc loopa_7

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_paste

m_end_paste:
        ret

mouse_paste endp


;***************************************;
;***    MOUSE_89                     ***;
;***************************************;
mouse_89 proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,11
        mov mousey2,11
        call mouse_xy

        jc m_end_89

        mov mcolor,47h                  ;red... pressed
        call pintar

        call a8bit

        mov bl,3
        call pausa

loopa_8:
        call leftbu
        jc loopa_8

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_89


m_end_89:
        ret

mouse_89 endp

;***************************************;
;***    MOUSE_CHARUP                 ***;
;***************************************;
mouse_charup proc near

        mov mousex1,3
        mov mousex2,5
        mov mousey1,16
        mov mousey2,16
        call mouse_xy
        jc m_end_charup

        mov mcolor,47h
        call pintar             ;color del boton

        mov ah,2
        xor bh,bh
        mov dh,16
        mov dl,7
        int 10h                 ;set cursor position

        mov ah,08h
        xor bh,bh
        int 10h                ;get char and attribute

        inc al
        mov carac,al
        mov carr,al

        mov ah,0ah
        xor bh,bh
        mov cx,1
        int 10h                 ;write char at cursor position

        call verchar
        call verascii           ;funcion nueva a partir de la v1.60

        mov bl,6
        call pausa

        call leftbu
        jnc __charup

_charup:
        mov ah,2
        xor bh,bh
        mov dh,16
        mov dl,7
        int 10h                 ;set cursor position

        mov ah,08h
        xor bh,bh
        int 10h                ;get char and attribute

        inc al
        mov carac,al
        mov carr,al

        mov ah,0ah
        xor bh,bh
        mov cx,1
        int 10h                 ;write char at cursor position

        call verchar
        call verascii           ;funcion nueva a partir de la v1.60

        mov bl,1
        call pausa

        call leftbu
        jc _charup

__charup:
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_charup      ;restaurar color

m_end_charup:

        ret

mouse_charup endp


;***************************************;
;***    MOUSE_CHARDW                 ***;
;***************************************;
mouse_chardw proc near

        mov mousex1,0
        mov mousex2,2
        mov mousey1,16
        mov mousey2,16
        call mouse_xy
        jc m_end_charup

        mov mcolor,47h
        call pintar

        mov ah,2
        xor bh,bh
        mov dh,16
        mov dl,7
        int 10h                 ;set cursor position

        mov ah,08h
        xor bh,bh
        int 10h                ;get char and attribute

        dec al
        mov carac,al
        mov carr,al

        mov ah,0ah
        xor bh,bh
        mov cx,1
        int 10h                 ;write char at cursor position

        call verchar
        call verascii           ;funcion nueva a partir de la v1.60

        mov bl,6
        call pausa

        call leftbu
        jnc __chardw

_chardw:
        mov ah,2
        xor bh,bh
        mov dh,16
        mov dl,7
        int 10h                 ;set cursor position

        mov ah,08h
        xor bh,bh
        int 10h                ;get char and attribute

        dec al
        mov carac,al
        mov carr,al

        mov ah,0ah
        xor bh,bh
        mov cx,1
        int 10h                 ;write char at cursor position

        call verchar
        call verascii           ;funcion nueva a partir de la v1.60

        mov bl,1
        call pausa

        call leftbu
        jc _chardw

__chardw:
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_chardw

m_end_chardw:
        ret

mouse_chardw endp


;***************************************;
;***    MOUSE_SALVAR                 ***;
;***************************************;
mouse_salvar proc near

        mov mousex1,10
        mov mousex2,34
        mov mousey1,6
        mov mousey2,6
        call mouse_xy

        jc m_end_salvar

        mov mcolor,47h                  ;color red
        call pintar

        call salvar

        mov bl,3
        call pausa

leftbu1:
        call leftbu
        jc leftbu1

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_salvar


m_end_salvar:
        ret
mouse_salvar endp

;***************************************;
;***    MOUSE_SHIFTF1                ***;
;***************************************;
mouse_shiftf1 proc near
        mov mousex1,48
        mov mousex2,78
        mov mousey1,4
        mov mousey2,4
        call mouse_xy
        jc m_end_shiftf1
        mov mcolor,17h                  ;color blue
        call pintar
        call see_ascii
        mov bl,3
        call pausa
leftbu1s:
        call leftbu
        jc leftbu1s
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_shiftf1
m_end_shiftf1:
        ret
mouse_shiftf1 endp

;***************************************;
;***    MOUSE_CTRLF1                 ***;
;***************************************;
mouse_CTRLf1 proc near
        mov mousex1,48
        mov mousex2,78
        mov mousey1,5
        mov mousey2,5
        call mouse_xy
        jc m_end_ctrlf1
        mov mcolor,17h                  ;color blue
        call pintar
        call clear_win
        mov bl,3
        call pausa
leftbu1c:
        call leftbu
        jc leftbu1c
        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_ctrlf1
m_end_ctrlf1:
        ret
mouse_ctrlf1 endp


;***************************************;
;***    MOUSE_F3                     ***;
;***************************************;
mouse_f3 proc near

        mov mousex1,4
        mov mousex2,21
        mov mousey1,17
        mov mousey2,17
        call mouse_xy

        jc m_end_f3

        mov mcolor,47h                  ;red
        call pintar

        mov bl,6
        call pausa

        call save

        mov mcolor,47h
        call pintar

conchit2:
        call leftbu
        jc conchit2

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_f3

m_end_f3:
        ret
mouse_f3 endp

;***************************************;
;***    MOUSE_CtrlF3                 ***;
;***************************************;
mouse_ctrlf3 proc near

        mov mousex1,26
        mov mousex2,48
        mov mousey1,17
        mov mousey2,17

        call mouse_xy
        jc m_end_ctrlf3

        mov mcolor,47h              ;green
        call pintar

        mov bl,6
        call pausa

        call sal_asm

        mov mcolor,47h              ;red
        call pintar

conchit3:
        call leftbu
        jc conchit3

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_ctrlf3

m_end_ctrlf3:
        ret
mouse_ctrlf3 endp


;***************************************;
;***    MOUSE_AltF3                  ***;
;***************************************;
mouse_altf3 proc near

        mov mousex1,54
        mov mousex2,75
        mov mousey1,17
        mov mousey2,17
        call mouse_xy

        jc m_end_altf3

        mov mcolor,47h                  ;red
        call pintar

        mov bl,6
        call pausa

        call sal_asc

        mov mcolor,47h                  ;red
        call pintar

conchit1:
        call leftbu
        jc conchit1

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_altf3

m_end_altf3:
        ret
mouse_altf3 endp

;***************************************;
;***    MOUSE_AltF11                 ***;
;***************************************;
mouse_altf11 proc near

        mov mousex1,63
        mov mousex2,79
        mov mousey1,11
        mov mousey2,11
        call mouse_xy

        jc m_end_altf11

        mov mcolor,17h                  ;Blue when pressed
        call pintar

        call gen_table
        CALL MOUSE_SHOW                 ;REVISION A V3.00 23/9/94

        mov bl,3
        call pausa

loopa_9:
        call leftbu
        jc loopa_9

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_altf11

m_end_altf11:
        ret

mouse_altf11 endp

;***************************************;
;***    MOUSE_AltF12                 ***;
;***************************************;
mouse_altf12 proc near

        mov mousex1,63
        mov mousex2,79
        mov mousey1,12
        mov mousey2,12
        call mouse_xy

        jc m_end_altf12

        mov mcolor,17h                  ;red... pressed
        call pintar

        call resori                     ;
        call mouse_show                 ;REVISION A V3.00  23/9/94

        mov bl,3
        call pausa

loopa_10:
        call leftbu
        jc loopa_10

        call mrestorexy                 ;poscicion del org del cursor
        call mmouse_altf12

m_end_altf12:
        ret

mouse_altf12 endp

;***************************************;
;***    MOUSE_F12                    ***;   ;new on v2.60
;***************************************;
mouse_f12 proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,12
        mov mousey2,12
        call mouse_xy

        jc m_end_f12

        mov mcolor,47h                  ;red... pressed
        call pintar

        call tog250                     ;LLAMAR A FUNCION
        call mouse_show                 ;REVISION A V3.00  23/9/94

        mov bl,3
        call pausa

loopa_11:
        call leftbu
        jc loopa_11

        call mrestorexy                 ;poscicion del org del cursor

        call mmouse_f12

m_end_f12:
        ret

mouse_f12 endp

;***************************************;
;***    MOUSE_ESC                    ***;   ;new on v3.00b
;***************************************;
mouse_esc proc near

        mov mousex1,48
        mov mousex2,78
        mov mousey1,7
        mov mousey2,7
        call mouse_xy

        jc m_end_esc

        mov mcolor,47h                  ;red... pressed
        call pintar

        call finalisimo                 ;LLAMAR A FUNCION y aca se acaba

m_end_esc:
        ret

mouse_esc endp


;***************************************;
;***    mMOUSE_SUP                   ***;
;***************************************;
mmouse_sup proc near

        mov mousex1,40
        mov mousex2,42
        mov mousey1,9
        mov mousey2,9
        call mouse_xy
        jc mm_end_sup

        mov mcolor,37h
        call pintar
        jmp short mm_sup_end

mm_end_sup:
        mov mcolor,7h
        call pintar

mm_sup_end:
        ret

mmouse_sup endp

;***************************************;
;***    mMOUSE_SDOWN                 ***;
;***************************************;
mmouse_sdown proc near

        mov mousex1,40
        mov mousex2,42
        mov mousey1,11
        mov mousey2,11
        call mouse_xy
        jc mm_end_sdown

        mov mcolor,37h
        call pintar
        jmp short mm_sdown_end

mm_end_sdown:
        mov mcolor,7h
        call pintar

mm_sdown_end:
        ret

mmouse_sdown endp

;***************************************;
;***    mMOUSE_SRIGHT                ***;
;***************************************;
mmouse_sright proc near

        mov mousex1,42
        mov mousex2,44
        mov mousey1,10
        mov mousey2,10
        call mouse_xy
        jc mm_end_srig

        mov mcolor,37h
        call pintar
        jmp short mm_end_sright

mm_end_srig:
        mov mcolor,7h
        call pintar

mm_end_sright:
        ret

mmouse_sright endp

;***************************************;
;***    mMOUSE_SLEFT                 ***;
;***************************************;
mmouse_sleft proc near

        mov mousex1,38
        mov mousex2,40
        mov mousey1,10
        mov mousey2,10
        call mouse_xy
        jc mm_end_sle

        mov mcolor,37h
        call pintar
        jmp short mm_end_sleft

mm_end_sle:
        mov mcolor,7h
        call pintar

mm_end_sleft:
        ret

mmouse_sleft endp

;***************************************;
;***    mMOUSE_CHARDW                ***;
;***************************************;
mmouse_chardw proc near

        mov mousex1,0
        mov mousex2,2
        mov mousey1,16
        mov mousey2,16
        call mouse_xy
        jc mm_n_chardw

        mov mcolor,37h                  ;marron
        call pintar
        jmp short mm_end_chardw

mm_n_chardw:
        mov mcolor,7h
        call pintar

mm_end_chardw:
        ret

mmouse_chardw endp


;***************************************;
;***    mMOUSE_CHARUP                ***;
;***************************************;
mmouse_charup proc near

        mov mousex1,3
        mov mousex2,5
        mov mousey1,16
        mov mousey2,16
        call mouse_xy
        jc mm_n_charup

        mov mcolor,37h                  ;verde
        call pintar
        jmp short mm_end_charup

mm_n_charup:

        mov mcolor,7h
        call pintar

mm_end_charup:
        ret

mmouse_charup endp


;***************************************;
;***    mMOUSE_ROTATE                ***;
;***************************************;
mmouse_rotate proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,9
        mov mousey2,9
        call mouse_xy

        jc mm_end_rotate

        mov mcolor,17h
        call pintar
        jmp short mm_rotate

mm_end_rotate:
        mov mcolor,07h
        call pintar

mm_rotate:
        ret

mmouse_rotate endp

;***************************************;
;***    mMOUSE_FLIP                  ***;
;***************************************;
mmouse_flip proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,10
        mov mousey2,10
        call mouse_xy

        jc mm_end_fl
        mov mcolor,17h
        call pintar
        jmp short mm_end_flip
mm_end_fl:
        mov mcolor,7h
        call pintar
mm_end_flip:
        ret

mmouse_flip endp

;***************************************;
;***    mMOUSE_INVERT                ***;
;***************************************;
mmouse_invert proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,11
        mov mousey2,11
        call mouse_xy

        jc mm_end_in
        mov mcolor,17h
        call pintar
        jmp short mm_end_invert
mm_end_in:
        mov mcolor,7h
        call pintar

mm_end_invert:
        ret

mmouse_invert endp


;***************************************;
;***    mMOUSE_CLEAN                 ***;
;***************************************;
mmouse_clean proc near

        mov mousex1,10
        mov mousex2,22
        mov mousey1,12
        mov mousey2,12
        call mouse_xy

        jc mm_end_cl
        mov mcolor,17h
        call pintar
        jmp short mm_end_clean
mm_end_cl:
        mov mcolor,7h
        call pintar
mm_end_clean:
        ret

mmouse_clean endp

;***************************************;
;***    mMOUSE_TROTA                 ***;
;***************************************;
mmouse_trota proc near

        mov mousex1,23
        mov mousex2,34
        mov mousey1,9
        mov mousey2,9
        call mouse_xy

        jc mm_end_tr
        mov mcolor,17h
        call pintar
        jmp short mm_end_trota
mm_end_tr:
        mov mcolor,7h
        call pintar
mm_end_trota:
        ret

mmouse_trota endp

;***************************************; new on v4.00
;***    mMOUSE_GRID                  ***;
;***************************************;
mmouse_grid proc near
        mov mousex1,23
        mov mousex2,34
        mov mousey1,10
        mov mousey2,10
        call mouse_xy
        jc mm_end_gr
        mov mcolor,17h
        call pintar
        jmp short mm_end_grid
mm_end_gr:
        mov mcolor,7h
        call pintar
mm_end_grid:
        ret
mmouse_grid endp

;***************************************;
;***    mMOUSE_ANIM                  ***;
;***************************************;
mmouse_anim proc near

        mov mousex1,10
        mov mousex2,34
        mov mousey1,7
        mov mousey2,7
        call mouse_xy

        jc mm_end_an
        mov mcolor,17h
        call pintar
        jmp short mm_end_anim
mm_end_an:
        mov mcolor,7h
        call pintar
mm_end_anim:
        ret

mmouse_anim endp

;***************************************;
;***    mMOUSE_COPY                  ***;
;***************************************;
mmouse_copy proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,9
        mov mousey2,9
        call mouse_xy

        jc mm_end_co
        mov mcolor,17h
        call pintar
        jmp short mm_end_copy
mm_end_co:
        mov mcolor,7h
        call pintar
mm_end_copy:
        ret

mmouse_copy endp


;***************************************;
;***    mMOUSE_PASTE                 ***;
;***************************************;
mmouse_paste proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,10
        mov mousey2,10
        call mouse_xy

        jc mm_end_pa
        mov mcolor,17h
        call pintar
        jmp short mm_end_paste
mm_end_pa:
        mov mcolor,7h
        call pintar
mm_end_paste:
        ret

mmouse_paste endp


;***************************************;
;***    mMOUSE_89                    ***;
;***************************************;
mmouse_89 proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,11
        mov mousey2,11
        call mouse_xy

        jc mm_end_8
        mov mcolor,17h
        call pintar
        jmp short mm_end_89
mm_end_8:
        mov mcolor,7
        call pintar
mm_end_89:
        ret

mmouse_89 endp

;***************************************;
;***    mMOUSE_SALVAR                ***;
;***************************************;
mmouse_salvar proc near

        mov mousex1,10
        mov mousex2,34
        mov mousey1,6
        mov mousey2,6
        call mouse_xy

        jc mm_end_sa
        mov mcolor,20h
        call pintar
        jmp short mm_end_salvar
mm_end_sa:
        mov mcolor,7
        call pintar
mm_end_salvar:
        ret
mmouse_salvar endp

;***************************************;
;***    mMOUSE_SHIFTF1               ***;
;***************************************;
mmouse_shiftf1 proc near
        mov mousex1,48
        mov mousex2,78
        mov mousey1,4
        mov mousey2,4
        call mouse_xy
        jc mm_end_s1
        mov mcolor,47h
        call pintar
        jmp short mm_end_shiftf1
mm_end_s1:
        mov mcolor,7
        call pintar
mm_end_shiftf1:
        ret
mmouse_shiftf1 endp

;***************************************;
;***    mMOUSE_CTRLF1                ***;
;***************************************;
mmouse_ctrlf1 proc near
        mov mousex1,48
        mov mousex2,78
        mov mousey1,5
        mov mousey2,5
        call mouse_xy
        jc mm_end_c1
        mov mcolor,47h
        call pintar
        jmp short mm_end_ctrlf1
mm_end_c1:
        mov mcolor,7
        call pintar
mm_end_ctrlf1:
        ret
mmouse_ctrlf1 endp

;***************************************;
;***    mMOUSE_F3                    ***;
;***************************************;
mmouse_f3 proc near

        mov mousex1,4
        mov mousex2,21
        mov mousey1,17
        mov mousey2,17
        call mouse_xy

        jc mm_end_f3
        mov mcolor,20h                  ;red with white
        call pintar
        ret
mm_end_f3:
        mov mcolor,7h
        call pintar
        ret
mmouse_f3 endp

;***************************************;
;***    mMOUSE_CtrlF3                ***;
;***************************************;
mmouse_ctrlf3 proc near

        mov mousex1,26
        mov mousex2,48
        mov mousey1,17
        mov mousey2,17

        call mouse_xy

        jc mm_end_ctrlf3
        mov mcolor,20h
        call pintar
        ret
mm_end_ctrlf3:
        mov mcolor,7h
        call pintar
        ret
mmouse_ctrlf3 endp

;***************************************;
;***    mMOUSE_AltF3                 ***;
;***************************************;
mmouse_altf3 proc near
        mov mousex1,54
        mov mousex2,75
        mov mousey1,17
        mov mousey2,17
        call mouse_xy
        jc mm_end_altf3
        mov mcolor,20h
        call pintar
        ret
mm_end_altf3:
        mov mcolor,7
        call pintar
        ret
mmouse_altf3 endp

;***************************************;
;***    mMOUSE_AltF11                ***;
;***************************************;
mmouse_altf11 proc near

        mov mousex1,63
        mov mousex2,79
        mov mousey1,11
        mov mousey2,11
        call mouse_xy

        jc mm_end_altf11

        mov mcolor,47h                  ;PELIGRO EN ROJO
        call pintar
        ret
mm_end_altf11:
        mov mcolor,7
        call pintar
        ret

mmouse_altf11 endp

;***************************************;
;***    mMOUSE_AltF12                ***;
;***************************************;
mmouse_altf12 proc near

        mov mousex1,63
        mov mousex2,79
        mov mousey1,12
        mov mousey2,12
        call mouse_xy

        jc mm_end_altf12

        mov mcolor,47h
        call pintar
        ret
mm_end_altf12:
        mov mcolor,7
        call pintar
        ret

mmouse_altf12 endp

;***************************************;
;***    mMOUSE_F12                   ***;
;***************************************;
mmouse_f12 proc near

        mov mousex1,48
        mov mousex2,62
        mov mousey1,12
        mov mousey2,12
        call mouse_xy

        jc mm_end_f12

        mov mcolor,17h
        call pintar
        ret
mm_end_f12:
        mov mcolor,7
        call pintar
        ret

mmouse_f12 endp

;***************************************;
;***    mMOUSE_ESC                   ***; new on v3.00b
;***************************************;
mmouse_esc proc near

        mov mousex1,48
        mov mousex2,78
        mov mousey1,7
        mov mousey2,7
        call mouse_xy

        jc mm_end_esc

        mov mcolor,47h                  ;Blinking RED
        call pintar
        ret
mm_end_esc:
        mov mcolor,7
        call pintar
        ret

mmouse_esc endp



;***************************************;
;***                                 ***;
;***    MOUSE_XY                     ***;
;***                                 ***;
;***************************************;
mouse_xy proc near
        mov cl,mousex1
        cmp mousex,cl
        jb mouxy_no
        mov cl,mousex2
        cmp mousex,cl
        ja mouxy_no
        mov cl,mousey1
        cmp mousey,cl
        jb mouxy_no
        mov cl,mousey2
        cmp mousey,cl
        ja mouxy_no

        clc
        jnc mousexye

mouxy_no:
        stc
mousexye:
        ret

mouse_xy endp


;***************************************;
;***                                 ***;
;***    PINTAR                       ***;
;***                                 ***;
;***************************************;
pintar proc near

        push ax
        push bx
        push cx
        push dx
        push si
        push ds

        call mouse_hide

        xor cx,cx                       ;cx = 0
        mov bl,mcolor                   ;color del fondo

        mov al,160                      ; y * 160 = coordena y
        mul mousey1
        mov si,ax                       ;prepara para DS:SI

        mov al,mousex1
        mov cl,mousex2
        sub cl,al                       ;ah = x2 - x1
        inc cl

        shl al,1                        ;multiplica por 2 a x1
        xor ah,ah
        add si,ax                       ;si = y * 160 + x * 2
        inc si                          ;si = y * 160 + x * 2 + 1

        mov ax,0b800h
        mov ds,ax                       ;ds:si
colorcambia:
        mov [si],bl                     ;atributo del fondo
        add si,2                        ;siguiente atributo
        dec cl
        or cl,cl
        jne colorcambia

        call mouse_show

        pop ds
        pop si
        pop dx
        pop cx
        pop bx
        pop ax

        ret
pintar endp

;***************************************;
;***    PAUSA                        ***;
;***************************************;
pausa proc near
        mov cs:tiempo,bl
pausa_loop:
        cmp cs:tiempo,0
        jne pausa_loop
        ret
pausa endp

;***************************************;
;***    LEFTBU                       ***;
;***************************************;
leftbu proc near

        mov ax,5
        mov bx,7
        int 33h                         ;Return Button Press Data

        and al,1
        or al,al
        jne leftbu_ok

        clc
        ret
leftbu_ok:
        stc
        ret

leftbu endp

;***************************************;
;***    MCOORDXY                     ***;
;***************************************;
mcoordxy proc near
        mov ax,3
        int 33h                 ;actualiza las coordenas y el boton del mouse

        mov di,cx               ;Place horizontal coordinate in DI
        mov cl,3                ;Counter for coordinate number
        shr dx,cl               ;Divide DX (vertical coord.) by 8
        mov mousey,dl

        shr di,cl               ;Divide DI (horizontal coord.) by 8
        mov dx,di
        mov mousex,dl

        mov mouseb,bx           ;valor del button

        ret

mcoordxy endp

;***************************************;
;***    MRESTOREXY                   ***;
;***************************************;
mrestorexy proc near
         mov ax,4
         xor cx,cx
         xor dx,dx
         mov cl,mousex
         shl cx,3                       ; cx * 8
         mov dl,mousey
         shl dx,3                       ; dx * 8
         int 33h
         ret
mrestorexy endp

;***************************************;
;***    key_SUP                      ***;
;***************************************;
key_sup proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,40
    mov mousex2,42
    mov mousey1,9
    mov mousey2,9
    mov mcolor,37h
    call pintar
    cmp macroW,1
    je npause1
    mov bl,2
    call pausa
npause1:
    mov mcolor,7
    call pintar
    pop ds
    pop dx
    ret
key_sup endp

;***************************************;
;***    key_SDOWN                  ***;
;***************************************;
key_sdown proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,40
    mov mousex2,42
    mov mousey1,11
    mov mousey2,11
    mov mcolor,37h
    call pintar
    cmp macroW,1
    je npause2
    mov bl,2
    call pausa
npause2:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_sdown endp

;***************************************;
;***    key_sright                   ***;
;***************************************;
key_sright proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,42
    mov mousex2,44
    mov mousey1,10
    mov mousey2,10
    mov mcolor,37h
    call pintar
    cmp macroW,1
    je npause3
    mov bl,2
    call pausa
npause3:
    mov mcolor,7
    call pintar
    pop ds
    pop dx
    ret
key_sright endp

;***************************************;
;***    key_Sleft                    ***;
;***************************************;
key_sleft proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,38
    mov mousex2,40
    mov mousey1,10
    mov mousey2,10
    mov mcolor,37h
    call pintar
    cmp macroW,1
    je npause4
    mov bl,2
    call pausa
npause4:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_sleft endp

;***************************************;
;***    KEY_ROTATE                   ***;
;***************************************;
key_rotate proc near
        push bx
        push ds
        push cs
        pop ds
        mov mousex1,10
        mov mousex2,22
        mov mousey1,9
        mov mousey2,9
        mov mcolor,17h                  ;red... pressed
        call pintar
        cmp macroW,1
        je nopausa1
        mov bl,3
        call pausa
nopausa1:
        mov mcolor,07h
        call pintar
        pop ds
        pop bx
        ret
key_rotate endp

;***************************************;
;***    KEY_FLIP                     ***;
;***************************************;
key_flip proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,10
    mov mousex2,22
    mov mousey1,10
    mov mousey2,10
    call mouse_xy
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa2
    mov bl,3
    call pausa
nopausa2:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_flip endp

;***************************************;
;***    key_INVERT                   ***;
;***************************************;
key_invert proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,10
    mov mousex2,22
    mov mousey1,11
    mov mousey2,11
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa3
    mov bl,3
    call pausa
nopausa3:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_invert endp

;***************************************;
;***    key_CLEAN                    ***;
;***************************************;
key_clean proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,10
    mov mousex2,22
    mov mousey1,12
    mov mousey2,12
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa4
    mov bl,3
    call pausa
nopausa4:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_clean endp

;***************************************;
;***    key_TROTA                    ***;
;***************************************;
key_trota proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,23
    mov mousex2,34
    mov mousey1,9
    mov mousey2,9
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa5
    mov bl,3
    call pausa
nopausa5:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_trota endp

;***************************************;
;***    key_GRID                     ***;
;***************************************;
key_grid proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,23
    mov mousex2,34
    mov mousey1,10
    mov mousey2,10
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa5g
    mov bl,3
    call pausa
nopausa5g:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_grid endp

;***************************************;
;***    key_ANIM                     ***;
;***************************************;
key_anim proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,10
    mov mousex2,34
    mov mousey1,7
    mov mousey2,7
    mov mcolor,17h                  ;red... pressed
    cmp macroW,1
    je nopausa6
    call pintar
    mov bl,3
    call pausa
nopausa6:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_anim endp

;***************************************;
;***    key_COPY                     ***;
;***************************************;
key_copy proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,48
    mov mousex2,62
    mov mousey1,9
    mov mousey2,9
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa7
    mov bl,3
    call pausa
nopausa7:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_copy endp

;***************************************;
;***    key_PASTE                    ***;
;***************************************;
key_paste proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,48
    mov mousex2,62
    mov mousey1,10
    mov mousey2,10
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa8
    mov bl,3
    call pausa
nopausa8:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_paste endp

;***************************************;
;***    key_89                       ***;
;***************************************;
key_89 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,48
    mov mousex2,62
    mov mousey1,11
    mov mousey2,11
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa9
    mov bl,3
    call pausa
nopausa9:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret
key_89 endp

;***************************************;
;***    KEY_SALVAR                   ***;
;***************************************;
key_salvar proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,10
    mov mousex2,34
    mov mousey1,6
    mov mousey2,6
    mov mcolor,20h                  ;color red
    call pintar
    cmp macroW,1
    je nopausa12
    mov bl,3
    call pausa
nopausa12:
    mov mcolor,7
    call pintar
    pop ds
    pop dx
    ret
key_salvar endp

;***************************************;
;***    KEY_SHIFTF1                  ***;
;***************************************;
key_shiftf1 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,48
    mov mousex2,78
    mov mousey1,4
    mov mousey2,4
    mov mcolor,47h                  ;color red
    call pintar
    cmp macroW,1
    je nopausa1s
    mov bl,3
    call pausa
nopausa1s:
    mov mcolor,7
    call pintar
    pop ds
    pop dx
    ret
key_shiftf1 endp

;***************************************;
;***    KEY_CtrlF1                   ***;
;***************************************;
key_ctrlf1 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,48
    mov mousex2,78
    mov mousey1,5
    mov mousey2,5
    mov mcolor,47h                  ;color red
    call pintar
    cmp macroW,1
    je nopausa1c
    mov bl,3
    call pausa
nopausa1c:
    mov mcolor,7
    call pintar
    pop ds
    pop dx
    ret
key_ctrlf1 endp


;***************************************;
;***    key_F3                       ***;
;***************************************;
key_f3 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,4
    mov mousex2,21
    mov mousey1,17
    mov mousey2,17
    mov mcolor,20h                  ;red
    call pintar
    cmp macroW,1
    je nopause13
    mov bl,6
    call pausa
nopause13:
    mov mcolor,7h
    call pintar
    pop ds
    pop bx
    ret
key_f3 endp

;***************************************;
;***    key_CtrlF3                   ***;
;***************************************;
key_ctrlf3 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,26
    mov mousex2,48
    mov mousey1,17
    mov mousey2,17
    mov mcolor,20h              ;green
    call pintar
    cmp macroW,1
    je nopause14
    mov bl,6
    call pausa
nopause14:
    mov mcolor,7h              ;red
    call pintar
    pop ds
    pop bx
    ret
key_ctrlf3 endp

;***************************************;
;***    key_AltF3                    ***;
;***************************************;
key_altf3 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,54
    mov mousex2,75
    mov mousey1,17
    mov mousey2,17
    mov mcolor,20h                  ;red
    call pintar
    cmp macroW,1
    je nopause15
    mov bl,6
    call pausa
nopause15:
    mov mcolor,7h                  ;red
    call pintar
    pop ds
    pop bx
    ret
key_altf3 endp

;***************************************;
;   KEY_F12                             ;
;***************************************; new on  v2.99
key_f12 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,48
    mov mousex2,62
    mov mousey1,12
    mov mousey2,12
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa10
    mov bl,3
    call pausa
nopausa10:
    mov mcolor,07h                  ;black ...depressed.
    call pintar
    pop ds
    pop bx
    ret
key_f12 endp

;***************************************;
;    KEY_ALTF9                          ;
;***************************************;
key_altf9 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,63
    mov mousex2,79
    mov mousey1,9
    mov mousey2,9
    mov mcolor,17h                  ;Blue when pressed
    call pintar
    cmp macroW,1
    je nopausa17
    mov bl,3
    call pausa
nopausa17:
    mov mcolor,07h
    call pintar
    pop ds
    pop bx
    ret
key_altf9  endp

;***************************************;
;***    key_AltF10                   ***;
;***************************************;
key_altf10 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,63
    mov mousex2,79
    mov mousey1,10
    mov mousey2,10
    mov mcolor,17h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa18
    mov bl,1
    call pausa
nopausa18:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret

key_altf10 endp

;***************************************;
;    KEY_ALTF11                         ;
;***************************************;
key_altf11 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,63
    mov mousex2,79
    mov mousey1,11
    mov mousey2,11
    mov mcolor,47h                  ;Blue when pressed
    call pintar
    cmp macroW,1
    je nopausa11
    mov bl,3
    call pausa
nopausa11:
    mov mcolor,07h
    call pintar
    pop ds
    pop bx
    ret
key_altf11 endp

;***************************************;
;***    key_AltF12                   ***;
;***************************************;
key_altf12 proc near
    push bx
    push ds
    push cs
    pop ds
    mov mousex1,63
    mov mousex2,79
    mov mousey1,12
    mov mousey2,12
    mov mcolor,47h                  ;red... pressed
    call pintar
    cmp macroW,1
    je nopausa16
    mov bl,3
    call pausa
nopausa16:
    mov mcolor,7
    call pintar
    pop ds
    pop bx
    ret

key_altf12 endp

;*********************************; New on 3.00b
;   doublefont                    ;
;*********************************;
doublefont proc near
    mov dx,3c4h             ; address of Sequencer Controller
    mov al,3                ; 3er register
    out dx,al               ; Character Map Select

    inc dx                  ; data of Sequencer Controller
    in al,dx                ;
    and al,11000000b        ; bits =0
    or al,1                 ; bit 0 = 1
    out dx,al               ; Double Fonts!

    push cs
    pop es
    push cs
    pop ds

    call gen_table2

    ret
doublefont endp

;*********************************; New on 4.00
;   doubleno                      ;
;*********************************;
doubleno proc near
    mov dx,3c4h             ; address of Sequencer Controller
    mov al,3                ; 3er register
    out dx,al               ; Character Map Select

    inc dx                  ; data of Sequencer Controller
    in al,dx                ;
    and al,11000000b        ; bits =0
    out dx,al               ; Double Fonts!

    push cs
    pop es
    push cs
    pop ds

    ret
doubleno endp


;*********************************;
;***      xorear               ***;
;*********************************;
xorear proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

        mov cx,0
        mov si,offset datos
 todavia_sigo:
        xor byte ptr [si],10101010b
        inc si
        inc cx
        cmp cx,final - datos
        jb todavia_sigo

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        ret
xorear endp


;************************************;
;  peque�as funciones adicionales    ;
;************************************;      NEW ON REVISION A V3.00 24/9/94
soundd:
    lodsb
    cmp al,'0'              ;si es intro fin
    je sound_off
    cmp al,'1'              ;si es espacio de nuevo
    je sound_on
    jmp display_help

sound_off:
        mov flagb,1
        jmp alopa               ;sound off
sound_on:
        mov flagb,0
        jmp alopa               ;sound on

mouse:
    lodsb
    cmp al,'0'              ;si es intro fin
    je mouse_off
    cmp al,'1'              ;si es espacio de nuevo
    je mouse_on
    jmp display_help

mouse_on:                       ;3.00 rev a
        mov flagf,1
        jmp alopa               ;no chequea si hay mouse
mouse_off:
        mov flagf,0
        jmp alopa

;***********************;
; ansiplus              ;
;***********************;
ansiplus proc near
    push cs
    pop ds
    xor ax,ax
    mov es,ax
    mov ax,1a00h
    mov bx,414eh
    mov cx,5349h
    mov dx,2b2bh
    int 2fh
    cmp al,0ffh
    jne _ansiplus
    mov ax,es
    or ax,ax
    je _ansiplus
    lea dx,ansimsg
    mov ah,9
    int 21h
    mov ax,4c01h
    int 21h
_ansiplus:
    ret
ansiplus endp

;***************************************;
; dacolor                               ;
;***************************************;   new on v4.00
dacolor proc near
   push ds
   push cs
   pop ds

   mov ax,1015h
   mov bl,5
   int 10h
   mov red5,dh
   mov gre5,cx
   mov ax,1015h
   mov bl,56
   int 10h
   mov red56,dh
   mov gre56,cx
   mov ax,1015h
   mov bl,57
   int 10h
   mov red57,dh
   mov gre57,cx

   mov ax,1015h                        ;read individual DAC reg
   mov bl,63
   int 10h
   mov ax,1010h
   mov bl,5
   int 10h                             ;set individual DAC reg
   mov ax,1015h                        ;read individual DAC reg
   mov bl,7
   int 10h
   mov ax,1010h
   mov bl,56
   int 10h                             ;set individual DAC reg
   mov ax,1015h                        ;read individual DAC reg
   mov bl,0
   int 10h
   mov ax,1010h
   mov bl,57
   int 10h                             ;set individual DAC reg
   pop ds
   ret
dacolor endp

;***************************************;
; dacolores                             ;
;***************************************;   new on v4.00
dacolores proc near
   push ds
   push cs
   pop ds
   mov ax,1010h
   mov bl,5
   mov dh,red5
   mov cx,gre5
   int 10h
   mov ax,1010h
   mov bl,56
   mov dh,red56
   mov cx,gre56
   int 10h
   mov ax,1010h
   mov bl,57
   mov dh,red57
   mov cx,gre57
   int 10h
   pop ds
   ret
dacolores endp

;***************************************;
;***    antivirus                    ***;
;***************************************;
antivirus proc near
        mov dx,offset prean_msg
        mov ah,9
        int 21h

        call autosave
        jc irerroras

        call xorear                     ;desencriptar

        push cs
        pop ds
        mov dx,offset antiv_msg
        mov ah,9
        int 21h

irerroras:
        mov ah,4ch
        int 21h                         ;volver a DOS.
antivirus endp

;*******************************;
;*      registracion           *;
;*******************************;
registracion:

        lodsb
        cmp al,254
        je reg_pos              ;es registracion
        cmp al,255
        je reg_neg              ;no unregistracion
        jmp display_help        ;

reg_neg:
        mov flagc,0
        jmp mostrarreg

reg_pos:
        xor cx,cx                       ;no mas de 55 letras el nombre
        mov di,offset nombrereg

cosicosi:
        lodsb
        cmp al,13
        je regisss

        mov [di],al
        inc di
        inc cx
        cmp cx,55
        jb cosicosi                      ;no m�s de 55 letras la long del name

regisss:
        cmp flagc,1
        jne proseguir

        mov ah,9
        mov dx,offset rebotar
        int 21h
        mov ah,4ch
        int 21h


proseguir:
        mov flagc,1
        mov dx,offset registramsg
        mov ah,9
        int 21h

mostrarreg:                     ;viene aca el inregistrador

        call autosave           ;salva el prog como esta
        jc errordeas            ;hubo error en autosave

        call xorear             ;vuelve todo al estado normal

        mov dx,offset reg_ok_msg
        cmp flagc,1                ;porque esta encriptado.
        je regis_msg_
        mov dx,offset unregmsg  ;el programa fue inregistrado

regis_msg_:
        mov ah,9
        int 21h
errordeas:
        mov ah,4ch
        int 21h


;***************************************;
;***    autosave                     ***;
;***************************************;
autosave proc near

        mov cx,0ffffh
        xor ax,ax
        xor di,di
        mov es,cs:[2ch]
scanear:
        repne scasb
        cmp byte ptr es:[di],0
        je ok_todo
        scasb
        jne scanear
ok_todo:
        mov dx,di
        add dx,3
        push es
        pop ds                  ;HASTA ACA DS:DX EL NOMBRE DEL PROG

        call xorear             ;invierte los caracteres

        mov ah,3ch              ; Create a File
        xor cx,cx               ; in this case Truncate
        int 21h
        jc derror               ; If error then go to derror

        mov bx,ax               ;file handle
        mov cx,final - start    ;number of bytes
        push cs
        pop ds
        mov dx,offset start     ;a partir de donde (desde el principio)
        mov ah,40h
        int 21h                 ;write
        jc derror

        mov ax,5701h
        mov cx,FILETIME
        mov dx,FILEDATE
        int 21h                 ;Set Time & Date.
        jc derror

        mov ah,3eh
        int 21h                 ;close
        jc derror

        ret

derror:                         ;error de drive.show message
        call xorear             ;mensaje en estado normal

        push cs
        pop ds
        mov dx,offset reg_not_ok
        mov ah,9
        int 21h
        stc                     ;set clear
        ret

autosave endp

;*******************************;
;       verascii                ;
;*******************************;

verascii proc near

        push ax
        push dx
        push ds

        push cs
        pop ds

        mov dbyte1,'0'          ;inicializa los valores
        mov dbyte2,'0'
        mov dbyte3,'0'

        mov ah,carac
        mov ascii,ah

db1:
        cmp ascii,0
        je disasc
        dec ascii               ;decrementa el valor

        cmp dbyte1,'9'
        je db2
        inc dbyte1
        jmp short db1
db2:
        mov dbyte1,'0'
        cmp dbyte2,'9'
        je db3
        inc dbyte2
        jmp short db1
db3:
        mov dbyte2,'0'
        inc dbyte3
        jmp short db1

disasc:
        mov erow,16
        mov ecol,9
        call irpos              ;fijar el cursor a una poscicion

        mov ah,9                ;mostrar esta cagada
        mov dx,offset ascii_str
        int 21h

        pop ds
        pop dx
        pop ax

        ret

verascii endp

;*******************************;
;       verchar                 ;
;*******************************;

verchar proc near

        call inicializar
        call getfont
        call restaurar
        call dischar

        ret

verchar endp

;*******************************;
;       charup_                 ;
;*******************************;
charup_:
        mov ah,08h
        xor bh,bh
        int 10h                ;get char and attribute

        inc al
        mov carac,al
        mov carr,al

        mov flaga,1
        jmp mostrar_char


;*******************************;
;       chardown_               ;
;*******************************;
chardown_:
        mov ah,08h
        xor bh,bh
        int 10h                ;get char and attribute

        dec al
        mov carac,al
        mov carr,al
        mov flaga,1
        jmp mostrar_char

;*******************************;
;       speeddown               ;
;*******************************;
speeddown:
        mov ah,speedy
        cmp ah,18
        je spd
        inc ah
        mov speedy,ah
spd:
        jmp loop1

;*******************************;
;       speedup                 ;
;*******************************;
speedup:
        mov ah,speedy
        cmp ah,0
        je spu
        dec ah
        mov speedy,ah
spu:
        jmp loop1


;*******************************;
;       inicializar             ;
;*******************************;
;use bitplane 2 , seg at a000h  ;

inicializar proc near

        push ax
        push dx
        push ds

        push cs
        pop ds

nopuedo_inic:
        cmp inic,0
        jne nopuedo_inic

        mov inic,1

        cli                     ;clear interrup flag

        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al

        inc dx                  ; data of Sequencer Controller
        in al,dx                ;
        mov srmap,al            ; restaura valor origianal
        mov al,04h              ; select bitplane 2
        out dx,al               ;
;
        mov dx,3c4h             ; Sequencer Controller
        mov al,4                ; 4th register
        out dx,al

        inc dx
        in al,dx                ;
        mov srmem,al            ;restaura valor original
        mov al,7
        out dx,al               ;256k & linear processing.
;
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register

        inc dx
        in al,dx
        mov grmap,al            ;restaura valor original
        mov al,02h              ;Bitplane 2
        out dx,al
;
        mov dx,3ceh             ;Graphic Controller
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register

        inc dx
        in al,dx
        mov grmod,al            ;restaura valor original
        mov al,0
        out dx,al               ;Write mode 0,linear adresses,16 colors
;
        mov dx,3ceh             ;Graphic controller
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register

        inc dx
        in al,dx
        mov grmis,al            ;restaura valor original
        mov al,04h
        out dx,al               ;Text,linear addresses,64k at A000h
;
        sti                     ;set interrupt flag

        pop ds
        pop dx
        pop ax

        ret

inicializar endp

;******************************;
;       restaurar              ;
;******************************;

restaurar proc near

        push ax
        push dx
        push ds

        push cs
        pop ds
;
        cli

        mov dx,3c4h             ; address of Sequencer Controller
        mov al,2                ; 2nd register
        out dx,al

        inc dx                  ; data of Sequencer Controller
        mov al,srmap            ;
        out dx,al               ; restaura
;
        mov dx,3c4h
        mov al,4                ; 4th register
        out dx,al

        inc dx
        mov al,srmem            ;
        out dx,al               ;restaura
;
        mov dx,3ceh             ;address of Graphic Controller
        mov al,4                ;Select
        out dx,al               ;Read Map Register

        inc dx
        mov al,grmap
        out dx,al               ;restaura
;
        mov dx,3ceh
        mov al,5                ;Select
        out dx,al               ;Graphics mode Register

        inc dx
        mov al,grmod
        out dx,al               ;restaura
;
        mov dx,3ceh
        mov al,6                ;Select
        out dx,al               ;Miscellaneous register

        inc dx
        mov al,grmis
        out dx,al               ;restaura
;
        mov inic,0

        sti

        pop ds
        pop dx
        pop ax

        ret

restaurar endp

;******************************;
;       getfont                ;
;******************************;

getfont proc near

        push ax
        push bx
        push cx
        push di
        push si

        mov al,carac

        mov ah,32
        mul ah

        mov si,ax
        mov di,offset char

        cld
        mov ax,0a000h
        mov ds,ax               ;ds= a000
        push cs
        pop es                  ;es= cs

        mov cx,32
rep     movsb

        pop si
        pop di
        pop cx
        pop bx
        pop ax

        ret

getfont endp

;******************************;
;       getchar                ;
;******************************;
getchar proc near

        push ax
        push bx
        push dx

        mov flaga,1

        mov ah,2
        mov dh,16
        mov dl,7
        xor bh,bh
        int 10h                 ;set cursor position

        mov ah,8h
        xor bh,bh
        int 10h
        mov bl,ATCOLOR
        mov cx,1
        mov ah,9
        int 10h


        call macro_wri          ;es un macro lo que se esta ejecutando
        cmp macroW,1
        je acaviejo             ;en ax esta el valor...

        mov ah,10h
        int 16h                 ;Keaboard Read

        call macro_rea

acaviejo:
        mov carac,al
        mov scancod,ax
        cmp al,0
        je flagon
        cmp al,0e0h
        jne ok_ok

flagon:
        mov flaga,0
        jmp short ok_ok2
ok_ok:
        mov carr,al             ;copia para Alt F11 y Alt F12 - v1.70
ok_ok2:
        pop dx
        pop bx
        pop ax

        ret

getchar endp


;******************************;
;       dischar                ;
;******************************;

dischar proc near

        push cs
        pop ds

        mov row,0

        mov si,offset char

mainloop1:
        mov col,0
        mov cl,80h              ;bit 7

again1:
        mov ch,cl

        and ch,[si]             ;compara si esta prendido
        or ch,ch                ;es =0 ?
        je bitoff               ;entonces, apagado

biton:
        mov char2,178              ; 178 = �
        jmp short aca

bitoff:
        mov char2,01h              ; 01h = grid
aca:
        call putchar

        inc col
        clc                     ;clear carry flag
        shr cl,1                ;rotate one bit to the right
        jnc again1              ;si no termino con los 8 bits sigue

        inc si
        inc row

        mov ah,totrow1          ;cantidad de rows
        inc ah
        cmp row,ah              ;Termino con las lineas de esto
        je fin1
        jne mainloop1

fin1:
        ret

dischar endp

;********************************;
;       putchar                  ;
;********************************;

putchar proc near

        push dx
        push cx
        push bx
        push ax

        xor bh,bh               ;pagina 0
        mov dh,row
        mov dl,col
        mov ah,02h
        int 10h                 ;set cursor pos

        mov ah,9                ;write char at cursor position
        mov al,char2
        mov bl,color            ;
        mov cx,1                ;one time
        int 10h                 ;

        pop ax
        pop bx
        pop cx
        pop dx

        ret

putchar endp

;***********************;
;       clrscr          ;
;***********************;

clrscr proc near
        push ds

        mov ax,0b800h
        mov ds,ax
        mov cx,8000
        xor si,si
        mov ax,0720h            ;color + caracter
miprimerloop:
        mov [si],ax
        add si,2
        dec cx
        jne miprimerloop

        mov ax,500h
        int 10h                 ;Select active display page

        mov ah,0Fh
        int 10h                 ;Get video mode

        mov ah,3                ;es el numero 3
        cmp al,ah
        je loc_3
        mov al,ah
        xor ah,ah
        int 10h                 ;no entonces lo pone

loc_3:
        mov ah,2
        mov bh,0
        xor dx,dx
        int 10h                 ;set cursos position

        pop ds
        ret

clrscr  endp


;***********************;
;       isvga           ;
;***********************;

isvga proc near

        mov ax,1a00h
        int 10h
        ret

isvga  endp

;***************************************;
;***    viewfile                     ***;
;***************************************;

viewfile proc near

        push ax
        push bx
        push dx
        push ds

        push cs
        pop ds

        mov ah,3
        xor bh,bh
        int 10h                 ;Read cursor positio
        push dx

        mov dh,17
        mov dl,0
        mov ah,2
        int 10h                 ;Set cursor position

        mov ah,9
        mov dx,offset dname     ;
        int 21h                 ;el filename

        pop dx
        mov ah,2
        int 10h                 ;set cursor old positon

        pop ds
        pop dx
        pop bx
        pop ax

        ret
viewfile endp

;***************************************;
;***    grabando                     ***;
;***************************************;

grabando proc near

        push ax
        push bx
        push dx
        push ds

        push cs
        pop ds

        mov ah,3
        xor bh,bh
        int 10h                 ;Read cursor positio
        push dx

        mov dh,17
        mov dl,0
        mov ah,2
        int 10h                 ;Set cursor position

        mov ah,9
        mov dx,offset graban
        int 21h                 ;el filename

        pop dx
        mov ah,2
        int 10h                 ;set cursor old positon

        pop ds
        pop dx
        pop bx
        pop ax

        ret
grabando endp

;*******************************;
;*******************************;
;       comun                   ;
;*******************************;
;*******************************;
comun proc near

        cmp active,0                    ;se esta usando el mouse ?
        je aquicomun
        ret
aquicomun:                              ;no entonces puedo usar las teclas...
        mov active,1                    ;pero ahora no el mouse

        cmp escan,F2
        je esalva
        cmp escan,F3
        je esave
        cmp escan,F4
        je eanima
        cmp escan,F5
        je erotate
        cmp escan,F6
        je eflip
        cmp escan,F7
        je einvert
        cmp escan,F8
        je eclean
        cmp escan,F9
        je ecopiar
        cmp escan,F10
        je epaste
        cmp escan,F11
        je ea8bit
        cmp escan,AltF12
        je eresori
        jmp parcheputo

;*******************************                parche

esalva: call key_salvar
        call salvar             ;F2
        jmp findelcomun
esave:  call key_f3
        call save               ;F3
        jmp findelcomun
eanima: call key_anim
        call toggle_anim        ;F4
        jmp findelcomun
erotate: call key_rotate
        call rotate            ;F5
        jmp findelcomun
eflip:  call key_flip
        call flip               ;F6
        jmp findelcomun
einvert: call key_invert
        call invert            ;F7
        jmp findelcomun
eclean: call key_clean
        call clean              ;F8
        jmp findelcomun
ecopiar: call key_copy
        call copiar            ;F9
        jmp findelcomun
epaste: call key_paste
        call paste              ;F10
        jmp findelcomun
ea8bit: call key_89
        call a8bit             ;F11
        jmp findelcomun
eresori: call key_altF12
        call resori            ;AltF12
        jmp findelcomun
;*******************************                parche


parcheputo:
        cmp escan,AltF5
        je etrota
        cmp escan,AltF3
        je esalvasc
        cmp escan,AltF6
        je egrid
        cmp escan,CtrlF3
        je esalvasm
        cmp escan,AltF11
        je eerasof
        cmp escan,F12
        je etog250
        cmp escan,AltF9
        je emacro_lee
        cmp escan,AltF10
        je emacro_gra
        cmp escan,_RIGHT
        je egright
        cmp escan,_LEFT
        je egleft
        cmp escan,_UP
        je egup
        cmp escan,_DOWN
        je egdown
        cmp escan,ShiftF1
        je esee_ascii
        cmp escan,CtrlF1
        je eclear_win

        jmp findelcomun

etrota: call key_trota          ;color
        call trota              ;Alt F5
        jmp findelcomun
esalvasc:
        call key_AltF3
        call sal_asc            ;Alt F3
        jmp findelcomun
egrid:
        call key_grid            ;Alt F6
        call grid
        jmp findelcomun
esalvasm:
        call key_CtrlF3
        call sal_asm            ;Ctrl F3
        jmp findelcomun
eerasof: call key_AltF11        ;color
        call gen_table          ;Alt F11
        jmp findelcomun
etog250: call key_F12           ;color
        call tog250             ;F12
        jmp findelcomun
emacro_lee: call key_altF9
        call macro_lee      ;Alt F9
        jmp findelcomun
emacro_gra: call key_altf10
        call macro_gra      ;Alt F10
        jmp findelcomun
egright: call key_sright
        call gright
        jmp findelcomun
egleft: call key_sleft
        call gleft
        jmp findelcomun
egup:   call key_sup
        call gup
        jmp findelcomun
egdown: call key_sdown
        call gdown
        jmp findelcomun
esee_ascii: call key_shiftf1
        call see_ascii      ;Shift F1
        jmp findelcomun
eclear_win: call key_ctrlf1
        call clear_win      ;Ctrl F1


findelcomun:
        mov active,0            ;se puede usar mouse y teclas
        ret                     ;retornar a casa tranquilos

comun endp

;*******************; Ctrl F1
; Clear_win         ;
;*******************;
clear_win proc near
        push ds
        push si
        call mouse_hide         ;bug fix del mouse
        mov cx,400
        mov ax,0b800h
        mov ds,ax
        mov si,0be0h
win_loop:
        mov byte ptr [si],20h
        add si,2
        dec cx
        or cx,cx
        jne win_loop
        call mouse_show         ;bug fix del mouse
        pop si
        pop ds
        ret
clear_win endp

;*****************; Shift F1
; See_ascii       ;
;*****************;
see_ascii proc near
        push ds
        push di
        call mouse_hide         ;bug fix del mouse
        xor cl,cl
        mov di,0be0h
        mov ax,0b800h
        mov ds,ax
see_asci2:
        mov [di],cl
        add di,2
        inc cl
        or cl,cl
        jne see_asci2
        call mouse_show
        pop di
        pop ds
        ret
see_ascii endp

;*******************************;
;                               ;
;         toggle_anim           ;
;                               ;
;*******************************;
toggle_anim proc near
        push ds
        push cs
        pop ds

        xor flagd,1

        pop ds
        ret
toggle_anim endp

;*******************************;
;                               ;
;         animacion             ;
;                               ;
;*******************************;
animacion proc near
        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds
        push es
        pushf

        push cs
        pop ds

        cmp active,1
        jne animacionpuede
        jmp final_anim

animacionpuede:
        mov ah,speedy
        cmp animac,ah           ;paso un segundo
        ja anim

        jmp final_anim

anim:
        mov animac,0            ;a poscicion original

        call inicializar        ;permite acceso al segmento A000

        mov ax,0a000h           ;segmento a000h
        mov ds,ax

;*************************************** scroll del mensaje de ERASOFT ****
        xor cx,cx               ;
seguirc:
        xor bx,bx               ;contadores
seguira:
        mov si,(32*246)         ;va a cambiar los valores entre 246
        add si,bx               ;para el futuro (ahora no influye)

        mov ah,[si+224]         ;compara el ultimo bit del ultimo byte
        and ah,1
        cmp ah,1
        je stc_

        clc
        jmp short seguirb
stc_:
        stc
seguirb:
        rcr byte ptr [si],1
        rcr byte ptr [si+32],1
        rcr byte ptr [si+64],1
        rcr byte ptr [si+96],1
        rcr byte ptr [si+128],1
        rcr byte ptr [si+160],1
        rcr byte ptr [si+192],1
        rcr byte ptr [si+224],1

acaaca:
        inc bx
        cmp bx,16
        jb seguira

        inc cx                  ;lo hace 2 veces por llamada
        cmp cx,2                ;
        jb seguirc              ;2 veces si !

;*************************************************** hombre saludando

        mov ah,cs:speedy2       ;counter
        cmp ah,3
        je hombre_corriendo

        inc ah
        mov cs:speedy2,ah       ;counter actualizado
        jmp siguiente_anim

hombre_corriendo:
        xor cx,cx               ;contador
        mov cs:speedy2,cl       ;reset counter del hombre

        mov si,254 * 32         ;1er hombre
        mov di,255 * 32         ;2do hombre

seguiggg:
        mov ah,[si]
        xchg [di],ah
        mov [si],ah

        inc si
        inc di
        inc cx
        cmp cx,16
        jb seguiggg

;************************************************** para futura animacion

siguiente_anim:

        call restaurar          ;restaura valores (inicializar)

final_anim:

        popf
        pop es
        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax
        ret

animacion endp

;***************************************;
;***                                 ***;
;***    sal_asm                      ***;
;***                                 ***;
;***************************************;

sal_asm proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

        mov ax,totrow8
        mov marica,ax

        mov ah,flagd            ;si hay animacion o no ?
        push ax
        cmp ah,0
        je noesperes_          ;no espero porque la animacion no esta ON

        mov flagd,0             ;corta animacion
        mov flage,0
comparando2:
        cmp flage,20            ;espera un segundo por lo menos para cortar
        jb comparando2

noesperes_:

        mov ah,48h              ;Hay suficiente memoria ?
        mov bx,513              ;513 paragraph, 2000h bytes (8k)
        int 21h
        jnc haymem2
        call sound
        call sound
        jmp chau_salasm

haymem2:
        call grabando

        mov segme,ax

        mov ah,3ch              ;crea archivo ASM
        xor cx,cx
        mov dx,offset asmname
        push cs
        pop ds
        int 21h
        mov handle,ax
        jnc todoOKK

huberrorsi:
        call sound
        jmp chau_salasm

todoOKK:
        mov ch,asmname+7          ;proximo sera 00000001.asc
        cmp ch,'9'              ;Esto es por un nombre es rrrrrrrr
        jbe forrazoA            ;es proximo sera rrrrrrrr0
        mov ch,'0'
        jmp short aboabA
forrazoA:
        inc ch                  ;incrementa 1
        and ch,00000111b        ;maximo permitido en 7
        or ch,30h               ;le agrega 30h
aboabA:
        mov asmname+7,ch

        call inicializar        ;ahora inicializa para esta concha

        mov ax,segme
        mov es,ax
        xor di,di               ;es:di  -> destination
        mov ax,0a000h           ;
        mov ds,ax
        xor si,si               ;ds:si  -> source
        mov cx,2000h
rep     movsb

        call restaurar

;***************************************;
;***                                 ***;
        push cs
        pop ds
        cmp flagc,1
        jne noWnoR

        mov bx,handle
        mov ah,40h
        mov cx,fffinal - princi         ;longitud
        mov dx,offset princi
        int 21h                         ;grabar cabecera...
        jmp short sharew
noWnoR:                                 ;no grabo porque no REG
        mov bx,handle
        mov ah,40h
        mov cx,share2 - share1
        mov dx,offset share1
        int 21h
sharew:
;***                                 ***;
;***************************************;


;***************************************;
;***************************************;
;***                                 ***;
        mov asc3,0              ;el primero es 0
        xor si,si               ;y el source = 0 para versi�n registrada
        cmp flagc,1
        je siRegW

        mov asc3,200            ;solo graba del 200 al 255 (shareware)
        mov si,200 * 32         ;versi�n no registrada
siRegW:

vueltaapay2:
        xor ch,ch
        mov cl,totrow1          ;new on  v2.60
        inc cx                  ;ver si es 50 o 25 columnas
        call grabar3

vueltaapay:
        mov ah,es:[si]          ;poscion de la tabla
        mov datab,ah
        call valorasc
        call grabar1
        inc si
        dec cx
        or cx,cx
        jne vueltaapay

        xor ah,ah
        mov al,totrow4
        add si,ax                       ;saltea los espacios chotos
                                        ;dependiendo si es 8x8 o 8x16
        mov ax,4201h                    ;move file pointer 1 char atras
        mov bx,handle
        mov cx,0ffffh
        mov dx,0ffffh                   ;-1
        int 21h

        mov ah,asc3
        mov datab,ah
        call valorasc
        call grabar2

        inc asc3
        cmp asc3,0
        jne vueltaapay2
;***                                 ***;
;***************************************;
;***************************************;

;***************************************;
;***                                 ***;
        push cs                         ;grabar lo �ltimo
        pop ds
        cmp flagc,1
        jne noWnoR2
        mov bx,handle
        mov ah,40h
        mov cx,fffina2 - princi2        ;longitud
        mov dx,offset princi2
        int 21h                         ;grabar cola
noWnoR2:                                ;no graba si no esta REG.
;***                                 ***;
;***************************************;

        mov ah,3eh
        mov bx,handle
        int 21h                         ;close handle

        mov ah,49h
        mov bx,segme
        mov es,bx
        int 21h                 ;free allocated memory in ES

chau_salasm:

        push cs
        pop ds

        call sound

        pop ax
        mov flagd,ah            ;si hay animacion o no .

        call viewfile

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        ret

sal_asm endp


;***************************************;
;***     valorasc                    ***;
;***************************************;

valorasc proc near

        push ax
        push dx
        push ds

        push cs
        pop ds

        mov numero,'0'          ;inicializa los valores
        mov numero+1,'0'
        mov numero+2,'0'

        mov ah,datab
        mov asc2,ah

adb1:
        cmp asc2,0
        je disasca
        dec asc2               ;decrementa el valor

        cmp numero+2,'9'
        je adb2
        inc numero+2
        jmp short adb1
adb2:
        mov numero+2,'0'
        cmp numero+1,'9'
        je adb3
        inc numero+1
        jmp short adb1
adb3:
        mov numero+1,'0'
        inc numero
        jmp short adb1

disasca:
        pop ds
        pop dx
        pop ax

        ret

valorasc endp

;***************************************;
;***    grabar1                      ***;
;***************************************;
grabar1 proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,40h
        mov bx,handle
        mov cx,4
        mov dx,offset numero
        int 21h

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret

grabar1 endp

;***************************************;
;***    grabar2                      ***;
;***************************************;
grabar2 proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,40h
        mov bx,handle
        mov cx,11
        mov dx,offset numero-8
        int 21h

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret

grabar2 endp

;***************************************;
;***    grabar3                      ***;
;***************************************;
grabar3 proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        mov ah,40h
        mov bx,handle
        mov cx,5
        mov dx,offset defbyt
        int 21h

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax
        ret

grabar3 endp

;***   FIN DE SALVAR ASM             ***;
;***************************************;
;***************************************;

;*******************************;
;                               ;
;           save                ;
;                               ;
;*******************************;
save proc near

        push ax
        push bx
        push cx
        push dx
        push ds

        push cs
        pop ds

        mov ah,totrow5          ;new on v2.60
        mov biteti+1,ah

        mov ah,flagd            ;si hay animacion o no ?
        push ax
        cmp ah,0
        je no_esperes_          ;no espero porque la animacion no esta ON

        mov flagd,0             ;corta animacion
        mov flage,0
comparando:
        cmp flage,20            ;espera un segundo por lo menos para cortar
        jb comparando

no_esperes_:

        mov ah,48h              ;Hay suficiente memoria ?
        mov bx,513              ;513 paragraph, 2000h bytes (8k)
        int 21h
        jnc haymem
        call sound
        call sound
        jmp chau_save
haymem:
        call grabando

        mov segme,ax

        mov ah,3ch              ;create file
        xor cx,cx               ;atributos
        mov dx,offset fname
        int 21h                 ;create a file or truncate
        mov handle,ax

        jnc nohuberror
huberror:
        call sound
        jmp chau_save

nohuberror:
        mov ch,fname+7          ;proximo sera 00000001.asc
        cmp ch,'9'              ;Esto es por un nombre es rrrrrrrr
        jbe forrazo1            ;es proximo sera rrrrrrrr0
        mov ch,'0'
        jmp short aboab1
forrazo1:
        inc ch                  ;incrementa 1
        and ch,00000111b        ;maximo permitido en 7
        or ch,30h               ;le agrega 30h
aboab1:
        mov fname+7,ch

        mov bx,ax               ;get handle
        mov ah,40h
        mov cx,CARG_SIZE        ;number of bytes
        mov dx,offset cargador
        int 21h                 ;write usign handle
        jb huberror

        call inicializar        ;ahora inicializa para esta concha

        mov ax,segme
        mov es,ax
        xor di,di               ;es:di  -> destination
        mov ax,0a000h           ;
        mov ds,ax
        xor si,si               ;ds:si  -> source

        mov cx,100h             ;total de veces
seguir__a:
        call mover16
        dec cx
        or cx,cx                ;cx=0?
        jne seguir__a

        call restaurar

        mov cx,cs:totrow6       ;grabar tabla completa
        push es
        pop ds                  ;ds:dx  la tabla
        xor dx,dx
        mov bx,cs:handle        ;file handle
        mov ah,40h
        int 21h                 ;Tabla grabada completa
        jc huberror

        mov ah,3eh              ;close file handle
        int 21h
        jc huberror

        mov ah,49h
        mov bx,cs:segme
        mov es,bx
        int 21h                 ;free allocated memory in ES

        call viewfile

chau_save:

        push cs
        pop ds

        call sound

        pop ax
        mov flagd,ah            ;si hay animacion o no .

        pop ds
        pop dx
        pop cx
        pop bx
        pop ax

        ret

save endp

cargador:
        jmp short abcd
        db'VChar NI v3.10 (c) Ricardo Quesada 1994.'
abcd:
        cld
        mov si,81h
_alopa: lodsb
        cmp al,13               ;si es intro fin
        je _apartir
        cmp al,20h              ;si es espacio de nuevo
        je _alopa
_lulu:  cmp al,'/'
        je _opcion
        cmp al,'-'
        jne _alopa

_opcion:
        lodsb
        cmp al,13
        je _apartir

        cmp al,'8'
        je _8_
        cmp al,'9'
        je _9_
        jmp _alopa

_8_:                            ;convierte todo a 8 bit width
        mov dx,3cch             ;cambia la frecuencia del monitor
        in al,dx
        and al,11110011b        ;borra lo que halla
        mov dx,3c2h
        out dx,al

        cli
        mov dx,3c4h
        mov ax,100h
        out dx,ax

        mov ax,101h
        out dx,ax

        mov ax,300h             ;Enable the controller
        out dx,ax               ;
        sti

        mov ax,1000h            ;interrupcion
        mov bl,13h
        mov bh,0                ;por default para 8bit
        int 10h                 ;posciciona bien la pantalla
        jmp _apartir

_9_:                            ;convierte todo a 9 bit width
        mov dx,3cch             ;cambia la frecuencia del monitor
        in al,dx
        and al,11110011b        ;borra lo que halla
        or al,00000100b         ;prende el puto bit
        mov dx,3c2h
        out dx,al

        cli
        mov dx,3c4h
        mov ax,100h
        out dx,ax

        mov ax,001h
        out dx,ax

        mov ax,300h             ;Enable the controller
        out dx,ax               ;
        sti

        mov ax,1000h            ;interrupcion
        mov bl,13h
        mov bh,8                ;por default para 8bit
        int 10h                 ;posciciona bien la pantalla

_apartir:
        mov bp,100h + CARG_SIZE ;offset donde estan los chars
biteti  db 0b7h,0               ;mov bh,8 o 10h
        mov cx,100h             ;cantidad de chars
        xor dx,dx               ;offset del block
        xor bl,bl               ;block
        mov ax,1110h            ;write usign 10h
        int 10h

        mov ax,4c00h            ;terminate
        int 21h

findecargador equ this byte

;***************************************;
;***    mover16                      ***;
;***************************************;
mover16 proc near
        push cx

        xor ch,ch
        mov cl,cs:totrow5
repne   movsb

        xor ch,ch
        mov cl,cs:totrow4
        add si,cx                   ;saltea espacios chotos

        pop cx
        ret

mover16 endp

;***    FIN DE SALVAR COM            ***;
;***************************************;
;***************************************;

;*******************************;
;                               ;
;        tog250                 ;
;                               ;
;*******************************;
tog250 proc near
    push ds

    push cs
    pop ds

;compara si hay animaci�n

    mov ah,flagd            ;si hay animacion o no ?
    push ax
    cmp ah,0
    je xsperes_             ;no espero porque la animacion no esta ON

    mov flagd,0             ;corta animacion
    mov flage,0
xarando:
    cmp flage,4             ;espera un segundo por lo menos para cortar
    jb xarando

xsperes_:
    call infovga

    cmp totrow3,25          ;Number of rows.
    jne row25
    call sold25             ;save old 25
    call resori50
    jmp short _tog250
row25:
    call sold50             ;save old fonts 50
    call resori25
_tog250:
    call infovga

    call rold250            ;llama a caca gen_table! new on v3.00b

    call gen_table2         ;new on v3.00b

    mov al,carr
    mov carac,al
    call verchar

    pop ax
    mov flagd,ah            ;restaura posible animaci�n

    mov erow,0              ;por si esta en 25 a 50 para que no
    mov ecol,0              ;desaparezca el cursor

    pop ds
    ret

tog250 endp

;*******************************;
;                               ;
;        resori                 ;
;                               ;
;*******************************;
resori proc near
    push ds

    call infovga
    push cs
    pop ds

    cmp totrow3,25      ;Number of rows.
    jne row502
    call resori25
    jmp short _resori
row502:
    call resori50
_resori:
    call infovga
    mov al,carr
    mov carac,al
    call verchar
    pop ds
    ret

resori endp

;*******************************;
;                               ;
;        resori25               ;
;                               ;
;*******************************;

resori25 proc near

        push ax
        push bx

        mov ax,1114h            ;restore to original table
        mov bl,0
        int 10h

        pop bx
        pop ax

        ret

resori25 endp

;*******************************;       ;new on v2.60
;                               ;
;        resori50               ;
;                               ;
;*******************************;

resori50 proc near

        push ax
        push bx

        mov ax,1112h            ;restore to original table
        mov bl,0
        int 10h

        mov al,cs:carr
        mov cs:carac,al
        call verchar

        mov ax,600h
        xor bh,bh
        mov cx,800h
        mov dx,0f07h
        int 10h

        pop bx
        pop ax
        ret

resori50 endp

;***************************************;
;   sold25                              ;
;***************************************;
sold25 proc near
    push ax
    push bx
    push cx
    push dx
    push di
    push si
    push ds
    push es

    push cs
    pop ds

    mov sav25,1

    call inicializar        ;ahora inicializa para esta concha

    mov ax,segme25
    mov es,ax
    xor di,di               ;es:di  -> destination
    mov ax,0a000h           ;
    mov ds,ax
    xor si,si               ;ds:si  -> source

    mov cx,100h             ;total de veces
xir__a:
    call mover16
    dec cx
    or cx,cx                ;cx=0?
    jne xir__a

    call restaurar

    pop es
    pop ds
    pop si
    pop di
    pop dx
    pop cx
    pop bx
    pop ax

    ret

endp sold25

;***************************************;
;   sold50                              ;
;***************************************;
sold50 proc near
    push ax
    push bx
    push cx
    push dx
    push di
    push si
    push ds
    push es

    push cs
    pop ds

    mov sav50,1

    call inicializar        ;ahora inicializa para esta concha

    mov ax,segme50
    mov es,ax
    xor di,di               ;es:di  -> destination
    mov ax,0a000h           ;
    mov ds,ax
    xor si,si               ;ds:si  -> source

    mov cx,100h             ;total de veces
xxir__a:
    call mover16
    dec cx
    or cx,cx                ;cx=0?
    jne xxir__a

    call restaurar

    pop es
    pop ds
    pop si
    pop di
    pop dx
    pop cx
    pop bx
    pop ax

    ret

endp sold50

;***************************************;
;   rold250                             ;
;***************************************;
rold250 proc near

    push cs
    pop es
    push cs
    pop ds
    cmp totrow1,7
    jne _chars25

_chars50:
    cmp sav50,1
    je __chars50
    call gen_table
    jmp _rold250

__chars50:
    mov ax,segme50
    mov es,ax
    xor bp,bp
    mov bh,8
    jmp short riqdon

_chars25:
    cmp sav25,1
    je __chars25
    call gen_table
    jmp _rold250

__chars25:
    mov ax,segme25
    mov es,ax
    xor bp,bp
    mov bh,10h              ;numer of bytes per char

riqdon:
    mov cx,100h             ;numer of patterns to store
    xor dx,dx               ;a partir de que char.
    xor bl,bl               ;block to load in map 2
    mov ax,1110h
    int 10h                 ;Cargar valor en la tabla

    call infovga            ;new on v2.60

    mov al,cs:carr
    mov cs:carac,al
    call verchar

_rold250:
    ret
rold250 endp

;*******************************;       new on v2.60
;                               ;
;            infovga            ;
;                               ;
;*******************************;
infovga proc near
    push ds

    mov ax,40h
    mov ds,ax
    mov si,84h
    mov ah,[si]

    push cs
    pop ds

    cmp ah,49                       ;
    je rrr50                        ; REVISION A 24/9/94 V3.00

    mov totrow1,15
    mov totrow2,15*8
    mov totrow3,25
    mov totrow4,16
    mov totrow5,16
    mov totrow6,1000h
    mov totrow7,8
    mov totrow8,3031h

    jmp short _infovga
rrr50:
    mov totrow1,7
    mov totrow2,7*8
    mov totrow3,50
    mov totrow4,24
    mov totrow5,8
    mov totrow6,800h
    mov totrow7,4
    mov totrow8,3830h

_infovga:
    pop ds
    ret
infovga endp

;***************************************; AltF9          NEW ON  V2.99
;   macro_lee                           ;
;***************************************;
macro_lee proc near
    push ax
    push bx
    push cx
    push dx
    push di
    push ds                 ;lo que hace es cambiar entre 0-1 el flag de leyendo
    push es
    push bp

    push cs                 ;macro ya que esta tecla se usa para encender y apagar
    pop ds

    cmp macroR,1
    je macroRi

macroRa:                    ;aca llega si macroR=0 (empieza a grabar macro)

    mov ax,1003h                ;new on 3.00
    mov bl,1                    ;modified on 3.00b
    mov bh,0
    int 10h                     ;enable blinking

    mov ah,3
    xor bh,bh
    int 10h
    push dx                 ;Get cursor position

    push cs
    pop es
    mov ax,1300h            ;Write Character String
    xor bh,bh               ;pagina 0
    mov bl,10000011b        ;atributo (blinking black and bright cyan)
    mov cx,45               ;longitud
    mov dh,13               ;fila
    mov dl,20               ;columna
    mov bp,offset macromsg1
    int 10h

    pop dx
    mov ah,2
    xor bh,bh
    int 10h                 ;set cursor Position

    xor di,di
    mov macro_off,di        ;offset del macro con 0... para empezar a grabar
    mov ax,macro_seg
    mov es,ax
    xor ax,ax
    mov es:[di],ax
    mov macroR,1            ;desde el principio.
    jmp short _macro_lee

macroRi:                    ;aca llega cuando ya estaba grabando entonces hay que
    mov ah,3
    xor bh,bh
    int 10h
    push dx                 ;Get cursor position

    mov dh,13               ;file
    mov dl,20               ;columa
    xor bh,bh
    mov ah,2
    int 10h                 ;set cursor positon

    mov ah,9                ;
    mov al,20h              ;ascii char
    xor bh,bh
    mov bl,7                ;attribute
    mov cx,45               ;number of times
    int 10h

    pop dx
    mov ah,2
    xor bh,bh
    int 10h                 ;set cursor Position


    mov ax,macro_seg        ;que parar,poniendo un 0 en es:[di] y en macroR
    mov es,ax
    mov ax,macro_off
    mov di,ax
    xor ax,ax
    mov es:[di],ax
    sub di,2                ;una tecla atras lo para.
    mov es:[di],ax
    mov macro_off,ax        ;� problema resuelto ?
    mov macroR,0

_macro_lee:
    pop bp
    pop es
    pop ds
    pop di
    pop dx
    pop cx
    pop bx
    pop ax
    ret
macro_lee endp

;***************************************; AltF10         NEW ON V2.99
;   macro_gra                           ;
;***************************************;
macro_gra proc near
    push ds

    push cs
    pop ds
    cmp macroR,1                        ;Si se esta leyendo un macro no se puede
    je _macro_gra                       ;escribir
    mov macroW,1
_macro_gra:
    pop ds
    ret
macro_gra endp

;***************************************;en AX va la tecla a leer  NEW ON V2.99
;   macro_rea                           ;
;***************************************;
macro_rea proc near
    push bx
    push cx
    push dx
    push di
    push si
    push ds
    push es

    push cs
    pop ds
    cmp macroR,1
    jne _macro_rea

    mov bx,macro_off
    mov di,bx
    mov bx,macro_seg                ;valor sacado de allocmem para macro
    mov es,bx
    mov es:[di],ax
    add di,2
    mov bx,di
    mov macro_off,bx
    cmp di,2048                     ;se acab� el buffer ? total 2k
    jb _macro_rea                   ;no!

    call macro_lee                  ;apaga el macro read y listo

_macro_rea:
    pop es
    pop ds
    pop si
    pop di
    pop dx
    pop cx
    pop bx
    ret
macro_rea endp

;***************************************;devuelve en ax tecla leida
;   macro_wri                           ;
;***************************************;   NEW ON V2.99
macro_wri proc near
    push bx
    push cx
    push dx
    push di
    push si
    push ds
    push es

    push cs
    pop ds
    cmp macroW,1
    jne _macro_wri

    mov bx,macro_off
    mov di,bx
    mov bx,macro_seg                ;valor sacado de allocmem para macro
    mov es,bx
    mov ax,es:[di]                  ;AX=tecla leida
    or ax,ax                        ;se acabo el macro ?
    jne scmacw

    mov macroW,0
    mov macro_off,0
    jmp short _macro_wri

scmacw:
    add di,2
    mov bx,di
    mov macro_off,bx                ;devuelve en ax=tecla leida

_macro_wri:
    pop es
    pop ds
    pop si
    pop di
    pop dx
    pop cx
    pop bx
    ret
macro_wri endp

;***************************************; Alt F6
;   GRID                                ;
;***************************************;
grid proc near
    push ds
    push es

    push cs
    pop ds
    push cs
    pop es

    mov al,fgrid
    xor al,1
    and al,1
    mov fgrid,al

    cmp al,1
    je gridon                       ;hay que prender la grid

gridoff:
    cld
    lea di,grid16
    lea si,offgrid16
    mov cx,16
    repne movsb
    lea di,grid8
    lea si,offgrid8
    mov cx,8
    repne movsb
    jmp gridres
gridon:
    cld
    lea di,grid16
    lea si,ongrid16
    mov cx,16
    repne movsb
    lea di,grid8
    lea si,ongrid8
    mov cx,8
    repne movsb
gridres:
    call gen_table2
    pop es
    pop ds
    ret
grid endp

;***************************************; Alt F5
;*                                     *;
;*      TROTA                          *;
;*                                     *;
;***************************************;
trota proc near
    push ax
    push bx
    push cx
    push dx
    push di
    push si
    push ds
    push es

    push cs
    pop ds
    push cs
    pop es

    call salvar_

    mov bl,1h               ;indice 2 - 1 2 4 8 10 20 40 80
    mov si,offset char
    xor ch,ch               ;contador1

trotaloop1:
    mov bh,80h              ;indice 1  -80 40 20 10 8 4 2 1
    mov di,offset char_
    xor cl,cl               ;contador2

trotaloop2:
    mov ah,[si]             ;linea 1

    and ah,bh               ;indice 1
    or ah,ah
    je trota_off

trota_on:
    mov ah,[di]             ;linea 2
    or ah,bl                ;indice 2
    jmp short trotajmp
trota_off:

    mov ah,[di]             ;linea 2
    mov dl,255              ;
    sub dl,bl               ;indice 2
    and ah,dl               ;

trotajmp:
    mov [di],ah

    shr bh,1                ;indice 1(80,40,20 line1)
    inc di
    inc cl
    cmp cl,8
    jb trotaloop2
    shl bl,1
    inc si
    inc ch
    cmp ch,8
    jb trotaloop1

;hace los mismo con la parte de abajo.

    mov bl,1h               ;indice 2 - 1 2 4 8 10 20 40 80
    mov si,offset char + 8
    xor ch,ch               ;contador1

xtrotaloop1:
    mov bh,80h              ;indice 1  -80 40 20 10 8 4 2 1
    mov di,offset char_ + 8
    xor cl,cl               ;contador2

xtrotaloop2:
    mov ah,[si]             ;linea 1

    and ah,bh               ;indice 1
    or ah,ah
    je xtrota_off

xtrota_on:
    mov ah,[di]             ;linea 2
    or ah,bl                ;indice 2
    jmp short xtrotajmp
xtrota_off:

    mov ah,[di]             ;linea 2
    mov dl,255              ;
    sub dl,bl               ;indice 2
    and ah,dl               ;

xtrotajmp:
    mov [di],ah

    shr bh,1                ;indice 1(80,40,20 line1)
    inc di
    inc cl
    cmp cl,8
    jb xtrotaloop2
    shl bl,1
    inc si
    inc ch
    cmp ch,8
    jb xtrotaloop1

    cld
    mov di,offset char
    mov si,offset char_
    mov cx,32
    rep movsb

    call dischar

    pop es
    pop ds
    pop si
    pop di
    pop dx
    pop cx
    pop bx
    pop ax
    ret

trota endp

;*******************************;   F9
;                               ;
;            copiar             ;
;                               ;
;*******************************;
copiar proc near

        push cx
        push si
        push di
        push ds
        push es

        mov ax,cs
        mov ds,ax
        mov es,ax

        call salvar_

        mov si,offset char
        mov di,offset char__
        cld
        mov cx,32
        rep movsb

        pop es
        pop ds
        pop di
        pop si
        pop cx

        ret

copiar endp

;*******************************;  F10
;                               ;
;            paste              ;
;                               ;
;*******************************;
paste proc near

        push cx
        push si
        push di
        push ds
        push es

        mov ax,cs
        mov ds,ax
        mov es,ax

        mov di,offset char
        mov si,offset char__
        cld
        mov cx,32
        rep movsb

        call dischar

        pop es
        pop ds
        pop di
        pop si
        pop cx

        ret

paste endp

;*******************************;
;                               ;
;            gup                ;
;                               ;
;*******************************;
gup proc near

        push ax
        push bx
        push cx
        push di
        push si
        push ds
        push es

        push cs
        pop ds
        push cs
        pop es

        call salvar_

        mov di,offset char
        mov si,offset char + 1

        mov al,[di]

        cld
        mov cx,15
        rep movsb

        mov si,offset char
        xor bh,bh
        mov bl,totrow1                      ;modified on v2.60
        mov [si+bx],al

        call dischar

        pop es
        pop ds
        pop si
        pop di
        pop cx
        pop bx
        pop ax

        ret

gup endp


;*******************************;
;                               ;
;            gdown              ;
;                               ;
;*******************************;
gdown proc near

        push ax
        push bx
        push cx
        push di
        push si
        push ds
        push es

        push cs
        pop ds
        push cs
        pop es

        call salvar_

        xor bh,bh                       ;mod on v2.60
        mov bl,totrow1
        mov di,offset char              ;ori char + 15
        add di,bx
        mov si,offset char              ;ori char + 14
        add si,bx
        dec si

        mov al,[di]

        std
        xor ch,ch
        mov cl,cs:totrow1
    rep movsb

        mov si,offset char
        mov [si],al

        call dischar

        pop es
        pop ds
        pop si
        pop di
        pop cx
        pop bx
        pop ax

        ret

gdown endp


;*******************************;
;                               ;
;           gright              ;
;                               ;
;*******************************;
gright proc near

        push cx
        push si
        push ds

        push cs
        pop ds

        call salvar_

        mov cl,0
        mov si,offset char

righting:

        clc
        ror byte ptr [si],1

        inc si
        inc cl
        cmp cl,16
        jb righting

        call dischar

        pop ds
        pop si
        pop cx

        ret

gright endp

;*******************************;
;                               ;
;           gleft               ;
;                               ;
;*******************************;
gleft proc near

        push cx
        push si
        push ds

        push cs
        pop ds

        call salvar_

        mov cl,0
        mov si,offset char

lefting:

        clc
        rol byte ptr [si],1

        inc si
        inc cl
        cmp cl,16
        jb lefting

        call dischar

        pop ds
        pop si
        pop cx

        ret

gleft endp


;*******************************;
;                               ;
;           invert              ;
;                               ;
;*******************************;
invert proc near

        push cx
        push si

        call salvar_

        mov cl,0
        mov si,offset char

invirtiendo:
        mov al,255
        xor [si],al
        inc si
        inc cl
        cmp cl,16
        jb invirtiendo

        call dischar

        pop si
        pop cx

        ret

invert endp

;*******************************;
;                               ;
;           clean               ;
;                               ;
;*******************************;
clean proc near

        push cx
        push si

        call salvar_

        mov cl,0
        mov si,offset char

cleaning:
        xor ah,ah
        mov [si],ah
        inc si
        inc cl
        cmp cl,16
        jb cleaning

        call dischar

        pop si
        pop cx

        ret

clean  endp

;*******************************;
;                               ;
;           flip                ;
;                               ;
;*******************************;
flip proc near

        push bx
        push cx
        push di
        push si

        call salvar_

        xor cl,cl
        mov si,offset char          ;modif on v2.60
        mov di,offset char          ;ori char + 15
        xor bh,bh
        mov bl,cs:totrow1
        add di,bx

flipeando:
        mov al,[si]
        mov ah,[di]
        xchg al,ah
        mov [di],ah
        mov [si],al

        inc si
        dec di
        inc cl
        cmp cl,cs:totrow7
        jb flipeando

        call dischar

        pop si
        pop di
        pop cx
        pop bx
        ret

flip endp

;*******************************;
;                               ;
;           rotate              ;
;                               ;
;*******************************;
rotate proc near

        push cx
        push si
        push di
        push ds
        push es

        push cs
        pop ds
        push cs
        pop es

        call salvar_

        mov si,offset char
        mov di,offset char_
        mov ch,0
rotando:
        mov cl,0
        xor ah,ah
        mov al,[si]
rotate1:
        shl al,1
        rcr ah,1
        inc cl
        cmp cl,8
        jb rotate1

        mov [di],ah

        inc si
        inc di
        inc ch
        cmp ch,16
        jb rotando

        cld
        mov di,offset char
        mov si,offset char_
        mov cx,32
        rep movsb

        call dischar

        pop es
        pop ds
        pop di
        pop si
        pop cx

        ret

rotate endp

;*******************************;
;                               ;
;           salvar              ;
;                               ;
;*******************************;
salvar proc near

        push ds

        push cs
        pop ds

        call salvar_            ;
        call grabar             ;

        pop ds

        ret
salvar endp

;*******************************;
;           salvar_             ;
;*******************************;
salvar_ proc near

        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds

        push cs
        pop ds

        mov al,ecol             ; a salvar valores
        mov ah,erow
        push ax

        mov ecol,7
        mov erow,16
        call irpos

        mov ah,8
        xor bh,bh               ;pagina, aunque siempre sea 0.
        int 10h                 ;obtiene char en question
        mov carac,al

        mov si,offset char      ;offset de la tabla

        mov erow,0
salir2:
        mov sind,80h
        mov ecol,0
teparece:
        call irpos              ;poscicionar cursor

        mov ah,8
        xor bh,bh               ;pagina 0
        int 10h                 ;obtener caracter

        cmp al,01h              ;charoff new on v4.00
        je bit_off

bit_on:
        mov al,sind
        or [si],al
        jmp salir1
bit_off:
        mov al,0ffh
        sub al,sind
        and [si],al

salir1:
        clc                     ;clear carry
        shr sind,1              ;rota, roto 8 veces
        jc fincol               ;si entonces ir a fincol

        inc ecol                ;no, entonces a la siguiente columna
        jmp teparece            ;vamos
fincol:
        inc erow                ;siguiente fila
        cmp erow,16             ;ya hizo 16 filas ?
        je salirend             ;si entonces podriamos salvar la info

        inc si                  ;no todavia no
        jmp salir2              ;vuelta a empezar todo

salirend:
        pop ax                  ;restaura valores originales
        mov ecol,al
        mov erow,ah

        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax

        ret

salvar_ endp

;*******************************;
;                               ;
;           grabar              ;
;                               ;
;*******************************;

grabar proc near

        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds

        push cs
        pop ds

        mov al,ecol             ; a salvar valores
        mov ah,erow
        push ax

        mov ecol,7
        mov erow,16
        call irpos

        mov ah,8
        xor bh,bh               ;pagina, aunque siempre sea 0.
        int 10h                 ;obtiene char en question
        mov carac,al

        mov bp,offset char
        push cs
        pop es                  ;es:bp ->user table

        mov bh,10h              ;numer of bytes per char
        mov cx,1                ;numer of patterns to store

        xor dh,dh
        mov dl,carac            ;offset del coso este
        xor bl,bl               ;block to load in map 2
        mov ax,1100h
        int 10h                 ;Cargar valor en la tabla

        pop ax                  ;restaura valores originales
        mov ecol,al
        mov erow,ah

        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax

        ret

grabar endp

;*******************************;
;*******************************;
;           editar              ;
;*******************************;
;*******************************;

editar_:
        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds
        push es

        push cs
        pop ds

        mov dh,MODOY
        mov dl,MODOX
        mov ah,2
        xor bh,bh
        int 10h
        mov dx,offset edita_msg
        mov ah,9
        int 21h

        mov erow,0
        mov ecol,0

edit_loop:

        call irpos
        call getchar2

        cmp escan,ESQ
        je endedit
        cmp escan,AltF1
        je endeditti
        cmp escan,F1
        je endedit
        cmp escan,UP
        je goup
        cmp escan,DOWN
        je godown
        cmp escan,LEFT
        je goleft
        cmp escan,RIGHT
        je goright
        cmp escan,SPACE
        je xorchar
        cmp escan,TECLA1                ;new on v2.99
        je bitton
        cmp escan,TECLA2                ;new on v2.99
        je bittoff
        call comun
        jmp edit_loop

endedit: mov retpoint,offset caracter
        jmp findeeditar         ;saltar a main loop
endeditti: mov retpoint,offset tipear
        jmp findeeditar
goup:    jmp go_up
godown:  jmp go_down
goleft:  jmp go_left
goright: jmp go_right
xorchar: jmp xor_char
bitton:  jmp bit_ton
bittoff: jmp bit_toff
gotipear2: mov retpoint,offset editar
         jmp editar
go_up:
        dec erow
        cmp erow,0ffh
        jne go_up_
        mov ah,totrow1          ;modificado (15)
        mov erow,ah             ;principio
go_up_:
        jmp edit_loop

go_left:
        dec ecol
        cmp ecol,0ffh
        jne go_left_
        mov ecol,7
go_left_:
        jmp edit_loop

go_down:
        inc erow
        mov ah,totrow1
        inc ah
        cmp erow,ah             ;modificado (16)
        jb go_down_
        mov erow,0              ;principio
go_down_:
        jmp edit_loop

go_right:
        inc ecol
        cmp ecol,8
        jne go_right_
        mov ecol,0
go_right_:
        jmp edit_loop

xor_char:
        mov ah,08h
        xor bh,bh               ;pagina 0
        int 10h                 ;get char and attribute
        xor al,XOR_VALUE
        mov bl,ah
        mov cx,1
        mov ah,9
        int 10h                 ;write char and attribute
        jmp edit_loop

bit_ton:                        ;Si aprieta 1
        mov ah,8
        xor bh,bh
        int 10h
        mov al,178              ;'�'
        mov bl,ah
        mov cx,1
        mov ah,9
        int 10h
        jmp edit_loop

bit_toff:                       ;Si aprieta 2
        mov ah,8
        xor bh,bh
        int 10h
        mov al,01h
        mov bl,ah
        mov cx,1
        mov ah,9
        int 10h
        jmp edit_loop

findeeditar:
        pop es
        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax

        jmp cs:[retpoint]

;*******************************;
;*******************************;
;           tipear              ;
;*******************************;
;*******************************;

tipear proc near

        push ax
        push bx
        push cx
        push dx
        push di
        push si
        push ds
        push es

        push cs
        pop ds

        mov ftipear,1

        mov ah,3
        xor bh,bh
        int 10h
        push dx                 ;read cursor position

        mov dh,MODOY
        mov dl,MODOX
        mov ah,2
        xor bh,bh
        int 10h
        mov dx,offset tipea_msg
        mov ah,9
        int 21h

        mov ah,flaga
        push ax

        mov row2,19             ;fila
        mov col2,0              ;columna

tipear_loop:
        call irpos2
        call getchar2

        call comun

        mov flaga,0             ;Flag usado tambien por GetChar
        mov ax,escan            ;
        cmp al,0                ;
        je flagggon             ;
        cmp al,0e0h             ;    detecta si se apreto tecla especial
        jne flagggoff           ;
flagggon:                       ;
        mov flaga,1             ;
flagggoff:
        cmp escan,ESQ
        je endtipear
        cmp escan,AltF1
        je endtipear
        cmp escan,F1
        je endtipeared
        cmp escan,UP
        je goarr
        cmp escan,DOWN
        je goaba
        cmp escan,LEFT
        je goizq
        cmp escan,RIGHT
        je goder
        cmp escan,BACKS
        je bspace
        cmp escan,TAB
        je gotab
        cmp escan,INTRO
        je genter
        cmp escan,AltF3
        je salasc

;                       ;cualquier otra tecla la escribe
        cmp flaga,1
        jne acapibe     ;tecla especial, no mostrar ir al comienzo
        jmp tipear_loop
acapibe:
        call mouse_hide         ;Bug Fix del mouse

        mov ax,escan
        mov ah,0ah
        mov cx,1
        xor bh,bh               ;pagina 0
        int 10h

        call mouse_show         ;Bug Fix del mouse

        call go_der
        jmp tipear_loop

endtipear: mov retpoint,offset caracter
        jmp end_tipear
endtipeared: mov retpoint,offset editar_
        jmp end_tipear
goarr:  call go_arr
        jmp tipear_loop
goaba:  call go_aba
        jmp tipear_loop
goizq:  call go_izq
        jmp tipear_loop
goder:  call go_der
        jmp tipear_loop
bspace: call b_space
        jmp tipear_loop
gotab:  call go_tab
        jmp tipear_loop
genter: call g_enter
        jmp tipear_loop
salasc: call sal_asc
        jmp tipear_loop
clearwin: call clear_win
        jmp tipear_loop
seeascii: call see_ascii
        jmp tipear_loop

go_arr:
        dec row2
        cmp row2,18             ;te pasastes
        jne go_arr_
        mov row2,23             ;principio
go_arr_:
        ret
go_izq:
        dec col2
        cmp col2,0ffh
        jne go_izq_
        mov col2,79
        jmp short go_arr
go_izq_:
        ret
go_aba:
        inc row2
        cmp row2,24
        jne go_aba_
        mov row2,19             ;principio
go_aba_:
        ret
go_der:
        inc col2
        cmp col2,80
        jne go_der_
        mov col2,0
        jmp short go_aba
go_der_:
        ret
b_space:
        call go_izq
        call irpos2
        mov ah,0ah
        mov al,32               ;espacio
        mov cx,1
        xor bh,bh               ;pagina 0
        int 10h
        ret

go_tab:
        push cx
        mov cl,8
again_tab:
        call go_der
        dec cl
        or cl,cl
        jne again_tab
        pop cx
        ret

g_enter:
        mov col2,0
        jmp go_aba

;*******************************;
;       sal_asc                 ;
;*******************************;
sal_asc proc near               ;**************** salvar el archivo ASCII
        mov ah,48h              ;allocate memory
        mov bx,26               ;paragraph to allocate
        int 21h                 ;allocate memory
        jnc bibon               ;no hubo error parece
        call sound
        call sound              ;
        call sound              ;problema de memoria
        ret                     ;vuelve fracasado
bibon:
        call grabando

        mov es,ax               ;Segmento destino
        mov ax,0b800h           ;Segmento fuente
        mov ds,ax               ;
        mov si,0be0h            ;offset
        xor di,di               ;DS:SI -> ES:DI
        xor cl,cl
lupeandoo:
        xor ch,ch
        cld                     ;clear direction flag
lupeando1:
        movsb                   ;mueve todo aca!
        inc si
        inc ch
        cmp ch,80               ;ya movio 80
        jne lupeando1

        mov es:[di],0a0dh       ;mueve ,13,10
        inc cl                  ;inc 1
        add di,2                ;le suma 2
        cmp cl,5                ;5 lineas?
        jne lupeandoo           ;la info ya esta es es:di

        push cs                 ;
        pop ds                  ;
        mov ah,3ch              ;create a file
        xor cx,cx               ;file attribute
        mov dx,offset ascname
        int 21h
        jnc nonou               ;carry =1 =error
        call sound              ;hubo error, entonces no seguir
        jmp ascsavsal
nonou:
        mov ch,ascname+7        ;proximo sera 00000001.asc
        cmp ch,'9'              ;Esto es por un nombre es rrrrrrrr
        jbe forrazo             ;es proximo sera rrrrrrrr0
        mov ch,'0'
        jmp short aboab
forrazo:
        inc ch                  ;incrementa 1
        and ch,00000111b        ;maximo permitido en 7
        or ch,30h               ;le agrega 30h
aboab:
        mov ascname+7,ch

        mov bx,ax               ;move file handle
        mov ah,40h              ;write to file
        mov cx,410              ;410 bytes to be written
        mov dx,0b800h           ;segmento
        push es
        pop ds                  ;ds:dx
        xor dx,dx               ;offset
        int 21h                 ;write them pleaseeee
        jnc nonou2
        call sound              ;hubo error... chillar
nonou2:
        mov ah,3eh              ;close file handle
        int 21h                 ;
ascsavsal:
        push cs                 ;restaura valores cambiados
        pop ds

        mov ah,49h              ;Free allocated memory
        int 21h                 ;en ES tiene que estar el segmento y esta!

        call sound
        call viewfile
        ret
sal_asc endp

end_tipear:                     ;******************************* TIPEAR END

        pop ax
        mov flaga,ah

        pop ax                  ;restaurar cursor position
        mov erow,ah
        mov ecol,al
        call irpos
        mov ftipear,0

        pop es
        pop ds
        pop si
        pop di
        pop dx
        pop cx
        pop bx
        pop ax

        jmp cs:[retpoint]

tipear endp

;******************************;
;       getchar2               ;
;******************************;

getchar2 proc near

        push ax
        push ds

        push cs
        pop ds

        call macro_wri          ;es un macro lo que se esta ejecutando
        cmp macroW,1
        je acaviejo2            ;en ax esta el valor...

        mov ah,11h
        int 16h
        cmp al,1
        je bullshit             ;no se apreto ninguna tecla.

        mov ah,10h
        int 16h                 ;Keaboard Read
        call macro_rea

acaviejo2:
        mov escan,ax

bullshit:
        pop ds
        pop ax

        ret

getchar2 endp

;*******************************;
;       sound                   ;
;*******************************;
sound proc near

        push ds

        push cs
        pop ds

        cmp flagb,0
        jne noni_noni

        mov ah,2
        mov dl,7
        int 21h                 ;output a Bell
noni_noni:

        pop ds

        ret

sound endp


;*******************************;
;       irpos                   ;
;*******************************;
irpos proc near
        push ax
        push bx
        push dx
        push ds
        push cs
        pop ds
        mov ah,2
        xor bh,bh               ;pagina 0
        mov dh,erow
        mov dl,ecol
        int 10h                 ;posciciona el cursor
        pop ds
        pop dx
        pop bx
        pop ax
        ret
irpos endp

;*******************************; new on  v3.00b
;       irpos2                  ;
;*******************************;
irpos2 proc near
        push ax
        push bx
        push dx
        push ds
        push cs
        pop ds
        mov ah,2
        xor bh,bh               ;pagina 0
        mov dh,row2
        mov dl,col2
        int 10h                 ;posciciona el cursor
        pop ds
        pop dx
        pop bx
        pop ax
        ret
irpos2 endp

;**********************;
; wait15s              ;
;**********************;
wait15s proc near
    push cs
    pop ds
    mov ah,9
    lea dx,waitmsg
    int 21h
    mov flagd,0
    mov tiempo,255
waiting:
    cmp tiempo,0
    jne waiting
    ret
wait15s endp
;******************************;
;       finalisimo             ;
;******************************;
finalisimo:
        mov ax,0
        int 33h                 ;reset mouse

        call mouse_hide         ;hide mouse cursor
        call doubleno           ;restaura chars.
        call dacolores          ;restaura dac palette
        call clrscr

        push cs
        pop ds

        cmp flagc,1             ;Estoy registrado
        je saltea_2

        mov ax,1301h            ;Write Character String
        xor bh,bh               ;pagina 0
        mov bl,30h              ;atributo
        mov cx,21 * 80           ;longitud
        mov dh,0                ;fila
        mov dl,0                ;columna
        push cs
        pop es                  ;es:bp -> string
        mov bp,offset msg2
        int 10h

        call wait15s            ;espera15segundos.

saltea_2:
        call u_int1c            ;desinstala la interrupcion 1ch

        mov ah,49h
        mov bx,segme25
        mov es,bx
        int 21h                 ;free allocated memory

        mov ah,49h
        mov bx,segme50
        mov es,bx
        int 21h                 ;free allocated memory

        mov ah,49h
        mov bx,macro_seg
        mov es,bx
        int 21h                 ;free allocated memory

        mov ax,4c00h
        int 21h                 ;Return to DOS.

;***************************************;
;               i_int1c                 ;
;***************************************;
i_int1c proc near

        push ax
        push bx
        push dx
        push si
        push ds
        push es

        push cs
        pop ds

        mov ax,351ch                    ;INT 1CH
        int 21h
        mov si,offset old1c
        mov [si],bx
        mov [si+2],es
        mov dx,offset new1c
        mov ax,251ch
        int 21h

        pop es
        pop ds
        pop si
        pop dx
        pop bx
        pop ax

        ret

i_int1c endp

;***************************************;
;               u_int1c                 ;
;***************************************;
u_int1c proc near

        push ax
        push dx
        push si
        push ds

        push cs
        pop ds

        mov si,offset old1c             ;restaura valor original
        mov dx,[si]
        mov ax,[si+2]
        mov ds,ax
        mov ax,251ch
        int 21h

        pop ds
        pop si
        pop dx
        pop ax

        ret

u_int1c endp

;***************************************;
;       interrupcion 1ch                ;
;***************************************;
new1c:
        pushf
        dec cs:tiempo           ;flag de espera
        inc cs:flage            ;flag de espera

        cmp cs:flagd,0          ;permite la animacion
        je noanimplis

        call animacion
        inc cs:animac           ;counter de la animacion

noanimplis:
        popf
        jmp cs:[old1c]

;***************************************; Alt F11
;       gen_table                       ;
;***************************************;
gen_table proc near

        push cs
        pop es
        push cs
        pop ds

        cmp totrow1,7
        jne era_chars
        mov bp,offset tabla_riq
        mov bh,8
        jmp short donriq

era_chars:
        mov bp,offset tabla_erasoft
        mov bh,10h              ;numer of bytes per char
donriq:
        mov cx,100h             ;numer of patterns to store
        xor dx,dx               ;a partir de que char.
        xor bl,bl               ;block to load in map 2
        mov ax,1110h
        int 10h                 ;Cargar valor en la tabla

        call infovga            ;new on v2.60

        mov al,cs:carr
        mov cs:carac,al
        call verchar
        ret
gen_table endp

;***************************************; Alt F11
;       gen_table2                      ;
;***************************************; New on v3.00b
gen_table2 proc near

        push cs
        pop es
        push cs
        pop ds

        cmp totrow1,7
        jne era_chars2
        mov bp,offset tabla_riq
        mov bh,8
        jmp short donriq2

era_chars2:
        mov bp,offset tabla_erasoft
        mov bh,10h              ;numer of bytes per char
donriq2:
        mov cx,100h             ;numer of patterns to store
        xor dx,dx               ;a partir de que char.
        mov bl,1                ;block to load in map 2
        mov ax,1110h
        int 10h                 ;Cargar valor en la tabla

        call infovga            ;new on v2.60
        ret
gen_table2 endp

;***************************************;
;       a8bit                           ;
;***************************************;
a8bit proc near

;1er paso, la frecuencia de 28MHz a 25MHz
        mov dx,3cch             ;cambia la frecuencia del monitor
        in al,dx
        mov ah,al
        and al,11110011b        ;borra lo que halla
        and ah,00000100b
        xor ah,00000100b
        or al,ah
        mov dx,3c2h
        out dx,al

;2do paso --- el reset y el clock
        cli
        mov dx,3c4h
        mov ax,100h
        out dx,ax

        mov dx,3c4h
        mov al,1
        out dx,al
        inc dx
        in al,dx
        and al,00000001b
        xor al,00000001b
        mov cs:bit89,al         ;mueve a Bit89
        out dx,al

        mov dx,3c4h
        mov ax,300h             ;Enable the controller
        out dx,ax               ;
        sti

;3er y ultimo paso
        mov ax,1000h            ;interrupcion
        mov bl,13h
        mov bh,0                ;por default para 8bit
        cmp cs:bit89,1
        je intt89
        mov bh,8
intt89:
        int 10h                 ;posciciona bien la pantalla

        ret
a8bit endp

db 13,10,'I love a mi Pipi Cucu.',13,10

datos equ this byte
;******************************;
;                              ;
;******************************;
;   data     data      data    ;
;******************************;
;                              ;
;******************************;
codigocaca label byte
db 13,10,13,10,13,10
db 'Ricardo Quesada informa:',13,10
db '    No me gusta que miren mi programa, y menos si no estas registrado.',13,10
db '    OK ?... Entonces si queres seguir mirando mi programa mandame 15 dolares',13,10
db '    y despu�s hablamos. (Adem�s no es EL programa para debugear, � ah ?)',13,10,13,10

srmap   db ?            ;Sequencer Register, Read Map
srmem   db ?            ;                  , Memory Mode
grmap   db ?            ;Graphics Register, Read Map
grmod   db ?            ;                 , Graph Mode
grmis   db ?            ;                 , Miscellaneous


scancod dw ?            ;scancode in question
carr    db ?            ;copia del carac (usada por ALT F11 y ALT F12) v1.70
carac   db ?            ;char en cuestion.
ascii   db 0            ;****
ascii_str db','         ;   *
dbyte3  db'0'           ;   *  valores usados por la f  verascii.
dbyte2  db'0'           ;   *
dbyte1  db'0$'          ;****
char    db 32 dup( ? )  ;valores del char a ver (vendria a ser la tabla )
char_   db 32 dup( ? )  ;valores usados para rotar, vio ?
char__  db 32 dup( ? )  ;valores usado por F9.
row     db 0            ;poscicion - fila
col     db 0            ;          - columna
row2    db 0            ; new on v3.00b
col2    db 0            ;  "  "   "
char2   db 0            ;char de relleno - 178 (�) o 32 (space) -
color   db 01110101b    ;color del char
segme   dw 0            ;segmento usado para allocar memoria
segme50 dw 0            ;allocmem 8x8
segme25 dw 0            ;allocmem 8x16
flaga   db 0            ;si esta en 0 no muestra caracter. (Especial)
flagb   db 0            ;si habilita sound.
flagc   db 0            ;si esta 1 esta registrado
flagd   db 0            ;si esta 1 permite animacion
flage   db 0            ;este siempre se incrementa. usado por Salvar
flagf   db 1            ;Si esta en 1, Chequea si hay mousee
tiempo  db 0            ;tiempo de espera (usado en 1ch)
bit89   db 0            ;valor inicial de la concha de mierda(toggle 8/9 bit width)
inic    db 0            ;0=puede inicializar
animac  db 0            ;counter de la velocidad de la animacion !
speedy  db 0            ;velocidad de la animacion. 0=max 18=min
speedy2 db 0            ;otro counter mas para la animacion (hombre corriendo)
handle  dw 0            ;manejo de archivos via handle
sav50   db 0            ;flag de para ver si se salvaron a mem los fonts 8x8
sav25   db 0            ; "                                              8X16
macroR  db 0            ;1=Se esta leyendo el macro.
macroW  db 0            ;1=Se esta escribiendo el macro.
macro_off dw 0          ; * es:di -> apuntan al buffer del macro
macro_seg dw 0          ; *
ftipear db 0            ;new on 3.00b - Se fija si se estan el Edit de Logo=1
fgrid   db 0            ;new on v4.00 - Flag del grid
retpoint dw 0           ;usada por tipear para volver al origen! new on v4.00
macromsg1 label byte
db'Grabando macro. Apretar Alt F9 para terminar.'

waitmsg label byte
db 13,10,13,10,'Amigo mio, o 15 dolares o 15 segundos. Te estoy esperando...$'

dname   db '     F3: '
fname   db '00000000.COM',0,' '       ;nombre del ejecutable
        db '    Ctrl F3: '
asmname db '00000000.ASM',0,' '       ;nombre del .asm
        db '     Alt F3: '
ascname db '00000000.ASC',0,'$'         ;nombre de los grafiquitos ASCII

graban  db '                                   GRABANDO                                    $'

old1c   dd 0            ;poscicion original de la INT 1CH

; * valores del edit * ;
escan   dw ?            ;scancode used during edition.
erow    db 0            ;pos fila
ecol    db 0            ;    col
echar   db 219          ;char de relleno
ecolor  db 17h          ;color del char

; * valores del salvar * ;
sind    db 0            ;indice, tiene valores (80,40,20,10...)

; * valores del mouse * ;
active  db 0                            ;mouse event handler active = 1
mousechar db 0                          ; 178 o 32
mouseb  dw 0                            ;buttons del mouse
mousex  db 0                            ;coor del mouse
mousey  db 0                            ;
mousex1 db 0                            ;valores entre los cuales esta
mousex2 db 0
mousey1 db 0
mousey2 db 0
mcolor  db 0                            ;color del dibujo

; * valores de la tabla de info de vga int10,1bh * ;

totrow1 db ?            ;usada por DISCHAR y BIGSCREEN
totrow2 dw ?            ;usada por BigScreen
totrow3 db ?            ;usada por Resori and Co.
totrow4 db ?            ;usada por Sal_asm (salteador de espacios chotos) y SAVE
totrow5 db ?            ;usada por SAVE
totrow6 dw ?            ;cantidad de bytes a grabar por tabla.
totrow7 db ?            ;usado por FLIP
totrow8 dw ?            ;usado en generacion de .asm

red5    db 0            ;colores originales de la paleta!
gre5    dw 0
red56   db 0
gre56   dw 0
red57   db 0
gre57   dw 0

head_msg db 13,10,'- VChar NI v3.10 (c) Ricardo Quesada 1994 -',13,10,'$'
vga_msg  db 13,10,'Lo siento viejita, VGA only!',13,10,'$'
help_msg db 13,10,'VChar [nombre] [/opciones]',13,10
         db '  [nombre] Nombre con que se grabar�n los archivos. La extensi�n var�a',13,10
         db '           seg�n lo que se grabe (.com ,.asm o .asc ).',13,10
         db '           Default = 00000000 . M�x 8 letras .',13,10
         db '   /s?     Cuando ?=0 Deshabilita el sonido.',13,10
         db '                  ?=1 Habilita el sonido.',13,10
         db '   /m?     Cuando ?=0 Deshabilita el mouse.',13,10
         db '                  ?=1 Habilita el mouse (solo si existe).',13,10
         db '   /a      Remueve cualquier tipo de compactaci�n o virus que tenga VChar.',13,10
         db "           (Built-in Ricardo Quesada's antivirus (c) 1993 ).",13,10
         db 13,10,'Para m�s informaci�n leer VChar.DOC',13,10,'$'
char_msg db 'A$'
menu_msg db 13,10,13,10,13,10
         db '                              VChar /H para m�s ayuda',13,10
         db '          F1: Toggles Modo Edit/Caracter        ShiftF1: Ascii Table',13,10
         db '          AltF1: Toggles  Texto/Caracter        CtrlF1: Clear Text Window',13,10
         db '          F2: Salva char                        SPACE: Bit On/Off  1:On  2:Off',13,10
         db '          F4: Animaci�n (Si/No)                 ESC: Salir (modo Caracter)',13,10,13,10
         db '          F5: Izq/Der   AltF5: Rota             F9: Copy       AltF9: Rec Macro'
         db '          F6: Up/Down   AltF6: Grid           F10: Paste     AltF10: PlayMacro'
         db '          F7: On/Off                           F11: 8/9 bits  AltF11: Era-Riq',13,10
         db '          F8: Borra                             F12: 25/50 row AltF12: Original',13,10,13,10
         db '          CURSORES BLANCOS (keypad):  scroll char (NumLock=OFF).',13,10
         db '          CURSORES GRISES:  Arr/Aba = sig/ant char  Izq/Der = Vel. animaci�n',13,10
         db '            <- apreta tecla o c�digo ascii (Modo CARACTER)'
         db '$'

carac_msg db'MODO CARACTER$'
edita_msg db'  MODO EDIT  $'
tipea_msg db' MODO TEXTO  $'

msg1 label byte
db' Click any key (inside this window)... yo se porque te lo digo.                 '
db' Man, escucha... Que maravilloso que es este programa.                          '
db' Este soft es porte�o ( Rep�blica Argentina ).                                  '
db'                                                         ...simplemente �����   '
db'                                                                        �����   '

msg2 label byte
db'                                 VChar NI v3.10                                 '
db'                      Editor de caracteres para placas VGA                      '
db'                            (c) Ricardo Quesada 1994                            '
db'��������������������������������������������������������������������������������'
db'REGISTRACION: VChar es un soft SHAREWARE. Significa que si Ud. continua us�ndolo'
db'deber� registrarlo.� C�mo ?.Enviando U$S 15 (quince d�lares) o su equivalente a:'
db'     Quesada Ricardo                                                            '
db'     Billinghurst 2444 piso 6B                                                  '
db'     C�digo Postal: 1425 - Capital Federal                                      '
db'     Buenos Aires, Argentina                                                    '
db'     Telefono: (541)-805-1247                                                   '
db'��������������������������������������������������������������������������������'
db'Si Ud. registra esta versi�n me estimular� para que siga continuando y mejorando'
db'el producto. Adem�s recibir� sin cargo las pr�ximas versiones.                  '
db'La versi�n registrada genera la tabla de caracteres .ASM completa, m�s el fuente'
db'de como pasar a 8 � 9 bits de ancho... todo en un archivo listo para compilar y '
db'linkear sin tener que modificar nada !                                          '
db'��������������������������������������������������������������������������������'
db'Mis agradecimientos van para: Alessia, Toro, Nacho, para todo los chicos del SMT'
db'(Javi,Emi,Santi,Edy),para los betatesters, y principalmente a los usuarios...   '
db'      ...registrados :-)             - Ricardo Quesada (4:900/309.3@fidonet.org)'

msg3 equ this byte
db'                                  �  �  �        � �                            '
db'                                  �  �  �  �  �  � �  ����                      '
db'                                �� Ricardo Quesada 1994                         '

msg4 equ this byte
db'                       V E R S I O N    S H A R E W A R E                     '
regmsg equ this byte
db'Versi�n registrada a: '
nombrereg equ this byte
db'                                                        '

msg5 equ this byte
db'������������� � ������������� � ������������� � ������������� � ������������� � '

registramsg equ this byte
db 13,10,'Registrando... $'
reg_ok_msg equ this byte
db 'OK.',13,10,'Gracias por tu aporte.',13,10,'$'
reg_not_ok equ this byte
db 'ERROR',13,10,'Verifique que su disco no este protegido.Gracias.',13,10,'$'
rebotar equ this byte
db 13,10,'Atenci�n: Esta versi�n ya fue registrada. No se puede volver a registrar.',13,10,'$'
unregmsg equ this byte
db 13,10,'Riq & Cuc',13,10,'$'
prean_msg label byte
db 13,10,'Tratando de remover cualquier tipo de virus o compactaci�n...$'
antiv_msg equ this byte
db'OK',13,10,'SUGERENCIA: BOOTEE LA COMPUTADORA AHORA MISMO YA QUE EL VIRUS DEBE ESTAR',13,10,'RESIDENTE.',13,10,'$'
alloc_error label byte
db 13,10,'Error: Necesita por lo menos 18k de memoria libre para correr.',13,10
      db 'Para usarlo con todas las funciones, libere 26k.',13,10,'$'
ansimsg label byte
db 13,10,'Error: Lamentablemente, por razones que no voy a explicar VChar NI 3.10,',13,10
db 'no funciona con el driver AnsiPlus cargado.',13,10,'$'
;***************************************;
;***    data de asm                  ***;
;***************************************;
share1  db';VChar NI v3.10 (c) Ricardo Quesada 1994',13,10
        db';- SHAREWARE VERSION -',13,10
        db';La versi�n registrada crea un archivo READY-TO-COMPILE-AND-LINK',13,10
        db';en assembler de la tabla entera de los c�digos ASCII.',13,10
        db';Por favor, si continua usando este soft, registrelo !',13,10
        db';Lo siguiente es la tabla incompleta - C�digos ascii del 200 al 255.',13,10,13,10
share2  equ this byte

princi  db';VChar NI v3.10 (c) Ricardo Quesada 1994',13,10
        db';Tabla de caracteres generada en assembler',13,10
        db';Cualquier duda acerca del c�digo por favor hagamela llegar.',13,10
        db';Compilar de la siguiente manera:',13,10
        db';TASM nombre',13,10
        db';TLINK /t nombre',13,10,13,10
        db"code    segment public byte 'code'",13,10
        db'org     100h',13,10
        db'VChar   PROC',13,10
        db'assume cs:code,ds:code,ss:code,es:code',13,10,13,10
        db'start:',13,10
        db'mov bp,offset TABLA',13,10
        db'mov bh,'
marica  dw ?
        db 'h           ;cantidad de bytes x char',13,10
        db'mov cx,100h          ;total de chars (256)',13,10
        db'xor dx,dx            ;a partir de que char (0).',13,10
        db'xor bl,bl            ;map 2.Grabar en 1er tabla',13,10
        db'mov ax,1110h         ;Define los carecteres creado por el usuario.',13,10
        db'int 10h',13,10,13,10
        db';call bit8           ;peque�o ejemplo',13,10,13,10
        db'mov ah,4ch',13,10
        db'int 21h',13,10

        db 13,10,13,10,13,10,';Funci�n de 8 bits',13,10
        db';bit8 proc',13,10,13,10
        db';mov dx,3cch',13,10
        db';in al,dx',13,10
        db';and al,11110011b',13,10
        db';mov dx,3c2h',13,10
        db';out dx,al',13,10,13,10
        db';cli',13,10
        db';mov dx,3c4h',13,10
        db';mov ax,100h',13,10
        db';out dx,ax',13,10,13,10
        db';mov ax,101h',13,10
        db';out dx,ax',13,10,13,10
        db';mov ax,300h',13,10
        db';out dx,ax',13,10
        db';sti',13,10,13,10
        db';mov ax,1000h',13,10
        db';mov bl,13h',13,10
        db';mov bh,0',13,10
        db';int 10h',13,10
        db';ret',13,10,13,10
        db';bit8 endp',13,10
        db 13,10,13,10,13,10,';Funci�n de 9 bits',13,10
        db';bit9 proc',13,10,13,10
        db';mov dx,3cch',13,10
        db';in al,dx',13,10
        db';and al,11110011b',13,10
        db';or al,00000100b',13,10
        db';mov dx,3c2h',13,10
        db';out dx,al',13,10,13,10
        db';cli',13,10
        db';mov dx,3c4h',13,10
        db';mov ax,100h',13,10
        db';out dx,ax',13,10,13,10
        db';mov ax,001h',13,10
        db';out dx,ax',13,10,13,10
        db';mov ax,300h',13,10
        db';out dx,ax',13,10
        db';sti',13,10,13,10
        db';mov ax,1000h',13,10
        db';mov bl,13h',13,10
        db';mov bh,8',13,10
        db';int 10h',13,10
        db';ret',13,10,13,10
        db';bit9 endp',13,10,13,10,13,10

        db'TABLA label byte',13,10
fffinal equ this byte

princi2 db 13,10,13,10,'VChar   endp',13,10
        db'code    ends',13,10
        db'        end VChar',13,10
fffina2 equ this byte

defbyt  db 13,10,'db '
asc2    db 0            ;valor temporal de ascii
asc3    db 0
        db ' ;ascii '
numero  db '000,'
datab   db 0            ;valor temporal de la tabla
;
;
;final de los datos de la tabla asm!

offgrid16 db 16 dup(0)
ongrid16 db 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,255
offgrid8  db 8 dup(0)
ongrid8 db 128,128,128,128,128,128,128,255

tabla_erasoft equ this byte
db 000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 000
grid16 db 000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 001
db 000,000,126,255,219,255,255,195,231,255,255,126,000,000,000,000 ;ascii 002
db 000,000,000,000,108,254,254,254,254,124,056,016,000,000,000,000 ;ascii 003
db 000,000,000,000,016,056,124,254,124,056,016,000,000,000,000,000 ;ascii 004
db 000,000,000,024,060,060,231,231,231,153,024,060,000,000,000,000 ;ascii 005
db 000,000,000,024,060,126,255,255,126,024,024,060,000,000,000,000 ;ascii 006
db 000,000,000,000,000,000,024,060,060,024,000,000,000,000,000,000 ;ascii 007
db 255,255,255,255,255,255,231,195,195,231,255,255,255,255,255,255 ;ascii 008
db 000,000,000,000,000,060,102,066,066,102,060,000,000,000,000,000 ;ascii 009
db 255,255,255,255,255,195,153,189,189,153,195,255,255,255,255,255 ;ascii 010
db 000,000,030,014,026,050,120,204,204,204,204,120,000,000,000,000 ;ascii 011
db 000,000,060,102,102,102,102,060,024,126,024,024,000,000,000,000 ;ascii 012
db 000,001,062,060,048,048,048,048,048,112,240,224,000,000,000,000 ;ascii 013
db 000,006,030,118,110,118,102,102,102,110,238,236,224,000,000,000 ;ascii 014
db 000,000,000,024,024,219,060,231,060,219,024,024,000,000,000,000 ;ascii 015
db 000,000,000,000,000,008,012,014,255,014,012,008,000,000,000,000 ;ascii 016
db 000,000,000,000,000,016,048,112,255,112,048,016,000,000,000,000 ;ascii 017
db 000,000,024,060,126,024,024,024,024,126,060,024,000,000,000,000 ;ascii 018
db 000,000,102,102,102,102,102,102,102,000,102,102,000,000,000,000 ;ascii 019
db 000,000,127,219,219,219,123,027,027,027,027,027,000,000,000,000 ;ascii 020
db 000,124,198,096,056,108,198,198,108,056,012,198,124,000,000,000 ;ascii 021
db 000,000,000,000,000,000,000,000,254,254,254,254,000,000,000,000 ;ascii 022
db 000,000,024,060,126,024,024,024,024,126,060,024,126,000,000,000 ;ascii 023
db 000,000,024,060,126,024,024,024,024,024,024,000,000,000,000,000 ;ascii 024
db 000,000,000,000,024,024,024,024,024,024,126,060,024,000,000,000 ;ascii 025
db 000,000,000,000,000,000,004,006,255,006,004,000,000,000,000,000 ;ascii 026
db 000,000,000,000,000,000,032,096,255,096,032,000,000,000,000,000 ;ascii 027
db 000,000,000,000,000,192,192,192,192,254,000,000,000,000,000,000 ;ascii 028
db 000,000,000,000,000,040,108,254,108,040,000,000,000,000,000,000 ;ascii 029
db 000,000,000,000,016,056,056,124,124,254,254,000,000,000,000,000 ;ascii 030
db 000,000,000,000,254,254,124,124,056,056,016,000,000,000,000,000 ;ascii 031
db 000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 032
db 000,024,024,024,024,024,024,024,000,024,060,024,000,000,000,000 ;ascii 033
db 000,000,000,102,102,102,098,000,000,000,000,000,000,000,000,000 ;ascii 034
db 000,000,000,108,108,254,108,108,108,254,108,108,000,000,000,000 ;ascii 035
db 000,024,024,126,219,217,220,126,027,027,219,126,024,024,000,000 ;ascii 036
db 000,000,000,000,194,198,012,024,048,096,198,134,000,000,000,000 ;ascii 037
db 000,000,056,108,108,056,118,220,204,204,204,118,000,000,000,000 ;ascii 038
db 000,000,000,024,024,024,048,000,000,000,000,000,000,000,000,000 ;ascii 039
db 000,000,000,000,006,012,024,024,024,024,012,006,000,000,000,000 ;ascii 040
db 000,000,000,000,096,048,024,024,024,024,048,096,000,000,000,000 ;ascii 041
db 000,000,000,000,000,000,102,060,254,127,060,102,000,000,000,000 ;ascii 042
db 000,000,000,000,000,024,024,024,255,024,024,024,000,000,000,000 ;ascii 043
db 000,000,000,000,000,000,000,000,000,000,024,024,048,000,000,000 ;ascii 044
db 000,000,000,000,000,000,000,000,254,000,000,000,000,000,000,000 ;ascii 045
db 000,000,000,000,000,000,000,000,000,000,024,024,000,000,000,000 ;ascii 046
db 000,000,000,000,002,006,012,024,048,096,192,128,000,000,000,000 ;ascii 047
db 000,000,000,056,108,198,198,222,230,198,108,056,000,000,000,000 ;ascii 048
db 000,000,000,024,120,024,024,024,024,024,024,060,000,000,000,000 ;ascii 049
db 000,000,000,124,198,198,006,012,056,096,194,254,000,000,000,000 ;ascii 050
db 000,000,000,254,198,012,056,012,006,006,198,124,000,000,000,000 ;ascii 051
db 000,000,000,012,028,060,108,204,254,012,012,030,000,000,000,000 ;ascii 052
db 000,000,000,254,192,192,252,006,006,006,198,124,000,000,000,000 ;ascii 053
db 000,000,000,124,198,192,252,198,198,198,198,124,000,000,000,000 ;ascii 054
db 000,000,000,254,006,012,012,024,124,048,096,096,000,000,000,000 ;ascii 055
db 000,000,000,124,198,198,124,198,198,198,198,124,000,000,000,000 ;ascii 056
db 000,000,000,124,198,198,198,198,126,006,102,060,000,000,000,000 ;ascii 057
db 000,000,000,000,000,000,024,024,000,000,024,024,000,000,000,000 ;ascii 058
db 000,000,000,000,000,000,000,000,000,000,024,024,000,000,024,048 ;ascii 059
db 000,000,000,000,000,000,014,056,255,056,014,000,000,000,000,000 ;ascii 060
db 000,000,000,000,000,000,254,000,000,254,000,000,000,000,000,000 ;ascii 061
db 000,000,000,000,000,000,112,028,014,028,112,000,000,000,000,000 ;ascii 062
db 000,000,124,198,198,006,014,024,024,000,024,024,000,000,000,000 ;ascii 063
db 000,000,000,124,198,198,222,222,222,220,192,124,000,000,000,000 ;ascii 064
db 000,000,000,124,198,198,198,254,198,198,198,198,000,000,000,000 ;ascii 065
db 000,000,000,248,204,204,252,198,198,198,198,252,000,000,000,000 ;ascii 066
db 000,000,000,124,198,198,192,192,192,198,198,124,000,000,000,000 ;ascii 067
db 000,000,000,252,198,198,198,198,198,198,198,252,000,000,000,000 ;ascii 068
db 000,000,000,252,192,192,192,248,192,192,192,252,000,000,000,000 ;ascii 069
db 000,000,000,254,192,192,192,252,192,192,192,192,000,000,000,000 ;ascii 070
db 000,000,000,124,198,192,192,252,198,198,198,124,000,000,000,000 ;ascii 071
db 000,000,000,198,198,198,198,254,198,198,198,198,000,000,000,000 ;ascii 072
db 000,000,000,060,024,024,024,024,024,024,024,060,000,000,000,000 ;ascii 073
db 000,000,000,030,012,012,012,012,204,204,204,120,000,000,000,000 ;ascii 074
db 000,000,000,198,198,204,204,248,204,204,198,198,000,000,000,000 ;ascii 075
db 000,000,000,192,192,192,192,192,192,192,192,254,000,000,000,000 ;ascii 076
db 000,000,000,198,238,254,214,198,198,198,198,198,000,000,000,000 ;ascii 077
db 000,000,000,198,230,246,222,206,198,198,198,198,000,000,000,000 ;ascii 078
db 000,000,000,124,198,198,198,198,198,198,198,124,000,000,000,000 ;ascii 079
db 000,000,000,252,198,198,198,252,192,192,192,192,000,000,000,000 ;ascii 080
db 000,000,000,124,198,198,198,198,198,214,222,124,012,006,000,000 ;ascii 081
db 000,000,000,252,198,198,198,252,198,198,198,198,000,000,000,000 ;ascii 082
db 000,000,000,124,198,192,192,124,006,006,198,124,000,000,000,000 ;ascii 083
db 000,000,000,126,024,024,024,024,024,024,024,024,000,000,000,000 ;ascii 084
db 000,000,000,198,198,198,198,198,198,198,198,124,000,000,000,000 ;ascii 085
db 000,000,000,198,198,198,198,198,198,108,056,016,000,000,000,000 ;ascii 086
db 000,000,000,198,198,198,198,198,214,254,238,198,000,000,000,000 ;ascii 087
db 000,000,000,198,198,198,108,056,108,198,198,198,000,000,000,000 ;ascii 088
db 000,000,000,102,102,102,102,102,060,024,024,024,000,000,000,000 ;ascii 089
db 000,000,000,254,006,012,024,124,048,096,192,254,000,000,000,000 ;ascii 090
db 000,000,060,048,048,048,048,048,048,048,048,060,000,000,000,000 ;ascii 091
db 000,000,000,128,192,224,112,056,028,014,006,002,000,000,000,000 ;ascii 092
db 000,000,060,012,012,012,012,012,012,012,012,060,000,000,000,000 ;ascii 093
db 016,056,108,198,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 094
db 000,000,000,000,000,000,000,000,000,000,000,255,000,000,000,000 ;ascii 095
db 000,000,000,204,204,204,204,000,000,000,000,000,000,000,000,000 ;ascii 096
db 000,000,000,000,000,000,060,102,102,102,102,063,000,000,000,000 ;ascii 097
db 000,000,096,096,096,096,124,102,102,102,102,124,000,000,000,000 ;ascii 098
db 000,000,000,000,000,000,060,102,096,096,102,060,000,000,000,000 ;ascii 099
db 000,000,006,006,006,006,006,062,102,102,102,062,000,000,000,000 ;ascii 100
db 000,000,000,000,000,000,060,102,102,124,096,060,000,000,000,000 ;ascii 101
db 000,000,000,000,000,000,028,054,054,048,120,048,048,048,048,048 ;ascii 102
db 000,000,000,000,000,000,062,102,102,102,102,062,006,102,102,060 ;ascii 103
db 000,000,096,096,096,096,124,102,102,102,102,102,000,000,000,000 ;ascii 104
db 000,000,000,024,024,000,024,024,024,024,024,024,000,000,000,000 ;ascii 105
db 000,000,000,006,006,000,006,006,006,006,006,102,102,102,102,060 ;ascii 106
db 000,000,096,096,102,102,102,120,102,102,102,102,000,000,000,000 ;ascii 107
db 000,000,024,024,024,024,024,024,024,024,024,024,000,000,000,000 ;ascii 108
db 000,000,000,000,000,000,124,106,106,106,106,106,000,000,000,000 ;ascii 109
db 000,000,000,000,000,000,124,102,102,102,102,102,000,000,000,000 ;ascii 110
db 000,000,000,000,000,000,060,102,102,102,102,060,000,000,000,000 ;ascii 111
db 000,000,000,000,000,000,124,102,102,102,102,124,096,096,096,096 ;ascii 112
db 000,000,000,000,000,000,062,102,102,102,102,062,006,006,006,006 ;ascii 113
db 000,000,000,000,000,000,124,118,096,096,096,096,000,000,000,000 ;ascii 114
db 000,000,000,000,000,000,060,102,048,012,102,060,000,000,000,000 ;ascii 115
db 000,000,048,048,048,048,060,048,048,048,054,028,000,000,000,000 ;ascii 116
db 000,000,000,000,000,000,102,102,102,102,102,060,000,000,000,000 ;ascii 117
db 000,000,000,000,000,000,102,102,102,102,060,024,000,000,000,000 ;ascii 118
db 000,000,000,000,000,000,106,106,106,106,106,124,000,000,000,000 ;ascii 119
db 000,000,000,000,000,000,102,102,060,060,102,102,000,000,000,000 ;ascii 120
db 000,000,000,000,000,000,102,102,102,102,102,062,006,102,102,060 ;ascii 121
db 000,000,000,000,000,000,126,006,012,056,096,126,000,000,000,000 ;ascii 122
db 000,000,014,024,024,024,112,024,024,024,024,014,000,000,000,000 ;ascii 123
db 000,000,024,024,024,024,000,024,024,024,024,024,000,000,000,000 ;ascii 124
db 000,000,112,024,024,024,014,024,024,024,024,112,000,000,000,000 ;ascii 125
db 000,000,118,220,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 126
db 000,000,000,000,016,056,108,198,198,198,254,000,000,000,000,000 ;ascii 127
db 000,000,060,102,194,192,192,192,194,102,060,012,006,124,000,000 ;ascii 128
db 000,000,000,102,102,000,102,102,102,102,102,060,000,000,000,000 ;ascii 129
db 000,000,006,012,024,000,060,102,102,124,096,060,000,000,000,000 ;ascii 130
db 000,000,016,056,108,000,060,102,102,102,102,063,000,000,000,000 ;ascii 131
db 000,000,000,102,102,000,060,102,102,102,102,063,000,000,000,000 ;ascii 132
db 000,000,096,048,024,000,060,102,102,102,102,063,000,000,000,000 ;ascii 133
db 000,000,056,108,056,000,060,102,102,102,102,063,000,000,000,000 ;ascii 134
db 000,000,000,000,060,102,096,096,102,060,012,006,060,000,000,000 ;ascii 135
db 000,000,016,056,108,000,060,102,102,124,096,060,000,000,000,000 ;ascii 136
db 000,000,000,102,102,000,060,102,102,124,096,060,000,000,000,000 ;ascii 137
db 000,000,096,048,024,000,060,102,102,124,096,060,000,000,000,000 ;ascii 138
db 000,000,000,054,054,000,024,024,024,024,024,024,000,000,000,000 ;ascii 139
db 000,000,016,056,108,000,024,024,024,024,024,024,000,000,000,000 ;ascii 140
db 000,000,096,048,024,000,024,024,024,024,024,024,000,000,000,000 ;ascii 141
db 198,198,000,124,198,198,198,254,198,198,198,198,000,000,000,000 ;ascii 142
db 056,108,056,124,198,198,198,254,198,198,198,198,000,000,000,000 ;ascii 143
db 012,024,000,252,192,192,192,248,192,192,192,252,000,000,000,000 ;ascii 144
db 000,000,000,000,000,204,118,054,126,216,216,110,000,000,000,000 ;ascii 145
db 000,000,062,108,204,204,254,204,204,204,204,206,000,000,000,000 ;ascii 146
db 000,000,016,056,108,000,060,102,102,102,102,060,000,000,000,000 ;ascii 147
db 000,000,000,102,102,000,060,102,102,102,102,060,000,000,000,000 ;ascii 148
db 000,000,096,048,024,000,060,102,102,102,102,060,000,000,000,000 ;ascii 149
db 000,000,016,056,108,000,102,102,102,102,102,060,000,000,000,000 ;ascii 150
db 000,000,096,048,024,000,102,102,102,102,102,060,000,000,000,000 ;ascii 151
db 000,000,000,102,102,000,102,102,102,102,102,062,006,102,102,060 ;ascii 152
db 198,198,000,124,198,198,198,198,198,198,198,124,000,000,000,000 ;ascii 153
db 198,198,000,198,198,198,198,198,198,198,198,124,000,000,000,000 ;ascii 154
db 000,024,024,060,102,096,096,096,102,060,024,024,000,000,000,000 ;ascii 155
db 000,056,108,100,096,240,096,096,096,096,230,252,000,000,000,000 ;ascii 156
db 000,000,102,102,060,024,126,024,126,024,024,024,000,000,000,000 ;ascii 157
db 000,248,204,204,248,196,204,222,204,204,204,198,000,000,000,000 ;ascii 158
db 000,014,027,024,024,024,126,024,024,024,024,024,216,112,000,000 ;ascii 159
db 000,000,006,012,024,000,060,102,102,102,102,063,000,000,000,000 ;ascii 160
db 000,000,006,012,024,000,024,024,024,024,024,024,000,000,000,000 ;ascii 161
db 000,000,006,012,024,000,060,102,102,102,102,060,000,000,000,000 ;ascii 162
db 000,000,006,012,024,000,102,102,102,102,102,060,000,000,000,000 ;ascii 163
db 000,000,000,118,220,000,124,102,102,102,102,102,000,000,000,000 ;ascii 164
db 118,220,000,198,230,246,222,206,198,198,198,198,000,000,000,000 ;ascii 165
db 000,060,108,108,062,000,126,000,000,000,000,000,000,000,000,000 ;ascii 166
db 000,056,108,108,056,000,124,000,000,000,000,000,000,000,000,000 ;ascii 167
db 000,000,000,000,000,000,024,024,000,024,024,048,096,099,099,062 ;ascii 168
db 000,000,000,000,000,000,254,192,192,192,192,000,000,000,000,000 ;ascii 169
db 000,000,000,000,000,000,254,006,006,006,006,000,000,000,000,000 ;ascii 170
db 000,192,192,194,198,204,024,048,096,206,147,006,012,031,000,000 ;ascii 171
db 000,192,192,194,198,204,024,048,102,206,154,063,006,015,000,000 ;ascii 172
db 000,000,000,000,000,024,060,024,000,024,024,024,024,024,024,024 ;ascii 173
db 000,000,000,000,000,051,102,204,102,051,000,000,000,000,000,000 ;ascii 174
db 000,000,000,000,000,204,102,051,102,204,000,000,000,000,000,000 ;ascii 175
db 017,068,017,068,017,068,017,068,017,068,017,068,017,068,017,068 ;ascii 176
db 085,170,085,170,085,170,085,170,085,170,085,170,085,170,085,170 ;ascii 177
db 221,119,221,119,221,119,221,119,221,119,221,119,221,119,221,119 ;ascii 178
db 024,024,024,024,024,024,024,024,024,024,024,024,024,024,024,024 ;ascii 179
db 000,198,198,198,198,198,198,198,198,198,198,198,198,198,198,198 ;ascii 180
db 198,198,198,198,198,198,108,108,108,108,108,056,056,056,016,016 ;ascii 181
db 000,028,118,096,192,192,192,192,192,192,192,192,192,192,192,192 ;ascii 182
db 192,192,192,192,192,192,192,192,192,192,192,192,192,096,118,028 ;ascii 183
db 000,192,192,192,192,192,192,192,192,192,192,192,192,192,192,192 ;ascii 184
db 192,220,230,198,198,198,198,198,198,198,198,198,198,198,198,198 ;ascii 185
db 000,124,198,198,006,014,030,054,102,198,198,198,198,198,110,059 ;ascii 186
db 000,220,246,198,192,192,192,192,192,192,192,192,192,192,192,192 ;ascii 187
db 000,195,195,195,195,195,195,227,227,227,227,243,211,211,211,219 ;ascii 188
db 203,203,203,207,199,199,199,199,195,195,195,195,195,195,195,195 ;ascii 189
db 000,255,024,024,024,024,024,024,024,024,024,024,024,024,024,024 ;ascii 190
db 024,024,024,024,024,024,024,024,024,024,024,024,024,024,024,255 ;ascii 191
db 000,000,000,000,000,000,000,000,000,000,000,102,102,102,060,024 ;ascii 192
db 000,000,000,000,000,000,000,254,198,012,056,012,006,006,198,124 ;ascii 193
db 000,000,000,000,000,000,000,006,030,006,006,006,006,006,198,207 ;ascii 194
db 000,000,000,000,000,000,000,056,108,198,198,198,198,198,108,056 ;ascii 195
db 000,000,000,000,000,000,000,255,000,000,000,000,000,000,000,000 ;ascii 196
db 024,024,024,024,024,024,024,255,024,024,024,024,024,024,024,024 ;ascii 197
db 024,024,024,024,024,031,024,031,024,024,024,024,024,024,024,024 ;ascii 198
db 054,054,054,054,054,054,054,055,054,054,054,054,054,054,054,054 ;ascii 199
db 054,054,054,054,054,055,048,063,000,000,000,000,000,000,000,000 ;ascii 200
db 000,000,000,000,000,063,048,055,054,054,054,054,054,054,054,054 ;ascii 201
db 054,054,054,054,054,247,000,255,000,000,000,000,000,000,000,000 ;ascii 202
db 000,000,000,000,000,255,000,247,054,054,054,054,054,054,054,054 ;ascii 203
db 054,054,054,054,054,055,048,055,054,054,054,054,054,054,054,054 ;ascii 204
db 000,000,000,000,000,255,000,255,000,000,000,000,000,000,000,000 ;ascii 205
db 024,024,024,024,024,255,000,255,000,000,000,000,000,000,000,000 ;ascii 206
db 024,024,024,024,024,255,000,255,000,000,000,000,000,000,000,000 ;ascii 207
db 054,054,054,054,054,054,054,255,000,000,000,000,000,000,000,000 ;ascii 208
db 000,000,000,000,000,255,000,255,024,024,024,024,024,024,024,024 ;ascii 209
db 000,000,000,000,000,000,000,255,054,054,054,054,054,054,054,054 ;ascii 210
db 054,054,054,054,054,054,054,063,000,000,000,000,000,000,000,000 ;ascii 211
db 024,024,024,024,024,031,024,031,000,000,000,000,000,000,000,000 ;ascii 212
db 000,000,000,000,000,031,024,031,024,024,024,024,024,024,024,024 ;ascii 213
db 000,000,000,000,000,000,000,063,054,054,054,054,054,054,054,054 ;ascii 214
db 054,054,054,054,054,054,054,255,054,054,054,054,054,054,054,054 ;ascii 215
db 024,024,024,024,024,255,024,255,024,024,024,024,024,024,024,024 ;ascii 216
db 024,024,024,024,024,024,024,248,000,000,000,000,000,000,000,000 ;ascii 217
db 000,000,000,000,000,000,000,031,024,024,024,024,024,024,024,024 ;ascii 218
db 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255 ;ascii 219
db 000,000,000,000,000,000,000,255,255,255,255,255,255,255,255,255 ;ascii 220
db 240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240 ;ascii 221
db 015,015,015,015,015,015,015,015,015,015,015,015,015,015,015,015 ;ascii 222
db 255,255,255,255,255,255,255,000,000,000,000,000,000,000,000,000 ;ascii 223
db 000,000,000,000,000,118,220,216,216,216,220,118,000,000,000,000 ;ascii 224
db 000,000,000,000,000,252,198,252,198,198,252,192,192,192,000,000 ;ascii 225
db 000,000,254,198,198,192,192,192,192,192,192,192,000,000,000,000 ;ascii 226
db 000,000,000,000,128,254,108,108,108,108,108,108,000,000,000,000 ;ascii 227
db 000,000,000,254,198,096,048,024,048,096,198,254,000,000,000,000 ;ascii 228
db 000,000,000,000,000,126,216,216,216,216,216,112,000,000,000,000 ;ascii 229
db 000,000,000,000,102,102,102,102,102,124,096,096,192,000,000,000 ;ascii 230
db 000,000,000,000,118,220,024,024,024,024,024,024,000,000,000,000 ;ascii 231
db 000,000,000,126,024,060,102,102,102,060,024,126,000,000,000,000 ;ascii 232
db 000,000,000,056,108,198,198,254,198,198,108,056,000,000,000,000 ;ascii 233
db 000,000,007,012,025,050,050,050,025,012,007,000,000,000,000,000 ;ascii 234
db 000,000,224,048,152,012,012,012,152,048,224,000,000,000,000,000 ;ascii 235
db 000,000,003,015,063,127,127,255,248,255,127,127,063,015,003,000 ;ascii 236
db 015,015,239,255,255,248,255,255,056,255,255,255,255,255,239,000 ;ascii 237
db 252,252,252,253,255,031,239,239,030,239,239,239,255,255,255,000 ;ascii 238
db 048,120,252,254,254,255,255,255,003,255,255,255,255,255,255,000 ;ascii 239
db 000,000,000,000,000,000,128,128,192,224,224,240,240,248,248,000 ;ascii 240
db 000,015,000,000,015,000,000,015,000,000,000,000,000,000,000,000 ;ascii 241
db 000,239,000,000,204,108,108,199,000,000,000,000,000,000,000,000 ;ascii 242
db 000,207,096,096,111,108,108,204,000,000,000,000,000,000,000,000 ;ascii 243
db 000,239,000,000,195,003,003,003,000,000,000,000,000,000,000,000 ;ascii 244
db 000,224,000,000,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 245
db 000,000,000,000,000,000,000,000,000,000,000,000,000,000,000,000 ;ascii 246
db 000,000,255,219,153,024,024,024,024,024,024,060,000,000,000,000 ;ascii 247
db 000,000,127,102,070,022,030,022,006,006,006,015,000,000,000,000 ;ascii 248
db 000,000,028,054,099,099,099,099,099,099,054,028,000,000,000,000 ;ascii 249
db 000,000,062,099,099,006,028,048,096,099,099,062,000,000,000,000 ;ascii 250
db 000,000,008,028,054,099,099,127,099,099,099,099,000,000,000,000 ;ascii 251
db 000,000,063,102,102,102,062,054,102,102,102,103,000,000,000,000 ;ascii 252
db 000,000,127,102,070,022,030,022,006,070,102,127,000,000,000,000 ;ascii 253
db 124,214,254,198,124,056,254,186,185,185,062,098,066,067,192,000 ;ascii 254
db 062,107,127,099,062,028,127,093,157,157,124,070,066,194,003,000 ;ascii 255

;************ los de arriba : caracteres generados por erasoft en 8x16
tabla_riq label byte
db 000,000,000,000,000,000,000,000 ;ascii 000
grid8 db 8 dup(0);ascii 001
db 126,255,219,255,219,231,255,126 ;ascii 002
db 108,254,254,254,124,056,016,000 ;ascii 003
db 016,056,124,254,124,056,016,000 ;ascii 004
db 056,124,056,254,254,146,016,124 ;ascii 005
db 000,016,056,124,254,124,056,124 ;ascii 006
db 000,000,024,060,060,024,000,000 ;ascii 007
db 255,255,231,195,195,231,255,255 ;ascii 008
db 000,060,102,066,066,102,060,000 ;ascii 009
db 255,195,153,189,189,153,195,255 ;ascii 010
db 015,007,015,125,204,204,204,120 ;ascii 011
db 060,102,102,102,060,024,126,024 ;ascii 012
db 063,051,063,048,048,112,240,224 ;ascii 013
db 031,115,103,123,099,103,230,192 ;ascii 014
db 153,090,060,231,231,060,090,153 ;ascii 015
db 008,012,014,255,014,012,008,000 ;ascii 016
db 016,048,112,255,112,048,016,000 ;ascii 017
db 024,060,126,024,024,126,060,024 ;ascii 018
db 102,102,102,102,102,000,102,000 ;ascii 019
db 127,219,219,123,027,027,027,000 ;ascii 020
db 062,099,056,108,108,056,134,252 ;ascii 021
db 000,000,000,000,126,126,126,000 ;ascii 022
db 024,060,126,024,126,060,024,255 ;ascii 023
db 024,060,126,024,024,024,024,000 ;ascii 024
db 024,024,024,024,126,060,024,000 ;ascii 025
db 000,024,012,254,012,024,000,000 ;ascii 026
db 000,048,096,254,096,048,000,000 ;ascii 027
db 000,000,192,192,192,254,000,000 ;ascii 028
db 000,036,102,255,102,036,000,000 ;ascii 029
db 000,024,060,126,255,255,000,000 ;ascii 030
db 000,255,255,126,060,024,000,000 ;ascii 031
db 000,000,000,000,000,000,000,000 ;ascii 032
db 024,060,060,024,024,000,024,000 ;ascii 033
db 119,119,034,000,000,000,000,000 ;ascii 034
db 108,108,254,108,254,108,108,000 ;ascii 035
db 024,126,192,124,006,252,024,000 ;ascii 036
db 000,198,204,024,048,102,198,000 ;ascii 037
db 056,108,056,118,220,204,118,000 ;ascii 038
db 048,048,096,000,000,000,000,000 ;ascii 039
db 024,048,096,096,096,048,024,000 ;ascii 040
db 096,048,024,024,024,048,096,000 ;ascii 041
db 000,102,060,255,060,102,000,000 ;ascii 042
db 000,024,024,126,024,024,000,000 ;ascii 043
db 000,000,000,000,000,024,024,048 ;ascii 044
db 000,000,000,126,000,000,000,000 ;ascii 045
db 000,000,000,000,000,024,024,000 ;ascii 046
db 006,012,024,048,096,192,128,000 ;ascii 047
db 124,198,198,222,246,198,124,000 ;ascii 048
db 048,112,048,048,048,048,048,000 ;ascii 049
db 120,204,012,056,096,192,252,000 ;ascii 050
db 252,012,024,056,012,204,120,000 ;ascii 051
db 204,204,204,204,252,012,012,000 ;ascii 052
db 252,192,248,012,012,204,120,000 ;ascii 053
db 096,192,192,248,204,204,120,000 ;ascii 054
db 252,012,012,024,048,048,048,000 ;ascii 055
db 120,204,204,120,204,204,120,000 ;ascii 056
db 120,204,204,124,012,012,024,000 ;ascii 057
db 000,024,024,000,000,024,024,000 ;ascii 058
db 000,024,024,000,000,024,024,048 ;ascii 059
db 024,048,096,192,096,048,024,000 ;ascii 060
db 000,000,126,000,126,000,000,000 ;ascii 061
db 096,048,024,012,024,048,096,000 ;ascii 062
db 060,102,012,024,024,000,024,000 ;ascii 063
db 124,198,222,222,220,192,124,000 ;ascii 064
db 048,120,204,204,252,204,204,000 ;ascii 065
db 124,102,102,124,102,102,124,000 ;ascii 066
db 060,102,192,192,192,102,060,000 ;ascii 067
db 120,108,102,102,102,108,120,000 ;ascii 068
db 126,096,096,120,096,096,126,000 ;ascii 069
db 126,096,096,120,096,096,096,000 ;ascii 070
db 060,102,192,192,206,102,058,000 ;ascii 071
db 204,204,204,252,204,204,204,000 ;ascii 072
db 048,048,048,048,048,048,048,000 ;ascii 073
db 012,012,012,012,204,204,120,000 ;ascii 074
db 102,102,108,120,108,102,102,000 ;ascii 075
db 096,096,096,096,096,096,126,000 ;ascii 076
db 198,238,254,254,214,198,198,000 ;ascii 077
db 198,230,246,222,206,198,198,000 ;ascii 078
db 056,108,198,198,198,108,056,000 ;ascii 079
db 124,102,102,124,096,096,096,000 ;ascii 080
db 124,198,198,198,214,124,014,000 ;ascii 081
db 124,102,102,124,108,102,102,000 ;ascii 082
db 124,198,224,120,014,198,124,000 ;ascii 083
db 252,048,048,048,048,048,048,000 ;ascii 084
db 204,204,204,204,204,204,120,000 ;ascii 085
db 204,204,204,204,204,120,048,000 ;ascii 086
db 198,198,198,198,214,254,108,000 ;ascii 087
db 198,198,108,056,108,198,198,000 ;ascii 088
db 204,204,204,120,048,048,048,000 ;ascii 089
db 254,198,140,024,050,102,254,000 ;ascii 090
db 120,096,096,096,096,096,120,000 ;ascii 091
db 192,096,048,024,012,006,002,000 ;ascii 092
db 120,024,024,024,024,024,120,000 ;ascii 093
db 016,056,108,198,000,000,000,000 ;ascii 094
db 000,000,000,000,000,000,000,255 ;ascii 095
db 048,048,024,000,000,000,000,000 ;ascii 096
db 000,000,120,012,124,204,124,000 ;ascii 097
db 096,096,096,124,102,102,124,000 ;ascii 098
db 000,000,120,204,192,204,120,000 ;ascii 099
db 012,012,012,124,204,204,124,000 ;ascii 100
db 000,000,120,204,252,192,120,000 ;ascii 101
db 056,108,100,112,096,096,096,000 ;ascii 102
db 000,000,124,204,204,124,012,248 ;ascii 103
db 096,096,108,118,102,102,102,000 ;ascii 104
db 048,000,048,048,048,048,048,000 ;ascii 105
db 012,000,012,012,012,204,204,120 ;ascii 106
db 096,096,102,108,120,108,102,000 ;ascii 107
db 048,048,048,048,048,048,048,000 ;ascii 108
db 000,000,204,254,254,214,214,000 ;ascii 109
db 000,000,184,204,204,204,204,000 ;ascii 110
db 000,000,120,204,204,204,120,000 ;ascii 111
db 000,000,124,102,102,124,096,096 ;ascii 112
db 000,000,124,204,204,124,012,012 ;ascii 113
db 000,000,124,118,098,096,096,000 ;ascii 114
db 000,000,124,192,112,028,248,000 ;ascii 115
db 016,048,120,048,048,052,024,000 ;ascii 116
db 000,000,204,204,204,204,124,000 ;ascii 117
db 000,000,204,204,204,120,048,000 ;ascii 118
db 000,000,198,198,214,254,108,000 ;ascii 119
db 000,000,198,108,056,108,198,000 ;ascii 120
db 000,000,204,204,204,124,012,248 ;ascii 121
db 000,000,252,024,048,096,252,000 ;ascii 122
db 028,048,048,224,048,048,028,000 ;ascii 123
db 024,024,024,000,024,024,024,000 ;ascii 124
db 224,048,048,028,048,048,224,000 ;ascii 125
db 118,220,000,000,000,000,000,000 ;ascii 126
db 000,016,056,108,198,198,254,000 ;ascii 127
db 124,198,192,198,124,012,006,124 ;ascii 128
db 000,204,000,204,204,204,124,000 ;ascii 129
db 028,000,120,204,252,192,120,000 ;ascii 130
db 126,129,060,006,062,102,062,000 ;ascii 131
db 204,000,120,012,124,204,124,000 ;ascii 132
db 224,000,120,012,124,204,124,000 ;ascii 133
db 048,048,120,012,124,204,124,000 ;ascii 134
db 000,000,124,198,192,120,012,056 ;ascii 135
db 126,129,060,102,126,096,060,000 ;ascii 136
db 204,000,120,204,252,192,120,000 ;ascii 137
db 224,000,120,204,252,192,120,000 ;ascii 138
db 204,000,048,048,048,048,048,000 ;ascii 139
db 124,130,024,024,024,024,024,000 ;ascii 140
db 224,000,048,048,048,048,048,000 ;ascii 141
db 198,016,124,198,254,198,198,000 ;ascii 142
db 048,048,000,120,204,252,204,000 ;ascii 143
db 028,000,124,096,120,096,124,000 ;ascii 144
db 000,000,127,012,127,204,127,000 ;ascii 145
db 062,108,204,254,204,204,206,000 ;ascii 146
db 120,132,000,120,204,204,120,000 ;ascii 147
db 000,204,000,120,204,204,120,000 ;ascii 148
db 000,224,000,120,204,204,120,000 ;ascii 149
db 120,132,000,204,204,204,124,000 ;ascii 150
db 000,224,000,204,204,204,124,000 ;ascii 151
db 000,204,000,204,204,124,012,248 ;ascii 152
db 195,024,060,102,102,060,024,000 ;ascii 153
db 204,000,204,204,204,204,120,000 ;ascii 154
db 024,024,126,192,192,126,024,024 ;ascii 155
db 056,108,100,240,096,230,252,000 ;ascii 156
db 204,204,120,048,252,048,252,048 ;ascii 157
db 248,204,204,250,198,207,198,195 ;ascii 158
db 014,027,024,060,024,024,216,112 ;ascii 159
db 028,000,120,012,124,204,124,000 ;ascii 160
db 056,000,048,048,048,048,048,000 ;ascii 161
db 000,028,000,120,204,204,120,000 ;ascii 162
db 000,028,000,204,204,204,124,000 ;ascii 163
db 000,248,000,184,204,204,204,000 ;ascii 164
db 252,000,204,236,252,220,204,000 ;ascii 165
db 060,108,108,062,000,126,000,000 ;ascii 166
db 056,108,108,056,000,124,000,000 ;ascii 167
db 024,000,024,024,048,102,060,000 ;ascii 168
db 000,000,000,252,192,192,000,000 ;ascii 169
db 000,000,000,252,012,012,000,000 ;ascii 170
db 198,204,216,054,107,194,132,015 ;ascii 171
db 195,198,204,219,055,109,207,003 ;ascii 172
db 024,000,024,024,060,060,024,000 ;ascii 173
db 000,051,102,204,102,051,000,000 ;ascii 174
db 000,204,102,051,102,204,000,000 ;ascii 175
db 034,136,034,136,034,136,034,136 ;ascii 176
db 085,170,085,170,085,170,085,170 ;ascii 177
db 221,119,221,119,221,119,221,119 ;ascii 178
db 024,024,024,024,024,024,024,024 ;ascii 179
db 000,198,198,198,198,198,198,198 ;ascii 180
db 198,198,198,198,108,108,056,016 ;ascii 181
db 000,056,108,198,192,192,192,192 ;ascii 182
db 192,192,192,192,192,198,108,056 ;ascii 183
db 000,192,192,192,192,192,192,192 ;ascii 184
db 216,252,230,198,198,198,198,198 ;ascii 185
db 124,198,006,126,198,198,198,125 ;ascii 186
db 220,246,230,192,192,192,192,192 ;ascii 187
db 000,195,195,195,195,227,243,219 ;ascii 188
db 207,199,195,195,195,195,195,195 ;ascii 189
db 000,255,024,024,024,024,024,024 ;ascii 190
db 024,024,024,024,024,024,024,255 ;ascii 191
db 000,000,000,102,102,102,060,024 ;ascii 192
db 000,252,012,024,056,012,204,120 ;ascii 193
db 000,012,028,012,012,012,204,204 ;ascii 194
db 000,124,198,198,198,198,198,124 ;ascii 195
db 000,000,000,000,255,000,000,000 ;ascii 196
db 024,024,024,024,255,024,024,024 ;ascii 197
db 024,024,031,024,031,024,024,024 ;ascii 198
db 054,054,054,054,055,054,054,054 ;ascii 199
db 054,054,055,048,063,000,000,000 ;ascii 200
db 000,000,063,048,055,054,054,054 ;ascii 201
db 054,054,247,000,255,000,000,000 ;ascii 202
db 000,000,255,000,247,054,054,054 ;ascii 203
db 054,054,055,048,055,054,054,054 ;ascii 204
db 000,000,255,000,255,000,000,000 ;ascii 205
db 054,054,247,000,247,054,054,054 ;ascii 206
db 024,024,255,000,255,000,000,000 ;ascii 207
db 054,054,054,054,255,000,000,000 ;ascii 208
db 000,000,255,000,255,024,024,024 ;ascii 209
db 000,000,000,000,255,054,054,054 ;ascii 210
db 054,054,054,054,063,000,000,000 ;ascii 211
db 024,024,031,024,031,000,000,000 ;ascii 212
db 000,000,031,024,031,024,024,024 ;ascii 213
db 000,000,000,000,063,054,054,054 ;ascii 214
db 054,054,054,054,255,054,054,054 ;ascii 215
db 024,024,255,024,255,024,024,024 ;ascii 216
db 024,024,024,024,248,000,000,000 ;ascii 217
db 000,000,000,000,031,024,024,024 ;ascii 218
db 255,255,255,255,255,255,255,255 ;ascii 219
db 000,000,000,000,255,255,255,255 ;ascii 220
db 240,240,240,240,240,240,240,240 ;ascii 221
db 015,015,015,015,015,015,015,015 ;ascii 222
db 255,255,255,255,000,000,000,000 ;ascii 223
db 000,000,118,220,200,220,118,000 ;ascii 224
db 000,120,204,248,204,248,192,192 ;ascii 225
db 000,252,204,192,192,192,192,000 ;ascii 226
db 000,000,254,108,108,108,108,000 ;ascii 227
db 252,204,096,048,096,204,252,000 ;ascii 228
db 000,000,126,216,216,216,112,000 ;ascii 229
db 000,102,102,102,102,124,096,192 ;ascii 230
db 000,118,220,024,024,024,024,000 ;ascii 231
db 252,048,120,204,204,120,048,252 ;ascii 232
db 056,108,198,254,198,108,056,000 ;ascii 233
db 007,012,025,026,026,025,012,007 ;ascii 234
db 224,048,152,024,024,152,048,224 ;ascii 235
db 031,031,028,028,028,028,028,028 ;ascii 236
db 000,192,240,056,028,028,028,056 ;ascii 237
db 024,060,060,024,000,024,024,060 ;ascii 238
db 007,031,060,120,112,112,112,112 ;ascii 239
db 224,248,060,030,014,014,014,014 ;ascii 240
db 031,031,028,028,028,028,028,028 ;ascii 241
db 224,224,112,056,028,028,028,028 ;ascii 242
db 060,060,060,060,060,060,060,060 ;ascii 243
db 112,112,112,112,120,060,031,007 ;ascii 244
db 014,014,014,238,254,060,255,239 ;ascii 245
db 000,000,000,000,000,000,000,000 ;ascii 246
db 000,000,029,051,051,051,051,000 ;ascii 247
db 000,000,030,051,051,051,030,000 ;ascii 248
db 030,054,102,102,102,054,030,000 ;ascii 249
db 000,000,000,000,000,000,000,000 ;ascii 250
db 000,000,062,051,051,062,048,048 ;ascii 251
db 012,000,012,012,012,012,012,000 ;ascii 252
db 063,102,102,062,054,102,103,000 ;ascii 253
db 195,090,060,024,024,036,036,036 ;ascii 254
db 036,090,060,024,024,036,038,032 ;ascii 255
final equ this byte

ruti endp
code ends
        end ruti
